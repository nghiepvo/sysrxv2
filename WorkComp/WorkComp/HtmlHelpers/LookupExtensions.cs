﻿using System.Collections.Generic;
using System.Web.Mvc;
using System.Web.Mvc.Html;
using System.Web.Routing;
using WorkComp.Models;
using WorkComp.Models.Lookup;

namespace WorkComp.HtmlHelpers
{
    public static class LookupExtensions
    {
        public static MvcHtmlString StateLookup(this HtmlHelper htmlHelper, string id, string label, object htmlAttribute = null, bool populatedByChildren = false, string hierarchyGroupName = "", string urlToReadData = "", int currentId = 0, object dataSource = null, bool isRequied = false)
        {
            return Lookup(htmlHelper, id, label, "State", "~/Views/Shared/Lookup/_StateLookup.cshtml", populatedByChildren, hierarchyGroupName, urlToReadData, isRequied, htmlAttribute, currentId, dataSource);
        }

        public static MvcHtmlString CityLookup(this HtmlHelper htmlHelper, string id, string label, object htmlAttribute = null, bool populatedByChildren = false, string hierarchyGroupName = "", string urlToReadData = "", int currentId = 0, object dataSource = null, bool isRequied = false)
        {
            return Lookup(htmlHelper, id, label, "City", "~/Views/Shared/Lookup/_CityLookup.cshtml", populatedByChildren, hierarchyGroupName, urlToReadData, isRequied, htmlAttribute, currentId, dataSource);
        }

        public static MvcHtmlString ZipLookup(this HtmlHelper htmlHelper, string id, string label, object htmlAttribute = null, bool populatedByChildren = false, string hierarchyGroupName = "", string urlToReadData = "", int currentId = 0,  object dataSource = null,bool isRequied = false)
        {
            return Lookup(htmlHelper, id, label, "Zip", "~/Views/Shared/Lookup/_ZipLookup.cshtml", populatedByChildren, hierarchyGroupName, urlToReadData, isRequied, htmlAttribute, currentId, dataSource);
        }
        public static MvcHtmlString ClaimantLookup(this HtmlHelper htmlHelper, string id, string label, object htmlAttribute = null, bool populatedByChildren = false, string hierarchyGroupName = "", string urlToReadData = "", int currentId = 0, object dataSource = null, bool isRequied = false)
        {
            return Lookup(htmlHelper, id, label, "Claimant", "~/Views/Shared/Lookup/_ClaimantLookup.cshtml", populatedByChildren, hierarchyGroupName, urlToReadData, isRequied, htmlAttribute, currentId, dataSource);
        }

        public static MvcHtmlString AdjusterLookup(this HtmlHelper htmlHelper, string id, string label, object htmlAttribute = null, bool populatedByChildren = false, string hierarchyGroupName = "", string urlToReadData = "", int currentId = 0, object dataSource = null, bool isRequied = false)
        {
            return Lookup(htmlHelper, id, label, "Adjuster", "~/Views/Shared/Lookup/_AdjusterLookup.cshtml", populatedByChildren, hierarchyGroupName, urlToReadData, isRequied, htmlAttribute, currentId, dataSource);
        }

        public static MvcHtmlString StateAbbreviationLookup(this HtmlHelper htmlHelper, string id, string label, object htmlAttribute = null, int currentId = 0, object dataSource = null, bool isRequied = false)
        {
            return Lookup(htmlHelper, id, label, "State", "", false,"", "/State/GetJurisdiction",  isRequied, htmlAttribute, currentId, dataSource);
        }

        public static MvcHtmlString AssignToLookup(this HtmlHelper htmlHelper, string id, string label, object htmlAttribute = null, string urlToReadData = "", int currentId = 0, object dataSource = null, bool isRequied = false, int heightLookup=250)
        {
            return Lookup(htmlHelper, id, label, "User", "", false, "", urlToReadData, isRequied, htmlAttribute, currentId, dataSource, heightLookup);
        }

        public static MvcHtmlString RoleLookup(this HtmlHelper htmlHelper, string id, string label, object htmlAttribute = null, string urlToReadData = "", int currentId = 0, object dataSource = null, bool isRequied = false)
        {
            return Lookup(htmlHelper, id, label, "UserRole", "", false, "", urlToReadData, isRequied, htmlAttribute, currentId, dataSource);
        }

        public static MvcHtmlString PayerLookup(this HtmlHelper htmlHelper, string id, string label, object htmlAttribute = null, bool populatedByChildren = false, string hierarchyGroupName = "", string urlToReadData = "", int currentId = 0, object dataSource = null, bool isRequied = false)
        {
            return Lookup(htmlHelper, id, label, "Payer", "~/Views/Shared/Lookup/_PayerLookup.cshtml", populatedByChildren, hierarchyGroupName, urlToReadData, isRequied, htmlAttribute, currentId, dataSource);
        }

        public static MvcHtmlString BranchLookup(this HtmlHelper htmlHelper, string id, string label, object htmlAttribute = null, bool populatedByChildren = false, string hierarchyGroupName = "", string urlToReadData = "", int currentId = 0, object dataSource = null, bool isRequied = false)
        {
            return Lookup(htmlHelper, id, label, "Branch", "~/Views/Shared/Lookup/_BranchLookup.cshtml", populatedByChildren, hierarchyGroupName, urlToReadData, isRequied, htmlAttribute, currentId, dataSource);
        }

        public static MvcHtmlString ClaimantLanguageLookup(this HtmlHelper htmlHelper, string id, string label, object htmlAttribute = null, string urlToReadData = "", int currentId = 0, object dataSource = null, bool isRequied = false)
        {
            return Lookup(htmlHelper, id, label, "ClaimantLanguage", "", false, "", urlToReadData, isRequied, htmlAttribute, currentId, dataSource);
        }

        public static MvcHtmlString AssayCodeDescriptionLookup(this HtmlHelper htmlHelper, string id, string label, object htmlAttribute = null, string urlToReadData = "", int currentId = 0, object dataSource = null, bool isRequied = false)
        {
            return Lookup(htmlHelper, id, label, "AssayCodeDescription", "", false, "", urlToReadData, isRequied, htmlAttribute, currentId, dataSource);
        }

        public static MvcHtmlString ReferralSourceLookup(this HtmlHelper htmlHelper, string id, string label, object htmlAttribute = null, string urlToReadData = "", int currentId = 0, object dataSource = null, bool isRequied = false)
        {
            return Lookup(htmlHelper, id, label, "ReferralSource", "", false, "", urlToReadData, isRequied, htmlAttribute, currentId, dataSource);
        }

        public static MvcHtmlString ClaimNumberLookup(this HtmlHelper htmlHelper, string id, string label, object htmlAttribute = null, bool populatedByChildren = false, string hierarchyGroupName = "", string urlToReadData = "", int currentId = 0, object dataSource = null, bool isRequied = false)
        {
            return Lookup(htmlHelper, id, label, "ClaimNumber", "~/Views/Shared/Lookup/_ClaimNumberLookup.cshtml", populatedByChildren, hierarchyGroupName, urlToReadData, isRequied, htmlAttribute, currentId, dataSource);
        }

        public static MvcHtmlString CaseManagerLookup(this HtmlHelper htmlHelper, string id, string label, object htmlAttribute = null, bool populatedByChildren = false, string hierarchyGroupName = "", string urlToReadData = "", int currentId = 0, object dataSource = null, bool isRequied = false)
        {
            return Lookup(htmlHelper, id, label, "CaseManager", "~/Views/Shared/Lookup/_CaseManagerLookup.cshtml", populatedByChildren, hierarchyGroupName, urlToReadData, isRequied, htmlAttribute, currentId, dataSource);
        }

        public static MvcHtmlString AttorneyLookup(this HtmlHelper htmlHelper, string id, string label, object htmlAttribute = null, string urlToReadData = "", int currentId = 0, object dataSource = null, bool isRequied = false)
        {
            return Lookup(htmlHelper, id, label, "Attorney", "", false, "", urlToReadData, isRequied, htmlAttribute, currentId, dataSource);
        }

        public static MvcHtmlString EmployerLookup(this HtmlHelper htmlHelper, string id, string label, object htmlAttribute = null, bool populatedByChildren = false, string hierarchyGroupName = "", string urlToReadData = "", int currentId = 0, object dataSource = null, bool isRequied = false)
        {
            return Lookup(htmlHelper, id, label, "Employer", "~/Views/Shared/Lookup/_EmployerLookup.cshtml", populatedByChildren, hierarchyGroupName, urlToReadData, isRequied, htmlAttribute, currentId, dataSource);
        }

        public static MvcHtmlString ReferralTypeLookup(this HtmlHelper htmlHelper, string id, string label, object htmlAttribute = null, string urlToReadData = "", int currentId = 0, object dataSource = null, bool isRequied = false)
        {
            return Lookup(htmlHelper, id, label, "ReferralType", "", false, "", urlToReadData, isRequied, htmlAttribute, currentId, dataSource);
        }

        public static MvcHtmlString ProductTypeLookup(this HtmlHelper htmlHelper, string id, string label, object htmlAttribute = null, bool populatedByChildren = false, string hierarchyGroupName = "", string urlToReadData = "", int currentId = 0, object dataSource = null, bool isRequied = false)
        {
            return Lookup(htmlHelper, id, label, "ProductType", "~/Views/Shared/Lookup/_ProductTypeLookup.cshtml", populatedByChildren, hierarchyGroupName, urlToReadData, isRequied, htmlAttribute, currentId, dataSource);
        }

        public static MvcHtmlString CollectionSiteLookup(this HtmlHelper htmlHelper, string id, string label, object htmlAttribute = null, string urlToReadData = "", int currentId = 0, object dataSource = null, bool isRequied = false)
        {
            return Lookup(htmlHelper, id, label, "CollectionSite", "", false, "", urlToReadData, isRequied, htmlAttribute, currentId, dataSource);
        }

        public static MvcHtmlString TreatingPhysicianLookup(this HtmlHelper htmlHelper, string id, string label, object htmlAttribute = null, string urlToReadData = "", int currentId = 0, object dataSource = null, bool isRequied = false)
        {
            return Lookup(htmlHelper, id, label, "TreatingPhysician", "", false, "", "/NpiNumber/GetLookupTreatingPhysician", isRequied, htmlAttribute, currentId, dataSource);
        }

        public static MvcHtmlString NpiNumberLookup(this HtmlHelper htmlHelper, string id, string label, object htmlAttribute = null, string urlToReadData = "", int currentId = 0, object dataSource = null, bool isRequied = false)
        {
            return Lookup(htmlHelper, id, label, "NpiNumber", "", false, "", urlToReadData, isRequied, htmlAttribute, currentId, dataSource);
        }

        public static MvcHtmlString PanelTypeLookup(this HtmlHelper htmlHelper, string id, string label, object htmlAttribute = null, bool populatedByChildren = false, string hierarchyGroupName = "", string urlToReadData = "", int currentId = 0, object dataSource = null, bool isRequied = false)
        {
            return Lookup(htmlHelper, id, label, "PanelType", "~/Views/Shared/Lookup/_PanelTypeLookup.cshtml", populatedByChildren, hierarchyGroupName, urlToReadData, isRequied, htmlAttribute, currentId, dataSource);
        }

        public static MvcHtmlString ReferralIdLookup(this HtmlHelper htmlHelper, string id, string label, object htmlAttribute = null, string urlToReadData = "", int currentId = 0, object dataSource = null, bool isRequied = false)
        {
            return Lookup(htmlHelper, id, label, "Referral", "", false, "", urlToReadData, isRequied, htmlAttribute, currentId, dataSource);
        }

        public static MvcHtmlString TaskTemplateLookup(this HtmlHelper htmlHelper, string id, string label, object htmlAttribute = null, string urlToReadData = "", int currentId = 0, object dataSource = null, bool isRequied = false)
        {
            return Lookup(htmlHelper, id, label, "TaskTemplate", "", false, "", urlToReadData, isRequied, htmlAttribute, currentId, dataSource);
        }


        



        public static MvcHtmlString Lookup(this HtmlHelper htmlHelper, string lookupId,
            string label,
            string modelName,
            string url="",
            bool populatedByChildren = false,
            string hierarchyGroupName = "",
            string urlToReadData = "", bool isRequied = false, object htmlAttribute = null, int currentId = 0, object dataSource = null, int heightLookup=250)
        {
            var attribute = new RouteValueDictionary();
            if (htmlAttribute != null)
            {
                attribute = HtmlHelper.AnonymousObjectToHtmlAttributes(htmlAttribute);
            }

            attribute.Add("id", lookupId);

            if (attribute.ContainsKey("style"))
            {
                if (!attribute["style"].ToString().Contains("width"))
                {
                    attribute["style"] = attribute["style"] + "width: 270px;";
                }
            }
            else
            {
                attribute.Add("style", "");
            }

            if (isRequied)
            {
                attribute["required"] = "required";
            }

            attribute["style"] = attribute["style"] + "display: none;";

            var model = new LookupViewModel
            {
                ID = lookupId,
                Label = label,
                ModelName = modelName,
                UrlToReadData = urlToReadData,
                CurrentId = currentId,
                HtmlAttributes = attribute,
                HierarchyGroupName=hierarchyGroupName,
                PopulatedByChildren = populatedByChildren,
                DataSource = new List<object>(),
                HeightLookup = heightLookup
            };
            if (dataSource != null) model.DataSource.Add(dataSource);
            if (string.IsNullOrEmpty(url))
            {
                url = "~/Views/Shared/Lookup/_Lookup.cshtml";
            }
            return htmlHelper.Partial(url, model);
        }


        public static MvcHtmlString WorkcompDropdownList(this HtmlHelper htmlHelper,
            string lookupId,
            string label,
            string modelName,
            string urlToReadData, object htmlAttribute = null, object currentId = null, bool isRequired = false,string styleAttribute="")
        {
            var attribute = new RouteValueDictionary();
            if (htmlAttribute != null)
            {
                attribute = HtmlHelper.AnonymousObjectToHtmlAttributes(htmlAttribute);
            }

            var model = new DropdownListViewModel
            {
                ID = lookupId,
                ModelName = modelName,
                Label = label,
                UrlToReadData = urlToReadData,
                CurrentId = currentId,
                HtmlAttributes = attribute,
                IsRequired = isRequired,
                StyleAttribute = styleAttribute
            };
            return htmlHelper.Partial("~/Views/Shared/Lookup/_DropdownList.cshtml", model);
        }

        public static MvcHtmlString WorkcompGenderDropdownList(this HtmlHelper htmlHelper,
            string lookupId,
            string label,
            string controllerName, object htmlAttribute = null, string currentValue = "", bool isRequired = false)
        {
            return WorkcompDropdownList(htmlHelper, lookupId, label, "Gender", "/Common/GenderLookup", htmlAttribute,
                currentValue, isRequired);
        }

       
    }
}