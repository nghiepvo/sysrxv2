﻿using System;
using System.Text;
using System.Web.Mvc;

namespace WorkComp.HtmlHelpers
{
    public static class FooterActionExtensions
    {
        public static MvcHtmlString FooterAction<TModel>(this HtmlHelper<TModel> htmlHelper, string action, string icon, string text, bool ignoreDirtyState, bool permission = true)
        {
            var ID = Guid.NewGuid();
            var controlStructure = new StringBuilder();

            controlStructure.AppendLine("<span><button class=\"win-command k-button\" id=\"footer-button-action\"  data-function=\"" +
                                        action + "\"");
            if (ignoreDirtyState) controlStructure.Append(" data-ays-ignore=\"true\"");
            if (!permission) controlStructure.Append(" disabled ");
            controlStructure.Append(" />");

            controlStructure.AppendLine("   <span class=\"k-icon " + icon + "\"></span>");
            controlStructure.AppendLine("<span id='lable-btn-"+ text.ToLower() +"'> "+text+"</span>" );
            controlStructure.AppendLine("</span>");
            return new MvcHtmlString(controlStructure.ToString());
        }
    }
}