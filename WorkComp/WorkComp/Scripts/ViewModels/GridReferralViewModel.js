﻿function GridViewModelReferral(id, modelName, schemaFields, columns, schemaFieldsTask, columnsTask, schemaFieldsNote,
                                columnsNote, userId, documentTypeId, gridInternalName, typeWithUser, widthPopup, heightPopup,
                                ischirent, isReferralTemplate, gridInPopup, urlToReadData) {
    var self = this;
    self.ID = ko.observable(id);
    self.getGridViewModelReferral = null;
    self.detailTabStrip = null;
    var flagFiler = false;
    var filterPanelData = null;
    // All validation messages to the postbox for the _ValidationSummary to display.
    self.ModelState = ko.observable();
    self.ModelState.subscribe(function (newValue) {
        Postbox.messages.notifySubscribers(newValue, Postbox.VALIDATION_MODEL_STATE);
    });
    var queryParent = "/" + modelName + "/GetDataParentForGrid";
    if (gridInPopup) {
        queryParent = urlToReadData;
    }
    var queryChildren = "/" + modelName + "/" + "GetDataChildForGrid";
    var queryUpdate = "/" + modelName + "/" + "Detail";
    var gridID = "#" + self.ID();
    self.AssignToReferral = function () {
        assignToReferralOrTask(1);
    };
    self.AssignToTask = function () {
        assignToReferralOrTask(2);
    };
    function assignToReferralOrTask(referralType) {
        var selectedRowIds = $(gridID).attr('data-kendo-grid-selected');
        var isSelectAll = $(gridID).attr('data-kendo-grid-selected-all');
        var title = "Assign To";
        if (typeWithUser == undefined) {
            typeWithUser = "1";
        }
        if (isSelectAll != "1" && selectedRowIds.length == 0) {
            alert("Select item is required. Please select a item.");
            return;
        }
        var url = "/Referral/ShowPopupAssignToReferral?typeWithUser=" + typeWithUser;
        if (referralType == 2) {
            url = "/ReferralTask/ShowPopupAssignToTask?typeWithUser=" + typeWithUser;
        }
        if (isSelectAll == undefined) {
            ShowPopupKendo(500, 250, title, url + '&selectedRowIdArray=' + selectedRowIds);
        } else {
            ShowPopupKendo(600, 250, title, url + '&selectedRowIdArray=' + selectedRowIds + '&isSelectAll=' + isSelectAll);
        }
    }
    function printReferralOrTask(referralType) {
        var selectedRowIds = $(gridID).attr('data-kendo-grid-selected');
        var isSelectAll = $(gridID).attr('data-kendo-grid-selected-all');
        var title = "Print Referral";
        if (isSelectAll == "1") {
            alert("Currently system don't support for print all.");
            return;
        }
        if (selectedRowIds.length == 0) {
            alert("Select item is required. Please select a item.");
            return;
        }
        var url = "/Referral/ShowPopupPrintReferral";
        if (referralType == 2) {
            title = "Print Task";
            url = "/ReferralTask/ShowPopupPrintTask";
        }
        var widthPopupTemp = $(window).width() - 50;
        var heightPopupTemp = $(window).height() - 100;
        ShowPopupKendo(widthPopupTemp, heightPopupTemp, title, url + '?selectedRowIdArray=' + selectedRowIds);
    }

    var popupNoIframe = null;
    self.EmailReferral = function () {
        var selectedRowIds = $(gridID).attr('data-kendo-grid-selected');
        var isSelectAll = $(gridID).attr('data-kendo-grid-selected-all');
        var title = "Send Email";
        if (isSelectAll == "1") {
            alert("Currently system don't support send email for all.");
            return;
        }
        if (selectedRowIds.length == 0) {
            alert("Select item is required. Please select a item.");
            return;
        }
        var url = "/Referral/ShowPopupSendEmail";
        
        var widthPopupTemp = 1050;
        var heightPopupTemp = 550;
        popupNoIframe = ShowPopupKendoNoIframe(title, widthPopupTemp, heightPopupTemp, true, ["Close"], false, url + '?selectedRowIdArray=' + selectedRowIds);
        popupNoIframe.bind("deactivate", function () {
            clearCheckBox();
        });
    };

    

    function clearCheckBox() {
        $(gridID).attr('data-kendo-grid-selected', '');
        var visibleRows = $(gridID).find('tr.kendo-data-row');
        $(visibleRows).each(function () {
            var row = $(this);
            row.find('.check_row').attr('checked', false);
        });
    }

    self.PrintReferral = function () {
        printReferralOrTask(1);
    };
    self.EmailTask = function () {
        var selectedRowIds = $(gridID).attr('data-kendo-grid-selected');
        var isSelectAll = $(gridID).attr('data-kendo-grid-selected-all');
        var title = "Send Email";
        if (isSelectAll == "1") {
            alert("Currently system don't support send email for all.");
            return;
        }
        if (selectedRowIds.length == 0) {
            alert("Select item is required. Please select a item.");
            return;
        }
        var url = "/ReferralTask/ShowPopupSendEmail";

        var widthPopupTemp = 1050;
        var heightPopupTemp = 550;
        popupNoIframe = ShowPopupKendoNoIframe(title, widthPopupTemp, heightPopupTemp, true, ["Close"], false, url + '?selectedRowIdArray=' + selectedRowIds);
        popupNoIframe.bind("deactivate", function () {
            clearCheckBox();
        });
    };
    self.PrintTask = function () {
        printReferralOrTask(2);
    };
    // referesh grid
    Postbox.messages.subscribe(function () {
        // Refresh the grid
        dataSourceData.read();
        // clear all checkbox
        $(gridID).attr('data-kendo-grid-selected', []);
        $(gridID).attr('data-kendo-grid-selected-all', 0);
        // Hide error mess
        Postbox.messages.notifySubscribers(null, Postbox.VALIDATION_MODEL_STATE);
        self.ShowHideButton([]);
    }, self, Postbox.REFRESH_READONLY_GRID);
    self.EnableButtonMenu = ko.observable(false);

    self.ShowHideButton = function (selectedVals) {
        if (selectedVals.length > 0) {
            self.EnableButtonMenu(true);
        } else {
            self.EnableButtonMenu(false);
        }
    };



    self.CheckRowInGrid = function (cb, idReferral) {
        $('.check_all_row').attr('checked', false);
        var chkAll = $('#chkAll-'+self.ID());
        //Get Current Selected Values
        var selectedVals = [];
        var selectedRowIds = $(gridID).attr('data-kendo-grid-selected');
        if (selectedRowIds != null && selectedRowIds.trim() != "") {
            selectedVals = selectedRowIds.split(',');
        }
        var visibleRows = $(gridID).find('.kendo-data-row');
        var row;
        $(visibleRows).each(function () {
            row = $(this);
            var rowId = row.attr('data-kendo-grid-rowid');
            if (rowId == idReferral) {
                return false;
            }
        });
        if (cb.checked) {
            selectedVals.push(idReferral);
        } else {
            selectedVals = jQuery.grep(selectedVals, function (value) {
                return value != idReferral;
            });
            chkAll.prop("checked", "");
            $(gridID).attr('data-kendo-grid-selected-all', 0);
        }
        //Set selected values to a custom data attribute on the grid
        $(gridID).attr('data-kendo-grid-selected', selectedVals);
        self.ShowHideButton(selectedVals);
    };

    self.CheckAllRowInGrid = function (cb) {
        var selectedVals = [];
        var visibleRows = $(gridID).find('tr.kendo-data-row');
        if (cb.checked) {
            $(gridID).attr('data-kendo-grid-selected-all', 1);
            $(visibleRows).each(function () {
                var row = $(this);
                var rowId = row.attr('data-kendo-grid-rowid');
                selectedVals.push(rowId);
                row.find('.check_row').prop('checked', true);
            });
        } else {
            $(gridID).attr('data-kendo-grid-selected-all', 0);
            selectedVals = [];
            $(visibleRows).each(function () {
                var row = $(this);
                row.find('.check_row').attr('checked', false);
            });
        }
        $(gridID).attr('data-kendo-grid-selected', selectedVals);
        self.ShowHideButton(selectedVals);
    };

    function bindCheckBox() {
        $("#chkAll-" + $(gridID).attr("id")).removeAttr('checked');
        $(".k-grid-content tbody tr").each(function () {
            var $tr = $(this);
            var uid = $tr.attr("data-uid");
            var dataEntry;
            $.each(dataSourceData._data, function (index, item) {
                if (item.uid === uid) {
                    dataEntry = item;
                }
            });
            if (dataEntry != undefined && dataEntry.Id != undefined) {
                $tr.addClass('kendo-data-row').attr('data-kendo-grid-rowid', dataEntry.Id);
            }

        });
        var chkAll = $(gridID).attr('data-kendo-grid-selected-all');

        var visibleRows = $(gridID).find('.kendo-data-row');
        if (chkAll == 1) {
            $(visibleRows).each(function () {
                var row = $(this);
                row.find('.check_row').attr('checked', 'checked');
            });
        } else {
            //Mark any selected rows as selected (persists selections from page to page)
            var selectedRowIds = $(gridID).attr('data-kendo-grid-selected');
            if (selectedRowIds != null) {
                var selectedRowIdArray = selectedRowIds.split(',');
                $(visibleRows).each(function () {
                    var row = $(this);
                    var rowId = row.attr('data-kendo-grid-rowid');
                    if ($.inArray(rowId, selectedRowIdArray) > -1) {
                        //row.addClass('k-state-selected');
                        row.find('.check_row').attr('checked', 'checked');
                    }
                });
            }
        }
    }

    var widthOfCommandColumn = 90;
    var widthOfCheckboxColumn = 30;

    if (widthPopup == undefined || widthPopup == null) {
        widthPopup = 600;
    }
    if (heightPopup == undefined || heightPopup == null) {
        heightPopup = 800;
    }

    
    self.ModelName = ko.observable(modelName);
    self.DataSource = ko.observable();
    var gridConfigViewModel = new GridConfigViewModel(0, userId, documentTypeId, gridInternalName);

    var dataSourceData = new kendo.data.DataSource({
        transport: {
            read: {
                url: queryParent,
                dataType: 'json',
                contentType: 'application/json; charset=utf-8'
            },
            parameterMap: function (options, operation) {
                if (operation !== "read" && options.models) {
                    return { models: kendo.stringify(options.models) };
                } else if (operation == "read") {

                    // Limitation of nested json object; temporarily have to modify the json to
                    // pass in sort information correctly.
                    var result = {
                        pageSize: options.pageSize,
                        skip: options.skip,
                        take: options.take,
                        createdby: userId,
                        TypeWithUserString: typeWithUser
                    };

                    if (options.sort) {
                        for (var i = 0; i < options.sort.length; i++) {
                            result["sort[" + i + "].field"] = options.sort[i].field;
                            result["sort[" + i + "].dir"] = options.sort[i].dir;
                        }
                    }
                    if (filterPanelData != null) {
                        if (!_.isUndefined(filterPanelData) && !_.isUndefined(filterPanelData.FieldFilters)) {
                            if (filterPanelData.FieldFilters.length == 2) {
                                result.DueDateFrom = filterPanelData.FieldFilters[0].Value;
                                result.DueDateTo = filterPanelData.FieldFilters[1].Value;
                                result.FilterType = filterPanelData.FilterType;
                            } else {
                                result.DueDateFrom = null;
                                result.DueDateTo = null;
                            }
                        }
                    }
                    if (options.filter) {
                        var filterData = handleFilterDataSource(options);
                        console.log(filterData);
                        var listFilterResult = Base64.encode(JSON.stringify(filterData));
                        result.FiltersStringEncode = listFilterResult;
                    }
                    return result;
                }
                return { models: kendo.stringify(options.models) };
            }
        },
        serverPaging: true,
        serverSorting: true,
        serverFiltering: true,
        pageSize: 50,
        batch: true,
        change: function () { },
        table: "#" + self.ID(),
        schema: {
            model: {
                id: "Id",
                fields: schemaFields
            },
            data: "Data",
            total: "TotalRowCount"
        }
    });

    function handleFilterDataSource(options) {
        var listFilterResult = [];
        var listFieldFilter = [];
        var listFilterParent = options.filter;
        for (var j = 0; j < listFilterParent.filters.length; j++) {
            var filterItem = listFilterParent.filters[j];
            if (filterItem) {
                var objFieldType = schemaFields[filterItem.field];
                var filterItemResult;
                if (!_.isUndefined(objFieldType)) {
                    if (!_.isUndefined(listFilterParent.filters[j + 1]) && listFilterParent.filters[j + 1].field == filterItem.field) {
                        listFieldFilter.push({ Value: filterItem.value, OperatorString: filterItem.operator });
                        continue;
                    } else {
                        listFieldFilter.push({ Value: filterItem.value, OperatorString: filterItem.operator });
                    }
                    filterItemResult = {
                        Field: filterItem.field,
                        FieldTypeString: _.isUndefined(objFieldType.type) ? "string" : _.isUndefined(objFieldType.isTime) ? objFieldType.type : "datetime",
                        LogicString: !_.isUndefined(listFilterParent.logic) && (listFieldFilter.length > 1) ? listFilterParent.logic : "",
                        FieldFilters: listFieldFilter
                    };
                    listFilterResult.push(filterItemResult);
                    listFieldFilter = [];
                } else {
                    var objFieldTypefilters = filterItem.filters;
                    if (!_.isUndefined(objFieldTypefilters)) {
                        var subfilterItem = {};
                        for (var k = 0; k < objFieldTypefilters.length; k++) {
                            subfilterItem = objFieldTypefilters[k];
                            listFieldFilter.push({ Value: subfilterItem.value, OperatorString: subfilterItem.operator });
                            objFieldType = schemaFields[objFieldTypefilters[k].field];
                        }
                        filterItemResult = {
                            Field: subfilterItem.field,
                            FieldTypeString: _.isUndefined(objFieldType.type) ? "string" : objFieldType.type,
                            LogicString: !_.isUndefined(filterItem.logic) && (objFieldTypefilters.length > 1) ? filterItem.logic : "",
                            FieldFilters: listFieldFilter
                        };
                        listFilterResult.push(filterItemResult);
                        listFieldFilter = [];
                    }
                }
            }
        }
        return listFilterResult;
    } 

    self.init = function () {
        self.getGridViewModelReferral = $("#" + self.ID()).kendoGrid({
            dataSource: dataSourceData,
            height: 710,
            sortable: true,
            filterable: true,
            columnMenu: true,
            resizable: true,
            reorderable: true,
            scrollable: { virtual: false },
            pageable: {
                refresh: true,
                pageSizes: true,
                buttonCount: 5
            },
            columnResize: changeColumnConfig,
            columnReorder: changeColumnPosition,
            columns: columns,
            editable: false,
            selectable: true,
            //change : onChange,
            columnHide: changeColumnConfig,
            columnShow: changeColumnConfig,
            detailInit: ischirent ? handelDetailInit : null,
            detailTemplate: ischirent ? kendo.template($("#templateDetail").html()) : null,
            dataBound: function (e) {
                resizeGrid();
                bindCheckBox();
                handleTooltip();
                handelDetailTemplate();
                if (isReferralTemplate) {
                    handleResizeDueration();
                } else {
                    handleResizeDuerationTask();
                }

            },
            columnMenuInit: function (e) {
                e.container.find('li.k-item.k-sort-asc[role="menuitem"]').remove();
                e.container.find('li.k-item.k-sort-desc[role="menuitem"]').remove();
                e.container.find('.k-columns-item .k-group').css({ 'width': '200px', 'max-height': '400px' });
                e.container.find('form.k-filter-menu, span.k-dropdown, span.k-numerictextbox, input.k-textbox, span.k-datepicker,span.k-datetimepicker')
                    .css({ 'width': '200px', 'max-height': '400px' });


            },
            detailExpand: function (e) {
                $(e.masterRow).attr('expandRow', 'true');
                //self.handleClickTaskAndNode('expand-' + $(e.masterRow).attr('data-kendo-grid-rowid'));
                setDefaultExpandRowInGrid(e);
                
            },
            detailCollapse: function (e) {
                //self.handleClickTaskAndNode("none");
                $(e.masterRow).attr('expandRow', 'false');
            },
        }).data("kendoGrid");

        //remove header column menu
        self.getGridViewModelReferral.thead.find("[data-field=IsChecked]>.k-header-column-menu").remove();
        self.getGridViewModelReferral.thead.find("[data-field=StatusId]>.k-header-column-menu").remove();
        self.getGridViewModelReferral.thead.find("[data-field=Status]>.k-header-column-menu").replaceWith("<a href='#' class='k-header-column-menu-temp' tabindex='-1' style='float:right'><span class='k-icon k-i-arrowhead-s'></span></a>");
        self.getGridViewModelReferral.thead.find("[data-field=Note]>.k-header-column-menu").remove();
        self.getGridViewModelReferral.thead.find("[data-field=Task]>.k-header-column-menu").remove();

        self.getGridViewModelReferral.thead.find('li.k-item.k-sort-asc[role="menuitem"]').remove();
        self.getGridViewModelReferral.thead.find('li.k-item.k-sort-desc[role="menuitem"]').remove();
        self.getGridViewModelReferral.thead.find('.k-columns-item .k-group').css({ 'width': '200px', 'max-height': '400px' });

        //Custom status menu
        var dsStatus = ["Open", "Reopen", "Pending", "Cancelled", "Completed"];
        self.getGridViewModelReferral.thead.find("[data-field=Status]>.k-header-column-menu-temp").click(function (event) {

            var offset = $(this).offset();
            event.stopPropagation();
            
            if ($(".menuStatus").length > 0 && $(".menuStatus").css("display")!="none") {
                $(".menuStatus").hide();
            } else {
                if ($(".menuStatus").length <= 0 ) {
                    var menuStatus = "<div class='menuStatus' style='left:" + offset.left + "px;top: " + (offset.top + 26) + "px;'><ul>";
                    for (var i = 0; i < dsStatus.length; i++) {
                        menuStatus += "<li><label><input type='checkbox' checked='checked' " +
                            " class='check' data-field='" + dsStatus[i] +
                            "'/>" + dsStatus[i] + "</label></li>";
                    }
                    menuStatus += "</ul></div>";
                    $("body").append(menuStatus);
                    $(".menuStatus").find("input:checkbox").change(function () {
                        var f = [];
                        $(".menuStatus").find("input:checkbox").each(function () {
                            if ($(this).is(":checked")) {
                                f.push({
                                    operator: "contains",
                                    value: $(this).data("field"),
                                    field: "Status"
                                });
                            }
                        }).promise().done(function () {
                            self.getGridViewModelReferral.dataSource.filter({
                                logic: "or",
                                filters: f
                            }
                           );
                        });
                    });
                } else {
                    $(".menuStatus").css({left:offset.left + "px",top: (offset.top + 26) + "px"});
                    $(".menuStatus").show();
                }
            }
        });

        $(document).mouseup(function (e) {
            var container = $(".menuStatus");
            var container1 = self.getGridViewModelReferral.thead.find("[data-field=Status]>.k-header-column-menu-temp");
            if (!container.is(e.target) // if the target of the click isn't the container...
                && container.has(e.target).length === 0// ... nor a descendant of the container
            && !container1.is(e.target)
            && container1.has(e.target).length === 0) {
                container.hide();
            }
        });
    };
    //Handle click view detail
    $('#' + self.ID()).on('click', 'td', function (e) {
        var objCurent = $(this);
        var objParent = $(this).parent().parent().parent().parent().parent();
        if (objCurent.hasClass('k-detail-cell') || objParent.hasClass('gridtask') || objParent.hasClass('gridnote')) {
            
            return;
        }
        var dataItem, idRow, url;
        if ($(this).has('a').length < 1 && $(this).has('input:checkbox').length < 1) {
            dataItem = self.getGridViewModelReferral.dataItem(self.getGridViewModelReferral.select());
            if (dataItem === undefined || dataItem == null) {
                return;
            }
            idRow = dataItem.Id;
            url = queryUpdate + "/" + idRow;
            window.location.href = url;
        }
        if (objCurent.children('a').hasClass('color-link-referral')) {
            dataItem = self.getGridViewModelReferral.dataItem(self.getGridViewModelReferral.select());
            if (dataItem === undefined || dataItem == null) {
                return;
            }
            idRow = dataItem.ReferralId;
            url = "/Referral/Detail/" + idRow;
            window.location.href = url;
        }
    });

    //Detail Grid
    function handelDetailInit(e) {
       CreateLoading();
       var detailRow = e.detailRow;
       $(detailRow.find(".tabstrip")).css('width', $('#' + self.ID()).width() - 70 + 'px');
       $(detailRow.find(".tabstrip")).css('height', '320px');
       self.detailTabStrip = detailRow.find(".tabstrip").kendoTabStrip({
        }).data("kendoTabStrip");
        var models = {
            ParentId: e.data.Id,
        };
       
        var request = $.ajax({ cache: false, async: false, url: queryChildren, type: "GET", data: models, dataType: "json" });
        request.done(function (result) {
            //set expand row
            if (_.isUndefined($(e.detailRow).attr('expandRow'))) {
                if (_.isUndefined(result.Error) && _.isEmpty(result.Error)) {
                    detailRow.find(".gridtask").kendoGrid({
                        dataSource: {
                            data: result.Data.TaskTab,
                            schema: {
                                model: {
                                    fields: {
                                        TaskTab: schemaFieldsTask,
                                    }
                                }
                            }
                        },
                        scrollable: true,
                        sortable: false,
                        columns: columnsTask,
                        dataBinding: function (e) {
                            $(e.sender.content).css('height', '200px');
                        },
                        dataBound: function (e) {
                            handleTooltip();
                            handleResizeDuerationTask();
                        }
                        
                    }).data("kendoGrid");

                    detailRow.find(".gridnote").kendoGrid({
                        dataSource: {
                            data: result.Data.NoteTab,
                            schema: {
                                model: {
                                    fields: {
                                        NoteTab: schemaFieldsNote,
                                    }
                                }
                            }
                        },
                        scrollable: true,
                        sortable: false,
                        columns: columnsNote,
                        dataBinding: function (e) {
                            $(e.sender.content).css('height', '200px');
                        },
                        dataBound: function (e) {
                            handleTooltip();
                        }
                    }).data("kendoGrid");

                    //$(detailRow).attr('expandRow', 'true');
                    DeleteLoading();
                }
            }
        });
    }

    //handle click Note, Task
    self.IdDetail = ko.observable(0);
    self.handleClickTaskAndNode = ko.observable('none');
    self.handleClickTaskAndNode.subscribe(function (val) {
        resizeGridWidthCaseDetail(true);
        var listVal = val.split('-');
        if (listVal.length == 2) {
            var dataItem = self.getGridViewModelReferral.dataSource.get(listVal[1]);
            var nodeDataItem = self.getGridViewModelReferral.tbody.find("tr[data-uid='" + dataItem.uid + "']");
            switch (listVal[0]) {
                case 'note':
                    $(nodeDataItem).attr('expandRow', 'true');
                    self.getGridViewModelReferral.expandRow(nodeDataItem);
                    self.detailTabStrip.select("li.tabnote");
                break;
                case 'task':
                    $(nodeDataItem).attr('expandRow', 'true');
                    self.getGridViewModelReferral.expandRow(nodeDataItem);
                    self.detailTabStrip.select("li.tabtask");
                    break;
                case 'collapse':
                    self.getGridViewModelReferral.collapseRow(nodeDataItem);
                    break;
            }
        }
    });

    //handle set defaul attribute expandRow = false

    function setDefaultExpandRowInGrid(e) {
        var idSelectedRow = parseInt($(e.masterRow).attr('data-kendo-grid-rowid'));
        if (self.IdDetail() > 0 && self.IdDetail() != idSelectedRow) {
            var dataItemCollapse = self.getGridViewModelReferral.dataSource.get(self.IdDetail());
            if (!_.isUndefined(dataItemCollapse)) {
            var nodeDataItemCollapse = self.getGridViewModelReferral.tbody.find("tr[data-uid='" + dataItemCollapse.uid + "']");
                if ($(nodeDataItemCollapse).attr('expandRow') == 'true') {
            $(nodeDataItemCollapse).attr('expandRow', 'false');
            self.getGridViewModelReferral.collapseRow(nodeDataItemCollapse);
                    resizeGridWidthCaseDetail(false);
        }
                
            }
        }
        self.IdDetail(idSelectedRow);
    }

    //handel click Add New
    self.handleClickAddNew = ko.observable('none');
    self.handleClickAddNew.subscribe(function (val) {
        var listVal = val.split('-');
        var dataItem, objReferral;
        if (listVal.length == 2) {
            dataItem = self.getGridViewModelReferral.dataSource.get(listVal[1]);
            objReferral = { Id: dataItem.Id, Value: dataItem.Id + '-' + dataItem.ProductType };
            switch (listVal[0]) {
                case 'note':
                    ShowPopupWhenClickOnLookup(800, 600, '', objReferral, "/ReferralNote/Create?type=1");
                    break;
                case 'task':
                    ShowPopupWhenClickOnLookup(1250, 800, '', objReferral, "/ReferralTask/Create?type=1");
                    break;
            }
        }
    });

    //handle click view detail
    function onChange(e) {
        e.preventDefault();
        console.log("click");
        
        var dataItem = self.getGridViewModelReferral.dataItem(self.getGridViewModelReferral.select());
        if (dataItem === undefined || dataItem == null) {
            return;
        }
        var idRow = dataItem.Id;
        var url = queryUpdate + "/" + idRow;
        //window.location.href = url;
        //ShowPopup(widthPopup, heightPopup, "Update item", url, null);
    }

    
    Postbox.messages.subscribe(function (value) {
        CreateLoading();
        var listVal = self.handleClickAddNew().split('-');
        if (listVal.length == 2) {
            var objBelow = $(self.getGridViewModelReferral.tbody.find("tr.k-state-selected[expandrow=true]"));
            var objDetail = objBelow.next('.k-detail-row');
            var request = $.ajax({ cache: false, async: false, url: queryChildren, type: "GET", data: { ParentId: listVal[1] }, dataType: "json" });
            var result;
            request.done(function (data) { result = data; });
            var kendoGrid;
            switch (listVal[0]) {
                case 'note':
                    kendoGrid = objDetail.find('div.gridnote').data("kendoGrid");
                    var dataSourceNote = new kendo.data.DataSource({
                        data: result.Data.NoteTab,
                        schema: {
                            model: {
                                fields: {
                                    NoteTab: schemaFieldsNote,
                                }
                            }
                        }
                    });
                    kendoGrid.setDataSource(dataSourceNote);
                    break;
                   
                case 'task':
                    kendoGrid = objDetail.find('div.gridtask').data("kendoGrid");
                    
                    var dataSourceTask = new kendo.data.DataSource({
                        data: result.Data.TaskTab,
                        schema: {
                            model: {
                                fields: {
                                    TaskTab: schemaFieldsTask,
                                }
                            }
                        }
                    });
                    kendoGrid.setDataSource(dataSourceTask);
                    break;
            }
            self.handleClickAddNew('none');
        }
        DeleteLoading();
    }, self, Postbox.RETURN_VALUE_FOR_GRID);
    
    Postbox.messages.subscribe(function (value) {
        filterPanelData = value;
        //self.getGridViewModelReferral.setDataSource(dataSourceData);
        dataSourceData.read();
    }, self, Postbox.RETURN_VALUE_FOR_FILTER);

    // Set data for filter  => don't use RETURN_VALUE_FOR_FILTER because I want to force 1 time to read data from server when load page
    self.SetFilterPanelDataValue = function (value) {
        filterPanelData = value;
    };
    // Get grid config
    $.ajax({
        type: "GET",
        url: "/GridConfig/Get",
        data: { UserId: userId, DocumentTypeId: documentTypeId, GridInternalName: gridInternalName },
        contentType: 'application/json; charset=utf-8',
        dataType: "json",
        async: false,
        success: function (d) {
            if (_.isNull(d.ViewColumns) || _.isUndefined(d.ViewColumns)) {
                columns.forEach(function (column) {
                    gridConfigViewModel.addColumn(column);
                });
                return;
            }
            gridConfigViewModel.Id(d.Id);
            gridConfigViewModel.importColumnConfigs(d.ViewColumns);

            //Processing Grid Columns Config
            var totalWidth = 0;
            columns.forEach(function (column) {

                if (_.isString(column.width))
                    column.width = parseInt(column.width.replace("px", ""));

                var columnConfig = gridConfigViewModel.findViewColumn(column);
                //Get column config
                if (!_.isUndefined(columnConfig) && !_.isNull(columnConfig)) {
                    column.hidden = columnConfig.HideColumn();

                    if (columnConfig.ColumnWidth() > 0) {
                        if (!_.isUndefined(column.field) && column.field.trim() == "IsChecked") {
                            column.width = widthOfCheckboxColumn;
                        }
                        else if (!_.isUndefined(column.command)) {
                            column.width = widthOfCommandColumn;
                        } else {
                            column.width = columnConfig.ColumnWidth();
                        }
                    }
                }
                else {

                    column.hidden = false;
                    gridConfigViewModel.addColumn(column);
                }

                if (_.isUndefined(column.hidden) || !column.hidden) {
                    totalWidth += column.width;
                }
            });

            //sort column order
            columns = _.sortBy(columns, function (column) { return gridConfigViewModel.findViewColumnIndex(column); });

            //work around extend the last column to fit with grid width if total columns width less than grid width
            //prevent auto ajust column
            var gridWidth = $("#" + self.ID()).width() - 22; //remove width of vertical scrollbar
            if (totalWidth < gridWidth) {
                var visibleColumns = _.where(columns, { hidden: false });
                var lastColumn = _.last(visibleColumns);
                if (!_.isUndefined(lastColumn.command)) {
                    lastColumn.width = widthOfCommandColumn;
                    // Caculate width the pre column
                    var prePosition = _.size(visibleColumns) - 2;
                    var preColumn = visibleColumns[prePosition];
                    if (prePosition != 0) {
                        preColumn.width += (gridWidth - totalWidth);
                    }
                } else {
                    lastColumn.width += (gridWidth - totalWidth);
                }
            }
        }
    });


    $(window).resize(function () {
        resizeGrid();
    });

    function saveGridConfig() {
        var gridConfig = ko.toJSON(gridConfigViewModel);
        //Save grid's config
        $.ajax({
            type: "POST",
            url: "/GridConfig/Save",
            contentType: 'application/json; charset=utf-8',
            data: gridConfig,
            dataType: "json",
            success: function (result) {
                if (result.Error === undefined || result.Error === '') {
                    gridConfigViewModel.Id(result.Data.Id);
                } else {
                    // Show error
                    self.ModelState(new FeedbackViewModel(result.Status, result.Error, result.StackTrace, result.ModelStateErrors));
                }
            }
        });
    }

    function changeColumnConfig(e) {
        gridConfigViewModel.changeColumnConfig(e.column);
        saveGridConfig();
        if (ischirent) {
            handleResizeDueration();
        } else {
            handleResizeDuerationTask();
        }
        
    }

    function changeColumnPosition(e) {
        var direction = 1;
        var start = e.newIndex;
        var end = e.oldIndex;
        if (e.oldIndex < e.newIndex) {
            direction = -1;
            start = e.oldIndex;
            end = e.newIndex;
        }
        //re-order the column in range of effect
        for (var j = start; j <= end; j++) {
            var column = setColumnIndex(self.getGridViewModelReferral.columns[j], j + 1 * direction);
            gridConfigViewModel.changeColumnOrder(column);
        }
        //update order of the changed column
        var changedColumn = setColumnIndex(e.column, e.newIndex);
        gridConfigViewModel.changeColumnOrder(changedColumn);
        saveGridConfig();
    }

    function setColumnIndex(column, index) {
        column.index = index;
        return column;
    }

    function handleResizeDuerationTask() {
        var widthElement = $('.task').width();
        var constant = 0, percenConstant, percentTimeline;
        $('.task .timeline').filter(function () {
            if ($(this).hasClass('over')) {
                var widthTimeLine = $(this).width();
                var marginLeftIconPercent = parseFloat($(this).children('.due').css('margin-left').replace('px'));
                var marginLeftIconResult = (marginLeftIconPercent / 100) * widthTimeLine;
                $(this).children('.due').css('margin-left', marginLeftIconResult + 'px');
                constant = !ischirent ? 20 : 5;
                percenConstant = (constant / (ischirent ? 100 : widthElement)) * 100;
                percentTimeline = 100 - percenConstant;
                $(this).css('width', percentTimeline + '%');
            } else {

                constant = !ischirent ? 29 : 7;
                percenConstant = (constant / (ischirent ? 100 : widthElement)) * 100;
                
                percentTimeline = 100 - percenConstant;
                $(this).css('width', percentTimeline + '%');
            }
        });
    }

    function handleResizeDueration() {
        var widthElement = $('.duration').width();
        var constant = 0, percenConstant, percentTimeline;
        $('.timeline').filter(function () {
            if ($(this).hasClass('over')) {
                var widthTimeLine = $(this).width();
                var marginLeftIconPercent = parseFloat($(this).children('.due').attr('default-percent'));
                var marginLeftIconResult = (marginLeftIconPercent / 100) * widthTimeLine;
                $(this).children('.due').css('margin-left', marginLeftIconResult + 'px');

                constant = 21.1;
                percenConstant = (constant / widthElement) * 100;
                percentTimeline = 100 - percenConstant;
                $(this).css('width', percentTimeline + '%');
            } else {
                constant = 42;
                percenConstant = (constant / widthElement) * 100;
                percentTimeline = 100 - percenConstant;
                $(this).css('width', percentTimeline + '%');
            }
        });
        
    }

    function resizeGrid() {
        var height = $('.main').height()-37;
        $('#' + self.ID()).css('height', height - 35 + 'px');
        if (gridInPopup) {
            $('#' + self.ID() + ' .k-grid-content').css('height', height - 30 + 'px');
        } else {
            $('#' + self.ID() + ' .k-grid-content').css('height', height - 75 + 'px');
        }
    }
    resizeGrid();

    function handleTooltip() {
        $('.tooltip-node, .tooltip-task').kendoTooltip({
            position: "top",
            content: kendo.template($("#templatetooltip").html()),
            show: function (e) {
                $(e.sender.content).parent().css('background-color', '#fff');
                $(e.sender.content).parent().css('border-color', '#04D215');
            },
            hide: function (e) {
                $(e.sender.content).parent().css('background-color', '');
                $(e.sender.content).parent().css('border-color', '');
            },
        });

        $('.tooltip-status').kendoTooltip({
            position: "top", content: kendo.template($("#templatetooltip").html()),
            show: function (e) {
                if (e.sender.content.children('span').text() == 'Canceled' || e.sender.content.children('span').text() == 'Cancelled') {
                    $(e.sender.content).parent().css('background-color', '#fff');
                    $(e.sender.content).parent().css('border-color', '#FF0F00');
                   
                } else if (e.sender.content.children('span').text() == 'Open') {
                    $(e.sender.content).parent().css('background-color', '#fff');
                    $(e.sender.content).parent().css('border-color', '#9FD300');
                } else {
                    $(e.sender.content).parent().css('background-color', '#fff');
                    $(e.sender.content).parent().css('border-color', '#04D215');
                }
            },
            hide: function (e) {
                $(e.sender.content).parent().css('background-color', '');
                $(e.sender.content).parent().css('border-color', '');
            },
        });

        $('.start, .due').kendoTooltip({
            position: "top",
            content: kendo.template($("#templatetooltip").html()),
            show: function (e) {
                if ($(e.sender.element).hasClass('start')) {
                    $(e.sender.content).parent().css('background-color', '#fff');
                    $(e.sender.content).parent().css('border-color', '#04D215');
                } else {
                    $(e.sender.content).parent().css('background-color', '#fff');
                    $(e.sender.content).parent().css('border-color', '#FF0F00');
                }
            },
            hide: function (e) {
                $(e.sender.content).parent().css('background-color', '');
                $(e.sender.content).parent().css('border-color', '');
            },
        });
    }

    //hide Expand Template
    function handelDetailTemplate() {
        if (!ischirent && self.getGridViewModelReferral != null) {
            $(self.getGridViewModelReferral.element).find(".k-hierarchy-cell, .k-hierarchy-col").hide();
        }
    }

    var heightDetail = 465;
    function resizeGridWidthCaseDetail(isExpand) {
        var elementObj = $('#' + self.ID() + ' .k-grid-content .k-scrollbar');
        if (!_.isUndefined(elementObj)) {
            var elementObjResize = elementObj.children('div');
            var height = elementObjResize.height();
            if (isExpand) {
                elementObjResize.css('height', height + heightDetail);
            } else {
                elementObjResize.css('height', height - heightDetail);
            }
            
        }
        
        
    }
    
}