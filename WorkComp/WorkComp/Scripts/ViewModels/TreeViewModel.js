﻿function TreeViewModel(id,
                        modelName,                        
                        getUrl,
                        childrenSchemas,
                        dataTextFields,
                        dataValueFields,
                        clientSideDataSource, treeViewTemplateHTML, isAllowSelectMultiplItems) {
    var self = this;

    self.ID = ko.observable(id);
    self.ModelName = ko.observable(modelName);
    self.DataSource = ko.observable();
    var dataSource = null;
    if (clientSideDataSource != null) {
        
        dataSource = new kendo.data.HierarchicalDataSource({
            data: clientSideDataSource,
            schema: {
                model: {
                    children: childrenSchemas
                }
            }
        });
       
    }
    else
    {
        dataSource = new kendo.data.HierarchicalDataSource({
            transport: {
                read: {
                    url: getUrl,
                    dataType: 'json',
                    contentType: 'application/json; charset=utf-8'
                }
            },
            schema: {
                model: {
                    children: childrenSchemas,
                    expanded:'Expanded'
                }
            }
        });
    }

    self.DataSource(dataSource);

    var kendoTemplate = (treeViewTemplateHTML == null)? null:kendo.template(treeViewTemplateHTML);


    $("#" + self.ID()).kendoTreeView({        
        dataSource: dataSource,
        dataTextField: dataTextFields,
        dataValueField: dataValueFields,
        template: kendoTemplate,
        select: function (e) {
            if (isAllowSelectMultiplItems) {
                e.preventDefault(); 
            }
            notifySubscribers();
        }
    }).data("kendoTreeView");

    if (isAllowSelectMultiplItems) {
        var treeViewItemSelectedClass = 'k-state-selected';
        var treeViewItemSelector = '.k-treeview .k-in';
        var treeViewItemHoverClass = 'k-state-hover';
        $(document).on('click', "#" + self.ID() + treeViewItemSelector, function (e) {
            if (e.ctrlKey || e.metaKey) {
                $(this).toggleClass(treeViewItemSelectedClass);
            } else {
                $(treeViewItemSelector)
                    .removeClass(treeViewItemSelectedClass)
                    .removeClass(treeViewItemHoverClass);

                $(this).addClass(treeViewItemSelectedClass);
            }
        });
    }

    function notifySubscribers() {
        var treeView = $("#" + self.ID()).data("kendoTreeView");
        Postbox.messages.notifySubscribers(treeView.select(), self.ID() + "_SELECTION");
    }
}

