﻿
function ExportOptionViewModel(type, number, isShow, element, elementControlTextNumberic, elementUsedFocusControlTextNumberic) {
    var self = this;
    var elementBinding = element, 
        elementControlTextNumbericBind = elementControlTextNumberic,
        elementUsedFocus = elementUsedFocusControlTextNumberic;
    var controlTextNumberic;
    self.Type = ko.observable(type);
    self.NumberRow = ko.observable(number);
    
    self.Take = ko.computed(function() {
        switch (self.Type()) {
        case 'r1000':
            return 1000;
        case 'r2000':
            return 2000;
        case 'all':
            return null;
        case 'custom':
            return self.NumberRow();
            default:
                return '0';
        }
    });
    
    self.Type.subscribe(function(newValue) {
        if (newValue != 'custom') {
            $(elementUsedFocus).css('z-index', '3');
        }
    });

    self.IsExportExcel = ko.observable(false);

    self.Export = function() {
        self.IsExportExcel(true);
    };
     

    self.ShowContent = ko.observable(isShow);

    self.ShowExportOption = function () {
        var temp = self.ShowContent();
        self.ShowContent(!temp);
    };

    
    
    self.init = function () {

    };

    $(document).ready(function () {
        
        $(elementControlTextNumbericBind).kendoNumericTextBox({
            format: "0"
        });

        controlTextNumberic = $(elementControlTextNumbericBind).data("kendoNumericTextBox");

        $($(elementUsedFocus).parent().children('span')[0]).css('z-index', '1');
        
        $(elementUsedFocus).bind('click', function() {
            self.Type('custom');
            $(this).css('z-index', '0');
            controlTextNumberic.focus();
            $(elementControlTextNumbericBind).select();
        });
    });
    
    $(document).click(function () {
        self.ShowContent(false);
        self.Type('r1000');
    });
    
    $(elementBinding).click(function (e) {
        e.stopPropagation(); // This is the preferred method.
    });
}