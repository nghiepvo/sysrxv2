﻿function KendoNumericEditor(container, options, decimals) {
    if (_.isUndefined(decimals)) {
        decimals = 0;
    }
    var maxlength = 9 + (decimals>0?(decimals+1):decimals);
    $('<input id=grid_"' + options.field + '" +  type="number" maxlength=' + maxlength + '  data-bind="value:' + options.field + '"/>')
       .appendTo(container)
       .kendoNumericTextBox(
       {
           format: options.format,
           min: 1,
           max: 999999999,
           step: 1,
           decimals: decimals,
           spinners: false
       });
}
function KendoNumericEditor2(container, options) {
    KendoNumericEditor(container, options, 2);
}
function KendoNumericEditor4(container, options) {
    KendoNumericEditor(container, options, 4);
}

// parentFilterOptions: array of ParentFieldName and ParentFieldValue foreg: [{ParentFieldName: MediaTypeID, ParentFieldValue: 1}]
function KendoLookupEditor(id, container, options, displayOptions, urlReadData, parentFilterOptions, validationString, childFilterOptions, childFilterModelName) {
    var name = "";
    var lookupControlDefinition = "";
  
    var regex = new RegExp("-", 'g'); // replace all
    var nameControl = id.replace(regex, "_");
    var displayValue = options.field;


    if (displayOptions != null) {
        displayValue += ".Name";
    }

    //Add required validation for lookup
    var validationDefinition = (validationString != undefined) ? validationString : '';
    lookupControlDefinition = lookupControlDefinition + '<input id="' + id + '" name= "' +  nameControl + '" data-bind="value:' + displayValue + '" ' + validationDefinition + ' />';


    $(lookupControlDefinition).appendTo(container);
    var lookupControl = new LookupViewModel(id, options.field, undefined, urlReadData,true);
    if (parentFilterOptions != null) {
        for (var index = 0; index < parentFilterOptions.length; index++) {
            lookupControl.UpdateParentFilter(lookupControl.ParentItems(), parentFilterOptions[index].ParentFieldName, parentFilterOptions[index].ParentFieldValue);
        }
    }
    var notGetdataFromServer = false;
    if (childFilterOptions != null) {
        // get value child
        var childValue = eval("options.model." + childFilterOptions + ".KeyId");
        
        if (childValue != undefined && childValue != 0) {
            if (displayOptions != null) {
                notGetdataFromServer = true;
            }
            lookupControl.UpdateSelectionBaseOnChild({ name: childFilterOptions, value: childValue, modelName: childFilterModelName });
        }
    }
    lookupControl.Lookup($('#' + id), displayOptions, options.model, notGetdataFromServer);
    if (validationString != undefined)
    {
        $('<span class="k-invalid-msg" data-for="' + nameControl + '"></span>').appendTo(container);
    }
}

function KendoCheckboxEditor(container, options)
{
    $('<input type="checkbox" id="grid_"' + options.field+ '" data-bind="checked:' + options.field + '"/>').appendTo(container);
}

function IsGridInEditMode(grid)
{
    var gridEditCell = grid.tbody.find('.k-edit-cell');
    /*if (gridEditCell.length > 0) {
        return true;
    }*/
    return false;
}

function ValidateGrids(grid) {
    var valid = true;
    // See if there are any insert rows
    var rows = grid.tbody.find("tr");

    for (var i = 0; i < rows.length; i++) {
        valid = ValidateGridRecord(grid, rows[i]);
        if (valid == false) return valid;
    }

    return valid;
};

// Validate the selected row to see whether the data is ok.
function ValidateGridRecord(grid, row) {
    var valid = true;
    // Get the model
    var model = grid.dataItem(row);
    if (grid.tbody.find('.k-edit-cell').length > 0) {                
        grid.editCell(grid.tbody.find('.k-edit-cell')[0]);
        if (grid.editable && !grid.editable.end()) {
            valid = false;             
            return valid;
        }
        else {
            // Take cell out of edit mode
            grid.closeCell();
        }
        return true;
    }
    
    // Loop through the columns and validate them
    var cols = $(row).find("td");

    for (var j = 0; j < cols.length; j++) {

        var dirtyContent = "";
        
        if ($(cols[j]).find(".k-dirty").length == 1) {
            dirtyContent = $(cols[j])[0].innerText;
        }
        if ($(cols[j]).find(".k-dirty").length == 1 && $.trim(dirtyContent) == "") // if there is required field not have data
        {
            // Put cell into edit mode
            grid.editCell($(cols[j]));

            // By calling editable end we will make validation fire
            if (grid.editable && !grid.editable.end()) {
                valid = false;
                return valid;
            }
            else {
                // Take cell out of edit mode
                grid.closeCell();
            }
        }
    }
    
    return valid;
};
