﻿using System.Collections.Generic;
using System.Web;
using System.Web.Mvc;
using Framework.DomainModel.Entities;
using Framework.DomainModel.Entities.Common;
using Framework.DomainModel.Entities.Security;
using Framework.DomainModel.Interfaces;
using Framework.Exceptions;
using Framework.Service.Diagnostics;
using ServiceLayer.Authorization;

namespace WorkComp.Attributes
{
    public class AuthorizeContextAttribute : AuthorizeAttribute
    {
        private IDiagnosticService _diagnosticService;
        public OperationAction OperationAction { get; set; }
        public DocumentTypeKey DocumentTypeKey { get; set; }
        protected override bool AuthorizeCore(HttpContextBase httpContext)
        {
            
            var user = httpContext.User as IWorkcompPrincipal;
            if (user == null)
            {
                return false;

            }
            
            _diagnosticService = DependencyResolver.Current.GetService<IDiagnosticService>();

            _diagnosticService.Info("Begin AuthorizeCore");
            if (DocumentTypeKey == DocumentTypeKey.None)
            {
                return true;
            }
            return true;
            var objectAuthorization = DependencyResolver.Current.GetService<IOperationAuthorization>();
            List<UserRoleFunction> userRoleFunctions;
            var permission = objectAuthorization.VerifyAccess(DocumentTypeKey, OperationAction, out userRoleFunctions);
            httpContext.Items["UserRoleFunctions"] = userRoleFunctions;

            var exception = new UnAuthorizedAccessException("UnAuthorizedAccessText")
            {
                WorkCompUserName =  user.User != null ? user.User.UserName : null
            };

            if (!permission)
                throw exception;

            //_diagnosticService.Info("AuthorizeCore Completed.");

            return true;
        }

        protected override void HandleUnauthorizedRequest(AuthorizationContext filterContext)
        {
            if (filterContext.HttpContext.Request.IsAjaxRequest())
            {
                // Fire back an unauthorized response
                filterContext.HttpContext.Response.StatusCode = 403;
            }
            else
                base.HandleUnauthorizedRequest(filterContext);
        }
    }
}