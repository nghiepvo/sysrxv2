﻿using System;
using System.Configuration;
using System.Linq;
using System.Reflection;
using Autofac;
using Autofac.Integration.Mvc;
using Framework.BusinessRule;
using Framework.DomainModel.Entities;
using ServiceLayer;
using ServiceLayer.BusinessRules.Adjuster;
using ServiceLayer.BusinessRules.AssayCode;
using ServiceLayer.BusinessRules.AssayCodeDescription;
using ServiceLayer.BusinessRules.Attorney;
using ServiceLayer.BusinessRules.Branch;
using ServiceLayer.BusinessRules.CaseManager;
using ServiceLayer.BusinessRules.City;
using ServiceLayer.BusinessRules.Claimant;
using ServiceLayer.BusinessRules.ClaimantLanguage;
using ServiceLayer.BusinessRules.ClaimNumber;
using ServiceLayer.BusinessRules.CollectionSite;
using ServiceLayer.BusinessRules.Comment;
using ServiceLayer.BusinessRules.Common;
using ServiceLayer.BusinessRules.Configuration;
using ServiceLayer.BusinessRules.Diary;
using ServiceLayer.BusinessRules.Drug;
using ServiceLayer.BusinessRules.Employer;
using ServiceLayer.BusinessRules.Heading;
using ServiceLayer.BusinessRules.Icd;
using ServiceLayer.BusinessRules.NpiNumber;
using ServiceLayer.BusinessRules.PanelType;
using ServiceLayer.BusinessRules.Payer;
using ServiceLayer.BusinessRules.ProductType;
using ServiceLayer.BusinessRules.ReasonReferral;
using ServiceLayer.BusinessRules.Referral;
using ServiceLayer.BusinessRules.ReferralEmailTemplate;
using ServiceLayer.BusinessRules.ReferralNote;
using ServiceLayer.BusinessRules.ReferralSource;
using ServiceLayer.BusinessRules.ReferralTask;
using ServiceLayer.BusinessRules.ReferralType;
using ServiceLayer.BusinessRules.State;
using ServiceLayer.BusinessRules.TaskGroup;
using ServiceLayer.BusinessRules.TaskTemplate;
using ServiceLayer.BusinessRules.User;
using ServiceLayer.BusinessRules.UserRole;
using ServiceLayer.BusinessRules.Zip;
using ServiceLayer.Interfaces;
using Solr.DomainModel;
using Solr.ServiceLayer;
using Solr.ServiceLayer.Interfaces;
using SolrNet;
using WorkComp.Models;
using Configuration = Framework.DomainModel.Entities.Configuration;
using Module = Autofac.Module;

namespace WorkComp.Services.Container
{
    public class WebModule : Module
    {
        protected override void Load(ContainerBuilder builder)
        {
            base.Load(builder);

            RegisterAspNetDependencies(builder);
            RegisterSystemServices(builder);
            RegisterServices(builder);
            RegisterSolrServices(builder);
            RegisterViewModels(builder);
            RegisterBusinessRules(builder);
        }

        private void RegisterAspNetDependencies(ContainerBuilder builder)
        {
            builder.RegisterControllers(typeof(MvcApplication).Assembly);
        }

        private void RegisterBusinessRules(ContainerBuilder builder)
        {
            builder.RegisterGeneric(typeof(DataAnnotationBusinessRule<>)).As(typeof(IBusinessRule<>));

            builder.RegisterType<BusinessRuleSet<Icd>>().AsImplementedInterfaces();
            builder.RegisterType<IcdRule<Icd>>().AsImplementedInterfaces();

            builder.RegisterType<BusinessRuleSet<State>>().AsImplementedInterfaces();
            builder.RegisterType<StateRule<State>>().AsImplementedInterfaces();

            builder.RegisterType<BusinessRuleSet<Drug>>().AsImplementedInterfaces();
            builder.RegisterType<DrugRule<Drug>>().AsImplementedInterfaces();

            builder.RegisterType<BusinessRuleSet<Employer>>().AsImplementedInterfaces();
            builder.RegisterType<EmployerRule<Employer>>().AsImplementedInterfaces();
            
            builder.RegisterType<BusinessRuleSet<NpiNumber>>().AsImplementedInterfaces();
            builder.RegisterType<NpiNumberRule<NpiNumber>>().AsImplementedInterfaces();

            builder.RegisterType<BusinessRuleSet<Attorney>>().AsImplementedInterfaces();
            builder.RegisterType<AttorneyRule<Attorney>>().AsImplementedInterfaces();

            builder.RegisterType<BusinessRuleSet<CollectionSite>>().AsImplementedInterfaces();
            builder.RegisterType<CollectionSiteRule<CollectionSite>>().AsImplementedInterfaces();

            
            builder.RegisterType<BusinessRuleSet<ReferralType>>().AsImplementedInterfaces();
            builder.RegisterType<ReferralTypeRule<ReferralType>>().AsImplementedInterfaces();

            builder.RegisterType<BusinessRuleSet<ReferralSource>>().AsImplementedInterfaces();
            builder.RegisterType<ReferralSourceRule<ReferralSource>>().AsImplementedInterfaces();

            builder.RegisterType<BusinessRuleSet<AssayCodeDescription>>().AsImplementedInterfaces();
            builder.RegisterType<AssayCodeDescriptionRule<AssayCodeDescription>>().AsImplementedInterfaces();

            builder.RegisterType<BusinessRuleSet<AssayCode>>().AsImplementedInterfaces();
            builder.RegisterType<AssayCodeRule<AssayCode>>().AsImplementedInterfaces();

            builder.RegisterType<BusinessRuleSet<Adjuster>>().AsImplementedInterfaces();
            builder.RegisterType<AdjusterRule<Adjuster>>().AsImplementedInterfaces();

            builder.RegisterType<BusinessRuleSet<Claimant>>().AsImplementedInterfaces();
            builder.RegisterType<ClaimantRule<Claimant>>().AsImplementedInterfaces();

            builder.RegisterType<BusinessRuleSet<ClaimNumber>>().AsImplementedInterfaces();
            builder.RegisterType<ClaimNumberRule<ClaimNumber>>().AsImplementedInterfaces();

            builder.RegisterType<BusinessRuleSet<City>>().AsImplementedInterfaces();
            builder.RegisterType<CityRule<City>>().AsImplementedInterfaces();

            builder.RegisterType<BusinessRuleSet<Zip>>().AsImplementedInterfaces();
            builder.RegisterType<ZipRule<Zip>>().AsImplementedInterfaces();

            builder.RegisterType<BusinessRuleSet<ClaimantLanguage>>().AsImplementedInterfaces();
            builder.RegisterType<ClaimantLanguageRule<ClaimantLanguage>>().AsImplementedInterfaces();

            builder.RegisterType<BusinessRuleSet<TaskTemplate>>().AsImplementedInterfaces();
            builder.RegisterType<TaskTemplateRule<TaskTemplate>>().AsImplementedInterfaces();

            builder.RegisterType<BusinessRuleSet<Payer>>().AsImplementedInterfaces();
            builder.RegisterType<PayerRule<Payer>>().AsImplementedInterfaces();

            builder.RegisterType<BusinessRuleSet<TaskGroup>>().AsImplementedInterfaces();
            builder.RegisterType<TaskGroupRule<TaskGroup>>().AsImplementedInterfaces();

            builder.RegisterType<BusinessRuleSet<Branch>>().AsImplementedInterfaces();
            builder.RegisterType<BranchRule<Branch>>().AsImplementedInterfaces();

            builder.RegisterType<BusinessRuleSet<UserRole>>().AsImplementedInterfaces();
            builder.RegisterType<UserRoleRule<UserRole>>().AsImplementedInterfaces();

            builder.RegisterType<BusinessRuleSet<User>>().AsImplementedInterfaces();
            builder.RegisterType<UserRule<User>>().AsImplementedInterfaces();

            builder.RegisterType<BusinessRuleSet<PanelType>>().AsImplementedInterfaces();
            builder.RegisterType<PanelTypeRule<PanelType>>().AsImplementedInterfaces();

            builder.RegisterType<BusinessRuleSet<CaseManager>>().AsImplementedInterfaces();
            builder.RegisterType<CaseManagerRule<CaseManager>>().AsImplementedInterfaces();

            builder.RegisterType<BusinessRuleSet<EmailTemplate>>().AsImplementedInterfaces();

            builder.RegisterType<BusinessRuleSet<ProductType>>().AsImplementedInterfaces();
            builder.RegisterType<ProductTypeRule<ProductType>>().AsImplementedInterfaces();

            builder.RegisterType<BusinessRuleSet<ReasonReferral>>().AsImplementedInterfaces();
            builder.RegisterType<ReasonReferralRule<ReasonReferral>>().AsImplementedInterfaces();

            builder.RegisterType<BusinessRuleSet<Referral>>().AsImplementedInterfaces();
            builder.RegisterType<ReferralRule<Referral>>().AsImplementedInterfaces();

            builder.RegisterType<BusinessRuleSet<Configuration>>().AsImplementedInterfaces();
            builder.RegisterType<ConfigurationRule<Configuration>>().AsImplementedInterfaces();

            builder.RegisterType<BusinessRuleSet<ReferralNote>>().AsImplementedInterfaces();
            builder.RegisterType<ReferralNoteRule<ReferralNote>>().AsImplementedInterfaces();

            builder.RegisterType<BusinessRuleSet<ReferralTask>>().AsImplementedInterfaces();
            builder.RegisterType<ReferralTaskRule<ReferralTask>>().AsImplementedInterfaces();

            builder.RegisterType<BusinessRuleSet<Diary>>().AsImplementedInterfaces();
            builder.RegisterType<DiaryRule<Diary>>().AsImplementedInterfaces();

            builder.RegisterType<BusinessRuleSet<Heading>>().AsImplementedInterfaces();
            builder.RegisterType<HeadingRule<Heading>>().AsImplementedInterfaces();

            builder.RegisterType<BusinessRuleSet<Comment>>().AsImplementedInterfaces();
            builder.RegisterType<CommentRule<Comment>>().AsImplementedInterfaces();

            builder.RegisterType<BusinessRuleSet<ReferralEmailTemplate>>().AsImplementedInterfaces();
            builder.RegisterType<ReferralEmailTemplateRule<ReferralEmailTemplate>>().AsImplementedInterfaces();
        }

        private void RegisterSystemServices(ContainerBuilder builder)
        {
            builder.RegisterGeneric(typeof(MasterFileService<>)).As(typeof(IMasterFileService<>));
            builder.RegisterGeneric(typeof(SolrMasterFileService<>)).As(typeof(ISolrMasterFileService<>));
        }

        private void RegisterServices(ContainerBuilder builder)
        {
            builder.RegisterType<CityService>().As<ICityService>();
            builder.RegisterType<StateService>().As<IStateService>();
            builder.RegisterType<DrugService>().As<IDrugService>();
            builder.RegisterType<EmployerService>().As<IEmployerService>();
            builder.RegisterType<NpiNumberService>().As<INpiNumberService>();
            builder.RegisterType<AttorneyService>().As<IAttorneyService>();
            builder.RegisterType<CollectionSiteService>().As<ICollectionSiteService>();
            builder.RegisterType<ReferralTypeService>().As<IReferralTypeService>();
            builder.RegisterType<ReferralSourceService>().As<IReferralSourceService>();
            builder.RegisterType<SampleTestingTypeService>().As<ISampleTestingTypeService>();
            builder.RegisterType<AssayCodeDescriptionService>().As<IAssayCodeDescriptionService>();
            builder.RegisterType<AssayCodeService>().As<IAssayCodeService>();
            builder.RegisterType<AdjusterService>().As<IAdjusterService>();
            builder.RegisterType<ClaimNumberService>().As<IClaimNumberService>();
            builder.RegisterType<ZipService>().As<IZipService>();
            builder.RegisterType<IcdService>().As<IIcdService>();
            builder.RegisterType<GridConfigService>().As<IGridConfigService>();
            builder.RegisterType<ClaimantLanguageService>().As<IClaimantLanguageService>();
            builder.RegisterType<TaskGroupService>().As<ITaskGroupService>();
            builder.RegisterType<TaskTemplateService>().As<ITaskTemplateService>();
            builder.RegisterType<PayerService>().As<IPayerService>();
            builder.RegisterType<UserService>().As<IUserService>();
            builder.RegisterType<UserRoleService>().As<IUserRoleService>();
            builder.RegisterType<BranchService>().As<IBranchService>();
            builder.RegisterType<ClaimantService>().As<IClaimantService>();
            builder.RegisterType<PanelTypeService>().As<IPanelTypeService>();
            builder.RegisterType<PanelService>().As<IPanelService>();
            builder.RegisterType<ReferralService>().As<IReferralService>();
            builder.RegisterType<CaseManagerService>().As<ICaseManagerService>();
            builder.RegisterType<ReasonReferralService>().As<IReasonReferralService>();
            builder.RegisterType<EmailTemplateService>().As<IEmailTemplateService>();
            builder.RegisterType<ProductTypeService>().As<IProductTypeService>();
            builder.RegisterType<ReferralAttachmentService>().As<IReferralAttachmentService>();
            builder.RegisterType<ConfigurationService>().As<IConfigurationService>();
            builder.RegisterType<ReferralNoteService>().As<IReferralNoteService>();
            builder.RegisterType<ReferralTaskService>().As<IReferralTaskService>();
            builder.RegisterType<DiaryService>().As<IDiaryService>();
            builder.RegisterType<HeadingService>().As<IHeadingService>();
            builder.RegisterType<CommentService>().As<ICommentService>();
            builder.RegisterType<ReportService>().As<IReportService>();
            builder.RegisterType<AlertService>().As<IAlertService>();
            builder.RegisterType<ReferralSampleTestingTypeService>().As<IReferralSampleTestingTypeService>();
            builder.RegisterType<SysRxReportService>().As<ISysRxReportService>();
            builder.RegisterType<DocumentTypeService>().As<IDocumentTypeService>();
            builder.RegisterType<UserRoleFunctionService>().As<IUserRoleFunctionService>();
            builder.RegisterType<ReferralEmailTemplateService>().As<IReferralEmailTemplateService>();
            builder.RegisterType<ReferralEmailTemplateAttachmentService>().As<IReferralEmailTemplateAttachmentService>();
        }

        private void RegisterSolrServices(ContainerBuilder builder)
        {
            var solrUrl = ConfigurationSettings.AppSettings["SolrUrl"];
            Startup.Init<SolrClaimant>(solrUrl+"Claimant");
            builder.RegisterType<SolrClaimantService>().As<ISolrClaimantService>();
            Startup.Init<SolrClaimNumber>(solrUrl + "ClaimNumber");
            builder.RegisterType<SolrClaimNumberService>().As<ISolrClaimNumberService>();
            Startup.Init<SolrReferral>(solrUrl + "Referral");
            builder.RegisterType<SolrReferralService>().As<ISolrReferralService>();
            Startup.Init<SolrNpiNumber>(solrUrl + "NpiNumber");
            builder.RegisterType<SolrNpiNumberService>().As<ISolrNpiNumberService>();
            Startup.Init<SolrReferralTask>(solrUrl + "ReferralTask");
            builder.RegisterType<SolrReferralTaskService>().As<ISolrReferralTaskService>();
            Startup.Init<SolrReferralResult>(solrUrl + "ReferralResult");
            builder.RegisterType<SolrReferralResultService>().As<ISolrReferralResultService>();
        }

        private void RegisterViewModels(ContainerBuilder builder)
        {
            RegisterInstanceOfType(builder, ThisAssembly, typeof(ViewModelBase));
        }

        private static void RegisterInstanceOfType(ContainerBuilder builder, Assembly assembly, Type typeRegistering)
        {
            var types = assembly.GetTypes();
            foreach (var type in types.Where(typeRegistering.IsAssignableFrom))
            {
                builder.RegisterType(type).AsSelf();
            }
        }
    }
}
