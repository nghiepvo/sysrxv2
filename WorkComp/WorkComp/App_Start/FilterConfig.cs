﻿using System.Web.Mvc;
using WorkComp.Attributes;

namespace WorkComp
{
    public class FilterConfig
    {
        public static void RegisterGlobalFilters(GlobalFilterCollection filters)
        {
            filters.Add(new HandleErrorAttribute());
            filters.Add(new ExceptionHandlerAttribute());
            filters.Add(new WorkCompActionFilterAttribute());
        }
    }
}