﻿using System.Web.Optimization;

namespace WorkComp
{
    public class BundleConfig
    {
        // For more information on Bundling, visit http://go.microsoft.com/fwlink/?LinkId=254725
        public static void RegisterBundles(BundleCollection bundles)
        {
            bundles.Add(new ScriptBundle("~/Scripts/jquery").Include(
              "~/Scripts/jquery-2.0.3.min.js",
              "~/Scripts/jquery.attrchange.js",
              "~/Scripts/jquery.AreYouSure.js",
              "~/Scripts/jquery.columnview.js",
              "~/Scripts/jquery.attrchange.js")
              );
            bundles.Add(new ScriptBundle("~/Scripts/ViewModeljs").IncludeDirectory(
               "~/Scripts/ViewModels", "*.js")
               );

            bundles.Add(new ScriptBundle("~/Scripts/knockout").Include(
                "~/Scripts/knockout-3.1.0.js",
                "~/Scripts/knockout-kendo.js",
                "~/Scripts/knockout-kendo.min.js")
                );

            bundles.Add(new ScriptBundle("~/Scripts/kendojs").Include(
                "~/Scripts/kendo/kendo.web.min.js",
                "~/Scripts/kendo/cultures/kendo.cultures.js")
                );
           
            bundles.Add(new ScriptBundle("~/Scripts/workcompjs").IncludeDirectory(
               "~/Scripts/WorkComp", "*.js")
               );
            // Use the development version of Modernizr to develop with and learn from. Then, when you're
            // ready for production, use the build tool at http://modernizr.com to pick only the tests you need.
            bundles.Add(new ScriptBundle("~/bundles/modernizr").Include(
                        "~/Scripts/modernizr-*"));

            //bundles.Add(new StyleBundle("~/Content/css").Include("~/Content/site.css"));

            //bundles.Add(new StyleBundle("~/Content/css/layout").Include(
            //            "~/Content/base.css",
            //            "~/Content/grid.css"));
            //bundles.Add(new StyleBundle("~/Content/css/base").Include(
            //            "~/Content/kendo/kendo.metro.min.css",
            //            "~/Content/kendo/kendo.common.min.css",
            //            "~/Content/kendo/kendo.common.min.css",
            //            "~/Content/form.base.css",
            //            "~/Content/font.css"));
        }
    }
}