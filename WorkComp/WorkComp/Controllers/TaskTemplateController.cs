﻿using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.Web.Mvc;
using Framework.DomainModel.Entities;
using Framework.DomainModel.Entities.Common;
using Framework.DomainModel.Entities.Security;
using Framework.DomainModel.ValueObject;
using Framework.Exceptions;
using Framework.Service.Diagnostics;
using Framework.Utility;
using Newtonsoft.Json;
using ServiceLayer.Interfaces;
using ServiceLayer.Interfaces.Authentication;
using WorkComp.Attributes;
using WorkComp.Models;
using WorkComp.Models.TaskTemplate;

namespace WorkComp.Controllers
{
    public class TaskTemplateController : ApplicationControllerGeneric<TaskTemplate, DashboardTaskTemplateDataViewModel>
    {
        private readonly ITaskTemplateService _taskTemplateService;
        public TaskTemplateController(IAuthenticationService authenticationService, IDiagnosticService diagnosticService, ITaskTemplateService taskTemplateService
                                        )
            : base(authenticationService, diagnosticService, taskTemplateService)
        {
            _taskTemplateService = taskTemplateService;
        }

        [AuthorizeContext(DocumentTypeKey = DocumentTypeKey.TaskTemplate, OperationAction = OperationAction.View)]
        public ActionResult Index()
        {
            var viewModel = new DashboardTaskTemplateIndexViewModel();
            Func<GridViewModel> gridViewModel = () => new GridViewModel
            {
                GridId = "TaskTemplateGrid",
                ModelName = "TaskTemplate",
                UseCheckBoxColumn = true,
                UseDeleteColumn = true,
                WidthPopup = 800,
                HeightPopup = 600,
                CheckIsReadOnlyRow=true
            };

            viewModel.GridViewModel = BuildGridViewModel(gridViewModel);
            return View(viewModel);
        }

        [HttpGet]
        [AuthorizeContext(DocumentTypeKey = DocumentTypeKey.TaskTemplate, OperationAction = OperationAction.View)]
        public override JsonResult GetDataForGrid(QueryInfo queryInfo)
        {
            return base.GetDataForGrid(queryInfo);
        }

        [AuthorizeContext(DocumentTypeKey = DocumentTypeKey.TaskTemplate, OperationAction = OperationAction.Delete)]
        public JsonResult DeleteMulti(string selectedRowIdArray, string isDeleteAll)
        {
            if (isDeleteAll == "1")
            {
                _taskTemplateService.DeleteAll(o => o.Id > 0&&o.IsSystem!=true);
            }
            else
            {
                var liststrId = selectedRowIdArray.Split(',');
                var listId = new List<int>();
                foreach (var item in liststrId)
                {
                    int id;
                    int.TryParse(item, out id);
                    if (id != 0)
                    {
                        listId.Add(id);
                    }
                }
                MasterFileService.DeleteAll(o => listId.Contains(o.Id) && o.IsSystem != true);
            }
            return Json(new { Error = string.Empty }, JsonRequestBehavior.AllowGet);
        }

        [AuthorizeContext(DocumentTypeKey = DocumentTypeKey.TaskTemplate, OperationAction = OperationAction.Delete)]
        public JsonResult Delete(string models)
        {
            var listItem = JsonConvert.DeserializeObject<Collection<dynamic>>(models);
            if (listItem.Count == 0)
            {
                return Json(false, JsonRequestBehavior.AllowGet);
            }
            var id = (int)(listItem[0].Id);
            _taskTemplateService.DeleteById(id);
            return Json(true, JsonRequestBehavior.AllowGet);
        }

        [AuthorizeContext(DocumentTypeKey = DocumentTypeKey.TaskTemplate, OperationAction = OperationAction.Add)]
        public ActionResult Create()
        {
            var viewModel = new DashboardTaskTemplateDataViewModel
            {
                SharedViewModel = new DashboardTaskTemplateShareViewModel
                {
                    CreateMode = true
                }
            };
            // Check show popup for create from dropdown
            var type = Request.QueryString["type"];
            if (type == "1")
            {
                viewModel.SharedViewModel.ShowPopupType = ShowPopupType.ComboBox;
            }
            return View(viewModel);
        }

        [AuthorizeContext(DocumentTypeKey = DocumentTypeKey.TaskTemplate, OperationAction = OperationAction.Add)]
        public ActionResult Copy(int id)
        {
            var viewModel = GetMasterFileViewModel(id);
            viewModel.SharedViewModel.CreateMode = true;
            viewModel.SharedViewModel.Id = 0;
            viewModel.Id = 0;
            return View("Create", viewModel);
        }

        [HttpPost]
        [AuthorizeContext(DocumentTypeKey = DocumentTypeKey.TaskTemplate, OperationAction = OperationAction.Add)]
        public int Create(TaskTemplateParameter parameters)
        {
            var savedEntity = CreateMasterFile(parameters);
            return savedEntity.Id;
        }

        [AuthorizeContext(DocumentTypeKey = DocumentTypeKey.TaskTemplate, OperationAction = OperationAction.Update)]
        public ActionResult Update(int id)
        {
            var viewModel = GetMasterFileViewModel(id);
            if (viewModel.IsSystem.GetValueOrDefault())
            {
                var exception = new UnAuthorizedAccessException("UnAuthorizedAccessText");
                throw exception;
            }
            // Check show popup for create from dropdown
            var type = Request.QueryString["type"];
            if (type == "1")
            {
                viewModel.SharedViewModel.ShowPopupType = ShowPopupType.ComboBox;
            }
            return View(viewModel);
        }

        [HttpPost]
        [AuthorizeContext(DocumentTypeKey = DocumentTypeKey.TaskTemplate, OperationAction = OperationAction.Update)]
        public ActionResult Update(TaskTemplateParameter parameters)
        {
            return UpdateMasterFile(parameters);
        }
        protected override IList<ViewColumnViewModel> GetViewColumns()
        {
            var objViewColumn = new List<ViewColumnViewModel>
            {
                new ViewColumnViewModel
                {
                    ColumnOrder = 1,
                    ColumnWidth = 500,
                    Name = "Title",
                    Text = "Title",
                    ColumnJustification = GridColumnJustification.Left
                },
                new ViewColumnViewModel
                {
                    ColumnOrder = 2,
                    Name = "Description",
                    Text = "Description",
                    ColumnWidth = 500,
                    ColumnJustification = GridColumnJustification.Left
                }
            };
            return objViewColumn;
        }

        [HttpGet]
        [AuthorizeContext(DocumentTypeKey = DocumentTypeKey.TaskTemplate, OperationAction = OperationAction.View)]
        public JsonResult GetAllTaskTemplateForDualListBox()
        {
            var queryData = _taskTemplateService.GetAllTaskTemplateForDualListBox();
            var clientsJson = Json(queryData, JsonRequestBehavior.AllowGet);
            return clientsJson;
        }

        [HttpGet]
        [AuthorizeContext(DocumentTypeKey = DocumentTypeKey.TaskTemplate, OperationAction = OperationAction.View)]
        public JsonResult GetTaskTemplateByTaskGroup(int taskGroupId)
        {
            var queryData = _taskTemplateService.GetTaskTemplateByTaskGroup(taskGroupId);
            var clientsJson = Json(queryData, JsonRequestBehavior.AllowGet);
            return clientsJson;
        }

        [AuthorizeContext(DocumentTypeKey = DocumentTypeKey.TaskTemplate, OperationAction = OperationAction.View)]
        public JsonResult ExportExcel(GridConfigViewModel gridConfig, QueryInfo queryInfo)
        {
            return base.ExportExcelMasterfile(gridConfig, queryInfo);
        }

        [AuthorizeContext(DocumentTypeKey = DocumentTypeKey.TaskTemplate, OperationAction = OperationAction.View)]
        public JsonResult GetLookup(LookupQuery queryInfo)
        {
            var selector = new Func<TaskTemplate, LookupItemVo>(o => new LookupItemVo
            {
                KeyId = o.Id,
                DisplayName = o.Title
            });
            return base.GetLookupForEntity(queryInfo, selector);
        }

        [AuthorizeContext(DocumentTypeKey = DocumentTypeKey.TaskTemplate, OperationAction = OperationAction.View)]
        public JsonResult GetItem(int idTaskTemplate)
        {
            var data = _taskTemplateService.GetById(idTaskTemplate);
            if (data == null) return Json("", JsonRequestBehavior.AllowGet);
            var item = new
            {
                data.Id,
                data.Title,
                data.Description,
                data.StatusId,
                AssignTo =
                   new LookupInGridDataSourceViewModel
                   {
                       KeyId = data.AssignToId.GetValueOrDefault(),
                       Name = data.AssignTo == null ? "" : data.AssignTo.FirstName+(!string.IsNullOrEmpty(data.AssignTo.MiddleName)?" "+data.AssignTo.MiddleName:"")+" "+data.AssignTo.LastName
                   },
                StartDate = DateTime.Now.ToString("MM/dd/yyyy HH:mm"),
                DueDate =
                    DateTime.Now.AddDays(data.NumDueDate.GetValueOrDefault()).AddHours(data.NumDueHour.GetValueOrDefault()).ToString("MM/dd/yyyy HH:mm"),
                TaskType =
                    new LookupInGridDataSourceViewModel
                    {
                        KeyId = data.TaskTypeId.GetValueOrDefault(),
                        Name =
                            XmlDataHelpper.Instance.GetValue(XmlDataTypeEnum.TaskType.ToString(),
                                data.TaskTypeId.GetValueOrDefault().ToString())
                    }
            };
            return Json(item, JsonRequestBehavior.AllowGet);
        }
    }
}