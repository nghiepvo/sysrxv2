﻿using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using Framework.DomainModel.Entities;
using Framework.DomainModel.Entities.Common;
using Framework.Exceptions;
using Framework.Service.Translation;
using Newtonsoft.Json;
using WorkComp.Models.Referral;
using WorkComp.Models.TaskGroup;

namespace WorkComp.Controllers
{
    public partial class ReferralController
    {
        private static void ProcessForTaskTemplateInfo(DashboardReferralDataViewModel viewModel, string taskTemplateInfo)
        {
            if (string.IsNullOrEmpty(taskTemplateInfo))
            {
                return;
            }
            var taskTemplateData = JsonConvert.DeserializeObject<Dictionary<string, object>>(taskTemplateInfo);

            var listAllJson = taskTemplateData.SingleOrDefault(g => g.Key.Equals("listAll")).Value.ToString();
            var listAll = JsonConvert.DeserializeObject<Collection<TaskTemplateGridItemViewModel>>(listAllJson);
            var objResult = new List<ReferralTask>();
            if (listAll != null && listAll.Count != 0)
            {
                CheckTaskInfoValid(listAll, viewModel.SharedViewModel.Id);
                foreach (var item in listAll)
                {
                    var objAdd = new ReferralTask
                    {
                        Title = item.Title,
                        Description = item.Description,
                        StartDate = item.StartDate.GetValueOrDefault(),
                        DueDate = item.DueDate.GetValueOrDefault(),
                        // When user add task, default task have status is 1
                        StatusId = 1,
                        Rush = viewModel.ReferralInfo.Rush
                    };
                    if (item.AssignTo == null || item.AssignTo.KeyId == 0)
                    {
                        objAdd.AssignToId = viewModel.ReferralInfo.AssignToId;
                    }
                    else
                    {
                        objAdd.AssignToId = item.AssignTo.KeyId;
                    }
                    if (item.TaskType == null || item.TaskType.KeyId == 0)
                    {
                        objAdd.TaskTypeId = null;
                    }
                    else
                    {
                        objAdd.TaskTypeId = item.TaskType.KeyId;
                    }
                    objResult.Add(objAdd);
                }
            }
            viewModel.Tasks = objResult;
        }

        private static void CheckTaskInfoValid(Collection<TaskTemplateGridItemViewModel> listTaskInfo, int referralId)
        {
            if (listTaskInfo.Count == 0)
            {
                return;
            }
            var failed = false;
            var validationResult = new List<ValidationResult>();
            if (listTaskInfo.Any(o => string.IsNullOrEmpty(o.Title)))
            {
                var mess = string.Format(SystemMessageLookup.GetMessage("RequiredTextResourceKey"), "Task Title");
                validationResult.Add(new ValidationResult(mess));
                failed = true;
            }
            if (listTaskInfo.Any(o => o.StartDate == null))
            {
                var mess = string.Format(SystemMessageLookup.GetMessage("RequiredTextResourceKey"), "Task Start Date");
                validationResult.Add(new ValidationResult(mess));
                failed = true;
            }
            if (listTaskInfo.Any(o => o.DueDate == null))
            {
                var mess = string.Format(SystemMessageLookup.GetMessage("RequiredTextResourceKey"), "Task Follow Up Date");
                validationResult.Add(new ValidationResult(mess));
                failed = true;
            }
            if (referralId == 0 && listTaskInfo.Any(o => o.DueDate.GetValueOrDefault().Date < DateTime.Now.Date))
            {
                var mess = string.Format(SystemMessageLookup.GetMessage("CannotGreaterThanText"), "Current Date", "Task Follow Up Date");
                validationResult.Add(new ValidationResult(mess));
                failed = true;
            }
            if (listTaskInfo.Any(o => o.StartDate.GetValueOrDefault() > o.DueDate.GetValueOrDefault()))
            {
                var mess = string.Format(SystemMessageLookup.GetMessage("CannotGreaterThanText"), "Task Start Date", "Task Follow Up Date");
                validationResult.Add(new ValidationResult(mess));
                failed = true;
            }
            //Check if there is >=2 Id
            var checkDupicate = listTaskInfo.GroupBy(s => new { s.Title }).Any(g => g.Count() > 1);
            if (checkDupicate)
            {
                var mess = string.Format(SystemMessageLookup.GetMessage("DuplicateItemText"), "Task Title");
                validationResult.Add(new ValidationResult(mess));
                failed = true;
            }
            var result = new BusinessRuleResult(failed, "", "ReferralTask", 0, null, "ReferralTaskRule") { ValidationResults = validationResult };
            if (failed)
            {
                // Give messages on every rule that failed
                throw new BusinessRuleException("BussinessGenericErrorMessageKey", new[] { result });
            }
        }
        
    }
}