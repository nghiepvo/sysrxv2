﻿using  System;
using System.Web.Mvc;
using Framework.DomainModel.Entities;
using Framework.DomainModel.ValueObject;
using ServiceLayer.Interfaces;
using WorkComp.Models.ClaimantLanguage;
using ServiceLayer.Interfaces.Authentication;
using Framework.Service.Diagnostics;
using WorkComp.Models;
using WorkComp.Attributes;
using Framework.DomainModel.Entities.Common;
using Framework.DomainModel.Entities.Security;
using System.Collections.Generic;
using Newtonsoft.Json;
using System.Collections.ObjectModel;

namespace WorkComp.Controllers
{
    public class ClaimantLanguageController : ApplicationControllerGeneric<ClaimantLanguage, DashboardClaimantLanguageDataViewModel>
    {
        //
        // GET: /ClaimantLanguage/
        private readonly IClaimantLanguageService _claimantLanguageService;
        public ClaimantLanguageController(IAuthenticationService authenticationService, IDiagnosticService diagnosticService, IClaimantLanguageService claimantLanguageService)
            : base(authenticationService, diagnosticService, claimantLanguageService)
        {
            _claimantLanguageService = claimantLanguageService;
        }

        [AuthorizeContext(DocumentTypeKey = DocumentTypeKey.ClaimantLanguage, OperationAction = OperationAction.View)]
        public ActionResult Index()
        {
            var viewModel = new DashboardClaimantLanguageIndexViewModel();
            Func<GridViewModel> gridViewModel = () => new GridViewModel
            {
                GridId = "ClaimantLanguageGrid",
                ModelName = "ClaimantLanguage",
                UseCheckBoxColumn = true,
                UseDeleteColumn = true,
                WidthPopup = 800,
                HeightPopup = 600
            };

            viewModel.GridViewModel = BuildGridViewModel(gridViewModel);
            return View(viewModel);
        }

        [HttpGet]
        [AuthorizeContext(DocumentTypeKey = DocumentTypeKey.ClaimantLanguage, OperationAction = OperationAction.View)]
        public override JsonResult GetDataForGrid(QueryInfo queryInfo)
        {
            return base.GetDataForGrid(queryInfo);
        }

        protected override IList<ViewColumnViewModel> GetViewColumns()
        {
            var objViewColumn = new List<ViewColumnViewModel>
            {
                new ViewColumnViewModel
                {
                    ColumnOrder = 1,
                    Name = "Name",
                    Text = "Name",
                    ColumnWidth = 1100,
                    ColumnJustification = GridColumnJustification.Left
                }
            };
            return objViewColumn;
        }

        [AuthorizeContext(DocumentTypeKey = DocumentTypeKey.ClaimantLanguage, OperationAction = OperationAction.Add)]
        public ActionResult Create()
        {
            var viewModel = new DashboardClaimantLanguageDataViewModel
            {
                SharedViewModel = new DashboardClaimantLanguageShareViewModel
                {
                    CreateMode = true
                }
            };
            // Check show popup for create from dropdown
            var type = Request.QueryString["type"];
            if (type == "1")
            {
                viewModel.SharedViewModel.ShowPopupType = ShowPopupType.ComboBox;
            }
            return View(viewModel);
        }

        [HttpPost]
        [AuthorizeContext(DocumentTypeKey = DocumentTypeKey.ClaimantLanguage, OperationAction = OperationAction.Add)]
        public int Create(ClaimantLanguageParameter parameters)
        {
            var savedEntity = CreateMasterFile(parameters);
            return savedEntity.Id;
        }

        [AuthorizeContext(DocumentTypeKey = DocumentTypeKey.ClaimantLanguage, OperationAction = OperationAction.Update)]
        public ActionResult Update(int id)
        {
            var viewModel = GetMasterFileViewModel(id);
            // Check show popup for create from dropdown
            var type = Request.QueryString["type"];
            if (type == "1")
            {
                viewModel.SharedViewModel.ShowPopupType = ShowPopupType.ComboBox;
            }
            return View(viewModel);
        }

        [HttpPost]
        [AuthorizeContext(DocumentTypeKey = DocumentTypeKey.ClaimantLanguage, OperationAction = OperationAction.Update)]
        public ActionResult Update(ClaimantLanguageParameter parameters)
        {
            return UpdateMasterFile(parameters);
        }

        [AuthorizeContext(DocumentTypeKey = DocumentTypeKey.ClaimantLanguage, OperationAction = OperationAction.Delete)]
        public JsonResult Delete(string models)
        {
            var listItem = JsonConvert.DeserializeObject<Collection<dynamic>>(models);
            if (listItem.Count == 0)
            {
                return Json(false, JsonRequestBehavior.AllowGet);
            }
            var id = (int)(listItem[0].Id);
            _claimantLanguageService.DeleteById(id);
            return Json(true, JsonRequestBehavior.AllowGet);
        }

        [AuthorizeContext(DocumentTypeKey = DocumentTypeKey.ClaimantLanguage, OperationAction = OperationAction.Delete)]
        public JsonResult DeleteMulti(string selectedRowIdArray, string isDeleteAll)
        {
            return DeleteMultiMasterfile(selectedRowIdArray, isDeleteAll);
        }

        [AuthorizeContext(DocumentTypeKey = DocumentTypeKey.ClaimantLanguage, OperationAction = OperationAction.Add)]
        public ActionResult Copy(int id)
        {
            var viewModel = GetMasterFileViewModel(id);
            viewModel.SharedViewModel.CreateMode = true;
            viewModel.SharedViewModel.Id = 0;
            viewModel.Id = 0;
            return View("Create", viewModel);
        }

        [AuthorizeContext(DocumentTypeKey = DocumentTypeKey.Adjuster, OperationAction = OperationAction.View)]
        public JsonResult GetLookup(LookupQuery queryInfo)
        {
            var selector = new Func<ClaimantLanguage, LookupItemVo>(o => new LookupItemVo
            {
                KeyId = o.Id,
                DisplayName = o.Name
            });
            return base.GetLookupForEntity(queryInfo, selector);
        }

        [AuthorizeContext(DocumentTypeKey = DocumentTypeKey.ClaimantLanguage, OperationAction = OperationAction.View)]
        public JsonResult ExportExcel(GridConfigViewModel gridConfig, QueryInfo queryInfo)
        {
            return base.ExportExcelMasterfile(gridConfig, queryInfo);
        }
    }
}
