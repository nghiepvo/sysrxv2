﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using Framework.DomainModel.Entities;
using Framework.DomainModel.Entities.Common;
using Framework.DomainModel.Entities.Security;
using Framework.DomainModel.ValueObject;
using Framework.Service.Diagnostics;
using ServiceLayer.Interfaces;
using ServiceLayer.Interfaces.Authentication;
using WorkComp.Attributes;
using WorkComp.Models;
using WorkComp.Models.ReferralNote;

namespace WorkComp.Controllers
{
    public class ReferralNoteController : ApplicationControllerGeneric<ReferralNote, DashboardReferralNoteDataViewModel>
    {
        private readonly IReferralNoteService _note;
        public ReferralNoteController(IAuthenticationService authenticationService, IDiagnosticService diagnosticService, IReferralNoteService note)
            : base(authenticationService, diagnosticService, note)
        {
            _note = note;
        }

        [AuthorizeContext(DocumentTypeKey = DocumentTypeKey.Referral, OperationAction = OperationAction.Add)]
        public ActionResult Create()
        {
            var viewModel = new DashboardReferralNoteDataViewModel
            {
                SharedViewModel = new DashboardReferralNoteShareViewModel
                {
                    CreateMode = true
                }
            };
            // Check show popup for create from dropdown
            var type = Request.QueryString["type"];
            if (type == "1")
            {
                viewModel.SharedViewModel.ShowPopupType = ShowPopupType.Grid;
            }
            return View(viewModel);
        }

        [HttpPost]
        [AuthorizeContext(DocumentTypeKey = DocumentTypeKey.Referral, OperationAction = OperationAction.Add)]
        public int Create(ReferralNoteParameter parameters)
        {
            var savedEntity = CreateMasterFile(parameters);
            return savedEntity.Id;
        }

        [AuthorizeContext(DocumentTypeKey = DocumentTypeKey.Referral, OperationAction = OperationAction.View)]
        public JsonResult GetCommentWhenChangeReferralTask(int? id)
        {
            if (id.GetValueOrDefault() == 0)
            {
                return Json("", JsonRequestBehavior.AllowGet);
            }
            var objData = _note.GetReferralNoteWhenChangeReferral(id.GetValueOrDefault());
            return objData != null ? Json(objData, JsonRequestBehavior.AllowGet) : Json("", JsonRequestBehavior.AllowGet);
        }
    }
}
