﻿using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.Linq;
using System.Web.Mvc;
using Framework.DomainModel.Entities;
using Framework.DomainModel.Entities.Common;
using Framework.DomainModel.Entities.Security;
using Framework.DomainModel.ValueObject;
using Framework.Service.Diagnostics;
using Newtonsoft.Json;
using ServiceLayer.Interfaces;
using ServiceLayer.Interfaces.Authentication;
using WorkComp.Attributes;
using WorkComp.Models;
using WorkComp.Models.Icd;

namespace WorkComp.Controllers
{
    public class IcdController : ApplicationControllerGeneric<Icd, DashboardIcdDataViewModel>
    {
        //
        // GET: /Icd/

        private readonly IIcdService _icdService;
        public IcdController(IAuthenticationService authenticationService, IDiagnosticService diagnosticService, IIcdService icdService)
            : base(authenticationService,diagnosticService, icdService)
        {
            _icdService = icdService;
        }
         [AuthorizeContext(DocumentTypeKey = DocumentTypeKey.Icd, OperationAction = OperationAction.View)]
        public ActionResult Index()
        {
            var viewModel = new DashboardIcdIndexViewModel();
            Func<GridViewModel> gridViewModel = () => new GridViewModel
            {
                GridId = "IcdGrid",
                ModelName = "Icd",
                UseCheckBoxColumn = true,
                UseDeleteColumn = true,
                WidthPopup = 800,
                HeightPopup = 600
            };

            viewModel.GridViewModel = BuildGridViewModel(gridViewModel);
            return View(viewModel);
        }

        [HttpGet]
        [AuthorizeContext(DocumentTypeKey = DocumentTypeKey.Icd, OperationAction = OperationAction.View)]
        public override JsonResult GetDataForGrid(QueryInfo queryInfo)
        {
            return base.GetDataForGrid(queryInfo);
        }

        [HttpGet]
        [AuthorizeContext(DocumentTypeKey = DocumentTypeKey.Icd, OperationAction = OperationAction.View)]
        public JsonResult GetListIcdTypes()
        {
            var queryData = _icdService.GetListIcdType();
            var clientsJson = Json(queryData, JsonRequestBehavior.AllowGet);
            return clientsJson;
        }
        [AuthorizeContext(DocumentTypeKey = DocumentTypeKey.Icd, OperationAction = OperationAction.Delete)]
        public JsonResult DeleteMulti(string selectedRowIdArray,string isDeleteAll)
        {
            return DeleteMultiMasterfile(selectedRowIdArray, isDeleteAll);
        }

        [AuthorizeContext(DocumentTypeKey = DocumentTypeKey.Icd, OperationAction = OperationAction.Delete)]
        public JsonResult Delete(string models)
        {
            var listItem = JsonConvert.DeserializeObject<Collection<dynamic>>(models);
            if (listItem.Count == 0)
            {
                return Json(false, JsonRequestBehavior.AllowGet);
            }
            var id = (int)(listItem[0].Id);
            _icdService.DeleteById(id);
            return Json(true, JsonRequestBehavior.AllowGet);
        }

        [AuthorizeContext(DocumentTypeKey = DocumentTypeKey.Icd, OperationAction = OperationAction.Add)]
        public ActionResult Create()
        {
            var viewModel = new DashboardIcdDataViewModel
            {
                SharedViewModel = new DashboardIcdShareViewModel
                {
                    CreateMode = true
                }
            };
            // Check show popup for create from dropdown
            var type = Request.QueryString["type"];
            if (type == "1")
            {
                viewModel.SharedViewModel.ShowPopupType = ShowPopupType.ComboBox;
            }
            return View(viewModel);
        }

        [AuthorizeContext(DocumentTypeKey = DocumentTypeKey.Icd, OperationAction = OperationAction.Add)]
        public ActionResult Copy(int id)
        {
            var viewModel = GetMasterFileViewModel(id);
            viewModel.SharedViewModel.CreateMode = true;
            viewModel.SharedViewModel.Id = 0;
            viewModel.Id = 0;
            return View("Create",viewModel);
        }

        [HttpPost]
        [AuthorizeContext(DocumentTypeKey = DocumentTypeKey.Icd, OperationAction = OperationAction.Add)]
        public int Create(IcdParameter parameters)
        {
            var savedEntity = CreateMasterFile(parameters);
            return savedEntity.Id;
        }

        [AuthorizeContext(DocumentTypeKey = DocumentTypeKey.Icd, OperationAction = OperationAction.Update)]
        public ActionResult Update(int id)
        {
            var viewModel = GetMasterFileViewModel(id);
            // Check show popup for create from dropdown
            var type = Request.QueryString["type"];
            if (type == "1")
            {
                viewModel.SharedViewModel.ShowPopupType = ShowPopupType.ComboBox;
            }
            return View(viewModel);
        }

        [HttpPost]
        [AuthorizeContext(DocumentTypeKey = DocumentTypeKey.Icd, OperationAction = OperationAction.Update)]
        public ActionResult Update(IcdParameter parameters)
        {
            return UpdateMasterFile(parameters);
        }

        [AuthorizeContext(DocumentTypeKey = DocumentTypeKey.Icd, OperationAction = OperationAction.View)]
        public JsonResult ExportExcel(GridConfigViewModel gridConfig, QueryInfo queryInfo)
        {
            return base.ExportExcelMasterfile(gridConfig, queryInfo);
        }

        protected override IList<ViewColumnViewModel> GetViewColumns()
        {
            var objViewColumn = new List<ViewColumnViewModel>
            {
                new ViewColumnViewModel
                {
                    ColumnOrder = 1,
                    ColumnWidth = 100,
                    Name = "Type",
                    Text = "Type",
                    ColumnJustification = GridColumnJustification.Left
                },
                new ViewColumnViewModel
                {
                    ColumnOrder = 2,
                    ColumnWidth = 100,
                    Name = "Code",
                    Text = "Code",
                    ColumnJustification = GridColumnJustification.Left
                },
                new ViewColumnViewModel
                {
                    ColumnOrder = 3,
                    Name = "Description",
                    Text = "Description",
                    ColumnWidth = 900,
                    ColumnJustification = GridColumnJustification.Left
                }
            };
            return objViewColumn;
        }
        [AuthorizeContext(DocumentTypeKey = DocumentTypeKey.Icd, OperationAction = OperationAction.View)]
        public JsonResult CheckCodeExists(int id,string code)
        {
            if (string.IsNullOrEmpty(code) || id==0)
            {
                return Json(true, JsonRequestBehavior.AllowGet);
            }
            return Json(_icdService.CheckExist(
                o =>o.Code.Trim().ToLower()==code.Trim().ToLower()
                    &&
                    o.Id != id), JsonRequestBehavior.AllowGet);
        }

        [AuthorizeContext(DocumentTypeKey = DocumentTypeKey.Icd, OperationAction = OperationAction.View)]
        public JsonResult GetLookup(LookupQuery queryInfo)
        {
            var selector = new Func<Icd, LookupItemVo>(o => new LookupItemVo
            {
                KeyId = o.Id,
                DisplayName = o.Code
            });
            return base.GetLookupForEntity(queryInfo, selector);
        }
        [AuthorizeContext(DocumentTypeKey = DocumentTypeKey.Icd, OperationAction = OperationAction.View)]
        public JsonResult GetLookupDescription(LookupQuery queryInfo)
        {
            var selector = new Func<Icd, LookupItemVo>(o => new LookupItemVo
            {
                KeyId = o.Id,
                DisplayName = o.Description
            });
            queryInfo.MasterFilterFieldName = "Description";
            return base.GetLookupForEntity(queryInfo, selector);

        }

        [AuthorizeContext(DocumentTypeKey = DocumentTypeKey.Icd, OperationAction = OperationAction.View)]
        public JsonResult GetLookupItem(LookupItem lookupItem)
        {
            var selector = new Func<Icd, LookupItemVo>(o => new LookupItemVo
            {
                KeyId = o.Id,
                DisplayName = o.Code
            });
            if (lookupItem.HierachyItem != null && lookupItem.HierachyItem.Name == "IcdCode")
            {
                selector = o => new LookupItemVo
                {
                    KeyId = o.Id,
                    DisplayName = o.Description
                };
            }
            return base.GetLookupItemForEntity(lookupItem, selector);
        }

        [AuthorizeContext(DocumentTypeKey = DocumentTypeKey.Icd, OperationAction = OperationAction.View), HttpPost]
        public JsonResult GetDescriptionById(int id)
        {
            var data = _icdService.Get(o => o.Id == id).Select(o => new LookupItemVo { KeyId = o.Id, DisplayName = o.Description }).FirstOrDefault();
            return Json(data, JsonRequestBehavior.AllowGet);
        }

        [AuthorizeContext(DocumentTypeKey = DocumentTypeKey.Icd, OperationAction = OperationAction.View), HttpPost]
        public JsonResult GetCodeById(int id)
        {
            var data = _icdService.Get(o => o.Id == id).Select(o => new LookupItemVo { KeyId = o.Id, DisplayName = o.Code }).FirstOrDefault();
            return Json(data, JsonRequestBehavior.AllowGet);
        }

    }

}
