﻿using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.Web.Mvc;
using Framework.DomainModel.Entities;
using Framework.DomainModel.Entities.Common;
using Framework.DomainModel.Entities.Security;
using Framework.DomainModel.ValueObject;
using Framework.Service.Diagnostics;
using Newtonsoft.Json;
using ServiceLayer.Interfaces;
using ServiceLayer.Interfaces.Authentication;
using WorkComp.Attributes;
using WorkComp.Models;
using WorkComp.Models.Adjuster;


namespace WorkComp.Controllers
{
    public class AdjusterController : ApplicationControllerGeneric<Adjuster, DashboardAdjusterDataViewModel>
    {
        //
        // GET: /State/
        private readonly IAdjusterService _adjuster;
        public AdjusterController(IAuthenticationService authenticationService, IDiagnosticService diagnosticService, IAdjusterService adjuster)
            : base(authenticationService, diagnosticService, adjuster)
        {
            _adjuster = adjuster;
        }
        [AuthorizeContext(DocumentTypeKey = DocumentTypeKey.Adjuster, OperationAction = OperationAction.View)]
        public ActionResult Index()
        {
            var viewModel = new DashboardAdjusterIndexViewModel();
            Func<GridViewModel> gridViewModel = () => new GridViewModel
            {
                GridId = "AdjusterGrid",
                ModelName = "Adjuster",
                UseCheckBoxColumn = true,
                UseDeleteColumn = true,
                WidthPopup = 800,
                HeightPopup = 600
            };

            viewModel.GridViewModel = BuildGridViewModel(gridViewModel);
            return View(viewModel);
        }


        [HttpGet]
        [AuthorizeContext(DocumentTypeKey = DocumentTypeKey.Adjuster, OperationAction = OperationAction.View)]
        public override JsonResult GetDataForGrid(QueryInfo queryInfo)
        {
            return base.GetDataForGrid(queryInfo);
        }

        [AuthorizeContext(DocumentTypeKey = DocumentTypeKey.Adjuster, OperationAction = OperationAction.Add)]
        public ActionResult Create()
        {
            var viewModel = new DashboardAdjusterDataViewModel
            {
                SharedViewModel = new DashboardAdjusterShareViewModel
                {
                    AssignedDate = DateTime.Now,
                    IsUserUpdate = true,
                    CreateMode = true
                }
            };
            // Check show popup for create from dropdown
            var type = Request.QueryString["type"];
            if (type == "1")
            {
                viewModel.SharedViewModel.ShowPopupType = ShowPopupType.ComboBox;
            }
            return View(viewModel);
        }

        [HttpPost]
        [AuthorizeContext(DocumentTypeKey = DocumentTypeKey.Adjuster, OperationAction = OperationAction.Add)]
        public int Create(AdjusterParameter parameters)
        {
            var savedEntity = CreateMasterFile(parameters);
            return savedEntity.Id;
        }

        [AuthorizeContext(DocumentTypeKey = DocumentTypeKey.Adjuster, OperationAction = OperationAction.Update)]
        public ActionResult Update(int id)
        {
            var viewModel = GetMasterFileViewModel(id);
            // Check show popup for create from dropdown
            var type = Request.QueryString["type"];
            if (type == "1")
            {
                viewModel.SharedViewModel.ShowPopupType = ShowPopupType.ComboBox;
            }
            return View(viewModel);
        }

        [HttpPost]
        [AuthorizeContext(DocumentTypeKey = DocumentTypeKey.Adjuster, OperationAction = OperationAction.Update)]
        public ActionResult Update(AdjusterParameter parameters)
        {
            return UpdateMasterFile(parameters);
        }

        [AuthorizeContext(DocumentTypeKey = DocumentTypeKey.Adjuster, OperationAction = OperationAction.Add)]
        public ActionResult Copy(int id)
        {
            var viewModel = GetMasterFileViewModel(id);
            viewModel.SharedViewModel.CreateMode = true;
            viewModel.SharedViewModel.Id = 0;
            viewModel.Id = 0;
            return View("Create",viewModel);
        }

        [AuthorizeContext(DocumentTypeKey = DocumentTypeKey.Adjuster, OperationAction = OperationAction.Delete)]
        public JsonResult Delete(string models)
        {
            var listItem = JsonConvert.DeserializeObject<Collection<dynamic>>(models);
            if (listItem.Count == 0)
            {
                return Json(false, JsonRequestBehavior.AllowGet);
            }
            var id = (int)(listItem[0].Id);
            _adjuster.DeleteById(id);
            return Json(true, JsonRequestBehavior.AllowGet);
        }

        [AuthorizeContext(DocumentTypeKey = DocumentTypeKey.Adjuster, OperationAction = OperationAction.Delete)]
        public JsonResult DeleteMulti(string selectedRowIdArray, string isDeleteAll)
        {
            return DeleteMultiMasterfile(selectedRowIdArray, isDeleteAll);
        }

        [AuthorizeContext(DocumentTypeKey = DocumentTypeKey.Adjuster, OperationAction = OperationAction.View)]
        public JsonResult ExportExcel(GridConfigViewModel gridConfig, QueryInfo queryInfo)
        {
            return base.ExportExcelMasterfile(gridConfig, queryInfo);
        }

        [AuthorizeContext(DocumentTypeKey = DocumentTypeKey.Adjuster, OperationAction = OperationAction.View)]
        public JsonResult GetLookup(LookupQuery queryInfo)
        {
            var selector = new Func<Adjuster, LookupItemVo>(o => new LookupItemVo
            {
                KeyId = o.Id,
                DisplayName = o.FirstName + " " + o.MiddleName + " " + o.LastName
            });
            return base.GetLookupForEntity(queryInfo, selector);
        }

        [AuthorizeContext(DocumentTypeKey = DocumentTypeKey.Adjuster, OperationAction = OperationAction.View)]
        public JsonResult GetLookupItem(LookupItem lookupItem)
        {
            var selector = new Func<Adjuster, LookupItemVo>(o => new LookupItemVo
            {
                KeyId = o.Id,
                DisplayName = o.FirstName + " " + o.MiddleName + " " + o.LastName
            });
            return base.GetLookupItemForEntity(lookupItem, selector);
        }

        protected override IList<ViewColumnViewModel> GetViewColumns()
        {
            var objViewColumn = new List<ViewColumnViewModel>
            {
                new ViewColumnViewModel
                {
                    ColumnOrder = 1,
                    ColumnWidth = 100,
                    Name = "Name",
                    Text = "Name",
                    ColumnJustification = GridColumnJustification.Left,
                },
                new ViewColumnViewModel
                {
                    ColumnOrder = 2,
                    Name = "Branch",
                    Text = "Branch",
                    ColumnWidth = 100,
                    ColumnJustification = GridColumnJustification.Left
                },
                new ViewColumnViewModel
                {
                    ColumnOrder = 3,
                    ColumnWidth = 150,
                    Name = "Email",
                    Text = "Email",
                    ColumnJustification = GridColumnJustification.Left
                },
                new ViewColumnViewModel
                {
                    ColumnOrder = 4,
                    Name = "Phone",
                    Text = "Phone",
                    ColumnWidth = 100,
                    ColumnJustification = GridColumnJustification.Left
                },
                new ViewColumnViewModel
                {
                    ColumnOrder = 5,
                    Name = "Extension",
                    Text = "Ext",
                    ColumnWidth = 50,
                    ColumnJustification = GridColumnJustification.Left
                },
                new ViewColumnViewModel
                {
                    ColumnOrder = 6,
                    Name = "FullAddress",
                    Text = "Address",
                    ColumnWidth = 250,
                    ColumnJustification = GridColumnJustification.Left,
                },
                new ViewColumnViewModel
                {
                    ColumnOrder = 7,
                    Name = "OptedOutSendMail",
                    Text = "Opted out of the Medication Monitoring Program",
                    ColumnWidth = 250,
                    ColumnJustification = GridColumnJustification.Left,
                },
            };
            return objViewColumn;
        }

    }
}
