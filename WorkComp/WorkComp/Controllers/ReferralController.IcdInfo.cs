﻿using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Web.Mvc;
using Framework.DomainModel.Entities;
using Framework.DomainModel.Entities.Common;
using Framework.DomainModel.Entities.Security;
using Framework.DomainModel.ValueObject;
using Framework.Exceptions;
using Framework.Service.Translation;
using Newtonsoft.Json;
using ServiceLayer.Interfaces;
using WorkComp.Attributes;
using WorkComp.Models.Icd;
using WorkComp.Models.Referral;

namespace WorkComp.Controllers
{
    public partial class ReferralController
    {
        private readonly IIcdService _icdService;
        [AuthorizeContext(DocumentTypeKey = DocumentTypeKey.Icd, OperationAction = OperationAction.View)]
        public JsonResult GetIcds(int? referralId)
        {
            var listIcd = _icdService.GetIcdsByReferral(referralId.GetValueOrDefault());
            // create list grid PanelCodeGridItemViewModel
            var objGrid = new List<IcdGridItemViewModel>();
            foreach (var icdItem in listIcd)
            {
                var objAdd = new IcdGridItemViewModel
                {
                    IcdCode = new LookupInGridDataSourceViewModel
                    {
                        KeyId = icdItem.Id,
                        Name = icdItem.Code
                    },
                    ReferralId = referralId,
                    RowGuid = Guid.NewGuid(),
                    IcdDescription = new LookupInGridDataSourceViewModel
                    {
                        KeyId = icdItem.Id,
                        Name = icdItem.Description
                    }
                };
                objGrid.Add(objAdd);
            }
            dynamic dynamicResult = new { Data = objGrid, TotalRowCount = objGrid.Count };
            var clientsJson = Json(dynamicResult, JsonRequestBehavior.AllowGet);
            return clientsJson;
        }

        private static void ProcessForIcdInfo(DashboardReferralDataViewModel viewModel, string icdInfo)
        {
            if (string.IsNullOrEmpty(icdInfo))
            {
                return;
            }
            var icdData = JsonConvert.DeserializeObject<Dictionary<string, object>>(icdInfo);
            var listAllJson = icdData.SingleOrDefault(g => g.Key.Equals("listAll")).Value.ToString();
            var listAll = JsonConvert.DeserializeObject<Collection<IcdGridItemViewModel>>(listAllJson);
            var objResult = new List<ReferralIcd>();
            if (listAll != null && listAll.Count != 0)
            {
                foreach (var icdItem in listAll)
                {
                    if (icdItem.IcdCode.KeyId <= 0)
                    {
                        icdItem.IcdCode.KeyId = icdItem.IcdDescription.KeyId;
                    }
                    if (icdItem.IcdDescription.KeyId <= 0)
                    {
                        icdItem.IcdDescription.KeyId = icdItem.IcdCode.KeyId;
                    }
                }
                // Validate Icd
                CheckIcdCodeValid(listAll);
                foreach (var item in listAll.Where(o => o.IcdCode.KeyId != 0))
                {
                    var objAdd = new ReferralIcd
                    {
                        IcdId = item.IcdCode.KeyId
                    };
                    objResult.Add(objAdd);
                }
            }
            viewModel.Icds = objResult;
        }
        private static void CheckIcdCodeValid(Collection<IcdGridItemViewModel> listIcd)
        {
            if (listIcd.Count == 0)
            {
                return;
            }
            var failed = false;
            var validationResult = new List<ValidationResult>();
            // Check if no select icd code
            if (listIcd.Any(o => o.IcdCode.KeyId <= 0&&o.IcdDescription.KeyId<=0))
            {
                var mess = string.Format(SystemMessageLookup.GetMessage("RequiredTextResourceKey"), "Icd Code or Description");
                validationResult.Add(new ValidationResult(mess));
                failed = true;
            }

            //if (listIcd.Any(o => o.IcdDescription.KeyId <= 0))
            //{
            //    var mess = string.Format(SystemMessageLookup.GetMessage("RequiredTextResourceKey"), "Icd Description");
            //    validationResult.Add(new ValidationResult(mess));
            //    failed = true;
            //}
            //Check if there is >=2 Id
            var checkDupicate = listIcd.GroupBy(s => s.IcdCode.KeyId).Any(g => g.Count() > 1);
            if (checkDupicate)
            {
                var mess = string.Format(SystemMessageLookup.GetMessage("DuplicateItemText"), "Icd Code or Icd Description");
                validationResult.Add(new ValidationResult(mess));
                failed = true;
            }
            var result = new BusinessRuleResult(failed, "", "ReferralIcd", 0, null, "ReferralIcdRule") { ValidationResults = validationResult };
            if (failed)
            {
                // Give messages on every rule that failed
                throw new BusinessRuleException("BussinessGenericErrorMessageKey", new[] { result });
            }
        }

        private static bool CheckUserChangeIcdWhenUpdate(IEnumerable<ReferralIcd> listOld,
            IEnumerable<ReferralIcd> listNew)
        {
            var listIdOld = listOld.OrderBy(o => o.IcdId.GetValueOrDefault()).Select(o => o.IcdId.GetValueOrDefault()).ToList();
            var listIdNew = listNew.OrderBy(o => o.IcdId.GetValueOrDefault()).Select(o => o.IcdId.GetValueOrDefault()).ToList();
            return !(listIdOld.Count == listIdNew.Count && new HashSet<int>(listIdOld).SetEquals(listIdNew));
        }
    }
}