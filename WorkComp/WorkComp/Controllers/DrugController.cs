﻿using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.Web.Mvc;
using Framework.DomainModel.Entities;
using Framework.DomainModel.Entities.Common;
using Framework.DomainModel.Entities.Security;
using Framework.DomainModel.ValueObject;
using Framework.Service.Diagnostics;
using Newtonsoft.Json;
using ServiceLayer.Interfaces;
using ServiceLayer.Interfaces.Authentication;
using WorkComp.Attributes;
using WorkComp.Models;
using WorkComp.Models.Drug;

namespace WorkComp.Controllers
{
    public class DrugController : ApplicationControllerGeneric<Drug, DashboardDrugDataViewModel>
    {
        //
        // GET: /State/
        private readonly IDrugService _drugService;
        public DrugController(IAuthenticationService authenticationService, IDiagnosticService diagnosticService, IDrugService drugService)
            : base(authenticationService, diagnosticService, drugService)
        {
            _drugService = drugService;
        }
        [AuthorizeContext(DocumentTypeKey = DocumentTypeKey.Drug, OperationAction = OperationAction.View)]
        public ActionResult Index()
        {
            var viewModel = new DashboardDrugIndexViewModel();
            Func<GridViewModel> gridViewModel = () => new GridViewModel
            {
                GridId = "DrugGrid",
                ModelName = "Drug",
                UseCheckBoxColumn = true,
                UseDeleteColumn = true,
                WidthPopup = 800,
                HeightPopup = 600
            };

            viewModel.GridViewModel = BuildGridViewModel(gridViewModel);
            return View(viewModel);
        }

        [HttpGet]
        [AuthorizeContext(DocumentTypeKey = DocumentTypeKey.Drug, OperationAction = OperationAction.View)]
        public override JsonResult GetDataForGrid(QueryInfo queryInfo)
        {
            return base.GetDataForGrid(queryInfo);
        }

        [AuthorizeContext(DocumentTypeKey = DocumentTypeKey.Drug, OperationAction = OperationAction.Add)]
        public ActionResult Create()
        {
            var viewModel = new DashboardDrugDataViewModel
            {
                SharedViewModel = new DashboardDrugShareViewModel
                {
                    CreateMode = true
                }
            };
            // Check show popup for create from dropdown
            var type = Request.QueryString["type"];
            if (type == "1")
            {
                viewModel.SharedViewModel.ShowPopupType = ShowPopupType.ComboBox;
            }
            return View(viewModel);
        }

        [HttpPost]
        [AuthorizeContext(DocumentTypeKey = DocumentTypeKey.Drug, OperationAction = OperationAction.Add)]
        public int Create(DrugParameter parameters)
        {
            var savedEntity = CreateMasterFile(parameters);
            return savedEntity.Id;
        }

        [AuthorizeContext(DocumentTypeKey = DocumentTypeKey.Drug, OperationAction = OperationAction.Update)]
        public ActionResult Update(int id)
        {
            var viewModel = GetMasterFileViewModel(id);
            // Check show popup for create from dropdown
            var type = Request.QueryString["type"];
            if (type == "1")
            {
                viewModel.SharedViewModel.ShowPopupType = ShowPopupType.ComboBox;
            }
            return View(viewModel);
        }

        [HttpPost]
        [AuthorizeContext(DocumentTypeKey = DocumentTypeKey.Drug, OperationAction = OperationAction.Update)]
        public ActionResult Update(DrugParameter parameters)
        {
            return UpdateMasterFile(parameters);
        }

        [AuthorizeContext(DocumentTypeKey = DocumentTypeKey.Drug, OperationAction = OperationAction.Add)]
        public ActionResult Copy(int id)
        {
            var viewModel = GetMasterFileViewModel(id);
            viewModel.SharedViewModel.CreateMode = true;
            viewModel.SharedViewModel.Id = 0;
            viewModel.Id = 0;
            return View("Create",viewModel);
        }

        [AuthorizeContext(DocumentTypeKey = DocumentTypeKey.Drug, OperationAction = OperationAction.Delete)]
        public JsonResult Delete(string models)
        {
            var listItem = JsonConvert.DeserializeObject<Collection<dynamic>>(models);
            if (listItem.Count == 0)
            {
                return Json(false, JsonRequestBehavior.AllowGet);
            }
            var id = (int)(listItem[0].Id);
            _drugService.DeleteById(id);
            return Json(true, JsonRequestBehavior.AllowGet);
        }

        [AuthorizeContext(DocumentTypeKey = DocumentTypeKey.Drug, OperationAction = OperationAction.Delete)]
        public JsonResult DeleteMulti(string selectedRowIdArray, string isDeleteAll)
        {
            return DeleteMultiMasterfile(selectedRowIdArray, isDeleteAll);
        }

        [AuthorizeContext(DocumentTypeKey = DocumentTypeKey.Drug, OperationAction = OperationAction.View)]
        public JsonResult ExportExcel(GridConfigViewModel gridConfig, QueryInfo queryInfo)
        {
            return base.ExportExcelMasterfile(gridConfig, queryInfo);
        }

        protected override IList<ViewColumnViewModel> GetViewColumns()
        {
            var objViewColumn = new List<ViewColumnViewModel>
            {
                
                new ViewColumnViewModel
                {
                    ColumnOrder = 1,
                    ColumnWidth = 250,
                    Name = "Name",
                    Text = "Name",
                    ColumnJustification = GridColumnJustification.Left
                },
                new ViewColumnViewModel
                {
                    ColumnOrder = 2,
                    ColumnWidth = 150,
                    Name = "Class",
                    Text = "Class",
                    ColumnJustification = GridColumnJustification.Left
                },
                new ViewColumnViewModel
                {
                    ColumnOrder = 3,
                    Name = "Description",
                    Text = "Description",
                    ColumnWidth = 700,
                    ColumnJustification = GridColumnJustification.Left
                }
            };
            return objViewColumn;
        }

        [AuthorizeContext(DocumentTypeKey = DocumentTypeKey.Drug, OperationAction = OperationAction.View)]
        public JsonResult GetLookup(LookupQuery queryInfo)
        {
            var selector = new Func<Drug, LookupItemVo>(o => new LookupItemVo
            {
                KeyId = o.Id,
                DisplayName = o.Name
            });
            return base.GetLookupForEntity(queryInfo, selector);
        }
        [AuthorizeContext(DocumentTypeKey = DocumentTypeKey.Drug, OperationAction = OperationAction.View)]
        public JsonResult GetItem(int idDrug)
        {
            var data = _drugService.GetById(idDrug);
            if (data == null) return Json("", JsonRequestBehavior.AllowGet);
            var item = new
            {
                data.Id,
                DrugClass = data.Class
            };
            return Json(item, JsonRequestBehavior.AllowGet);
        }
    }
}
