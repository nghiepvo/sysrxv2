﻿using System.IO;
using System.Web;
using System.Web.Mvc;

namespace WorkComp.Controllers
{
    public class ImageBrowserController : Controller
    {
        //
        // GET: /ImageBrowser/

       
        public JsonResult Read(string path)
        {
            var objImages = new object();
            var url = Server.MapPath("/Content/userfiles/imagebrowser/" + path);
            string[] filePaths = Directory.GetFiles(url);
            objImages = new
            {
                name="logo.png",
                type="f",
                size="1253"
            };
            return Json(objImages,JsonRequestBehavior.AllowGet);
        }

        public JsonResult Create(string name,string path,string size,string type)
        {
            var objImages = new object();
            var url = Server.MapPath("/Content/userfiles/imagebrowser/" + path + name);
            if (!Directory.Exists(url))
               Directory.CreateDirectory(url);
            objImages = new
            {
                name = name,
                type = type,
                size = size
            };
            return Json(objImages, JsonRequestBehavior.AllowGet);
        }
        public JsonResult Destroy(string name, string path, string size, string type)
        {
            var objImages = new object();
            return Json(objImages, JsonRequestBehavior.AllowGet);
        }
        public JsonResult Upload(HttpPostedFileBase file)
        {
            var objImages = new object();

            return Json(objImages, JsonRequestBehavior.AllowGet);
        }
    }
}
