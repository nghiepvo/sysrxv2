﻿using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Web.Mvc;
using DotNetOpenAuth.Messaging;
using Framework.DomainModel.Entities;
using Framework.DomainModel.Entities.Common;
using Framework.DomainModel.Entities.Security;
using Framework.DomainModel.ValueObject;
using Framework.Exceptions;
using Framework.Mapping;
using Framework.Service.Diagnostics;
using Framework.Service.Translation;
using Framework.Utility;
using Newtonsoft.Json;
using ServiceLayer.Interfaces;
using ServiceLayer.Interfaces.Authentication;
using WorkComp.Attributes;
using WorkComp.Models;
using WorkComp.Models.Referral;

namespace WorkComp.Controllers
{
    public partial class ReferralController : ApplicationControllerGeneric<Referral, DashboardReferralDataViewModel>
    {
        private readonly IReferralService _referralService;
        private readonly IUserService _userService;
        private readonly ISampleTestingTypeService _sampleTestingTypeService;
        private readonly IReferralSampleTestingTypeService _referralSampleTestingTypeService;
        private readonly IDrugService _drugService; 
        private readonly IReferralEmailTemplateService _referralEmailTemplateService;

        public ReferralController(IAuthenticationService authenticationService, IDiagnosticService diagnosticService, IReferralService referralService,
                                    IReferralSourceService referralSourceService, IClaimNumberService claimNumberService, IPayerService payerService,
                                    IAdjusterService adjusterService, IClaimantService claimantService, ICaseManagerService caseManagerService,
                                    IAttorneyService attorneyService, IEmployerService employerService, IPanelTypeService panelTypeService,
                                    ICollectionSiteService collectionSiteService, INpiNumberService npiNumberService, IReferralAttachmentService fileAttachmentService,
                                    IIcdService icdService, IUserService userService, IEmailTemplateService emailTemplateService, ISampleTestingTypeService sampleTestingTypeService,
                                    IReferralSampleTestingTypeService referralSampleTestingTypeService, IDiaryService diaryService, IConfigurationService configurationService, IDrugService drugService,
            IReferralEmailTemplateService referralEmailTemplateService, IReferralEmailTemplateAttachmentService referralEmailTemplateAttachmentService)
            : base(authenticationService, diagnosticService, referralService)
        {
            _referralService = referralService;
            _referralSourceService = referralSourceService;
            _claimNumberService = claimNumberService;
            _payerService = payerService;
            _adjusterService = adjusterService;
            _claimantService = claimantService;
            _caseManagerService = caseManagerService;
            _attorneyService = attorneyService;
            _employerService = employerService;
            _panelTypeService = panelTypeService;
            _collectionSiteService = collectionSiteService;
            _npiNumberService = npiNumberService;
            _fileAttachmentService = fileAttachmentService;
            _icdService = icdService;
            _userService = userService;
            _emailTemplateService = emailTemplateService;
            _sampleTestingTypeService = sampleTestingTypeService;
            _referralSampleTestingTypeService = referralSampleTestingTypeService;
            _drugService = drugService;
            _referralEmailTemplateService = referralEmailTemplateService;
            _referralEmailTemplateAttachmentService = referralEmailTemplateAttachmentService;
            _configurationService = configurationService;
            _diaryService = diaryService;
        }

        [AuthorizeContext(DocumentTypeKey = DocumentTypeKey.Referral, OperationAction = OperationAction.View)]
        public ActionResult Index()
        {
            return ViewReferral(TypeWithUserQueryEnum.Current);
        }

        [AuthorizeContext(DocumentTypeKey = DocumentTypeKey.Referral, OperationAction = OperationAction.View)]
        public ActionResult All()
        {
            return ViewReferral(TypeWithUserQueryEnum.All);
        }

        [AuthorizeContext(DocumentTypeKey = DocumentTypeKey.Referral, OperationAction = OperationAction.View)]
        public ActionResult Other()
        {
            return ViewReferral(TypeWithUserQueryEnum.Other);
        }

        private ActionResult ViewReferral(TypeWithUserQueryEnum type)
        {
            var viewModel = new DashboardReferralIndexViewModel();
            var filterType = new ReferralFilterItem
            {
                FilterType = ReferralFilterType.All
            };
            if (Session["ReferralFilterType"] != null)
            {
                filterType = Session["ReferralFilterType"] as ReferralFilterItem;
            }
            viewModel.FilterType = filterType;
            Func<GridViewModel> gridViewModel = () => new GridViewModel
            {
                GridId = "ReferralGrid",
                ModelName = "Referral",
                UseCheckBoxColumn = true,
                UseDeleteColumn = true,
                WidthPopup = 800,
                HeightPopup = 600,
                TypeWithUser = type
            };
            viewModel.GridViewModel = BuildGridViewModel(gridViewModel);
            return View("Index", viewModel);
        }

        [AuthorizeContext(DocumentTypeKey = DocumentTypeKey.Referral, OperationAction = OperationAction.View)]
        public JsonResult GetListTaskByReferralId(int referralId)
        {
            return Json(_referralService.GetListTaskByReferralId(referralId), JsonRequestBehavior.AllowGet);
        }

        [HttpGet]
        [AuthorizeContext(DocumentTypeKey = DocumentTypeKey.Referral, OperationAction = OperationAction.View)]
        public JsonResult GetDataParentForGrid(ReferralQueryInfo queryInfo)
        {
            var data = _referralService.GetDataParentForGrid(queryInfo);
            Session["ReferralFilterType"] = new ReferralFilterItem
            {
                FilterType = queryInfo.FilterType,
                DueDateFrom = queryInfo.DueDateFrom,
                DueDateTo = queryInfo.DueDateTo
            };
            return Json(data, JsonRequestBehavior.AllowGet);
        }

        [HttpGet]
        [AuthorizeContext(DocumentTypeKey = DocumentTypeKey.Referral, OperationAction = OperationAction.View)]
        public JsonResult GetDataChildForGrid(ReferralQueryInfo queryInfo)
        {
            var data = _referralService.GetDataChildrenForGrid(queryInfo);
            return Json(data, JsonRequestBehavior.AllowGet);
        }


        [AuthorizeContext(DocumentTypeKey = DocumentTypeKey.Referral, OperationAction = OperationAction.Add)]
        public ActionResult Create()
        {
            var viewModel = new DashboardReferralDataViewModel
            {
                SharedViewModel = new DashboardReferralShareViewModel
                {
                    CreateMode = true
                }
            };
            // Check show popup for create from dropdown
            var type = Request.QueryString["type"];
            if (type == "1")
            {
                viewModel.SharedViewModel.ShowPopupType = ShowPopupType.ComboBox;
            }
            return View(viewModel);
        }

        private DashboardReferralDataViewModel GetViewModel(ReferralParameter parameters)
        {
            var viewModel = MapFromClientParameters(parameters);
            // Create view model from parametter
            ProcessForClaimInfo(viewModel, parameters.ClaimInfoParams);
            ProcessForReasonReferralInfo(viewModel, parameters.ReasonReferralParams);
            ProcessForReferralInfo(viewModel, parameters.ReferralInfoParams);
            ProcessForIcdInfo(viewModel, parameters.IcdInfoParams);
            ProcessForTestingInfo(viewModel, parameters.TestingInfoParams);
            ProcessForMedicationHistoryInfo(viewModel, parameters.MedicationHistoryInfoParams);
            ProcessForTaskTemplateInfo(viewModel, parameters.TaskGroupInfoParams);
            ProcessForFileAttachment(viewModel, parameters.AttachmentInfoParams);
            return viewModel;
        }

        [HttpPost]
        [AuthorizeContext(DocumentTypeKey = DocumentTypeKey.Referral, OperationAction = OperationAction.Add)]
        public int Create(ReferralParameter parameters)
        {
            var viewModel = GetViewModel(parameters);
            var entity = viewModel.MapTo<Referral>();
            // Reason for referral
            entity.ReferralReasonReferrals = new Collection<ReferralReasonReferral>();
            entity.ReferralReasonReferrals.AddRange(viewModel.ReasonReferrals);
            //Icd for referral
            entity.ReferralIcds = new Collection<ReferralIcd>();
            entity.ReferralIcds.AddRange(viewModel.Icds);
            // Testing info
            entity.PanelTypeId = viewModel.TestingInfo.PanelTypeId.GetValueOrDefault() == 0 ? null : viewModel.TestingInfo.PanelTypeId;
            if (entity.IsCollectionSite.GetValueOrDefault())
            {
                entity.ReferralCollectionSites = new Collection<ReferralCollectionSite>();
                var objCollectionSiteAdd = new ReferralCollectionSite
                {
                    CollectionSiteId = viewModel.TestingInfo.CollectionSiteId,
                    SpecialInstructions = viewModel.TestingInfo.CollectionSiteSpecialInstructions,
                    CollectionDate = viewModel.TestingInfo.CollectionSiteDate,
                    Phone = viewModel.TestingInfo.CollectionSitePhone,
                    Fax = viewModel.TestingInfo.CollectionSiteFax,
                    Address = viewModel.TestingInfo.CollectionSiteAddress
                };
                entity.ReferralCollectionSites.Add(objCollectionSiteAdd);
            }
            else
            {
                entity.ReferralNpiNumbers = new Collection<ReferralNpiNumber>();
                var objTreatingPhysicianAdd = new ReferralNpiNumber
                {
                    NpiNumberId = viewModel.TestingInfo.TreatingPhysicianId,
                    NextMdVisitDate = viewModel.TestingInfo.NextMdVisitDate,
                    SpecialHandling = viewModel.TestingInfo.TreatingPhysicianSpecialHandling,
                    Phone = viewModel.TestingInfo.TreatingPhysicianPhone,
                    Fax = viewModel.TestingInfo.TreatingPhysicianFax,
                    Address = viewModel.TestingInfo.TreatingPhysicianAddress,
                    Email = viewModel.TestingInfo.TreatingPhysicianEmail,
                };
                entity.ReferralNpiNumbers.Add(objTreatingPhysicianAdd);
            }
            entity.ReferralSampleTestingTypes = new Collection<ReferralSampleTestingType>();
            var objListSampleTestingType = _sampleTestingTypeService.ListAll();
            if (viewModel.TestingInfo.CheckUrine)
            {
                var objUrine = objListSampleTestingType.FirstOrDefault(o => o.Name.Trim() == "Urine");
                if (objUrine != null)
                {
                    var objSampleTesting = new ReferralSampleTestingType
                    {
                        SampleTestingTypeId = objUrine.Id
                    };
                    entity.ReferralSampleTestingTypes.Add(objSampleTesting);
                }
            }
            if (viewModel.TestingInfo.CheckOralFluid)
            {
                var objOralFluid = objListSampleTestingType.FirstOrDefault(o => o.Name.Trim() == "Oral Fluid");
                if (objOralFluid != null)
                {
                    var objSampleTesting = new ReferralSampleTestingType
                    {
                        SampleTestingTypeId = objOralFluid.Id
                    };
                    entity.ReferralSampleTestingTypes.Add(objSampleTesting);
                }
            }
            if (viewModel.TestingInfo.CheckBlood)
            {
                var objBlood = objListSampleTestingType.FirstOrDefault(o => o.Name.Trim() == "Blood");
                if (objBlood != null)
                {
                    var objSampleTesting = new ReferralSampleTestingType
                    {
                        SampleTestingTypeId = objBlood.Id
                    };
                    entity.ReferralSampleTestingTypes.Add(objSampleTesting);
                }
            }
            if (viewModel.TestingInfo.CheckHair)
            {
                var objHair = objListSampleTestingType.FirstOrDefault(o => o.Name.Trim() == "Hair");
                if (objHair != null)
                {
                    var objSampleTesting = new ReferralSampleTestingType
                    {
                        SampleTestingTypeId = objHair.Id
                    };
                    entity.ReferralSampleTestingTypes.Add(objSampleTesting);
                }
            }
            // Medication history
            if (!entity.NoMedicationHistory.GetValueOrDefault())
            {
                entity.ReferralMedicationHistories = new Collection<ReferralMedicationHistory>();
                entity.ReferralMedicationHistories.AddRange(viewModel.MedicationHistories);
            }
            // Task info
            entity.ReferralTasks = new Collection<ReferralTask>();
            entity.ReferralTasks.AddRange(viewModel.Tasks);
            // Attachment Info
            _fileAttachmentService.TransferUploadPath = TransferUploadPath;
            foreach (var attachmentItem in viewModel.Attachments)
            {
                if (!attachmentItem.IsDeleted)
                {
                    var fileContent = _fileAttachmentService.ReadFile(attachmentItem.AttachedFileName,
                                                                         attachmentItem.RowGUID);
                    if (fileContent != null)
                    {
                        attachmentItem.AttachedFileContent = fileContent;
                    }
                }
            }
            entity.ReferralAttachments = new Collection<ReferralAttachment>();
            var listAttachmentsAdd = viewModel.Attachments.Where(o => !o.IsDeleted).ToList();
            entity.ReferralAttachments.AddRange(listAttachmentsAdd);
            var savedEntity = _referralService.Add(entity);
            if (savedEntity.Id > 0)
            {
                // Delete file in folder
                foreach (var attachmentItem in viewModel.Attachments)
                {
                    if (!attachmentItem.IsDeleted)
                    {
                        _fileAttachmentService.RemoveFile(attachmentItem.AttachedFileName, attachmentItem.RowGUID);
                    }
                }
            }
            return savedEntity.Id;
        }

        [AuthorizeContext(DocumentTypeKey = DocumentTypeKey.Referral, OperationAction = OperationAction.Update)]
        public ActionResult Update(int id)
        {
            var viewModel = GetMasterFileViewModel(id);
            Session["ReferralCancelData"] = null;
            return View(viewModel);
        }

        [AuthorizeContext(DocumentTypeKey = DocumentTypeKey.Referral, OperationAction = OperationAction.Add)]
        public ActionResult Copy(int id)
        {
            var viewModel = GetMasterFileViewModel(id);
            viewModel.SharedViewModel.CreateMode = true;
            viewModel.SharedViewModel.Id = 0;
            viewModel.Id = 0;
            return View("Create", viewModel);
        }

        [HttpPost]
        [AuthorizeContext(DocumentTypeKey = DocumentTypeKey.Referral, OperationAction = OperationAction.Update)]
        public ActionResult Update(ReferralParameter parameters)
        {
            byte[] lastModified = null;

            if (ModelState.IsValid)
            {
                
                var viewModel = GetViewModel(parameters);
                var entity = MasterFileService.GetById(viewModel.SharedViewModel.Id);
                var oldStatusId = entity.StatusId;
                var isCollectionSiteOld = entity.IsCollectionSite.GetValueOrDefault();
                var noMedicationHistOld = entity.NoMedicationHistory.GetValueOrDefault();
                var mappedEntity = viewModel.MapPropertiesToInstance(entity);
                // Reason for referral
                var userChangeResonReferral =
                    CheckUserChangeReasonReferralWhenUpdate(mappedEntity.ReferralReasonReferrals,
                        viewModel.ReasonReferrals);
                if (userChangeResonReferral)
                {
                    // Delete all old
                    foreach (var oldItem in mappedEntity.ReferralReasonReferrals)
                    {
                        oldItem.IsDeleted = true;
                    }
                    mappedEntity.ReferralReasonReferrals.AddRange(viewModel.ReasonReferrals);
                }

                //Icd for referral
                var userChangeIcd =
                   CheckUserChangeIcdWhenUpdate(mappedEntity.ReferralIcds,
                       viewModel.Icds);
                if (userChangeIcd)
                {
                    // Delete all old
                    foreach (var oldItem in mappedEntity.ReferralIcds)
                    {
                        oldItem.IsDeleted = true;
                    }
                    mappedEntity.ReferralIcds.AddRange(viewModel.Icds);
                }

                // Testing info
                mappedEntity.PanelTypeId = viewModel.TestingInfo.PanelTypeId.GetValueOrDefault() == 0 ? null : viewModel.TestingInfo.PanelTypeId;
                var checkUserChangeColectionMethod =
                    CheckUserChangeCollectionMethod(isCollectionSiteOld, viewModel.TestingInfo);

                var userChangeCollectionSite =
                    CheckUserChangeCollectionSiteInfo(mappedEntity.ReferralCollectionSites.ToList(),
                        mappedEntity.ReferralNpiNumbers.ToList(), isCollectionSiteOld,
                        viewModel.TestingInfo);
                // Check if user change collection site method=> delete old info => add new info
                if (checkUserChangeColectionMethod)
                {
                    // Delete all old
                    foreach (var oldItem in mappedEntity.ReferralCollectionSites)
                    {
                        oldItem.IsDeleted = true;
                    }
                    foreach (var oldItem in mappedEntity.ReferralNpiNumbers)
                    {
                        oldItem.IsDeleted = true;
                    }
                    if (viewModel.TestingInfo.CollectionMethodId == 1)
                    {
                        var objCollectionSiteAdd = new ReferralCollectionSite
                        {
                            CollectionSiteId = viewModel.TestingInfo.CollectionSiteId,
                            SpecialInstructions = viewModel.TestingInfo.CollectionSiteSpecialInstructions,
                            CollectionDate = viewModel.TestingInfo.CollectionSiteDate,
                            Phone = viewModel.TestingInfo.CollectionSitePhone,
                            Fax = viewModel.TestingInfo.CollectionSiteFax,
                            Address = viewModel.TestingInfo.CollectionSiteAddress,
                        };
                        mappedEntity.ReferralCollectionSites.Add(objCollectionSiteAdd);
                    }
                    else
                    {
                        var objTreatingPhysicianAdd = new ReferralNpiNumber
                        {
                            NpiNumberId = viewModel.TestingInfo.TreatingPhysicianId,
                            NextMdVisitDate = viewModel.TestingInfo.NextMdVisitDate,
                            SpecialHandling = viewModel.TestingInfo.TreatingPhysicianSpecialHandling,
                            Phone = viewModel.TestingInfo.TreatingPhysicianPhone,
                            Fax = viewModel.TestingInfo.TreatingPhysicianFax,
                            Address = viewModel.TestingInfo.TreatingPhysicianAddress,
                            Email = viewModel.TestingInfo.TreatingPhysicianEmail,
                        };
                        mappedEntity.ReferralNpiNumbers.Add(objTreatingPhysicianAdd);
                    }
                }
                foreach (var oldItem in mappedEntity.ReferralSampleTestingTypes)
                {
                    oldItem.IsDeleted = true;
                }
                var objListSampleTestingType = _sampleTestingTypeService.ListAll();
                if (viewModel.TestingInfo.CheckUrine)
                {
                    var objUrine = objListSampleTestingType.FirstOrDefault(o => o.Name.Trim() == "Urine");
                    if (objUrine != null)
                    {
                        var objSampleTesting = new ReferralSampleTestingType
                        {
                            SampleTestingTypeId = objUrine.Id
                        };
                        entity.ReferralSampleTestingTypes.Add(objSampleTesting);
                    }
                }
                if (viewModel.TestingInfo.CheckOralFluid)
                {
                    var objOralFluid = objListSampleTestingType.FirstOrDefault(o => o.Name.Trim() == "Oral Fluid");
                    if (objOralFluid != null)
                    {
                        var objSampleTesting = new ReferralSampleTestingType
                        {
                            SampleTestingTypeId = objOralFluid.Id
                        };
                        entity.ReferralSampleTestingTypes.Add(objSampleTesting);
                    }
                }
                if (viewModel.TestingInfo.CheckBlood)
                {
                    var objBlood = objListSampleTestingType.FirstOrDefault(o => o.Name.Trim() == "Blood");
                    if (objBlood != null)
                    {
                        var objSampleTesting = new ReferralSampleTestingType
                        {
                            SampleTestingTypeId = objBlood.Id
                        };
                        entity.ReferralSampleTestingTypes.Add(objSampleTesting);
                    }
                }
                if (viewModel.TestingInfo.CheckHair)
                {
                    var objHair = objListSampleTestingType.FirstOrDefault(o => o.Name.Trim() == "Hair");
                    if (objHair != null)
                    {
                        var objSampleTesting = new ReferralSampleTestingType
                        {
                            SampleTestingTypeId = objHair.Id
                        };
                        entity.ReferralSampleTestingTypes.Add(objSampleTesting);
                    }
                }
                // Update info
                if (userChangeCollectionSite)
                {
                    if (viewModel.TestingInfo.CollectionMethodId == 1)
                    {
                        var objCollectionSiteUpdate = mappedEntity.ReferralCollectionSites.FirstOrDefault();
                        if (objCollectionSiteUpdate == null)
                        {
                            var objAddItem = new ReferralCollectionSite
                            {
                                CollectionSiteId = viewModel.TestingInfo.CollectionSiteId,
                                SpecialInstructions =
                                    viewModel.TestingInfo.CollectionSiteSpecialInstructions,
                                CollectionDate = viewModel.TestingInfo.CollectionSiteDate,
                                Phone = viewModel.TestingInfo.CollectionSitePhone,
                                Fax = viewModel.TestingInfo.CollectionSiteFax,
                                Address = viewModel.TestingInfo.CollectionSiteAddress,
                            };
                             mappedEntity.ReferralCollectionSites.Add(objAddItem);
                        }
                        else
                        {
                            objCollectionSiteUpdate.CollectionSiteId = viewModel.TestingInfo.CollectionSiteId;
                            objCollectionSiteUpdate.SpecialInstructions =
                                viewModel.TestingInfo.CollectionSiteSpecialInstructions;
                            objCollectionSiteUpdate.CollectionDate = viewModel.TestingInfo.CollectionSiteDate;
                            objCollectionSiteUpdate.Phone = viewModel.TestingInfo.CollectionSitePhone;
                            objCollectionSiteUpdate.Fax = viewModel.TestingInfo.CollectionSiteFax;
                            objCollectionSiteUpdate.Address = viewModel.TestingInfo.CollectionSiteAddress;
                        }
                        
                    }
                    else
                    {
                        var objTreatingPhysicianUpdate = mappedEntity.ReferralNpiNumbers.FirstOrDefault();
                        if (objTreatingPhysicianUpdate == null)
                        {
                            var objAddItem = new ReferralNpiNumber
                            {
                                NpiNumberId = viewModel.TestingInfo.TreatingPhysicianId,
                                NextMdVisitDate = viewModel.TestingInfo.NextMdVisitDate,
                                SpecialHandling = viewModel.TestingInfo.TreatingPhysicianSpecialHandling,
                                Phone = viewModel.TestingInfo.TreatingPhysicianPhone,
                                Fax = viewModel.TestingInfo.TreatingPhysicianFax,
                                Address = viewModel.TestingInfo.TreatingPhysicianAddress,
                                Email = viewModel.TestingInfo.TreatingPhysicianEmail,
                            };
                            mappedEntity.ReferralNpiNumbers.Add(objAddItem);
                        }
                        else
                        {
                            objTreatingPhysicianUpdate.NpiNumberId = viewModel.TestingInfo.TreatingPhysicianId;
                            objTreatingPhysicianUpdate.NextMdVisitDate = viewModel.TestingInfo.NextMdVisitDate;
                            objTreatingPhysicianUpdate.SpecialHandling = viewModel.TestingInfo.TreatingPhysicianSpecialHandling;
                            objTreatingPhysicianUpdate.Phone = viewModel.TestingInfo.TreatingPhysicianPhone;
                            objTreatingPhysicianUpdate.Fax = viewModel.TestingInfo.TreatingPhysicianFax;
                            objTreatingPhysicianUpdate.Address = viewModel.TestingInfo.TreatingPhysicianAddress;
                            objTreatingPhysicianUpdate.Email = viewModel.TestingInfo.TreatingPhysicianEmail;
                        }
                       
                    }
                }


                // Medication history
                var checkUserChangeMedicationHistMethod = CheckUserChangeMedicationHist(noMedicationHistOld,
                                                                    mappedEntity.NoMedicationHistory.GetValueOrDefault(),
                                                                    mappedEntity.ReferralMedicationHistories.ToList(),
                                                                    viewModel.MedicationHistories);
                if (checkUserChangeMedicationHistMethod)
                {
                    // Delete all old
                    foreach (var oldItem in mappedEntity.ReferralMedicationHistories)
                    {
                        oldItem.IsDeleted = true;
                    }
                    if (!mappedEntity.NoMedicationHistory.GetValueOrDefault())
                    {
                        mappedEntity.ReferralMedicationHistories.AddRange(viewModel.MedicationHistories);
                    }
                }

                // Attachment Info
                _fileAttachmentService.TransferUploadPath = TransferUploadPath;

                foreach (var attachmentItem in viewModel.Attachments)
                {

                    if (!attachmentItem.IsDeleted)
                    {
                        var fileContent = _fileAttachmentService.ReadFile(attachmentItem.AttachedFileName,
                                                                             attachmentItem.RowGUID);
                        if (fileContent != null)
                        {
                            attachmentItem.AttachedFileContent = fileContent;
                        }
                    }
                    else
                    {
                        // Delete all old
                        var oldItem =
                            mappedEntity.ReferralAttachments.FirstOrDefault(o => o.RowGUID == attachmentItem.RowGUID);
                        if (oldItem != null)
                        {
                            oldItem.IsDeleted = true;
                        }
                    }
                }
                // Remove all item attachment which is delete
                var listAttachmentsAdd = viewModel.Attachments.Where(o => !o.IsDeleted).ToList();
                
                mappedEntity.ReferralAttachments.AddRange(listAttachmentsAdd);
                // Check for update with cancel status( if user change status to cancel => user must to fill in cancel reason
                if (mappedEntity.StatusId == 2 && oldStatusId!=2)
                {
                    var validationResult = new List<ValidationResult>();
                    var failed = false;
                    var objCancelData = Session["ReferralCancelData"] as ReferralCancelViewModel;
                    if (objCancelData == null)
                    {
                        var mess = string.Format(SystemMessageLookup.GetMessage("UserMustChooseCancelReason"));
                        validationResult.Add(new ValidationResult(mess));
                        failed = true;
                    }
                    else
                    {
                        if (objCancelData.ReasonId.GetValueOrDefault() == 0)
                        {
                            var mess = string.Format(SystemMessageLookup.GetMessage("RequiredTextResourceKey"), "Reason For Cancel");
                            validationResult.Add(new ValidationResult(mess));
                            failed = true;
                        }
                        if (string.IsNullOrEmpty(objCancelData.Comment))
                        {
                            var mess = string.Format(SystemMessageLookup.GetMessage("RequiredTextResourceKey"), "Comment For Cancel");
                            validationResult.Add(new ValidationResult(mess));
                            failed = true;
                        }
                    }
                    var result = new BusinessRuleResult(failed, "", "ReferralCancel", 0, null, "ReferralCancelRule") { ValidationResults = validationResult };
                    if (failed)
                    {
                        // Give messages on every rule that failed
                        throw new BusinessRuleException("BussinessGenericErrorMessageKey", new[] { result });
                    }
                    mappedEntity.ReferralCancels.Add(new ReferralCancel
                    {
                        Comment=objCancelData.Comment,
                        Reason=XmlDataHelpper.Instance.GetValue(XmlDataTypeEnum.CancelReason.ToString(), objCancelData.ReasonId.ToString()),
                    });
                }
                lastModified = _referralService.Update(mappedEntity).LastModified;
                // Delete file in folder
                foreach (var attachmentItem in viewModel.Attachments)
                {
                    if (!attachmentItem.IsDeleted)
                    {
                        _fileAttachmentService.RemoveFile(attachmentItem.AttachedFileName, attachmentItem.RowGUID);
                    }
                }
            }

            return Json(new { Error = string.Empty, Data = new { LastModified = lastModified } }, JsonRequestBehavior.AllowGet);
        }

        [AuthorizeContext(DocumentTypeKey = DocumentTypeKey.Referral, OperationAction = OperationAction.View)]
        public JsonResult GetLookup(LookupQuery queryInfo)
        {
            var selector = new Func<Referral, LookupItemVo>(o => new LookupItemVo
            {
                KeyId = o.Id,
                DisplayName = o.Id + "-" + (o.ProductType == null ? "" : o.ProductType.Name)
            });
            return base.GetLookupForEntity(queryInfo, selector);
        }


        [AuthorizeContext(DocumentTypeKey = DocumentTypeKey.Referral, OperationAction = OperationAction.View)]
        public ActionResult ShowPopupCancel()
        {
            var model = new ReferralCancelViewModel
            {
                
            };
            return View(model);
        }

        [HttpPost]
        [AuthorizeContext(DocumentTypeKey = DocumentTypeKey.Referral, OperationAction = OperationAction.Update)]
        public JsonResult UpdateReferralCancel(string referralCancel)
        {
            var cancelData = JsonConvert.DeserializeObject<ReferralCancelViewModel>(referralCancel);
            var validationResult = new List<ValidationResult>();
            var failed = false;
            if (cancelData.ReasonId.GetValueOrDefault() == 0)
            {
                var mess = string.Format(SystemMessageLookup.GetMessage("RequiredTextResourceKey"), "Reason");
                validationResult.Add(new ValidationResult(mess));
                failed = true;
            }
            if (string.IsNullOrEmpty(cancelData.Comment))
            {
                var mess = string.Format(SystemMessageLookup.GetMessage("RequiredTextResourceKey"), "Comment");
                validationResult.Add(new ValidationResult(mess));
                failed = true;
            }
            if (failed)
            {
                var result = new BusinessRuleResult(true, "", "ReferralCancel", 0, null, "ReferralCancelRule") { ValidationResults = validationResult };
                throw new BusinessRuleException("BussinessGenericErrorMessageKey", new[] { result });
            }
            Session["ReferralCancelData"] = cancelData;
            return Json("1", JsonRequestBehavior.AllowGet);
        }

        [AuthorizeContext(DocumentTypeKey = DocumentTypeKey.Referral, OperationAction = OperationAction.View)]
        public ActionResult ShowPopupAssignToReferral(string typeWithUser,string selectedRowIdArray, string isSelectAll)
        {
            var isSelectAllValue = isSelectAll == "1";
            var listIdSelected = new List<int>();
            if (!string.IsNullOrEmpty(selectedRowIdArray))
            {
                var strArrayId = selectedRowIdArray.Split(',');
                foreach (var idStr in strArrayId)
                {
                    int idAdd;
                    int.TryParse(idStr, out idAdd);
                    if (idAdd != 0)
                    {
                        listIdSelected.Add(idAdd);
                    }
                }
            }
            var model = new ReferralAssignToViewModel
            {
                ListReferralIdSelected = listIdSelected,
                IsSelectAll = isSelectAllValue,
                ListReferralIdSelectedString = selectedRowIdArray,
                TypeWithUser = typeWithUser
            };
            return View(model);
        }
        [HttpPost]
        [AuthorizeContext(DocumentTypeKey = DocumentTypeKey.Referral, OperationAction = OperationAction.Update)]
        public JsonResult UpdateReferralAssignTo(string referralAssignTo)
        {
            var referralAssignToData = JsonConvert.DeserializeObject<ReferralAssignToViewModel>(referralAssignTo);
           
            var listIdSelected = new List<int>();
            if (!string.IsNullOrEmpty(referralAssignToData.ListReferralIdSelectedString))
            {
                var strArrayId = referralAssignToData.ListReferralIdSelectedString.Split(',');
                foreach (var idStr in strArrayId)
                {
                    int idAdd;
                    int.TryParse(idStr, out idAdd);
                    if (idAdd != 0)
                    {
                        listIdSelected.Add(idAdd);
                    }
                }
            }
            _referralService.AssignToForReferral(listIdSelected, referralAssignToData.IsSelectAll,
                referralAssignToData.AssignToId,referralAssignToData.TypeWithUser);

            return Json("1", JsonRequestBehavior.AllowGet);
        }

        [AuthorizeContext(DocumentTypeKey = DocumentTypeKey.Referral, OperationAction = OperationAction.View)]
        public ActionResult ShowPopupPrintReferral(string selectedRowIdArray)
        {
            var model = new ReferralPrintViewModel
            {
                ListReferralIdSelectedString = selectedRowIdArray,
                CurrentUserId = AuthenticationService.GetCurrentUser().User.Id
            };
            return View(model);
        }
        [HttpGet]
        [AuthorizeContext(DocumentTypeKey = DocumentTypeKey.Referral, OperationAction = OperationAction.View)]
        public JsonResult GetDataPrintToReferral( string selectedRowIdArray,ReferralQueryInfo queryInfo)
        {
            var listIdSelected = new List<int>();
            if (!string.IsNullOrEmpty(selectedRowIdArray))
            {
                var strArrayId = selectedRowIdArray.Split(',');
                foreach (var idStr in strArrayId)
                {
                    int idAdd;
                    int.TryParse(idStr, out idAdd);
                    if (idAdd != 0)
                    {
                        listIdSelected.Add(idAdd);
                    }
                }
            }
            var data = _referralService.GetDataPrintToReferral(listIdSelected, queryInfo);
            return Json(data, JsonRequestBehavior.AllowGet);
        }
    }
}