﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using Framework.DomainModel.Entities.Common;
using Framework.Exceptions;
using Framework.Service.Translation;
using Newtonsoft.Json;
using WorkComp.Models.Referral;

namespace WorkComp.Controllers
{
    public partial class ReferralController
    {

        private static void ProcessForReferralInfo(DashboardReferralDataViewModel viewModel, string referralInfo)
        {
            var referralInfoData = JsonConvert.DeserializeObject<ReferralInfoPartialForReferral>(referralInfo);
            CheckReferralInfoValid(referralInfoData,viewModel.SharedViewModel.Id);
            viewModel.ReferralInfo.EnteredDate = referralInfoData.EnteredDate;
            viewModel.ReferralInfo.ReceivedDate = referralInfoData.ReceivedDate;
            viewModel.ReferralInfo.DueDate = referralInfoData.DueDate;
            viewModel.ReferralInfo.StatusId = referralInfoData.StatusId;
            viewModel.ReferralInfo.AssignToId = referralInfoData.AssignToId;
            viewModel.ReferralInfo.ReferralMethodId = referralInfoData.ReferralMethodId == 0 ? null : referralInfoData.ReferralMethodId;
            viewModel.ReferralInfo.AttorneyId = referralInfoData.AttorneyId;
            viewModel.ReferralInfo.SpecialInstruction = referralInfoData.SpecialInstruction;
            viewModel.ReferralInfo.ControlNumber = referralInfoData.ControlNumber;
            viewModel.ReferralInfo.Rush = referralInfoData.Rush;
        }
        private static void CheckReferralInfoValid(ReferralInfoPartialForReferral referralInfo, int referralId)
        {
            if (referralInfo==null)
            {
                return;
            }
            var failed = false;
            var validationResult = new List<ValidationResult>();
            if (referralInfo.EnteredDate == null)
            {
                var mess = string.Format(SystemMessageLookup.GetMessage("RequiredTextResourceKey"), "Entered Date");
                validationResult.Add(new ValidationResult(mess));
                failed = true;
            }
            if (referralInfo.ReceivedDate == null)
            {
                var mess = string.Format(SystemMessageLookup.GetMessage("RequiredTextResourceKey"), "Received Date");
                validationResult.Add(new ValidationResult(mess));
                failed = true;
            }
            if (referralInfo.DueDate == null)
            {
                var mess = string.Format(SystemMessageLookup.GetMessage("RequiredTextResourceKey"), "Follow Up Date");
                validationResult.Add(new ValidationResult(mess));
                failed = true;
            }
            if (referralId == 0 && referralInfo.DueDate.GetValueOrDefault().Date < DateTime.Now.Date)
            {
                var mess = string.Format(SystemMessageLookup.GetMessage("CannotGreaterThanText"), "Current Date", "Referral Follow Up Date");
                validationResult.Add(new ValidationResult(mess));
                failed = true;
            }
            if (referralInfo.ReceivedDate.GetValueOrDefault() > referralInfo.DueDate.GetValueOrDefault())
            {
                var mess = string.Format(SystemMessageLookup.GetMessage("CannotGreaterThanText"), "Referral Recieved Date", "Referral Follow Up Date");
                validationResult.Add(new ValidationResult(mess));
                failed = true;
            }
            var result = new BusinessRuleResult(failed, "", "ReferralTask", 0, null, "ReferralTaskRule") { ValidationResults = validationResult };
            if (failed)
            {
                // Give messages on every rule that failed
                throw new BusinessRuleException("BussinessGenericErrorMessageKey", new[] { result });
            }
        }
    }
}