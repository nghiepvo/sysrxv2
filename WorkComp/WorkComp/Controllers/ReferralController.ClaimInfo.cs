﻿using System.Web.Mvc;
using Framework.DomainModel.Entities.Common;
using Framework.DomainModel.Entities.Security;
using Framework.DomainModel.ValueObject;
using Newtonsoft.Json;
using ServiceLayer.Interfaces;
using WorkComp.Attributes;
using WorkComp.Models.Referral;

namespace WorkComp.Controllers
{
    public partial class ReferralController
    {
        private readonly IReferralSourceService _referralSourceService;
        private readonly IClaimNumberService _claimNumberService;
        private readonly IPayerService _payerService;
        private readonly IAdjusterService _adjusterService;
        private readonly IClaimantService _claimantService;
        private readonly ICaseManagerService _caseManagerService;
        private readonly IAttorneyService _attorneyService;
        private readonly IEmployerService _employerService;
        private static void ProcessForClaimInfo(DashboardReferralDataViewModel viewModel, string claimInfo)
        {
            var claimInfoData = JsonConvert.DeserializeObject<ClaimInfoPartialForReferral>(claimInfo);
            viewModel.ClaimInfo.ReferralSourceId = claimInfoData.ReferralSourceId;
            viewModel.ClaimInfo.AdjusterIsReferral = claimInfoData.AdjusterIsReferral;
            viewModel.ClaimInfo.ClaimNumberId = claimInfoData.ClaimNumberId;
            viewModel.ClaimInfo.PayerId = claimInfoData.PayerId;
            viewModel.ClaimInfo.BranchId = claimInfoData.BranchId;
            viewModel.ClaimInfo.AdjusterId = claimInfoData.AdjusterId;
            viewModel.ClaimInfo.ClaimantId = claimInfoData.ClaimantId;
            viewModel.ClaimInfo.CaseManagerId = claimInfoData.CaseManagerId;
            viewModel.ClaimInfo.EmployerId = claimInfoData.EmployerId;
            viewModel.ClaimInfo.ProductTypeId = claimInfoData.ProductTypeId;
        }

        [AuthorizeContext(DocumentTypeKey = DocumentTypeKey.ReferralSource, OperationAction = OperationAction.View)]
        public JsonResult GetInfoWhenChangeReferralSource(int? id)
        {
            if (id.GetValueOrDefault() == 0)
            {
                return Json("", JsonRequestBehavior.AllowGet);
            }
            var objData = _referralSourceService.GetInfoWhenChangeReferralSourceInReferral(id.GetValueOrDefault());
            return objData != null ? Json(objData, JsonRequestBehavior.AllowGet) : Json("", JsonRequestBehavior.AllowGet);
        }

        [AuthorizeContext(DocumentTypeKey = DocumentTypeKey.ClaimNumber, OperationAction = OperationAction.View)]
        public JsonResult GetInfoWhenChangeClaimNumber(int? id)
        {
            if (id.GetValueOrDefault() == 0)
            {
                return Json("", JsonRequestBehavior.AllowGet);
            }
            var objData = _claimNumberService.GetInfoWhenChangeClaimNumberInReferral(id.GetValueOrDefault());
            return objData != null ? Json(objData, JsonRequestBehavior.AllowGet) : Json("", JsonRequestBehavior.AllowGet);
        }

        [AuthorizeContext(DocumentTypeKey = DocumentTypeKey.Payer, OperationAction = OperationAction.View)]
        public JsonResult GetInfoWhenChangePayer(int? id)
        {
            if (id.GetValueOrDefault() == 0)
            {
                return Json("", JsonRequestBehavior.AllowGet);
            }
            var objPayer = _payerService.GetById(id.GetValueOrDefault());
            if (objPayer == null)
            {
                return Json("", JsonRequestBehavior.AllowGet);
            }
            var objData = new InfoWhenChangePayerInReferral
            {
                PayerId = objPayer.Id,
                PayerInstruction = objPayer.SpecialInstructions
            };
            return Json(objData, JsonRequestBehavior.AllowGet);
        }

        [AuthorizeContext(DocumentTypeKey = DocumentTypeKey.Adjuster, OperationAction = OperationAction.View)]
        public JsonResult GetInfoWhenChangeAdjuster(int? id)
        {
            if (id.GetValueOrDefault() == 0)
            {
                return Json("", JsonRequestBehavior.AllowGet);
            }
            var objData = _adjusterService.GetInfoWhenChangeAdjusterInReferral(id.GetValueOrDefault());
            return objData != null ? Json(objData, JsonRequestBehavior.AllowGet) : Json("", JsonRequestBehavior.AllowGet);
        }

        [AuthorizeContext(DocumentTypeKey = DocumentTypeKey.Adjuster, OperationAction = OperationAction.View)]
        public JsonResult GetAdjusterWhenAdjusterIdIsNull(int? id)
        {
            var adjusterId = 0;
            var objClaimNumber = _claimNumberService.FirstOrDefault(o => o.Id == id);
            if (objClaimNumber != null)
            {
                adjusterId = objClaimNumber.AdjusterId.GetValueOrDefault();
            }

            if (adjusterId == 0)
            {
                return Json("", JsonRequestBehavior.AllowGet);
            }

            var objData = _adjusterService.GetInfoWhenChangeAdjusterInReferral(adjusterId);
            return objData != null ? Json(objData, JsonRequestBehavior.AllowGet) : Json("", JsonRequestBehavior.AllowGet);
        }
        [AuthorizeContext(DocumentTypeKey = DocumentTypeKey.Claimant, OperationAction = OperationAction.View)]
        public JsonResult GetInfoWhenChangeClaimant(int? id)
        {
            if (id.GetValueOrDefault() == 0)
            {
                return Json("", JsonRequestBehavior.AllowGet);
            }
            var objData = _claimantService.GetInfoWhenChangeClaimantInReferral(id.GetValueOrDefault());
            return objData != null ? Json(objData, JsonRequestBehavior.AllowGet) : Json("", JsonRequestBehavior.AllowGet);
        }

        [AuthorizeContext(DocumentTypeKey = DocumentTypeKey.CaseManager, OperationAction = OperationAction.View)]
        public JsonResult GetInfoWhenChangeCaseManager(int? id)
        {
            if (id.GetValueOrDefault() == 0)
            {
                return Json("", JsonRequestBehavior.AllowGet);
            }
            var objData = _caseManagerService.GetInfoWhenChangeCaseManagerInReferral(id.GetValueOrDefault());
            return objData != null ? Json(objData, JsonRequestBehavior.AllowGet) : Json("", JsonRequestBehavior.AllowGet);
        }
        [AuthorizeContext(DocumentTypeKey = DocumentTypeKey.Attorney, OperationAction = OperationAction.View)]
        public JsonResult GetInfoWhenChangeAttorney(int? id)
        {
            if (id.GetValueOrDefault() == 0)
            {
                return Json("", JsonRequestBehavior.AllowGet);
            }
            var objData = _attorneyService.GetInfoWhenChangeAttorneyInReferral(id.GetValueOrDefault());
            return objData != null ? Json(objData, JsonRequestBehavior.AllowGet) : Json("", JsonRequestBehavior.AllowGet);
        }

        [AuthorizeContext(DocumentTypeKey = DocumentTypeKey.Employer, OperationAction = OperationAction.View)]
        public JsonResult GetInfoWhenChangeEmployer(int? id)
        {
            if (id.GetValueOrDefault() == 0)
            {
                return Json("", JsonRequestBehavior.AllowGet);
            }
            var objData = _employerService.GetInfoWhenChangeEmployerInReferral(id.GetValueOrDefault());
            return objData != null ? Json(objData, JsonRequestBehavior.AllowGet) : Json("", JsonRequestBehavior.AllowGet);
        }
    }
}