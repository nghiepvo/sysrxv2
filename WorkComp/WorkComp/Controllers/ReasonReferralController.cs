﻿using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.Dynamic;
using System.Linq;
using System.Web.Mvc;
using Framework.DomainModel.Entities;
using Framework.DomainModel.Entities.Common;
using Framework.DomainModel.Entities.Security;
using Framework.DomainModel.ValueObject;
using Framework.Service.Diagnostics;
using Framework.Service.Translation;
using Newtonsoft.Json;
using ServiceLayer.Interfaces;
using ServiceLayer.Interfaces.Authentication;
using WorkComp.Attributes;
using WorkComp.Models.ReasonReferral;

namespace WorkComp.Controllers
{
    public class ReasonReferralController : ApplicationControllerGeneric<ReasonReferral, DashboardReasonReferralDataViewModel>
    {
        //
        // GET: /State/
        private readonly IReasonReferralService _reasonReferralService;
        public ReasonReferralController(IAuthenticationService authenticationService, IDiagnosticService diagnosticService, IReasonReferralService reasonReferralService)
            : base(authenticationService, diagnosticService, reasonReferralService)
        {
            _reasonReferralService = reasonReferralService;
        }
        [AuthorizeContext(DocumentTypeKey = DocumentTypeKey.ReasonReferral, OperationAction = OperationAction.View)]
        public ActionResult Index()
        {
            var viewModel = new DashboardReasonReferralIndexViewModel();
            return View(viewModel);
        }

        #region Nghiep
        [HttpGet]
        [AuthorizeContext(DocumentTypeKey = DocumentTypeKey.ReasonReferral, OperationAction = OperationAction.View)]
        public JsonResult GetReasonReferral(int? parentId)
        {
            return Json(_reasonReferralService.GetReasonReferral(parentId), JsonRequestBehavior.AllowGet);
        }

        [HttpPost]
        [AuthorizeContext(DocumentTypeKey = DocumentTypeKey.ReasonReferral, OperationAction = OperationAction.Update)]
        public JsonResult Update(ReasonReferralParameter parameters)
        {
            return UpdateMasterFile(parameters);
        }

        [HttpPost]
        [AuthorizeContext(DocumentTypeKey = DocumentTypeKey.ReasonReferral, OperationAction = OperationAction.Delete)]
        public JsonResult Delete(string models)
        {
            var listItem = JsonConvert.DeserializeObject<Collection<dynamic>>(models);
            if (listItem.Count == 0)
            {
                return Json(false, JsonRequestBehavior.AllowGet);
            }
            var id = (int)(listItem[0].Id);
            var list = _reasonReferralService.Get(o => o.ParentId == id).ToList();
            if (list.Count > 0)
            {
                return Json(new { Error = string.Format(SystemMessageLookup.GetMessage("DeleteChildText"), "reason child") }, JsonRequestBehavior.AllowGet);
            }
            _reasonReferralService.DeleteById(id);
            return Json(true, JsonRequestBehavior.AllowGet);
        }

        [HttpPost]
        [AuthorizeContext(DocumentTypeKey = DocumentTypeKey.ReasonReferral, OperationAction = OperationAction.Update)]
        public JsonResult Active(string models)
        {
            var listItem = JsonConvert.DeserializeObject<Collection<dynamic>>(models);
            if (listItem.Count == 0)
            {
                return Json(false, JsonRequestBehavior.AllowGet);
            }
            var id = (int)(listItem[0].Id);
            return Json(_reasonReferralService.UpdateActiveReasonReferral(id), JsonRequestBehavior.AllowGet);
        }


        [HttpPost]
        [AuthorizeContext(DocumentTypeKey = DocumentTypeKey.ReasonReferral, OperationAction = OperationAction.Add)]
        public JsonResult Create(ReasonReferralParameter parameters)
        {
            CreateMasterFile(parameters);
            return Json(true);
        }

        #endregion

        [HttpGet]
        [AuthorizeContext(DocumentTypeKey = DocumentTypeKey.ReasonReferral, OperationAction = OperationAction.View)]
        public JsonResult GetSelectedReasonReferralByReferral(int? referralId)
        {
            var queryResult = _reasonReferralService.GetSelectedReasonReferralbyReferral(referralId.GetValueOrDefault()).Select(x => new
            {
                x.Id,
                DisplayName = x.Name,
                x.ParentId
            });
            return Json(new { Data = queryResult, TotalRowCount = queryResult.Count() }, JsonRequestBehavior.AllowGet);
        }

        [HttpGet]
        [AuthorizeContext(DocumentTypeKey = DocumentTypeKey.ReasonReferral, OperationAction = OperationAction.View)]
        public JsonResult GetAllReasonReferralByReferralForTreeView(int? referralId)
        {
            var listData = _reasonReferralService.GetUnselectedReasonReferralByReferral(referralId.GetValueOrDefault());
            var queryResult = new List<dynamic>();
            foreach (var parentItem in listData)
            {
                dynamic objAdd = new ExpandoObject();
                objAdd.Id = parentItem.Id;
                objAdd.ParentDisplayName = parentItem.Name;
                objAdd.Active = true;
                objAdd.expanded = true;
                GetChildItemForTreeView(objAdd, parentItem);
                queryResult.Add(objAdd);
            }
            return Json(queryResult, JsonRequestBehavior.AllowGet);
        }

        private void GetChildItemForTreeView(dynamic objAdd, ReasonReferralWithReferralVo parentItem)
        {
            if (parentItem.Childrent != null && parentItem.Childrent.Count != 0)
            {
                objAdd.childTypes = new List<ExpandoObject>();
                foreach (var childItem in parentItem.Childrent)
                {
                    dynamic objChildAdd = new ExpandoObject();
                    objChildAdd.Id = childItem.Id;
                    objChildAdd.Active = true;
                    objChildAdd.expanded = true;
                    if (childItem.Childrent != null && childItem.Childrent.Count != 0)
                    {
                        objChildAdd.ParentDisplayName = childItem.Name;
                        GetChildItemForTreeView(objChildAdd, childItem);
                    }
                    else
                    {
                        objChildAdd.ChildDisplayName = childItem.Name;
                    }
                    objAdd.childTypes.Add(objChildAdd);
                }
            }
        }

        [AuthorizeContext(DocumentTypeKey = DocumentTypeKey.ReasonReferral, OperationAction = OperationAction.View)]
        public JsonResult GetLookup(LookupQuery queryInfo)
        {
            var selector = new Func<ReasonReferral, LookupItemVo>(o => new LookupItemVo
            {
                KeyId = o.Id,
                DisplayName = o.Name
            });
            return base.GetLookupForEntity(queryInfo, selector);
        }

        [AuthorizeContext(DocumentTypeKey = DocumentTypeKey.ReasonReferral, OperationAction = OperationAction.View)]
        public JsonResult GetLookupItem(LookupItem lookupItem)
        {
            var selector = new Func<ReasonReferral, LookupItemVo>(o => new LookupItemVo
            {
                KeyId = o.Id,
                DisplayName = o.Name
            });
            return base.GetLookupItemForEntity(lookupItem, selector);
        }

        [HttpGet]
        [AuthorizeContext(DocumentTypeKey = DocumentTypeKey.ReasonReferral, OperationAction = OperationAction.View)]
        public JsonResult GetListReasonReferral()
        {
            var queryData = _reasonReferralService.GetListReasonReferral();
            var clientsJson = Json(queryData, JsonRequestBehavior.AllowGet);
            return clientsJson;
        }
    }
}
