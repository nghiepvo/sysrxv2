﻿using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.Web.Mvc;
using Framework.DomainModel.Entities;
using Framework.DomainModel.Entities.Common;
using Framework.DomainModel.Entities.Security;
using Framework.DomainModel.ValueObject;
using Framework.Service.Diagnostics;
using Newtonsoft.Json;
using ServiceLayer.Interfaces;
using ServiceLayer.Interfaces.Authentication;
using WorkComp.Attributes;
using WorkComp.Models;
using WorkComp.Models.CollectionSite;

namespace WorkComp.Controllers
{
    public class CollectionSiteController : ApplicationControllerGeneric<CollectionSite, DashboardCollectionSiteDataViewModel>
    {
        //
        // GET: /State/
        private readonly ICollectionSiteService _collectionSiteService;
        public CollectionSiteController(IAuthenticationService authenticationService, IDiagnosticService diagnosticService, ICollectionSiteService collectionSiteService)
            : base(authenticationService, diagnosticService, collectionSiteService)
        {
            _collectionSiteService = collectionSiteService;
        }
        [AuthorizeContext(DocumentTypeKey = DocumentTypeKey.CollectionSite, OperationAction = OperationAction.View)]
        public ActionResult Index()
        {
            var viewModel = new DashboardCollectionSiteIndexViewModel();
            Func<GridViewModel> gridViewModel = () => new GridViewModel
            {
                GridId = "CollectionSiteGrid",
                ModelName = "CollectionSite",
                UseCheckBoxColumn = true,
                UseDeleteColumn = true,
                WidthPopup = 800,
                HeightPopup = 600
            };

            viewModel.GridViewModel = BuildGridViewModel(gridViewModel);
            return View(viewModel);
        }

        [HttpGet]
        [AuthorizeContext(DocumentTypeKey = DocumentTypeKey.CollectionSite, OperationAction = OperationAction.View)]
        public override JsonResult GetDataForGrid(QueryInfo queryInfo)
        {
            return base.GetDataForGrid(queryInfo);
        }

        [AuthorizeContext(DocumentTypeKey = DocumentTypeKey.CollectionSite, OperationAction = OperationAction.Add)]
        public ActionResult Create()
        {
            var viewModel = new DashboardCollectionSiteDataViewModel
            {
                SharedViewModel = new DashboardCollectionSiteShareViewModel
                {
                    Contracted = true,
                    CreateMode = true
                }
            };
            // Check show popup for create from dropdown
            var type = Request.QueryString["type"];
            if (type == "1")
            {
                viewModel.SharedViewModel.ShowPopupType = ShowPopupType.ComboBox;
            }
            return View(viewModel);
        }

        [HttpPost]
        [AuthorizeContext(DocumentTypeKey = DocumentTypeKey.CollectionSite, OperationAction = OperationAction.Add)]
        public int Create(CollectionSiteParameter parameters)
        {
            var savedEntity = CreateMasterFile(parameters);
            return savedEntity.Id;
        }

        [AuthorizeContext(DocumentTypeKey = DocumentTypeKey.CollectionSite, OperationAction = OperationAction.Update)]
        public ActionResult Update(int id)
        {
            var viewModel = GetMasterFileViewModel(id);
            // Check show popup for create from dropdown
            var type = Request.QueryString["type"];
            if (type == "1")
            {
                viewModel.SharedViewModel.ShowPopupType = ShowPopupType.ComboBox;
            }
            return View(viewModel);
        }

        [HttpPost]
        [AuthorizeContext(DocumentTypeKey = DocumentTypeKey.CollectionSite, OperationAction = OperationAction.Update)]
        public ActionResult Update(CollectionSiteParameter parameters)
        {
            return UpdateMasterFile(parameters);
        }

        [AuthorizeContext(DocumentTypeKey = DocumentTypeKey.CollectionSite, OperationAction = OperationAction.Add)]
        public ActionResult Copy(int id)
        {
            var viewModel = GetMasterFileViewModel(id);
            viewModel.SharedViewModel.CreateMode = true;
            viewModel.SharedViewModel.Id = 0;
            viewModel.Id = 0;
            return View("Create",viewModel);
        }

        [AuthorizeContext(DocumentTypeKey = DocumentTypeKey.CollectionSite, OperationAction = OperationAction.Delete)]
        public JsonResult Delete(string models)
        {
            var listItem = JsonConvert.DeserializeObject<Collection<dynamic>>(models);
            if (listItem.Count == 0)
            {
                return Json(false, JsonRequestBehavior.AllowGet);
            }
            var id = (int)(listItem[0].Id);
            _collectionSiteService.DeleteById(id);
            return Json(true, JsonRequestBehavior.AllowGet);
        }

        [AuthorizeContext(DocumentTypeKey = DocumentTypeKey.CollectionSite, OperationAction = OperationAction.Delete)]
        public JsonResult DeleteMulti(string selectedRowIdArray, string isDeleteAll)
        {
            return DeleteMultiMasterfile(selectedRowIdArray, isDeleteAll);
        }

        [AuthorizeContext(DocumentTypeKey = DocumentTypeKey.CollectionSite, OperationAction = OperationAction.View)]
        public JsonResult GetLookup(LookupQuery queryInfo)
        {
            var selector = new Func<CollectionSite, LookupItemVo>(o => new LookupItemVo
            {
                KeyId = o.Id,
                DisplayName = o.Name
            });
            return base.GetLookupForEntity(queryInfo, selector);
        }

        [AuthorizeContext(DocumentTypeKey = DocumentTypeKey.CollectionSite, OperationAction = OperationAction.View)]
        public JsonResult ExportExcel(GridConfigViewModel gridConfig, QueryInfo queryInfo)
        {
            return base.ExportExcelMasterfile(gridConfig, queryInfo);
        }

        protected override IList<ViewColumnViewModel> GetViewColumns()
        {
            var objViewColumn = new List<ViewColumnViewModel>
            {
                
                new ViewColumnViewModel
                {
                    ColumnOrder = 1,
                    ColumnWidth = 250,
                    Name = "Name",
                    Text = "Name",
                    ColumnJustification = GridColumnJustification.Left
                },
                new ViewColumnViewModel
                {
                    ColumnOrder = 2,
                    ColumnWidth = 250,
                    Name = "Phone",
                    Text = "Phone",
                    ColumnJustification = GridColumnJustification.Left
                },
                new ViewColumnViewModel
                {
                    ColumnOrder = 3,
                    Name = "Address",
                    Text = "Address",
                    ColumnWidth = 300,
                    ColumnJustification = GridColumnJustification.Left
                },
                new ViewColumnViewModel
                {
                    ColumnOrder = 4,
                    Name = "State",
                    Text = "State",
                    ColumnWidth = 100,
                    ColumnJustification = GridColumnJustification.Left
                },
                new ViewColumnViewModel
                {
                    ColumnOrder = 5,
                    Name = "City",
                    Text = "City",
                    ColumnWidth = 100,
                    ColumnJustification = GridColumnJustification.Left
                },
                new ViewColumnViewModel
                {
                    ColumnOrder = 6,
                    Name = "Zip",
                    Text = "Zip",
                    ColumnWidth = 100,
                    ColumnJustification = GridColumnJustification.Left
                }
            };
            return objViewColumn;
        }

    }
}
