﻿using System.IO;
using System.Web;
using System.Web.Mvc;
using Framework.Service.Diagnostics;
using ServiceLayer.Interfaces.Authentication;

namespace WorkComp.Controllers
{
    public class FileAttachmentController : ApplicationControllerBase
    {
        private readonly IDiagnosticService _diagnosticService;
        private readonly IAuthenticationService _authenticationService;
        public FileAttachmentController(IAuthenticationService authenticationService,
            IDiagnosticService diagnosticService
            )
            : base(authenticationService,diagnosticService,null)
        {
            _authenticationService = authenticationService;
            _diagnosticService = diagnosticService;
        }

        [HttpPost]
        public JsonResult UploadAttachment(HttpPostedFileBase file)
        {
            // Upload to temp folder
            string fileName = file.FileName;
            var tempPath = Path.GetTempPath() + fileName;
            if (System.IO.File.Exists(tempPath))
            {
                System.IO.File.Delete(tempPath);
            }
            file.SaveAs(tempPath);

            return Json(fileName, JsonRequestBehavior.AllowGet);
        }
        [HttpPost]
        public JsonResult RemoveAttachment(string fileName)
        {
            var tempPath = Path.GetTempPath() + fileName;
            if (System.IO.File.Exists(tempPath))
            {
                System.IO.File.Delete(tempPath);
            }
            return Json(true, JsonRequestBehavior.AllowGet);
        }
    }
}