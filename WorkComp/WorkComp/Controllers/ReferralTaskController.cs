﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Net;
using System.Text;
using System.Web.Mvc;
using Common;
using Framework.DomainModel.Entities;
using Framework.DomainModel.Entities.Common;
using Framework.DomainModel.Entities.Security;
using Framework.Exceptions;
using Framework.Mapping;
using Framework.Service.Diagnostics;
using Framework.Service.Translation;
using Framework.Utility;
using Newtonsoft.Json;
using ServiceLayer.Interfaces;
using ServiceLayer.Interfaces.Authentication;
using WorkComp.Attributes;
using WorkComp.Models;
using WorkComp.Models.Referral;
using WorkComp.Models.ReferralTask;
using WorkComp.Models.SendMailWithDualGrid;

namespace WorkComp.Controllers
{
    public class ReferralTaskController : ApplicationControllerGeneric<ReferralTask, DashboardReferralTaskDataViewModel>
    {
        private readonly IReferralTaskService _referralTaskService;
        private readonly IUserService _userService;
        private readonly IEmailTemplateService _emailTemplateService;
        private readonly IDiaryService _diaryService;
        
        public ReferralTaskController(IAuthenticationService authenticationService, IDiagnosticService diagnosticService, IReferralTaskService referralTaskService, IUserService userService, IEmailTemplateService emailTemplateService, IDiaryService diaryService)
            : base(authenticationService, diagnosticService, referralTaskService)
        {
            _referralTaskService = referralTaskService;
            _userService = userService;
            _emailTemplateService = emailTemplateService;
            _diaryService = diaryService;
        }

        [AuthorizeContext(DocumentTypeKey = DocumentTypeKey.ReferralTask, OperationAction = OperationAction.View)]
        public ActionResult Index()
        {
            return ViewReferralTask(TypeWithUserQueryEnum.Current);
        }

        [AuthorizeContext(DocumentTypeKey = DocumentTypeKey.ReferralTask, OperationAction = OperationAction.View)]
        public ActionResult All()
        {
            return ViewReferralTask(TypeWithUserQueryEnum.All);
        }

        [AuthorizeContext(DocumentTypeKey = DocumentTypeKey.ReferralTask, OperationAction = OperationAction.View)]
        public ActionResult Other()
        {
            return ViewReferralTask(TypeWithUserQueryEnum.Other);
        }

        private ActionResult ViewReferralTask(TypeWithUserQueryEnum type)
        {
            var viewModel = new DashboardReferralTaskIndexViewModel();
            var filterType = new ReferralFilterItem
            {
                FilterType = ReferralFilterType.All
            };
            if (Session["ReferralTaskFilterType"] != null)
            {
                filterType = Session["ReferralTaskFilterType"] as ReferralFilterItem;
            }
            viewModel.FilterType = filterType;
            Func<GridViewModel> gridViewModel = () => new GridViewModel
            {
                GridId = "ReferralTaskGrid",
                ModelName = "ReferralTask",
                UseCheckBoxColumn = true,
                UseDeleteColumn = true,
                WidthPopup = 800,
                HeightPopup = 600,
                TypeWithUser = type
            };

            viewModel.GridViewModel = BuildGridViewModel(gridViewModel);
            return View("Index",viewModel);
        }

        [AuthorizeContext(DocumentTypeKey = DocumentTypeKey.ReferralTask, OperationAction = OperationAction.View)]
        public ActionResult Detail(int id)
        {
            var data = _referralTaskService.FirstOrDefault(o => o.Id == id);
            var viewModel = new DashboardReferralTaskDetailViewModel();
            if (data != null)
            {
                viewModel = data.MapTo<DashboardReferralTaskDetailViewModel>();
                var objUser = _userService.FirstOrDefault(o => o.Id == data.CreatedById);
                if (objUser != null)
                {
                    viewModel.CreatedBy = objUser.FirstName + " " +
                                          (string.IsNullOrEmpty(objUser.MiddleName) ? "" : objUser.MiddleName + " ") +
                                          objUser.LastName;
                }
               
                viewModel.NextId = _referralTaskService.GetNextReferralTaskId(id, viewModel.ReferralId);
                viewModel.PreviousId = _referralTaskService.GetPreviousReferralTaskId(id, viewModel.ReferralId);
            }
            return View(viewModel);
        }

        [HttpGet]
        [AuthorizeContext(DocumentTypeKey = DocumentTypeKey.ReferralTask, OperationAction = OperationAction.View)]
        public JsonResult GetDataParentForGrid(ReferralQueryInfo queryInfo)
        {
            var data = _referralTaskService.GetDataParentForGrid(queryInfo);
            Session["ReferralTaskFilterType"] = new ReferralFilterItem
            {
                FilterType = queryInfo.FilterType,
                DueDateFrom = queryInfo.DueDateFrom,
                DueDateTo = queryInfo.DueDateTo
            };
            return Json(data, JsonRequestBehavior.AllowGet);
        }

        [AuthorizeContext(DocumentTypeKey = DocumentTypeKey.ReferralTask, OperationAction = OperationAction.Add)]
        public ActionResult Create()
        {
            var viewModel = new DashboardReferralTaskDataViewModel
            {
                SharedViewModel = new DashboardReferralTaskShareViewModel
                {
                    CreateMode = true
                }
            };
            // Check show popup for create from dropdown
            var type = Request.QueryString["type"];
            if (type == "1")
            {
                viewModel.SharedViewModel.ShowPopupType = ShowPopupType.Grid;
            }
            return View(viewModel);
        }

        [HttpPost]
        [AuthorizeContext(DocumentTypeKey = DocumentTypeKey.ReferralTask, OperationAction = OperationAction.Add)]
        public int Create(ReferralTaskParameter parameters)
        {
            CreateProcessForTaskTemplateInfo(parameters.SharedParameter);
            return 1;
        }

        [AuthorizeContext(DocumentTypeKey = DocumentTypeKey.ReferralTask, OperationAction = OperationAction.Update)]
        public ActionResult Update(int id)
        {
            var viewModel = GetMasterFileViewModel(id);
            // Check show popup for create from dropdown
            var type = Request.QueryString["type"];
            if (type == "1")
            {
                viewModel.SharedViewModel.ShowPopupType = ShowPopupType.ComboBox;
            }
            return View(viewModel);
        }

        [HttpPost]
        [AuthorizeContext(DocumentTypeKey = DocumentTypeKey.ReferralTask, OperationAction = OperationAction.Update)]
        public ActionResult Update(ReferralTaskParameter parameters)
        {
            byte[] data = Convert.FromBase64String(parameters.SharedParameter);
            parameters.SharedParameter = Encoding.UTF8.GetString(data);

            return UpdateMasterFile(parameters);
        }
        [ChildActionOnly]
        public override JsonResult UpdateMasterFile(MasterfileParameter parameters, Action<DashboardReferralTaskDataViewModel> advanceMapping = null)
        {
            var viewModel = MapFromClientParameters(parameters);

            if (advanceMapping != null)
                advanceMapping.Invoke(viewModel);

            byte[] lastModified = null;

            if (ModelState.IsValid)
            {
                var entity = MasterFileService.GetById(viewModel.SharedViewModel.Id);
                var referralId = entity.ReferralId;
                var mappedEntity = viewModel.MapPropertiesToInstance(entity);
                lastModified = MasterFileService.Update(mappedEntity).LastModified;
                //Add diary two Referral
                if (entity.ReferralId != referralId)
                {
                    var objDiary = new Diary
                    {
                        Heading = "Move",
                        Reason = string.Format("Task Id: {0} moved", entity.Id),
                        Comment =
                            string.Format("It moved from Referral Id: {0} to Referral Id: {1}",
                                referralId, mappedEntity.ReferralId),
                        ReferralId = referralId,
                    };
                    _diaryService.Add(objDiary);
                    objDiary.ReferralId = entity.ReferralId;
                    _diaryService.Add(objDiary);
                }
                

            }

            return Json(new { Error = string.Empty, Data = new { LastModified = lastModified } }, JsonRequestBehavior.AllowGet);
        }

        private void CreateProcessForTaskTemplateInfo(string listTaskTemplate)
        {
            var jSettings = new JsonSerializerSettings()
            {
                Formatting = Formatting.Indented,
                DateTimeZoneHandling = DateTimeZoneHandling.Utc,
                Converters = new JsonConverter[] { new DefaultWrongFormatDeserialize() }
            };
            var data = JsonConvert.DeserializeObject<DashboardReferralTaskCreateViewModel>(listTaskTemplate, jSettings);
            //CheckTaskInfoValid(data);
            var listTask = data.MapTo<List<ReferralTask>>();
             CheckTaskInfoValid(data);
            _referralTaskService.AddMultiple(listTask, data.ReferralId.GetValueOrDefault());
        }

        private static void CheckTaskInfoValid(DashboardReferralTaskCreateViewModel data)
        {
            if (data.ReferralTaskList == null || data.ReferralTaskList.Count == 0)
            {
                return;
            }
            var failed = false;
            var validationResult = new List<ValidationResult>();

            if (data.ReferralId.GetValueOrDefault() == 0)
            {
                var mess = string.Format(SystemMessageLookup.GetMessage("RequiredTextResourceKey"), "Referral");
                validationResult.Add(new ValidationResult(mess));
                failed = true;
            }
            var result = new BusinessRuleResult(failed, "", "ReferralTask", 0, null, "ReferralTaskRule") { ValidationResults = validationResult };
            if (failed)
            {
                // Give messages on every rule that failed
                throw new BusinessRuleException("BussinessGenericErrorMessageKey", new[] { result });
            }
        }

        [HttpPost]
        [AuthorizeContext(DocumentTypeKey = DocumentTypeKey.ReferralTask, OperationAction = OperationAction.Delete)]
        public JsonResult CompletedTask(int id)
        {
            _referralTaskService.CompletedTask(id);
            return Json(true, JsonRequestBehavior.AllowGet);
        }

        [AuthorizeContext(DocumentTypeKey = DocumentTypeKey.ReferralTask, OperationAction = OperationAction.View)]
        public ActionResult ShowPopupAssignToTask(string typeWithUser, string selectedRowIdArray, string isSelectAll)
        {
            var isSelectAllValue = isSelectAll == "1";
            var listIdSelected = new List<int>();
            if (!string.IsNullOrEmpty(selectedRowIdArray))
            {
                var strArrayId = selectedRowIdArray.Split(',');
                foreach (var idStr in strArrayId)
                {
                    int idAdd;
                    int.TryParse(idStr, out idAdd);
                    if (idAdd != 0)
                    {
                        listIdSelected.Add(idAdd);
                    }
                }
            }
            var model = new ReferralTaskAssignToViewModel
            {
                ListReferralTaskIdSelected = listIdSelected,
                IsSelectAll = isSelectAllValue,
                ListReferralTaskIdSelectedString = selectedRowIdArray,
                TypeWithUser = typeWithUser
            };
            return View(model);
        }
        [HttpPost]
        [AuthorizeContext(DocumentTypeKey = DocumentTypeKey.ReferralTask, OperationAction = OperationAction.Update)]
        public JsonResult UpdateReferralTaskAssignTo(string referralTaskAssignTo)
        {
            var referralTaskAssignToData = JsonConvert.DeserializeObject<ReferralTaskAssignToViewModel>(referralTaskAssignTo);

            var listIdSelected = new List<int>();
            if (!string.IsNullOrEmpty(referralTaskAssignToData.ListReferralTaskIdSelectedString))
            {
                var strArrayId = referralTaskAssignToData.ListReferralTaskIdSelectedString.Split(',');
                foreach (var idStr in strArrayId)
                {
                    int idAdd;
                    int.TryParse(idStr, out idAdd);
                    if (idAdd != 0)
                    {
                        listIdSelected.Add(idAdd);
                    }
                }
            }
            _referralTaskService.AssignToForReferralTask(listIdSelected, referralTaskAssignToData.IsSelectAll,
                referralTaskAssignToData.AssignToId, referralTaskAssignToData.TypeWithUser);
            return Json("1", JsonRequestBehavior.AllowGet);
        }
        [AuthorizeContext(DocumentTypeKey = DocumentTypeKey.ReferralTask, OperationAction = OperationAction.View)]
        public ActionResult ShowPopupPrintTask(string selectedRowIdArray)
        {
            var model = new ReferralTaskPrintViewModel
            {
                ListReferralTaskIdSelectedString = selectedRowIdArray,
                CurrentUserId = AuthenticationService.GetCurrentUser().User.Id
            };
            return View(model);
        }
        [HttpGet]
        [AuthorizeContext(DocumentTypeKey = DocumentTypeKey.ReferralTask, OperationAction = OperationAction.View)]
        public JsonResult GetDataPrintToReferralTask(string selectedRowIdArray, ReferralQueryInfo queryInfo)
        {
            var listIdSelected = new List<int>();
            if (!string.IsNullOrEmpty(selectedRowIdArray))
            {
                var strArrayId = selectedRowIdArray.Split(',');
                foreach (var idStr in strArrayId)
                {
                    int idAdd;
                    int.TryParse(idStr, out idAdd);
                    if (idAdd != 0)
                    {
                        listIdSelected.Add(idAdd);
                    }
                }
            }
            var data = _referralTaskService.GetDataPrintToReferralTask(listIdSelected, queryInfo);
            return Json(data, JsonRequestBehavior.AllowGet);
        }

        [AuthorizeContext(DocumentTypeKey = DocumentTypeKey.ReferralTask, OperationAction = OperationAction.View)]
        public JsonResult SendMailResult(string dataJson)
        {
            var result = false;
            byte[] dataEncode = Convert.FromBase64String(dataJson);
            dataJson = Encoding.UTF8.GetString(dataEncode);

            var data = JsonConvert.DeserializeObject<ReferralResultSendMailViewModel>(dataJson);
            if (data != null)
            {
                var fromEmail = AppSettingsReader.GetValue("EmailFrom", typeof(String)) as string;
                var displayName = AppSettingsReader.GetValue("EmailFromDisplayName", typeof(String)) as string;
                var listTo = data.To.Split(';').Where(o => !string.IsNullOrEmpty(o)).ToArray();
                var listCc = data.Cc.Split(';').Where(o => !string.IsNullOrEmpty(o)).ToArray();
                if (data.ReferralId == 0)
                {
                    data.Message = data.Message + "<br>" + data.IncludeContent;
                }
                if (data.SecureEmail)
                {
                    data.Title = SystemMessageLookup.GetMessage("SecureEmail") + " " + data.Title;
                }
                result = Common.EmailHelper.SendEmail(fromEmail, listTo, data.Title, data.Message, true, displayName, null, listCc);

                return result ? Json(new { Data = SystemMessageLookup.GetMessage("SendEmailSuccess") }, JsonRequestBehavior.AllowGet) : Json(new { Error = SystemMessageLookup.GetMessage("SendEmailFailed") }, JsonRequestBehavior.AllowGet);

            }
            return Json(new { Error = SystemMessageLookup.GetMessage("SendEmailFailed") }, JsonRequestBehavior.AllowGet);
        }
        [HttpPost]
        [AuthorizeContext(DocumentTypeKey = DocumentTypeKey.ReferralTask, OperationAction = OperationAction.View)]
        public JsonResult SendMailFaxResult(ReferralTaskSendEmailFaxViewModel data)
        {
            var result = false;
            if (data != null)
            {
                if (data.Type == "Email")
                {
                    var fromEmail = AppSettingsReader.GetValue("EmailFrom", typeof(String)) as string;
                    var displayName = AppSettingsReader.GetValue("EmailFromDisplayName", typeof(String)) as string;
                    string[] listTo = null, listCc = null;
                    if (data.EmailTo != null && data.EmailCc != null)
                    {
                        listTo = data.EmailTo.Split(';').Where(o => !string.IsNullOrEmpty(o)).ToArray();
                        listCc = data.EmailCc.Split(';').Where(o => !string.IsNullOrEmpty(o)).ToArray();
                    }
                    

                    if (data.IsSecureEmail)
                    {
                        data.Subject = SystemMessageLookup.GetMessage("SecureEmail") + " " + data.Subject;
                    }
                    result = EmailHelper.SendEmail(fromEmail, listTo, data.Subject, data.ContentHtml, true, displayName, null, listCc);
                    if (result)
                    {
                        var dataReferralTask = _referralTaskService.GetById(data.ReferralTaskId);
                        if (dataReferralTask!= null)
                        {
                            result = _referralTaskService.UpdateComplateTask(dataReferralTask);
                        }
                    }
                    return result ? Json(new { Data = SystemMessageLookup.GetMessage("SendEmailSuccess") }, JsonRequestBehavior.AllowGet) : Json(new { Error = SystemMessageLookup.GetMessage("SendEmailFailed") }, JsonRequestBehavior.AllowGet);
                }
                else
                {
                    result = false;
                    //Implement send Fax
                    //EmailHelper.SendFax()
                    return result ? Json(new { Data = SystemMessageLookup.GetMessage("SendEmailSuccess") }, JsonRequestBehavior.AllowGet) : Json(new { Error = SystemMessageLookup.GetMessage("SendEmailFailed") }, JsonRequestBehavior.AllowGet);
                }
                

            }
            return Json(new { Error = SystemMessageLookup.GetMessage("SendEmailFailed") }, JsonRequestBehavior.AllowGet);
        }


        [AuthorizeContext(DocumentTypeKey = DocumentTypeKey.ReferralTask, OperationAction = OperationAction.View)]
        public ActionResult ShowPopupSendEmail(string selectedRowIdArray)
        {
            var includeContentHtml = string.Empty;
            if (!string.IsNullOrEmpty(selectedRowIdArray))
            {
                var strArrayId = selectedRowIdArray.Split(',');
                includeContentHtml += SystemMessageLookup.GetMessage("IncludedMessageHtml");
                includeContentHtml += "<ul>";
                includeContentHtml = strArrayId.Aggregate(includeContentHtml, (current, idStr) => current + ("<li> Task ID: " + idStr + "</li>"));
                includeContentHtml += "</ul>";
            }

            var viewModel = new SendMailWithDualGridViewModel()
            {
                Claimant = string.Empty,
                EmailTitle = string.Empty,
                ParentId = 0,
                IncludeContent = includeContentHtml,
                ModelName = "ReferralTask",
            };

            return View("SendMailWithDualGrid/_SendMailWithDualGridViewModel", viewModel);
        }

        [AuthorizeContext(DocumentTypeKey = DocumentTypeKey.ReferralTask, OperationAction = OperationAction.View)]
        public ActionResult ShowPopupSendEmailCollectionServiceRequestLetter(int referralId, int referralTaskId)
        {
            var data = _emailTemplateService.Get(o => o.Type == EmailTemplateType.CollectionServiceRequestBroadSpire).FirstOrDefault();
            var viewModel = new CollectionServiceRequestLetterViewModel();
            if (data!= null)
            {
                viewModel = GetContentHtmlWithEmailTemplate(referralId, data.Content);
                viewModel.ReferralTaskId = referralTaskId;
            }
            return View("_CollectionServiceRequestLetter", viewModel);
        }

        [AuthorizeContext(DocumentTypeKey = DocumentTypeKey.ReferralTask, OperationAction = OperationAction.View)]
        public ActionResult ShowPopupSelectEmail()
        {
            return View("DualGrid/_DualGridViewModel");
        }

        private CollectionServiceRequestLetterViewModel GetContentHtmlWithEmailTemplate(int referralId, string emailTemplateContent)
        {
            var result = new CollectionServiceRequestLetterViewModel();
            var data = _emailTemplateService.GetDataRequestForCollection(referralId);
            var date = DateTime.Now.ToString("MMMM d, yyyy");
            if (data != null)
            {
                var collectionCityStateZip = "";
                var hasCollectionCity = false;
                var hasCollectionState = false;
                if (!string.IsNullOrEmpty(data.CollectionSiteOrPhysicianCity))
                {
                    hasCollectionCity = true;
                    collectionCityStateZip += data.CollectionSiteOrPhysicianCity;
                }
                if (!string.IsNullOrEmpty(data.CollectionSiteOrPhysicianState))
                {
                    hasCollectionState = true;
                    if (hasCollectionCity)
                    {
                        collectionCityStateZip += ", " + data.CollectionSiteOrPhysicianState;
                    }
                    else
                    {
                        collectionCityStateZip += data.CollectionSiteOrPhysicianState;
                    }
                }
                if (!string.IsNullOrEmpty(data.CollectionSiteOrPhysicianZip))
                {
                    if (hasCollectionState || hasCollectionCity)
                    {
                        collectionCityStateZip += ", " + data.CollectionSiteOrPhysicianZip;
                    }
                    else
                    {
                        collectionCityStateZip += data.CollectionSiteOrPhysicianZip;
                    }
                }

                var resultHtml = TemplateHelpper.FormatTemplateWithContentTemplate(emailTemplateContent, new
                {
                    date,
                    collection_site_or_physician_fax = data.CollectionSiteOrPhysicianFax.ApplyFormatPhone(),
                    collection_site_or_physician_name = data.CollectionSiteOrPhysicianName,
                    collection_site_or_physician_address = data.CollectionSiteOrPhysicianAddress,
                    collection_city_state_zip = collectionCityStateZip,
                    payer_name = data.PayerName,
                    claimant_name = data.Claimant,
                    patient_name = data.Claimant,
                    patient_phone = data.PatientPhone.ApplyFormatPhone(),
                    patient_address = data.PatientAddr,
                    patient_city = data.PatientCity,
                    patient_state = data.PatientState,
                    patient_zip = data.PatientZip,
                    claim_number = data.ClaimNumber,
                    patient_doi = data.Doi == null ? "" : data.Doi.GetValueOrDefault().ToString("MMMM d, yyyy"),
                    patient_jur = data.ClaimJurisdiction,
                    patient_dob = data.Dob == null ? "" : data.Dob.GetValueOrDefault().ToString("MMMM d, yyyy"),
                    service_request = data.PanelType,
                    urine_oral_fluids_blood_hair = data.PanelTypeSimpleTest,
                    physician_name = data.PhysicianName,
                    physician_addr = data.PhysicianAddress,
                    physician_phone = data.PhysicianPhone.ApplyFormatPhone(),
                    customer_service_rep_name = data.AssignTo,
                    next_md_date = data.NextMdVisit,
                    provider_credential_text = data.ProviderCredentialText,
                    //yes = "<a href='" + (string)settingsReader.GetValue("Url", typeof(String)) + "/yes-authorized.html?posx=" + Base64ForUrlEncode(idOrder.ToString()) + "'>Yes</a>",
                    //no = "<a href='" + (string)settingsReader.GetValue("Url", typeof(String)) + "/no-authorized.html?posx=" + Base64ForUrlEncode(idOrder.ToString()) + "'>No</a>"
                });
                result.Fax = data.CollectionSiteOrPhysicianFax.ApplyFormatPhone();
                result.ContentHtml = WebUtility.HtmlDecode(resultHtml);
                result.ReferralId = referralId;
            }
            return result;
        }
    }
}