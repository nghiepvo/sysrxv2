﻿using System;
using System.Collections.Generic;
using System.Web.Mvc;
using Framework.DomainModel.Entities;
using Framework.Service.Diagnostics;
using ServiceLayer.Interfaces;
using ServiceLayer.Interfaces.Authentication;
using WorkComp.Models.Payer;
using WorkComp.Attributes;
using Framework.DomainModel.Entities.Common;
using Framework.DomainModel.Entities.Security;
using WorkComp.Models;
using Framework.DomainModel.ValueObject;
using Newtonsoft.Json;
using System.Collections.ObjectModel;

namespace WorkComp.Controllers
{
    public class PayerController : ApplicationControllerGeneric<Payer, DashboardPayerDataViewModel>
    {
        private readonly IPayerService _payerService;
        public PayerController(IAuthenticationService authenticationService, IDiagnosticService diagnosticService, IPayerService payerService)
            : base(authenticationService, diagnosticService, payerService)
        {
            _payerService = payerService;
        }

        [AuthorizeContext(DocumentTypeKey = DocumentTypeKey.Payer, OperationAction = OperationAction.View)]
        public ActionResult Index()
        {
            var viewModel = new DashboardPayerIndexViewModel();
            Func<GridViewModel> gridViewModel = () => new GridViewModel
            {
                GridId = "PayerGrid",
                ModelName = "Payer",
                UseCheckBoxColumn = true,
                UseDeleteColumn = true,
                WidthPopup = 900,
                HeightPopup = 600
            };

            viewModel.GridViewModel = BuildGridViewModel(gridViewModel);
            return View(viewModel);
        }

        [HttpGet]
        [AuthorizeContext(DocumentTypeKey = DocumentTypeKey.Payer, OperationAction = OperationAction.View)]
        public override JsonResult GetDataForGrid(QueryInfo queryInfo)
        {
            return base.GetDataForGrid(queryInfo);
        }

        protected override IList<ViewColumnViewModel> GetViewColumns()
        {
            var objViewColumn = new List<ViewColumnViewModel>
            {
                new ViewColumnViewModel
                {
                    ColumnOrder = 1,
                    ColumnWidth = 150,
                    Name = "Name",
                    Text = "Name",
                    ColumnJustification = GridColumnJustification.Left
                },
                new ViewColumnViewModel
                {
                    ColumnOrder = 1,
                    ColumnWidth = 180,
                    Name = "ControlNumberPrefix",
                    Text = "Control Number Prefix",
                    ColumnJustification = GridColumnJustification.Left
                },
                new ViewColumnViewModel
                {
                    ColumnOrder = 1,
                    ColumnWidth = 400,
                    Name = "SpecialInstructions",
                    Text = "Special Instructions",
                    ColumnJustification = GridColumnJustification.Left
                },
                new ViewColumnViewModel
                {
                    ColumnOrder = 1,
                    ColumnWidth = 200,
                    Name = "Address",
                    Text = "Address",
                    ColumnJustification = GridColumnJustification.Left
                },
                new ViewColumnViewModel
                {
                    ColumnOrder = 2,
                    ColumnWidth = 100,
                    Name = "City",
                    Text = "City",
                    ColumnJustification = GridColumnJustification.Left
                },
                  new ViewColumnViewModel
                {
                    ColumnOrder = 3,
                    ColumnWidth = 100,
                    Name = "State",
                    Text = "State",
                    ColumnJustification = GridColumnJustification.Left
                },
                new ViewColumnViewModel
                {
                    ColumnOrder = 2,
                    ColumnWidth = 100,
                    Name = "Zip",
                    Text = "Zip",
                    ColumnJustification = GridColumnJustification.Left
                },
            };
            return objViewColumn;
        }

        [AuthorizeContext(DocumentTypeKey = DocumentTypeKey.Payer, OperationAction = OperationAction.Add)]
        public ActionResult Create()
        {
            var viewModel = new DashboardPayerDataViewModel
            {
                SharedViewModel = new DashboardPayerShareViewModel()
                {
                    CreateMode = true
                }
            };
            // Check show popup for create from dropdown
            var type = Request.QueryString["type"];
            if (type == "1")
            {
                viewModel.SharedViewModel.ShowPopupType = ShowPopupType.ComboBox;
            }
            return View(viewModel);
        }

        [HttpPost]
        [AuthorizeContext(DocumentTypeKey = DocumentTypeKey.Payer, OperationAction = OperationAction.Add)]
        public int Create(PayerParameter parameters)
        {
            var savedEntity = CreateMasterFile(parameters);
            return savedEntity.Id;
        }

        [AuthorizeContext(DocumentTypeKey = DocumentTypeKey.Payer, OperationAction = OperationAction.Update)]
        public ActionResult Update(int id)
        {
            var viewModel = GetMasterFileViewModel(id);
            // Check show popup for create from dropdown
            var type = Request.QueryString["type"];
            if (type == "1")
            {
                viewModel.SharedViewModel.ShowPopupType = ShowPopupType.ComboBox;
            }
            return View(viewModel);
        }

        [HttpPost]
        [AuthorizeContext(DocumentTypeKey = DocumentTypeKey.Payer, OperationAction = OperationAction.Update)]
        public ActionResult Update(PayerParameter parameters)
        {
            return UpdateMasterFile(parameters);
        }

        [AuthorizeContext(DocumentTypeKey = DocumentTypeKey.Payer, OperationAction = OperationAction.Add)]
        public ActionResult Copy(int id)
        {
            var viewModel = GetMasterFileViewModel(id);
            viewModel.SharedViewModel.CreateMode = true;
            viewModel.SharedViewModel.Id = 0;
            viewModel.Id = 0;
            return View("Create", viewModel);
        }

        [AuthorizeContext(DocumentTypeKey = DocumentTypeKey.Payer, OperationAction = OperationAction.Delete)]
        public JsonResult Delete(string models)
        {
            var listItem = JsonConvert.DeserializeObject<Collection<dynamic>>(models);
            if (listItem.Count == 0)
            {
                return Json(false, JsonRequestBehavior.AllowGet);
            }
            var id = (int)(listItem[0].Id);
            _payerService.DeleteById(id);
            return Json(true, JsonRequestBehavior.AllowGet);
        }

        [AuthorizeContext(DocumentTypeKey = DocumentTypeKey.Payer, OperationAction = OperationAction.Delete)]
        public JsonResult DeleteMulti(string selectedRowIdArray, string isDeleteAll)
        {
            return DeleteMultiMasterfile(selectedRowIdArray, isDeleteAll);
        }

        [AuthorizeContext(DocumentTypeKey = DocumentTypeKey.Payer, OperationAction = OperationAction.View)]
        public JsonResult ExportExcel(GridConfigViewModel gridConfig, QueryInfo queryInfo)
        {
            return base.ExportExcelMasterfile(gridConfig, queryInfo);
        }

        [AuthorizeContext(DocumentTypeKey = DocumentTypeKey.Payer, OperationAction = OperationAction.View)]
        public JsonResult GetLookup(LookupQuery queryInfo)
        {
            var selector = new Func<Payer, LookupItemVo>(o => new LookupItemVo
            {
                KeyId = o.Id,
                DisplayName = o.Name
            });
            return base.GetLookupForEntity(queryInfo, selector);
        }

        [AuthorizeContext(DocumentTypeKey = DocumentTypeKey.Payer, OperationAction = OperationAction.View)]
        public JsonResult GetLookupItem(LookupItem lookupItem)
        {
            var selector = new Func<Payer, LookupItemVo>(o => new LookupItemVo
            {
                KeyId = o.Id,
                DisplayName = o.Name
            });
            return base.GetLookupItemForEntity(lookupItem, selector);
        }
    }
}
