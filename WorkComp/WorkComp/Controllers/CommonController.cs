﻿using System.Linq;
using System.Web.Mvc;
using Framework.DomainModel.Entities.Common;
using Framework.DomainModel.ValueObject;
using Framework.Service.Diagnostics;
using Framework.Utility;
using Newtonsoft.Json;
using ServiceLayer.Interfaces.Authentication;
using WorkComp.Models;

namespace WorkComp.Controllers
{
    public class CommonController : ApplicationControllerBase
    {
         private readonly IDiagnosticService _diagnosticService;
        private readonly IAuthenticationService _authenticationService;
        public CommonController(IAuthenticationService authenticationService,
                               IDiagnosticService diagnosticService
            )
            : base(authenticationService,diagnosticService,null)
        {
            _authenticationService = authenticationService;
            _diagnosticService = diagnosticService;
        }
        [HttpGet]
        public JsonResult GenderLookup()
        {
            var data = XmlDataHelpper.Instance.GetData(XmlDataTypeEnum.Gender.ToString()).Select(o => new { DisplayName = o.Value, KeyId = o.Key });
            return Json(data, JsonRequestBehavior.AllowGet);
        }

        [HttpGet]
        public JsonResult StatusLookup()
        {
            var data = XmlDataHelpper.Instance.GetData(XmlDataTypeEnum.Status.ToString()).Select(o => new { DisplayName = o.Value, KeyId = int.Parse(o.Key) });
            return Json(data, JsonRequestBehavior.AllowGet);
        }

        [HttpGet]
        public JsonResult CollectionMethodLookup()
        {
            var data = XmlDataHelpper.Instance.GetData(XmlDataTypeEnum.CollectionMethod.ToString()).Select(o => new { DisplayName = o.Value, KeyId = int.Parse(o.Key) });
            return Json(data, JsonRequestBehavior.AllowGet);
        }
        [HttpGet]
        public JsonResult TaskTypeLookup()
        {
            var data = XmlDataHelpper.Instance.GetData(XmlDataTypeEnum.TaskType.ToString()).Select(o => new LookupItemVo { DisplayName = o.Value, KeyId = int.Parse(o.Key) }).ToList();
            data.Insert(0, new LookupItemVo
            {
                KeyId = 0,
                DisplayName = "-- Select type --"
            });
            return Json(data, JsonRequestBehavior.AllowGet);
        }
        [HttpGet]
        public JsonResult ReferralMethodLookup()
        {
            var data = XmlDataHelpper.Instance.GetData(XmlDataTypeEnum.ReferralMethod.ToString()).Select(o => new LookupItemVo { DisplayName = o.Value, KeyId = int.Parse(o.Key) }).ToList();
            data.Insert(0, new LookupItemVo
            {
                KeyId = 0,
                DisplayName = "-- Select method --"
            });
            return Json(data, JsonRequestBehavior.AllowGet);
        }

        [HttpGet]
        public JsonResult DosageUnitLookup()
        {
            var data = XmlDataHelpper.Instance.GetData(XmlDataTypeEnum.DosageUnit.ToString()).Select(o => new { DisplayName = o.Value, KeyId = int.Parse(o.Key) });
            return Json(data, JsonRequestBehavior.AllowGet);
        }

        [HttpGet]
        public JsonResult NoteTypeLookup()
        {
            var data = XmlDataHelpper.Instance.GetData(XmlDataTypeEnum.NoteType.ToString()).Select(o => new LookupItemVo { DisplayName = o.Value, KeyId = int.Parse(o.Key) }).ToList();
            data.Insert(0, new LookupItemVo
            {
                KeyId = 0,
                DisplayName = "-- Select type --"
            });
            return Json(data, JsonRequestBehavior.AllowGet);
        }
        [HttpPost]
        public JsonResult SaveSearchReferralCondition(string searchParameter)
        {
            if (string.IsNullOrEmpty(searchParameter))
                return null;


            var jSettings = new JsonSerializerSettings()
            {
                Formatting = Formatting.Indented,
                DateTimeZoneHandling = DateTimeZoneHandling.Utc
            };
            jSettings.Converters.Add(new DefaultWrongFormatDeserialize());
            var searchViewModel = JsonConvert.DeserializeObject<ReferralSearchViewModel>(searchParameter, jSettings );
            Session["ReferralSearchViewModel"] = searchViewModel;
            if (searchViewModel == null)
            {
                return Json(0, JsonRequestBehavior.AllowGet);
            }
            return Json(1, JsonRequestBehavior.AllowGet);
        }

        [HttpGet]
        public JsonResult CancelReasonLookup()
        {
            var data = XmlDataHelpper.Instance.GetData(XmlDataTypeEnum.CancelReason.ToString()).Select(o => new { DisplayName = o.Value, KeyId = int.Parse(o.Key) });
            return Json(data, JsonRequestBehavior.AllowGet);
        }
    }
}
