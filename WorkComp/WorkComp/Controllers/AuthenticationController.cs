﻿using System;
using System.Web.Mvc;
using Common;
using Framework.Service.Diagnostics;
using Framework.Service.Translation;
using Framework.Utility;
using ServiceLayer.Interfaces;
using ServiceLayer.Interfaces.Authentication;
using WorkComp.Models.Authentication;

namespace WorkComp.Controllers
{
    public class AuthenticationController : ApplicationControllerBase
    {
        private readonly IDiagnosticService _diagnosticService;
        private readonly IAuthenticationService _authenticationService;
        private readonly IUserService _userService;
        public AuthenticationController(IAuthenticationService authenticationService,
                                        IDiagnosticService diagnosticService, IUserService userService)
            : base(authenticationService, diagnosticService, null)
        {
            _authenticationService = authenticationService;
            _diagnosticService = diagnosticService;
            _userService = userService;
        }
        public ActionResult SignOut()
        {
            //Sign out from authentication
            _authenticationService.SignOut();
            return RedirectToAction("SignIn");
        }
        [AllowAnonymous]
        public ActionResult SignIn(string returnUrl)
        {
            _diagnosticService.Debug("SignIn");
            var viewModel = new DashboardAuthenticationSignInViewModel();
            return View(viewModel);
        }

        [HttpPost]
        public JsonResult WorkCompSignIn(string userName, string password, bool rememberMe, string returnUrl)
        {
            password = PasswordHelper.HashString(password, userName);
            _authenticationService.SignIn(userName, password, rememberMe);

            return Json(1,JsonRequestBehavior.AllowGet);
        }

        private string ModifyReturnUrl(string returnUrl)
        {
            if (string.IsNullOrEmpty(returnUrl))
                return "/";
            return returnUrl;

        }

        public ActionResult RestorePassword()
        {
            var viewModel = new DashboardAuthenticationRestorePasswordViewModel();
            return View(viewModel);
        }
        [HttpPost]
        public JsonResult RestorePassword(string email)
        {
            if (ModelState.IsValid)
            {
                // Get password restore
                string passwordRandom;
                var user = _authenticationService.RestorePassword(email, out passwordRandom);
                if (user != null)
                {
                    var webLink = AppSettingsReader.GetValue("Url", typeof(String)) as string;
                    var imgSrc = webLink + "/Content/images/logo.png";
                    var urlChangePass = webLink + "/user/changepass/" + user.Id;
                    var fromEmail = AppSettingsReader.GetValue("EmailFrom", typeof(String)) as string;
                    var displayName = AppSettingsReader.GetValue("EmailFromDisplayName", typeof(String)) as string;
                    var emailContent = TemplateHelpper.FormatTemplateWithContentTemplate(
                        TemplateHelpper.ReadContentFromFile(TemplateConfigFile.RestorePasswordTemplate, true),
                        new
                        {
                            img_src = imgSrc,
                            full_name = user.FirstName+" "+(user.MiddleName??"")+user.LastName,
                            web_link = webLink,
                            user_name = user.UserName,
                            password = passwordRandom,
                            url_change_pass = urlChangePass
                        });
                    // send email
                    EmailHelper.SendEmail(fromEmail, new[] { user.Email }, SystemMessageLookup.GetMessage("SubjectToSendEmailForCreateUser"),
                                            emailContent, true, displayName);
                }
            }
            return Json(1,JsonRequestBehavior.AllowGet);
        }
    }
}
