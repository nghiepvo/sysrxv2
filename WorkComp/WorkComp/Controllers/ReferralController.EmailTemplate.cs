﻿using System;
using System.Collections.Generic;
using System.Configuration;
using System.Linq;
using System.Text;
using System.Web.Mvc;
using Common;
using Framework.DomainModel.Entities;
using Framework.DomainModel.Entities.Common;
using Framework.DomainModel.Entities.Security;
using Framework.Mapping;
using Framework.Service.Translation;
using Framework.Utility;
using Newtonsoft.Json;
using ServiceLayer.Interfaces;
using WorkComp.Attributes;
using WorkComp.Models.Referral;
using System.Web;
using System.IO;
using System.ComponentModel.DataAnnotations;

namespace WorkComp.Controllers
{
    public partial class ReferralController
    {
        private readonly IDiaryService _diaryService;
        private readonly IEmailTemplateService _emailTemplateService;
        private readonly IReferralEmailTemplateAttachmentService _referralEmailTemplateAttachmentService;
        [AuthorizeContext(DocumentTypeKey = DocumentTypeKey.Referral, OperationAction = OperationAction.View)]
        public ActionResult EmailTemplateDetail(int referralId, int emailId)
        {
            var model = new ReferralEmailTemplateDetailViewModel();
            if (referralId != 0 && emailId != 0)
            {
                var objReferralEmailTemplate =
                    _referralEmailTemplateService.Get(o => o.ReferralId == referralId && o.EmailTemplateId == emailId)
                        .FirstOrDefault();

                if (objReferralEmailTemplate == null)
                {
                    var objEmailTemplate = _emailTemplateService.GetById(emailId);
                    if (objEmailTemplate != null)
                    {
                        var objOrderEmailTemplate = _emailTemplateService.GetOrderEmailTemplate(referralId, emailId);
                        var emailContentTemplate = objEmailTemplate.Content;
                        var emailTemplateType = objEmailTemplate.Type;
                        model.PageTitle = model.EmailTitle = objEmailTemplate.Title;
                        model.EmailTemplateId = objEmailTemplate.Id;
                        model.ReferralId = referralId;
                        string emailFrom = "";
                        string displayName = "";

                        var objReferral = _referralService.GetById(referralId);
                        string claimNumber = (objReferral != null
                            ? (objReferral.ClaimNumber != null ? "- Claim number: " + objReferral.ClaimNumber.Name : "")
                            : "");
                        if (objOrderEmailTemplate != null)
                        {
                            model.DateToSendEmail = objOrderEmailTemplate.DateToSendMail == null
                                ? ""
                                : objOrderEmailTemplate.DateToSendMail.Value.ToString("MM/dd/yyyy HH:mm");
                            model.DateToSendFax = objOrderEmailTemplate.DateToSendFax == null
                                ? ""
                                : objOrderEmailTemplate.DateToSendFax.Value.ToString("MM/dd/yyyy HH:mm");
                            model.FaxNumber = objOrderEmailTemplate.FaxNumber;

                        }
                        if (emailTemplateType == EmailTemplateType.RequestForAuthorization
                            || emailTemplateType == EmailTemplateType.RequestForAuthorizationSecondRequest
                            || emailTemplateType == EmailTemplateType.CancellationDueToLackOfInformation
                            || emailTemplateType == EmailTemplateType.SchedulingConfirmed
                            || emailTemplateType == EmailTemplateType.NotificateTestingCandidate)
                        {
                            model.EmailSubject = objEmailTemplate.Subject + claimNumber;
                            model.EmailContent = GetDataForRequestForAuthorizationEmailTemplate(referralId,
                                emailContentTemplate, ref emailFrom, ref displayName);
                            model.EmailFrom = emailFrom;
                            model.DisplayName = displayName;
                        }
                        else if (emailTemplateType == EmailTemplateType.IwAppointmentReminder
                                 || emailTemplateType == EmailTemplateType.IwAppointmentNotification
                                 || emailTemplateType == EmailTemplateType.CollectionServiceRequestBroadSpire
                                 ||
                                 emailTemplateType == EmailTemplateType.CollectionServiceRequestMedicationMonitoringPme)
                        {
                            var patientName = "";
                            var faxNumber = "";
                            model.EmailContent = GetDataForRequestForCollectionEmailTemplate(referralId,
                                emailContentTemplate, ref patientName, ref emailFrom, ref displayName, emailTemplateType,
                                ref faxNumber);
                            model.EmailSubject =
                                TemplateHelpper.FormatTemplateWithContentTemplate(objEmailTemplate.Subject, new
                                {
                                    patient_name = patientName + claimNumber
                                });
                            model.EmailFrom = emailFrom;
                            model.DisplayName = displayName;
                            if (objOrderEmailTemplate != null && !string.IsNullOrEmpty(objOrderEmailTemplate.FaxNumber))
                            {

                                model.FaxNumber = objOrderEmailTemplate.FaxNumber.ApplyFormatPhone();
                            }
                            else
                            {
                                model.FaxNumber = faxNumber.ApplyFormatPhone();
                            }
                        }
                        //else if (emailTemplateType == EmailTemplateType.CollectionSiteRequest)
                        //{
                        //    var faxNumber = "";
                        //    model.EmailContent = GetDataForCollectionServiceRequestEmailTemplate(referralId,
                        //        emailContentTemplate, ref emailFrom, ref displayName, ref faxNumber);
                        //    model.EmailSubject = objEmailTemplate.Subject + claimNumber;
                        //    model.EmailFrom = emailFrom;
                        //    model.DisplayName = displayName;
                        //    if (objOrderEmailTemplate != null &&
                        //        !string.IsNullOrEmpty(objOrderEmailTemplate.EmailAddress))
                        //    {
                        //        model.FaxNumber = objOrderEmailTemplate.FaxNumber.ApplyFormatPhone();
                        //    }
                        //    else
                        //    {
                        //        model.FaxNumber = (faxNumber).ApplyFormatPhone();
                        //    }
                        //}
                        else if (emailTemplateType == EmailTemplateType.MdNotificationLetter
                            || emailTemplateType == EmailTemplateType.PhysicianShipmentConfirmation)
                        {
                            var faxNumber = "";
                            string payerName = "";
                            model.EmailContent = GetDataMdNotificationLetterEmailTemplate(referralId, emailContentTemplate, ref payerName, ref faxNumber);
                            
                            if (objOrderEmailTemplate != null &&
                                !string.IsNullOrEmpty(objOrderEmailTemplate.EmailAddress))
                            {
                                model.FaxNumber = objOrderEmailTemplate.FaxNumber.ApplyFormatPhone();
                            }
                            else
                            {
                                model.FaxNumber = (faxNumber).ApplyFormatPhone();
                            }
                            model.EmailSubject = TemplateHelpper.FormatTemplateWithContentTemplate(objEmailTemplate.Subject, new
                               {
                                   payer_name = payerName
                               });
                        }
                        else if (emailTemplateType == EmailTemplateType.CustomCommunication)
                        {
                            model.EmailContent = GetDataDataCustomCommunicationTemplate(referralId, emailContentTemplate);
                           
                            model.EmailSubject = objEmailTemplate.Subject;
                        }
                       
                        // Set email from for all email template
                        model.ListEmailAddressSetup = new List<EmailItemTitle>();
                        var objEmailForReferral = _emailTemplateService.GetListEmailInReferral(referralId);
                        if (objEmailForReferral != null)
                        {
                            // create email for referral
                            var objEmailReferral = new EmailItemTitle
                            {
                                Email = objEmailForReferral.EmailReferral,
                                Name = objEmailForReferral.NameReferral,
                                Type = "Referral",
                            };
                            model.ListEmailAddressSetup.Add(objEmailReferral);
                            // create email for CaseManager
                            var objEmailCaseManager = new EmailItemTitle
                            {
                                Email = objEmailForReferral.EmailCaseManager,
                                Name = objEmailForReferral.NameCaseManager,
                                Type = "Case Manager",
                            };
                            model.ListEmailAddressSetup.Add(objEmailCaseManager);
                            // create email for Adjuster
                            var objEmailAdjuster = new EmailItemTitle
                            {
                                Email = objEmailForReferral.EmailAdjuster,
                                Name = objEmailForReferral.NameAdjuster,
                                Type = "Adjuster",
                            };
                            model.ListEmailAddressSetup.Add(objEmailAdjuster);
                            // create email for Claimant
                            var objEmailClaimant = new EmailItemTitle
                            {
                                Email = objEmailForReferral.EmailClaimant,
                                Name = objEmailForReferral.NameClaimant,
                                Type = "Claimant",
                            };
                            model.ListEmailAddressSetup.Add(objEmailClaimant);
                            // create email for Attorney
                            var objEmailAttorney = new EmailItemTitle
                            {
                                Email = objEmailForReferral.EmailAttorney,
                                Name = objEmailForReferral.NameAttorney,
                                Type = "Attorney",
                            };
                            model.ListEmailAddressSetup.Add(objEmailAttorney);
                            // create email for CollectionSite
                            var objEmailCollectionSiteOrPhysician = new EmailItemTitle
                            {
                                Email = objEmailForReferral.CollectionSiteOrPhysicianEmail,
                                Name =
                                    objEmailForReferral.CollectionSiteOrPhysicianName,
                                Type =
                                    objEmailForReferral.IsCollectionSite.GetValueOrDefault()
                                        ? "True"
                                        : "",
                            };
                            model.ListEmailAddressSetup.Add(objEmailCollectionSiteOrPhysician);
                        }
                    }
                }
                else
                {
                    model = objReferralEmailTemplate.MapTo<ReferralEmailTemplateDetailViewModel>();
                    var listAttacment = _referralEmailTemplateAttachmentService.GetAttachments(model.EmailTemplateId,
                        o =>
                            new AttachmentSendMail()
                            {
                                Id = o.Id,
                                FileName = o.AttachedFileName,
                                RowGuid = o.RowGuid.ToString(),
                                IsDelete = false,
                            });
                    model.AttachmentSendMails = listAttacment.ToArray();
                    var objEmailTemplate = _emailTemplateService.GetById(model.EmailTemplateId);
                    if (objEmailTemplate != null)
                    {
                        model.PageTitle = objEmailTemplate.Title;
                    }
                    if (!string.IsNullOrEmpty(model.EmailAddressSetup))
                    {
                        var arrEmailAddressSetup = model.EmailAddressSetup.Split(';');
                        var objEmailForReferral = _emailTemplateService.GetListEmailInReferral(referralId);
                        if (objEmailForReferral != null && arrEmailAddressSetup.Length > 0)
                        {
                            //EmailReferral
                            if (arrEmailAddressSetup.Any(o=>o.Equals(objEmailForReferral.EmailReferral)))
                            {
                                model.ListEmailAddressSetup.Add(new EmailItemTitle()
                                {
                                    Email = objEmailForReferral.EmailReferral,
                                    Name = objEmailForReferral.NameReferral,
                                    Type = "Referral",
                                });
                            }

                            //EmailCaseManager
                            if (arrEmailAddressSetup.Any(o => o.Equals(objEmailForReferral.EmailCaseManager)))
                            {
                                model.ListEmailAddressSetup.Add(new EmailItemTitle()
                                {
                                    Email = objEmailForReferral.EmailCaseManager,
                                    Name = objEmailForReferral.NameCaseManager,
                                    Type = "Case Manager",
                                });
                            }

                            //EmailCaseAdjuster
                            if (arrEmailAddressSetup.Any(o => o.Equals(objEmailForReferral.EmailAdjuster)))
                            {
                                model.ListEmailAddressSetup.Add(new EmailItemTitle()
                                {
                                    Email = objEmailForReferral.EmailAdjuster,
                                    Name = objEmailForReferral.NameAdjuster,
                                    Type = "Adjuster",
                                });
                            }

                            //Email Claimant
                            if (arrEmailAddressSetup.Any(o => o.Equals(objEmailForReferral.EmailClaimant)))
                            {
                                model.ListEmailAddressSetup.Add(new EmailItemTitle()
                                {
                                    Email = objEmailForReferral.EmailClaimant,
                                    Name = objEmailForReferral.NameClaimant,
                                    Type = "Claimant",
                                });
                            }

                            //Email Attorney
                            if (arrEmailAddressSetup.Any(o => o.Equals(objEmailForReferral.EmailAttorney)))
                            {
                                model.ListEmailAddressSetup.Add(new EmailItemTitle()
                                {
                                    Email = objEmailForReferral.EmailAttorney,
                                    Name = objEmailForReferral.NameAttorney,
                                    Type = "Attorney",
                                });
                            }
                            
                        }
                    }
                }
            }
            


            return View(model);

        }

        [AuthorizeContext(DocumentTypeKey = DocumentTypeKey.Referral, OperationAction = OperationAction.View)]
        public ActionResult BrowserEmail()
        {
            var model = new BrowserEmailModels();
            model.PageTitle = "Browser email";
            return View(model);
        }

        public JsonResult SendEmailForReferral(EmailContentToSendViewModel emailContentToSend)
        {
            var arrEmailTo = emailContentToSend.EmailTo.Split(',');
            var content = HttpUtility.HtmlDecode(emailContentToSend.Content);
            // Get attach file
            var listAttachFile = new Dictionary<string, string>();
            if (!string.IsNullOrEmpty(emailContentToSend.AttachFile))
            {
                var objListAttachFileName = emailContentToSend.AttachFile.Split(';');

                var tempPathFolder = Path.GetTempPath();
                foreach (var filename in objListAttachFileName.Where(filename => filename.Trim() != ""))
                {
                    var tempPath = tempPathFolder + filename;
                    if (System.IO.File.Exists(tempPath))
                    {
                        listAttachFile.Add(filename,tempPath);
                    }
                }
            }
            var isSuccess = EmailHelper.SendEmail(emailContentToSend.EmailFrom, arrEmailTo, emailContentToSend.Subject,
                content, true, emailContentToSend.DisplayName, listAttachFile);
            if (isSuccess)
            {
                string idRandom = DateTime.Now.ToString("MMddyyyyHHmmss");
                var referralId = 0;
                int.TryParse(emailContentToSend.IdReferral, out referralId);
                if (referralId != 0)
                {
                    var objDiary = new Diary
                    {
                        Heading = "Communication",
                        Reason = "Email",
                        Comment = "<div style='width:100%;overflow-x: auto'>-Subject: " + emailContentToSend.Subject + " <br/>-Email recipient: " + emailContentToSend.EmailTo + "<br/>-Content: <input type='button' id='btnShowHideDiary-" + idRandom + "' value='Show' onclick=\"javascript:ShowHideCommunicationDiary('" + idRandom + "')\"/><div id='content-communication-diary-" + idRandom + "' style='display:none'>" + content + "</div></div>",
                        ReferralId = referralId
                    };
                    _diaryService.Add(objDiary);
                }
            }
            return Json(isSuccess ? "1" : "0", JsonRequestBehavior.AllowGet);
        }

        public JsonResult SendFaxForReferral(FaxContentToSendModel faxContentToSend)
        {
            var content = HttpUtility.HtmlDecode(faxContentToSend.Content);
            var errorMess = "";
            var isSuccess = EmailHelper.SendFax(faxContentToSend.Subject, faxContentToSend.SenderName, "",
                faxContentToSend.FaxNumber, content, ref errorMess);
            if (isSuccess)
            {
                string idRandom = DateTime.Now.ToString("MMddyyyyHHmmss");
                var referralId = 0;
                int.TryParse(faxContentToSend.IdReferral, out referralId);
                if (referralId != 0)
                {
                    var objDiary = new Diary
                    {
                        Heading = "Communication",
                        Reason = "Fax",
                        Comment = "<div style='width:100%;overflow-x: auto'>-Subject: " + faxContentToSend.Subject + " <br/>-Fax recipient: " + faxContentToSend.FaxRecipient + "<br/>Fax number: " + faxContentToSend.FaxNumber + "<br/>-Content: <input type='button' id='btnShowHideDiary-" + idRandom + "' value='Show' onclick=\"javascript:ShowHideCommunicationDiary('" + idRandom + "')\"/><div id='content-communication-diary-" + idRandom + "' style='display:none'>" + content + "</div></div>",
                        ReferralId = referralId
                    };
                    _diaryService.Add(objDiary);
                }
            }
            return Json(isSuccess ? "1" : errorMess, JsonRequestBehavior.AllowGet);
        }
        

        [HttpPost,ValidateInput(false)]
        [AuthorizeContext(DocumentTypeKey = DocumentTypeKey.User, OperationAction = OperationAction.Update)]
        public ActionResult SaveReferralEmailTemplate(ReferralEmailTemplateParameter parameters)
        {
            byte[] lastModified = null;
            if (string.IsNullOrEmpty(parameters.SharedParameter))
                return Json(new { Error = string.Empty, Data = new { LastModified = lastModified } }, JsonRequestBehavior.AllowGet);

            byte[] data = Convert.FromBase64String(parameters.SharedParameter);
            parameters.SharedParameter = Encoding.UTF8.GetString(data);
            var jSettings = new JsonSerializerSettings()
            {
                Formatting = Formatting.Indented,
                DateTimeZoneHandling = DateTimeZoneHandling.Utc
            };
            jSettings.Converters.Add(new DefaultWrongFormatDeserialize());
            var objModel = JsonConvert.DeserializeObject<ReferralEmailTemplateDataViewModel>(parameters.SharedParameter, jSettings);
            // Store data to table ReferralEmailTemplate
            var objReferralEmailTemplate = _referralEmailTemplateService.GetIncludeAttachment(objModel.ReferralId, objModel.EmailTemplateId);
            if (objReferralEmailTemplate== null)
            {
                objReferralEmailTemplate = objModel.MapTo<ReferralEmailTemplate>();
               lastModified = _referralEmailTemplateService.Add(objReferralEmailTemplate).LastModified;
            }
            else
            {
                objReferralEmailTemplate.Subject = objModel.EmailSubject;
                objReferralEmailTemplate.Content = objModel.EmailContent;
                objReferralEmailTemplate.EmailAddress = objModel.EmailAddressSetup;
                objReferralEmailTemplate.DateToSendMail = objModel.DateToSendMail;

                //Update attachment
                foreach (var fileName in objModel.FileNames)
                {
                    if (!fileName.IsDelete && fileName.Id == 0)
                    {
                        var fullFilePath = Path.Combine(Path.GetTempPath(), fileName.RowGuid + fileName.FileName);
                        byte[] savedFile = null;
                        if (System.IO.File.Exists(fullFilePath))
                        {
                            savedFile = System.IO.File.ReadAllBytes(fullFilePath);
                            System.IO.File.Delete(fullFilePath);
                        }
                        objReferralEmailTemplate.ReferralEmailTemplateAttachments.Add(new ReferralEmailTemplateAttachment()
                        {
                            AttachedFileName = fileName.FileName,
                            RowGuid = Guid.Parse(fileName.RowGuid),
                            AttachedFileContent = savedFile,
                            AttachedFileSize = savedFile != null ? savedFile.Length : 0
                        });
                    }
                    else if (fileName.IsDelete && fileName.Id != 0)
                    {
                        objReferralEmailTemplate.ReferralEmailTemplateAttachments.First(o => o.Id == fileName.Id)
                            .IsDeleted = true;
                    }
                }
                objReferralEmailTemplate.FaxNumber = objModel.FaxNumber;
                objReferralEmailTemplate.DateToSendFax = objModel.DateToSendFax;
                lastModified = _referralEmailTemplateService.Update(objReferralEmailTemplate).LastModified;
            }

            return Json(new { Error = string.Empty, Data = new { LastModified = lastModified } }, JsonRequestBehavior.AllowGet);
        }

        [HttpPost]
        public JsonResult UploadAttachmentSendEmail(HttpPostedFileBase file)
        {
            // Upload to temp folder
            string fileName = file.FileName, rowGuid = Guid.NewGuid().ToString();

            var tempPath = Path.GetTempPath() + rowGuid + fileName;
            if (System.IO.File.Exists(tempPath))
            {
                System.IO.File.Delete(tempPath);
            }
            file.SaveAs(tempPath);

            return Json(new { FileName = fileName, RowGuid=rowGuid }, JsonRequestBehavior.AllowGet);
        }
        [HttpPost]
        public JsonResult RemoveAttachmentSendEmail(string fileName, string rowGuid)
        {
            var tempPath = Path.GetTempPath() + rowGuid + fileName;
            if (System.IO.File.Exists(tempPath))
            {
                System.IO.File.Delete(tempPath);
            }
            return Json(true, JsonRequestBehavior.AllowGet);
        }

        
        
        private string GetDataForRequestForAuthorizationEmailTemplate(int referralId, string emailTemplateContent,
                                    ref string emailFrom, ref string displayName)
        {
            var date = DateTime.Now.ToString("MMMM d, yyyy");
            var data = _emailTemplateService.GetDataRequestForAuthorization(referralId);
            var result = "";
            var listReasonCodeForReferral = _referralService.GetReasonReferralInfos(referralId);
            if (data != null)
            {
                var payerCityStateZip = "";
                var hasPayorCity = false;
                var hasPayorState = false;
                if (!string.IsNullOrEmpty(data.PayerCity))
                {
                    hasPayorCity = true;
                    payerCityStateZip += data.PayerCity;
                }
                if (!string.IsNullOrEmpty(data.PayerState))
                {
                    hasPayorState = true;
                    if (hasPayorCity)
                    {
                        payerCityStateZip += ", " + data.PayerState;
                    }
                    else
                    {
                        payerCityStateZip += data.PayerState;
                    }
                }
                if (!string.IsNullOrEmpty(data.PayerZip))
                {
                    if (hasPayorState || hasPayorCity)
                    {
                        payerCityStateZip += ", " + data.PayerZip;
                    }
                    else
                    {
                        payerCityStateZip += data.PayerZip;
                    }
                }
                string collectionSiteName = data.CollectionSiteOrPhysicianName;
                displayName = data.AssignTo;
                emailFrom = data.AssignEmail;
                //Get list reason code for referral
                var sbReasonCodeForReferral = new StringBuilder();
                string description = "";
                if (listReasonCodeForReferral != null && listReasonCodeForReferral.Count > 0)
                {
                    description += "<ul>";
                    foreach (var rc in listReasonCodeForReferral)
                    {
                        description += "<li>" + rc.Name + "</li>";
                    }
                    description += "</ul>";
                    sbReasonCodeForReferral.AppendLine(description);
                }
                var urlLogo = ConfigurationManager.AppSettings["Url"] + "Content/images/logo.png";
                result = TemplateHelpper.FormatTemplateWithContentTemplate(emailTemplateContent, new
                {
                    logo_sterling =urlLogo,
                    date,
                    adjuster_name = data.AdjusterName,
                    collection_site_or_physician_name = collectionSiteName,
                    payer_name = data.PayerName,
                    payer_address = data.PayerAddress,
                    payer_city_state_zip = payerCityStateZip,
                    claimant_name = data.Claimant,
                    patient_name = data.Claimant,
                    patient_phone = data.PatientPhone.ApplyFormatPhone(),
                    patient_address = data.PatientAddr,
                    patient_city = data.PatientCity,
                    patient_state = data.PatientState,
                    patient_zip = data.PatientZip,
                    claim_number = data.ClaimNumber,
                    patient_doi = data.Doi.GetValueOrDefault().ToString("MMMM d, yyyy"),
                    patient_jur = data.ClaimJurisdiction,
                    patient_dob = data.Dob.GetValueOrDefault().ToString("MMMM d, yyyy"),
                    service_request = data.PanelType,
                    customer_service_rep_name = data.AssignTo,
                    testing_date = data.NextMdVisit,
                    order_title = data.OrderTitle,
                    reason_code_for_referral = sbReasonCodeForReferral.ToString(),
                    //yes = "<a href='" + (string)settingsReader.GetValue("Url", typeof(String)) + "/yes-authorized.html?posx=" + Base64ForUrlEncode(idOrder.ToString()) + "'>Yes</a>",
                    //no = "<a href='" + (string)settingsReader.GetValue("Url", typeof(String)) + "/no-authorized.html?posx=" + Base64ForUrlEncode(idOrder.ToString()) + "'>No</a>"
                });
            }
            return result;
        }
        private string GetDataForRequestForCollectionEmailTemplate(int referralId, string emailTemplateContent, ref string patientName,
                                                                    ref string emailFrom, ref string displayName,
                                                                    EmailTemplateType emailTemplateType, ref string faxNumber)
        {
            var date = DateTime.Now.ToString("MMMM d, yyyy");
            var getPhysicianInfo = (emailTemplateType == EmailTemplateType.CollectionServiceRequestBroadSpire || emailTemplateType == EmailTemplateType.CollectionServiceRequestMedicationMonitoringPme);
            var data = _emailTemplateService.GetDataRequestForCollection(referralId);
            var result = "";
            if (data != null)
            {
                var collectionCityStateZip = "";
                var hasCollectionCity = false;
                var hasCollectionState = false;
                if (!string.IsNullOrEmpty(data.CollectionSiteOrPhysicianCity))
                {
                    hasCollectionCity = true;
                    collectionCityStateZip += data.CollectionSiteOrPhysicianCity;
                }
                if (!string.IsNullOrEmpty(data.CollectionSiteOrPhysicianState))
                {
                    hasCollectionState = true;
                    if (hasCollectionCity)
                    {
                        collectionCityStateZip += ", " + data.CollectionSiteOrPhysicianState;
                    }
                    else
                    {
                        collectionCityStateZip += data.CollectionSiteOrPhysicianState;
                    }
                }
                if (!string.IsNullOrEmpty(data.CollectionSiteOrPhysicianZip))
                {
                    if (hasCollectionState || hasCollectionCity)
                    {
                        collectionCityStateZip += ", " + data.CollectionSiteOrPhysicianZip;
                    }
                    else
                    {
                        collectionCityStateZip += data.CollectionSiteOrPhysicianZip;
                    }
                }
                patientName = data.Claimant;
                displayName = data.AssignTo;
                emailFrom = data.AssignEmail;
                var urlLogo = ConfigurationManager.AppSettings["Url"] + "Content/images/logo.png";
                if (emailTemplateType == EmailTemplateType.CollectionServiceRequestBroadSpire || emailTemplateType == EmailTemplateType.CollectionServiceRequestMedicationMonitoringPme)
                {
                    faxNumber = data.PhysicianFax;
                    
                    result = TemplateHelpper.FormatTemplateWithContentTemplate(emailTemplateContent, new
                    {
                        logo_sterling = urlLogo,
                        date,
                        collection_site_or_physician_fax = data.CollectionSiteOrPhysicianFax.ApplyFormatPhone(),
                        collection_site_or_physician_name = data.CollectionSiteOrPhysicianName,
                        collection_site_or_physician_address = data.CollectionSiteOrPhysicianAddress,
                        collection_city_state_zip = collectionCityStateZip,
                        payer_name = data.PayerName,
                        claimant_name = data.Claimant,
                        patient_name = data.Claimant,
                        patient_phone = data.PatientPhone.ApplyFormatPhone(),
                        patient_address = data.PatientAddr,
                        patient_city = data.PatientCity,
                        patient_state = data.PatientState,
                        patient_zip = data.PatientZip,
                        claim_number = data.ClaimNumber,
                        patient_doi = data.Doi==null?"":data.Doi.GetValueOrDefault().ToString("MMMM d, yyyy"),
                        patient_jur = data.ClaimJurisdiction,
                        patient_dob = data.Dob == null ? "" : data.Dob.GetValueOrDefault().ToString("MMMM d, yyyy"),
                        service_request = data.PanelType,
                        urine_oral_fluids_blood_hair = data.PanelTypeSimpleTest,
                        physician_name = data.PhysicianName,
                        physician_addr = data.PhysicianAddress,
                        physician_phone = data.PhysicianPhone.ApplyFormatPhone(),
                        customer_service_rep_name = data.AssignTo,
                        next_md_date = data.NextMdVisit,
                        provider_credential_text = data.ProviderCredentialText,
                        //yes = "<a href='" + (string)settingsReader.GetValue("Url", typeof(String)) + "/yes-authorized.html?posx=" + Base64ForUrlEncode(idOrder.ToString()) + "'>Yes</a>",
                        //no = "<a href='" + (string)settingsReader.GetValue("Url", typeof(String)) + "/no-authorized.html?posx=" + Base64ForUrlEncode(idOrder.ToString()) + "'>No</a>"
                    });
                }
                else
                {
                    result = TemplateHelpper.FormatTemplateWithContentTemplate(emailTemplateContent, new
                    {
                        logo_sterling = urlLogo,
                        date,
                        collection_site_or_physician_fax = data.CollectionSiteOrPhysicianFax.ApplyFormatPhone(),
                        collection_site_or_physician_name = data.CollectionSiteOrPhysicianName,
                        collection_site_or_physician_address = data.CollectionSiteOrPhysicianAddress,
                        collection_city_state_zip = collectionCityStateZip,
                        claimant_name = data.Claimant,
                        patient_name = data.Claimant,
                        patient_phone = data.PatientPhone.ApplyFormatPhone(),
                        patient_address = data.PatientAddr,
                        patient_city = data.PatientCity,
                        patient_state = data.PatientState,
                        patient_zip = data.PatientZip,
                        claim_number = data.ClaimNumber,
                        patient_doi = data.Doi==null?"":data.Doi.GetValueOrDefault().ToString("MMMM d, yyyy"),
                        patient_jur = data.ClaimJurisdiction,
                        patient_dob = data.Dob == null ? "" : data.Dob.GetValueOrDefault().ToString("MMMM d, yyyy"),
                        service_request = data.PanelType,
                        urine_oral_fluids_blood_hair = data.PanelTypeSimpleTest,
                        customer_service_rep_name = data.AssignTo,
                        //yes = "<a href='" + (string)settingsReader.GetValue("Url", typeof(String)) + "/yes-authorized.html?posx=" + Base64ForUrlEncode(idOrder.ToString()) + "'>Yes</a>",
                        //no = "<a href='" + (string)settingsReader.GetValue("Url", typeof(String)) + "/no-authorized.html?posx=" + Base64ForUrlEncode(idOrder.ToString()) + "'>No</a>"
                    });
                }

            }
            return result;
        }

        private string GetDataMdNotificationLetterEmailTemplate(int referralId, string emailTemplateContent, ref string payerName, ref string faxNumber)
        {
            string result = "";
            var data = _emailTemplateService.GetDataMdNotificationLetterTemplate(referralId);
            faxNumber = data.PhysicianFax;
            payerName = data.PayerName;
            var date = DateTime.Now.ToString("MMMM d, yyyy");
            var urlLogo = ConfigurationManager.AppSettings["Url"] + "Content/images/logo.png";
            result = TemplateHelpper.FormatTemplateWithContentTemplate(emailTemplateContent, new
            {

                logo_sterling = urlLogo,
                date,
                physician_name = data.PhysicianName.ApplyFormatPhone(),
                physician_address = data.PhysicianAddress,
                physician_city_state_zip = data.PhysicianCityStateZip,
                physician_fax = data.PhysicianFax,
                claimant_name = data.Claimant,
                patient_name = data.Claimant,
                patient_phone = data.PatientPhone.ApplyFormatPhone(),
                patient_address = data.PatientAddr,
                patient_city = data.PatientCity,
                patient_state = data.PatientState,
                patient_zip = data.PatientZip,
                claim_number = data.ClaimNumber,
                patient_doi = data.Doi == null ? "" : data.Doi.GetValueOrDefault().ToString("MMMM d, yyyy"),
                patient_jur = data.ClaimJurisdiction,
                patient_dob = data.Dob == null ? "" : data.Dob.GetValueOrDefault().ToString("MMMM d, yyyy"),
                service_request = data.PanelType,
                sample_type = data.SampleTestingType,
                panel = data.PanelType,
                customer_service_rep_name = data.AssignTo,
                order_title = data.ProductTypeTitle,
                reason_code_for_referral = data.HtmlListReasonReferrals,
                //yes = "<a href='" + (string)settingsReader.GetValue("Url", typeof(String)) + "/yes-authorized.html?posx=" + Base64ForUrlEncode(idOrder.ToString()) + "'>Yes</a>",
                //no = "<a href='" + (string)settingsReader.GetValue("Url", typeof(String)) + "/no-authorized.html?posx=" + Base64ForUrlEncode(idOrder.ToString()) + "'>No</a>"
            });
            return result;
        }
        private string GetDataDataCustomCommunicationTemplate(int referralId, string emailTemplateContent)
        {
            string result = "";

            var data = _emailTemplateService.GetDataCustomCommunicationTemplate(referralId);
            var date = DateTime.Now.ToString("MMMM d, yyyy");
            var urlLogo = ConfigurationManager.AppSettings["Url"] + "Content/images/logo.png";
            result = TemplateHelpper.FormatTemplateWithContentTemplate(emailTemplateContent, new
            {

                logo_sterling = urlLogo,
                date,
                physician_name = data.PhysicianName.ApplyFormatPhone(),
                physician_address = data.PhysicianAddress,
                physician_phone = data.PhysicianPhone,
                claimant_name = data.Claimant,
                patient_name = data.Claimant,
                patient_phone = data.PatientPhone.ApplyFormatPhone(),
                patient_address = data.PatientAddr,
                patient_city = data.PatientCity,
                patient_state = data.PatientState,
                patient_zip = data.PatientZip,
                claim_number = data.ClaimNumber,
                patient_doi = data.Doi == null ? "" : data.Doi.GetValueOrDefault().ToString("MMMM d, yyyy"),
                patient_jur = data.ClaimJurisdiction,
                patient_dob = data.Dob == null ? "" : data.Dob.GetValueOrDefault().ToString("MMMM d, yyyy"),
                service_request = data.PanelType,
                panel = data.PanelType,
                customer_service_rep_name = data.AssignTo,
                //yes = "<a href='" + (string)settingsReader.GetValue("Url", typeof(String)) + "/yes-authorized.html?posx=" + Base64ForUrlEncode(idOrder.ToString()) + "'>Yes</a>",
                //no = "<a href='" + (string)settingsReader.GetValue("Url", typeof(String)) + "/no-authorized.html?posx=" + Base64ForUrlEncode(idOrder.ToString()) + "'>No</a>"
            });
            return result;
        }
        private string GetDataForCollectionServiceRequestEmailTemplate(int referralId, string emailTemplateContent,
                                                                    ref string emailFrom, ref string displayName, ref string faxnumber)
        {
            var result = "";
            var data = _emailTemplateService.GetDataRequestForCollectionServiceRequest(referralId);
            if (data != null)
            {
                faxnumber = data.CollectionSiteOrPhysicianFax;
                var collectionCityStateZip = "";
                var hasCollectionCity = false;
                var hasCollectionState = false;
                if (!string.IsNullOrEmpty(data.CollectionSiteOrPhysicianCity))
                {
                    hasCollectionCity = true;
                    collectionCityStateZip += data.CollectionSiteOrPhysicianCity;
                }
                if (!string.IsNullOrEmpty(data.CollectionSiteOrPhysicianState))
                {
                    hasCollectionState = true;
                    if (hasCollectionCity)
                    {
                        collectionCityStateZip += ", " + data.CollectionSiteOrPhysicianState;
                    }
                    else
                    {
                        collectionCityStateZip += data.CollectionSiteOrPhysicianState;
                    }
                }
                if (!string.IsNullOrEmpty(data.CollectionSiteOrPhysicianZip))
                {
                    if (hasCollectionState || hasCollectionCity)
                    {
                        collectionCityStateZip += ", " + data.CollectionSiteOrPhysicianZip;
                    }
                    else
                    {
                        collectionCityStateZip += data.CollectionSiteOrPhysicianZip;
                    }
                }
                displayName = data.AssignTo;
                emailFrom = data.AssignEmail;
                var sbMedicalHistData = new StringBuilder();
                string prescription = "";
                if (data.MedicationHistoryEmails != null && data.MedicationHistoryEmails.Count != 0)
                {
                    prescription = "Prescription Medication History:";
                    const string medicalHistHeaderTemplate = @"<table width='700px' cellspacing='0' cellpadding='0' border='1'>  
                                                                            <tr>
                                                                                <th>Drug Name</th>
                                                                                <th>Class</th>
                                                                                <th>Days Supply</th>
                                                                                <th>Dosage</th>
                                                                                <th>Medication history</th>
                                                                                <th>Fill date</th>
                                                                            </tr>

                                                                    ";
                    const string medicalHistFooterTemplate = @"</table>";
                    const string medicalHistItemTemplate = @"<tr>
                                                                    <td>{0}</td>
                                                                    <td>{1}</td>
                                                                    <td>{2}</td>
                                                                    <td>{3}</td>
                                                                    <td>{4}</td>
                                                                    <td>{5}</td>
                                                                </tr>";
                    sbMedicalHistData.AppendLine(medicalHistHeaderTemplate);
                    foreach (var item in data.MedicationHistoryEmails)
                    {
                        var startDayHist = item.StartDay == null
                            ? ""
                            : item.StartDay.GetValueOrDefault().ToString("MMM d, yyyy");
                        sbMedicalHistData.AppendFormat(medicalHistItemTemplate, item.DrugName, item.Class,
                            item.DaySupply, item.Dosage, item.MedicationHist, startDayHist);
                    }
                    sbMedicalHistData.AppendLine(medicalHistFooterTemplate);
                }
                else
                {
                    prescription = "No medication history provided";
                }
                const string referalTitleItemTemplate = @"<td>
                                                            <p><strong><input type='checkbox' {0} /> {1}</strong></p>
                                                         </td>";
                var listTitle = new List<string> { "Medication Monitoring", "Pharmacologic Testing", "Random", "Pre- Employment Testing", "Fitness for Duty", "Other" };
                const string referalTileHeaderTemplate = @"<table  width='700px' cellspacing='0' cellpadding='0' border='1'>";
                const string referalTileFooterTemplate = @"</table>";
                var sbReferalTileData = new StringBuilder();
                sbReferalTileData.AppendLine(referalTileHeaderTemplate);
                for (int i = 0; i < listTitle.Count; i++)
                {
                    if (i % 3 == 0)
                    {
                        sbReferalTileData.AppendLine("<tr>");
                    }
                    var stringChecked = "";
                    if (listTitle[i].Trim().ToUpper() == data.ReferralTitle.Trim().ToUpper())
                    {
                        stringChecked = "checked='checked'";
                    }
                    sbReferalTileData.AppendFormat(referalTitleItemTemplate, stringChecked, listTitle[i].Trim());
                    if (i % 3 == 2)
                    {
                        sbReferalTileData.AppendLine("</tr>");
                    }
                }
                sbReferalTileData.AppendLine(referalTileFooterTemplate);
                var urlLogo = ConfigurationManager.AppSettings["Url"] + "Content/images/logo.png";
                result = TemplateHelpper.FormatTemplateWithContentTemplate(emailTemplateContent, new
                {
                    logo_sterling = urlLogo,
                    prescription,
                    referral_no = data.ReferralNo,
                    claimant_name = data.ClaimantName,
                    payer_name = data.CarrierName,
                    payer_addr = data.CarrierAddr,
                    claim_no = data.ClaimNo,
                    date_of_inju = data.DateOfInjury == null ? "" : data.DateOfInjury.GetValueOrDefault().ToString("MMM d, yyyy"),
                    request_product = data.RequestedProduct,
                    list_medication_hist = sbMedicalHistData.ToString(),
                    referral_title = sbReferalTileData.ToString(),
                    loc_name = data.CollectionSiteOrPhysicianName,
                    location_title = data.HasCollectionSite.GetValueOrDefault() ? "Collection Site Address" : "Office Address",
                    loc_street = data.CollectionSiteOrPhysicianAddress,
                    loc_city_state_zip = collectionCityStateZip,
                    location_phone_title = data.HasCollectionSite.GetValueOrDefault() ? "Collection Site Phone" : "Office Phone",
                    loc_phone = data.CollectionSiteOrPhysicianPhone.ApplyFormatPhone(),
                    control_no = data.ControlNo
                });
            }
            return result;
        }
    }


}