﻿using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.Web.Mvc;
using Framework.DomainModel.Entities;
using Framework.DomainModel.Entities.Common;
using Framework.DomainModel.Entities.Security;
using Framework.DomainModel.ValueObject;
using Framework.Service.Diagnostics;
using Newtonsoft.Json;
using ServiceLayer.Interfaces;
using ServiceLayer.Interfaces.Authentication;
using WorkComp.Attributes;
using WorkComp.Models;
using WorkComp.Models.ReferralSource;


namespace WorkComp.Controllers
{
    public class ReferralSourceController : ApplicationControllerGeneric<ReferralSource, DashboardReferralSourceDataViewModel>
    {
        //
        // GET: /State/
        private readonly IReferralSourceService _referralSourceService;
        public ReferralSourceController(IAuthenticationService authenticationService, IDiagnosticService diagnosticService, IReferralSourceService referralSourceService)
            : base(authenticationService, diagnosticService, referralSourceService)
        {
            _referralSourceService = referralSourceService;
        }
        [AuthorizeContext(DocumentTypeKey = DocumentTypeKey.ReferralSource, OperationAction = OperationAction.View)]
        public ActionResult Index()
        {
            var viewModel = new DashboardReferralSourceIndexViewModel();
            Func<GridViewModel> gridViewModel = () => new GridViewModel
            {
                GridId = "ReferralSourceGrid",
                ModelName = "ReferralSource",
                UseCheckBoxColumn = true,
                UseDeleteColumn = true,
                WidthPopup = 800,
                HeightPopup = 600
            };

            viewModel.GridViewModel = BuildGridViewModel(gridViewModel);
            return View(viewModel);
        }

        [HttpGet]
        [AuthorizeContext(DocumentTypeKey = DocumentTypeKey.ReferralSource, OperationAction = OperationAction.View)]
        public override JsonResult GetDataForGrid(QueryInfo queryInfo)
        {
            return base.GetDataForGrid(queryInfo);
        }

        [AuthorizeContext(DocumentTypeKey = DocumentTypeKey.ReferralSource, OperationAction = OperationAction.Add)]
        public ActionResult Create()
        {
            var viewModel = new DashboardReferralSourceDataViewModel
            {
                SharedViewModel = new DashboardReferralSourceShareViewModel
                {
                    AuthorizationFrom = DateTime.Now,
                    AuthorizationTo = DateTime.Now,
                    IsAuthorization = true,
                    CreateMode = true
                }
            };
            // Check show popup for create from dropdown
            var type = Request.QueryString["type"];
            if (type == "1")
            {
                viewModel.SharedViewModel.ShowPopupType = ShowPopupType.ComboBox;
            }
            return View(viewModel);
        }

        [HttpPost]
        [AuthorizeContext(DocumentTypeKey = DocumentTypeKey.ReferralSource, OperationAction = OperationAction.Add)]
        public int Create(ReferralSourceParameter parameters)
        {
            var savedEntity = CreateMasterFile(parameters);
            return savedEntity.Id;
        }

        [AuthorizeContext(DocumentTypeKey = DocumentTypeKey.ReferralSource, OperationAction = OperationAction.Update)]
        public ActionResult Update(int id)
        {
            var viewModel = GetMasterFileViewModel(id);
            // Check show popup for create from dropdown
            var type = Request.QueryString["type"];
            if (type == "1")
            {
                viewModel.SharedViewModel.ShowPopupType = ShowPopupType.ComboBox;
            }
            return View(viewModel);
        }

        [HttpPost]
        [AuthorizeContext(DocumentTypeKey = DocumentTypeKey.ReferralSource, OperationAction = OperationAction.Update)]
        public ActionResult Update(ReferralSourceParameter parameters)
        {
            return UpdateMasterFile(parameters);
        }

        [AuthorizeContext(DocumentTypeKey = DocumentTypeKey.ReferralSource, OperationAction = OperationAction.Add)]
        public ActionResult Copy(int id)
        {
            var viewModel = GetMasterFileViewModel(id);
            viewModel.SharedViewModel.CreateMode = true;
            viewModel.SharedViewModel.Id = 0;
            viewModel.Id = 0;
            return View("Create",viewModel);
        }

        [AuthorizeContext(DocumentTypeKey = DocumentTypeKey.ReferralSource, OperationAction = OperationAction.Delete)]
        public JsonResult Delete(string models)
        {
            var listItem = JsonConvert.DeserializeObject<Collection<dynamic>>(models);
            if (listItem.Count == 0)
            {
                return Json(false, JsonRequestBehavior.AllowGet);
            }
            var id = (int)(listItem[0].Id);
            _referralSourceService.DeleteById(id);
            return Json(true, JsonRequestBehavior.AllowGet);
        }

        [AuthorizeContext(DocumentTypeKey = DocumentTypeKey.ReferralSource, OperationAction = OperationAction.Delete)]
        public JsonResult DeleteMulti(string selectedRowIdArray, string isDeleteAll)
        {
            return DeleteMultiMasterfile(selectedRowIdArray, isDeleteAll);
        }

        [AuthorizeContext(DocumentTypeKey = DocumentTypeKey.ReferralSource, OperationAction = OperationAction.View)]
        public JsonResult ExportExcel(GridConfigViewModel gridConfig, QueryInfo queryInfo)
        {
            return base.ExportExcelMasterfile(gridConfig, queryInfo);
        }

        protected override IList<ViewColumnViewModel> GetViewColumns()
        {
            var objViewColumn = new List<ViewColumnViewModel>
            {
                new ViewColumnViewModel
                {
                    ColumnOrder = 1,
                    ColumnWidth = 150,
                    Name = "Name",
                    Text = "Name",
                    ColumnJustification = GridColumnJustification.Left,
                },
                new ViewColumnViewModel
                {
                    ColumnOrder = 2,
                    Name = "Type",
                    Text = "Type",
                    ColumnWidth = 80,
                    ColumnJustification = GridColumnJustification.Left
                },
                new ViewColumnViewModel
                {
                    ColumnOrder = 3,
                    Name = "Authorization",
                    Text = "Authorization",
                    ColumnWidth = 170,
                    ColumnJustification = GridColumnJustification.Left,
                    Sortable = false
                },
                new ViewColumnViewModel
                {
                    ColumnOrder = 4,
                    ColumnWidth = 100,
                    Name = "Company",
                    Text = "Company",
                    ColumnJustification = GridColumnJustification.Left
                },
                new ViewColumnViewModel
                {
                    ColumnOrder = 5,
                    Name = "Phone",
                    Text = "Phone",
                    ColumnWidth = 100,
                    ColumnJustification = GridColumnJustification.Left
                },
                new ViewColumnViewModel
                {
                    ColumnOrder = 6,
                    Name = "Email",
                    Text = "Email",
                    ColumnWidth = 100,
                    ColumnJustification = GridColumnJustification.Left
                },
                new ViewColumnViewModel
                {
                    ColumnOrder = 7,
                    Name = "Address",
                    Text = "Address",
                    ColumnWidth = 200,
                    ColumnJustification = GridColumnJustification.Left
                },
                new ViewColumnViewModel
                {
                    ColumnOrder = 8,
                    Name = "State",
                    Text = "State",
                    ColumnWidth = 70,
                    ColumnJustification = GridColumnJustification.Left
                },
                new ViewColumnViewModel
                {
                    ColumnOrder = 9,
                    Name = "City",
                    Text = "City",
                    ColumnWidth = 80,
                    ColumnJustification = GridColumnJustification.Left
                },
                new ViewColumnViewModel
                {
                    ColumnOrder = 10,
                    Name = "Zip",
                    Text = "Zip",
                    ColumnWidth = 50,
                    ColumnJustification = GridColumnJustification.Left
                }
            };
            return objViewColumn;
        }

        [AuthorizeContext(DocumentTypeKey = DocumentTypeKey.ReferralSource, OperationAction = OperationAction.View)]
        public JsonResult GetLookup(LookupQuery queryInfo)
        {
            var selector = new Func<ReferralSource, LookupItemVo>(o => new LookupItemVo
            {
                KeyId = o.Id,
                DisplayName = o.FirstName+" "+o.MiddleName+" "+o.LastName
            });
            return base.GetLookupForEntity(queryInfo, selector);
        }

        
    }
}
