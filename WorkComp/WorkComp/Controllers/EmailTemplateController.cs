﻿using System;
using System.Collections.Generic;
using System.Configuration;
using System.Text;
using System.Web.Mvc;
using Framework.DomainModel.EmailTemplateValueObject;
using Framework.DomainModel.Entities;
using Framework.DomainModel.Entities.Common;
using Framework.DomainModel.Entities.Security;
using Framework.Mapping;
using Framework.Service.Diagnostics;
using ServiceLayer.Interfaces;
using ServiceLayer.Interfaces.Authentication;
using WorkComp.Attributes;
using WorkComp.Models;
using WorkComp.Models.EmailTemplate;

namespace WorkComp.Controllers
{
    public class EmailTemplateController : ApplicationControllerGeneric<EmailTemplate, DashboardEmailTemplateDataViewModel>
    {
        //
        // GET: /State/
        private readonly IEmailTemplateService _emailTemplateService;
        public EmailTemplateController(IAuthenticationService authenticationService, IDiagnosticService diagnosticService, IEmailTemplateService emailTemplateService)
            : base(authenticationService, diagnosticService, emailTemplateService)
        {
            _emailTemplateService = emailTemplateService;
        }
        [AuthorizeContext(DocumentTypeKey = DocumentTypeKey.EmailTemplate, OperationAction = OperationAction.View)]
        public ActionResult Index()
        {
            var viewModel = new DashboardEmailTemplateIndexViewModel();
            Func<GridViewModel> gridViewModel = () => new GridViewModel
            {
                GridId = "EmailTemplateGrid",
                ModelName = "EmailTemplate",
                UseCheckBoxColumn = true,
                UseDeleteColumn = false,
                WidthPopup = 1250,
                HeightPopup = 800,
                CanCopyItem = false,
                CanCreateItem = false,
                CanDeleteMulti = false,
            };

            viewModel.GridViewModel = BuildGridViewModel(gridViewModel);
            return View(viewModel);
        }

        public ActionResult TestEmailTemplate()
        {
            var requestForAuthorizationVo = new RequestForAuthorizationVo();
            _emailTemplateService.ContentHtmlRequestForAuthorization(165, EmailTemplateType.RequestForAuthorization);
            return View(requestForAuthorizationVo);
        }

        [HttpGet]
        [AuthorizeContext(DocumentTypeKey = DocumentTypeKey.EmailTemplate, OperationAction = OperationAction.View)]
        public override JsonResult GetDataForGrid(QueryInfo queryInfo)
        {
            return base.GetDataForGrid(queryInfo);
        }

        [HttpGet]
        [AuthorizeContext(DocumentTypeKey = DocumentTypeKey.Referral, OperationAction = OperationAction.View)]
        public JsonResult GetDataForGridReferralDetail()
        {
            var data = _emailTemplateService.GetDataForGridReferralDetail();
            return Json(data, JsonRequestBehavior.AllowGet);
        }

        [AuthorizeContext(DocumentTypeKey = DocumentTypeKey.EmailTemplate, OperationAction = OperationAction.Update)]
        public ActionResult Update(int id)
        {
            var viewModel = GetMasterFileViewModel(id);
            
            // Check show popup for create from dropdown
            var type = Request.QueryString["type"];
            if (type == "1")
            {
                viewModel.SharedViewModel.ShowPopupType = ShowPopupType.ComboBox;
            }
            return View(viewModel);
        }

        public override DashboardEmailTemplateDataViewModel GetMasterFileViewModel(int id)
        {
            var entity = MasterFileService.GetById(id);
            var urlLogo = ConfigurationManager.AppSettings["Url"] + "Content/images/logo.png";
            entity.Content = entity.Content.Replace("{{logo_sterling}}", urlLogo);
            var viewModel = entity.MapTo<DashboardEmailTemplateDataViewModel>();
            return viewModel;
        }

        [HttpPost]
        [AuthorizeContext(DocumentTypeKey = DocumentTypeKey.EmailTemplate, OperationAction = OperationAction.Update)]
        public ActionResult Update(EmailTemplateParameter parameters)
        {
            byte[] data = Convert.FromBase64String(parameters.SharedParameter);
            parameters.SharedParameter = Encoding.UTF8.GetString(data);
            return UpdateMasterFile(parameters);
        }

        [AuthorizeContext(DocumentTypeKey = DocumentTypeKey.EmailTemplate, OperationAction = OperationAction.View)]
        public JsonResult ExportExcel(GridConfigViewModel gridConfig, QueryInfo queryInfo)
        {
            return base.ExportExcelMasterfile(gridConfig, queryInfo);
        }

        [AuthorizeContext(DocumentTypeKey = DocumentTypeKey.EmailTemplate, OperationAction = OperationAction.Update)]
        public JsonResult ActiveUser(int id)
        {
            if (id == 0)
            {
                return Json(false, JsonRequestBehavior.AllowGet);
            }
            var objEmailTemplate = _emailTemplateService.GetById(id);
            objEmailTemplate.IsActived = !objEmailTemplate.IsActived;
            _emailTemplateService.Update(objEmailTemplate);
            return Json(true, JsonRequestBehavior.AllowGet);
        }

        protected override IList<ViewColumnViewModel> GetViewColumns()
        {
            var objViewColumn = new List<ViewColumnViewModel>
            {
                
                new ViewColumnViewModel
                {
                    ColumnOrder = 1,
                    ColumnWidth = 500,
                    Name = "Title",
                    Text = "Title",
                    ColumnJustification = GridColumnJustification.Left
                },
                new ViewColumnViewModel
                {
                    ColumnOrder = 2,
                    ColumnWidth = 500,
                    Name = "Subject",
                    Text = "Subject",
                    ColumnJustification = GridColumnJustification.Left
                },
                new ViewColumnViewModel
                {
                    ColumnOrder = 3,
                    ColumnWidth = 100,
                    Name = "IsService",
                    Text = "Is Service",
                    ColumnJustification = GridColumnJustification.Center,
                    IsBoolValue = true
                },
                new ViewColumnViewModel
                {
                    ColumnOrder = 4,
                    ColumnWidth = 100,
                    Name = "Active",
                    Text = "Active",
                    CustomTemplate = "activeFieldTemplate",
                    ColumnJustification = GridColumnJustification.Left
                }
            };
            return objViewColumn;
        }

    }
}
