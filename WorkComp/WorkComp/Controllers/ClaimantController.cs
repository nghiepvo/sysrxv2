﻿using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.Web.Mvc;
using AutoMapper;
using Framework.DomainModel.Entities;
using Framework.DomainModel.Entities.Common;
using Framework.DomainModel.Entities.Security;
using Framework.DomainModel.ValueObject;
using Framework.Mapping;
using Framework.Service.Diagnostics;
using Newtonsoft.Json;
using ServiceLayer.Interfaces;
using ServiceLayer.Interfaces.Authentication;
using WorkComp.Attributes;
using WorkComp.Models;
using WorkComp.Models.Claimant;

namespace WorkComp.Controllers
{
    public class ClaimantController : ApplicationControllerGeneric<Claimant, DashboardClaimantDataViewModel>
    {
        //
        // GET: /Claimant/

        //
        // GET: /State/
        private readonly IClaimantService _claimantService;
        public ClaimantController(IAuthenticationService authenticationService, IDiagnosticService diagnosticService, IClaimantService claimantService)
            : base(authenticationService, diagnosticService, claimantService)
        {
            _claimantService = claimantService;
        }

        [AuthorizeContext(DocumentTypeKey = DocumentTypeKey.Claimant, OperationAction = OperationAction.View)]
        public ActionResult Index()
        {
            var viewModel = new DashboardClaimantIndexViewModel();
            Func<GridViewModel> gridViewModel = () => new GridViewModel
            {
                GridId = "ClaimantGrid",
                ModelName = "Claimant",
                UseCheckBoxColumn = true,
                UseDeleteColumn = true,
                WidthPopup = 800,
                HeightPopup = 600
            };

            viewModel.GridViewModel = BuildGridViewModel(gridViewModel);
            return View(viewModel);
        }

        [HttpGet]
        [AuthorizeContext(DocumentTypeKey = DocumentTypeKey.Claimant, OperationAction = OperationAction.View)]
        public override JsonResult GetDataForGrid(QueryInfo queryInfo)
        {
            return base.GetDataForGrid(queryInfo);
        }

        [AuthorizeContext(DocumentTypeKey = DocumentTypeKey.Claimant, OperationAction = OperationAction.Add)]
        public ActionResult Create()
        {
            var viewModel = new DashboardClaimantDataViewModel
            {
                SharedViewModel = new DashboardClaimantShareViewModel
                {
                    
                    Birthday = DateTime.Now,
                    IsUserUpdate = true,
                    CreateMode = true
                }
            };
            // Check show popup for create from dropdown
            var type = Request.QueryString["type"];
            if (type == "1")
            {
                viewModel.SharedViewModel.ShowPopupType = ShowPopupType.ComboBox;
            }
            return View(viewModel);
        }

        [HttpPost]
        [AuthorizeContext(DocumentTypeKey = DocumentTypeKey.Claimant, OperationAction = OperationAction.Add)]
        public int Create(ClaimantParameter parameters)
        {
            var savedEntity = CreateMasterFile(parameters);
            return savedEntity.Id;
        }

        [AuthorizeContext(DocumentTypeKey = DocumentTypeKey.Claimant, OperationAction = OperationAction.Update)]
        public ActionResult Update(int id)
        {
            var viewModel = GetMasterFileViewModel(id);
            // Check show popup for create from dropdown
            var type = Request.QueryString["type"];
            if (type == "1")
            {
                viewModel.SharedViewModel.ShowPopupType = ShowPopupType.ComboBox;
            }
            return View(viewModel);
        }

        [HttpPost]
        [AuthorizeContext(DocumentTypeKey = DocumentTypeKey.Claimant, OperationAction = OperationAction.Update)]
        public ActionResult Update(ClaimantParameter parameters)
        {
            return UpdateMasterFile(parameters);
        }

        [AuthorizeContext(DocumentTypeKey = DocumentTypeKey.Claimant, OperationAction = OperationAction.Add)]
        public ActionResult Copy(int id)
        {
            var viewModel = GetMasterFileViewModel(id);
            viewModel.SharedViewModel.CreateMode = true;
            viewModel.SharedViewModel.Id = 0;
            viewModel.Id = 0;
            return View("Create",viewModel);
        }

        [AuthorizeContext(DocumentTypeKey = DocumentTypeKey.Claimant, OperationAction = OperationAction.Delete)]
        public JsonResult Delete(string models)
        {
            var listItem = JsonConvert.DeserializeObject<Collection<dynamic>>(models);
            if (listItem.Count == 0)
            {
                return Json(false, JsonRequestBehavior.AllowGet);
            }
            var id = (int)(listItem[0].Id);
            _claimantService.DeleteById(id);
            return Json(true, JsonRequestBehavior.AllowGet);
        }

        [AuthorizeContext(DocumentTypeKey = DocumentTypeKey.Claimant, OperationAction = OperationAction.Delete)]
        public JsonResult DeleteMulti(string selectedRowIdArray, string isDeleteAll)
        {
            return DeleteMultiMasterfile(selectedRowIdArray, isDeleteAll);
        }

        [AuthorizeContext(DocumentTypeKey = DocumentTypeKey.Claimant, OperationAction = OperationAction.View)]
        public JsonResult GetLookup(LookupQuery queryInfo)
        {
            var selector = new Func<Claimant, LookupItemVo>(o => new LookupItemVo
            {
                KeyId = o.Id,
                DisplayName = o.FirstName + " " + o.MiddleName + " " + o.LastName
            });
            return base.GetLookupForEntity(queryInfo, selector);
        }

        [AuthorizeContext(DocumentTypeKey = DocumentTypeKey.Claimant, OperationAction = OperationAction.View)]
        public JsonResult GetLookupItem(LookupItem lookupItem)
        {
            var selector = new Func<Claimant, LookupItemVo>(o => new LookupItemVo
            {
                KeyId = o.Id,
                DisplayName = o.FirstName + " " + o.MiddleName + " " + o.LastName
            });
            return base.GetLookupItemForEntity(lookupItem, selector);
        }

        [AuthorizeContext(DocumentTypeKey = DocumentTypeKey.Claimant, OperationAction = OperationAction.View)]
        public JsonResult ExportExcel(GridConfigViewModel gridConfig, QueryInfo queryInfo)
        {
            return base.ExportExcelMasterfile(gridConfig, queryInfo);
        }

        protected override IList<ViewColumnViewModel> GetViewColumns()
        {
            var objViewColumn = new List<ViewColumnViewModel>
            {
                
                new ViewColumnViewModel
                {
                    ColumnOrder = 1,
                    ColumnWidth = 150,
                    Name = "Name",
                    Text = "Name",
                    ColumnJustification = GridColumnJustification.Left,
                    Sortable = false
                },
                new ViewColumnViewModel
                {
                    ColumnOrder = 2,
                    ColumnWidth = 75,
                    Name = "GenderName",
                    Text = "Gender",
                    ColumnJustification = GridColumnJustification.Left,
                    Sortable = false
                },
                new ViewColumnViewModel
                {
                    ColumnOrder = 3,
                    Name = "Dob",
                    Text = "DOB",
                    ColumnWidth = 100,
                    ColumnJustification = GridColumnJustification.Left,
                    Sortable = false
                },
                new ViewColumnViewModel
                {
                    ColumnOrder = 4,
                    Name = "Email",
                    Text = "Email",
                    ColumnWidth = 100,
                    ColumnJustification = GridColumnJustification.Left,
                    Sortable = false
                },
                new ViewColumnViewModel
                {
                    ColumnOrder = 5,
                    Name = "BestContactNumber",
                    Text = "Best Contact Number",
                    ColumnWidth = 150,
                    ColumnJustification = GridColumnJustification.Left,
                    Sortable = false
                },
                new ViewColumnViewModel
                {
                    ColumnOrder = 6,
                    Name = "Ssn",
                    Text = "SSN",
                    ColumnWidth = 75,
                    ColumnJustification = GridColumnJustification.Left,
                    Sortable = false
                },
                new ViewColumnViewModel
                {
                    ColumnOrder = 7,
                    Name = "Language",
                    Text = "Language",
                    ColumnWidth = 100,
                    ColumnJustification = GridColumnJustification.Left,
                    Sortable = false
                },new ViewColumnViewModel
                {
                    ColumnOrder = 8,
                    Name = "FullAddress",
                    Text = "Address",
                    ColumnWidth = 350,
                    ColumnJustification = GridColumnJustification.Left,
                    Sortable = false
                }
            };
            return objViewColumn;
        }
        
        [AuthorizeContext(DocumentTypeKey = DocumentTypeKey.Claimant, OperationAction = OperationAction.View)]
        public ActionResult Detail(int id)
        {
            var data = _claimantService.GetById(id);
            var viewModel = new DashboardClaimainItemDetail();
            if (data != null)
            {
                viewModel = data.MapTo<DashboardClaimainItemDetail>();
            }
            return View(viewModel);
        }
    }
}
