﻿using System;
using System.Collections.Generic;
using System.Web.Mvc;
using Framework.DomainModel.Entities.Common;
using Framework.DomainModel.Entities.Security;
using Framework.DomainModel.Interfaces;
using Framework.DomainModel.ValueObject;
using Framework.Exceptions;
using Framework.Service.Diagnostics;
using Newtonsoft.Json;
using ServiceLayer.Interfaces;
using ServiceLayer.Interfaces.Authentication;
using WorkComp.Attributes;
using WorkComp.Models;
using WorkComp.Models.Export;
using WorkComp.Models.Report;

namespace WorkComp.Controllers
{
    public class ReportController : ApplicationControllerBase
    {
        private readonly IDiagnosticService _diagnosticService;
        private readonly IAuthenticationService _authenticationService;
        private readonly IReportService _reportService;
        public ReportController(IAuthenticationService authenticationService,
            IDiagnosticService diagnosticService, IReportService reportService
            )
            : base(authenticationService, diagnosticService, null)
        {
            _authenticationService = authenticationService;
            _diagnosticService = diagnosticService;
            _reportService = reportService;
        }

        [AuthorizeContext(DocumentTypeKey = DocumentTypeKey.ReferralReport, OperationAction = OperationAction.View)]
        public ActionResult ReferralResult()
        {
            var referralSearchViewModel = Session["ReferralSearchViewModel"] as ReferralSearchViewModel;
            if (referralSearchViewModel == null)
            {
                var user = _authenticationService.GetCurrentUser();
                var exception = new UserVisibleException("UserMustChooseSearchCondition")
                {
                    WorkCompUserName = user.User != null ? user.User.UserName : null
                };
                throw exception;
            }
            var viewModel = new DashboardReferralReportViewModel();
            Func<GridViewModel> gridViewModel = () => new GridViewModel
            {
                GridId = "ReferralReportGrid",
                ModelName = "Report",
                UseCheckBoxColumn = false,
                UseDeleteColumn = false,
                WidthPopup = 800,
                HeightPopup = 600,
                CanCreateItem = false,
                CanCopyItem = false,
                CanDeleteMulti = false
            };

            viewModel.GridViewModel = BuildGridViewModel(gridViewModel);
            viewModel.GridViewModel.DocumentTypeId = (int)DocumentTypeKey.ReferralReport;
            viewModel.GridViewModel.CurrentUser = _authenticationService.GetCurrentUser();
            viewModel.ReferralSearchType = referralSearchViewModel.TypeSearch;
            viewModel.GridViewModel.GridInternalName = "ReferralReportGrid";
            return View(viewModel);
        }
        [HttpGet]
        [AuthorizeContext(DocumentTypeKey = DocumentTypeKey.ReferralReport, OperationAction = OperationAction.View)]
        public override JsonResult GetDataForGrid(QueryInfo queryInfo)
        {
            var referralSearchViewModel = Session["ReferralSearchViewModel"] as ReferralSearchViewModel;
            if (referralSearchViewModel == null)
            {
                var user = _authenticationService.GetCurrentUser();
                var exception = new UserVisibleException("UserMustChooseSearchCondition")
                {
                    WorkCompUserName = user.User != null ? user.User.UserName : null
                };
                throw exception;
            }
            var queryData = _reportService.GetDataForGrid(queryInfo, referralSearchViewModel);
            var clientsJson = Json(queryData, JsonRequestBehavior.AllowGet);
            return clientsJson;
        }

        [AuthorizeContext(DocumentTypeKey = DocumentTypeKey.ReferralReport, OperationAction = OperationAction.View)]
        public JsonResult ExportExcel(GridConfigViewModel gridConfig, QueryInfo queryInfo)
        {
            var data = new ExportExcel();
            var referralSearchViewModel = Session["ReferralSearchViewModel"] as ReferralSearchViewModel;
            if (referralSearchViewModel == null)
            {
                var user = _authenticationService.GetCurrentUser();
                var exception = new UserVisibleException("UserMustChooseSearchCondition")
                {
                    WorkCompUserName = user.User != null ? user.User.UserName : null
                };
                throw exception;
            }
            var dataBind = _reportService.GetDataForGrid(queryInfo, referralSearchViewModel);
            string jsonTemp = JsonConvert.SerializeObject(dataBind);
            var dynamicTemp = JsonConvert.DeserializeObject<dynamic>(jsonTemp);
            string dataTemp = JsonConvert.SerializeObject(dynamicTemp.Data);
            var dataItem = JsonConvert.DeserializeObject<List<dynamic>>(dataTemp);
            //Set order +1; 
            foreach (var column in gridConfig.ViewColumns)
            {
                column.ColumnOrder = column.ColumnOrder + 1;
            }
            data.GridConfigViewModel = gridConfig;
            data.ListDataSource = dataItem;

            var content = RenderRazorViewToString("~/Views/Shared/Export/_BusinessReportExportContent.cshtml", data);
            return Json(new { Item = content, Error = string.Empty }, JsonRequestBehavior.AllowGet);
        }
    }
}