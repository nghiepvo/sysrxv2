﻿using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.Web.Mvc;
using Framework.DomainModel.Entities;
using Framework.DomainModel.Entities.Common;
using Framework.DomainModel.Entities.Security;
using Framework.DomainModel.ValueObject;
using Framework.Service.Diagnostics;
using Newtonsoft.Json;
using ServiceLayer.Interfaces;
using ServiceLayer.Interfaces.Authentication;
using WorkComp.Attributes;
using WorkComp.Models;
using WorkComp.Models.ProductType;

namespace WorkComp.Controllers
{
    public class ProductTypeController : ApplicationControllerGeneric<ProductType, DashboardProductTypeDataViewModel>
    {
        private readonly IProductTypeService _productTypeService;
        public ProductTypeController(IAuthenticationService authenticationService, IDiagnosticService diagnosticService, IProductTypeService productTypeService)
            : base(authenticationService, diagnosticService, productTypeService)
        {
            _productTypeService = productTypeService;
        }

        [AuthorizeContext(DocumentTypeKey = DocumentTypeKey.ProductType, OperationAction = OperationAction.View)]
        public ActionResult Index()
        {
            var viewModel = new DashboardProductTypeIndexViewModel();
            Func<GridViewModel> gridViewModel = () => new GridViewModel
            {
                GridId = "ProductTypeGrid",
                ModelName = "ProductType",
                UseCheckBoxColumn = true,
                UseDeleteColumn = true,
                WidthPopup = 800,
                HeightPopup = 600
            };

            viewModel.GridViewModel = BuildGridViewModel(gridViewModel);
            return View(viewModel);
        }

        [HttpGet]
        [AuthorizeContext(DocumentTypeKey = DocumentTypeKey.ProductType, OperationAction = OperationAction.View)]
        public override JsonResult GetDataForGrid(QueryInfo queryInfo)
        {
            return base.GetDataForGrid(queryInfo);
        }

        [AuthorizeContext(DocumentTypeKey = DocumentTypeKey.ProductType, OperationAction = OperationAction.Delete)]
        public JsonResult DeleteMulti(string selectedRowIdArray, string isDeleteAll)
        {
            return DeleteMultiMasterfile(selectedRowIdArray, isDeleteAll);
        }

        [AuthorizeContext(DocumentTypeKey = DocumentTypeKey.ProductType, OperationAction = OperationAction.Delete)]
        public JsonResult Delete(string models)
        {
            var listItem = JsonConvert.DeserializeObject<Collection<dynamic>>(models);
            if (listItem.Count == 0)
            {
                return Json(false, JsonRequestBehavior.AllowGet);
            }
            var id = (int)(listItem[0].Id);
            _productTypeService.DeleteById(id);
            return Json(true, JsonRequestBehavior.AllowGet);
        }

        [AuthorizeContext(DocumentTypeKey = DocumentTypeKey.ProductType, OperationAction = OperationAction.Add)]
        public ActionResult Create()
        {
            var viewModel = new DashboardProductTypeDataViewModel
            {
                SharedViewModel = new DashboardProductTypeShareViewModel
                {
                    CreateMode = true
                }
            };
            // Check show popup for create from dropdown
            var type = Request.QueryString["type"];
            if (type == "1")
            {
                viewModel.SharedViewModel.ShowPopupType = ShowPopupType.ComboBox;
            }
            return View(viewModel);
        }

        [AuthorizeContext(DocumentTypeKey = DocumentTypeKey.ProductType, OperationAction = OperationAction.Add)]
        public ActionResult Copy(int id)
        {
            var viewModel = GetMasterFileViewModel(id);
            viewModel.SharedViewModel.CreateMode = true;
            viewModel.SharedViewModel.Id = 0;
            viewModel.Id = 0;
            return View("Create", viewModel);
        }

        [HttpPost]
        [AuthorizeContext(DocumentTypeKey = DocumentTypeKey.ProductType, OperationAction = OperationAction.Add)]
        public int Create(ProductTypeParameter parameters)
        {
            var savedEntity = CreateMasterFile(parameters);
            return savedEntity.Id;
        }

        [AuthorizeContext(DocumentTypeKey = DocumentTypeKey.ProductType, OperationAction = OperationAction.Update)]
        public ActionResult Update(int id)
        {
            var viewModel = GetMasterFileViewModel(id);
            // Check show popup for create from dropdown
            var type = Request.QueryString["type"];
            if (type == "1")
            {
                viewModel.SharedViewModel.ShowPopupType = ShowPopupType.ComboBox;
            }
            return View(viewModel);
        }

        [HttpPost]
        [AuthorizeContext(DocumentTypeKey = DocumentTypeKey.ProductType, OperationAction = OperationAction.Update)]
        public ActionResult Update(ProductTypeParameter parameters)
        {
            return UpdateMasterFile(parameters);
        }

        [AuthorizeContext(DocumentTypeKey = DocumentTypeKey.ProductType, OperationAction = OperationAction.View)]
        public JsonResult GetLookup(LookupQuery queryInfo)
        {
            var selector = new Func<ProductType, LookupItemVo>(o => new LookupItemVo
            {
                KeyId = o.Id,
                DisplayName = o.Name
            });
            return base.GetLookupForEntity(queryInfo, selector);
        }

        [AuthorizeContext(DocumentTypeKey = DocumentTypeKey.ProductType, OperationAction = OperationAction.View)]
        public JsonResult ExportExcel(GridConfigViewModel gridConfig, QueryInfo queryInfo)
        {
            return base.ExportExcelMasterfile(gridConfig, queryInfo);
        }

        protected override IList<ViewColumnViewModel> GetViewColumns()
        {
            var objViewColumn = new List<ViewColumnViewModel>
            {
                new ViewColumnViewModel
                {
                    ColumnOrder = 1,
                    ColumnWidth = 500,
                    Name = "Name",
                    Text = "Name",
                    ColumnJustification = GridColumnJustification.Left
                },
                new ViewColumnViewModel
                {
                    ColumnOrder = 2,
                    ColumnWidth = 500,
                    Name = "Payer",
                    Text = "Payer",
                    ColumnJustification = GridColumnJustification.Left
                }
            };
            return objViewColumn;
        }

        [AuthorizeContext(DocumentTypeKey = DocumentTypeKey.ProductType, OperationAction = OperationAction.View)]
        public JsonResult GetLookupItem(LookupItem lookupItem)
        {
            var selector = new Func<ProductType, LookupItemVo>(o => new LookupItemVo
            {
                KeyId = o.Id,
                DisplayName = o.Name
            });
            return base.GetLookupItemForEntity(lookupItem, selector);
        }
    }
}