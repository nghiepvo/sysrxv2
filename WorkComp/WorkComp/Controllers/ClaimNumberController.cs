﻿using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.Web.Mvc;
using Framework.DomainModel.Entities;
using Framework.DomainModel.Entities.Common;
using Framework.DomainModel.Entities.Security;
using Framework.DomainModel.ValueObject;
using Framework.Service.Diagnostics;
using Newtonsoft.Json;
using ServiceLayer.Interfaces;
using ServiceLayer.Interfaces.Authentication;
using WorkComp.Attributes;
using WorkComp.Models;
using WorkComp.Models.ClaimNumber;


namespace WorkComp.Controllers
{
    public class ClaimNumberController : ApplicationControllerGeneric<ClaimNumber, DashboardClaimNumberDataViewModel>
    {
        //
        // GET: /State/
        private readonly IClaimNumberService _claimNumber;
        public ClaimNumberController(IAuthenticationService authenticationService, IDiagnosticService diagnosticService, IClaimNumberService claimNumber)
            : base(authenticationService, diagnosticService, claimNumber)
        {
            _claimNumber = claimNumber;
        }
        [AuthorizeContext(DocumentTypeKey = DocumentTypeKey.ClaimNumber, OperationAction = OperationAction.View)]
        public ActionResult Index()
        {
            var viewModel = new DashboardClaimNumberIndexViewModel();
            Func<GridViewModel> gridViewModel = () => new GridViewModel
            {
                GridId = "ClaimNumberGrid",
                ModelName = "ClaimNumber",
                UseCheckBoxColumn = true,
                UseDeleteColumn = true,
                WidthPopup = 800,
                HeightPopup = 600
            };

            viewModel.GridViewModel = BuildGridViewModel(gridViewModel);
            return View(viewModel);
        }

        [HttpGet]
        [AuthorizeContext(DocumentTypeKey = DocumentTypeKey.ClaimNumber, OperationAction = OperationAction.View)]
        public override JsonResult GetDataForGrid(QueryInfo queryInfo)
        {
            return base.GetDataForGrid(queryInfo);
        }

        [AuthorizeContext(DocumentTypeKey = DocumentTypeKey.ClaimNumber, OperationAction = OperationAction.Add)]
        public ActionResult Create()
        {
            var viewModel = new DashboardClaimNumberDataViewModel
            {
                SharedViewModel = new DashboardClaimNumberShareViewModel
                {
                    LableIsOpen = "Open",
                    OpenDate = DateTime.Now,
                    DateTimeServer = DateTime.Now,
                    EnableIsOpen = false,
                    IsOpen = true,
                    IsUserUpdate = true,
                    CreateMode = true
                }
            };
            // Check show popup for create from dropdown
            var type = Request.QueryString["type"];
            if (type == "1")
            {
                viewModel.SharedViewModel.ShowPopupType = ShowPopupType.ComboBox;
            }
            return View(viewModel);
        }

        [HttpPost]
        [AuthorizeContext(DocumentTypeKey = DocumentTypeKey.ClaimNumber, OperationAction = OperationAction.Add)]
        public int Create(ClaimNumberParameter parameters)
        {
            var savedEntity = CreateMasterFile(parameters);
            return savedEntity.Id;
        }

        [AuthorizeContext(DocumentTypeKey = DocumentTypeKey.ClaimNumber, OperationAction = OperationAction.Update)]
        public ActionResult Update(int id)
        {
            var viewModel = GetMasterFileViewModel(id);
            // Check show popup for create from dropdown
            var type = Request.QueryString["type"];
            if (type == "1")
            {
                viewModel.SharedViewModel.ShowPopupType = ShowPopupType.ComboBox;
            }
            return View(viewModel);
        }

        [HttpPost]
        [AuthorizeContext(DocumentTypeKey = DocumentTypeKey.ClaimNumber, OperationAction = OperationAction.Update)]
        public ActionResult Update(ClaimNumberParameter parameters)
        {
            return UpdateMasterFile(parameters);
        }

        [AuthorizeContext(DocumentTypeKey = DocumentTypeKey.ClaimNumber, OperationAction = OperationAction.Add)]
        public ActionResult Copy(int id)
        {
            var viewModel = GetMasterFileViewModel(id);
            viewModel.SharedViewModel.CreateMode = true;
            viewModel.SharedViewModel.Id = 0;
            viewModel.Id = 0;
            return View("Create",viewModel);
        }

        [AuthorizeContext(DocumentTypeKey = DocumentTypeKey.ClaimNumber, OperationAction = OperationAction.Delete)]
        public JsonResult Delete(string models)
        {
            var listItem = JsonConvert.DeserializeObject<Collection<dynamic>>(models);
            if (listItem.Count == 0)
            {
                return Json(false, JsonRequestBehavior.AllowGet);
            }
            var id = (int)(listItem[0].Id);
            _claimNumber.DeleteById(id);
            return Json(true, JsonRequestBehavior.AllowGet);
        }

        [AuthorizeContext(DocumentTypeKey = DocumentTypeKey.ClaimNumber, OperationAction = OperationAction.Delete)]
        public JsonResult DeleteMulti(string selectedRowIdArray, string isDeleteAll)
        {
            return DeleteMultiMasterfile(selectedRowIdArray, isDeleteAll);
        }

        [AuthorizeContext(DocumentTypeKey = DocumentTypeKey.ClaimNumber, OperationAction = OperationAction.View)]
        public JsonResult ExportExcel(GridConfigViewModel gridConfig, QueryInfo queryInfo)
        {
            return base.ExportExcelMasterfile(gridConfig, queryInfo);
        }

        [AuthorizeContext(DocumentTypeKey = DocumentTypeKey.ClaimNumber, OperationAction = OperationAction.View)]
        public JsonResult TimeServer()
        {
            return Json(new { TimeServer = DateTime.Now.ToString("MM/dd/yyyy HH:mm:ss") }, JsonRequestBehavior.AllowGet);
        }

        protected override IList<ViewColumnViewModel> GetViewColumns()
        {
            var objViewColumn = new List<ViewColumnViewModel>
            {
                new ViewColumnViewModel
                {
                    ColumnOrder = 1,
                    ColumnWidth = 100,
                    Name = "Name",
                    Text = "Name",
                    ColumnJustification = GridColumnJustification.Left,
                    Sortable = false
                },
                new ViewColumnViewModel
                {
                    ColumnOrder = 2,
                    Name = "Claimant",
                    Text = "Claimant",
                    ColumnWidth = 100,
                    ColumnJustification = GridColumnJustification.Left,
                    Sortable = false
                },
                new ViewColumnViewModel
                {
                    ColumnOrder = 3,
                    Name = "Adjuster",
                    Text = "Adjuster",
                    ColumnWidth = 100,
                    ColumnJustification = GridColumnJustification.Left,
                    Sortable = false
                },
                new ViewColumnViewModel
                {
                    ColumnOrder = 4,
                    ColumnWidth = 100,
                    Name = "Payer",
                    Text = "Payer",
                    ColumnJustification = GridColumnJustification.Left,
                    Sortable = false
                },
                new ViewColumnViewModel
                {
                    ColumnOrder = 5,
                    Name = "Branch",
                    Text = "Branch",
                    ColumnWidth = 100,
                    ColumnJustification = GridColumnJustification.Left,
                    Sortable = false
                },
                new ViewColumnViewModel
                {
                    ColumnOrder = 6,
                    Name = "Doi",
                    Text = "DOI",
                    ColumnWidth = 100,
                    ColumnJustification = GridColumnJustification.Left,
                    Sortable = false
                },
                new ViewColumnViewModel
                {
                    ColumnOrder = 7,
                    Name = "State",
                    Text = "Jurisdiction",
                    ColumnWidth = 100,
                    ColumnJustification = GridColumnJustification.Left,
                    Sortable = false
                },
                new ViewColumnViewModel
                {
                    ColumnOrder = 8,
                    Name = "SpecialInstructions",
                    Text = "Special Instructions",
                    ColumnWidth = 325,
                    ColumnJustification = GridColumnJustification.Left,
                    Sortable = false
                }
                ,
                new ViewColumnViewModel
                {
                    ColumnOrder = 9,
                    Name = "StatusName",
                    Text = "Status",
                    ColumnWidth = 75,
                    ColumnJustification = GridColumnJustification.Left,
                    Sortable = false
                }
            };
            return objViewColumn;
        }

        [AuthorizeContext(DocumentTypeKey = DocumentTypeKey.ClaimNumber, OperationAction = OperationAction.View)]
        public JsonResult GetLookup(LookupQuery queryInfo)
        {
            var selector = new Func<ClaimNumber, LookupItemVo>(o => new LookupItemVo
            {
                KeyId = o.Id,
                DisplayName = o.Name
            });
            return base.GetLookupForEntity(queryInfo, selector);
        }

        [AuthorizeContext(DocumentTypeKey = DocumentTypeKey.ClaimNumber, OperationAction = OperationAction.View)]
        public JsonResult GetLookupItem(LookupItem lookupItem)
        {
            var selector = new Func<ClaimNumber, LookupItemVo>(o => new LookupItemVo
            {
                KeyId = o.Id,
                DisplayName = o.Name
            });
            return base.GetLookupItemForEntity(lookupItem, selector);
        }

    }
}
