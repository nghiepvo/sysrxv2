﻿using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using AutoMapper;
using Common;
using Framework.DomainModel.Entities;
using Framework.DomainModel.Entities.Common;
using Framework.DomainModel.Entities.Security;
using Framework.Mapping;
using Newtonsoft.Json;
using ServiceLayer.Interfaces;
using WorkComp.Attributes;
using WorkComp.Models.Referral;

namespace WorkComp.Controllers
{
    public partial class ReferralController
    {
        private readonly IReferralAttachmentService _fileAttachmentService;
        [AuthorizeContext(DocumentTypeKey = DocumentTypeKey.Referral, OperationAction = OperationAction.View)]
        public JsonResult GetAttachments(int referralId)
        {
            Func<ReferralAttachment, ReferralAttachment> selector = f => new ReferralAttachment
            {
                Id = f.Id,
                AttachedFileName = f.AttachedFileName,
                AttachedFileSize = f.AttachedFileSize,
                ReferralId = f.ReferralId,
                RowGUID = f.RowGUID
            };

            var count = 0;
            var queryResult = _fileAttachmentService.GetAttachments(referralId, selector);

            var files = Mapper.Map<IList<ReferralAttachment>, IList<FileAttachment>>(queryResult);
            if (files != null) count = files.Count();

            var result = new { Data = files, TotalRowCount = count };
            return Json(result, JsonRequestBehavior.AllowGet);
        }
        public string TransferUploadPath
        {
            get
            {
                return ControllerContext.HttpContext.Server == null
                           ? null
                           : ControllerContext.HttpContext.Server.MapPath(
                              UserUploadPathFile.UploadFolder);
            }
        }
        [HttpPost]
        [AuthorizeContext(DocumentTypeKey = DocumentTypeKey.Referral, OperationAction = OperationAction.View)]
        public JsonResult UploadAttachment(HttpPostedFileBase files, string oldFile, string oldToken)
        {
            _fileAttachmentService.TransferUploadPath = TransferUploadPath;
            var token = Guid.Empty;
            if (!string.IsNullOrEmpty(oldToken))
                token = Guid.Parse(oldToken);

            var attachment = _fileAttachmentService.UploadFile(files, oldFile, token);
           
            return Json(
                new
                {
                    status = 0,
                    data = attachment.MapTo<FileAttachment>()
                }, "text/plain");
        }
        [HttpPost]
        [AuthorizeContext(DocumentTypeKey = DocumentTypeKey.Referral, OperationAction = OperationAction.View)]
        public ActionResult RemoveAttachment(string rowGuid, string fileName,bool isSaveToDatabase,int referralId)
        {
            _fileAttachmentService.TransferUploadPath = TransferUploadPath;
            _fileAttachmentService.RemoveFile(fileName, Guid.Parse(rowGuid));
            if (isSaveToDatabase && referralId != 0)
            {
                // Remove attachment file in db
                _referralService.RemoveAttachmentFile(referralId, rowGuid, fileName);
            }
            return Json(new { Error = string.Empty, Data = string.Empty }, JsonRequestBehavior.AllowGet);
        }

        [HttpPost]
        [AuthorizeContext(DocumentTypeKey = DocumentTypeKey.Referral, OperationAction = OperationAction.View)]
        public ActionResult AddAttachmentToDb(string rowGuid, string fileName,int fileSize, bool isSaveToDatabase, int referralId)
        {
            if (isSaveToDatabase && referralId != 0)
            {
                // Remove attachment file in db
                var guidRow = Guid.Parse(rowGuid);
                _fileAttachmentService.TransferUploadPath = TransferUploadPath;
                var fileContent = _fileAttachmentService.ReadFile(fileName, guidRow);
                var objAdd = new ReferralAttachment
                {
                    AttachedFileContent = fileContent,
                    AttachedFileName = fileName,
                    AttachedFileSize = fileSize,
                    ReferralId = referralId,
                    RowGUID = guidRow
                };
                
                _referralService.AddReferralAttachment(objAdd);
            }
            return Json(new { Error = string.Empty, Data = string.Empty }, JsonRequestBehavior.AllowGet);
        }

        [AuthorizeContext(DocumentTypeKey = DocumentTypeKey.Referral, OperationAction = OperationAction.View)]
        [HttpGet]
        public void DownloadAttachment(string fileName, string rowGuid)
        {
            if (string.IsNullOrEmpty(rowGuid))
                throw new Exception("Invalid download token.");

            if (string.IsNullOrEmpty(fileName))
                throw new Exception("Invalid file name to download.");

            _fileAttachmentService.TransferUploadPath = TransferUploadPath;
            _fileAttachmentService.DownloadFile(ControllerContext.HttpContext, fileName, Guid.Parse(rowGuid));
        }


        private static string FormatItemStringToJson(string item)
        {
            if (item.StartsWith("{")) item = item.Substring(1, item.Length - 2);
            return "{" + item + "}";
        }

        public void ProcessForFileAttachment(DashboardReferralDataViewModel viewModel, string fileAttachmentJson)
        {
            var fileAttachments = new Collection<FileAttachment>();

            if (string.IsNullOrEmpty(fileAttachmentJson))
                return;

            var fileAttachmentDictionary = JsonConvert.DeserializeObject<Dictionary<string, object>>(fileAttachmentJson);
            var updatedListJson = fileAttachmentDictionary.SingleOrDefault(g => g.Key.Equals("updated")).Value.ToString();
            var createdListJson = fileAttachmentDictionary.SingleOrDefault(g => g.Key.Equals("created")).Value.ToString();
            var destroyedListJson =
                fileAttachmentDictionary.SingleOrDefault(g => g.Key.Equals("destroyed")).Value.ToString();

            var serializedAttachments = JsonConvert.DeserializeObject<Collection<FileAttachment>>(updatedListJson);

            foreach (var attachment in serializedAttachments)
            {
                var item =
                    string.Format(
                        "'Id': '{0}', 'AttachedFileName': '{1}' , 'RowGUID': '{2}' , 'FileSize': '{3}'",
                        attachment.Id, attachment.AttachedFileName, attachment.RowGuid, attachment.FileSize);

                fileAttachments.Add(JsonConvert.DeserializeObject<FileAttachment>(FormatItemStringToJson(item)));
            }

            serializedAttachments = JsonConvert.DeserializeObject<Collection<FileAttachment>>(createdListJson);

            foreach (var attachment in serializedAttachments)
            {
                var item =
                    string.Format(
                        "'AttachedFileName': '{0}' , 'RowGUID': '{1}' , 'FileSize': '{2}'",
                        attachment.AttachedFileName, attachment.RowGuid, attachment.FileSize);

                fileAttachments.Add(JsonConvert.DeserializeObject<FileAttachment>(FormatItemStringToJson(item)));
            }

            serializedAttachments = JsonConvert.DeserializeObject<Collection<FileAttachment>>(destroyedListJson);
            foreach (var attachment in serializedAttachments)
            {
                var item =
                    string.Format(
                     "'Id': '{0}', 'AttachedFileName': '{1}' , 'RowGUID': '{2}' , 'FileSize': '{3}' , 'IsDeleted':{4}",
                        attachment.Id, attachment.AttachedFileName, attachment.RowGuid, attachment.FileSize, "true");
                var destroyItem =
                    JsonConvert.DeserializeObject<FileAttachment>(FormatItemStringToJson(item));

                if (destroyItem.Id != 0)
                {
                    fileAttachments.Add(destroyItem);
                }
            }
            viewModel.Attachments = new List<ReferralAttachment>();
            foreach (var item in fileAttachments)
            {
                var objAdd = item.MapTo<ReferralAttachment>();
                viewModel.Attachments.Add(objAdd);
            }
        }
    }
}