﻿using System.Collections.Generic;
using System.Linq;
using System.Web.Mvc;
using Framework.DomainModel.Entities;
using Framework.DomainModel.Entities.Common;
using Framework.DomainModel.Entities.Security;
using Newtonsoft.Json;
using ServiceLayer.Interfaces;
using WorkComp.Attributes;
using WorkComp.Models.Referral;

namespace WorkComp.Controllers
{
    public partial class ReferralController
    {
        private readonly IPanelTypeService _panelTypeService;
        private readonly ICollectionSiteService _collectionSiteService;
        private readonly INpiNumberService _npiNumberService;

        private static void ProcessForTestingInfo(DashboardReferralDataViewModel viewModel, string testingInfo)
        {
            var testingInfoData = JsonConvert.DeserializeObject<TestingInfoPartialForReferral>(testingInfo);
            viewModel.TestingInfo.PanelTypeId = testingInfoData.PanelTypeId;
            viewModel.TestingInfo.CollectionMethodId = testingInfoData.CollectionMethodId;
            viewModel.TestingInfo.CollectionSiteId = testingInfoData.CollectionSiteId;
            viewModel.TestingInfo.CollectionSiteSpecialInstructions = testingInfoData.CollectionSiteSpecialInstructions;
            viewModel.TestingInfo.CollectionSiteDate = testingInfoData.CollectionSiteDate;
            viewModel.TestingInfo.CollectionSitePhone = testingInfoData.CollectionSitePhone;
            viewModel.TestingInfo.CollectionSiteFax = testingInfoData.CollectionSiteFax;
            viewModel.TestingInfo.CollectionSiteAddress = testingInfoData.CollectionSiteAddress;
            viewModel.TestingInfo.TreatingPhysicianId = testingInfoData.TreatingPhysicianId;
            viewModel.TestingInfo.TreatingPhysicianSpecialHandling = testingInfoData.TreatingPhysicianSpecialHandling;
            viewModel.TestingInfo.NextMdVisitDate = testingInfoData.NextMdVisitDate;
            viewModel.TestingInfo.TreatingPhysicianPhone = testingInfoData.TreatingPhysicianPhone;
            viewModel.TestingInfo.TreatingPhysicianFax = testingInfoData.TreatingPhysicianFax;
            viewModel.TestingInfo.TreatingPhysicianAddress = testingInfoData.TreatingPhysicianAddress;
            viewModel.TestingInfo.TreatingPhysicianEmail = testingInfoData.TreatingPhysicianEmail;
            viewModel.TestingInfo.CheckOralFluid = testingInfoData.CheckOralFluid;
            viewModel.TestingInfo.CheckBlood = testingInfoData.CheckBlood;
            viewModel.TestingInfo.CheckHair = testingInfoData.CheckHair;
            viewModel.TestingInfo.CheckUrine = testingInfoData.CheckUrine;
        }

        [AuthorizeContext(DocumentTypeKey = DocumentTypeKey.PanelType, OperationAction = OperationAction.View)]
        public JsonResult GetInfoWhenChangePanelType(int? id)
        {
            if (id.GetValueOrDefault() == 0)
            {
                return Json("", JsonRequestBehavior.AllowGet);
            }
            var objData = _panelTypeService.GetInfoWhenChangePanelTypeInReferral(id.GetValueOrDefault());
            return objData != null ? Json(objData, JsonRequestBehavior.AllowGet) : Json("", JsonRequestBehavior.AllowGet);
        }

        [AuthorizeContext(DocumentTypeKey = DocumentTypeKey.CollectionSite, OperationAction = OperationAction.View)]
        public JsonResult GetInfoWhenChangeCollectionSite(int? id)
        {
            if (id.GetValueOrDefault() == 0)
            {
                return Json("", JsonRequestBehavior.AllowGet);
            }
            var objData = _collectionSiteService.GetInfoWhenChangeCollectionSiteInReferral(id.GetValueOrDefault());
            return objData != null ? Json(objData, JsonRequestBehavior.AllowGet) : Json("", JsonRequestBehavior.AllowGet);
        }

        [AuthorizeContext(DocumentTypeKey = DocumentTypeKey.NpiNumber, OperationAction = OperationAction.View)]
        public JsonResult GetInfoWhenChangeTreatingPhysician(int? id)
        {
            if (id.GetValueOrDefault() == 0)
            {
                return Json("", JsonRequestBehavior.AllowGet);
            }
            var objData = _npiNumberService.GetInfoWhenChangeTreatingPhysicianInReferral(id.GetValueOrDefault());
            return objData != null ? Json(objData, JsonRequestBehavior.AllowGet) : Json("", JsonRequestBehavior.AllowGet);
        }

        private static bool CheckUserChangeCollectionSiteInfo(
            List<ReferralCollectionSite> listCollectionSiteOld,
            List<ReferralNpiNumber> listNpiNumberOld,
            bool isCollectionSiteOld, TestingInfoPartialForReferral testingInfo)
        {
            if (listCollectionSiteOld.Count == 0 && listNpiNumberOld.Count == 0)
            {
                return true;
            }
            if (isCollectionSiteOld)
            {
                // Check have change in collection site
                if (listCollectionSiteOld.Count == 0)
                {
                    return true;
                }
                var objCollectionSite = listCollectionSiteOld[0];
                return !(objCollectionSite.Address == testingInfo.CollectionSiteAddress
                       && objCollectionSite.Fax == testingInfo.CollectionSiteFax
                       && objCollectionSite.Phone == testingInfo.CollectionSitePhone
                       && objCollectionSite.SpecialInstructions == testingInfo.CollectionSiteSpecialInstructions
                       && objCollectionSite.CollectionDate == testingInfo.CollectionSiteDate
                       && objCollectionSite.CollectionSiteId == testingInfo.CollectionSiteId);
            }
            else
            {
                // Check have change in collection site
                if (listNpiNumberOld.Count == 0)
                {
                    return true;
                }
                var objNpiNumber = listNpiNumberOld[0];
                return !(objNpiNumber.Address == testingInfo.TreatingPhysicianAddress
                       && objNpiNumber.Fax == testingInfo.TreatingPhysicianFax
                       && objNpiNumber.Phone == testingInfo.TreatingPhysicianPhone
                       && objNpiNumber.SpecialHandling == testingInfo.TreatingPhysicianSpecialHandling
                       && objNpiNumber.NextMdVisitDate == testingInfo.NextMdVisitDate
                       && objNpiNumber.NpiNumberId == testingInfo.TreatingPhysicianId
                       && objNpiNumber.Email == testingInfo.TreatingPhysicianEmail);
            }
        }

        private static bool CheckUserChangeCollectionMethod(
            bool isCollectionSiteOld, TestingInfoPartialForReferral testingInfo)
        {
            var isCollectionSiteNew = testingInfo.CollectionMethodId == 1;
            if (isCollectionSiteOld != isCollectionSiteNew)
            {
                return true;
            }
            return false;
        }
    }
}