﻿using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.Linq;
using System.Text;
using System.Web;
using System.Web.Mvc;
using Framework.DomainModel.Entities;
using Framework.DomainModel.Entities.Common;
using Framework.DomainModel.Entities.Security;
using Framework.Service.Diagnostics;
using Newtonsoft.Json;
using ServiceLayer.Interfaces;
using ServiceLayer.Interfaces.Authentication;
using WorkComp.Attributes;
using WorkComp.Models;
using WorkComp.Models.Comment;

namespace WorkComp.Controllers
{
    public class CommentController : ApplicationControllerGeneric<Comment, DashboardCommentDataViewModel>
    {
        private readonly ICommentService _commentService;
        public CommentController(IAuthenticationService authenticationService, IDiagnosticService diagnosticService, ICommentService commentService)
            : base(authenticationService, diagnosticService, commentService)
        {
            _commentService = commentService;
        }

        //Fixed

        [AuthorizeContext(DocumentTypeKey = DocumentTypeKey.Referral, OperationAction = OperationAction.View)]
        public ActionResult Index()
        {
            var viewModel = new DashboardCommentIndexViewModel();
            Func<GridViewModel> gridViewModel = () => new GridViewModel
            {
                GridId = "CommentGrid",
                ModelName = "Comment",
                UseCheckBoxColumn = true,
                UseDeleteColumn = true,
                WidthPopup = 800,
                HeightPopup = 600,
                CanCopyItem = true,
                CanDeleteMulti = true,
                CanCreateItem = true
            };

            viewModel.GridViewModel = BuildGridViewModel(gridViewModel);
            return View(viewModel);
        }

        [AuthorizeContext(DocumentTypeKey = DocumentTypeKey.Referral, OperationAction = OperationAction.Add)]
        public ActionResult Create()
        {
            var viewModel = new DashboardCommentDataViewModel
            {
                SharedViewModel = new DashboardCommentShareViewModel
                {
                    CreateMode = true
                }
            };
            // Check show popup for create from dropdown
            var type = Request.QueryString["type"];
            if (type == "1")
            {
                viewModel.SharedViewModel.ShowPopupType = ShowPopupType.ComboBox;
            }
            return View(viewModel);
        }

        [HttpPost]
        [AuthorizeContext(DocumentTypeKey = DocumentTypeKey.Referral, OperationAction = OperationAction.Add)]
        public int Create(CommentParameter parameters)
        {
            byte[] data = Convert.FromBase64String(parameters.SharedParameter);
            parameters.SharedParameter = Encoding.UTF8.GetString(data);
            var savedEntity = CreateMasterFile(parameters);
            return savedEntity.Id;
        }

        [HttpGet]
        [AuthorizeContext(DocumentTypeKey = DocumentTypeKey.Referral, OperationAction = OperationAction.View)]
        public override JsonResult GetDataForGrid(QueryInfo queryInfo)
        {
            return base.GetDataForGrid(queryInfo);
        }

        [AuthorizeContext(DocumentTypeKey = DocumentTypeKey.Referral, OperationAction = OperationAction.Update)]
        public ActionResult Update(int id)
        {
            var viewModel = GetMasterFileViewModel(id);
            // Check show popup for create from dropdown
            var type = Request.QueryString["type"];
            if (type == "1")
            {
                viewModel.SharedViewModel.ShowPopupType = ShowPopupType.ComboBox;
            }
            return View(viewModel);
        }

        [HttpPost]
        [AuthorizeContext(DocumentTypeKey = DocumentTypeKey.Referral, OperationAction = OperationAction.Update)]
        public ActionResult Update(CommentParameter parameters)
        {
            return UpdateMasterFile(parameters);
        }

        [AuthorizeContext(DocumentTypeKey = DocumentTypeKey.Referral, OperationAction = OperationAction.Delete)]
        public JsonResult Delete(string models)
        {
            var listItem = JsonConvert.DeserializeObject<Collection<dynamic>>(models);
            if (listItem.Count == 0)
            {
                return Json(false, JsonRequestBehavior.AllowGet);
            }
            var id = (int)(listItem[0].Id);
            _commentService.DeleteById(id);
            return Json(true, JsonRequestBehavior.AllowGet);
        }

        [AuthorizeContext(DocumentTypeKey = DocumentTypeKey.Referral, OperationAction = OperationAction.Delete)]
        public JsonResult DeleteMulti(string selectedRowIdArray, string isDeleteAll)
        {
            return DeleteMultiMasterfile(selectedRowIdArray, isDeleteAll);
        }

        protected override IList<ViewColumnViewModel> GetViewColumns()
        {
            var objViewColumn = new List<ViewColumnViewModel>
            {
                new ViewColumnViewModel
                {
                    ColumnOrder = 1,
                    ColumnWidth = 300,
                    Name = "CommentContent",
                    Text = "Comment",
                    ColumnJustification = GridColumnJustification.Left
                }
            };
            return objViewColumn;
        }

        [AuthorizeContext(DocumentTypeKey = DocumentTypeKey.Referral, OperationAction = OperationAction.View)]
        public JsonResult GetCommentWhenChangeReferralTask(int? id)
        {
            if (id.GetValueOrDefault() == 0)
            {
                return Json("", JsonRequestBehavior.AllowGet);
            }
            var objData = _commentService.GetCommentWhenChangeReferral(id.GetValueOrDefault());
            return objData != null ? Json(objData, JsonRequestBehavior.AllowGet) : Json("", JsonRequestBehavior.AllowGet);
        }
    }
}
