﻿
//using the Revealing Prototype Structure

//All state is contained in the constructor
var DualSelectListBoxViewModel = function(name, keyId, treeViewModel, childGrid, parentGrid, window, offset) {
    this.treeViewModel = treeViewModel;
    this.treeview = treeViewModel.treeView;
    this.childGridControl = childGrid.getAvailableChildGridControl;
    this.parentGridControl = parentGrid.getAvailableParentGridControl;
    this.childGrid = childGrid;
    this.parentGrid = parentGrid;
    this.modelName = ko.observable(name);
    this.childTypes = ko.observable([]);
    this.keyID = ko.observable(keyId);
    this.window = window;
    this.offset = offset;
};


//Functions are only loaded into memory once. Allows for public and private members
DualSelectListBoxViewModel.prototype = function () {

    //private members
    var addParent = function() {
        var grid = this.parentGridControl;
        var treeview = this.treeview;
        var gridItems = [];
        _.each(grid.select(), function(value, key) {
            var dataItem = grid.dataItem(value);
            addParentItem(dataItem, gridItems, treeview);
        });
        treeview.expand(".k-item");
        this.parentGrid.selectParentAction(gridItems);

        removeDisabledSave.call(this, gridItems);
    },
        addChild = function() {
            var gridItems = [];
            var grid = this.childGridControl;
            var treeview = this.treeview;
            var selectedNode = treeview.select();

            if (selectedNode.length == 0) {
                return;
            }
            var parent = selectedNode.parent();
            _.each(grid.select(), function(value, key) {
                var dataItem = grid.dataItem(value);
                if (hasChildTypes(parent, treeview) && treeview.dataItem(parent[0]).childTypes.length > 0) {
                    selectedNode = treeview.findByUid(treeview.dataItem(parent[0]).uid);
                }
                addChildItem(selectedNode, dataItem, gridItems, treeview, false);
            });


            treeview.expand(".k-item");
            this.childGrid.selectChildAction(gridItems);

            removeDisabledSave(gridItems);
        },
        addAllChildren = function() {
            var gridItems = [];
            var grid = this.childGridControl;
            var treeview = this.treeview;
            var selectedNode = treeview.select();

            if (selectedNode.length == 0 || selectedNode.length > 1) {
                return;
            }

            _.each(grid._data, function(dataItem, key) {
                addChildItem(selectedNode, dataItem, gridItems, treeview);
            });

            treeview.expand(".k-item");
            this.childGrid.selectChildAction(gridItems);

            removeDisabledSave.call(this, gridItems);
        },
        addAllParents = function() {
            var gridItems = [];
            var grid = this.parentGridControl;
            var treeview = this.treeview;
            _.each(grid._data, function(dataItem, key) {
                addParentItem(dataItem, gridItems, treeview, false);
            });

            treeview.expand(".k-item");

            this.parentGrid.selectParentAction(gridItems);

            removeDisabledSave.call(this, gridItems);
        },
        removeSelected = function() {
            var treeview = this.treeview;
            var selectedNode = treeview.select();
            var treeViewRemovedParentItems = [];
            var treeViewRemovedSelectedChildItems = [];
            if (selectedNode.length == 0) {
                return;
            }

            for (var index = 0; index < selectedNode.length; index++) {

                var selectedNodeItem = treeview.dataItem(selectedNode[index]);

                if (selectedNodeItem.ChildDisplayName == undefined) {
                    var childNodeDetails = getNodeDetails(selectedNodeItem);

                    if (childNodeDetails.childNodeItems != undefined) {

                        var treeViewRemovedChildItems = [];

                        for (var i1 = 0; i1 < childNodeDetails.childNodeItems.length; i1++) {
                            removeChildItem(childNodeDetails.childNodeItems[i1], treeViewRemovedChildItems, treeview)
                            if (childNodeDetails.subractIterator) {
                                i1--;
                            }
                        }
                        this.childGrid.removeChildAction(treeViewRemovedChildItems);
                    }
                    disableParentMoveButton(false);
                    removeParentItem(selectedNodeItem, treeViewRemovedParentItems, treeview);
                } else {
                    removeChildItem(selectedNodeItem, treeViewRemovedSelectedChildItems, treeview);
                }
            }

            this.childGrid.removeChildAction(treeViewRemovedSelectedChildItems);
            this.parentGrid.removeParentAction(treeViewRemovedParentItems);
            removeDisabledSave(treeViewRemovedParentItems);
            removeDisabledSave(treeViewRemovedSelectedChildItems);
        },
        removeAll = function() {
            var treeview = this.treeview;
            var items = treeview.dataSource.view();
            var treeViewRemovedParentItems = [];
            for (var index = 0; index < items.length; index++) {
                var parentNodeItem = items[index];
                var parentNodeDetails = getNodeDetails(parentNodeItem);

                if (parentNodeDetails.childNodeItems != undefined) {
                    var treeViewRemovedChildItems = [];
                    for (var i1 = 0; i1 < parentNodeDetails.childNodeItems.length; i1++) {
                        removeChildItem(parentNodeDetails.childNodeItems[i1], treeViewRemovedChildItems, treeview)
                        if (parentNodeDetails.subractIterator) {
                            i1--;
                        }
                    }
                    removeDisabledSave(treeViewRemovedChildItems);
                    this.childGrid.removeChildAction(treeViewRemovedChildItems);
                }

                Postbox.messages.notifySubscribers(treeViewRemovedParentItems, "RemoveParentAction");

                disableParentMoveButton(false);
                removeParentItem(parentNodeItem, treeViewRemovedParentItems, treeview);
            }

            this.parentGrid.removeParentAction(treeViewRemovedParentItems);
            removeDisabledSave(treeViewRemovedParentItems);
        },
        addParentItem = function(dataItem, gridItems, treeview) {
            var itemToAdd = { ParentDisplayName: getReplacedTextAdd(dataItem.ParentDisplayName), KeyID: dataItem.KeyID, Active: dataItem.Active };
            gridItems.push(itemToAdd);
            treeview.append(itemToAdd);
        },
        removeDisabledSave = function(gridItems) {
            if (gridItems !== undefined && gridItems.length > 0) {
                EnableCreateFooterButton(true);
            }
        },
        getReplacedTextAdd = function(value) {
            //return value;
            return value.replace("<", "&lt;").replace(">", "&gt;");
        },
        getReplacedTextRemove = function(value) {
            //return value;
            return value.replace("&lt;", "<").replace("&gt;", ">");
        },
        sortTreeChildren = function(selectedNode, treeview) {
            var unsortedList = []
            var ids = []
            var sortedList = [];
            var childItems = treeview.dataItem(selectedNode).childTypes;
            _.each(childItems, function(v, k) {
                unsortedList.push(v.ChildDisplayName + "::ID::" + v.KeyID);
                ids.push(v.uid);
            });
            removeAllChildrenFromSelectedNode(ids, treeview);
            sortedList = unsortedList.sort();
            addSortedChildrenToSelectedNode(sortedList, selectedNode, treeview);
        },
        removeAllChildrenFromSelectedNode = function(ids, treeview) {
            _.each(ids, function(v, k) {
                var removeNode = treeview.findByUid(v);
                treeview.remove(removeNode);
            });
        },
        addSortedChildrenToSelectedNode = function(sortedList, selectedNode, treeview) {
            _.each(sortedList, function(v, k) {
                var split = v.split("::ID::");
                var displayName = split[0]
                var keyId = split[1];
                var ita = { ChildDisplayName: displayName, KeyID: keyId };
                treeview.append(ita, selectedNode);
            })
        },
        hasChildTypes = function(parent, treeview) {
            return (parent != undefined && treeview.dataItem(parent[0]) != undefined && treeview.dataItem(parent[0]).childTypes != undefined);
        },
        addChildItem = function(selectedNode, dataItem, gridItems, treeview) {
            var itemToAdd = { ChildDisplayName: getReplacedTextAdd(dataItem.ChildDisplayName), KeyID: dataItem.KeyID, Active: dataItem.Active };
            gridItems.push(itemToAdd);
            treeview.append(itemToAdd, selectedNode);
            Postbox.messages.notifySubscribers(dataItem, "SelectChildAction");
        },
        removeChildItemFromId = function(id, gridItems, treeview) {
            var removeChildNode = treeview.findByUid(id);
            var childData = treeview.dataItem(removeChildNode);
            var displayName = getReplacedTextRemove(childData.ChildDisplayName);
            var itemToRemove = { ChildDisplayName: displayName, KeyID: childData.KeyID,  Active: childData.Active };
            gridItems.push(itemToRemove);
            treeview.remove(removeChildNode);
        },
        removeChildItem = function(dataItem, gridItems, treeview) {
            var removeChildNode = treeview.findByUid(dataItem.uid);
            var itemToRemove = { ChildDisplayName: getReplacedTextRemove(dataItem.ChildDisplayName), KeyID: dataItem.KeyID, Active: dataItem.Active };
            gridItems.push(itemToRemove);
            treeview.remove(removeChildNode);
        },
        removeParentItem = function(dataItem, gridItems, treeview) {
            var parentItemToRemove = { ParentDisplayName: getReplacedTextRemove(dataItem.ParentDisplayName), KeyID: dataItem.KeyID, Active: dataItem.Active };
            gridItems.push(parentItemToRemove);
            var removeParentNode = treeview.findByUid(dataItem.uid);
            treeview.remove(removeParentNode);

            Postbox.messages.notifySubscribers(gridItems, "RemoveParentAction");
        },
        getNodeDetails = function(selectedNodeItem) {
            var childNodeItems = selectedNodeItem._childrenOptions.data.childTypes;
            var subractIterator = false;
            if (childNodeItems == undefined) {
                childNodeItems = selectedNodeItem.childTypes;
                subractIterator = true;
            }
            var childNodeDetails = { childNodeItems: childNodeItems, subractIterator: subractIterator };
            return childNodeDetails;
        },
        getParentData = function(dataItem, treeview) {
            var childNode = treeview.findByUid(dataItem.uid);
            var parentNode = treeview.parent(childNode);
            var parentData = treeview.dataItem(parentNode);
            var parentData = { node: parentNode, data: parentData }
            return parentData;
        },
        disableParentMoveButton = function(disable) {
            $(".left-dual-select-list-box-top .move").attr("disabled", disable);
        },
        disableParentMoveAllButton = function(disable) {
            $(".left-dual-select-list-box-top .moveall").attr("disabled", disable);
        },
        disableChildMoveButton = function(disable) {

        },
        disableChildMoveAllButton = function(disable) {
            $(".left-dual-select-list-box-bottom .buttons .moveall").attr("disabled", disable);
        },
        resizeComponents = function(offset) {

            if (offset === undefined) {
                offset = this.offset;
            }

            var height = $(this.window).height() - offset;

            //var interim = (1550 - height) / 15.9;
            var gridSize = (height / 2) - 40;
            var interim = 50;
            $($(".dual-select-tree-container").children()[0]).addClass("dual-select-tree");
            $(".dual-select-list-box").height(height);
            $(".right-dual-select-list-box, .left-dual-select-list-box").height(height);
            $(".dual-select-tree-container").height(height - interim);
            $(".left-dual-select-list-box-top, .left-dual-select-list-box-bottom").height(gridSize);


        },
        initStyles = function() {
            resizeComponents.call(this, 150);
            this.treeViewModel.sortTreeView();
            disableChildMoveAllButton(true);
        };

    //public members
    return {
        addParent: addParent,
        addChild: addChild,
        addAllChildren: addAllChildren,
        addAllParents: addAllParents,
        removeSelected: removeSelected,
        removeAll: removeAll,
        initStyles: initStyles,
        resizeComponents: resizeComponents
    };
}();