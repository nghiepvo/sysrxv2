﻿function DropdownListViewModel(id,modelName, urlToReadData, currentId) {
    var self = this;
    self.CurrentId = ko.observable(currentId);
    self.Id = ko.observable(id);
    self.ModelName = ko.observable(modelName);
    if (urlToReadData == '') {
        urlToReadData = "/" + self.ModelName() + "/GetLookup";
    }
    var dataSource = new kendo.data.DataSource({
        transport: {
            read: {
                url: urlToReadData,
                dataType: "json",
                type: "GET"
            }
        },
        schema: {
            model: {
                id: 'KeyId',
                fields: { KeyId: {}, DisplayName: {} }
            }
        }
    });
    var dropdownId = '#dropdown-' + id;
    var idValControl = '#' + id;
    $(dropdownId).kendoDropDownList({
        dataTextField: "DisplayName",
        dataValueField: "KeyId",
        dataSource: dataSource,
        change: onChange,
        height:120,
        dataBound: function () {
            $(idValControl).val(dropdownlist.value()).trigger('change');
        }
    });

    var dropdownlist = $(dropdownId).data("kendoDropDownList");
    if (self.CurrentId() !== '0' || self.CurrentId() !== '') {
        dropdownlist.value(self.CurrentId());
    }
    function onChange() {
        $('#dropdown-container-' + id).parents('form').addClass('dirty');
        EnableCreateFooterButton(true);
        $(idValControl).val(dropdownlist.value()).trigger('change');
        // Notify for another control know dropdown select change
        Postbox.messages.notifySubscribers(dropdownlist.value(), Postbox.WORKCOMP_DROPDOWN_CHANGE + "_" + self.ModelName());
    }
}