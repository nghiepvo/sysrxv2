﻿

var DualSelectTreeViewModel = function (modelId, modelName, treeViewUrl, templateHtml) {
    this.dataTextFields = ["ParentDisplayName", "ChildDisplayName"];
    this.dataValueFields = ["Id", "Id"];
    this.childrenSchemas = "childTypes";
    this.modelId = modelId;
    this.treeViewModel = new TreeViewModel("tree-" + modelId, modelName, treeViewUrl, this.childrenSchemas, this.dataTextFields, this.dataValueFields, null, templateHtml, false);
    this.treeView = $("#tree-" + this.modelId).data("kendoTreeView");
};

DualSelectTreeViewModel.prototype = function () {
    var sortTreeView = function () {
        this.treeView.dataSource.sort({ field: "ParentDisplayName", dir: "asc" });
    },
    getUnsavedData = function () {
        return this.treeView.dataSource.GetDualSelectUnsavedData();
    };

    return {
        sortTreeView: sortTreeView,
        getUnsavedData: getUnsavedData
    };
}();