﻿function EditableGridViewModel(elementID,
                                schemaModel,
                                columns,
                                readUrl,
                                toolbars,
                                editTemplate,
                                clientSideDataSource,
                                pagingOptions,
                                hasScrollBar) {

    var self = this;
    if (hasScrollBar == undefined) {
        hasScrollBar = true;
    }
    self.DataSource = ko.observable();
    self.AddedRow = ko.observable(false);
    self.SelectedDataRow = ko.observable({});
    self.ModelID = ko.observable(elementID);
    self.AllowResizeColumn = ko.observable(false);
    self.AllowReorderColumn = ko.observable(false);
    self.AllowShowHideColumn = ko.observable(false);
    self.PageSize = ko.observable((pagingOptions == undefined) ? 100 : pagingOptions.pageSize);
    self.ServerPaging = ko.observable((pagingOptions == undefined) ? false : pagingOptions.serverPaging);
    self.ServerSorting = ko.observable((pagingOptions == undefined) ? false : pagingOptions.serverSorting);
    self.Virtual = ko.observable((pagingOptions == undefined || pagingOptions.virtual == undefined) ? false : pagingOptions.virtual);
    if (hasScrollBar== false) {
        self.Virtual(false);
    }
    var gridID = "#" + elementID;
    self.ElementId = ko.observable(gridID);
    self.PendingChanges = [];
    
    var dataSource =  new kendo.data.DataSource({
        data: [],
        schema: {
            model: schemaModel,
            data: "Data",
            total: "TotalRowCount"
        }
    });
    if (clientSideDataSource != null ) {

        dataSource = new kendo.data.DataSource({
            data: clientSideDataSource,
            schema: {
                model: schemaModel,
                data: "Data",
                total: "TotalRowCount"
            }
        });

    }
    else if (readUrl != "" && readUrl!=undefined) {
        dataSource = new kendo.data.DataSource({
            transport: {
                read: {
                    url: readUrl,
                    dataType: 'json',
                    contentType: 'application/json; charset=utf-8'
                },

                parameterMap: function (options, operation) {

                    //Some editable grid still needs lazy load
                    if (operation == "read") {
                        var result = {
                            pageSize: options.pageSize,
                            skip: options.skip,
                            take: options.take
                        };

                        
                        if (options.sort) {
                            for (var i = 0; i < options.sort.length; i++) {
                                result["sort[" + i + "].field"] = options.sort[i].field;
                                result["sort[" + i + "].dir"] = options.sort[i].dir;
                            }
                        }
                        return result;
                    }
                }
            },
            pageSize: self.PageSize(),
            batch: true,
            serverPaging: self.ServerPaging(), // for editable grid, we don't support server side paging.
            serverSorting: self.ServerSorting(), // For editable grid, we only support client side sorting
            schema: {
                model: schemaModel,
                data: "Data",
                total: "TotalRowCount"
            }

        });
    }

    self.DataSource(dataSource);
    
    $(gridID).kendoGrid({
        toolbar: toolbars,
        dataSource: dataSource,
        pageable: false,
        sortable: false,
        columns: columns,
        editable: editTemplate,
        selectable: "row",
        change: onChange,
        navigatable: true,
        scrollable: {
            virtual: self.Virtual()
        },
        columnMenuInit: function (e) {
            e.container.find('li.k-item.k-sort-asc[role="menuitem"]').remove();
            e.container.find('li.k-item.k-sort-desc[role="menuitem"]').remove();
            e.container.find('.k-columns-item .k-group').css({ 'width': '200px', 'max-height': '400px' });
        },
        resizable: self.AllowResizeColumn(),
        edit: reformatDateEditorInputMask,
        reorderable: self.AllowReorderColumn(),
        dataBound:dataBound
    });

    function reformatDateEditorInputMask(e) {
        //var dateInputs = e.container.find("input[data-type=date]");
        //if (dateInputs.length > 0) {
        //    var dateFormat = kendo.culture().calendar.patterns.d;

        //    //re-format from Kendo Dateformat to input mask for date 
        //    //e.g: Kendo's format yyyy/M/d --> yyyy/mm/dd
        //    var dateParts = dateFormat.toLowerCase().split('/');
        //    for (var i = 0; i < dateParts.length; i++) {
        //        if (dateParts[i].length < 2) {
        //            dateParts[i] += dateParts[i];
        //        }
        //    }
        //    dateFormat = dateParts.join("/");
        //    e.container.find("input[data-type=date]").inputmask(dateFormat);
        //};
    }

    // This will set the form as dirty if any changes are made to the datasource of this grid
    dataSource.bind('change', function (e) {
        if (e.action !== undefined) {
            $("#" + elementID).parents('form').addClass('dirty');
            EnableCreateFooterButton(true);
            var grid = $("#" + elementID).data("kendoGrid");

            if (e.action == "itemchange") {

                e.items[0].dirtyFields = e.items[0].dirtyFields || {};
                e.items[0].dirtyFields[e.field] = true;
            }
            else if (e.action == "add") {
                e.items[0].dirtyFields = {};

                for (var i = 0; i < grid.columns.length; i++) {
                    e.items[0].dirtyFields[grid.columns[i].field] = true;
                }
            }

            //This is to notify in case parent grid wants to listen to children list changed.
            Postbox.messages.notifySubscribers($("#" + elementID), self.ModelID() + "_DATACHANGED");
        }
    });
    

    function onChange(arg) {
        var grid = $(gridID).data("kendoGrid");
        var dataItem = grid.dataItem(grid.select());
        if (self.SelectedDataRow().uid == undefined || self.SelectedDataRow().uid != dataItem.uid) {
        self.SelectedDataRow(dataItem);
        Postbox.messages.notifySubscribers(self.SelectedDataRow(), self.ModelID() + "_SELECTION");
        }
    }

    function dataBound(e) {
        var grid = $(gridID).data("kendoGrid");
        Postbox.messages.notifySubscribers(null, Postbox.RESCAN_ORGININAL_VALUES);
        Postbox.messages.notifySubscribers(e, self.ModelID() + "_DATABOUND");                
    }
}