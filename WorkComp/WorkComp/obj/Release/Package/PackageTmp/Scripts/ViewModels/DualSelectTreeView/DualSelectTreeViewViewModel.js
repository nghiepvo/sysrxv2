﻿
//using the Revealing Prototype Structure

//All state is contained in the constructor
var DualSelectTreeViewViewModel = function (name, keyId, treeViewModel, selectedGrid, window, offset) {
    this.treeViewModel = treeViewModel;
    this.treeview = treeViewModel.treeView;
    this.modelName = ko.observable(name);
    this.selectedItemGridControl = selectedGrid;
    this.childTypes = ko.observable([]);
    this.keyID = ko.observable(keyId);
    this.window = window;
    this.offset = offset;
};


//Functions are only loaded into memory once. Allows for public and private members
DualSelectTreeViewViewModel.prototype = function () {

    //private members
    var moveSelected = function () {
        var treeview = this.treeview;
        var selectedNode = treeview.select();
        var treeViewRemovedSelectedChildItems = [];
        if (selectedNode.length != 1) {
            return;
        }
        var selectedNodeItem = treeview.dataItem(selectedNode[0]);
        var parentNode = treeview.dataItem(treeview.parent(selectedNode[0]));
        if (selectedNodeItem.ChildDisplayName == undefined) {
            var childNodeDetails = getNodeDetails(selectedNodeItem);

            if (childNodeDetails.childNodeItems != undefined) {
                for (var i = 0; i < childNodeDetails.childNodeItems.length; i++) {
                    var preLength = childNodeDetails.childNodeItems.length;
                    moveNodeChildHasChildSelected(childNodeDetails.childNodeItems[i], treeViewRemovedSelectedChildItems, treeview, selectedNodeItem.Id);
                    var curLength = childNodeDetails.childNodeItems.length;
                    if (preLength > curLength) {
                        i = i - 1;
                    }
                }
            }
        } else {
            removeTreeViewChildItem(selectedNodeItem, treeViewRemovedSelectedChildItems, treeview, parentNode.Id);
        }

        this.selectedItemGridControl.selectItemAction(treeViewRemovedSelectedChildItems);
        removeDisabledSave(treeViewRemovedSelectedChildItems);
    },
        moveNodeChildHasChildSelected = function (selectedNodeItem, treeViewRemovedSelectedChildItems, treeview, parentNodeId) {
            if (selectedNodeItem === undefined || selectedNodeItem===null) {
                return;
            }
            
            if (selectedNodeItem.ChildDisplayName == undefined) {
                var childNodeDetails = getNodeDetails(selectedNodeItem);

                if (childNodeDetails.childNodeItems != undefined) {
                    while (childNodeDetails.childNodeItems.length > 0) {
                        removeTreeViewChildItem(childNodeDetails.childNodeItems[0], treeViewRemovedSelectedChildItems, treeview, selectedNodeItem.Id);
                    }
                }
            } else {
                removeTreeViewChildItem(selectedNodeItem, treeViewRemovedSelectedChildItems, treeview, parentNodeId);
            }

            //this.selectedItemGridControl.selectItemAction(treeViewRemovedSelectedChildItems);
            //removeDisabledSave(treeViewRemovedSelectedChildItems);
        },
         removeDisabledSave = function (listItem) {
             if (listItem !== undefined &&listItem!=null&& listItem.length > 0) {
                 EnableCreateFooterButton(true);
             }
         },
        moveAll = function () {
            var treeview = this.treeview;
            var items = treeview.dataSource.view();
            for (var index = 0; index < items.length; index++) {
                var parentNodeItem = items[index];
                var parentNodeDetails = getNodeDetails(parentNodeItem);

                if (parentNodeDetails.childNodeItems != undefined) {
                    var treeViewRemovedChildItems = [];
                    for (var i = 0; i < parentNodeDetails.childNodeItems.length; i++) {
                        var preLength = parentNodeDetails.childNodeItems.length;
                        moveNodeChildHasChildSelected(parentNodeDetails.childNodeItems[i], treeViewRemovedChildItems, treeview, parentNodeItem.Id);
                        var curLength = parentNodeDetails.childNodeItems.length;
                        if (preLength > curLength) {
                            i = i - 1;
                        }
                    }
                    removeDisabledSave(treeViewRemovedChildItems);
                    this.selectedItemGridControl.selectItemAction(treeViewRemovedChildItems);
                }
            }
        },
        removeSelected = function() {
            var grid = this.selectedItemGridControl;
            var treeview = this.treeview;
            var gridItems = [];
            _.each(grid.getAvailableSelectedItemGridControl.select(), function (value, key) {
                var dataItem = grid.getAvailableSelectedItemGridControl.dataItem(value);
                addSelectedItem(dataItem, gridItems, treeview);
            });
            treeview.expand(".k-item");
            removeDisabledSave(gridItems);
            grid.removeItemAction(gridItems);
            
            //removeDisabledSave.call(this, gridItems);
        },
         addSelectedItem = function (dataItem, gridItems, treeview) {
             var itemToAdd = { ChildDisplayName: getReplacedTextAdd(dataItem.DisplayName), Id: dataItem.Id };
             gridItems.push(itemToAdd);
             // get parent node
             var idParent = dataItem.ParentId;
             var dataSource = treeview.dataSource._data;
             
             var parentUid;
             _.each(dataSource,function (value,key)
             {
                 if (value.Id == idParent) {
                     parentUid = value.uid;
                     return;
                 } else {
                     // Find in the child type
                     var childDataSource = value.childTypes;
                     _.each(childDataSource, function (childValue, childKey) {
                         if (childValue.Id == idParent) {
                             parentUid = childValue.uid;
                             return;
                         }
                     });
                 }
             });
             var parentNode = treeview.findByUid(parentUid);
             if (parentNode != null) {
                 treeview.append(itemToAdd, parentNode);
             }
             //treeview.append(itemToAdd);
         },
        removeAll = function() {
            var grid = this.selectedItemGridControl;
            var treeview = this.treeview;
            var gridItems = [];
            _.each(grid.getAvailableSelectedItemGridControl._data, function (dataItem, key) {
                addSelectedItem(dataItem, gridItems, treeview);
            });
            treeview.expand(".k-item");
            removeDisabledSave(gridItems);
            grid.removeItemAction(gridItems);
        },
        getNodeDetails = function (selectedNodeItem) {
            //var childNodeItems = selectedNodeItem._childrenOptions.data.childTypes;
            //var subractIterator = false;
            //if (childNodeItems == undefined) {
            //    childNodeItems = selectedNodeItem.childTypes;
            //    subractIterator = true;
            //}
            var childNodeItems = selectedNodeItem.childTypes;
            var childNodeDetails = { childNodeItems: childNodeItems, subractIterator: true };
            return childNodeDetails;
        },
        removeTreeViewChildItem = function (dataItem, gridItems, treeview, parentId) {
            var removeChildNode = treeview.findByUid(dataItem.uid);
            if (dataItem.ChildDisplayName == undefined) {
                var childNodeDetails = getNodeDetails(dataItem);

                if (childNodeDetails.childNodeItems != undefined) {
                    while (childNodeDetails.childNodeItems.length > 0) {
                        removeTreeViewChildItem(childNodeDetails.childNodeItems[0], gridItems, treeview, dataItem.Id);
                    }
                }
            } else {
                var itemToRemove = { DisplayName: getReplacedTextRemove(dataItem.ChildDisplayName), Id: dataItem.Id, ParentId: parentId };
                gridItems.push(itemToRemove);
                treeview.remove(removeChildNode);
            }
        },
        getReplacedTextRemove = function(value) {
            //return value;
            return value.replace("&lt;", "<").replace("&gt;", ">");
        },
        getReplacedTextAdd = function (value) {
            //return value;
            return value.replace("<", "&lt;").replace(">", "&gt;");
        },
        resizeComponents = function (offset) {

            if (offset === undefined) {
                offset = this.offset;
            }

            var height = $(this.window).height() - offset;

            //var interim = (1550 - height) / 15.9;
            var gridSize = (height / 2) - 40;
            var interim = 50;
            $($(".dual-select-tree-container").children()[0]).addClass("dual-select-tree");
            $(".dual-select-list-box").height(height);
            $(".right-dual-select-list-box, .left-dual-select-list-box").height(height);
            $(".dual-select-tree-container").css({ height: (height - interim) });
            $(".left-dual-select-list-box-top, .left-dual-select-list-box-bottom").height(gridSize);
           

        },
        initStyles = function () {
            this.treeViewModel.sortTreeView();

            $(".dual-select-tree-container,.left-dual-select-list-box-grid").css({ height: $(this.window).height() - 220 });
            $(".dual-select-tree-container").find(".k-treeview").css({ width: "auto" });
            $(".left-dual-select-list-box-grid").find(".k-grid.k-widget").css({ width: "auto" });
            $("fieldset").css({border:"none",height:"auto"});
        };

    //public members
    return {
        moveSelected: moveSelected,
        moveAll: moveAll,
        removeSelected: removeSelected,
        removeAll: removeAll,
        initStyles: initStyles,
        resizeComponents: resizeComponents
    };
}();