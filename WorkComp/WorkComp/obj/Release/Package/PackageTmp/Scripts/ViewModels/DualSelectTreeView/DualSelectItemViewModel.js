﻿

var DualSelectItemViewModel = function (modelId, selectUrl, codeText) {

    this.schemaModel = {
        id: "RowGuid",
        fields: {
            Id: { editable: false, nullable: false },
            DisplayName: { editable: false, nullable: false, validation: { required: true } },
            ParentId: { editable: false, nullable: false }
        }
    };
    this.columns = [
       {
           field: "DisplayName",
           title: codeText,
           width: "40px",
       }
    ];
    this.gridSelectViewModel = new GridLookupViewModel("grid-container-selected-" + modelId, "", this.schemaModel, this.columns, selectUrl);
    this.getAvailableSelectedItemGridControl = $("#grid-container-selected-" + modelId).data("kendoGrid");
};

DualSelectItemViewModel.prototype = function () {
    var removeItemAction = function (gridItems) {
        var gridViewDataSource = this.gridSelectViewModel.DataSource();
        _.each(gridItems, function (value, key) {
            var dataItem;
            $.each(gridViewDataSource._data, function (index, item) {
                if (item.Id === value.Id) {
                    dataItem = item;
                }
            });
            eval(gridViewDataSource.remove(dataItem));
        });
    },
    selectItemAction = function (seletedNodes) {
        var gridViewDataSource = this.gridSelectViewModel.DataSource();
        _.each(seletedNodes, function (value, key) {
            eval(gridViewDataSource.add(value));
        });
        gridViewDataSource.sort({ field: "DisplayName", dir: "asc" });
    };
    return {
        selectItemAction: selectItemAction,
        removeItemAction: removeItemAction
    };
}();