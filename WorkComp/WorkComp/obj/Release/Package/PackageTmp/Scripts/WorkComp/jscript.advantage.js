﻿function generateGuid() {
    var result, i, j;
    result = '';
    for (j = 0; j < 32; j++) {
        if (j == 8 || j == 12 || j == 16 || j == 20)
            result = result + '-';
        i = Math.floor(Math.random() * 16).toString(16).toUpperCase();
        result = result + i;
    }
    return result;
}

function InitInputTime(timePickerId, hiddenTimeInputId, initialValue) {
    var timeValue = initialValue;
   
    var pickerId = "#" + timePickerId;
    var hiddenIput = "#" + hiddenTimeInputId;
    if (_.isUndefined(timeValue))
        timeValue = $(hiddenIput).val();
    
    var hour = timeValue.substring(0,2 - (4 - timeValue.length));
    var minute = timeValue.substring(hour.length);
    
    $(pickerId).kendoTimePicker({
        format: "HH:mm",
        change: onChange,
        value: hour + ":" + minute
    });

    function onChange() {
        var hour = kendo.toString(this.value(), 'H');
        var minute = kendo.toString(this.value(), 'mm');
        $(hiddenIput).val(hour + minute).trigger('change');
        Postbox.messages.notifySubscribers(null, Postbox.INPUT_TIME_CHANGE);
    }
    $(pickerId).inputmask("hh:mm");
};