﻿namespace WorkComp.Models.ReferralNote
{
    public class DashboardReferralNoteDataViewModel : MasterfileViewModelBase<Framework.DomainModel.Entities.ReferralNote>
    {
        public override void MapFromClientParameters(MasterfileParameter parameters)
        {
            SharedViewModel = MapFromClientParameters<DashboardReferralNoteShareViewModel>(parameters);
        }

        public override string PageTitle
        {
            get { return "Create Referral Note"; }
        }
    }
}