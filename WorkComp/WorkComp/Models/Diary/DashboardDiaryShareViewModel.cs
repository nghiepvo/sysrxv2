﻿using System;
using Framework.DomainModel.ValueObject;

namespace WorkComp.Models.Diary
{
    public class DashboardDiaryShareViewModel : DashboardSharedViewModel
    {
        public string Heading { get; set; }
        public string Reason { get; set; }
        public string Comment { get; set; }

        public int ReferralId { get; set; }
        public LookupItemVo ReferralDataSource { get; set; }
    }
}