﻿namespace WorkComp.Models.Payer
{
    public class DashboardPayerDataViewModel : MasterfileViewModelBase<Framework.DomainModel.Entities.Payer>
    {
        public override void MapFromClientParameters(MasterfileParameter parameters)
        {
            SharedViewModel = MapFromClientParameters<DashboardPayerShareViewModel>(parameters);
        }
    }
}