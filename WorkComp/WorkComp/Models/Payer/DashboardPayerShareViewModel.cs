﻿using Framework.DomainModel.ValueObject;

namespace WorkComp.Models.Payer
{
    public class DashboardPayerShareViewModel : DashboardSharedViewModel
    {
        public string Name { get; set; }
        public string ControlNumberPrefix { get; set; }
        public string SpecialInstructions { get; set; }
        public string Address { get; set; }
        public int? CityId { get; set; }
        public LookupItemVo CityDataSource { get; set; }
        public int? StateId { get; set; }
        public LookupItemVo StateDataSource { get; set; }
        public int? ZipId { get; set; }
        public LookupItemVo ZipDataSource { get; set; }
    }
}