﻿namespace WorkComp.Models.Payer
{
    public class DashboardPayerIndexViewModel : DashboardGridViewModelBase<Framework.DomainModel.Entities.Payer>
    {
        public override string PageTitle
        {
            get
            {
                return "Payer";
            }
        }
    }
}