﻿namespace WorkComp.Models.AssayCode
{
    public class DashboardAssayCodeDataViewModel : MasterfileViewModelBase<Framework.DomainModel.Entities.AssayCode>
    {
        public override void MapFromClientParameters(MasterfileParameter parameters)
        {
            SharedViewModel = MapFromClientParameters<DashboardAssayCodeShareViewModel>(parameters);
        }

        public override string PageTitle
        {
            get
            {
                return SharedViewModel.CreateMode ? "Create Assay Code" : "Update Assay Code";
            }
        }
    }
}