﻿using System.Web.Routing;

namespace WorkComp.Models.Date
{
    public class DatetimePickerViewModel : ControlSharedViewModelBase
    {
        public string Class { get; set; }
        public string Style { get; set; }
        public string Format { get; set; }
        public bool Required { get; set; }
        public bool IsSetEmpty { get; set; }
        public string RequiredAttribute
        {
            get
            {
                return Required ? "required=\"required\"" : "";
            }
        }
        public RouteValueDictionary HtmlAttributes { get; set; }
    }
}