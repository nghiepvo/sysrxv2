﻿namespace WorkComp.Models.Employer
{
    public class DashboardEmployerDataViewModel : MasterfileViewModelBase<Framework.DomainModel.Entities.Employer>
    {
        public override void MapFromClientParameters(MasterfileParameter parameters)
        {
            SharedViewModel = MapFromClientParameters<DashboardEmployerShareViewModel>(parameters);
        }
    }
}