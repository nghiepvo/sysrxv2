﻿using System.ComponentModel.DataAnnotations;
using Framework.DomainModel.ValueObject;

namespace WorkComp.Models.Employer
{
    public class DashboardEmployerShareViewModel : DashboardSharedViewModel
    {
        public string ExternalId { get; set; }
        [Required]
        public string Name { get; set; }
        public string Phone { get; set; }
        public string FederalTaxId { get; set; }
        public string GeoAddress { get; set; }
        public string Address { get; set; }
        public string Address1 { get; set; }

        public int? Rank { get; set; }

        public int? CityId { get; set; }
        public LookupItemVo CityDataSource { get; set; }
        public int? StateId { get; set; }
        public LookupItemVo StateDataSource { get; set; }
        public int? ZipId { get; set; }
        public LookupItemVo ZipDataSource { get; set; }
        
        public bool IsUserUpdate { get; set; }
    }
}