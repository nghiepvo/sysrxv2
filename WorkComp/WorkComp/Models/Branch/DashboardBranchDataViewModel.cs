﻿namespace WorkComp.Models.Branch
{
    public class DashboardBranchDataViewModel : MasterfileViewModelBase<Framework.DomainModel.Entities.Branch>
    {
        public override void MapFromClientParameters(MasterfileParameter parameters)
        {
            SharedViewModel = MapFromClientParameters<DashboardBranchShareViewModel>(parameters);
        }
    }
}