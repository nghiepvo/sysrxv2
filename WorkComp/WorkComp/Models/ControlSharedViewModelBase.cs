﻿namespace WorkComp.Models
{
    public class ControlSharedViewModelBase
    {
        public string ID { get; set; }
        public string Label { get; set; }
        public int Length { get; set; }
        public bool ReadOnly { get; set; }

        public string ReadOnlyAttr
        {
            get {
                return ReadOnly ? "readonly" : "";
            }
        }
        public string Enabled { get; set; }
        public string DataBindingValue { get; set; }
    }
}