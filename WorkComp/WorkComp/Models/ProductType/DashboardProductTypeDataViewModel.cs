﻿namespace WorkComp.Models.ProductType
{
    public class DashboardProductTypeDataViewModel :
        MasterfileViewModelBase<Framework.DomainModel.Entities.ProductType>
    {
        public override void MapFromClientParameters(MasterfileParameter parameters)
        {
            SharedViewModel = MapFromClientParameters<DashboardProductTypeShareViewModel>(parameters);
        }

        public override string PageTitle
        {
            get
            {
                return SharedViewModel.CreateMode ? "Create Product Type" : "Update Product Type";
            }
        }
    }
}