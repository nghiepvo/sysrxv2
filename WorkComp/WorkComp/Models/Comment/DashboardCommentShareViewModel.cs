﻿using System;
using Framework.DomainModel.ValueObject;

namespace WorkComp.Models.Comment
{
    public class DashboardCommentShareViewModel : DashboardSharedViewModel
    {
        public int ReferralTaskId { get; set; }
        public string CommentContent { get; set; }
    }
}