﻿namespace WorkComp.Models.Comment
{
    public class DashboardCommentIndexViewModel : DashboardGridViewModelBase<Framework.DomainModel.Entities.Comment>
    {
        public override string PageTitle
        {
            get
            {
                return "Comment";
            }
        }
    }
    
}