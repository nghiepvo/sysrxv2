﻿using System;
using System.IO;
using System.Net;
using Framework.Utility;

namespace WorkComp.Models.Referral
{
    public class ReferralDetailExtension
    {
        public int ReferralId { get; set; }
        public string ClaimNumber { get; set; }
        public string HeaderReult { get; set; }
        public string EmailTitle
        {
            get { return WebUtility.HtmlDecode(HeaderReult).RemoveHtml() + "- Claim Number: " + ClaimNumber; }
        }

        public string Claimant { get; set; }

        public string FileName { get; set; }
    }
}