﻿using System;
using Framework.DomainModel.ValueObject;
using WorkComp.Models.Drug;
using WorkComp.Models.Icd;
using WorkComp.Models.TaskGroup;

namespace WorkComp.Models.Referral
{
    public class DashboardReferralShareViewModel : DashboardSharedViewModel
    {
        public DashboardReferralShareViewModel()
        {
            ClaimInfo = new DashboardClaimInfoForReferralViewModel();
            ReferralInfo = new DashboardReferralInfoForReferralViewModel();
            IcdInfo = new DashboardIcdInfoForReferralViewModel();
            TestingInfo = new DashboardTestingInfoForReferralViewModel();
            MedicationHistoryInfo = new DashboardMedicationHistoryInfoForReferralViewModel();
            TaskGroupInfo=new DashboardTaskGroupInfoForReferralViewModel();
            AttachmentsInfo=new ReferralFileAttachmentsGridViewModel
            {
                GridId = "FileAttachments",
            };
        }
        public DashboardClaimInfoForReferralViewModel ClaimInfo { get; set; }
        public DashboardReferralInfoForReferralViewModel ReferralInfo { get; set; }
        public DashboardIcdInfoForReferralViewModel IcdInfo { get; set; }
        public DashboardTestingInfoForReferralViewModel TestingInfo { get; set; }
        public DashboardMedicationHistoryInfoForReferralViewModel MedicationHistoryInfo { get; set; }
        public DashboardTaskGroupInfoForReferralViewModel TaskGroupInfo { get; set; }
        public ReferralFileAttachmentsGridViewModel AttachmentsInfo { get; set; }
        public int TaskGroupIdSelected { get; set; } 
    }

    public class DashboardReferralInfoForReferralViewModel
    {
        public DashboardReferralInfoForReferralViewModel()
        {
            AttorneyInfo = new InfoWhenChangeAttorneyInReferral();
           
            EnteredDate = DateTime.Now;
            ReceivedDate = DateTime.Now;
            DueDate = DateTime.Now;
        }
        
        
        public DateTime EnteredDate { get; set; }
        public DateTime ReceivedDate { get; set; }
        public DateTime DueDate { get; set; }
        public int StatusId { get; set; }
        public int AssignToId { get; set; }
        public LookupItemVo AssignToDataSource { get; set; }
        public int? ReferralMethodId { get; set; }
        public bool Rush { get; set; }
        public string SpecialInstruction { get; set; }
        public string ControlNumber { get; set; }
        public InfoWhenChangeAttorneyInReferral AttorneyInfo { get; set; }
        public LookupItemVo AttorneyDataSource { get; set; }
        public bool CreateMode { get; set; }
       
    }

    public class DashboardClaimInfoForReferralViewModel
    {
        public DashboardClaimInfoForReferralViewModel()
        {
            ReferralSourceInfo = new InfoWhenChangeReferralSourceInReferral();
            ClaimNumberInfo = new InfoWhenChangeClaimNumberInReferral();
            PayerInfo = new InfoWhenChangePayerInReferral();
            AdjusterInfo = new InfoWhenChangeAdjusterInReferral();
            ClaimantInfo = new InfoWhenChangeClaimantInReferral();
            CaseManagerInfo = new InfoWhenChangeCaseManagerInReferral();
            EmployerInfo = new InfoWhenChangeEmployerInReferral();
        }

        public LookupItemVo ReferralSourceDataSource { get; set; }
        public bool? AdjusterIsReferral { get; set; }
        public InfoWhenChangeReferralSourceInReferral ReferralSourceInfo { get; set; }

        public InfoWhenChangeClaimNumberInReferral ClaimNumberInfo { get; set; }
        public LookupItemVo ClaimNumberDataSource { get; set; }

        public InfoWhenChangePayerInReferral PayerInfo { get; set; }
        public LookupItemVo PayerDataSource { get; set; }

        public int? BranchId { get; set; }
        public LookupItemVo BranchDataSource { get; set; }

        public int? ProductTypeId { get; set; }
        public LookupItemVo ProductTypeDataSource { get; set; }

        public InfoWhenChangeAdjusterInReferral AdjusterInfo { get; set; }
        public LookupItemVo AdjusterDataSource { get; set; }

        public InfoWhenChangeClaimantInReferral ClaimantInfo { get; set; }
        public LookupItemVo ClaimantDataSource { get; set; }

        public InfoWhenChangeCaseManagerInReferral CaseManagerInfo { get; set; }
        public LookupItemVo CaseManagerDataSource { get; set; }

        public InfoWhenChangeEmployerInReferral EmployerInfo { get; set; }
        public LookupItemVo EmployerDataSource { get; set; }
    }

    public class DashboardTestingInfoForReferralViewModel
    {
        public DashboardTestingInfoForReferralViewModel()
        {
            TreatingPhysicianInfo = new InfoWhenChangeTreatingPhysicianInReferral();
            CollectionSiteInfo = new InfoWhenChangeCollectionSiteInReferral();
            PanelTypeInfo = new InfoWhenChangePanelTypeInReferral();
            CheckUrine = true;
            CollectionMethodId = 1;
        }

        public int? PayerId { get; set; }
        public string PayerName { get; set; }
        public bool CheckOralFluid { get; set; }
        public bool CheckBlood { get; set; }
        public bool CheckHair { get; set; }
        public bool CheckUrine { get; set; }

        public InfoWhenChangePanelTypeInReferral PanelTypeInfo { get; set; }
        public LookupItemVo PanelTypeDataSource { get; set; }

        public int CollectionMethodId { get; set; }

        public string CollectionSiteSpecialInstructions { get; set; }
        public LookupItemVo CollectionSiteDataSource { get; set; }
        public DateTime? CollectionSiteDate { get; set; }
        public InfoWhenChangeCollectionSiteInReferral CollectionSiteInfo { get; set; }
        public string TreatingPhysicianSpecialHandling { get; set; }
        public DateTime? NextMdVisitDate { get; set; }
        public InfoWhenChangeTreatingPhysicianInReferral TreatingPhysicianInfo { get; set; }
        public LookupItemVo TreatingPhysicianDataSource { get; set; }
        public LookupItemVo NpiNumberDataSource { get; set; }
    }

    public class DashboardIcdInfoForReferralViewModel
    {
        public int ReferralId { get; set; }
        public DashboardIcdInfoForReferralViewModel()
        {
            IcdGridInfo = new IcdGridViewModel
            {
                GridId = "IcdCodeGrid"
            };
        }
        public IcdGridViewModel IcdGridInfo { get; set; }
    }

    public class DashboardMedicationHistoryInfoForReferralViewModel
    {
        public int ReferralId { get; set; }
        public bool NoMedicationHistory { get; set; }
        public DashboardMedicationHistoryInfoForReferralViewModel()
        {
            MedicationHistoryGridInfo = new MedicationHistoryGridViewModel
            {
                GridId = "MedicationHistoryGrid"
            };
            NoMedicationHistory = true;
        }
        public MedicationHistoryGridViewModel MedicationHistoryGridInfo { get; set; }
    }

    public class DashboardTaskGroupInfoForReferralViewModel
    {
        public DashboardTaskGroupInfoForReferralViewModel()
        {
            TaskGroupGridInfo = new TaskGroupGridViewModel
            {
                GridId = "TaskGroupGrid"
            };
        }
        public bool IsCopy { get; set; }
        public int ReferralId { get; set; }
        public int TaskGroupId { get; set; }
        public TaskGroupGridViewModel TaskGroupGridInfo { get; set; }
    }
}