﻿using System;
using System.Collections.Generic;

namespace WorkComp.Models.Referral
{
    public class ReferralEmailTemplateDetailViewModel : ViewModelBase
    {
        public int EmailTemplateId { get; set; }
        public string EmailContent { get; set; }
        public string EmailTitle { get; set; }
        public string EmailSubject { get; set; }
        public string ClaimNumber { get; set; }
        public List<EmailItemTitle> ListEmailAddressSetup { get; set; }
        public override string PageTitle { get; set; }

        private string _emailAddressSetup;
        public string EmailAddressSetup
        {
            get
            {
                if (string.IsNullOrEmpty(_emailAddressSetup))
                {
                    if (ListEmailAddressSetup != null && ListEmailAddressSetup.Count != 0)
                    {
                        foreach (var item in ListEmailAddressSetup)
                        {
                            if (!string.IsNullOrEmpty(item.Email))
                            {
                                _emailAddressSetup += item.Email + ";";
                            }
                        }
                        if (!string.IsNullOrEmpty(_emailAddressSetup))
                        {
                            _emailAddressSetup = _emailAddressSetup.Substring(0, _emailAddressSetup.Length - 1);
                        }
                    }
                }
                return _emailAddressSetup;
            }
            set
            {
                _emailAddressSetup = value;
            }
        }

        public AttachmentSendMail[] AttachmentSendMails { get; set; }

        public int ReferralId { get; set; }
        public string DateToSendEmail { get; set; }
        public string DateToSendFax { get; set; }
        public string FaxNumber { get; set; }
        public string EmailFrom { get; set; }
        public string DisplayName { get; set; }
    }

    public class AttachmentSendMail
    {
        public int Id { get; set; }
        public string FileName { get; set; }
        public string RowGuid { get; set; }
        public bool IsDelete { get; set; }
    }


    public class EmailItemTitle
    {
        public string Email { get; set; }
        public string Name { get; set; }
        public string Type { get; set; }

        public string EmailDisplay
        {
            get
            {
                return Type + ": <br/>-Name: " + Name + "<br/>-Email: " + Email;
            }
        }
    }
}