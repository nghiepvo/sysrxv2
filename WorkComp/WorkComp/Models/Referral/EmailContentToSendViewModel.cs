﻿

namespace WorkComp.Models.Referral
{
    public class EmailContentToSendViewModel
    {
        public string EmailFrom { get; set; }
        public string EmailTo { get; set; }
        public string EmailCc { get; set; }
        public string DisplayName { get; set; }
        public string Content { get; set; }
        public string Subject { get; set; }
        public string IdReferral { get; set; }
        public string IdTask { get; set; }
        public string AttachFile { get; set; }
    }

    public class FaxContentToSendModel
    {
        public string SenderName { get; set; }
        public string FaxNumber { get; set; }
        public string Content { get; set; }
        public string Subject { get; set; }
        public string IdReferral { get; set; }
        //Physician name
        public string FaxRecipient { get; set; }
    }
}