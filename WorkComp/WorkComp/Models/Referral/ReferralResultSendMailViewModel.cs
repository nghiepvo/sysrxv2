﻿using System.Security.AccessControl;

namespace WorkComp.Models.Referral
{
    public class ReferralResultSendMailViewModel
    {
        public int ReferralId { get; set; }
        public string Title { get; set; }
        public string To { get; set; }
        public string Cc { get; set; }
        public string Message { get; set; }
        public bool SecureEmail { get; set; }
        public string FileName { get; set; }
        public string IncludeContent { get; set; }
    }
}