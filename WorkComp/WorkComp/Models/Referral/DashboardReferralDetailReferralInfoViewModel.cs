﻿using System;
using System.Collections.Generic;
using System.Globalization;
using System.Linq;
using Framework.DomainModel.Entities.Common;
using Framework.DomainModel.ValueObject;
using Framework.Utility;

namespace WorkComp.Models.Referral
{
    #region Single
    public class ReferralInfo
    {
        public int ReferralId { get; set; }

        private string _controlNumber;
        public string ControlNumber
        {
            get { return string.IsNullOrEmpty(_controlNumber) ? "(none)" : _controlNumber; }
            set { _controlNumber = value; }
        }
        private string _productType;
        public string ProductType
        {
            get { return string.IsNullOrEmpty(_productType) ? "(none)" : _productType; }
            set { _productType = value; }
        }

        private string _dateEntered;
        public string DateEntered
        {
            get { return string.IsNullOrEmpty(_dateEntered) ? "(none)" : _dateEntered; }
            set { _dateEntered = value; }
        }
        private string _dateReceived;
        public string DateReceived
        {
            get { return string.IsNullOrEmpty(_dateReceived) ? "(none)" : _dateReceived; }
            set { _dateReceived = value; }
        }

        private string _dueDate;
        public string DueDate
        {
            get { return string.IsNullOrEmpty(_dueDate) ? "(none)" : _dueDate; }
            set { _dueDate = value; }
        }
        public int StatusId { get; set; }

        public string Status
        {

            get
            {
                return XmlDataHelpper.Instance.GetValue(XmlDataTypeEnum.Status.ToString(), StatusId.ToString(CultureInfo.InvariantCulture));
            }
        }
        private string _assignTo;
        public string AssignTo
        {
            get { return string.IsNullOrEmpty(_assignTo) ? "(none)" : _assignTo; }
            set { _assignTo = value; }
        }

        public int? ReferralMethodId { get; set; }

        public string ReferralMethod
        {
            get
            {
                if (ReferralMethodId != null)
                {
                    return XmlDataHelpper.Instance.GetValue(XmlDataTypeEnum.ReferralMethod.ToString(),
                    ReferralMethodId.ToString());
                }
                return "(none)";
            }
        }
        private string _specialInstruction;
        public string SpecialInstruction
        {
            get { return string.IsNullOrEmpty(_specialInstruction) ? "(none)" : _specialInstruction; }
            set { _specialInstruction = value; }
        }

        private string _createdBy;
        public string CreatedBy
        {
            get { return string.IsNullOrEmpty(_createdBy) ? "(none)" : _createdBy; }
            set { _createdBy = value; }
        }
        private string _createdDate;
        public string CreatedDate
        {
            get { return string.IsNullOrEmpty(_createdDate) ? "(none)" : _createdDate; }
            set { _createdDate = value; }
        }

    }

    public class ReferralSourceInfo
    {
        private string _name;
        public string Name
        {
            get { return string.IsNullOrEmpty(_name) ? "(none)" : _name; }
            set { _name = value; }
        }
        private string _type;
        public string Type
        {
            get { return string.IsNullOrEmpty(_type) ? "(none)" : _type; }
            set { _type = value; }
        }
        private string _company;
        public string Company
        {
            get { return string.IsNullOrEmpty(_company) ? "(none)" : _company; }
            set { _company = value; }
        }
        private string _phone;

        public string Phone
        {
            get
            {
                return string.IsNullOrEmpty(_phone)? "(none)": _phone.ApplyFormatPhone();
            }
            set
            {
                _phone = value;
            }
        }
        private string _fax;
        public string Fax
        {
            get
            {
                return string.IsNullOrEmpty(_fax) ? "(none)" : _fax.ApplyFormatPhone();
            }
            set
            {
                _fax = value;
            }
        }

        private string _email;
        public string Email
        {
            get { return string.IsNullOrEmpty(_email) ? "(none)" : _email; }
            set { _email = value; }
        }
      

        public string State { get; set; }
        public string City { get; set; }
        public string Zip { get; set; }
        public string Address { get; set; }

        public string FullAddress
        {
            get
            {
                if (!string.IsNullOrEmpty(Address) && !string.IsNullOrEmpty(State) && !string.IsNullOrEmpty(City) && !string.IsNullOrEmpty(Zip))
                {
                    return Address + ", " + State + ", " + City + ", " + Zip;
                }

                if (string.IsNullOrEmpty(State) && string.IsNullOrEmpty(City) && string.IsNullOrEmpty(Zip))
                {
                    return Address;
                }
                if (string.IsNullOrEmpty(City) && string.IsNullOrEmpty(Zip))
                {
                    return Address + ", " + State;
                }
                if (string.IsNullOrEmpty(Zip))
                {
                    return Address + ", " + State + ", " + City;
                }
                return "(none)";

            }
        }

        public DateTime? AuthorizationFrom { get; set; }
        public DateTime? AuthorizationTo { get; set; }
        public bool IsAuthorization { get; set; }

        public string ReAuthorizationData
        {
            get
            {
                if (!IsAuthorization) return "No";
                var temp = "";
                temp += AuthorizationFrom != null
                    ? AuthorizationFrom.GetValueOrDefault().ToShortDateString() + " - "
                    : "";
                temp += AuthorizationTo != null ? AuthorizationFrom.GetValueOrDefault().ToShortDateString() : "";
                return temp;
            }
        }

    }

    public class PayorInfo
    {
        private string _payer;

        public string Payer
        {
            get { return string.IsNullOrEmpty(_payer) ? "(none)" : _payer; }
            set { _payer = value; }
        }

        private string _branch;

        public string Branch
        {
            get { return string.IsNullOrEmpty(_branch) ? "(none)" : _branch; }
            set { _branch = value; }

        }
        private string _specialInstruction;

        public string SpecialInstruction
        {
            get { return string.IsNullOrEmpty(_specialInstruction) ? "(none)" : _specialInstruction; }
            set { _specialInstruction = value; }
        }
    }

    public class AdjusterInfo
    {

        private string _name;
        public string Name
        {
            get { return string.IsNullOrEmpty(_name) ? "(none)" : _name; }
            set { _name = value; }
        }
        private string _email;
        public string Email
        {
            get { return string.IsNullOrEmpty(_email) ? "(none)" : _email; }
            set { _email = value; }
        }
        private string _phone;

        public string Phone
        {
            get
            {
                return string.IsNullOrEmpty(_phone) ? "(none)" : _phone.ApplyFormatPhone();
            }
            set
            {
                _phone = value;
            }
        }
        private string _externalId;
        public string ExternalId
        {
            get { return string.IsNullOrEmpty(_externalId) ? "(none)" : _externalId; }
            set { _externalId = value; }
        }

        public string State { get; set; }
        public string City { get; set; }
        public string Zip { get; set; }
        public string Address { get; set; }

        public string FullAddress
        {
            get
            {
                if (!string.IsNullOrEmpty(Address) && !string.IsNullOrEmpty(State) && !string.IsNullOrEmpty(City) && !string.IsNullOrEmpty(Zip))
                {
                    return Address + ", " + State + ", " + City + ", " + Zip;
                }

                if (string.IsNullOrEmpty(State) && string.IsNullOrEmpty(City) && string.IsNullOrEmpty(Zip))
                {
                    return Address;
                }
                if (string.IsNullOrEmpty(City) && string.IsNullOrEmpty(Zip))
                {
                    return Address + ", " + State;
                }
                if (string.IsNullOrEmpty(Zip))
                {
                    return Address + ", " + State + ", " + City;
                }
                return "(none)";

            }
        }
    }

    public class ClaimantInfo
    {
        public int Id { get; set; }

        private string _name;
        public string Name
        {
            get { return string.IsNullOrEmpty(_name) ? "(none)" : _name; }
            set { _name = value; }
        }

        public string PayerPatientId { get; set; }
        
        private string _homePhone;
        public string HomePhone
        {
            get
            {
                return string.IsNullOrEmpty(_homePhone) ? "(none)" : _homePhone.ApplyFormatPhone();
            }
            set
            {
                _homePhone = value;
            }
        }

        private string _cellPhone;
        public string CellPhone
        {
            get
            {
                return string.IsNullOrEmpty(_cellPhone) ? "(none)" : _homePhone.ApplyFormatPhone();
            }
            set
            {
                _cellPhone = value;
            }
        }


        private string _gender;
        public string Gender
        {
            get
            {
                var temp = XmlDataHelpper.Instance.GetValue(XmlDataTypeEnum.Gender.ToString(), _gender);
                return string.IsNullOrEmpty(temp)? "(none)": temp;
            }
            set { _gender = value; }
        }
        private string _dateOfBirth;
        public string DateOfBirth
        {
            get { return string.IsNullOrEmpty(_dateOfBirth) ? "(none)" : _dateOfBirth; }
            set { _dateOfBirth = value; }
        }
        private string _ssnId;
        public string SsnId
        {
            get { return string.IsNullOrEmpty(_ssnId) ? "(none)" : _ssnId; }
            set { _ssnId = value; }
        }
        private string _claimantLanguage;
        public string ClaimantLanguage
        {
            get { return string.IsNullOrEmpty(_claimantLanguage) ? "(none)" : _claimantLanguage; }
            set { _claimantLanguage = value; }
        }
        private string _email;
        public string Email
        {
            get { return string.IsNullOrEmpty(_email) ? "(none)" : _email; }
            set { _email = value; }
        }
        

        public string State { get; set; }
        public string City { get; set; }
        public string Zip { get; set; }
        public string Address1 { get; set; }
        public string Address2 { get; set; }
        public string FullAddress
        {
            get
            {
                if (!string.IsNullOrEmpty(Address1) && !string.IsNullOrEmpty(Address2) && !string.IsNullOrEmpty(State) && !string.IsNullOrEmpty(City) && !string.IsNullOrEmpty(Zip))
                {
                    return Address1 + " - " + Address2 + ", " + State + ", " + City + ", " + Zip;
                }

                if (string.IsNullOrEmpty(Address2))
                {
                    if (!string.IsNullOrEmpty(Address1) && !string.IsNullOrEmpty(State) && !string.IsNullOrEmpty(City) && !string.IsNullOrEmpty(Zip))
                    {
                        return Address1 + ", " + State + ", " + City + ", " + Zip;
                    }

                    if (string.IsNullOrEmpty(State) && string.IsNullOrEmpty(City) && string.IsNullOrEmpty(Zip))
                    {
                        return Address1;
                    }
                    if (string.IsNullOrEmpty(City) && string.IsNullOrEmpty(Zip))
                    {
                        return Address1 + ", " + State;
                    }
                    if (string.IsNullOrEmpty(Zip))
                    {
                        return Address1 + ", " + State + ", " + City;
                    }
                }

                if (string.IsNullOrEmpty(Address1))
                {
                    if (!string.IsNullOrEmpty(Address2) && !string.IsNullOrEmpty(State) && !string.IsNullOrEmpty(City) && !string.IsNullOrEmpty(Zip))
                    {
                        return Address2 + ", " + State + ", " + City + ", " + Zip;
                    }

                    if (string.IsNullOrEmpty(State) && string.IsNullOrEmpty(City) && string.IsNullOrEmpty(Zip))
                    {
                        return Address2;
                    }
                    if (string.IsNullOrEmpty(City) && string.IsNullOrEmpty(Zip))
                    {
                        return Address2 + ", " + State;
                    }
                    if (string.IsNullOrEmpty(Zip))
                    {
                        return Address2 + ", " + State + ", " + City;
                    }
                }
                
                return "(none)";
            }
        }
    }

    public class ClaimNumberInfo
    {
        private string _claimNumber;

        public string ClaimNumber
        {
            get { return string.IsNullOrEmpty(_claimNumber) ? "(none)" : _claimNumber; }
            set { _claimNumber = value; }
        }

        private string _doi;

        public string Doi
        {
            get { return string.IsNullOrEmpty(_doi) ? "(none)" : _doi; }
            set { _doi = value; }
        }

        private string _claimJurisdiction;

        public string ClaimJurisdiction
        {
            get { return string.IsNullOrEmpty(_claimJurisdiction) ? "(none)" : _claimJurisdiction; }
            set { _claimJurisdiction = value; }
        }

        private string _specialInstruction;
        public string SpecialInstruction
        {
            get { return string.IsNullOrEmpty(_specialInstruction) ? "(none)" : _specialInstruction; }
            set { _specialInstruction = value; }
        }

        public string Status { get; set; }

        public DateTime? OpenDateTime { get; set; }

        public string OpenDate
        {
            get
            {
                return OpenDateTime == null
                    ? "(none)"
                    : OpenDateTime.GetValueOrDefault().ToShortDateString();
            }
        }

        public DateTime? CloseDateTime { get; set; }
        public string CloseDate
        {
            get
            {
                return CloseDateTime == null
                    ? "(none)"
                    : CloseDateTime.GetValueOrDefault().ToShortDateString();
            }
        }
        public DateTime? ReopenDateTime { get; set; }
        public string ReopenDate
        {
            get
            {
                return ReopenDateTime == null
                    ? "(none)"
                    : ReopenDateTime.GetValueOrDefault().ToShortDateString();
            }
        }
        public DateTime? RecloseDateDateTime { get; set; }
        public string RecloseDate
        {
            get
            {
                return RecloseDateDateTime == null
                    ? "(none)"
                    : RecloseDateDateTime.GetValueOrDefault().ToShortDateString();
            }
        }
    }

    public class CaseManagerInfo
    {
        private string _name;
        public string Name
        {
            get { return string.IsNullOrEmpty(_name) ? "(none)" : _name; }
            set { _name = value; }
        }
        private string _email;
        public string Email
        {
            get { return string.IsNullOrEmpty(_email) ? "(none)" : _email; }
            set { _email = value; }
        }
        private string _phone;

        public string Phone
        {
            get
            {
                return string.IsNullOrEmpty(_phone) ? "(none)" : _phone.ApplyFormatPhone();
            }
            set
            {
                _phone = value;
            }
        }

        private string _fax;
        public string Fax
        {
            get
            {
                return string.IsNullOrEmpty(_fax) ? "(none)" : _fax.ApplyFormatPhone();
            }
            set
            {
                _fax = value;
            }
        }
        private string _agency;

        public string Agency
        {
            get { return string.IsNullOrEmpty(_agency) ? "(none)" : _agency; }
            set { _agency = value; }
        }
        private string _specialInstruction;
        public string SpecialInstruction
        {
            get { return string.IsNullOrEmpty(_specialInstruction) ? "(none)" : _specialInstruction; }
            set { _specialInstruction = value; }
        }

        public string State { get; set; }
        public string City { get; set; }
        public string Zip { get; set; }
        public string Address { get; set; }

        public string FullAddress
        {
            get
            {
                if (!string.IsNullOrEmpty(Address) && !string.IsNullOrEmpty(State) && !string.IsNullOrEmpty(City) && !string.IsNullOrEmpty(Zip))
                {
                    return Address + ", " + State + ", " + City + ", " + Zip;
                }

                if (string.IsNullOrEmpty(State) && string.IsNullOrEmpty(City) && string.IsNullOrEmpty(Zip))
                {
                    return Address;
                }
                if (string.IsNullOrEmpty(City) && string.IsNullOrEmpty(Zip))
                {
                    return Address + ", " + State;
                }
                if (string.IsNullOrEmpty(Zip))
                {
                    return Address + ", " + State + ", " + City;
                }
                return "(none)";

            }
        }
    }

    public class AttorneyInfo
    {
        private string _name;
        public string Name
        {
            get { return string.IsNullOrEmpty(_name) ? "(none)" : _name; }
            set { _name = value; }
        }

        private string _email;
        public string Email
        {
            get { return string.IsNullOrEmpty(_email) ? "(none)" : _email; }
            set { _email = value; }
        }
        private string _phone;

        public string Phone
        {
            get
            {
                return string.IsNullOrEmpty(_phone) ? "(none)" : _phone.ApplyFormatPhone();
            }
            set
            {
                _phone = value;
            }
        }


        public string State { get; set; }
        public string City { get; set; }
        public string Zip { get; set; }
        public string Address { get; set; }

        public string FullAddress
        {
            get
            {
                if (!string.IsNullOrEmpty(Address) && !string.IsNullOrEmpty(State) && !string.IsNullOrEmpty(City) && !string.IsNullOrEmpty(Zip))
                {
                    return Address + ", " + State + ", " + City + ", " + Zip;
                }

                if (string.IsNullOrEmpty(State) && string.IsNullOrEmpty(City) && string.IsNullOrEmpty(Zip))
                {
                    return Address;
                }
                if (string.IsNullOrEmpty(City) && string.IsNullOrEmpty(Zip))
                {
                    return Address + ", " + State;
                }
                if (string.IsNullOrEmpty(Zip))
                {
                    return Address + ", " + State + ", " + City;
                }
                return "(none)";

            }
        }
    }

    public class EmployerInfo
    {
        private string _name;
        public string Name
        {
            get { return string.IsNullOrEmpty(_name) ? "(none)" : _name; }
            set { _name = value; }
        }

       
        private string _phone;

        public string Phone
        {
            get
            {
                return string.IsNullOrEmpty(_phone) ? "(none)" : _phone.ApplyFormatPhone();
            }
            set
            {
                _phone = value;
            }
        }


        public string State { get; set; }
        public string City { get; set; }
        public string Zip { get; set; }
        public string Address { get; set; }

        public string FullAddress
        {
            get
            {
                if (!string.IsNullOrEmpty(Address) && !string.IsNullOrEmpty(State) && !string.IsNullOrEmpty(City) && !string.IsNullOrEmpty(Zip))
                {
                    return Address + ", " + State + ", " + City + ", " + Zip;
                }

                if (string.IsNullOrEmpty(State) && string.IsNullOrEmpty(City) && string.IsNullOrEmpty(Zip))
                {
                    return Address;
                }
                if (string.IsNullOrEmpty(City) && string.IsNullOrEmpty(Zip))
                {
                    return Address + ", " + State;
                }
                if (string.IsNullOrEmpty(Zip))
                {
                    return Address + ", " + State + ", " + City;
                }
                return "(none)";

            }
        }
        
    }

    public class CollectionMethodInfo
    {
        public bool IsCollectionSite { get; set; }

        private string _collectionSiteName;
        public string CollectionSiteName
        {
            get { return string.IsNullOrEmpty(_collectionSiteName) ? "(none)" : _collectionSiteName; }
            set { _collectionSiteName = value; }
        }

        private string _collectionSiteDate;
        public string CollectionSiteDate
        {
            get { return string.IsNullOrEmpty(_collectionSiteDate) ? "(none)" : _collectionSiteDate; }
            set { _collectionSiteDate = value; }
        }

        private string _treatingPhysician;
        public string TreatingPhysician
        {
            get { return string.IsNullOrEmpty(_treatingPhysician) ? "(none)" : _treatingPhysician; }
            set { _treatingPhysician = value; }
        }

        private string _nextMdVisit;
        public string NextMdVisit
        {
            get { return string.IsNullOrEmpty(_nextMdVisit) ? "(none)" : _nextMdVisit; }
            set { _nextMdVisit = value; }
        }
        private string _npiNumber;
        public string NpiNumber
        {
            get { return string.IsNullOrEmpty(_npiNumber) ? "(none)" : _npiNumber; }
            set { _npiNumber = value; }
        }

        private string _specialHandling;
        public string SpecialHandling
        {
            get { return string.IsNullOrEmpty(_specialHandling) ? "(none)" : _specialHandling; }
            set { _specialHandling = value; }
        }
        private string _rush;
        public string Rush
        {
            get { return string.IsNullOrEmpty(_rush) ? "(none)" : _rush; }
            set { _rush = value; }
        }

        private string _email;
        public string Email
        {
            get { return string.IsNullOrEmpty(_email) ? "(none)" : _email; }
            set { _email = value; }
        }
        private string _phone;

        public string Phone
        {
            get
            {
                return string.IsNullOrEmpty(_phone) ? "(none)" : _phone.ApplyFormatPhone();
            }
            set
            {
                _phone = value;
            }
        }
        private string _fax;
        public string Fax
        {
            get
            {
                return string.IsNullOrEmpty(_fax) ? "(none)" : _fax.ApplyFormatPhone();
            }
            set
            {
                _fax = value;
            }
        }
        private string _address;
        public string Address
        {
            get { return string.IsNullOrEmpty(_address) ? "(none)" : _address; }
            set { _address = value; }
        }
    }

    public class TestingInfo
    {
        public bool CheckOralFluid { get; set; }
        public bool CheckBlood { get; set; }
        public bool CheckHair { get; set; }
        public bool CheckUrine { get; set; }
    }

    #endregion

    #region List
    public class ReasonReferralInfo
    {
        public string NameParent { get; set; }
        public List<string> ListChildren { get; set; }
    }
    
    #endregion

    public class DashboardReferralDetailReferralInfoViewModel : DashboardSharedViewModel
    {
        public DashboardReferralDetailReferralInfoViewModel()
        {
            ReasonReferralInReferralDetailVos = new List<ReasonReferralInReferralDetailVo>();
            ReferralInfo = new ReferralInfo();
            ReferralSourceInfo = new ReferralSourceInfo();
            ReferralSourceInfo = new ReferralSourceInfo();
            PayorInfo = new PayorInfo();
            AdjusterInfo = new AdjusterInfo();
            ReasonReferralInfos = new Dictionary<int, ReasonReferralInfo>();
            ClaimantInfo = new ClaimantInfo();
            ClaimNumberInfo= new ClaimNumberInfo();
            CaseManagerInfo =new CaseManagerInfo();
            AttorneyInfo= new AttorneyInfo();
            EmployerInfo = new EmployerInfo();
            CollectionMethodInfo = new CollectionMethodInfo();
            TestingInfo = new TestingInfo();
        }
        
        public ReferralInfo ReferralInfo { get; set; }
        public ReferralSourceInfo ReferralSourceInfo { get; set; }
        public List<ReasonReferralInReferralDetailVo> ReasonReferralInReferralDetailVos { get; set; }
        public Dictionary<int, ReasonReferralInfo> ReasonReferralInfos { get; set; }
        public void SetReasonReferralInfos()
        {
            if (ReasonReferralInReferralDetailVos == null || ReasonReferralInReferralDetailVos.Count <= 0) return;
            foreach (var item in ReasonReferralInReferralDetailVos.Where(item => item.ParentId.GetValueOrDefault() != 0))
            {
                if (ReasonReferralInfos.ContainsKey(item.ParentId.GetValueOrDefault()))
                {
                    ReasonReferralInfos[item.ParentId.GetValueOrDefault()].ListChildren.Add(item.Name);
                }
                else
                {

                    ReasonReferralInfos.Add(item.ParentId.GetValueOrDefault(), new ReasonReferralInfo
                    {
                        NameParent = item.NameParent,
                        ListChildren = new List<string>{item.Name}
                    });
                }
            }
        }
        public PayorInfo PayorInfo { get; set; }
        public AdjusterInfo AdjusterInfo { get; set; }
        public ClaimantInfo ClaimantInfo { get; set; }
        public ClaimNumberInfo ClaimNumberInfo { get; set; }
        public CaseManagerInfo CaseManagerInfo { get; set; }
        public AttorneyInfo AttorneyInfo { get; set; }
        public EmployerInfo EmployerInfo { get; set; }
        public CollectionMethodInfo CollectionMethodInfo { get; set; }

        public TestingInfo TestingInfo { get; set; }

        public int ReferralId { get; set; }
        public int PanelTypeId { get; set; }
        public bool AdjusterIsReferral { get; set; }
    }
    
}