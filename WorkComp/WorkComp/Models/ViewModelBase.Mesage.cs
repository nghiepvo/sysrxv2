﻿using Framework.Service.Translation;

namespace WorkComp.Models
{
    public partial class ViewModelBase
    {
        public string DirtyDialogMessageText
        {
            get { return SystemMessageLookup.GetMessage("DirtyDialogMessageText"); }
        }

        public string NoRecord
        {
            get { return SystemMessageLookup.GetMessage("NoRecord"); }
        }

        public string ConfirmationRequiredText
        {
            get { return SystemMessageLookup.GetMessage("ConfirmationRequiredText"); }
        }
        public string ConfirmDeleteText
        {
            get { return SystemMessageLookup.GetMessage("ConfirmDeleteText"); }
        }
        public string ConfirmCompleteTaskText
        {
            get { return SystemMessageLookup.GetMessage("ConfirmCompleteTaskText"); }
        }
        public string NoText
        {
            get { return SystemMessageLookup.GetMessage("NoText"); }
        }
        public string YesText
        {
            get { return SystemMessageLookup.GetMessage("YesText"); }
        }
        public string CreateText
        {
            get { return SystemMessageLookup.GetMessage("CreateText"); }
        }
        public string UpdateText
        {
            get { return SystemMessageLookup.GetMessage("UpdateText"); }
        }
        public string CannotCopyText
        {
            get { return SystemMessageLookup.GetMessage("CannotCopyText"); }
        }

        public string UpdateSuccessText
        {
            get { return SystemMessageLookup.GetMessage("UpdateSuccessText"); }
        }
        public string RestorePasswordSuccessText
        {
            get { return SystemMessageLookup.GetMessage("RestorePasswordSuccessText"); }
        }
        public string CreateSuccessText
        {
            get { return SystemMessageLookup.GetMessage("CreateSuccessText"); }
        }

        public string DeleteSuccessText
        {
            get { return SystemMessageLookup.GetMessage("DeleteSuccessText"); }
        }

        public string ActiveText
        {
            get { return SystemMessageLookup.GetMessage("ActiveText"); }
        }

        public string InactiveText
        {
            get { return SystemMessageLookup.GetMessage("InactiveText"); }
        }

        public string SendEmailSuccessText
        {
            get { return SystemMessageLookup.GetMessage("SendEmailSuccess"); }
        }

        public string SendEmailFailedText
        {
            get { return SystemMessageLookup.GetMessage("SendEmailFailed"); }
        }

        public string RequiredFileStringFormat(string field)
        {
            return string.Format(SystemMessageLookup.GetMessage("RequiredTextResourceKey"), field);
        }

        public string GetMessage(string key)
        {
            return SystemMessageLookup.GetMessage("RequiredTextResourceKey");
        }
    }
}