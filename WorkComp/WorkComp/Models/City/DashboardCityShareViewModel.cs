﻿using Framework.DomainModel.ValueObject;

namespace WorkComp.Models.City
{
    public class DashboardCityShareViewModel : DashboardSharedViewModel
    {
        public string Name { get; set; }
        public int? StateId { get; set; }
        public LookupItemVo StateDataSource { get; set; }
    }
}