﻿namespace WorkComp.Models.City
{
    public class DashboardCityDataViewModel :
        MasterfileViewModelBase<Framework.DomainModel.Entities.City>
    {
        public override void MapFromClientParameters(MasterfileParameter parameters)
        {
            SharedViewModel = MapFromClientParameters<DashboardCityShareViewModel>(parameters);
        }
    }
}