﻿using AutoMapper;
using Framework.DomainModel.ValueObject;
using Framework.Mapping;
using WorkComp.Models.PanelType;

namespace WorkComp.Models.Mapping
{
    public class PanelTypeMappingProfile : Profile
    {
        protected override void Configure()
        {
            Mapper.CreateMap<Framework.DomainModel.Entities.PanelType, DashboardPanelTypeShareViewModel>()
                .ForMember(desc => desc.PayerDataSource, opt => opt.Ignore())
                .AfterMap((s,d)=>
                {
                    if (s.PayerId.GetValueOrDefault() != 0)
                    {
                        var objPayer = s.Payer;
                        if (objPayer != null)
                        {
                            d.PayerDataSource = new LookupItemVo
                            {
                                KeyId = s.PayerId.GetValueOrDefault(),
                                DisplayName = objPayer.Name
                            };
                        }
                    }
                    else
                    {
                        d.PayerDataSource = null;
                    }
                    d.PanelCodeGrid=new PanelCodeGridViewModel
                    {
                        GridId = "PanelCodeGrid",
                        PanelTypeId = d.Id
                    };
                });
            Mapper.CreateMap<DashboardPanelTypeShareViewModel, Framework.DomainModel.Entities.PanelType>();

            Mapper.CreateMap<Framework.DomainModel.Entities.PanelType, DashboardPanelTypeDataViewModel>()
                .AfterMap((s, d) =>
                {
                    d.SharedViewModel = s.MapTo<DashboardPanelTypeShareViewModel>();
                });

            Mapper.CreateMap<DashboardPanelTypeDataViewModel, Framework.DomainModel.Entities.PanelType>()
                .AfterMap((s, d) =>
                {
                    var shareViewModel = s.SharedViewModel as DashboardPanelTypeShareViewModel;
                    if (shareViewModel != null)
                    {
                        d = shareViewModel.MapPropertiesToInstance(d);
                        if (shareViewModel.PayerId.GetValueOrDefault() == 0)
                        {
                            d.PayerId = null;
                        }
                    }
                    
                });
        }
    }
}