﻿using AutoMapper;
using Framework.DomainModel.ValueObject;
using Framework.Mapping;
using WorkComp.Models.Diary;

namespace WorkComp.Models.Mapping
{
    public class DiaryMappingProfile : Profile
    {
        protected override void Configure()
        {
            Mapper.CreateMap<Framework.DomainModel.Entities.Diary, DashboardDiaryShareViewModel>()
                .ForMember(desc => desc.ReferralDataSource, opt => opt.Ignore())
                .AfterMap((s, d) =>
                {
                    if (s.ReferralId != 0)
                    {
                        d.ReferralDataSource = new LookupItemVo
                        {
                            KeyId = s.ReferralId,
                            DisplayName = s.Referral.ControlNumber
                        };
                    }
                    else
                    {
                        d.ReferralDataSource = null;
                    }

                });
            Mapper.CreateMap<DashboardDiaryShareViewModel, Framework.DomainModel.Entities.Diary>();

            Mapper.CreateMap<Framework.DomainModel.Entities.Diary, DashboardDiaryDataViewModel>()
                .AfterMap((s, d) =>
                {
                    d.SharedViewModel = s.MapTo<DashboardDiaryShareViewModel>();
                });

            Mapper.CreateMap<DashboardDiaryDataViewModel, Framework.DomainModel.Entities.Diary>()
                .AfterMap((s, d) =>
                {
                    d = s.SharedViewModel.MapPropertiesToInstance(d);
                });
        }
    }
}