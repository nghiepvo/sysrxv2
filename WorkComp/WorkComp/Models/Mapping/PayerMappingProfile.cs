﻿using AutoMapper;
using Framework.DomainModel.ValueObject;
using Framework.Mapping;
using WorkComp.Models.Payer;

namespace WorkComp.Models.Mapping
{
    public class PayerMappingProfile : Profile
    {
        protected override void Configure()
        {
            Mapper.CreateMap<Framework.DomainModel.Entities.Payer, DashboardPayerShareViewModel>()
                .ForMember(desc => desc.StateDataSource, opt => opt.Ignore())
                .ForMember(desc => desc.CityDataSource, opt => opt.Ignore())
                .ForMember(desc => desc.ZipDataSource, opt => opt.Ignore())
                .AfterMap((s, d) =>
                {
                    if (s.ZipId.GetValueOrDefault() != 0)
                    {
                        var objZip = s.Zip;
                        if (objZip != null)
                        {
                            d.ZipDataSource = new LookupItemVo
                            {
                                KeyId = s.ZipId.GetValueOrDefault(),
                                DisplayName = objZip.Name
                            };
                            if (s.CityId.GetValueOrDefault() != 0)
                            {
                                var objCity = s.City;
                                if (objCity != null)
                                {
                                    d.CityDataSource = new LookupItemVo
                                    {
                                        KeyId = s.CityId.GetValueOrDefault(),
                                        DisplayName = objCity.Name
                                    };
                                    var objState = objCity.State;
                                    if (objState != null)
                                    {
                                        d.StateId = objState.Id;
                                        d.StateDataSource = new LookupItemVo
                                        {
                                            KeyId = objState.Id,
                                            DisplayName = objState.Name
                                        };
                                    }
                                    else
                                    {
                                        d.StateDataSource = null;
                                    }
                                }
                                else
                                {
                                    d.CityDataSource = null;
                                    d.StateDataSource = null;
                                }
                            }
                            else
                            {
                                d.CityDataSource = null;
                                d.StateDataSource = null;
                            }
                        }
                        else
                        {
                            d.CityDataSource = null;
                            d.StateDataSource = null;
                            d.ZipDataSource = null;
                        }

                    }
                    else
                    {
                        d.CityDataSource = null;
                        d.StateDataSource = null;
                        d.ZipDataSource = null;
                    }

                });
            Mapper.CreateMap<DashboardPayerShareViewModel, Framework.DomainModel.Entities.Payer>();

            Mapper.CreateMap<Framework.DomainModel.Entities.Payer, DashboardPayerDataViewModel>()
                 .AfterMap((s, d) =>
                 {
                     d.SharedViewModel = s.MapTo<DashboardPayerShareViewModel>();
                 });

            Mapper.CreateMap<DashboardPayerDataViewModel, Framework.DomainModel.Entities.Payer>()
                .AfterMap((s, d) =>
                {
                    d = s.SharedViewModel.MapPropertiesToInstance(d);
                });
        }
    }
}