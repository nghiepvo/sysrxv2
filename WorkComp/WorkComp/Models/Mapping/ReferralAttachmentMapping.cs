﻿using AutoMapper;
using Framework.DomainModel.Entities;
using WorkComp.Models.Referral;

namespace WorkComp.Models.Mapping
{
    public class ReferralAttachmentMapping : Profile
    {
        protected override void Configure()
        {
            // Need spefic mapping from ViewModel to Entity
            Mapper.CreateMap<FileAttachment, ReferralAttachment>()
                .ForMember(x => x.AttachedFileSize, opt => opt.MapFrom(s => s.FileSize));


            // Only need to map generic into FileAttachment for Entity to ViewModel because service already
            // handle filtering the DocumentTypeID
            Mapper.CreateMap<ReferralAttachment, FileAttachment>()
                .ForMember(x => x.FileSize, opt => opt.MapFrom(s => s.AttachedFileSize));
        }
    }
}