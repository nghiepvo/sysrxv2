﻿using AutoMapper;
using Framework.DomainModel.ValueObject;
using Framework.Mapping;
using WorkComp.Models.Heading;

namespace WorkComp.Models.Mapping
{
    public class HeadingMappingProfile : Profile
    {
        protected override void Configure()
        {
            Mapper.CreateMap<Framework.DomainModel.Entities.Heading, DashboardHeadingShareViewModel>();
            Mapper.CreateMap<DashboardHeadingShareViewModel, Framework.DomainModel.Entities.Heading>();

            Mapper.CreateMap<Framework.DomainModel.Entities.Heading, DashboardHeadingDataViewModel>()
                .AfterMap((s, d) =>
                {
                    d.SharedViewModel = s.MapTo<DashboardHeadingShareViewModel>();
                });

            Mapper.CreateMap<DashboardHeadingDataViewModel, Framework.DomainModel.Entities.Heading>()
                .AfterMap((s, d) =>
                {
                    d = s.SharedViewModel.MapPropertiesToInstance(d);
                });
        }
    }
}