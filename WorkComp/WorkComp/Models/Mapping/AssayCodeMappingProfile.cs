﻿using AutoMapper;
using Framework.DomainModel.ValueObject;
using Framework.Mapping;
using WorkComp.Models.AssayCode;

namespace WorkComp.Models.Mapping
{
    public class AssayCodeMappingProfile : Profile
    {
        protected override void Configure()
        {
            Mapper.CreateMap<Framework.DomainModel.Entities.AssayCode, DashboardAssayCodeShareViewModel>().AfterMap(
                (s, d) =>
                {
                    if (s.AssayCodeDescriptionId != 0)
                    {
                        var objAssayCodeDescription = s.AssayCodeDescription;
                        if (objAssayCodeDescription!= null)
                        {
                            d.AssayCodeDescriptionDataSource = new LookupItemVo()
                            {
                                KeyId = objAssayCodeDescription.Id,
                                DisplayName = objAssayCodeDescription.Description
                            };
                        }
                    }
                });
            Mapper.CreateMap<DashboardAssayCodeShareViewModel, Framework.DomainModel.Entities.AssayCode>()
                .AfterMap((s, d) =>
                {
                    if (s.AssayCodeDescriptionId == null)
                    {
                        d.AssayCodeDescriptionId = 0;
                    }
                    if (s.SampleTestingTypeId == null)
                    {
                        d.SampleTestingTypeId = 0;
                    }
                });

            Mapper.CreateMap<Framework.DomainModel.Entities.AssayCode, DashboardAssayCodeDataViewModel>()
                 .AfterMap((s, d) =>
                 {
                     d.SharedViewModel = s.MapTo<DashboardAssayCodeShareViewModel>();
                 });

            Mapper.CreateMap<DashboardAssayCodeDataViewModel, Framework.DomainModel.Entities.AssayCode>()
                .AfterMap((s, d) =>
                {
                    d = s.SharedViewModel.MapPropertiesToInstance(d);
                });
        }
    }
}