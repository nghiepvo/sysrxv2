﻿using System;
using System.Globalization;
using AutoMapper;
using Framework.DomainModel.Entities.Common;
using Framework.DomainModel.ValueObject;
using Framework.Mapping;
using Framework.Utility;
using WorkComp.Models.ClaimNumber;


namespace WorkComp.Models.Mapping
{
    public class ClaimNumberMappingProfile : Profile
    {
        protected override void Configure()
        {
            Mapper.CreateMap<Framework.DomainModel.Entities.ClaimNumber, DashboardClaimNumberShareViewModel>()
                .ForMember(desc => desc.StateDataSource, opt => opt.Ignore())
                .ForMember(desc => desc.AdjusterDataSource, opt => opt.Ignore())
                .ForMember(desc => desc.ClaimantDataSource, opt => opt.Ignore())
                .AfterMap((s, d) =>
                {
                    var objState = s.State;
                    if (objState != null)
                    {
                        d.StateId = objState.Id;
                        d.StateDataSource = new LookupItemVo
                        {
                            KeyId = objState.Id,
                            DisplayName = objState.AbbreviationName
                        };
                    }
                    else
                    {
                        d.StateDataSource = null;
                    }

                    var objAdjuster = s.Adjuster;
                    if (objAdjuster != null)
                    {
                        d.AdjusterId = objAdjuster.Id;
                        d.AdjusterDataSource = new LookupItemVo
                        {
                            KeyId = objAdjuster.Id,
                            DisplayName = objAdjuster.FirstName + " " + objAdjuster.MiddleName + " " + objAdjuster.LastName
                        };
                    }
                    else
                    {
                        d.AdjusterDataSource = null;
                    }

                    var objClaimant = s.Claimant;
                    if (objClaimant != null)
                    {
                        d.ClaimantId = objClaimant.Id;
                        d.ClaimantDataSource = new LookupItemVo
                        {
                            KeyId = objClaimant.Id,
                            DisplayName = objClaimant.FirstName + " " + objClaimant.MiddleName + " " + objClaimant.LastName
                        };
                    }
                    else
                    {
                        d.ClaimantDataSource = null;
                    }

                    if (s.PayerId != 0)
                    {
                        var objPayer = s.Payer;
                        if (objPayer == null) return;
                        d.PayerId = objPayer.Id;
                        d.PayerDataSource = new LookupItemVo{KeyId = objPayer.Id,DisplayName = objPayer.Name};
                    }

                    if (s.BranchId != null)
                    {
                        var objBranch = s.Branch;
                        if (objBranch != null)
                        {
                            d.BranchId = objBranch.Id;
                            d.BranchDataSource = new LookupItemVo{KeyId = objBranch.Id,DisplayName = objBranch.Name};
                        }
                    }

                    if (s.EmployerId != 0)
                    {
                        var objEmployer = s.Employer;
                        if (objEmployer != null)
                        {
                            d.EmployerId = objEmployer.Id;
                            d.EmployerDataSource = new LookupItemVo { KeyId = objEmployer.Id, DisplayName = objEmployer.Name };
                        }
                    }

                    //set DateServer 
                    d.DateTimeServer = DateTime.Now;
                    d.CurrentStatus = s.Status;
                    switch (s.Status)
                    {
                        case 1: case 2:
                            d.LableIsOpen = XmlDataHelpper.Instance.GetValue(XmlDataTypeEnum.ClaimNumberStatus.ToString(), (s.Status + 1).ToString(CultureInfo.InvariantCulture)); 
                            d.EnableIsOpen = true;
                            d.IsOpen = false;
                            d.Status = Convert.ToInt16(s.Status + 1);
                            break;
                        case 3:
                            if (s.ReOpenDate == null && s.ReClosedDate == null )
                            {
                                d.LableIsOpen = XmlDataHelpper.Instance.GetValue(XmlDataTypeEnum.ClaimNumberStatus.ToString(), (s.Status + 1).ToString(CultureInfo.InvariantCulture));
                                d.EnableIsOpen = true;
                                d.IsOpen = false;
                                d.Status = Convert.ToInt16(s.Status + 1);
                            }
                            else
                            {
                                d.LableIsOpen = XmlDataHelpper.Instance.GetValue(XmlDataTypeEnum.ClaimNumberStatus.ToString(), (s.Status + 1).ToString(CultureInfo.InvariantCulture));
                                d.EnableIsOpen = true;
                                d.IsOpen = false;
                                d.Status = Convert.ToInt16(s.Status + 1);
                            }
                            break;
                        case 4:
                            if (s.ReClosedDate == null)
                            {
                                d.LableIsOpen = XmlDataHelpper.Instance.GetValue(XmlDataTypeEnum.ClaimNumberStatus.ToString(), (s.Status + 1).ToString(CultureInfo.InvariantCulture));
                                d.EnableIsOpen = true;
                                d.IsOpen = false;
                                d.Status = Convert.ToInt16(s.Status + 1);
                            }
                            else
                            {
                                d.LableIsOpen = XmlDataHelpper.Instance.GetValue(XmlDataTypeEnum.ClaimNumberStatus.ToString(), (s.Status - 1).ToString(CultureInfo.InvariantCulture));
                                d.EnableIsOpen = true;
                                d.IsOpen = false;
                                d.Status = Convert.ToInt16(s.Status - 1);
                            }
                            break;
                    }
                });
            
            Mapper.CreateMap<DashboardClaimNumberShareViewModel, Framework.DomainModel.Entities.ClaimNumber>()
                .AfterMap((s, d) =>
                {
                    if (s.StateId == null)
                        d.StateId = 0;
                    if (s.PayerId == null)
                        d.PayerId = 0;
                    if (s.AdjusterId == null)
                        d.AdjusterId = 0;
                    if (s.ClaimantId== null)
                        d.ClaimantId = 0;
                    if (s.EmployerId == null)
                        d.EmployerId = 0;
                    //Case Create New
                    if (!s.EnableIsOpen)
                    {
                        d.OpenDate = s.DateTimeServer;
                        d.CloseDate = null;
                        d.ReOpenDate = null;
                        d.ReClosedDate = null;
                        d.Status = 1;
                    }
                    else
                    {
                        if (s.IsOpen)
                        {
                            switch (s.Status)
                            {
                                case 2:
                                    d.CloseDate = s.DateTimeServer;
                                    break;
                                case 3:
                                    d.ReOpenDate = s.DateTimeServer;
                                    break;
                                case 4:
                                    d.ReClosedDate = s.DateTimeServer;
                                    break;
                            }
                        }
                        else
                        {
                            d.Status = Convert.ToInt16(s.Status - 1);
                        }                    
                    }
                });

            Mapper.CreateMap<Framework.DomainModel.Entities.ClaimNumber, DashboardClaimNumberDataViewModel>()
                 .AfterMap((s, d) =>
                 {
                     d.SharedViewModel = s.MapTo<DashboardClaimNumberShareViewModel>();
                 });

            Mapper.CreateMap<DashboardClaimNumberDataViewModel, Framework.DomainModel.Entities.ClaimNumber>()
                .AfterMap((s, d) =>
                {
                    d = s.SharedViewModel.MapPropertiesToInstance(d);
                });
        }
    }
}