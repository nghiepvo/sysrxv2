﻿using AutoMapper;
using Framework.Mapping;
using WorkComp.Models.ReferralType;


namespace WorkComp.Models.Mapping
{
    public class ReferralTypeMappingProfile : Profile
    {
        protected override void Configure()
        {
            Mapper.CreateMap<Framework.DomainModel.Entities.ReferralType, DashboardReferralTypeShareViewModel>();
            Mapper.CreateMap<DashboardReferralTypeShareViewModel, Framework.DomainModel.Entities.ReferralType>();

            Mapper.CreateMap<Framework.DomainModel.Entities.ReferralType, DashboardReferralTypeDataViewModel>()
                 .AfterMap((s, d) =>
                 {
                     d.SharedViewModel = s.MapTo<DashboardReferralTypeShareViewModel>();
                 });

            Mapper.CreateMap<DashboardReferralTypeDataViewModel, Framework.DomainModel.Entities.ReferralType>()
                .AfterMap((s, d) =>
                {
                    d = s.SharedViewModel.MapPropertiesToInstance(d);
                });
        }
    }
}