﻿using AutoMapper;
using Framework.Mapping;
using WorkComp.Models.Configuration;

namespace WorkComp.Models.Mapping
{
    public class ConfigurationMappingProfile : Profile
    {
        protected override void Configure()
        {
            Mapper.CreateMap<Framework.DomainModel.Entities.Configuration, DashboardConfigurationShareViewModel>().AfterMap(
                (s, d) =>
                {

                });
            Mapper.CreateMap<DashboardConfigurationShareViewModel, Framework.DomainModel.Entities.Configuration>().AfterMap(
                (s, d) =>
                {
                    
                });

            Mapper.CreateMap<Framework.DomainModel.Entities.Configuration, DashboardConfigurationDataViewModel>()
                .AfterMap((s, d) =>
                {
                    d.SharedViewModel = s.MapTo<DashboardConfigurationShareViewModel>();
                });

            Mapper.CreateMap<DashboardConfigurationDataViewModel, Framework.DomainModel.Entities.Configuration>()
                .AfterMap((s, d) =>
                {
                    d = s.SharedViewModel.MapPropertiesToInstance(d);
                });
        }
    }
}