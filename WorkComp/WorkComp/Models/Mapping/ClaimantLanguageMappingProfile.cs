﻿using AutoMapper;
using Framework.Mapping;
using WorkComp.Models.ClaimantLanguage;

namespace WorkComp.Models.Mapping
{
    public class ClaimantLanguageMappingProfile : Profile
    {
        protected override void Configure()
        {
            Mapper.CreateMap<Framework.DomainModel.Entities.ClaimantLanguage, DashboardClaimantLanguageShareViewModel>();
            Mapper.CreateMap<DashboardClaimantLanguageShareViewModel, Framework.DomainModel.Entities.ClaimantLanguage>();

            Mapper.CreateMap<Framework.DomainModel.Entities.ClaimantLanguage, DashboardClaimantLanguageDataViewModel>()
                 .AfterMap((s, d) =>
                 {
                     d.SharedViewModel = s.MapTo<DashboardClaimantLanguageShareViewModel>();
                 });

            Mapper.CreateMap<DashboardClaimantLanguageDataViewModel, Framework.DomainModel.Entities.ClaimantLanguage>()
                .AfterMap((s, d) =>
                {
                    d = s.SharedViewModel.MapPropertiesToInstance(d);
                });
        }
    }
}