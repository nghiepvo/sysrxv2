﻿using AutoMapper;
using Framework.DomainModel.ValueObject;
using Framework.Mapping;
using WorkComp.Models.Employer;

namespace WorkComp.Models.Mapping
{
    public class EmployerMappingProfile : Profile
    {
        protected override void Configure()
        {
            Mapper.CreateMap<Framework.DomainModel.Entities.Employer, DashboardEmployerShareViewModel>()
                .ForMember(desc => desc.StateDataSource, opt => opt.Ignore())
                .ForMember(desc => desc.CityDataSource, opt => opt.Ignore())
                .AfterMap((s, d) =>
                {

                    if (s.ZipId.GetValueOrDefault() != 0)
                    {

                        var objZip = s.Zip;
                        if (objZip != null)
                        {
                            d.ZipDataSource = new LookupItemVo
                            {
                                KeyId = s.ZipId.GetValueOrDefault(),
                                DisplayName = objZip.Name
                            };
                        }
                    }

                    if (s.CityId.GetValueOrDefault() != 0)
                    {
                         var objCity = s.City;
                        if (objCity != null)
                        {
                            d.CityDataSource = new LookupItemVo
                            {
                                KeyId = s.CityId.GetValueOrDefault(),
                                DisplayName = objCity.Name
                            };
                        }
                    }

                    if (s.StateId.GetValueOrDefault() != 0)
                    {
                         var objState = s.State;
                        if (objState != null)
                        {
                            d.StateId = objState.Id;
                            d.StateDataSource = new LookupItemVo
                            {
                                KeyId = objState.Id,
                                DisplayName = objState.Name
                            };
                        }
                    }

                }); 
            Mapper.CreateMap<DashboardEmployerShareViewModel, Framework.DomainModel.Entities.Employer>();

            Mapper.CreateMap<Framework.DomainModel.Entities.Employer, DashboardEmployerDataViewModel>()
                 .AfterMap((s, d) =>
                 {
                     d.SharedViewModel = s.MapTo<DashboardEmployerShareViewModel>();
                 });

            Mapper.CreateMap<DashboardEmployerDataViewModel, Framework.DomainModel.Entities.Employer>()
                .AfterMap((s, d) =>
                {
                    d = s.SharedViewModel.MapPropertiesToInstance(d);
                });
        }
    }
}