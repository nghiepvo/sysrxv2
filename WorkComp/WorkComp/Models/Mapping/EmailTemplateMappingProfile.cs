﻿using AutoMapper;
using Framework.Mapping;
using WorkComp.Models.EmailTemplate;

namespace WorkComp.Models.Mapping
{
    public class EmailTemplateMappingProfile : Profile
    {
        protected override void Configure()
        {
            Mapper.CreateMap<Framework.DomainModel.Entities.EmailTemplate, DashboardEmailTemplateShareViewModel>();
                
                
            Mapper.CreateMap<DashboardEmailTemplateShareViewModel, Framework.DomainModel.Entities.EmailTemplate>()
                .ForMember(desc => desc.IsActived, opt => opt.Ignore())
                .ForMember(desc => desc.Type, opt => opt.Ignore());

            Mapper.CreateMap<Framework.DomainModel.Entities.EmailTemplate, DashboardEmailTemplateDataViewModel>()
                 .AfterMap((s, d) =>
                 {
                     d.SharedViewModel = s.MapTo<DashboardEmailTemplateShareViewModel>();
                 });

            Mapper.CreateMap<DashboardEmailTemplateDataViewModel, Framework.DomainModel.Entities.EmailTemplate>()
                .ForMember(desc => desc.IsActived, opt => opt.Ignore())
                .ForMember(desc => desc.Type, opt => opt.Ignore())
                .AfterMap((s, d) =>
                {
                    d = s.SharedViewModel.MapPropertiesToInstance(d);
                });
        }
    }
}