﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using AutoMapper;
using Framework.DomainModel.Entities;
using Framework.DomainModel.ValueObject;
using Framework.Mapping;
using Framework.Utility;
using WorkComp.Models.Referral;

namespace WorkComp.Models.Mapping
{
    public class ReferralMapping : Profile
    {
        protected override void Configure()
        {
            Mapper.CreateMap<Framework.DomainModel.Entities.Referral, DashboardReferralShareViewModel>()
                .ForMember(desc => desc.ClaimInfo, opt => opt.Ignore())
                .ForMember(desc => desc.ReferralInfo, opt => opt.Ignore())
                .ForMember(desc => desc.IcdInfo, opt => opt.Ignore())
                .ForMember(desc => desc.TestingInfo, opt => opt.Ignore())
                .ForMember(desc => desc.MedicationHistoryInfo, opt => opt.Ignore())
                .ForMember(desc => desc.TaskGroupInfo, opt => opt.Ignore())
                .ForMember(desc => desc.AttachmentsInfo, opt => opt.Ignore())
                .AfterMap((s, d) =>
                {
                    d.ClaimInfo = s.MapTo<DashboardClaimInfoForReferralViewModel>();
                    d.ReferralInfo = s.MapTo<DashboardReferralInfoForReferralViewModel>();
                    d.IcdInfo = s.MapTo<DashboardIcdInfoForReferralViewModel>();
                    d.TestingInfo = s.MapTo<DashboardTestingInfoForReferralViewModel>();
                    d.MedicationHistoryInfo = s.MapTo<DashboardMedicationHistoryInfoForReferralViewModel>();
                    d.AttachmentsInfo = s.MapTo<ReferralFileAttachmentsGridViewModel>();
                });

            Mapper
                .CreateMap<Framework.DomainModel.Entities.Referral, ReferralFileAttachmentsGridViewModel>()
                .AfterMap((s, d) =>
                {
                    d.ReferralId = s.Id;
                });

            Mapper
                .CreateMap<Framework.DomainModel.Entities.Referral, DashboardMedicationHistoryInfoForReferralViewModel>()
                .AfterMap((s, d) =>
                {
                    d.ReferralId = s.Id;
                });


            Mapper.CreateMap<Framework.DomainModel.Entities.Referral, DashboardTestingInfoForReferralViewModel>()
                .AfterMap((s, d) =>
                {
                    if (s.PanelType != null)
                    {
                        d.PanelTypeDataSource = new LookupItemVo
                        {
                            KeyId = s.PanelTypeId.GetValueOrDefault(),
                            DisplayName = s.PanelType.Name
                        };
                    }
                    else
                    {
                        d.PanelTypeDataSource = null;
                    }

                    if (s.ClaimNumber!= null && s.ClaimNumber.Payer != null)
                    {
                        d.PayerId = s.ClaimNumber.Payer.Id;
                        d.PayerName = s.ClaimNumber.Payer.Name;
                    }
                    d.PanelTypeInfo.PanelTypeId = s.PanelTypeId;
                    d.CollectionMethodId = s.IsCollectionSite.GetValueOrDefault() ? 1 : 2;
                    if (s.IsCollectionSite.GetValueOrDefault())
                    {
                        if (s.ReferralCollectionSites.Count != 0)
                        {
                            var objReferralCollectionSite = s.ReferralCollectionSites.ElementAt(0);
                            d.CollectionSiteSpecialInstructions = objReferralCollectionSite.SpecialInstructions;
                            if (objReferralCollectionSite.CollectionSite != null)
                            {
                                d.CollectionSiteDataSource = new LookupItemVo
                                {
                                    KeyId = objReferralCollectionSite.CollectionSiteId.GetValueOrDefault(),
                                    DisplayName = objReferralCollectionSite.CollectionSite.Name
                                };
                            }
                            d.CollectionSiteDate = objReferralCollectionSite.CollectionDate;
                            d.CollectionSiteInfo.CollectionSiteId = objReferralCollectionSite.CollectionSiteId;
                            d.CollectionSiteInfo.CollectionSiteAddress = objReferralCollectionSite.Address;
                            d.CollectionSiteInfo.Phone = objReferralCollectionSite.Phone.ApplyFormatPhone();
                            d.CollectionSiteInfo.Fax = objReferralCollectionSite.Fax.ApplyFormatPhone();
                        }
                    }
                    else
                    {
                        if (s.ReferralNpiNumbers.Count != 0)
                        {
                            var objReferralTreatingPhysician = s.ReferralNpiNumbers.ElementAt(0);
                            d.TreatingPhysicianSpecialHandling = objReferralTreatingPhysician.SpecialHandling;
                            if (objReferralTreatingPhysician.NpiNumber != null)
                            {
                                d.TreatingPhysicianDataSource = new LookupItemVo
                                {
                                    KeyId = objReferralTreatingPhysician.NpiNumberId.GetValueOrDefault(),
                                    DisplayName = objReferralTreatingPhysician.NpiNumber.FirstName + " " + objReferralTreatingPhysician.NpiNumber.MiddleName + " " + objReferralTreatingPhysician.NpiNumber.LastName
                                };
                                d.NpiNumberDataSource = new LookupItemVo
                                {
                                    KeyId = objReferralTreatingPhysician.NpiNumberId.GetValueOrDefault(),
                                    DisplayName = objReferralTreatingPhysician.NpiNumber.Npi
                                };
                            }
                            d.NextMdVisitDate = objReferralTreatingPhysician.NextMdVisitDate;
                            d.TreatingPhysicianInfo.TreatingPhysicianAddress = objReferralTreatingPhysician.Address;
                            d.TreatingPhysicianInfo.Phone = objReferralTreatingPhysician.Phone.ApplyFormatPhone();
                            d.TreatingPhysicianInfo.Fax = objReferralTreatingPhysician.Fax.ApplyFormatPhone();
                            d.TreatingPhysicianInfo.TreatingPhysicianEmail = objReferralTreatingPhysician.Email;
                            d.TreatingPhysicianInfo.TreatingPhysicianId = objReferralTreatingPhysician.NpiNumberId;
                        }
                    }
                    var objListSampleTestingType = s.ReferralSampleTestingTypes.ToList();
                    var objUrine =
                        objListSampleTestingType.FirstOrDefault(
                            o => o.SampleTestingType != null && o.SampleTestingType.Name.Trim().ToLower() == "urine");
                    if (objUrine != null)
                    {
                        d.CheckUrine = true;
                    }
                    var objOral =
                        objListSampleTestingType.FirstOrDefault(
                            o => o.SampleTestingType != null && o.SampleTestingType.Name.Trim().ToLower() == "oral fluid");
                    if (objOral != null)
                    {
                        d.CheckOralFluid = true;
                    }
                    var objBlood =
                        objListSampleTestingType.FirstOrDefault(
                            o => o.SampleTestingType != null && o.SampleTestingType.Name.Trim().ToLower() == "blood");
                    if (objBlood != null)
                    {
                        d.CheckBlood = true;
                    }
                    var objHair =
                        objListSampleTestingType.FirstOrDefault(
                            o => o.SampleTestingType != null && o.SampleTestingType.Name.Trim().ToLower() == "hair");
                    if (objHair != null)
                    {
                        d.CheckHair = true;
                    }
                });

            Mapper.CreateMap<Framework.DomainModel.Entities.Referral, DashboardIcdInfoForReferralViewModel>()
                .AfterMap((s,d)=>
                {
                    d.ReferralId = s.Id;
                });

            Mapper.CreateMap<Framework.DomainModel.Entities.Referral, DashboardClaimInfoForReferralViewModel>()
                .ForMember(desc => desc.ProductTypeDataSource, opt => opt.Ignore())
                .ForMember(desc => desc.ReferralSourceDataSource, opt => opt.Ignore())
                .ForMember(desc => desc.ClaimNumberDataSource, opt => opt.Ignore())
                .ForMember(desc => desc.PayerDataSource, opt => opt.Ignore())
                .ForMember(desc => desc.BranchDataSource, opt => opt.Ignore())
                .ForMember(desc => desc.AdjusterDataSource, opt => opt.Ignore())
                .ForMember(desc => desc.ClaimantDataSource, opt => opt.Ignore())
                .ForMember(desc => desc.CaseManagerDataSource, opt => opt.Ignore())
                .ForMember(desc => desc.EmployerDataSource, opt => opt.Ignore())
                .AfterMap((s, d) =>
            {
                if (s.ClaimNumber == null)
                {
                    return;
                }
                d.ReferralSourceInfo.ReferralSourceId = s.ReferralSourceId;
                if (s.ReferralSource != null)
                {
                    d.ReferralSourceDataSource = new LookupItemVo
                    {
                        KeyId = s.ReferralSourceId,
                        DisplayName = s.ReferralSource.FirstName + " " + s.ReferralSource.MiddleName + " " + s.ReferralSource.LastName
                    };
                }
                else
                {
                    d.ReferralSourceDataSource = null;
                }
                d.AdjusterIsReferral = s.AdjusterIsReferral;
                d.ClaimNumberInfo.ClaimNumberId = s.ClaimantNumberId;
                if (s.ClaimNumber != null)
                {
                    d.ClaimNumberDataSource = new LookupItemVo
                    {
                        KeyId = s.ClaimantNumberId,
                        DisplayName = s.ClaimNumber.Name
                    };
                    d.ClaimNumberInfo.ClaimNumberInstruction = s.ClaimNumber.SpecialInstructions;
                    d.ClaimNumberInfo.ClaimNumberJurisdiction = s.ClaimNumber.State.AbbreviationName;
                    d.ClaimNumberInfo.Doi = s.ClaimNumber.Doi;
                    d.ClaimNumberInfo.StatusId = s.ClaimNumber.Status;
                }
                else
                {
                    d.ClaimNumberDataSource = null;
                }

                d.PayerInfo.PayerId = s.ClaimNumber.PayerId;
                if (s.ClaimNumber.Payer != null)
                {
                    d.PayerDataSource = new LookupItemVo
                    {
                        KeyId = s.ClaimNumber.PayerId.GetValueOrDefault(),
                        DisplayName = s.ClaimNumber.Payer.Name
                    };
                }
                else
                {
                    d.PayerDataSource = null;
                }

                d.BranchId = s.ClaimNumber.BranchId;
                if (s.ClaimNumber.Branch != null)
                {
                    d.BranchDataSource = new LookupItemVo
                    {
                        KeyId = s.ClaimNumber.BranchId.GetValueOrDefault(),
                        DisplayName = s.ClaimNumber.Branch.Name
                    };
                }
                else
                {
                    d.BranchDataSource = null;
                }

                d.AdjusterInfo.AdjusterId = s.ClaimNumber.AdjusterId;
                if (s.ClaimNumber.Adjuster != null)
                {
                    d.AdjusterDataSource = new LookupItemVo
                    {
                        KeyId = s.ClaimNumber.AdjusterId.GetValueOrDefault(),
                        DisplayName = s.ClaimNumber.Adjuster.FirstName + " " + s.ClaimNumber.Adjuster.MiddleName + " " + s.ClaimNumber.Adjuster.LastName
                    };
                    d.AdjusterInfo.AllowCreateReferral = s.ClaimNumber.Adjuster.AllowCreateReferral;
                }
                else
                {
                    d.AdjusterDataSource = null;
                }

                d.ClaimantInfo.ClaimantId = s.ClaimNumber.ClaimantId;
                if (s.ClaimNumber.Claimant != null)
                {
                    d.ClaimantDataSource = new LookupItemVo
                    {
                        KeyId = s.ClaimNumber.ClaimantId.GetValueOrDefault(),
                        DisplayName = s.ClaimNumber.Claimant.FirstName + " " + s.ClaimNumber.Claimant.MiddleName + " " + s.ClaimNumber.Claimant.LastName
                    };
                    
                }
                else
                {
                    d.ClaimantDataSource = null;
                }

                d.CaseManagerInfo.CaseManagerId = s.CaseManagerId;
                if (s.CaseManager != null)
                {
                    d.CaseManagerDataSource = new LookupItemVo
                    {
                        KeyId = s.CaseManager.Id,
                        DisplayName = s.CaseManager.Name
                    };
                }
                else
                {
                    d.CaseManagerDataSource = null;
                }

                d.EmployerInfo.EmployerId = s.ClaimNumber.EmployerId;
                if (s.ClaimNumber.Employer != null)
                {
                    d.EmployerDataSource = new LookupItemVo
                    {
                        KeyId = s.ClaimNumber.EmployerId.GetValueOrDefault(),
                        DisplayName = s.ClaimNumber.Employer.Name
                    };
                }
                else
                {
                    d.EmployerDataSource = null;
                }
                d.ProductTypeId = s.ProductTypeId;
                if (s.ProductType != null)
                {
                    d.ProductTypeDataSource = new LookupItemVo
                    {
                        KeyId = s.ProductTypeId,
                        DisplayName = s.ProductType.Name
                    };
                }
                else
                {
                    d.ProductTypeDataSource = null;
                }
                
                
            });

            Mapper.CreateMap<Framework.DomainModel.Entities.Referral, DashboardReferralInfoForReferralViewModel>()
                
                .ForMember(desc => desc.AssignToDataSource, opt => opt.Ignore())
                .ForMember(desc => desc.AttorneyDataSource, opt => opt.Ignore())
                .AfterMap((s, d) =>
                {
                    
                    if (s.AssignTo != null)
                    {
                        d.AssignToDataSource = new LookupItemVo
                        {
                            KeyId = s.AssignToId,
                            DisplayName = s.AssignTo.FirstName + " " + (string.IsNullOrEmpty(s.AssignTo.MiddleName)? "": s.AssignTo.MiddleName + " ") + s.AssignTo.LastName
                        };
                    }
                    else
                    {
                        d.AssignToDataSource = null;
                    }

                    if (s.Attorney != null)
                    {
                        d.AttorneyDataSource = new LookupItemVo
                        {
                            KeyId = s.AttorneyId.GetValueOrDefault(),
                            DisplayName = s.Attorney.FirstName+" "+s.Attorney.MiddleName+" "+s.Attorney.LastName
                        };
                    }
                    else
                    {
                        d.AttorneyDataSource = null;
                    }
                    d.AttorneyInfo.AttorneyId = s.AttorneyId;
                });

            Mapper.CreateMap<DashboardReferralShareViewModel, Framework.DomainModel.Entities.Referral>();

            Mapper.CreateMap<Framework.DomainModel.Entities.Referral, DashboardReferralDataViewModel>()
                .AfterMap((s, d) =>
                {
                    d.OldStatusId = s.StatusId;
                    d.SharedViewModel = s.MapTo<DashboardReferralShareViewModel>();
                });

            Mapper.CreateMap<DashboardReferralDataViewModel, Framework.DomainModel.Entities.Referral>()
                .AfterMap((s, d) =>
                {
                    d = s.SharedViewModel.MapPropertiesToInstance(d);
                    d.ControlNumber = s.ReferralInfo.ControlNumber;
                    d.Rush = s.ReferralInfo.Rush;
                    d.ReferralSourceId = s.ClaimInfo.ReferralSourceId.GetValueOrDefault();
                    d.ClaimantNumberId = s.ClaimInfo.ClaimNumberId.GetValueOrDefault();
                    d.AdjusterIsReferral = s.ClaimInfo.AdjusterIsReferral;
                    d.ProductTypeId = s.ClaimInfo.ProductTypeId.GetValueOrDefault();
                    d.EnteredDate = s.ReferralInfo.EnteredDate.GetValueOrDefault();
                    d.ReceivedDate = s.ReferralInfo.ReceivedDate.GetValueOrDefault();
                    d.DueDate = s.ReferralInfo.DueDate.GetValueOrDefault();
                    d.AssignToId = s.ReferralInfo.AssignToId.GetValueOrDefault();
                    d.StatusId = s.ReferralInfo.StatusId.GetValueOrDefault();
                    d.ReferralMethodId = s.ReferralInfo.ReferralMethodId;
                    d.SpecialInstruction = s.ReferralInfo.SpecialInstruction;
                    d.AttorneyId = s.ReferralInfo.AttorneyId;
                    d.IsCollectionSite = s.TestingInfo.CollectionMethodId==1;
                    d.NoMedicationHistory = s.NoMedicationHistory.GetValueOrDefault();
                    switch (s.ReferralInfo.StatusId)
                    {
                        case 5:
                            d.ReopenDate = DateTime.Now;
                            d.CompletedDate = null;
                            d.CancelDate = null;
                            d.PendingDate = null;
                            break;
                        case 4:
                            d.PendingDate = DateTime.Now;
                            d.CompletedDate = null;
                            d.CancelDate = null;
                            d.ReopenDate = null;
                            break;
                        case 3:
                            d.CompletedDate = DateTime.Now;
                            d.CancelDate = null;
                            d.PendingDate = null;
                            d.ReopenDate = null;
                            break;
                        case 2:
                            d.CancelDate = DateTime.Now;
                            d.CompletedDate = null;
                            d.PendingDate = null;
                            d.ReopenDate = null;
                            break;
                        default:
                            d.CompletedDate = null;
                            d.CancelDate = null;
                            d.PendingDate = null;
                            d.ReopenDate = null;
                            break;
                    }
                });
            Mapper.CreateMap<ReferralDetailVo, DashboardReferralDetailViewModel>()
                .AfterMap((s, d) =>
                {
                    var detailReferralInfo = new DashboardReferralDetailReferralInfoViewModel();

                    #region referral info
                    var referralInfo = new ReferralInfo
                    {
                        ReferralId = s.ReferralInfoId,
                        ControlNumber = s.ReferralInfoControlNumber,
                        ProductType = s.ReferralInfoProductType,
                        DateEntered = s.ReferralInfoDateEntered.ToString("g"),
                        DateReceived = s.ReferralInfoDateReceived.ToString("g"),
                        DueDate = s.ReferralInfoDueDate.ToString("g"),
                        StatusId = s.ReferralInfoStatusId,

                        AssignTo =
                            s.ReferralInfoAssignToFirstName + " " +
                            (string.IsNullOrEmpty(s.ReferralInfoAssignToMiddleName)
                                ? ""
                                : s.ReferralInfoAssignToMiddleName + " ") + s.ReferralInfoAssignToLastName,
                        ReferralMethodId = s.ReferralInfoReferralMethodId,
                        SpecialInstruction = s.ReferralInfoSpecialInstruction,
                        CreatedBy = s.ReferralInfoCreatedByFirstName + " " +(string.IsNullOrEmpty(s.ReferralInfoCreatedByMiddleName)? "": s.ReferralInfoCreatedByMiddleName + " ") + s.ReferralInfoCreatedByLastName,
                        CreatedDate = s.ReferralInfoCreatedDate.ToString("g"),
                    };
                    detailReferralInfo.ReferralInfo = referralInfo;
                    #endregion

                    #region ReferralSourceInfo

                    var referralSourceInfo = new ReferralSourceInfo
                    {
                        Name = s.ReferralSourceInfoFirstName+ " " +(string.IsNullOrEmpty(s.ReferralSourceInfoMiddleName)? "": s.ReferralSourceInfoMiddleName + " ") + s.ReferralSourceInfoLastName,
                        Type = s.ReferralSourceInfoType,
                        Company = s.ReferralSourceInfoCompany,
                        Phone = s.ReferralSourceInfoPhone,
                        Fax = s.ReferralSourceInfoFax,
                        Email = s.ReferralSourceInfoEmail,
                        State = s.ReferralSourceInfoState,
                        City = s.ReferralSourceInfoCity,
                        Zip = s.ReferralSourceInfoZip,
                        Address = s.ReferralSourceInfoAddress,
                        AuthorizationFrom = s.ReferralSourceInfoAuthorizationFrom,
                        AuthorizationTo = s.ReferralSourceInfoAuthorizationTo,
                        IsAuthorization = s.ReferralSourceInfoIsAuthorization,
                    };
                    detailReferralInfo.ReferralSourceInfo = referralSourceInfo;

                    #endregion

                    #region PayorInfo

                    var payorInfo = new PayorInfo
                    {
                        Payer = s.PayorInfoPayer,
                        Branch = s.PayorInfoBranch,
                        SpecialInstruction = s.PayorInfoSpecialInstruction,
                    };
                    detailReferralInfo.PayorInfo = payorInfo;

                    #endregion

                    #region AdjusterInfo

                    detailReferralInfo.AdjusterIsReferral = s.AdjusterIsReferral.GetValueOrDefault();
                    var adjusterInfo = new AdjusterInfo
                    {
                        Name =
                            s.AdjusterInfoFirstName + " " +
                            (string.IsNullOrEmpty(s.AdjusterInfoMiddleName) ? "" : s.AdjusterInfoMiddleName + " ") +
                            s.AdjusterInfoLastName,
                        Email = s.AdjusterInfoEmail,
                        Phone = s.AdjusterInfoPhone,
                        ExternalId = s.AdjusterInfoExternalId,
                        State = s.AdjusterInfoState,
                        City = s.AdjusterInfoCity,
                        Zip = s.AdjusterInfoZip,
                        Address = s.AdjusterInfoAddress
                    };
                    detailReferralInfo.AdjusterInfo = adjusterInfo;

                    #endregion

                    #region ClaimantInfo

                    var claimantInfo = new ClaimantInfo
                    {
                        Id = s.ClaimantInfoId,
                        Name = s.ClaimantInfoFirstName + " " +
                               (string.IsNullOrEmpty(s.ClaimantInfoMiddleName) ? "" : s.ClaimantInfoMiddleName + " ") +
                               s.ClaimantInfoLastName,
                        PayerPatientId = s.ClaimantInfoPayerPatientId,
                        HomePhone = s.ClaimantInfoHomePhone,
                        CellPhone = s.ClaimantInfoCellPhone,
                        Gender = s.ClaimantInfoGender,
                        DateOfBirth = s.ClaimantInfoDateOfBirth!= null? s.ClaimantInfoDateOfBirth.GetValueOrDefault().ToString("d"): "",
                        SsnId = s.ClaimantInfoSsnId,
                        ClaimantLanguage = s.ClaimantInfoClaimantLanguage,
                        Email = s.ClaimantInfoEmail,
                        Address1 = s.ClaimantInfoAddress1,
                        Address2 = s.ClaimantInfoAddress2,
                        State = s.ClaimantInfoState,
                        City = s.ClaimantInfoCity,
                        Zip = s.ClaimantInfoZip,
                        
                    };
                    detailReferralInfo.ClaimantInfo = claimantInfo;

                    #endregion

                    #region ClaimNumberInfo

                    var claimNumberInfo = new ClaimNumberInfo
                    {
                        ClaimNumber = s.ClaimNumberInfoClaimNumber,
                        Doi = s.ClaimNumberInfoDoi.ToString("d"),
                        ClaimJurisdiction = s.ClaimNumberInfoClaimJurisdiction,
                        SpecialInstruction = s.ClaimNumberInfoSpecialInstruction,
                        Status = s.ClaimNumberInfoStatus,
                        OpenDateTime = s.ClaimNumberInfoOpenDate,
                        CloseDateTime = s.ClaimNumberInfoCloseDate,
                        ReopenDateTime = s.ClaimNumberInfoReopenDate,
                        RecloseDateDateTime = s.ClaimNumberInfoRecloseDate,
                        
                    };
                    detailReferralInfo.ClaimNumberInfo = claimNumberInfo;

                    #endregion

                    #region CaseManagerInfo

                    var caseManagerInfo = new CaseManagerInfo
                    {
                        Name = s.CaseManagerInfoName,
                        Email = s.CaseManagerInfoEmail,
                        Phone = s.CaseManagerInfoPhone,
                        Fax = s.CaseManagerInfoFax,
                        Agency = s.CaseManagerInfoAgency,
                        SpecialInstruction = s.CaseManagerInfoSpecialInstruction,
                        State = s.CaseManagerInfoState,
                        City = s.CaseManagerInfoCity,
                        Zip = s.CaseManagerInfoZip,
                        Address = s.CaseManagerInfoAddress,
                    };
                    detailReferralInfo.CaseManagerInfo = caseManagerInfo;

                    #endregion

                    #region  AttorneyInfo

                    var attorneyInfo = new AttorneyInfo
                    {
                        Name =
                            s.AttorneyInfoFirstName + " " +
                            (string.IsNullOrEmpty(s.AttorneyInfoMiddleName) ? "" : s.AttorneyInfoMiddleName + " ") +
                            s.AttorneyInfoLastName,
                        Email = s.AttorneyInfoEmail,
                        Phone = s.AttorneyInfoPhone,
                        State = s.AttorneyInfoState,
                        City = s.AttorneyInfoCity,
                        Zip = s.AttorneyInfoZip,
                        Address = s.AttorneyInfoAddress,
                    };
                    detailReferralInfo.AttorneyInfo = attorneyInfo;

                    #endregion

                    #region EmployerInfo

                    var employerInfo = new EmployerInfo
                    {
                        Name = s.EmployerInfoName,
                        Phone = s.EmployerInfoPhone,
                        State = s.EmployerInfoState,
                        City = s.EmployerInfoCity,
                        Zip = s.EmployerInfoZip,
                        Address = s.EmployerInfoAddress,
                    };
                    detailReferralInfo.EmployerInfo = employerInfo;

                    #endregion

                    #region CollectionMethodInfo

                    var collectionMethodInfo = new CollectionMethodInfo();
                    
                    if (s.IsCollectionSite == true)
                    {
                        #region ReferralColectionSite
                        collectionMethodInfo = new CollectionMethodInfo
                        {
                            CollectionSiteName = s.ReferralColectionSite,
                            SpecialHandling = s.ReferralColectionSiteSpecialHandling,
                            Phone = s.ReferralColectionSitePhone,
                            Fax = s.ReferralColectionSiteFax,
                            Address = s.ReferralColectionSiteAddress,
                            Rush = (s.ReferralRush ?? false).ToString(),
                            CollectionSiteDate = s.ReferralColectionSiteCollectionDate != null? s.ReferralColectionSiteCollectionDate.GetValueOrDefault().ToString("g"): string.Empty,
                            NpiNumber = string.Empty,
                        };
                        #endregion
                    }
                    else
                    {
                         #region ReferralNpiNumber

                        if (s.ReferralNpiNumberNextMdVisit != null)
                        {
                            collectionMethodInfo = new CollectionMethodInfo
                            {
                                TreatingPhysician =
                                    s.ReferralNpiNumberTreatingPhysicianFirstName + " " +
                                    (string.IsNullOrEmpty(s.ReferralNpiNumberTreatingPhysicianMiddleName)
                                        ? ""
                                        : s.ReferralNpiNumberTreatingPhysicianMiddleName + " ") +
                                    s.ReferralNpiNumberTreatingPhysicianLastName,
                                NextMdVisit =
                                    s.ReferralNpiNumberNextMdVisit != null? s.ReferralNpiNumberNextMdVisit.GetValueOrDefault().ToString("g"): string.Empty,
                                NpiNumber = s.ReferralNpiNumberNpiNumber,
                                SpecialHandling = s.ReferralNpiNumberSpecialHandling,
                                Phone = s.ReferralNpiNumberPhone,
                                Fax = s.ReferralNpiNumberFax,
                                Email = s.ReferralNpiNumberEmail,
                                Address = s.ReferralNpiNumberAddress,
                            };
                        }

                        #endregion
                    }
                    collectionMethodInfo.IsCollectionSite = s.IsCollectionSite.GetValueOrDefault();
                    detailReferralInfo.CollectionMethodInfo = collectionMethodInfo;

                    #endregion

                    #region Param handle

                    detailReferralInfo.ReferralId = s.ReferralInfoId;
                    detailReferralInfo.PanelTypeId = s.ReferralInfoPanelTypeId.GetValueOrDefault();
                    #endregion

                    #region Main
                    d.Patient = s.ClaimantInfoFirstName + " " +
                                (string.IsNullOrEmpty(s.ClaimantInfoMiddleName) ? "" : s.ClaimantInfoMiddleName + " ") +
                                s.ClaimantInfoLastName;
                    d.ClaimantId = s.ClaimantInfoId;
                    d.ControlNumber = s.ReferralInfoControlNumber;
                    d.PanelTypeId = s.ReferralInfoPanelTypeId.GetValueOrDefault();
                    d.Id = s.ReferralInfoId;
                    
                    d.DashboardReferralDetailReferralInfoViewModel = detailReferralInfo;
                    

                    #endregion

                    #region DetailReferralTaskInfoViewModel

                    var detailReferralTaskInfoViewModel = new DetailReferralTaskInfoViewModel
                    {
                        ReferralDataSource = new LookupItemVo
                        {
                            KeyId = s.ReferralInfoId,
                            DisplayName = s.ReferralInfoId + "-" + s.ReferralInfoProductType
                        },
                        AssignToDataSource = new LookupItemVo
                        {
                            KeyId = s.ReferralInfoAssignToId,
                            DisplayName =
                                s.ReferralInfoAssignToFirstName + " " +
                                (string.IsNullOrEmpty(s.ReferralInfoAssignToMiddleName)
                                    ? ""
                                    : s.ReferralInfoAssignToMiddleName + " ") + s.ReferralInfoAssignToLastName
                        }
                    };
                    d.DetailReferralTaskInfoViewModel = detailReferralTaskInfoViewModel;

                    #endregion

                    #region RequisitionMappingModel

                    var requisitionMappingModel = new RequisitionMappingModel
                    {
                        ClaimantName = s.ClaimantInfoFirstName + (!string.IsNullOrEmpty(s.ClaimantInfoMiddleName)?" "+s.ClaimantInfoMiddleName:"") + " " + s.ClaimantInfoLastName,
                        ClaimantSsn = s.ClaimantInfoSsnId,
                        ClaimantAddress = s.ClaimantInfoAddress1,
                        ClaimantCity = s.ClaimantInfoCity,
                        ClaimantState = s.ClaimantInfoState,
                        ClaimantZip = s.ClaimantInfoZip,
                        ClaimantDobDate = s.ClaimantInfoDateOfBirth,
                        ClaimantHomePhone = s.ClaimantInfoHomePhone,
                        ClaimantGender = s.ClaimantInfoGender,
                        PayerName = s.PayorInfoPayer,
                        ClaimNumber = s.ClaimNumberInfoClaimNumber,
                        EmployerName = s.EmployerInfoName,
                        Jurisdiction = s.ClaimNumberInfoClaimJurisdiction,
                        DoiDate = s.ClaimNumberInfoDoi,
                        TreatingPhysicianName = (!string.IsNullOrEmpty(s.ReferralNpiNumberTreatingPhysicianOrganizationName)?s.ReferralNpiNumberTreatingPhysicianOrganizationName:(s.ReferralNpiNumberTreatingPhysicianFirstName+(!string.IsNullOrEmpty(s.ReferralNpiNumberTreatingPhysicianMiddleName)?" "+s.ReferralNpiNumberTreatingPhysicianMiddleName:"")+" "+s.ReferralNpiNumberTreatingPhysicianLastName)),
                        NextMdVisitDate=s.ReferralNpiNumberNextMdVisit
                    };
                    d.RequisitionMappingModel = requisitionMappingModel;

                    #endregion
                });
            Mapper.CreateMap<ReferralResultDetailVo, ReferralDetailExtension>().AfterMap((s, d) =>
            {
                if (string.IsNullOrEmpty(s.FileResult))
                    d.FileName = s.FileResult;
                else
                {
                    var pathWeb = Path.Combine(AppDomain.CurrentDomain.BaseDirectory, "FileUpload", "ResultPdf", s.FileResult);
                    d.FileName =  File.Exists(pathWeb) ? s.FileResult : string.Empty;
                }
            });
            #region Working with ReferralEmailTemplate
            Mapper.CreateMap<ReferralEmailTemplateDataViewModel, ReferralEmailTemplate>()
            .AfterMap((s, d) =>
            {
                d.ReferralId = s.ReferralId;
                d.EmailTemplateId = s.EmailTemplateId;
                d.Subject = s.EmailSubject;
                d.Content = s.EmailContent;
                d.EmailAddress = s.EmailAddressSetup;
                d.DateToSendMail = s.DateToSendMail;
                d.FaxNumber = s.FaxNumber;
                d.DateToSendFax = s.DateToSendFax;
                if (s.FileNames!= null && s.FileNames.Length > 0)
                {
                    foreach (var item in s.FileNames)
                    {
                        var fullFilePath = Path.Combine(Path.GetTempPath(), item.RowGuid + item.FileName);
                        byte[] savedFile = null;
                        if (File.Exists(fullFilePath))
                        {
                            savedFile = File.ReadAllBytes(fullFilePath);
                        }

                        d.ReferralEmailTemplateAttachments.Add(new ReferralEmailTemplateAttachment()
                        {
                            AttachedFileName = item.FileName,
                            RowGuid = Guid.Parse(item.RowGuid),
                            AttachedFileContent = savedFile,
                            AttachedFileSize = savedFile != null ? savedFile.Length : 0
                        });
                    }
                }
                
                
            })
            ;

            Mapper.CreateMap<ReferralEmailTemplate, ReferralEmailTemplateDataViewModel>()
                //.AfterMap((s, d) =>
                //{

                //});
                ;

            Mapper.CreateMap<ReferralEmailTemplate, ReferralEmailTemplateDetailViewModel>()
                .AfterMap((s, d) =>
                {
                    d.DateToSendEmail = s.DateToSendMail == null
                        ? ""
                        : ((DateTime) s.DateToSendMail).ToString("MM/dd/yyyy hh:mm tt");
                    d.DateToSendFax = s.DateToSendFax == null
                        ? ""
                        : ((DateTime)s.DateToSendFax).ToString("MM/dd/yyyy hh:mm tt");
                    d.EmailContent = s.Content;
                    d.EmailAddressSetup = s.EmailAddress ?? string.Empty;
                    d.EmailSubject = s.Subject;
                    d.FaxNumber = s.FaxNumber;
                    d.EmailTemplateId = s.EmailTemplateId;
                    d.ReferralId = s.ReferralId;
                    d.ListEmailAddressSetup = new List<EmailItemTitle>();
                });
                

            #endregion
        }
    }
}