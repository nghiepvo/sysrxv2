﻿using AutoMapper;
using Solr.DomainModel;

namespace WorkComp.Models.Mapping.SolrMapping
{
    public class SolrClaimantMappingProfile : Profile
    {
        protected override void Configure()
        {
            Mapper.CreateMap<Framework.DomainModel.Entities.Claimant, SolrClaimant>()
                    .ForMember(desc => desc.FullName, opt => opt.Ignore())
                  .ForMember(desc => desc.Address, opt => opt.Ignore())
                  .ForMember(desc => desc.Language, opt => opt.Ignore())
                  .ForMember(desc => desc.Gender, opt => opt.Ignore())
                  .ForMember(desc => desc.Dob, opt => opt.Ignore())
                  .AfterMap((s, d) =>
                  {
                      d.FullName = s.FirstName + " " + s.MiddleName + " " + s.LastName;
                      d.Address = s.Address1 + ",-" + s.Address2 + ", "
                                    + (s.State == null ? "" : s.State.Name) + ", " + (s.City == null ? "" : s.City.Name)
                                    + ", " + (s.Zip == null ? "" : s.Zip.Name);
                      d.Language = s.ClaimantLanguage == null ? "" : s.ClaimantLanguage.Name;
                      switch (s.Gender)
                      {
                          case "M":
                              d.Gender = "Male";
                              break;
                          case "F":
                              d.Gender = "Female";
                              break;
                          default:
                              d.Gender = "Unknown";
                              break;
                      }
                      d.Dob = s.Birthday;
                      d.IdClaimant = s.Id;
                  });
            Mapper.CreateMap<SolrClaimant, Framework.DomainModel.Entities.Claimant>();
        }
    }
}