﻿using System;
using AutoMapper;
using Framework.DomainModel.Entities.Common;
using Framework.Utility;
using Solr.DomainModel;

namespace WorkComp.Models.Mapping.SolrMapping
{
    public class SolrClaimNumberMappingProfile : Profile
    {
        protected override void Configure()
        {
            Mapper.CreateMap<Framework.DomainModel.Entities.ClaimNumber, SolrClaimNumber>()
                .AfterMap((s, d) =>
                {
                    d.ClaimNumberId = s.Id;
                    d.ClaimantName = s.Claimant == null ? "" : (s.Claimant.FirstName ?? "") + " " + (s.Claimant.MiddleName ?? "") + " " + (s.Claimant.LastName??"");
                    d.AdjusterName = s.Adjuster == null ? "" : (s.Adjuster.FirstName ?? "") + " " + (s.Adjuster.MiddleName ?? "") + " " + (s.Adjuster.LastName ?? "");
                    d.BranchId = s.BranchId.GetValueOrDefault();
                    d.BranchName = s.Branch == null ? "" : (s.Branch.Name ?? "");
                    d.PayerName = s.Payer == null ? "" : (s.Payer.Name ?? "");
                    d.Jurisdiction = s.State == null ? "" : (s.State.Name ?? "");
                    d.Status = XmlDataHelpper.Instance.GetValue(XmlDataTypeEnum.ClaimNumberStatus.ToString(),
                        s.Status.ToString());
                    d.ClaimantSsn=s.Claimant == null ? "" : (s.Claimant.Ssn ?? "");
                    d.ClaimantDob=s.Claimant == null ? (DateTime?)null : s.Claimant.Birthday;
                    d.SpecialInstructions = s.SpecialInstructions;
                });
            Mapper.CreateMap<SolrClaimNumber, Framework.DomainModel.Entities.ClaimNumber>();
        }
    }
}