﻿using AutoMapper;
using Solr.DomainModel;

namespace WorkComp.Models.Mapping.SolrMapping
{
    public class SolrNpiNumberMappingProfile : Profile
    {
        protected override void Configure()
        {
            Mapper.CreateMap<Framework.DomainModel.Entities.NpiNumber, SolrNpiNumber>()
                .ForMember(desc => desc.FullName, opt => opt.Ignore())
                .ForMember(desc => desc.Address, opt => opt.Ignore())
                .ForMember(desc => desc.NpiNumberId, opt => opt.Ignore())
                .ForMember(desc => desc.ProviderCredential, opt => opt.Ignore())
                 .ForMember(desc => desc.Organization, opt => opt.Ignore())
                .AfterMap((s, d) =>
                {
                    d.NpiNumberId = s.Id;
                    d.FullName = s.FirstName + " " + s.MiddleName + " " + s.LastName;
                    d.Address = s.Address + ", "
                                + (s.State == null ? "" : s.State.Name) + ", " + (s.City == null ? "" : s.City.Name)
                                + ", " + (s.Zip == null ? "" : s.Zip.Name);
                    d.ProviderCredential = s.ProviderCredentialText;
                    d.Organization = s.OrganizationName;
                });
            Mapper.CreateMap<SolrNpiNumber, Framework.DomainModel.Entities.NpiNumber>();
        }
    }
}