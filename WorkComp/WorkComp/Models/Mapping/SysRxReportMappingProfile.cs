﻿using System.Collections.Generic;
using System.Linq;
using AutoMapper;
using Framework.DomainModel.Entities.Common;
using Framework.DomainModel.ReportValueObject;
using Framework.Mapping;
using Framework.Utility;
using WorkComp.Models.SysRxReport;

namespace WorkComp.Models.Mapping
{
    public class SysRxReportMappingProfile : Profile
    {
        protected override void Configure()
        {
            #region MGMTUserProductivity Report
            Mapper.CreateMap<List<MGMTUserProductivityVo>, List<MGMTUserProductivityViewModel>>()
                .AfterMap((s, d) =>
                {
                    var lengthList = s.Count;
                    for (var i = 0; i < lengthList; i++)
                    {
                        d.Add(s[i].MapTo<MGMTUserProductivityViewModel>());
                    }

                });

            Mapper.CreateMap<MGMTUserProductivityVo, MGMTUserProductivityViewModel>();
            #endregion

            #region MGMTStatusSummary Report
            Mapper.CreateMap<List<MGMTStatusSummaryReportVo>, List<MGMTStatusSummaryViewModel>>()
                .AfterMap((s, d) =>
                {
                    var lengthList = s.Count;
                    for (var i = 0; i < lengthList; i++)
                    {
                        d.Add(s[i].MapTo<MGMTStatusSummaryViewModel>());
                    }

                });

            Mapper.CreateMap<MGMTStatusSummaryReportVo, MGMTStatusSummaryViewModel>();
            #endregion

            #region CancellationDetail Report
            Mapper.CreateMap<List<CancellationDetailVo>, List<CancellationDetailViewModel>>()
                .AfterMap((s, d) =>
                {
                    var lengthList = s.Count;
                    for (var i = 0; i < lengthList; i++)
                    {
                        d.Add(s[i].MapTo<CancellationDetailViewModel>());
                    }

                });

            Mapper.CreateMap<CancellationDetailVo, CancellationDetailViewModel>();
            #endregion

            #region TestResultSummary Report
            Mapper.CreateMap<List<TestResultsSummaryVo>, List<TestResultSummaryViewModel>>()
                .AfterMap((s, d) =>
                {
                    var lengthList = s.Count;
                    for (var i = 0; i < lengthList; i++)
                    {
                        d.Add(s[i].MapTo<TestResultSummaryViewModel>());
                    }

                });

            Mapper.CreateMap<TestResultsSummaryVo, TestResultSummaryViewModel>();
            #endregion

            #region UtilizationByInjuredWorker Report 
            Mapper.CreateMap<List<UtilizationByInjuredWorkerVo>, List<UtilizationByInjuredWorkerViewModel>>()
                .AfterMap((s, d) =>
                {
                    int? referralIdOld = 0;
                    var listVoTemp = new List<UtilizationByInjuredWorkerVo>();
                    var countVo = 0;
                    //Duyet list
                    var lengthList = s.Count;
                    for (var i = 0; i < lengthList; i++)
                    {
                        if (referralIdOld != s[i].ReferralId)
                        {
                            listVoTemp = new List<UtilizationByInjuredWorkerVo> {s[i]};
                            countVo = 1;
                        }
                        else
                        {
                            listVoTemp.Add(s[i]);
                            countVo ++;
                        }


                        if (i + 1 < lengthList && referralIdOld == s[i + 1].ReferralId)
                        {
                            continue;
                        }

                        var objVo = listVoTemp.First();
                        var objModel = objVo.MapTo<UtilizationByInjuredWorkerViewModel>();
                        objModel.TotalTestsCompletedDuringReportingTimeframe = countVo;
                        var countTemp = listVoTemp.Count(o => o.InConsistent > 0);
                        objModel.TotalInConsistent = countTemp > 0 ? (countTemp/countVo) : 0;
                        countTemp = listVoTemp.Count(o => o.InConsistent < 0 && o.Consistent > 0);
                        objModel.TotalConsistent = countTemp > 0 ? (countVo/countTemp) : 0;
                        d.Add(objModel);
                        
                        referralIdOld = s[i].ReferralId;
                    }

                });

            Mapper.CreateMap<UtilizationByInjuredWorkerVo, UtilizationByInjuredWorkerViewModel>().AfterMap((s, d) =>
            {
                d.ReportedDetected = s.ReportedDetected.ProtectYesAndEmpty();
                d.ReportedNotDetected = s.ReportedNotDetected.ProtectYesAndEmpty();
                d.NotReportedDetected = s.NotReportedDetected.ProtectYesAndEmpty();
                d.AlcoholDetected = s.AlcoholDetected.ProtectYesAndEmpty();
                d.IllicitsDetected = s.IllicitsDetected.ProtectYesAndEmpty();
                d.InvalidSample = s.InvalidSample.ProtectYesAndEmpty();
                d.NoMedication = s.NoMedication.ProtectYesAndEmpty();
                d.TestResult = d.InConsistent > 0 ? "INCONSISTENT" : (d.Consistent > 0 ? "CONSISTENT" : string.Empty);
            });
            #endregion

            #region TestResultDetail Report
            Mapper.CreateMap<List<TestResultsDetailVo>, List<TestResultDetailViewModel>>()
                .AfterMap((s, d) =>
                {
                    //Duyet list
                    var lengthList = s.Count;
                    for (var i = 0; i < lengthList; i++)
                    {
                        d.Add(s[i].MapTo<TestResultDetailViewModel>());
                    }

                });

            Mapper.CreateMap<TestResultsDetailVo, TestResultDetailViewModel>().AfterMap((s, d) =>
            {
                d.ReportedDetected = s.ReportedDetected.ProtectYesAndEmpty();
                d.ReportedNotDetected = s.ReportedNotDetected.ProtectYesAndEmpty();
                d.NotReportedDetected = s.NotReportedDetected.ProtectYesAndEmpty();
                d.AlcoholDetected = s.AlcoholDetected.ProtectYesAndEmpty();
                d.IllicitsDetected = s.IllicitsDetected.ProtectYesAndEmpty();
                d.InvalidSample = s.InvalidSample.ProtectYesAndEmpty();
                d.NoMedication = s.NoMedication.ProtectYesAndEmpty();
                d.TestResult = d.InConsistent > 0 ? "INCONSISTENT" : (d.Consistent > 0 ? "CONSISTENT" : string.Empty);
            });
            #endregion

            #region Common Report

            Mapper.CreateMap<SysRxReportQueryInfo, CommonSysRxReportViewModel>().AfterMap((s, d) =>
            {
                d.HasReport = (s.PayerId != null && s.DateFrom != null && s.DateTo != null && s.TotalRow > 0);
                d.DateFrom = s.DateFrom.GetValueOrDefault();
                d.DateTo = s.DateTo.GetValueOrDefault();
            });

            #endregion
        }
    }
}