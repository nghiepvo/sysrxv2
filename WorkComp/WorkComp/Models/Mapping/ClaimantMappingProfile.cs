﻿using AutoMapper;
using Framework.DomainModel.ValueObject;
using Framework.Mapping;
using Framework.Utility;
using WorkComp.Models.Claimant;


namespace WorkComp.Models.Mapping
{
    public class ClaimantMappingProfile : Profile
    {
        protected override void Configure()
        {
            Mapper.CreateMap<Framework.DomainModel.Entities.Claimant, DashboardClaimantShareViewModel>()
                .ForMember(desc => desc.StateDataSource, opt => opt.Ignore())
                .ForMember(desc => desc.CityDataSource, opt => opt.Ignore())
                .ForMember(desc => desc.ZipDataSource, opt => opt.Ignore())
                .ForMember(desc=>desc.BestContactNumberType, opt=>opt.Ignore())
                .AfterMap((s, d) =>
                {
                    if (s.ZipId != 0)
                    {
                        var objZip = s.Zip;
                        if (objZip!= null)
                        {
                            d.ZipDataSource = new LookupItemVo
                            {
                                KeyId = s.ZipId.GetValueOrDefault(),
                                DisplayName = objZip.Name
                            };
                            if (s.CityId != 0)
                            {
                                var objCity = s.City;
                                if (objCity != null)
                                {
                                    d.CityDataSource = new LookupItemVo
                                    {
                                        KeyId = s.CityId.GetValueOrDefault(),
                                        DisplayName = objCity.Name
                                    };
                                    var objState = objCity.State;
                                    if (objState != null)
                                    {
                                        d.StateId = objState.Id;
                                        d.StateDataSource = new LookupItemVo
                                        {
                                            KeyId = objState.Id,
                                            DisplayName = objState.Name
                                        };
                                    }
                                    else
                                    {
                                        d.StateDataSource = null;
                                    }
                                }
                                else
                                {
                                    d.CityDataSource = null;
                                    d.StateDataSource = null;
                                    d.ZipDataSource = null;
                                }
                            }
                            else
                            {
                                d.CityDataSource = null;
                                d.StateDataSource = null;
                                d.ZipDataSource = null;
                            }
                        }
                        else
                        {
                            d.CityDataSource = null;
                            d.StateDataSource = null;
                            d.ZipDataSource = null;
                        }
                        
                    }
                    else
                    {
                        d.CityDataSource = null;
                        d.StateDataSource = null;
                        d.ZipDataSource = null;
                    }

                    if (s.ClaimantLanguageId != null)
                    {
                        var objClaimantLanguage = s.ClaimantLanguage;
                        if (objClaimantLanguage != null)
                        {
                            d.ClaimantLanguageDataSource = new LookupItemVo
                            {
                                KeyId = (int)s.ClaimantLanguageId,
                                DisplayName = objClaimantLanguage.Name
                            };
                        }
                    }

                    if (string.IsNullOrEmpty(s.BestContactNumber))
                    {
                        d.BestContactNumberType = string.Empty;
                    }
                    else
                    {
                        if (!string.IsNullOrEmpty(s.WorkPhone) && s.BestContactNumber.Equals(s.WorkPhone))
                        {
                            d.BestContactNumberType = "work";
                        }
                        if (!string.IsNullOrEmpty(s.CellPhone) && s.BestContactNumber.Equals(s.CellPhone))
                        {
                            d.BestContactNumberType = "cell";
                        }
                        if (!string.IsNullOrEmpty(s.HomePhone) && s.BestContactNumber.Equals(s.HomePhone))
                        {
                            d.BestContactNumberType = "home";
                        }
                    }
                });
            Mapper.CreateMap<DashboardClaimantShareViewModel, Framework.DomainModel.Entities.Claimant>()
                .AfterMap((s, d) =>
                {
                    if (s.ZipId == null)
                        d.ZipId = 0;
                    if (s.CityId == null)
                        d.CityId = 0;
                    if (s.ZipId == null)
                        d.ZipId = 0;

                    d.CellPhone = s.CellPhone.RemoveFormatPhone();
                    d.HomePhone = s.HomePhone.RemoveFormatPhone();
                    d.WorkPhone = s.WorkPhone.RemoveFormatPhone();

                    switch (s.BestContactNumberType)
                    {
                        case "cell":
                            d.BestContactNumber = d.CellPhone;
                            break;
                        case "home":
                            d.BestContactNumber = d.HomePhone;
                            break;
                        case "work":
                            d.BestContactNumber = d.WorkPhone;
                            break;
                        default:
                            d.BestContactNumber =  null;
                            break;
                    }
                });

            Mapper.CreateMap<Framework.DomainModel.Entities.Claimant, DashboardClaimantDataViewModel>()
                 .AfterMap((s, d) =>
                 {
                     d.SharedViewModel = s.MapTo<DashboardClaimantShareViewModel>();
                 });

            Mapper.CreateMap<DashboardClaimantDataViewModel, Framework.DomainModel.Entities.Claimant>()
                
                .AfterMap((s, d) =>
                {
                    d = s.SharedViewModel.MapPropertiesToInstance(d);
                });

            Mapper.CreateMap<DashboardClaimainItemDetail, Framework.DomainModel.Entities.Claimant>();
            Mapper.CreateMap<Framework.DomainModel.Entities.Claimant, DashboardClaimainItemDetail>()
                .AfterMap((s, d) =>
                {
                    if (s.CityId != 0) d.State = s.State.Name;
                    if (s.ZipId != 0) d.Zip = s.Zip.Name;
                    if (s.CityId != 0) d.City = s.City.Name;
                    if (s.ClaimantLanguageId != null) d.ClaimantLanguage = s.ClaimantLanguage.Name;
                    d.Name = string.Format("{0} {1} {2}", s.FirstName, s.MiddleName, s.LastName);
                });

            Mapper.CreateMap<DashboardClaimainItemDetailDataViewModel, Framework.DomainModel.Entities.Claimant>()
                .AfterMap((s, d) =>
                {
                    d = s.SharedViewModel.MapPropertiesToInstance(d);
                });
            Mapper.CreateMap<Framework.DomainModel.Entities.Claimant, DashboardClaimainItemDetailDataViewModel>()
                 .AfterMap((s, d) =>
                 {
                     d.SharedViewModel = s.MapTo<DashboardClaimainItemDetail>();
                 });
        }
    }
}