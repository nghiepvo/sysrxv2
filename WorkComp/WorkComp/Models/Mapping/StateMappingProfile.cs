﻿using AutoMapper;
using Framework.Mapping;
using WorkComp.Models.State;

namespace WorkComp.Models.Mapping
{
    public class StateMappingProfile : Profile
    {
        protected override void Configure()
        {
            Mapper.CreateMap<Framework.DomainModel.Entities.State, DashboardStateShareViewModel>()
                .ForMember(desc => desc.ShowPopupType,opt=>opt.Ignore());
            Mapper.CreateMap<DashboardStateShareViewModel, Framework.DomainModel.Entities.State>();

            Mapper.CreateMap<Framework.DomainModel.Entities.State, DashboardStateDataViewModel>()
                 .AfterMap((s, d) =>
                 {
                     d.SharedViewModel = s.MapTo<DashboardStateShareViewModel>();
                 });

            Mapper.CreateMap<DashboardStateDataViewModel, Framework.DomainModel.Entities.State>()
                .AfterMap((s, d) =>
                {
                    d = s.SharedViewModel.MapPropertiesToInstance(d);
                });
        }
    }
}