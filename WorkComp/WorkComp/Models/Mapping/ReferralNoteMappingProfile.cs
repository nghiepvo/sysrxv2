﻿using AutoMapper;
using Framework.DomainModel.ValueObject;
using Framework.Mapping;
using WorkComp.Models.ReferralNote;

namespace WorkComp.Models.Mapping
{
    public class ReferralNoteMappingProfile : Profile
    {
        protected override void Configure()
        {
            Mapper.CreateMap<Framework.DomainModel.Entities.ReferralNote, DashboardReferralNoteShareViewModel>()
                .ForMember(desc => desc.ReferralDataSource, opt => opt.Ignore())
                .ForMember(desc => desc.NoteTypeDataSource, opt => opt.Ignore())
                .ForMember(desc => desc.AssignToDataSource, opt => opt.Ignore())
                .AfterMap((s, d) =>
                {
                    if (s.ReferralId != 0)
                    {
                        d.ReferralDataSource = new LookupItemVo
                        {
                            KeyId = s.ReferralId,
                            DisplayName = s.Referral.ControlNumber
                        };
                    }
                    else
                    {
                        d.ReferralDataSource = null;
                    }

                    if (s.AssignToId != 0)
                    {
                        d.AssignToDataSource = new LookupItemVo
                        {
                            KeyId = s.AssignToId,
                            DisplayName = s.User.UserName
                        };
                    }
                    else
                    {
                        d.AssignToDataSource = null;
                    }
                });
            Mapper.CreateMap<DashboardReferralNoteShareViewModel, Framework.DomainModel.Entities.ReferralNote>();

            Mapper.CreateMap<Framework.DomainModel.Entities.ReferralNote, DashboardReferralNoteDataViewModel>()
                .AfterMap((s, d) =>
                {
                    d.SharedViewModel = s.MapTo<DashboardReferralNoteShareViewModel>();
                });

            Mapper.CreateMap<DashboardReferralNoteDataViewModel, Framework.DomainModel.Entities.ReferralNote>()
                .AfterMap((s, d) =>
                {
                    d = s.SharedViewModel.MapPropertiesToInstance(d);
                });
        }
    }
}