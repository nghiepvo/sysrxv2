﻿using AutoMapper;
using Framework.DomainModel.ValueObject;
using Framework.Mapping;
using WorkComp.Models.City;

namespace WorkComp.Models.Mapping
{
    public class CityMappingProfile : Profile
    {
        protected override void Configure()
        {
            Mapper.CreateMap<Framework.DomainModel.Entities.City, DashboardCityShareViewModel>()
                .ForMember(desc=>desc.StateDataSource,opt=>opt.Ignore())
                .AfterMap((s, d) =>
                {
                    if (s.StateId != 0)
                    {
                        d.StateDataSource=new LookupItemVo
                        {
                            KeyId = s.StateId,
                            DisplayName = s.State.Name
                        };
                    }
                    else
                    {
                        d.StateDataSource = null;
                    }
                });
            Mapper.CreateMap<DashboardCityShareViewModel, Framework.DomainModel.Entities.City>()
                .AfterMap((s, d) =>
                {
                    d.StateId = s.StateId ?? 0;
                });

            Mapper.CreateMap<Framework.DomainModel.Entities.City, DashboardCityDataViewModel>()
                .AfterMap((s, d) =>
                {
                    d.SharedViewModel = s.MapTo<DashboardCityShareViewModel>();
                });

            Mapper.CreateMap<DashboardCityDataViewModel, Framework.DomainModel.Entities.City>()
                .AfterMap((s, d) =>
                {
                    d = s.SharedViewModel.MapPropertiesToInstance(d);
                });

        }
    }
}