﻿using System;
using System.Collections.Generic;
using AutoMapper;
using Framework.DomainModel.ValueObject;
using Framework.Mapping;
using WorkComp.Models.ReferralTask;

namespace WorkComp.Models.Mapping
{
    public class ReferralTaskMappingProfile : Profile
    {
        protected override void Configure()
        {
            Mapper.CreateMap<Framework.DomainModel.Entities.ReferralTask, DashboardReferralTaskShareViewModel>()
                .ForMember(desc => desc.ReferralDataSource, opt => opt.Ignore())
                .ForMember(desc => desc.AssignToDataSource, opt => opt.Ignore())
                .ForMember(desc => desc.StatusDataSource, opt => opt.Ignore())
                .AfterMap((s, d) =>
                {
                    if (s.ReferralId != 0)
                    {
                        d.ReferralDataSource = new LookupItemVo
                        {
                            KeyId = s.ReferralId,
                            DisplayName = s.Referral.Id + "-" + (s.Referral.ProductType == null ? "" : s.Referral.ProductType.Name)
                        };
                        
                    }
                    else
                    {
                        d.ReferralDataSource = null;
                    }

                    if (s.AssignToId != 0)
                    {
                        d.AssignToDataSource = new LookupItemVo
                        {
                            KeyId = s.AssignToId.GetValueOrDefault(),
                            DisplayName = s.AssignTo.FirstName + " " + (string.IsNullOrEmpty(s.AssignTo.MiddleName) ? "" : s.AssignTo.MiddleName + " ") + s.AssignTo.LastName
                        };
                    }
                    else
                    {
                        d.AssignToDataSource = null;
                    }
                });
            Mapper.CreateMap<DashboardReferralTaskShareViewModel, Framework.DomainModel.Entities.ReferralTask>().AfterMap(
                (s, d) =>
                {
                    switch (s.StatusId)
                    {
                        case 3:
                            d.CompletedDate = DateTime.Now;
                            d.CancelDate = null;
                            break;
                        case 2:
                            d.CancelDate = DateTime.Now;
                            d.CompletedDate = null;
                            break;
                        default:
                            d.CompletedDate = null;
                            d.CancelDate = null;
                            break;
                    }
                });

            Mapper.CreateMap<Framework.DomainModel.Entities.ReferralTask, DashboardReferralTaskDataViewModel>()
                .AfterMap((s, d) =>
                {
                    d.SharedViewModel = s.MapTo<DashboardReferralTaskShareViewModel>();
                });

            Mapper.CreateMap<DashboardReferralTaskDataViewModel, Framework.DomainModel.Entities.ReferralTask>()
                .AfterMap((s, d) =>
                {
                    d = s.SharedViewModel.MapPropertiesToInstance(d);
                
                });

            Mapper.CreateMap<ReferralTaskItemCreate, Framework.DomainModel.Entities.ReferralTask>();
            Mapper.CreateMap<DashboardReferralTaskCreateViewModel, List<Framework.DomainModel.Entities.ReferralTask>>()
                .AfterMap((s, d) =>
            {
                foreach (var item in s.ReferralTaskList)
                {
                    var dataItem = item.MapTo<Framework.DomainModel.Entities.ReferralTask>();
                    dataItem.ReferralId = s.ReferralId.GetValueOrDefault();
                    d.Add(dataItem);
                }
            });
            
            Mapper.CreateMap<Framework.DomainModel.Entities.ReferralTask, DashboardReferralTaskDetailViewModel>()
                .AfterMap((s, d) =>
                {
                    var objReferral = s.Referral;
                    if (objReferral != null)
                    {
                        d.ReferralId = objReferral.Id;
                        if (objReferral.ClaimNumber!= null && objReferral.ClaimNumber.Claimant != null)
                        {
                            var objClaimant = objReferral.ClaimNumber.Claimant;
                            d.ClaimantId = objClaimant.Id;
                            d.Patient = objClaimant.FirstName + " " +
                                        (string.IsNullOrEmpty(objClaimant.MiddleName)
                                            ? ""
                                            : objClaimant.MiddleName + " ") + objClaimant.LastName;

                        }
                    }
                    else
                    {
                        d.ReferralId = 0;
                    }
                    

                    var objAssign = s.AssignTo;
                    if (objReferral != null)
                    {
                        d.AssignTo = objAssign.FirstName + " " +
                                     (string.IsNullOrEmpty(objAssign.MiddleName) ? "" : objAssign.MiddleName + " ") +
                                     objAssign.LastName;
                    }
                    else
                    {
                        d.AssignTo = "";
                    }

                    d.CreatedDate = s.CreatedOn;
                });
        }
    }
}