﻿using System.Web.Routing;

namespace WorkComp.Models.Input
{
    public class AreaTextViewModel : ControlSharedViewModelBase
    {
        public string Class { get; set; }
        public string Style { get; set; }
        public bool Required { get; set; }
        public string PlaceHolderText { get; set; }
        public string RequiredAttribute
        {
            get
            {
                return Required ? "required=\"required\"" : "";
            }
        }

        public int Cols { get; set; }
        public int Rows { get; set; }
        public double WidthPercentLable { get; set; }
        public double WidthPercentField { get; set; }
        public RouteValueDictionary HtmlAttributes { get; set; }
    }
}