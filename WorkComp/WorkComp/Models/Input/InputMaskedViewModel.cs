﻿using System.Web.Routing;
namespace WorkComp.Models.Input
{
    public class InputMaskedViewModel : ControlSharedViewModelBase
    {
        public string Format { get; set; }
        public RouteValueDictionary HtmlAttributes { get; set; }
        public bool Required { get; set; }
        public string RequiredAttribute
        {
            get
            {
                return Required ? "required=\"required\"" : "";
            }
        }
    }
}