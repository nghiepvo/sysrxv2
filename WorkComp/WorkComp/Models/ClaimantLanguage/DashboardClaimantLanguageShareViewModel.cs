﻿namespace WorkComp.Models.ClaimantLanguage
{
    public class DashboardClaimantLanguageShareViewModel : DashboardSharedViewModel
    {
        public string Name { get; set; }
    }
}