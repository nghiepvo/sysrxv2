﻿using Framework.Service.Translation;

namespace WorkComp.Models
{
    public class MessageModelBase
    {
        public string GetMessage(string key)
        {
            return SystemMessageLookup.GetMessage("RequiredTextResourceKey");
        }
    }
}