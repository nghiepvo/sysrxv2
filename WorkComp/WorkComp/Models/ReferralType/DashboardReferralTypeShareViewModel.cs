﻿namespace WorkComp.Models.ReferralType
{
    public class DashboardReferralTypeShareViewModel : DashboardSharedViewModel
    {
        public string Name { get; set; }
    }
}