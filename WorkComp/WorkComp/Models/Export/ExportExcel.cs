﻿using System.Collections.Generic;

namespace WorkComp.Models.Export
{
    public class ExportExcel
    {
        public GridConfigViewModel GridConfigViewModel { get; set; }
        public List<dynamic> ListDataSource { get; set; }
    }
}