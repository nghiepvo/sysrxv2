﻿namespace WorkComp.Models.ReasonReferral
{
    public class DashboardReasonReferralIndexViewModel : DashboardGridViewModelBase<Framework.DomainModel.Entities.ReasonReferral>
    {
        public override string PageTitle
        {
            get
            {
                return "Referral Reasons";
            }
        }
    }
}