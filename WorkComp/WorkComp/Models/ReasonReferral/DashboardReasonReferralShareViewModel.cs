﻿using System.Collections;
using System.Collections.Generic;

namespace WorkComp.Models.ReasonReferral
{
    public class DashboardReasonReferralShareViewModel : DashboardSharedViewModel
    {
        public string Name { get; set; }
        public int? ReportsTo { get; set; }
        public bool? HasChild { get; set; }
    }
}