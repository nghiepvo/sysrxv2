﻿namespace WorkComp.Models.Drug
{
    public class DashboardDrugShareViewModel : DashboardSharedViewModel
    {
        public string Name { get; set; }
        public string Class { get; set; }
        public string Description { get; set; }
    }
}