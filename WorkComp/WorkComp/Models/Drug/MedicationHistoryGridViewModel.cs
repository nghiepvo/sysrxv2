﻿using System;
using System.Collections.ObjectModel;
using Framework.DomainModel.Entities.Common;
using Framework.DomainModel.ValueObject;

namespace WorkComp.Models.Drug
{
    public class MedicationHistoryGridViewModel : EditableGridViewModel
    {
        public MedicationHistoryGridViewModel()
        {
            DocumentTypeId = (int)DocumentTypeKey.Drug;
        }

        public Collection<MedicationHistoryGridItemViewModel> MedicationHistories { get; set; }
        public int ReferralId { get; set; }

    }

    public class MedicationHistoryGridItemViewModel
    {
        public Guid RowGuid { get; set; }
        public int Id { get; set; }
        public int? ReferralId { get; set; }
        public LookupInGridDataSourceViewModel Drug { get; set; }
        public int? DaysSupply { get; set; }
        public string Dosage { get; set; }
        public LookupInGridDataSourceViewModel DosageUnit { get; set; }
        public LookupInGridDataSourceViewModel ProvidedBy { get; set; }
        public DateTime? FillDate { get; set; }
        public string DrugClass { get; set; }
    }
}