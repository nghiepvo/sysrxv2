﻿namespace WorkComp.Models.Drug
{
    public class DashboardDrugDataViewModel : MasterfileViewModelBase<Framework.DomainModel.Entities.Drug>
    {
        public override void MapFromClientParameters(MasterfileParameter parameters)
        {
            SharedViewModel = MapFromClientParameters<DashboardDrugShareViewModel>(parameters);
        }
    }
}