﻿namespace WorkComp.Models.Heading
{
    public class DashboardHeadingDataViewModel : MasterfileViewModelBase<Framework.DomainModel.Entities.Heading>
    {
        public override void MapFromClientParameters(MasterfileParameter parameters)
        {
            SharedViewModel = MapFromClientParameters<DashboardHeadingShareViewModel>(parameters);
        }
    }
}