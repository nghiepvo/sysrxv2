﻿namespace WorkComp.Models.CaseManager
{
    public class DashboardCaseManagerIndexViewModel : DashboardGridViewModelBase<Framework.DomainModel.Entities.CaseManager>
    {
        public override string PageTitle
        {
            get
            {
                return "Case Manager";
            }
        }
    }
    
}