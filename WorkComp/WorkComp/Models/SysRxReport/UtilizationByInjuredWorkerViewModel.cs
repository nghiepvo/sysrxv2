﻿using System;

namespace WorkComp.Models.SysRxReport
{
    public class UtilizationByInjuredWorkerViewModel
    {
        public int? ReferralId { get; set; }
        public string ClaimNumber { get; set; }
        public string PayerName { get; set; }
        public string ClaimantFirstName { get; set; }
        public string ClaimantLastName { get; set; }
        public string AdjusterName { get; set; }
        public string ReferralSourceName { get; set; }
        public string EmployerName { get; set; }
        public string EmployerLocation { get; set; }
        public string Jurisdiction { get; set; }
        public DateTime? InjuryDate { get; set; }
        public string Icd910 { get; set; }
        public string TreatingPhysician { get; set; }
        public DateTime? LastCompletedTestDate { get; set; }
        public int Consistent { get; set; }
        public int InConsistent { get; set; }
        public string TestingSchedule { get; set; }

        public string ReportedDetected { get; set; }
        public string ReportedNotDetected { get; set; }
        public string NotReportedDetected { get; set; }
        public string AlcoholDetected { get; set; }
        public string IllicitsDetected { get; set; }
        public string InvalidSample { get; set; }
        public string NoMedication { get; set; }
        public string TestResult { get; set; }
        

        public int TotalTestsCompletedDuringReportingTimeframe { get; set; }
        public float TotalConsistent { get; set; }
        public float TotalInConsistent { get; set; }
        
    }
}