﻿using System;

namespace WorkComp.Models.SysRxReport
{
    public class MGMTUserProductivityViewModel
    {
        public string PayerName { get; set; }
        public string PayerAddress { get; set; }
        public string AssignedUser { get; set; }
        public string ProductTypeName { get; set; }
        public int? New { get; set; }
        public int? Open { get; set; }
        public int? Completed { get; set; }
        public int? Cancelled { get; set; }
        public int? Created { get; set; }
        public int? Reopen { get; set; }
        public int? AppointmentsScheduled { get; set; }
        public DateTime? DateFrom { get; set; }
        public DateTime? DateTo { get; set; }
        public int? TotalPage { get; set; }
        public int? TotalRow { get; set; }
    }
}