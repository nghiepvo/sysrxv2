﻿using System;

namespace WorkComp.Models.SysRxReport
{
    public class CancellationDetailViewModel
    {
        public int? ClaimantId { get; set; }
        public int? ReferralId { get; set; }
        public string PayerName { get; set; }
        public string PayerAddress { get; set; }
        public string AdjusterName { get; set; }
        public string ReferralSourceName { get; set; }
        public string EmployerName { get; set; }
        public string EmployerAddress { get; set; }
        public string ClaimantFirstName { get; set; }
        public string ClaimantLastName { get; set; }
        public string ClaimNumber { get; set; }
        public int? ClaimNumberId { get; set; }
        public string Jurisdiction { get; set; }
        public string Doi { get; set; }
        public DateTime? DateRequestReceived { get; set; }
        public string Product { get; set; }
        public string PanelRequested { get; set; }
        public string RequestReason { get; set; }
        public string Criteria { get; set; }
        public string TreatingPhysicianNpi { get; set; }
        public string TreatingPhysicianName { get; set; }
        public string TreatingPhysicianAddress { get; set; }
        public string TreatingPhysicianCity { get; set; }
        public string TreatingPhysicianState { get; set; }
        public string TreatingPhysicianZip { get; set; }
        public string TreatingPhysicianPhone { get; set; }
        public DateTime? CancelDate { get; set; }
        public string CancelReason { get; set; }
        public DateTime? DateFrom { get; set; }
        public DateTime? DateTo { get; set; }
        public int? TotalPage { get; set; }
        public int? TotalRow { get; set; }
    }
}