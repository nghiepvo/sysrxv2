﻿using System;
using System.Configuration;

namespace WorkComp.Models.SysRxReport
{
    public class CommonSysRxReportViewModel
    {
        public string UrlLogo
        {
            get { return ConfigurationManager.AppSettings["Url"] + "Content/images/logo.png"; }
        }

        public string Title { get; set; }
        public string PayerName { get; set; }
        public DateTime DateFrom { get; set; }
        public DateTime DateTo { get; set; }

        public DateTime CurentDateReport
        {
            get { return DateTime.Now; }
        }

        public bool HasReport { get; set; }
    }
}