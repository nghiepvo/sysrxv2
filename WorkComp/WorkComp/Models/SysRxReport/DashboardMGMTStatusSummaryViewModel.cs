﻿using System.Collections.Generic;

namespace WorkComp.Models.SysRxReport
{
    public class DashboardMGMTStatusSummaryViewModel
    {
        public DashboardMGMTStatusSummaryViewModel()
        {
            MGMTStatusSummaryViewModels = new List<MGMTStatusSummaryViewModel>();
            CommonSysRxReportViewModel = new CommonSysRxReportViewModel();
        }
        public CommonSysRxReportViewModel CommonSysRxReportViewModel { get; set; }
        public List<MGMTStatusSummaryViewModel> MGMTStatusSummaryViewModels { get; set; }
    }
}