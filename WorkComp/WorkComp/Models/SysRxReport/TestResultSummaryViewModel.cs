﻿using System;

namespace WorkComp.Models.SysRxReport
{
    public class TestResultSummaryViewModel
    {
        public string PayerName { get; set; }
        public string PayerAddress { get; set; }
        public double? ConsistentCompletedTests { get; set; }
        public double? InConsistentCompletedTests { get; set; }
        public double? TotalCompletedTests { get; set; }
        public double? ConsistentResultsRatio { get; set; }
        public double? InConsistentResultsRatio { get; set; }
        public double? NumReportedPrescriptionMedNotDetected { get; set; }
        public double? ReportedPrescriptionMedNotDetected { get; set; }
        public double? NumNonReportedPrescriptionMedsDetected { get; set; }
        public double? NonReportedPrescriptionMedsDetected { get; set; }
        public double? NumWithAlcoholPresent { get; set; }
        public double? WithAlcoholPresent { get; set; }
        public double? NumWithIllicitDrugsPresent { get; set; }
        public double? WithIllicitDrugsPresent { get; set; }
        public double? NumWithInvalidSample { get; set; }
        public double? WithInvalidSample { get; set; }
        public double? NumNoMedicationHistoryProvided { get; set; }
        public double? NoMedicationHistoryProvided { get; set; }
        public double? NumConsistentTestsPerClaimantDoiSmall3Month { get; set; }
        public double? ConsistentTestsPerClaimantDoiSmall3Month { get; set; }
        public double? NumInConsistentTestsPerClaimantDoiSmall3Month { get; set; }
        public double? InConsistentTestsPerClaimantDoiSmall3Month { get; set; }
        public double? NumConsistentTestsPerClaimantDoiLarge3MonthAndSmall6Month { get; set; }
        public double? ConsistentTestsPerClaimantDoiLarge3MonthAndSmall6Month { get; set; }
        public double? NumInConsistentTestsPerClaimantDoiLarge3MonthAndSmall6Month { get; set; }
        public double? InConsistentTestsPerClaimantDoiLarge3MonthAndSmall6Month { get; set; }
        public double? NumConsistentTestsPerClaimantDoiLarge6MonthAndSmall12Month { get; set; }
        public double? ConsistentTestsPerClaimantDoiLarge6MonthAndSmall12Month { get; set; }
        public double? NumInConsistentTestsPerClaimantDoiLarge6MonthAndSmall12Month { get; set; }
        public double? InConsistentTestsPerClaimantDoiLarge6MonthAndSmall12Month { get; set; }
        public double? NumConsistentTestsPerClaimantDoiLarge12Month { get; set; }
        public double? ConsistentTestsPerClaimantDoiLarge12Month { get; set; }
        public double? NumInConsistentTestsPerClaimantDoiLarge12Month { get; set; }
        public double? InConsistentTestsPerClaimantDoiLarge12Month { get; set; }
        public double? NumConsistentUrine { get; set; }
        public double? ConsistentUrine { get; set; }
        public double? NumInConsistentUrine { get; set; }
        public double? InConsistentUrine { get; set; }
        public double? NumConsistentOral { get; set; }
        public double? ConsistentOral { get; set; }
        public double? NumInConsistentOral { get; set; }
        public double? InConsistentOral { get; set; }
        public double? NumConsistentBlood { get; set; }
        public double? ConsistentBlood { get; set; }
        public double? NumInConsistentBlood { get; set; }
        public double? InConsistentBlood { get; set; }
        public double? NumConsistentHair { get; set; }
        public double? ConsistentHair { get; set; }
        public double? NumInConsistentHair { get; set; }
        public double? InConsistentHair { get; set; }
        public DateTime? DateFrom { get; set; }
        public DateTime? DateTo { get; set; }
        public double? TotalPage { get; set; }
        public double? TotalRow { get; set; }
    }
}