﻿using System.Collections.Generic;

namespace WorkComp.Models.SysRxReport
{
    public class DashboardTestResultDetailViewModel 
    {
        public DashboardTestResultDetailViewModel()
        {
            TestResultDetailViewModels = new List<TestResultDetailViewModel>();
            CommonSysRxReportViewModel = new CommonSysRxReportViewModel();
        }
        public CommonSysRxReportViewModel CommonSysRxReportViewModel { get; set; }
        public List<TestResultDetailViewModel> TestResultDetailViewModels { get; set; }
    }
}