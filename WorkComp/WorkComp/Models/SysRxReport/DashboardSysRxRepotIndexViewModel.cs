﻿using Framework.DomainModel.ValueObject;

namespace WorkComp.Models.SysRxReport
{
    public class DashboardSysRxRepotIndexViewModel : ViewModelBase
    {
        public DashboardSysRxRepotIndexViewModel()
        {
            //Chỗ này lấy dự liệu từ database default id = 1
            DefaulPayerDataSource = new LookupItemVo { KeyId = 1, DisplayName = "Broadspire" };
        }

        public override string PageTitle
        {
            get { return "SYSRX REPORTS"; }
        }

        public LookupItemVo DefaulPayerDataSource { get; set; }
    }
    
}