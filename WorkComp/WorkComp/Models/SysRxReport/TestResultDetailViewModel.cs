﻿using System;

namespace WorkComp.Models.SysRxReport
{
    public class TestResultDetailViewModel
    {
        public int? ReferralId { get; set; }
        public string PayerName { get; set; }
        public string ClaimNumber { get; set; }
        public string ClaimantFirstName { get; set; }
        public string ClaimantLastName { get; set; }
        public DateTime? ClaimantDob { get; set; }
        public string ClaimantState { get; set; }
        public string AdjusterName { get; set; }
        public string ReferralSourceName { get; set; }
        public string EmployerName { get; set; }
        public string EmployerLocation { get; set; }
        public string Jurisdiction { get; set; }
        public DateTime? InjuryDate { get; set; }
        public string Icd910 { get; set; }
        public DateTime? DateRequestReceived { get; set; }
        public string Product { get; set; }
        public string SampleType { get; set; }
        public string PanelRequested { get; set; }
        public string RequestReason { get; set; }
        public string Criteria { get; set; }
        public string Accession { get; set; }
        public DateTime? DateCollected { get; set; }
        public string CollectionSite { get; set; }
        public string CollectionSiteLocation { get; set; }
        public string TreatingPhysicianNpi { get; set; }
        public string TreatingPhysicianName { get; set; }
        public string TreatingPhysicianAddress { get; set; }
        public string TreatingPhysicianState { get; set; }
        public string TreatingPhysicianCity { get; set; }
        public string TreatingPhysicianZip { get; set; }
        public string TreatingPhysicianPhone { get; set; }
        public string ReviewStatusStr { get; set; }

        public DateTime? SpecimenReceivedDate { get; set; }
        public DateTime? CompletedTestDate { get; set; }
        public DateTime? CompletedDate { get; set; }
        public string PrescribedDrugs { get; set; }

        public int Consistent { get; set; }
        public int InConsistent { get; set; }

        public string ReportedDetected { get; set; }
        public string ReportedNotDetected { get; set; }
        public string NotReportedDetected { get; set; }
        public string AlcoholDetected { get; set; }
        public string IllicitsDetected { get; set; }
        public string InvalidSample { get; set; }
        public string NoMedication { get; set; }
        public string TestResult { get; set; }
        public string TestingSchedule { get; set; }
        
        
    }
}