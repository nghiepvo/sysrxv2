﻿using System.Collections.Generic;

namespace WorkComp.Models.SysRxReport
{
    public class DashboardTestResultSummaryViewModel
    {
        public DashboardTestResultSummaryViewModel()
        {
            TestResultSummaryViewModels = new List<TestResultSummaryViewModel>();
            CommonSysRxReportViewModel = new CommonSysRxReportViewModel();
        }
        public CommonSysRxReportViewModel CommonSysRxReportViewModel { get; set; }
        public List<TestResultSummaryViewModel> TestResultSummaryViewModels { get; set; }
    }
}