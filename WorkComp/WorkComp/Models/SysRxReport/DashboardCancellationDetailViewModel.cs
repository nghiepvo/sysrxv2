﻿using System.Collections.Generic;

namespace WorkComp.Models.SysRxReport
{
    public class DashboardCancellationDetailViewModel
    {
        public DashboardCancellationDetailViewModel()
        {
            CancellationDetailViewModels = new List<CancellationDetailViewModel>();
            CommonSysRxReportViewModel = new CommonSysRxReportViewModel();
        }
        public CommonSysRxReportViewModel CommonSysRxReportViewModel { get; set; }
        public List<CancellationDetailViewModel> CancellationDetailViewModels { get; set; }
    }
}