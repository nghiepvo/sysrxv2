﻿using Framework.DomainModel.Entities.Common;
using Framework.Utility;

namespace WorkComp.Models.Configuration
{
    public class DashboardConfigurationShareViewModel : DashboardSharedViewModel
    {
        public int TypeId { get; set; }

        public string TypeName
        {
            get
            {
                return XmlDataHelpper.Instance.GetValue(XmlDataTypeEnum.ConfigurationType.ToString(), TypeId.ToString());
            }
        }

        public string Name { get; set; }
        public string Value { get; set; }
        public bool? Configurable { get; set; }
        public bool? IsHtml { get; set; }
    }
}