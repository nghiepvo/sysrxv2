﻿namespace WorkComp.Models.PanelType
{
    public class PanelTypeParameter : MasterfileParameter
    {
        public string PanelCodeParams { get; set; }
    }
}