﻿using System;
using Framework.DomainModel.ValueObject;

namespace WorkComp.Models.PanelType
{
    public class PanelCodeGridItemViewModel
    {
        public Guid RowGuid { get; set; }
        public int Id { get; set; }
        public int? PanelId { get; set; }
        public string CptCode { get; set; }
        public string Urine { get; set; }
        public string OralFluid { get; set; }
        public string Blood { get; set; }
        public string Hair { get; set; }
        public LookupInGridDataSourceViewModel Panel { get; set; }
    }
}