﻿using System.Collections.ObjectModel;
using Framework.DomainModel.Entities.Common;

namespace WorkComp.Models.PanelType
{
    public class PanelCodeGridViewModel: EditableGridViewModel
    {
        public PanelCodeGridViewModel()
        {
            DocumentTypeId = (int)DocumentTypeKey.PanelType;
        }

        public Collection<PanelCodeGridItemViewModel> PanelCodes { get; set; }
        public int PanelTypeId { get; set; }
    }
}