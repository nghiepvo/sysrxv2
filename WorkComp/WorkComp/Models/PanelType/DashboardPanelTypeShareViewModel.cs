﻿using Framework.DomainModel.ValueObject;

namespace WorkComp.Models.PanelType
{
    public class DashboardPanelTypeShareViewModel : DashboardSharedViewModel
    {
        public string Name { get; set; }
        public string Code { get; set; }
        public int? PayerId { get; set; }
        public bool? IsBasic { get; set; }
        public LookupItemVo PayerDataSource { get; set; }
        public PanelCodeGridViewModel PanelCodeGrid { get; set; }

    }
}