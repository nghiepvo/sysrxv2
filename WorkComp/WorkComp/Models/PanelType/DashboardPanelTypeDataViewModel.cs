﻿namespace WorkComp.Models.PanelType
{
    public class DashboardPanelTypeDataViewModel :
        MasterfileViewModelBase<Framework.DomainModel.Entities.PanelType>
    {
        public override void MapFromClientParameters(MasterfileParameter parameters)
        {
            SharedViewModel = MapFromClientParameters<DashboardPanelTypeShareViewModel>(parameters);
        }
        public override string PageTitle
        {
            get
            {
                return SharedViewModel.CreateMode ? "Create Panel Type" : "Update Panel Type";
            }
        }
    }
}