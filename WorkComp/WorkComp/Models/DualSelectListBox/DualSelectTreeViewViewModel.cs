﻿namespace WorkComp.Models.DualSelectListBox
{
    public class DualSelectTreeViewViewModel : ViewModelBase
    {
        public string ControlId { get; set; }

        public string ModelName { get; set; }
        public string SelectedUrl { get; set; }

        public string TreeViewUrl { get; set; }

        public int MasterfileId { get; set; }

        public string TreeViewLabelText { get; set; }

        public string SelectedItemText { get; set; }

        public int? OffsetHeight { get; set; }

    }
}