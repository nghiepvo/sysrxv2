﻿namespace WorkComp.Models.ReferralSource
{
    public class DashboardReferralSourceIndexViewModel : DashboardGridViewModelBase<Framework.DomainModel.Entities.ReferralSource>
    {
        public override string PageTitle
        {
            get
            {
                return "Referral Source";
            }
        }
    }
    
}