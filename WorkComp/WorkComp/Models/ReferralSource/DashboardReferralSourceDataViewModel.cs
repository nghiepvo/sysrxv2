﻿namespace WorkComp.Models.ReferralSource
{
    public class DashboardReferralSourceDataViewModel : MasterfileViewModelBase<Framework.DomainModel.Entities.ReferralSource>
    {
        public override void MapFromClientParameters(MasterfileParameter parameters)
        {
            SharedViewModel = MapFromClientParameters<DashboardReferralSourceShareViewModel>(parameters);
        }

        public override string PageTitle
        {
            get
            {
                return SharedViewModel.CreateMode ? "Create Referral Source" : "Update Referral Source";
            }
        }
    }
}