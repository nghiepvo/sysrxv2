﻿using System;
using Framework.DomainModel.ValueObject;

namespace WorkComp.Models.ReferralSource
{
    public class DashboardReferralSourceShareViewModel : DashboardSharedViewModel
    {
        public string FirstName { get; set; }
        public string LastName { get; set; }
        public string MiddleName { get; set; }
        
        public int? ReferralTypeId { get; set; }
        public LookupItemVo ReferralTypeDataSource { get; set; }

        public string Company { get; set; }
        public string Phone { get; set; }
        public string Extension { get; set; }
        public string Fax { get; set; }
        public string Email { get; set; }
        public string Address { get; set; }

        public int? CityId { get; set; }
        public LookupItemVo CityDataSource { get; set; }
        public int? StateId { get; set; }
        public LookupItemVo StateDataSource { get; set; }
        public int? ZipId { get; set; }
        public LookupItemVo ZipDataSource { get; set; }

        public bool IsAuthorization { get; set; }
        public DateTime? AuthorizationFrom { get; set; }
        public DateTime? AuthorizationTo { get; set; }

        
    }
}