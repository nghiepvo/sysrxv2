﻿using Framework.DomainModel.ValueObject;

namespace WorkComp.Models.TaskTemplate
{
    public class DashboardTaskTemplateShareViewModel : DashboardSharedViewModel
    {
        public string Title { get; set; }
        public string Description { get; set; }
        public int? NumDueDate { get; set; }
        public double? NumDueHour { get; set; }
        public int StatusId { get; set; }
        public int? AssignToId { get; set; }
        public int? TaskTypeId { get; set; }

        public LookupItemVo AssignToDataSource { get; set; }
    }
}