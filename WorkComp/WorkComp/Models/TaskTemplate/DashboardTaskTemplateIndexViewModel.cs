﻿namespace WorkComp.Models.TaskTemplate
{
    public class DashboardTaskTemplateIndexViewModel : DashboardGridViewModelBase<Framework.DomainModel.Entities.TaskTemplate>
    {
        public override string PageTitle
        {
            get
            {
                return "Task Template";
            }
        }
    }
}