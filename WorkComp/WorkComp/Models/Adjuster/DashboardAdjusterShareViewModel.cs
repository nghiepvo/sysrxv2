﻿using System;
using Framework.DomainModel.ValueObject;

namespace WorkComp.Models.Adjuster
{
    public class DashboardAdjusterShareViewModel : DashboardSharedViewModel
    {
        
        public int PayerId { get; set; }
        public LookupItemVo PayerDataSource { get; set; }

        public int? BranchId { get; set; }
        public LookupItemVo BranchDataSource { get; set; }

        public string FirstName { get; set; }
        public string LastName { get; set; }
        public string MiddleName { get; set; }

        public DateTime? AssignedDate { get; set; }

        public string Phone { get; set; }

        public string Extension { get; set; }
        public string Email { get; set; }
        public string Address { get; set; }
        
        public int? CityId { get; set; }
        public LookupItemVo CityDataSource { get; set; }
        public int? StateId { get; set; }
        public LookupItemVo StateDataSource { get; set; }
        public int? ZipId { get; set; }
        public LookupItemVo ZipDataSource { get; set; }

        public bool IsUserUpdate { get; set; }

        //TODO: Edit Allow Create Referral
        public bool AllowCreateReferral
        {
            get { return true; }
        }
        public bool OptedOutSendMail { get; set; }

        
    }
}