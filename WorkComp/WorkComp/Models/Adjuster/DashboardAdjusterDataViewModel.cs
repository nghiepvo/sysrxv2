﻿namespace WorkComp.Models.Adjuster
{
    public class DashboardAdjusterDataViewModel : MasterfileViewModelBase<Framework.DomainModel.Entities.Adjuster>
    {
        public override void MapFromClientParameters(MasterfileParameter parameters)
        {
            SharedViewModel = MapFromClientParameters<DashboardAdjusterShareViewModel>(parameters);
        }
    }
}