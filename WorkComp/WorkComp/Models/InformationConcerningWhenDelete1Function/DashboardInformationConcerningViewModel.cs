﻿using System;
using System.Collections.Generic;

namespace WorkComp.Models.InformationConcerningWhenDelete1Function
{
    public class DashboardInformationConcerningViewModel : ViewModelBase
    {
        public string Title { get; set; }
        public Dictionary<string, IList<string>> InformationConcernings { get; set; }
    }
}