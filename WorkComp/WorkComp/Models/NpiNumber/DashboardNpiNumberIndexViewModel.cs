﻿namespace WorkComp.Models.NpiNumber
{
    public class DashboardNpiNumberIndexViewModel : DashboardGridViewModelBase<Framework.DomainModel.Entities.NpiNumber>
    {
        public override string PageTitle
        {
            get
            {
                return "Npi Number";
            }
        }
    }
    
}