﻿namespace WorkComp.Models.NpiNumber
{
    public class DashboardNpiNumberDataViewModel : MasterfileViewModelBase<Framework.DomainModel.Entities.NpiNumber>
    {
        public override void MapFromClientParameters(MasterfileParameter parameters)
        {
            SharedViewModel = MapFromClientParameters<DashboardNpiNumberShareViewModel>(parameters);
        }

        public override string PageTitle
        {
            get
            {
                return SharedViewModel.CreateMode ? "Create Npi Number" : "Update Npi Number";
            }
        }
    }
}