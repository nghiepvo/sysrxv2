﻿namespace WorkComp.Models.TaskGroup
{
    public class DashboardTaskGroupDataViewModel : MasterfileViewModelBase<Framework.DomainModel.Entities.TaskGroup>
    {
        public override void MapFromClientParameters(MasterfileParameter parameters)
        {
            SharedViewModel = MapFromClientParameters<DashboardTaskGroupShareViewModel>(parameters);
        }
        public override string PageTitle
        {
            get
            {
                return SharedViewModel.CreateMode ? "Create Task Group" : "Update Task Group";
            }
        }
    }
}