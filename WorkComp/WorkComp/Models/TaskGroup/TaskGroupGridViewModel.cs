﻿using System;
using System.Collections.ObjectModel;
using Framework.DomainModel.Entities.Common;
using Framework.DomainModel.ValueObject;

namespace WorkComp.Models.TaskGroup
{
    public class TaskGroupGridViewModel : EditableGridViewModel
    {
        public TaskGroupGridViewModel()
        {
            DocumentTypeId = (int)DocumentTypeKey.TaskGroup;
        }

        public Collection<TaskTemplateGridItemViewModel> TaskTemplates { get; set; }
        public int ReferralId { get; set; }

    }

    public class TaskTemplateGridItemViewModel
    {
        public Guid RowGuid { get; set; }
        public int Id { get; set; }
        public LookupInGridDataSourceViewModel TaskTemplate { get; set; }
        public string Title { get; set; }
        public string Description { get; set; }
        public LookupInGridDataSourceViewModel AssignTo { get; set; }
        public DateTime? StartDate { get; set; }
        public DateTime? DueDate { get; set; }
        public LookupInGridDataSourceViewModel TaskType { get; set; }
    }


    
}