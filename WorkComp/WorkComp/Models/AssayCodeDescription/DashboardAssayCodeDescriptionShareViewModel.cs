﻿namespace WorkComp.Models.AssayCodeDescription
{
    public class DashboardAssayCodeDescriptionShareViewModel : DashboardSharedViewModel
    {
        public string Description { get; set; }
    }
}