﻿namespace WorkComp.Models.AssayCodeDescription
{
    public class DashboardAssayCodeDescriptionDataViewModel : MasterfileViewModelBase<Framework.DomainModel.Entities.AssayCodeDescription>
    {
        public override void MapFromClientParameters(MasterfileParameter parameters)
        {
            SharedViewModel = MapFromClientParameters<DashboardAssayCodeDescriptionShareViewModel>(parameters);
        }

        public override string PageTitle
        {
            get
            {
                return SharedViewModel.CreateMode ? "Create Assay Code Description" : "Update Assay Code Description";
            }
        }
    }
}