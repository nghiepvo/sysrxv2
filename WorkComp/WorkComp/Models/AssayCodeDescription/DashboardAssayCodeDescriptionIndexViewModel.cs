﻿namespace WorkComp.Models.AssayCodeDescription
{
    public class DashboardAssayCodeDescriptionIndexViewModel : DashboardGridViewModelBase<Framework.DomainModel.Entities.AssayCodeDescription>
    {
        public override string PageTitle
        {
            get
            {
                return "Assay Code Description";
            }
        }
    }
    
}