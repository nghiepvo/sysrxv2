﻿using System;
using System.Text;

namespace WorkComp.Models.EmailTemplate
{
    public class DashboardEmailTemplateShareViewModel : DashboardSharedViewModel
    {
        public string Title { get; set; }
        public string Subject { get; set; }
        public string Content { get; set; }
        public bool IsService { get; set; }
    }
}