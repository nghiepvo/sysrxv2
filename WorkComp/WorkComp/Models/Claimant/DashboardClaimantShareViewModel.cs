﻿using System;
using Framework.DomainModel.ValueObject;

namespace WorkComp.Models.Claimant
{
    public class DashboardClaimantShareViewModel : DashboardSharedViewModel
    {
        public string FirstName { get; set; }
        public string LastName { get; set; }
        public string MiddleName { get; set; }
        public string Suffix { get; set; }
        public string Ssn { get; set; }
        public string PatientAbbr { get; set; }

        public string PayerPatientId { get; set; }

        public DateTime? ExpirationDate { get; set; }

        public string Gender { get; set; }
        public DateTime Birthday { get; set; }
        public string Email { get; set; }

        public string WorkPhone { get; set; }
        public string WorkPhoneExtension { get; set; }
        public string HomePhone { get; set; }
        public string HomePhoneExtension { get; set; }
        public string CellPhone { get; set; }

        public string BestContactNumber { get; set; }

       

        public string BestContactNumberType { get; set; }
        

        public string Address1 { get; set; }
        public string Address2 { get; set; }

        public int? ClaimantLanguageId { get; set; }
        public LookupItemVo ClaimantLanguageDataSource { get; set; }
        
        public int? StateId { get; set; }
        public LookupItemVo StateDataSource { get; set; }
        public int? CityId { get; set; }
        public LookupItemVo CityDataSource { get; set; }
        public int? ZipId { get; set; }
        public LookupItemVo ZipDataSource { get; set; }
        public bool IsUserUpdate { get; set; }
    }
}