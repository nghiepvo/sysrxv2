﻿namespace WorkComp.Models.Attorney
{
    public class DashboardAttorneyDataViewModel : MasterfileViewModelBase<Framework.DomainModel.Entities.Attorney>
    {
        public override void MapFromClientParameters(MasterfileParameter parameters)
        {
            SharedViewModel = MapFromClientParameters<DashboardAttorneyShareViewModel>(parameters);
        }
    }
}