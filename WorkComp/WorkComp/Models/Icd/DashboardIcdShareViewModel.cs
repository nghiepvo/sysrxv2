﻿namespace WorkComp.Models.Icd
{
    public class DashboardIcdShareViewModel : DashboardSharedViewModel
    {
        public string Code { get; set; }
        public string Description { get; set; }
        public int IcdTypeId { get; set; }
    }
}