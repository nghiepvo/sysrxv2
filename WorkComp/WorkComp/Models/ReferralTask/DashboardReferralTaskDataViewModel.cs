﻿namespace WorkComp.Models.ReferralTask
{
    public class DashboardReferralTaskDataViewModel : MasterfileViewModelBase<Framework.DomainModel.Entities.ReferralTask>
    {
        public override void MapFromClientParameters(MasterfileParameter parameters)
        {
            SharedViewModel = MapFromClientParameters<DashboardReferralTaskShareViewModel>(parameters);
        }

        public override string PageTitle
        {
            get { return SharedViewModel.CreateMode ? "Create Referral Task" : "Update Referral Task"; }
        }
    }
}