﻿using System;
using Framework.DomainModel.Entities.Common;
using Framework.Utility;

namespace WorkComp.Models.ReferralTask
{
    public class DashboardReferralTaskDetailViewModel : DashboardSharedViewModel
    {
        public override string PageTitle
        {
            get
            {
                return "TASK DETAIL";
            }
        }

        public int ReferralId { get; set; }
        
        public string Title { get; set; }
        public int StatusId { get; set; }

        public string Status
        {
            get
            {
                return XmlDataHelpper.Instance.GetValue(XmlDataTypeEnum.Status.ToString(), StatusId.ToString());
            }
        }

        public string AssignTo { get; set; }
        public DateTime StartDate { get; set; }
        public DateTime DueDate { get; set; }
        public string CreatedBy { get; set; }
        public DateTime CreatedDate { get; set; }
        public string Description { get; set; }

        
        public int ClaimantId { get; set; }
        public string Patient { get; set; }

        public int PreviousId { get; set; }
        public int NextId { get; set; }
    }
    
}