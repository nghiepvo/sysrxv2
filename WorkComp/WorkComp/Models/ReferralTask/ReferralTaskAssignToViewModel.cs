﻿using System.Collections.Generic;

namespace WorkComp.Models.ReferralTask
{
    public class ReferralTaskAssignToViewModel : ViewModelBase
    {
        public List<int> ListReferralTaskIdSelected { get; set; }
        public bool IsSelectAll { get; set; }
        public string ListReferralTaskIdSelectedString { get; set; }
        public int? AssignToId { get; set; }
        public string TypeWithUser { get; set; }
    }
    public class ReferralTaskPrintViewModel : ViewModelBase
    {
        public string ListReferralTaskIdSelectedString { get; set; }
        public int? CurrentUserId { get; set; }
    }
}