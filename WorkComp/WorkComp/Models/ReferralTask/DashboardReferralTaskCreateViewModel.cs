﻿using System;
using System.Collections.Generic;
using Framework.DomainModel.ValueObject;

namespace WorkComp.Models.ReferralTask
{
    public class ReferralTaskItemCreate
    {
        public int? ReferralId { get; set; }
        public string Title {set; get;}
        public int? TaskTypeId {set; get;}
        public string Description {set; get;}
        public DateTime? StartDate {set; get;}
        public DateTime? DueDate {set; get;}
        public int StatusId {set; get;}
        public int? AssignToId {set; get;}
        public bool Rush { get; set; }
    }

    public class DashboardReferralTaskCreateViewModel : DashboardSharedViewModel
    {
        public int? ReferralId { get; set; }
        public List<ReferralTaskItemCreate> ReferralTaskList { get; set; }
    }
}