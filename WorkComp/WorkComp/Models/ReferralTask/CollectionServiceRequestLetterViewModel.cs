﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using Framework.Utility;

namespace WorkComp.Models.ReferralTask
{
    public class CollectionServiceRequestLetterViewModel : MessageModelBase
    {
        public int ReferralId { get; set; }
        private string _fax;
        public string Fax
        {
            get { return _fax.ApplyFormatPhone(); }
            set { _fax = value; }
        }

        public string ContentHtml { get; set; }
        public int ReferralTaskId { get; set; }

    }
}