﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web.Helpers;
using Framework.DomainModel.Entities.Common;
using Framework.DomainModel.ValueObject;
using Framework.Utility;
using Newtonsoft.Json;

namespace WorkComp.Models.ClaimNumber
{
    public class DashboardClaimNumberShareViewModel : DashboardSharedViewModel
    {
        
        public int? PayerId { get; set; }
        public LookupItemVo PayerDataSource { get; set; }



        public int? BranchId { get; set; }
        public LookupItemVo BranchDataSource { get; set; }

        public int? ClaimantId { get; set; }
        public LookupItemVo ClaimantDataSource { get; set; }
        public int? AdjusterId { get; set; }
        public LookupItemVo AdjusterDataSource { get; set; }

        public int? EmployerId { get; set; }
        public LookupItemVo EmployerDataSource { get; set; }

        public string Name { get; set; }

        public int? StateId { get; set; }
        public LookupItemVo StateDataSource { get; set; }

        public string SpecialInstructions { get; set; }

        public DateTime? Doi { get; set; }
        public DateTime? CloseDate { get; set; }
        public DateTime? OpenDate { get; set; }
        public DateTime? ReClosedDate { get; set; }
        public DateTime? ReOpenDate { get; set; }
        public DateTime? ReportDate { get; set; }

        public bool IsUserUpdate { get; set; }

        public bool IsOpen { get; set; }
        public string LableIsOpen { get; set; }
        public bool EnableIsOpen { get; set; }

        public Int16 Status { get; set; }
        public int CurrentStatus { get; set; }

        public DateTime DateTimeServer { get; set; }


        public string JsonStatusList {
            get
            {
                string result = "";
                var datas = XmlDataHelpper.Instance.GetData(XmlDataTypeEnum.ClaimNumberStatus.ToString());
                var entries = datas.Select(d => new LookupItemVo(){KeyId = int.Parse(d.Key), DisplayName = d.Value});
                result = JsonConvert.SerializeObject(entries);
                return result;
            } 
        }

    }
}