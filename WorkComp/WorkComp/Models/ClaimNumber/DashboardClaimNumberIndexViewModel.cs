﻿namespace WorkComp.Models.ClaimNumber
{
    public class DashboardClaimNumberIndexViewModel : DashboardGridViewModelBase<Framework.DomainModel.Entities.ClaimNumber>
    {
        public override string PageTitle
        {
            get
            {
                return "Claim Number";
            }
        }
    }
    
}