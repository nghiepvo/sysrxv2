﻿using System.Web.SessionState;
using Autofac;
using Autofac.Extras.DynamicProxy2;
using Framework.Exceptions.DataAccess.Interceptor;
using Framework.Exceptions.DataAccess.Meta;
using Framework.Exceptions.DataAccess.Translator;
using Framework.Service.Diagnostics;
using Framework.Web;
using Repositories;
using Repositories.Interfaces;
using ServiceLayer.Authentication;
using ServiceLayer.Authorization;
using ServiceLayer.Interfaces.Authentication;

namespace Common
{
    public class CoreModule:Module
    {
        public CoreModule()
        {
            NameOrConnectionString = "WorkCompDB";
        }
        public CoreModule(string nameOrConnectionString)
        {
            NameOrConnectionString = nameOrConnectionString;
        }

        public string NameOrConnectionString { get; set; }

        protected override void Load(ContainerBuilder builder)
        {
            base.Load(builder);

            RegisterServices(builder);
            RegisterRepositories(builder);
        }
        private void RegisterServices(ContainerBuilder builder)
        {
            builder.RegisterType<AuthenticationService>().As<IAuthenticationService>().InstancePerLifetimeScope();
            builder.RegisterType<DiagnosticService>().As<IDiagnosticService>();
            builder.RegisterType<FormAuthenticationService>().As<IFormAuthenticationService>().InstancePerLifetimeScope();
            builder.RegisterType<WorkCompHttpContext>().As<IWorkCompHttpContext>().InstancePerLifetimeScope();
            builder.RegisterType<SessionIDManager>().As<ISessionIDManager>().InstancePerLifetimeScope();
            builder.RegisterType<ClaimsManager>().As<IClaimsManager>().InstancePerLifetimeScope();
            builder.RegisterType<OperationAuthorization>().As<IOperationAuthorization>();
        }
        private void RegisterRepositoriesInterceptor(ContainerBuilder builder)
        {
            builder.RegisterType<DataAccessExceptionInterceptor>();
        }

        private void RegisterTenantSystemRepositories(ContainerBuilder builder)
        {
            builder.RegisterType<SqlServerDbMetaInfo>().As<IDbMetaInfo>();
            builder.RegisterType<EntityFrameworkExceptionTranslator>().As<IExceptionTranslator>();
        }
        private void RegisterRepositories(ContainerBuilder builder)
        {
            RegisterRepositoriesInterceptor(builder);
            RegisterTenantSystemRepositories(builder);
            builder.RegisterType<EntityFrameworkEmailTemplateRepository>().As<IEmailTemplateRepository>().EnableInterfaceInterceptors().InterceptedBy(typeof(DataAccessExceptionInterceptor));
            builder.RegisterType<EntityFrameworkCityRepository>().As<ICityRepository>().EnableInterfaceInterceptors().InterceptedBy(typeof(DataAccessExceptionInterceptor));
            builder.RegisterType<EntityFrameworkIcdRepository>().As<IIcdRepository>().EnableInterfaceInterceptors().InterceptedBy(typeof(DataAccessExceptionInterceptor));
            builder.RegisterType<EntityFrameworkStateRepository>().As<IStateRepository>().EnableInterfaceInterceptors().InterceptedBy(typeof(DataAccessExceptionInterceptor));
            builder.RegisterType<EntityFrameworkDrugRepository>().As<IDrugRepository>().EnableInterfaceInterceptors().InterceptedBy(typeof(DataAccessExceptionInterceptor));
            builder.RegisterType<EntityFrameworkNpiNumberRepository>().As<INpiNumberRepository>().EnableInterfaceInterceptors().InterceptedBy(typeof(DataAccessExceptionInterceptor));
            builder.RegisterType<EntityFrameworkAttorneyRepository>().As<IAttorneyRepository>().EnableInterfaceInterceptors().InterceptedBy(typeof(DataAccessExceptionInterceptor));
            builder.RegisterType<EntityFrameworkCollectionSiteRepository>().As<ICollectionSiteRepository>().EnableInterfaceInterceptors().InterceptedBy(typeof(DataAccessExceptionInterceptor));
            builder.RegisterType<EntityFrameworkEmployerRepository>().As<IEmployerRepository>().EnableInterfaceInterceptors().InterceptedBy(typeof(DataAccessExceptionInterceptor));
            builder.RegisterType<EntityFrameworkReferralTypeRepository>().As<IReferralTypeRepository>().EnableInterfaceInterceptors().InterceptedBy(typeof(DataAccessExceptionInterceptor));
            builder.RegisterType<EntityFrameworkReferralSourceRepository>().As<IReferralSourceRepository>().EnableInterfaceInterceptors().InterceptedBy(typeof(DataAccessExceptionInterceptor));
            builder.RegisterType<EntityFrameworkAssayCodeRepository>().As<IAssayCodeRepository>().EnableInterfaceInterceptors().InterceptedBy(typeof(DataAccessExceptionInterceptor));
            builder.RegisterType<EntityFrameworkSampleTestingTypeRepository>().As<ISampleTestingTypeRepository>().EnableInterfaceInterceptors().InterceptedBy(typeof(DataAccessExceptionInterceptor));
            builder.RegisterType<EntityFrameworkAssayCodeDescriptionRepository>().As<IAssayCodeDescriptionRepository>().EnableInterfaceInterceptors().InterceptedBy(typeof(DataAccessExceptionInterceptor));
            builder.RegisterType<EntityFrameworkAdjusterRepository>().As<IAdjusterRepository>().EnableInterfaceInterceptors().InterceptedBy(typeof(DataAccessExceptionInterceptor));
            builder.RegisterType<EntityFrameworkClaimNumberRepository>().As<IClaimNumberRepository>().EnableInterfaceInterceptors().InterceptedBy(typeof(DataAccessExceptionInterceptor));
            builder.RegisterType<EntityFrameworkZipRepository>().As<IZipRepository>().EnableInterfaceInterceptors().InterceptedBy(typeof(DataAccessExceptionInterceptor));
            builder.RegisterType<EntityFrameworkUserRepository>().As<IUserRepository>().EnableInterfaceInterceptors().InterceptedBy(typeof(DataAccessExceptionInterceptor));
            builder.RegisterType<EntityFrameworkUserRoleFunctionRepository>().As<IUserRoleFunctionRepository>().EnableInterfaceInterceptors().InterceptedBy(typeof(DataAccessExceptionInterceptor));
            builder.RegisterType<EntityFrameworkGridConfigRepository>().As<IGridConfigRepository>().EnableInterfaceInterceptors().InterceptedBy(typeof(DataAccessExceptionInterceptor));
            builder.RegisterType<EntityFrameworkClaimantLanguageRepository>().As<IClaimantLanguageRepository>().EnableInterfaceInterceptors().InterceptedBy(typeof(DataAccessExceptionInterceptor));
            builder.RegisterType<EntityFrameworkTaskTemplateRepository>().As<ITaskTemplateRepository>().EnableInterfaceInterceptors().InterceptedBy(typeof(DataAccessExceptionInterceptor));
            builder.RegisterType<EntityFrameworkTaskGroupRepository>().As<ITaskGroupRepository>().EnableInterfaceInterceptors().InterceptedBy(typeof(DataAccessExceptionInterceptor));
            builder.RegisterType<EntityFrameworkPayerRepository>().As<IPayerRepository>().EnableInterfaceInterceptors().InterceptedBy(typeof(DataAccessExceptionInterceptor));
            builder.RegisterType<EntityFrameworkUserRoleRepository>().As<IUserRoleRepository>().EnableInterfaceInterceptors().InterceptedBy(typeof(DataAccessExceptionInterceptor));
            builder.RegisterType<EntityFrameworkBranchRepository>().As<IBranchRepository>().EnableInterfaceInterceptors().InterceptedBy(typeof(DataAccessExceptionInterceptor));
            builder.RegisterType<EntityFrameworkClaimantRepository>().As<IClaimantRepository>().EnableInterfaceInterceptors().InterceptedBy(typeof(DataAccessExceptionInterceptor));
            builder.RegisterType<EntityFrameworkPanelTypeRepository>().As<IPanelTypeRepository>().EnableInterfaceInterceptors().InterceptedBy(typeof(DataAccessExceptionInterceptor));
            builder.RegisterType<EntityFrameworkPanelRepository>().As<IPanelRepository>().EnableInterfaceInterceptors().InterceptedBy(typeof(DataAccessExceptionInterceptor));
            builder.RegisterType<EntityFrameworkReferralRepository>().As<IReferralRepository>().EnableInterfaceInterceptors().InterceptedBy(typeof(DataAccessExceptionInterceptor));
            builder.RegisterType<EntityFrameworkCaseManagerRepository>().As<ICaseManagerRepository>().EnableInterfaceInterceptors().InterceptedBy(typeof(DataAccessExceptionInterceptor));
            builder.RegisterType<EntityFrameworkReasonReferralRepository>().As<IReasonReferralRepository>().EnableInterfaceInterceptors().InterceptedBy(typeof(DataAccessExceptionInterceptor));
            builder.RegisterType<EntityFrameworkEmailTemplateRepository>().As<IEmailTemplateRepository>().EnableInterfaceInterceptors().InterceptedBy(typeof(DataAccessExceptionInterceptor));
            builder.RegisterType<EntityFrameworkProductTypeRepository>().As<IProductTypeRepository>().EnableInterfaceInterceptors().InterceptedBy(typeof(DataAccessExceptionInterceptor));
            builder.RegisterType<EntityFrameworkReferralAttachmentRepository>().As<IReferralAttachmentRepository>().EnableInterfaceInterceptors().InterceptedBy(typeof(DataAccessExceptionInterceptor));
            builder.RegisterType<EntityFrameworkConfigurationRepository>().As<IConfigurationRepository>().EnableInterfaceInterceptors().InterceptedBy(typeof(DataAccessExceptionInterceptor));
            builder.RegisterType<EntityFrameworkReferralNoteRepository>().As<IReferralNoteRepository>().EnableInterfaceInterceptors().InterceptedBy(typeof(DataAccessExceptionInterceptor));
            builder.RegisterType<EntityFrameworkReferralTaskRepository>().As<IReferralTaskRepository>().EnableInterfaceInterceptors().InterceptedBy(typeof(DataAccessExceptionInterceptor));
            builder.RegisterType<EntityFrameworkDiaryRepository>().As<IDiaryRepository>().EnableInterfaceInterceptors().InterceptedBy(typeof(DataAccessExceptionInterceptor));
            builder.RegisterType<EntityFrameworkHeadingRepository>().As<IHeadingRepository>().EnableInterfaceInterceptors().InterceptedBy(typeof(DataAccessExceptionInterceptor));
            builder.RegisterType<EntityFrameworkCommentRepository>().As<ICommentRepository>().EnableInterfaceInterceptors().InterceptedBy(typeof(DataAccessExceptionInterceptor));
            builder.RegisterType<EntityFrameworkAlertRepository>().As<IAlertRepository>().EnableInterfaceInterceptors().InterceptedBy(typeof(DataAccessExceptionInterceptor));
            builder.RegisterType<EntityFrameworkReferralSampleTestingTypeRepository>().As<IReferralSampleTestingTypeRepository>().EnableInterfaceInterceptors().InterceptedBy(typeof(DataAccessExceptionInterceptor));
            builder.RegisterType<SysRxReportRepository>().As<ISysRxReportRepository>().EnableInterfaceInterceptors().InterceptedBy(typeof(DataAccessExceptionInterceptor));
            builder.RegisterType<EntityFrameworkDocumentTypeRepository>().As<IDocumentTypeRepository>().EnableInterfaceInterceptors().InterceptedBy(typeof(DataAccessExceptionInterceptor));
            builder.RegisterType<EntityFrameworkReferralEmailTemplateRepository>().As<IReferralEmailTemplateRepository>().EnableInterfaceInterceptors().InterceptedBy(typeof(DataAccessExceptionInterceptor));
            builder.RegisterType<EntityFrameworkReferralEmailTemplateAttachmentRepository>().As<IReferralEmailTemplateAttachmentRepository>().EnableInterfaceInterceptors().InterceptedBy(typeof(DataAccessExceptionInterceptor));
        }
    }
}
