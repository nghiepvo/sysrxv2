﻿namespace Common
{
    public class UserUploadPathFile
    {
        public static string UserAvatar
        {
            get
            {
                return "~/FileUpload/Avatar/";
            }
        }
        public static string UploadFolder
        {
            get
            {
                return "~/FileUpload/Upload/";
            }
        }
    }
}