﻿using System.Collections.Generic;
using Framework.DomainModel.Entities;
using Framework.DomainModel.ValueObject;

namespace ServiceLayer.Interfaces
{
    public interface IHeadingService : IMasterFileService<Heading>
    {
        List<LookupItemVo> GetListHeading();
    }
}