﻿using Framework.DomainModel.Entities;

namespace ServiceLayer.Interfaces
{
    public interface IReferralTypeService : IMasterFileService<ReferralType>
    {
    }
}