﻿using System;
using System.Collections.Generic;
using Framework.DomainModel.Entities;
using Framework.DomainModel.Entities.Common;
using Framework.DomainModel.ValueObject;

namespace ServiceLayer.Interfaces
{
    public interface INpiNumberService : IMasterFileService<NpiNumber>
    {
        List<LookupItemVo> GetLookupTreatingPhysician(LookupQuery query, Func<NpiNumber, LookupItemVo> selector);
        InfoWhenChangeTreatingPhysicianInReferral GetInfoWhenChangeTreatingPhysicianInReferral(int idTreatingPhysician);
    }
}