﻿using System.Collections.Generic;
using Framework.DomainModel.Entities;
using Framework.DomainModel.Interfaces;
using Framework.DomainModel.ValueObject;

namespace ServiceLayer.Interfaces
{
    public interface IReferralService : IMasterFileService<Referral>
    {
        List<ReferralMedicationHistory> GetMedicationHistoriesByReferral(int referralId);
        dynamic GetDataParentForGrid(IQueryInfo queryInfo);
        dynamic GetDataChildrenForGrid(IQueryInfo queryInfo);
        dynamic GetListTaskByReferralId(int id);
        ReferralDetailVo GetReferralDeltailById(int id);
        ReferralResultDetailVo GetReferralResultInfo(int referralId);
        List<ReasonReferralInReferralDetailVo> GetReasonReferralInfos(int id);
        void RemoveAttachmentFile(int referralId, string rowGuid, string fileName);
        void AddReferralAttachment(ReferralAttachment objAdd);
        int GetPreviousReferralId(int id);
        int GetNextReferralId(int id);
        void AssignToForReferral(List<int> listIdSelected, bool isSelectAll, int? assignToId,string typeWithUser);
        dynamic GetDataPrintToReferral(List<int> listIdSelected, IQueryInfo queryInfo);
        SendMailWithReferralIdVo GetSendMailInfo(int referralId);
    }
}