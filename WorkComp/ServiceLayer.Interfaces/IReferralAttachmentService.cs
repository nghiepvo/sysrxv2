﻿using System;
using System.Collections.Generic;
using System.Web;
using Framework.DomainModel.Entities;

namespace ServiceLayer.Interfaces
{
    public interface IReferralAttachmentService
    {
        string TransferUploadPath { get; set; }
        IList<TResult> GetAttachments<TResult>(int referralId, Func<ReferralAttachment, TResult> selector);
        ReferralAttachment UploadFile(HttpPostedFileBase files, string oldFile, Guid oldToken);
        void DownloadFile(HttpContextBase context, string fileName, Guid rowGuid);
        void RemoveFile(string fileName, Guid rowGuid);
        byte[] ReadFile(string fileName, Guid rowGuid);
    }
}