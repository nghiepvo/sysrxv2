﻿using System.Collections.Generic;
using Framework.DomainModel.Entities;

namespace ServiceLayer.Interfaces
{
    public interface IReferralSampleTestingTypeService : IMasterFileService<ReferralSampleTestingType>
    {
        List<string> GetListTestingTypeName(int referralId);
    }
}