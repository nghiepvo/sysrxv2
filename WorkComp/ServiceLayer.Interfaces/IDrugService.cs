﻿using Framework.DomainModel.Entities;

namespace ServiceLayer.Interfaces
{
    public interface IDrugService : IMasterFileService<Drug>
    {
        string GetDrugs(int idReferral);
    }
}