﻿using Framework.DomainModel.Entities;

namespace ServiceLayer.Interfaces
{
    public interface IUserRoleFunctionService : IMasterFileService<UserRoleFunction>
    {
    }
}