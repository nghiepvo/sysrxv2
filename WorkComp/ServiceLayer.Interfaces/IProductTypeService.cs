﻿using Framework.DomainModel.Entities;

namespace ServiceLayer.Interfaces
{
    public interface IProductTypeService : IMasterFileService<ProductType>
    {
    }
}