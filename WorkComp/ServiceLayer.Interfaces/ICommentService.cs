﻿using System.Collections.Generic;
using Framework.DomainModel.Entities;

namespace ServiceLayer.Interfaces
{
    public interface ICommentService : IMasterFileService<Comment>
    {
        IList<Comment> GetCommentWhenChangeReferral(int? referralTaskId);
    }
}