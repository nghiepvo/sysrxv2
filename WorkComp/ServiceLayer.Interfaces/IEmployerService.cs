﻿using Framework.DomainModel.Entities;
using Framework.DomainModel.Interfaces;
using Framework.DomainModel.ValueObject;

namespace ServiceLayer.Interfaces
{
    public interface IEmployerService : IMasterFileService<Employer>
    {
        InfoWhenChangeEmployerInReferral GetInfoWhenChangeEmployerInReferral(int idEmployer);
    }
}