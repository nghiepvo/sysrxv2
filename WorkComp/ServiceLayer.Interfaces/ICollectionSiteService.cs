﻿using Framework.DomainModel.Entities;
using Framework.DomainModel.ValueObject;

namespace ServiceLayer.Interfaces
{
    public interface ICollectionSiteService : IMasterFileService<CollectionSite>
    {
        InfoWhenChangeCollectionSiteInReferral GetInfoWhenChangeCollectionSiteInReferral(int idCollectionSite);
    }
}