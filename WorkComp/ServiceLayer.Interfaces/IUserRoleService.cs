﻿using System.Collections.Generic;
using Framework.DomainModel.Entities;

namespace ServiceLayer.Interfaces
{
    public interface IUserRoleService : IMasterFileService<UserRole>
    {
        dynamic GetRoleFunction(int idRole);
        List<DocumentType> GetAllDocumentType();
    }
}