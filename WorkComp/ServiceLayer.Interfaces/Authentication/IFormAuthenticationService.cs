﻿using System;
using Framework.DomainModel.Interfaces;

namespace ServiceLayer.Interfaces.Authentication
{
    public interface IFormAuthenticationService
    {
        string AuthenticationCookieValue { get; }
        void SignOut();
        void SignIn(IWorkcompPrincipal principal, bool rememberMe, string authToken, DateTime? expires);
        void SetAuthenticationCookie(IWorkcompPrincipal principal, string authToken, DateTime? expires);
        void SetPrincipalCache(IWorkcompPrincipal principal, string authKey, DateTime? expires);
    }
}
