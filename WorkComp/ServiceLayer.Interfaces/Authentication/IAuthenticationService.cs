﻿using Framework.DomainModel.Entities;
using Framework.DomainModel.Interfaces;

namespace ServiceLayer.Interfaces.Authentication
{
    public interface IAuthenticationService
    {
        IWorkcompPrincipal GetCurrentUser();
        void SignOut();
        bool SignIn(string userName, string password, bool rememberMe);
        User RestorePassword(string email,out string passwordRandom);
        void UpdatePrincipal(IWorkcompPrincipal principal);
    }
}
