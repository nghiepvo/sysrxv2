﻿using Framework.DomainModel.Entities;

namespace ServiceLayer.Interfaces
{
    public interface IBranchService : IMasterFileService<Branch>
    {
    }
}
