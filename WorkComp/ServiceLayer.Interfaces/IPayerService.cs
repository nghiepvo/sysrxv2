﻿using Framework.DomainModel.Entities;
using Framework.DomainModel.ValueObject;

namespace ServiceLayer.Interfaces
{
    public interface IPayerService : IMasterFileService<Payer>
    {
        LookupItemVo GetDefaulDataSource(int id = 1);
    }
}
