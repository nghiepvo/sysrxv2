﻿using System;
using System.Collections.Generic;
using System.Linq;
using Framework.DomainModel.Entities.Common;
using Framework.DomainModel.Interfaces;
using Framework.DomainModel.ValueObject;
using Solr.DomainModel;
using SolrNet;
using SolrNet.Commands.Parameters;
using SolrNet.Exceptions;

namespace Solr.ServiceLayer
{
    public class SolrReferralMasterService<TEntity,TGridEntity> : SolrMasterFileService<TEntity>
        where TEntity : SolrEntity
        where TGridEntity:ReadOnlyGridVo
    {
        protected Dictionary<string, string> ListParseColumnInGridWithColumnInSolrWithSort;
        protected Dictionary<string, string> ListParseColumnInGridWithColumnInSolr;
        protected string KeyIdInSolr { get; set; }
        protected override List<SortOrder> BuildSortExpression(IQueryInfo queryInfo)
        {
            if (queryInfo.Sort == null || queryInfo.Sort.Count == 0)
            {
                queryInfo.Sort = new List<Sort> { new Sort { Field = "Id", Dir = "desc" } };
            }
            var result = new List<SortOrder>();
            foreach (var sort in queryInfo.Sort)
            {
                sort.Field = ParseColumnInGridToColumnInSolrWithSort(sort.Field);
                if (sort.Dir == "desc")
                {
                    var objAdd = new SortOrder(sort.Field.ToLower(), Order.DESC);
                    result.Add(objAdd);
                }
                else
                {
                    var objAdd = new SortOrder(sort.Field.ToLower(), Order.ASC);
                    result.Add(objAdd);
                }
            }
            return result;
        }

        private string ParseColumnInGridToColumnInSolrWithSort(string columnInGrid)
        {
            return ListParseColumnInGridWithColumnInSolrWithSort[columnInGrid].ToLower();
        }

        private string ParseColumnInGridToColumnInSolr(string columnInGrid)
        {
            return ListParseColumnInGridWithColumnInSolr[columnInGrid].ToLower();
        }

        private ISolrQuery BuildFilterQueryWithNumber(Filter filter)
        {
            var listFilterChild = new List<ISolrQuery>();
            foreach (var itemFilter in filter.FieldFilters)
            {
                int value;
                int.TryParse(itemFilter.Value, out value);
                ISolrQuery queryItemSolr = new SolrQueryByField(ParseColumnInGridToColumnInSolr(filter.Field),
                            itemFilter.Value);
                switch (itemFilter.Operation)
                {
                    case Operator.Equals:
                        queryItemSolr = new SolrQueryByField(ParseColumnInGridToColumnInSolr(filter.Field),
                            itemFilter.Value);
                        break;
                    case Operator.NotEquals:
                        queryItemSolr = new SolrQueryByField("-" + ParseColumnInGridToColumnInSolr(filter.Field),
                            itemFilter.Value);
                        break;
                    case Operator.GreaterThan:
                        queryItemSolr = new SolrQueryByRange<int>(ParseColumnInGridToColumnInSolr(filter.Field),
                            value, int.MaxValue, false);
                        break;
                    case Operator.GreaterThanOrEqual:
                        queryItemSolr = new SolrQueryByRange<int>(ParseColumnInGridToColumnInSolr(filter.Field),
                            value, int.MaxValue, true);
                        break;
                    case Operator.LessThan:
                        queryItemSolr = new SolrQueryByRange<int>(ParseColumnInGridToColumnInSolr(filter.Field),
                           int.MinValue, value, false);
                        break;
                    case Operator.LessThanOrEqual:
                        queryItemSolr = new SolrQueryByRange<int>(ParseColumnInGridToColumnInSolr(filter.Field),
                             int.MinValue, value, true);
                        break;
                }
                listFilterChild.Add(queryItemSolr);
            }
            if (listFilterChild.Count == 1)
            {
                return listFilterChild[0];
            }
            if (listFilterChild.Count > 1)
            {
                var arrFilter = listFilterChild.ToArray();
                return filter.Logical == Logic.And ? new SolrMultipleCriteriaQuery(arrFilter, "AND")
                    : new SolrMultipleCriteriaQuery(arrFilter, "OR");
            }
            return new SolrQuery(filter.Field + ":*");
        }
        private ISolrQuery BuildFilterQueryWithString(Filter filter)
        {
            var listFilterChild = new List<ISolrQuery>();
            foreach (var itemFilter in filter.FieldFilters)
            {
                ISolrQuery queryItemSolr = new SolrQueryByField(ParseColumnInGridToColumnInSolr(filter.Field),
                            itemFilter.Value);
                switch (itemFilter.Operation)
                {
                    case Operator.Equals:
                        queryItemSolr = new SolrQueryByField(ParseColumnInGridToColumnInSolr(filter.Field),
                            itemFilter.Value);
                        break;
                    case Operator.NotEquals:
                        queryItemSolr = new SolrQueryByField("-" + ParseColumnInGridToColumnInSolr(filter.Field),
                            itemFilter.Value);
                        break;
                    case Operator.StartsWith:
                        queryItemSolr = new SolrQuery(ParseColumnInGridToColumnInSolr(filter.Field) + ":" +
                            itemFilter.Value + "*");
                        break;
                    case Operator.Contains:
                        queryItemSolr = new SolrQuery(ParseColumnInGridToColumnInSolr(filter.Field) + ":" +
                            "*" + itemFilter.Value + "*");
                        break;
                    case Operator.NotContains:
                        queryItemSolr = new SolrQuery("-" + ParseColumnInGridToColumnInSolr(filter.Field) + ":" +
                            "*" + itemFilter.Value + "*");
                        break;
                    case Operator.EndsWith:
                        queryItemSolr = new SolrQuery(ParseColumnInGridToColumnInSolr(filter.Field) + ":" +
                            "*" + itemFilter.Value);
                        break;
                }
                listFilterChild.Add(queryItemSolr);
            }
            if (listFilterChild.Count == 1)
            {
                return listFilterChild[0];
            }
            if (listFilterChild.Count >1)
            {
                var arrFilter = listFilterChild.ToArray();
                return filter.Logical == Logic.And ? new SolrMultipleCriteriaQuery(arrFilter, "AND")
                    : new SolrMultipleCriteriaQuery(arrFilter, "OR");
            }
            return new SolrQuery(filter.Field + ":*");
        }
        private ISolrQuery BuildFilterQueryWithDateTime(Filter filter)
        {
            var listFilterChild = new List<ISolrQuery>();
            foreach (var itemFilter in filter.FieldFilters)
            {
                DateTime value;
                DateTime.TryParse(itemFilter.Value.Replace("Z", ""), out value);
                ISolrQuery queryItemSolr = new SolrQueryByField(ParseColumnInGridToColumnInSolr(filter.Field),
                            itemFilter.Value);
                switch (itemFilter.Operation)
                {
                    case Operator.Equals:
                        queryItemSolr = new SolrQueryByRange<DateTime>(ParseColumnInGridToColumnInSolr(filter.Field),
                            value, value.AddMinutes(1).AddMilliseconds(-1), true);
                        break;
                    case Operator.NotEquals:
                        queryItemSolr = new SolrQueryByRange<DateTime>("-" + ParseColumnInGridToColumnInSolr(filter.Field),
                            value, value.AddMinutes(1).AddMilliseconds(-1), true);
                        break;
                    case Operator.GreaterThan:
                        queryItemSolr = new SolrQueryByRange<DateTime>(ParseColumnInGridToColumnInSolr(filter.Field),
                            value.AddMinutes(1).AddMilliseconds(-1), DateTime.MaxValue, false);
                        break;
                    case Operator.GreaterThanOrEqual:
                        queryItemSolr = new SolrQueryByRange<DateTime>(ParseColumnInGridToColumnInSolr(filter.Field),
                            value, DateTime.MaxValue, true);
                        break;
                    case Operator.LessThan:
                        queryItemSolr = new SolrQueryByRange<DateTime>(ParseColumnInGridToColumnInSolr(filter.Field),
                           DateTime.MinValue, value, false);
                        break;
                    case Operator.LessThanOrEqual:
                        queryItemSolr = new SolrQueryByRange<DateTime>(ParseColumnInGridToColumnInSolr(filter.Field),
                             DateTime.MinValue, value, true);
                        break;
                }
                listFilterChild.Add(queryItemSolr);
            }
            if (listFilterChild.Count == 1)
            {
                return listFilterChild[0];
            }
            if (listFilterChild.Count >1)
            {
                var arrFilter = listFilterChild.ToArray();
                return filter.Logical == Logic.And ? new SolrMultipleCriteriaQuery(arrFilter, "AND")
                    : new SolrMultipleCriteriaQuery(arrFilter, "OR");
            }
            return new SolrQuery(filter.Field + ":*");
        }
        public override List<ISolrQuery> BuildFilterQuery(IQueryInfo queryInfo)
        {
            var result = new List<ISolrQuery>();

            if (queryInfo.Filters != null && queryInfo.Filters.Count > 0)
            {
                result = new List<ISolrQuery>();
                foreach (var filter in queryInfo.Filters)
                {
                    switch (filter.FieldType)
                    {
                        case TypeDataKendo.Number:
                            result.Add(BuildFilterQueryWithNumber(filter));
                            break;
                        case TypeDataKendo.Date:
                        case TypeDataKendo.DateTime:
                            result.Add(BuildFilterQueryWithDateTime(filter));
                            break;
                        case TypeDataKendo.String:
                            result.Add(BuildFilterQueryWithString(filter));
                            break;
                    }
                }
            }
            // Check for get data with referral
            var referralQueryInfo = queryInfo as ReferralQueryInfo;
            if (referralQueryInfo != null)
            {
                if (referralQueryInfo.DueDateFrom != null && referralQueryInfo.DueDateTo != null)
                {
                   var queryItemSolr = new SolrQueryByRange<DateTime>("duedate",
                            referralQueryInfo.DueDateFrom.GetValueOrDefault(), referralQueryInfo.DueDateTo.GetValueOrDefault(), true);
                   result.Add(queryItemSolr);
                }
            }
            return result;
        }

        public override AbstractSolrQuery SearchStringForGetData(IQueryInfo queryInfo)
        {
            var result = SolrQuery.All;
            var condition = "";
            switch (queryInfo.TypeWithUser)
            {
                case TypeWithUserQueryEnum.Current:
                    condition = "assigntoid:" + queryInfo.CreatedBy;
                    break;
                case TypeWithUserQueryEnum.Other:
                    condition = "-assigntoid != " + queryInfo.CreatedBy;
                    break;
            }
            if (!string.IsNullOrEmpty(condition))
            {
                result = new SolrQuery(condition);
            }
            return result;
        }

        public virtual dynamic GetDataPrint(List<int> selectedIds, IQueryInfo queryInfo)
        {
            if (Solr == null)
            {
                return null;
            }
            if (selectedIds.Count(o => o != 0) == 0)
            {
                return new { Data = new List<TGridEntity>(), TotalRowCount = 0 };
            }

            var query = SolrQuery.All;
            var filterQueryListTemp = new List<ISolrQuery>();
            foreach (var id in selectedIds.Where(o => o != 0))
            {
                ISolrQuery queryItemSolr = new SolrQueryByField(KeyIdInSolr.ToLower(), id.ToString());
                filterQueryListTemp.Add(queryItemSolr);
            }
            var filterQueryList = new List<ISolrQuery> { new SolrMultipleCriteriaQuery(filterQueryListTemp, "OR") };
            // Caculate for search string
            SolrQueryResults<TEntity> countItemQuery;
            try
            {
                countItemQuery = Solr.Query(query, new QueryOptions
                {
                    Rows = 0,
                    FilterQueries = filterQueryList
                });
            }
            catch (SolrConnectionException)
            {
                return null;
            }

            queryInfo.TotalRecords = countItemQuery.NumFound;
            var data = Solr.Query(query, new QueryOptions
            {
                OrderBy = BuildSortExpression(queryInfo),
                Start = queryInfo.Skip,
                Rows = queryInfo.Take,
                FilterQueries = filterQueryList
            });
            var dataGrid = GetDataForGridWithEntity(data);
            return new { Data = dataGrid, TotalRowCount = queryInfo.TotalRecords };
        }
    }
}