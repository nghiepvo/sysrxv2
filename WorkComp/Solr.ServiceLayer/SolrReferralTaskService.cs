﻿using System.Collections.Generic;
using System.Linq;
using Framework.DomainModel.ValueObject;
using Framework.Utility;
using Solr.DomainModel;
using Solr.ServiceLayer.Interfaces;

namespace Solr.ServiceLayer
{
    public class SolrReferralTaskService : SolrReferralMasterService<SolrReferralTask,ReferralTaskParentGridVo>, ISolrReferralTaskService
    {
        public SolrReferralTaskService()
        {
            DisplayColumnForCombobox = "title";
            KeyIdInSolr = "referraltaskid";
            ListParseColumnInGridWithColumnInSolrWithSort = new Dictionary<string, string>
            {
                {"Id","ReferralTaskId"},
                {"ReferralId","ReferralId"},
                {"Title","Title_Sort"},
                {"AssignTo","AssignToName_Sort"},
                {"Jurisdiction","Jurisdiction_Sort"},
                {"DueDate","DueDate"},
                {"Claimant","ClaimantName_Sort"},
                {"PatientState","PatientState_Sort"},
                {"CreatedDate","CreatedDate"},
                {"StartDate","StartDate"},
                {"Status","Status"},
                 {"AssignToId","AssignToId"},
                 {"ClaimNumberId","ClaimNumberId"},
                 {"ClaimantId","ClaimantId"}
            };
            ListParseColumnInGridWithColumnInSolr = new Dictionary<string, string>
            {
                {"Id","ReferralTaskId"},
                {"ReferralId","ReferralId"},
                {"Title","Title_Sort"},
                {"AssignTo","AssignToName"},
                {"Jurisdiction","Jurisdiction"},
                {"DueDate","DueDate"},
                {"Claimant","ClaimantName"},
                {"PatientState","PatientState"},
                {"CreatedDate","CreatedDate"},
                {"StartDate","StartDate"},
                {"Status","Status"},
                 {"AssignToId","AssignToId"},
                 {"ClaimNumberId","ClaimNumberId"},
                 {"ClaimantId","ClaimantId"}
            };
        }
        public override IEnumerable<ReadOnlyGridVo> GetDataForGridWithEntity(IList<SolrReferralTask> listData)
        {
            var finalResult = listData.Select(x => new ReferralTaskParentGridVo
            {
                Status = x.Status,
                Title = x.Title,
                ReferralId = x.ReferralId.GetValueOrDefault(),
                Jurisdiction=x.Jurisdiction,
                Claimant = x.ClaimantName,
                PatientState = x.PatientState,
                StartDateDateTime = x.StartDate.GetValueOrDefault(),
                DueDateDateTime = x.DueDate.GetValueOrDefault(),
                AssignTo = x.AssignToName,
                CreatedDateDateTime = x.CreatedDate.GetValueOrDefault(),
                Id = x.Id.ParseToInt(),
                CancelDate = x.CancelDate,
                CompletedDate = x.CompletedDate
            });

            return finalResult;
        }

        public override IEnumerable<LookupItemVo> GetDataForComboBox(IList<SolrReferralTask> listData)
        {
            var finalResult = listData.Select(x => new LookupItemVo
            {
                KeyId = x.Id.ParseToInt(),
                DisplayName = x.Title,
            });

            return finalResult;
        }

       
    }
}