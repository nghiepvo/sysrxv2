﻿using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.Linq;
using Framework.DomainModel.Entities.Common;
using Framework.DomainModel.Interfaces;
using Framework.DomainModel.ValueObject;
using Framework.Utility;
using Solr.DomainModel;
using Solr.ServiceLayer.Interfaces;
using SolrNet;

namespace Solr.ServiceLayer
{
    public class SolrClaimNumberService : SolrMasterFileService<SolrClaimNumber>, ISolrClaimNumberService
    {
        public SolrClaimNumberService()
        {
            SearchColumns = new Collection<string> { "name", "claimantname", "adjustername", "branchname", "payername", "doi", "jurisdiction", "status" };
            DisplayColumnForCombobox = "name";
        }
        public override IEnumerable<ReadOnlyGridVo> GetDataForGridWithEntity(IList<SolrClaimNumber> listData)
        {
            var finalResult = listData.Select(x => new ClaimNumberGridVo()
            {
                Name = x.Name,
                Claimant = x.ClaimantName,
                Adjuster = x.AdjusterName,
                Payer = x.PayerName,
                Branch = x.BranchName,
                Doi = x.Doi != null ? x.Doi.GetValueOrDefault().ToShortDateString() : "",
                State = x.Jurisdiction,
                StatusName = x.Status,
                Id = x.ClaimNumberId,
                SpecialInstructions = x.SpecialInstructions,
            });

            return finalResult;
        }
        
        public override IEnumerable<LookupItemVo> GetDataForComboBox(IList<SolrClaimNumber> listData)
        {
            var finalResult = listData.Select(x => new LookupItemVo()
            {
                KeyId = x.ClaimNumberId,
                DisplayName = x.Name,
            });

            return finalResult;
        }

        protected override List<SortOrder> BuildSortExpression(IQueryInfo queryInfo)
        {
            //if (queryInfo.Sort == null || queryInfo.Sort.Count == 0)
            //{
            //    queryInfo.Sort = new List<Sort> { new Sort { Field = "id", Dir = "desc" } };
            //}
            var result = new List<SortOrder>();
            //foreach (var sort in queryInfo.Sort)
            //{
            //    if (sort.Field == "Claimant")
            //    {
            //        sort.Field = "ClaimantName";
            //    }
            //    else if (sort.Field == "Adjuster")
            //    {
            //        sort.Field = "AdjusterName";
            //    }
            //    else if (sort.Field == "Payer")
            //    {
            //        sort.Field = "PayerName";
            //    }
            //    else if (sort.Field == "Branch")
            //    {
            //        sort.Field = "BranchName";
            //    }
            //    else if (sort.Field == "State")
            //    {
            //        sort.Field = "Jurisdiction";
            //    }
            //    else if (sort.Field == "StatusName")
            //    {
            //        sort.Field = "Status";
            //    }
            //    if (sort.Dir == "desc")
            //    {
            //        var objAdd = new SortOrder(sort.Field.ToLower(), Order.DESC);
            //        result.Add(objAdd);
            //    }
            //    else
            //    {
            //        var objAdd = new SortOrder(sort.Field.ToLower(), Order.ASC);
            //        result.Add(objAdd);
            //    }
            //}
            var objAdd = new SortOrder("claimnumberid", Order.DESC);
            result.Add(objAdd);
            return result;
        }

        protected override List<SortOrder> BuildSortOrders()
        {
            //new List<SortOrder>{new SortOrder("id", Order.ASC)} ,
            return new List<SortOrder>() { new SortOrder(DisplayColumnForCombobox, Order.ASC) };
        }
    }
}