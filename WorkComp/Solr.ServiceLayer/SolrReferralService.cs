﻿using System.Collections.Generic;
using System.Linq;
using Framework.DomainModel.Entities.Common;
using Framework.DomainModel.Interfaces;
using Framework.DomainModel.ValueObject;
using Framework.Utility;
using Microsoft.Practices.ServiceLocation;
using Solr.DomainModel;
using Solr.ServiceLayer.Interfaces;
using SolrNet;
using SolrNet.Commands.Parameters;
using SolrNet.Exceptions;

namespace Solr.ServiceLayer
{
    public class SolrReferralService : SolrReferralMasterService<SolrReferral,ReferralParentGridVo>, ISolrReferralService
    {
        private readonly ISolrOperations<SolrReferralTask> _referralTaskSolrService;
        private readonly ISolrOperations<SolrReferral> _referralSolrService;
        public SolrReferralService()
        {
            KeyIdInSolr = "referralid";
            _referralTaskSolrService = ServiceLocator.Current.GetInstance<ISolrOperations<SolrReferralTask>>();
            _referralSolrService = ServiceLocator.Current.GetInstance<ISolrOperations<SolrReferral>>();
            DisplayColumnForCombobox = "id";
            ListParseColumnInGridWithColumnInSolrWithSort=new Dictionary<string, string>
            {
                {"Id","ReferralId"},
                {"Status","Status_Sort"},
                {"ControlNumber","ControlNumber_Sort"},
                {"ProductType","ProductType_Sort"},
                {"DueDate","DueDate"},
                {"TreatingPhysicianName","TreatingPhysicianName_Sort"},
                {"TreatingPhysicianPhone","TreatingPhysicianPhone_Sort"},
                {"AssignTo","AssignToName_Sort"},
                {"Payer","PayerName_Sort"},
                {"Jurisdiction","Jurisdiction_Sort"},
                {"Claimant","ClaimantName_Sort"},
                {"ClaimNumber","ClaimNumber_Sort"},
                {"PatientState","PatientState_Sort"},
                {"Doi","Doi"},
                {"CreatedDate","CreatedDate"},
                {"CreatedBy","CreatedByName_Sort"},
            };
            ListParseColumnInGridWithColumnInSolr = new Dictionary<string, string>
            {
                {"Id","ReferralId"},
                {"Status","Status"},
                {"ControlNumber","ControlNumber"},
                {"ProductType","ProductType"},
                {"DueDate","DueDate"},
                {"TreatingPhysicianName","TreatingPhysicianName"},
                {"TreatingPhysicianPhone","TreatingPhysicianPhone"},
                {"AssignTo","AssignToName"},
                {"Payer","PayerName"},
                {"Jurisdiction","Jurisdiction"},
                {"Claimant","ClaimantName"},
                {"ClaimNumber","ClaimNumber"},
                {"PatientState","PatientState"},
                {"Doi","Doi"},
                {"CreatedDate","CreatedDate"},
                {"CreatedBy","CreatedByName"},
            };
        }

        private int GetTotalTaskForReferral(int idReferral)
        {
            var query = new SolrQuery("referralid:" + idReferral);
            var countItemQuery = _referralTaskSolrService.Query(query, new QueryOptions
            {
                Rows = 0
            });
            return countItemQuery.NumFound;
        }
        private int GetTotalTaskCompletedForReferral(int idReferral)
        {
            var query = new SolrQuery("(referralid:" + idReferral + ")" + " AND " + "(status:completed)");
            var countItemQuery = _referralTaskSolrService.Query(query, new QueryOptions
            {
                Rows = 0
            });
            return countItemQuery.NumFound;
        }

        public override IEnumerable<ReadOnlyGridVo> GetDataForGridWithEntity(IList<SolrReferral> listData)
        {
            var finalResult = listData.Select(x => new ReferralParentGridVo
            {
                Status = x.Status,
                TotalNote = x.TotalNote,
                TotalTask = GetTotalTaskForReferral(x.ReferralId),
                TotalTaskCompleted = GetTotalTaskCompletedForReferral(x.ReferralId),
                ControlNumber = x.ControlNumber,
                ProductType = x.ProductType,
                StartDateDateTime = x.RecievedDate.GetValueOrDefault(),
                DueDateDateTime = x.DueDate.GetValueOrDefault(),
                IsCollectionSite=x.IsCollectionSite,
                TreatingPhysicianName=x.TreatingPhysicianName,
                TreatingPhysicianPhone=x.TreatingPhysicianPhone.ApplyFormatPhone(),
                AssignTo=x.AssignToName,
                Payer=x.PayerName,
                Jurisdiction=x.Jurisdiction,
                Rush=x.Rush,
                Claimant=x.ClaimantName,
                ClaimNumber=x.ClaimNumber,
                PatientState=x.PatientState,
                DoiDateTime=x.Doi.GetValueOrDefault(),
                CreatedDateDateTime=x.CreatedDate.GetValueOrDefault(),
                CreatedBy=x.CreatedByName,
                Id = x.Id.ParseToInt(),
                CancelDate = x.CancelDate,
                CompletedDate = x.CompletedDate
            });

            return finalResult;
        }

        public override IEnumerable<LookupItemVo> GetDataForComboBox(IList<SolrReferral> listData)
        {
            var finalResult = listData.Select(x => new LookupItemVo
            {
                KeyId = x.Id.ParseToInt(),
                DisplayName = x.Id+"-"+x.ProductType,
            });

            return finalResult;
        }

        protected override List<SortOrder> BuildSortExpression(IQueryInfo queryInfo)
        {
            if (queryInfo.Sort.Count == 0)
            {
                queryInfo.Sort = new List<Sort> { new Sort { Field = "DueDate", Dir = "asc" } };
            }
            return base.BuildSortExpression(queryInfo);

        }
    }
}