﻿using Solr.DomainModel;
using Solr.ServiceLayer.Interfaces;

namespace Solr.ServiceLayer
{
    public class SolrReferralResultService : SolrMasterFileService<SolrReferralResult>, ISolrReferralResultService
    {
    }
}