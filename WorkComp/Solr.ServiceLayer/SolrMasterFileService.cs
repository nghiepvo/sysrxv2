﻿using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.Linq;
using System.Linq.Expressions;
using System.Text;
using Framework.DomainModel.Entities.Common;
using Framework.DomainModel.Interfaces;
using Framework.DomainModel.ValueObject;
using Microsoft.Practices.ServiceLocation;
using Solr.DomainModel;
using Solr.ServiceLayer.Interfaces;
using SolrNet;
using SolrNet.Commands.Parameters;
using SolrNet.Exceptions;

namespace Solr.ServiceLayer
{
    public class SolrMasterFileService<TEntity> : ISolrMasterFileService<TEntity>
        where TEntity : SolrEntity
    {
        protected readonly ISolrOperations<TEntity> Solr;

        public SolrMasterFileService()
        {
            Solr = ServiceLocator.Current.GetInstance<ISolrOperations<TEntity>>();
        }
        public TEntity GetById(int id)
        {
            var data = Solr.Query(new SolrQuery("id:" + id), new QueryOptions
            {
                Start = 0,
                Rows = 1,
            });
            return data.FirstOrDefault();
        }

        public TEntity Add(TEntity model)
        {
            if (Solr == null)
            {
                return null;
            }
            Solr.Add(model);
            return model;
        }

        public TEntity Update(TEntity model)
        {
            if (Solr == null)
            {
                return null;
            }
            Solr.Add(model);
            return model;
        }

        public void Delete(TEntity model)
        {
            if (Solr != null)
            {
                Solr.Delete(model);
            }
        }

        public void Commit()
        {
            if (Solr != null)
            {
                Solr.Commit();
            }
        }

        public void DeleteById(int id)
        {
            if (Solr != null)
            {
                Solr.Delete(id.ToString());
            }
        }

        public IList<TEntity> ListAll()
        {
            return Solr == null ? null : Solr.Query(SolrQuery.All);
        }

        public int Count(Expression<Func<TEntity, bool>> @where = null)
        {
            throw new NotImplementedException();
        }

        public bool CheckExist(Expression<Func<TEntity, bool>> @where = null)
        {
            throw new NotImplementedException();
        }

        public IList<TEntity> GetByField(SolrQueryByField predicate)
        {
            return Solr == null ? null : Solr.Query(predicate);
        }

        public void InsertOrUpdate(TEntity entity)
        {
            throw new NotImplementedException();
        }

        public List<int> DeleteById(IEnumerable<int> ids)
        {
            foreach (var id in ids)
            {
                DeleteById(id);
            }
            return ids.ToList();
        }

        public void DeleteAll(IEnumerable<TEntity> entities)
        {
            if (Solr == null)
            {
                return;
            }
            Solr.Delete(entities);
        }

        public void DeleteAllByField(SolrQueryByField predicate)
        {
            if (Solr == null)
            {
                return;
            }
            var entities = GetByField(predicate);
            Solr.Delete(entities);
        }

        public virtual dynamic GetDataForGridMasterfile(IQueryInfo queryInfo)
        {
            if (Solr == null)
            {
                return null;
            }
            var query = SearchStringForGetData(queryInfo);
            var filterQuery = BuildFilterQuery(queryInfo);
            // Caculate for search string

            SolrQueryResults<TEntity> countItemQuery;
            try
            {
               countItemQuery= Solr.Query(query, new QueryOptions
                {
                    Rows = 0,
                    FilterQueries = filterQuery
                });
            }
            catch (SolrConnectionException ex)
            {
                return null;
            }
           
            queryInfo.TotalRecords = countItemQuery.NumFound;
            var data = Solr.Query(query, new QueryOptions
            {
                OrderBy = BuildSortExpression(queryInfo),
                Start = queryInfo.Skip,
                Rows = queryInfo.Take,
                FilterQueries = filterQuery
            });
            var dataGrid = GetDataForGridWithEntity(data);
            return new { Data = dataGrid, TotalRowCount = queryInfo.TotalRecords };
        }

        public List<LookupItemVo> GetLookup(LookupQuery query)
        {
            if (Solr == null)
            {
                return null;
            }
            var filterQuery = BuildLookupCondition(query);
            try
            {
                var queryOptions = new QueryOptions
                {
                    //OrderBy = new List<SortOrder>{new SortOrder("id", Order.ASC)} ,
                    Start = 0,
                    Rows = query.Take,
                    FilterQueries = filterQuery
                };

                var sortOrders = BuildSortOrders();

                if (sortOrders.Count > 0)
                {
                    queryOptions.OrderBy = sortOrders;
                }
                var data = Solr.Query(SolrQuery.All, queryOptions);
                var dataGrid = GetDataForComboBox(data);
                return dataGrid.ToList();
            }
            catch (SolrConnectionException ex)
            {
                return null;
            }
        }

        public virtual List<ISolrQuery> BuildFilterQuery(IQueryInfo query)
        {
            return null;
        }
        public virtual IEnumerable<ReadOnlyGridVo> GetDataForGridWithEntity(IList<TEntity> listData)
        {
            return null;
        }

        public virtual IEnumerable<LookupItemVo> GetDataForComboBox(IList<TEntity> listData)
        {
            return null;
        }
        public Collection<string> SearchColumns { get; set; }
        public virtual AbstractSolrQuery SearchStringForGetData(IQueryInfo queryInfo)
        {
            var result = SolrQuery.All;
            if (!string.IsNullOrEmpty(queryInfo.SearchString))
            {
                queryInfo.SearchString = queryInfo.SearchString.Replace(' ', '+');
                queryInfo.SearchString = Encoding.UTF8.GetString(Convert.FromBase64String(queryInfo.SearchString));
                queryInfo.ParseParameters(queryInfo.SearchString);
                var listStrQuery = new List<string>();
                if (!string.IsNullOrEmpty(queryInfo.SearchTerms))
                {
                    var keyword = queryInfo.SearchTerms;
                    var listKeySearch = keyword.Split(' ');
                    foreach (var item in SearchColumns)
                    {
                        var keySolr = "";
                        var countKeySearch = listKeySearch.Count();
                        for (var i = 0; i < countKeySearch; i++)
                        {
                            var key = listKeySearch[i];
                            keySolr += item + ":*" + key + "*";
                            if (i < (countKeySearch - 1))
                            {
                                keySolr += " AND ";
                            }
                        }
                        keySolr = "(" + keySolr + ")";
                        listStrQuery.Add(keySolr);
                    }
                    var strQuery = String.Join(" OR ", listStrQuery.ToArray());
                    result = new SolrQuery(strQuery);
                }
            }
            return result;
        }

        protected virtual List<SortOrder> BuildSortExpression(IQueryInfo queryInfo)
        {
            if (queryInfo.Sort == null || queryInfo.Sort.Count == 0)
            {
                queryInfo.Sort = new List<Sort> { new Sort { Field = "id", Dir = "desc" } };
            }
            var result = new List<SortOrder>();
            foreach (var sort in queryInfo.Sort)
            {
                if (sort.Dir == "desc")
                {
                    var objAdd = new SortOrder(sort.Field.ToLower(), Order.DESC);
                    result.Add(objAdd);
                }
                else
                {
                    var objAdd = new SortOrder(sort.Field.ToLower(), Order.ASC);
                    result.Add(objAdd);
                }
            }
            return result;
        }

        public string DisplayColumnForCombobox { get; set; }
        protected virtual List<ISolrQuery> BuildLookupCondition(LookupQuery query)
        {
            var listFilterChild = new List<ISolrQuery>();
            if (!string.IsNullOrEmpty(query.Query))
            {
                var keyword = query.Query;
                var listKeySearch = keyword.Split(' ');
                var keySolr = "";
                var countKeySearch = listKeySearch.Count();
                for (var i = 0; i < countKeySearch; i++)
                {
                    var key = listKeySearch[i];

                    //keySolr += DisplayColumnForCombobox + ":*" + key + "*";
                    keySolr += "((" + DisplayColumnForCombobox + ":\"" + key + "\") OR (" + DisplayColumnForCombobox + ":" + key + "*))";
                    if (i < (countKeySearch - 1))
                    {
                        keySolr += " AND ";
                    }
                }
                listFilterChild.Add(new SolrQuery(keySolr));
            }
            if (query.HierachyItems != null)
            {
                foreach (var parentItem in query.HierachyItems.Where(parentItem => parentItem.Value != string.Empty
                    && parentItem.Value != "-1"
                    && parentItem.Value != "0"
                    && !parentItem.IgnoredFilter
                    &&parentItem.Name.ToLower()!="branchid"))
                {
                    var keySolr = "("+parentItem.Name.ToLower() + ":" + parentItem.Value+")";
                    listFilterChild.Add(new SolrQuery(keySolr));
                }
            }
            return listFilterChild;
        }

        protected virtual List<SortOrder> BuildSortOrders()
        {
            return new List<SortOrder>();
        }
    }
}