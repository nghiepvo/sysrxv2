﻿using Framework.Service.Translation;

namespace Framework.DataAnnotations
{
    public class LocalizeFaxAttribute : LocalizePhoneAttribute
    {
        public override string FormatErrorMessage(string name)
        {
            return string.Format(SystemMessageLookup.GetMessage("FaxValid"), name);
        }
    }
}
