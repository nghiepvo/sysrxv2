﻿using System.ComponentModel.DataAnnotations;
using Framework.Service.Translation;

namespace Framework.DataAnnotations
{
    public class LocalizeEmailAddressAttribute : RegularExpressionAttribute
    {
        public LocalizeEmailAddressAttribute()
            : base(@"[a-zA-Z0-9._%+-]+@[a-zA-Z0-9.-]+\.[a-zA-Z]{2,4}")
        {
            
        }

        public override string FormatErrorMessage(string name)
        {
            return string.Format(SystemMessageLookup.GetMessage("EmailValid"), name);
        }
    }
}
