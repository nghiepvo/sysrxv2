﻿using System.ComponentModel.DataAnnotations;
using Framework.Service.Translation;

namespace Framework.DataAnnotations
{
    public class LocalizePhoneAttribute : RegularExpressionAttribute
    {
        public LocalizePhoneAttribute()
            : base(@"(^\s*$)|(^\d{10}$)|(^[(]\d{3}[)]\s\d{3}[-]\d{4}$)")
        {
            
        }
        public override string FormatErrorMessage(string name)
        {
            return string.Format(SystemMessageLookup.GetMessage("PhoneValid"), name);
        }
    }

   
}
