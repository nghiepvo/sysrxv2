﻿using Framework.BusinessRule;
using Framework.DomainModel.Entities;
using Repositories.Interfaces;
using ServiceLayer.Interfaces;

namespace ServiceLayer
{
    public class CommentService : MasterFileService<Comment>, ICommentService
    {
        private readonly ICommentRepository _commentRepository;
        public CommentService(ICommentRepository commentRepository, IBusinessRuleSet<Comment> businessRuleSet = null)
            : base(commentRepository, commentRepository, businessRuleSet)
        {
            _commentRepository = commentRepository;
        }

        public System.Collections.Generic.IList<Comment> GetCommentWhenChangeReferral(int? referralTaskId)
        {
            return _commentRepository.GetCommentWhenChangeReferral(referralTaskId);
        }
    }
}