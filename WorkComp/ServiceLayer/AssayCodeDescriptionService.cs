﻿using Framework.BusinessRule;
using Framework.DomainModel.Entities;
using Repositories.Interfaces;
using ServiceLayer.Interfaces;

namespace ServiceLayer
{
    public class AssayCodeDescriptionService : MasterFileService<AssayCodeDescription>, IAssayCodeDescriptionService
    {
        private readonly IAssayCodeDescriptionRepository _assayCodeDescriptionRepository;

        public AssayCodeDescriptionService(IAssayCodeDescriptionRepository assayCodeDescriptionRepository, IBusinessRuleSet<AssayCodeDescription> businessRuleSet = null)
            : base(assayCodeDescriptionRepository, assayCodeDescriptionRepository, businessRuleSet)
        {
            _assayCodeDescriptionRepository = assayCodeDescriptionRepository;
        }
    }
}