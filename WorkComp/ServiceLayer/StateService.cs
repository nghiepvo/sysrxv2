﻿using Framework.BusinessRule;
using Framework.DomainModel.Entities;
using Repositories.Interfaces;
using ServiceLayer.Interfaces;

namespace ServiceLayer
{
    public class StateService : MasterFileService<State>, IStateService
    {
        private readonly IStateRepository _stateRepository;
        public StateService(IStateRepository stateRepository, IBusinessRuleSet<State> businessRuleSet = null)
            : base(stateRepository, stateRepository, businessRuleSet)
        {
            _stateRepository = stateRepository;
        }

    }
}