﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Linq.Expressions;
using System.Transactions;
using Framework.BusinessRule;
using Framework.DomainModel.Entities;
using Framework.DomainModel.Entities.Common;
using Framework.DomainModel.Interfaces;
using Framework.DomainModel.ValueObject;
using Framework.Mapping;
using Repositories.Interfaces;
using ServiceLayer.Interfaces;
using Solr.DomainModel;
using Solr.ServiceLayer.Interfaces;
using SolrNet;

namespace ServiceLayer
{
    public class NpiNumberService : MasterFileService<NpiNumber>, INpiNumberService
    {
        private readonly INpiNumberRepository _npiNumberRepository;
        private readonly ISolrNpiNumberService _npiNumberSolrService;
        private readonly ISolrReferralService _referralSolrService;

        public NpiNumberService(INpiNumberRepository npiNumberRepository,ISolrNpiNumberService npiNumberSolrService,
                                ISolrReferralService referralSolrService, IBusinessRuleSet<NpiNumber> businessRuleSet = null)
            : base(npiNumberRepository, npiNumberRepository, businessRuleSet)
        {
            _npiNumberRepository = npiNumberRepository;
            _npiNumberSolrService = npiNumberSolrService;
            _referralSolrService = referralSolrService;
        }

       

        public InfoWhenChangeTreatingPhysicianInReferral GetInfoWhenChangeTreatingPhysicianInReferral(int idTreatingPhysician)
        {
            return _npiNumberRepository.GetInfoWhenChangeTreatingPhysicianInReferral(idTreatingPhysician);
        }

        public override NpiNumber Add(NpiNumber model)
        {
            using (var objTransaction = new TransactionScope())
            {
                var objAddItem = base.Add(model);
                if (objAddItem.Id > 0)
                {
                    //Write to solr
                    var objSolrItem = objAddItem.MapTo<SolrNpiNumber>();
                    // Delete all item which have same code
                    _npiNumberSolrService.Add(objSolrItem);
                    _npiNumberSolrService.Commit();
                }
                objTransaction.Complete();
                return objAddItem;
            }
        }

        public override NpiNumber Update(NpiNumber model)
        {
            using (var objTransaction = new TransactionScope())
            {
                // Get old name
                var id = model.Id;
                var oldData = _npiNumberRepository.FirstOrDefault(o=>o.Id==id);
                if (oldData == null)
                {
                    return model;
                }
                var newName = (model.FirstName ?? "") + " " + (model.MiddleName ?? "") + " " + (model.LastName ?? "");
                var oldName = (oldData.FirstName ?? "") + " " + (oldData.MiddleName ?? "") + " " + (oldData.LastName ?? "");
                base.Update(model);
                //Write to solr
                var objSolrItem = model.MapTo<SolrNpiNumber>();
                // Delete all item which have same code
                _npiNumberSolrService.Update(objSolrItem);
                _npiNumberSolrService.Commit();
                if (newName.Trim() != oldName.Trim())
                {
                    var listReferral =
                   _referralSolrService.GetByField(new SolrQueryByField("npinumberid", model.Id.ToString())).ToList();
                    if (listReferral.Count > 0)
                    {
                        foreach (var solrReferral in listReferral)
                        {
                            solrReferral.TreatingPhysicianName =newName;
                            _referralSolrService.Update(solrReferral);
                        }
                        _referralSolrService.Commit();
                    }
                }
                
                objTransaction.Complete();
                return model;
            }
        }

        public override void DeleteById(int id)
        {
            using (var objTransaction = new TransactionScope())
            {
                base.DeleteById(id);
                //Write to solr
                _npiNumberSolrService.DeleteAllByField(new SolrQueryByField("id", id.ToString()));
                _npiNumberSolrService.Commit();
                objTransaction.Complete();
            }
        }

        public override void DeleteAll(Expression<Func<NpiNumber, bool>> @where = null)
        {
            using (var objTransaction = new TransactionScope())
            {
                // Get listId from where
                var listItemDelete = base.Get(@where);
                base.DeleteAll(@where);
                //Write to solr
                foreach (var itemDelete in listItemDelete)
                {
                    _npiNumberSolrService.DeleteAllByField(new SolrQueryByField("id", itemDelete.Id.ToString()));
                }
                _npiNumberSolrService.Commit();
                objTransaction.Complete();
            }
        }

        public override List<LookupItemVo> GetLookup(LookupQuery query, Func<NpiNumber, LookupItemVo> selector)
        {
            var objData = _npiNumberSolrService.GetLookup(query);
            var result = objData ?? base.GetLookup(query, selector);
            return result;
        }

        public List<LookupItemVo> GetLookupTreatingPhysician(LookupQuery query, Func<NpiNumber, LookupItemVo> selector)
        {
            var objData = _npiNumberSolrService.GetLookupTreatingPhysician(query);
            var result = objData ?? _npiNumberRepository.GetLookupTreatingPhysician(query, selector);
            return result;
        }

        public override dynamic GetDataForGridMasterfile(IQueryInfo queryInfo)
        {
            var objData = _npiNumberSolrService.GetDataForGridMasterfile(queryInfo);
            var result = objData ?? base.GetDataForGridMasterfile(queryInfo);
            return result;
        }
    }
}