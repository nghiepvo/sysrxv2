﻿using Framework.BusinessRule;
using Framework.DomainModel.Entities;
using Framework.DomainModel.ValueObject;
using Repositories.Interfaces;
using ServiceLayer.Interfaces;

namespace ServiceLayer
{
    public class AttorneyService : MasterFileService<Attorney>, IAttorneyService
    {
        private readonly IAttorneyRepository _attorneyRepository;

        public AttorneyService(IAttorneyRepository attorneyRepository, IBusinessRuleSet<Attorney> businessRuleSet = null)
            : base(attorneyRepository, attorneyRepository, businessRuleSet)
        {
            _attorneyRepository = attorneyRepository;
        }

        public InfoWhenChangeAttorneyInReferral GetInfoWhenChangeAttorneyInReferral(int idAttorney)
        {
            return _attorneyRepository.GetInfoWhenChangeAttorneyInReferral(idAttorney);
        }
    }
}