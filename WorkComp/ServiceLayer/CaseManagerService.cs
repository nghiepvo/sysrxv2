﻿using Framework.BusinessRule;
using Framework.DomainModel.Entities;
using Framework.DomainModel.ValueObject;
using Repositories.Interfaces;
using ServiceLayer.Interfaces;

namespace ServiceLayer
{
    public class CaseManagerService : MasterFileService<CaseManager>, ICaseManagerService
    {
        private readonly ICaseManagerRepository _caseManagerRepository;

        public CaseManagerService(ICaseManagerRepository caseManagerRepository, IBusinessRuleSet<CaseManager> businessRuleSet = null)
            : base(caseManagerRepository, caseManagerRepository, businessRuleSet)
        {
            _caseManagerRepository = caseManagerRepository;
        }

        public InfoWhenChangeCaseManagerInReferral GetInfoWhenChangeCaseManagerInReferral(int idCaseManager)
        {
            return _caseManagerRepository.GetInfoWhenChangeCaseManagerInReferral(idCaseManager);
        }
    }
}