﻿using System.Linq;
using System.Transactions;
using Framework.DomainModel.Entities;
using Framework.DomainModel.ValueObject;
using Repositories.Interfaces;
using ServiceLayer.Interfaces;
using Framework.BusinessRule;
using Solr.ServiceLayer.Interfaces;
using SolrNet;

namespace ServiceLayer
{
    public class PayerService : MasterFileService<Payer>, IPayerService
    {
        private readonly IPayerRepository _payerRepository;
        private readonly ISolrReferralService _referralSolrService;
        public PayerService(IPayerRepository payerRepository, ISolrReferralService referralSolrService, IBusinessRuleSet<Payer> businessRuleSet = null)
            : base(payerRepository, payerRepository, businessRuleSet)
        {
            _payerRepository = payerRepository;
            _referralSolrService = referralSolrService;
        }
        public override Payer Update(Payer model)
        {
            using (var objTransaction = new TransactionScope())
            {
                var id = model.Id;
                var oldData = _payerRepository.FirstOrDefault(o => o.Id == id);
                if (oldData == null)
                {
                    return model;
                }
                var oldName = oldData.Name;
                base.Update(model);
                //Write to solr
                if (oldName.Trim() != model.Name.Trim())
                {
                    var listReferral =
                   _referralSolrService.GetByField(new SolrQueryByField("payerid", model.Id.ToString())).ToList();
                    if (listReferral.Count > 0)
                    {
                        foreach (var solrReferral in listReferral)
                        {
                            solrReferral.PayerName = model.Name;
                            _referralSolrService.Update(solrReferral);
                        }
                        _referralSolrService.Commit();
                    }
                }
                objTransaction.Complete();
                return model;
            }
        }

        public LookupItemVo GetDefaulDataSource(int id = 1)
        {
            return _payerRepository.GetDefaulDataSource(id);
        }
    }
}
