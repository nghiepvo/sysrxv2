﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.IdentityModel.Services;
using System.Linq;
using System.Security.Claims;
using System.Text.RegularExpressions;
using System.Web.SessionState;
using Framework.DomainModel.Entities;
using Framework.DomainModel.Entities.Common;
using Framework.DomainModel.Interfaces;
using Framework.Exceptions;
using Framework.Service.Diagnostics;
using Framework.Service.Translation;
using Framework.Web;
using Repositories.Interfaces;
using ServiceLayer.Interfaces.Authentication;

namespace ServiceLayer.Authentication
{
    public class AuthenticationService:IAuthenticationService
    {
        private const int MaxSessionDuration = 540;
        public AuthenticationService(
                                     IWorkCompHttpContext httpContext,
                                     IClaimsManager claimsManager, IFormAuthenticationService formAuthenticationService,
                                     ISessionIDManager sessionIdManager, IDiagnosticService diagnosticService, IUserRepository userRepository)
        {
            HttpContext = httpContext;
            FormAuthenticationService = formAuthenticationService;
            ClaimsManager = claimsManager;
            SessionIdManager = sessionIdManager;
            _diagnosticService = diagnosticService;
            _userRepository = userRepository;
        }
        private readonly IDiagnosticService _diagnosticService;
        public IClaimsManager ClaimsManager { get; set; }
        public IFormAuthenticationService FormAuthenticationService { get; private set; }
        public IWorkCompHttpContext HttpContext { get; private set; }
        public ISessionIDManager SessionIdManager { get; private set; }
        private readonly IUserRepository _userRepository;
        public IWorkcompPrincipal GetCurrentUser()
        {
            var principal = (HttpContext.User as IWorkcompPrincipal);
            if (principal == null)
                FormAuthenticationService.SignOut();

            return principal;
        }

        public void SignOut()
        {
            FormAuthenticationService.SignOut();
            if (FederatedAuthentication.WSFederationAuthenticationModule != null)
            {
                FederatedAuthentication.WSFederationAuthenticationModule.SignOut();
                FederatedAuthentication.WSFederationAuthenticationModule.SignOut(true);
                FederatedAuthentication.SessionAuthenticationModule.SignOut();
            }
           
        }

        public bool SignIn(string userName, string password, bool rememberMe)
        {
            // encript pasword
            
            var claims = ClaimsManager.CreateClaims(userName, password).ToList();
            var user = ClaimsManager.ValidateWorkcompLogin(claims);

            if (user == null || !user.IsWorkcompUser)
            {
                var claimException = new InvalidClaimsException("InvalidUserAndPasswordText")
                {
                    WorkCompUserName = (user != null) ? user.UserName : string.Empty
                };
                _diagnosticService.Error(SystemMessageLookup.GetMessage("InvalidUserAndPasswordText"));
                _diagnosticService.Error("UserName:" + userName);
                throw claimException;
            }

            var principal = CreatePrincipalFromClaimsAndUser(user, claims);

            if (principal != null)
                FormAuthenticationService.SignIn(principal, true, principal.AuthToken,
                                                 DateTime.Now.AddMinutes(MaxSessionDuration));
            return true;
        }

        public User RestorePassword(string email,out string passwordRandom)
        {
            var failed = false;
            var validationResult = new List<ValidationResult>();
            User objUser = null;
            // Check email
            if (string.IsNullOrEmpty(email))
            {
                var mess = string.Format(SystemMessageLookup.GetMessage("RequiredTextResourceKey"), "Email");
                validationResult.Add(new ValidationResult(mess));
                failed = true;
            }
            else if (
                !Regex.IsMatch(email,
                    @"\A(?:[a-z0-9!#$%&'*+/=?^_`{|}~-]+(?:\.[a-z0-9!#$%&'*+/=?^_`{|}~-]+)*@(?:[a-z0-9](?:[a-z0-9-]*[a-z0-9])?\.)+[a-z0-9](?:[a-z0-9-]*[a-z0-9])?)\Z"))
            {
                var mess = string.Format(SystemMessageLookup.GetMessage("FieldInvalidText"), "Email");
                validationResult.Add(new ValidationResult(mess));
                failed = true;
            }
            else
            {
                objUser = _userRepository.FirstOrDefault(o => o.Email == email);
                if (objUser==null)
                {
                    var mess = string.Format(SystemMessageLookup.GetMessage("FieldInvalidText"), "Email");
                    validationResult.Add(new ValidationResult(mess));
                    failed = true;
                }
            }
            var result = new BusinessRuleResult(failed, "", "RestorePassword", 0, null, "RestorePasswordRule") { ValidationResults = validationResult };
            if (failed)
            {
                // Give messages on every rule that failed
                throw new BusinessRuleException("BussinessGenericErrorMessageKey", new[] { result });
            }
            // Create password
            passwordRandom = Guid.NewGuid().ToString().Replace("-", "").Substring(0, 6);
            //var password = PasswordHelper.HashString(passwordRandom, objUser.UserName);
            // Update password to database
            //objUser.Password = password;
            _userRepository.Update(objUser);
            _userRepository.Commit();
            return objUser;
        }

        /// <summary>
        ///     Update principal for security threads.
        /// </summary>
        public void UpdatePrincipal(IWorkcompPrincipal principal)
        {
            FormAuthenticationService.SetPrincipalCache(principal, principal.AuthToken, null);
        }

        protected IWorkcompPrincipal CreatePrincipalFromClaimsAndUser(User user, List<Claim> claims)
        {
            var firstOrDefault = claims.FirstOrDefault(x => x.Type == ClaimsDeclaration.AuthenticationTypeClaimType);
            if (firstOrDefault != null)
            {
                var providerClaim = firstOrDefault.Value;
                var orDefault = claims.FirstOrDefault(x => x.Type == ClaimsDeclaration.NameClaimType);
                if (orDefault != null)
                {
                    var loginName = orDefault.Value;

                    var primaryIdentity = new WorkcompIdentity(loginName, user.Id, providerClaim);

                    // create principal with primary identity
                    var returnPrincipal = new WorkcompPrincipal(primaryIdentity)
                    {
                        AuthToken = SessionIdManager.CreateSessionID(null),
                        User = user
                    };

                    return returnPrincipal;
                }
            }
            return null;
        }
    }
}
