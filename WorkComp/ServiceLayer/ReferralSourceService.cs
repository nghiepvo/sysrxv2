﻿using Framework.BusinessRule;
using Framework.DomainModel.Entities;
using Framework.DomainModel.ValueObject;
using Repositories.Interfaces;
using ServiceLayer.Interfaces;

namespace ServiceLayer
{
    public class ReferralSourceService : MasterFileService<ReferralSource>, IReferralSourceService
    {
        private readonly IReferralSourceRepository _referralSourceRepository;

        public ReferralSourceService(IReferralSourceRepository referralSourceRepository, IBusinessRuleSet<ReferralSource> businessRuleSet = null)
            : base(referralSourceRepository, referralSourceRepository, businessRuleSet)
        {
            _referralSourceRepository = referralSourceRepository;
        }

        public InfoWhenChangeReferralSourceInReferral GetInfoWhenChangeReferralSourceInReferral(int idReferral)
        {
            return _referralSourceRepository.GetInfoWhenChangeReferralSourceInReferral(idReferral);
        }
    }
}