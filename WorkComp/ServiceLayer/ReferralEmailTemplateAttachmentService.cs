﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Web;
using Framework.DomainModel.Entities;
using Framework.Utility;
using Repositories.Interfaces;
using ServiceLayer.Interfaces;

namespace ServiceLayer
{
    public sealed class ReferralEmailTemplateAttachmentService : IReferralEmailTemplateAttachmentService
    {
        private readonly IReferralEmailTemplateAttachmentRepository _referralEmailTemplateAttachmentRepository;
        public ReferralEmailTemplateAttachmentService(IReferralEmailTemplateAttachmentRepository referralEmailTemplateAttachmentRepository)
        {
            _referralEmailTemplateAttachmentRepository = referralEmailTemplateAttachmentRepository;
        }

        public string TransferUploadPath { get; set; }
        public IList<TResult> GetAttachments<TResult>(int referralEmailTemplateId, Func<ReferralEmailTemplateAttachment, TResult> selector)
        {
            return _referralEmailTemplateAttachmentRepository.GetAttachments(referralEmailTemplateId, selector);
        }

        public ReferralAttachment UploadFile(HttpPostedFileBase files, string oldFile, Guid oldToken)
        {
            var token = Guid.NewGuid();

            //Create temp upload folder at server side
            if (!Directory.Exists(TransferUploadPath))
            {
                Directory.CreateDirectory(TransferUploadPath);
            }
            var filesName = files.FileName.Split('\\');
            // In IE, the system include full path of file name at client site.
            var fileName = filesName[filesName.Length - 1];

            files.SaveAs(Path.Combine(TransferUploadPath, token.ToString() + fileName));

            RemoveUploadFile(oldFile, oldToken.ToString());
            return new ReferralAttachment
            {
                RowGUID = token,
                AttachedFileName = fileName,
                AttachedFileSize = files.ContentLength / 1024
            };
        }

        public void DownloadFile(HttpContextBase context, string fileName, Guid rowGuid)
        {
            // Get persisted attached file from database if any.
            var savedFile = _referralEmailTemplateAttachmentRepository.GetAttachmentByGuid(rowGuid);
            if (savedFile == null)
            // if there is no saved file, looking for temp folder of previous upload but hasn't been saved to database.
            {
                var fullFilePath = Path.Combine(TransferUploadPath, rowGuid.ToString() + fileName);
                savedFile = File.ReadAllBytes(fullFilePath);
            }
            if (savedFile == null)
                throw new Exception(""); //TODO: zxczxcxz

            DownloadFileProvider.DownloadFile(context, fileName, savedFile);
        }

        public void RemoveFile(string fileName, Guid rowGuid)
        {
            RemoveUploadFile(fileName, rowGuid.ToString());
        }

        public byte[] ReadFile(string fileName, Guid rowGuid)
        {
            var file = Path.Combine(TransferUploadPath, rowGuid.ToString() + fileName);
            byte[] content = null;
            if (File.Exists(file))
            {
                content = File.ReadAllBytes(file);
            }
            return content;
        }

        public void RemoveUploadFile(string fileName, string rowGuid)
        {
            var filePath = Path.Combine(TransferUploadPath, rowGuid + fileName);
            if (File.Exists(filePath))
                File.Delete(filePath);
        }
    }
}
