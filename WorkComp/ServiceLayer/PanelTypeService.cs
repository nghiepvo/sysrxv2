﻿using System;
using System.Collections.Generic;
using Framework.BusinessRule;
using Framework.DomainModel.Entities;
using Framework.DomainModel.Entities.Common;
using Framework.DomainModel.ValueObject;
using Repositories.Interfaces;
using ServiceLayer.Interfaces;

namespace ServiceLayer
{
    public class PanelTypeService : MasterFileService<PanelType>, IPanelTypeService
    {
        private readonly IPanelTypeRepository _panelTypeRepository;
        public PanelTypeService(IPanelTypeRepository panelTypeRepository, IBusinessRuleSet<PanelType> businessRuleSet = null)
            : base(panelTypeRepository, panelTypeRepository, businessRuleSet)
        {
            _panelTypeRepository = panelTypeRepository;
        }

        public List<LookupItemVo> GetListPanelTypeStandard()
        {
            return _panelTypeRepository.GetListPanelTypeStandard();
        }

        public List<PanelCode> GetListPanelCode(int panelTypeId)
        {
            return _panelTypeRepository.GetListPanelCode(panelTypeId);
        }

        public InfoWhenChangePanelTypeInReferral GetInfoWhenChangePanelTypeInReferral(int idPanelType)
        {
            return _panelTypeRepository.GetInfoWhenChangePanelTypeInReferral(idPanelType);
        }

        public List<LookupItemVo> GetLookupForReferral(LookupQuery queryInfo, Func<PanelType, LookupItemVo> selector)
        {
            return _panelTypeRepository.GetLookupForReferral(queryInfo, selector);
        }
    }
}