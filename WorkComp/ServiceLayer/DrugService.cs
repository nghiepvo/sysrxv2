﻿using Framework.BusinessRule;
using Framework.DomainModel.Entities;
using Repositories.Interfaces;
using ServiceLayer.Interfaces;

namespace ServiceLayer
{
    public class DrugService : MasterFileService<Drug>, IDrugService
    {
        private readonly IDrugRepository _drugRepository;

        public DrugService(IDrugRepository drugRepository, IBusinessRuleSet<Drug> businessRuleSet = null)
            : base(drugRepository, drugRepository, businessRuleSet)
        {
            _drugRepository = drugRepository;
        }

        public string GetDrugs(int idReferral)
        {
            return _drugRepository.GetDrugs(idReferral);
        }
    }
}