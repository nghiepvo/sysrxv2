﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text.RegularExpressions;
using Framework.DomainModel.Entities.Common;
using Framework.DomainModel.Interfaces;
using Framework.DomainModel.ValueObject;
using Microsoft.Practices.ServiceLocation;
using ServiceLayer.Interfaces;
using Solr.DomainModel;
using Solr.Utility;
using SolrNet;
using SolrNet.Commands.Parameters;

namespace ServiceLayer
{
    public class ReportService : IReportService
    {
        private readonly ISolrOperations<SolrReferral> _referralSolrService;
        private readonly ISolrOperations<SolrClaimNumber> _claimNumberSolrService;
        private readonly ISolrOperations<SolrReferralResult> _referralResultSolrService;
        public ReportService()
        {
            _claimNumberSolrService = ServiceLocator.Current.GetInstance<ISolrOperations<SolrClaimNumber>>();
            _referralSolrService = ServiceLocator.Current.GetInstance<ISolrOperations<SolrReferral>>();
            _referralResultSolrService = ServiceLocator.Current.GetInstance<ISolrOperations<SolrReferralResult>>();
        }

        public dynamic GetDataForGrid(QueryInfo queryInfo, ReferralSearchViewModel searchCondition)
        {
            if (searchCondition.TypeSearch == "ClaimSearch")
            {
                var countClaimNumber = 0;
                var objListClaimNumber= GetDataForClaimNumberByClaimantSearch(queryInfo, searchCondition, ref countClaimNumber);
                return new { Data = objListClaimNumber, TotalRowCount = countClaimNumber };
            }
            if (searchCondition.TypeSearch == "ReferralClaimSearch")
            {
                var countReferral = 0;
                var objListReferral = GetDataForReferralByClaimantSearch(queryInfo, searchCondition, ref countReferral);
                return new { Data = objListReferral, TotalRowCount = countReferral };
            }
            if (searchCondition.TypeSearch == "ReferralSearch")
            {
                return GetDataForReferralByReferralSearch(queryInfo, searchCondition);
            }
            if (searchCondition.TypeSearch == "PharmcogeneticSearch")
            {
                return GetDataForReferralByPharmcogeneticSearch(queryInfo);
            }
            if (searchCondition.TypeSearch == "NovSearch")
            {
                return GetDataForReferralByNovSearch(queryInfo, searchCondition);
            }
            if (searchCondition.TypeSearch == "SearchTestResult")
            {
                return GetDataForReferralByResultSearch(queryInfo, searchCondition);
            }
            if (searchCondition.TypeSearch == "PhysicianReferralSearch")
            {
                return GetDataForReferralByPhycicianSearch(queryInfo, searchCondition);
            }
            return null;
        }

        private IEnumerable<ReferralSearchByClaimantGridVo> GetDataForReferralByClaimantSearch(IQueryInfo queryInfo, ReferralSearchViewModel searchCondition, ref int countTotal)
        {
            // Get list referral with search condition
            var listFilter = new List<ISolrQuery>();
            if (!string.IsNullOrEmpty(searchCondition.ReferralClaimantName))
            {
                if (searchCondition.ReferralClaimantName.MatchSpecialCharacterInSolr())
                {
                    listFilter.Add(new SolrQuery("claimantname: \"" + searchCondition.ReferralClaimantName + "\""));
                }
                else
                {
                    var listKeySearch = searchCondition.ReferralClaimantName.Split(' ');
                    var keySolr = "";
                    var countKeySearch = listKeySearch.Length;
                    for (var i = 0; i < countKeySearch; i++)
                    {
                        var key = listKeySearch[i];
                        keySolr += "claimantname:*" + key + "*";
                        if (i < (countKeySearch - 1))
                        {
                            keySolr += " AND ";
                        }
                    }
                    keySolr = "(" + keySolr + ")";
                    listFilter.Add(new SolrQuery(keySolr));
                }
            }
            if (!string.IsNullOrEmpty(searchCondition.ReferralClaimantSsn))
            {
                if (searchCondition.ReferralClaimantSsn.MatchSpecialCharacterInSolr())
                {
                    listFilter.Add(new SolrQuery("claimantssn: \"" + searchCondition.ReferralClaimantSsn + "\""));
                }
                else
                {
                    var listKeySearch = searchCondition.ReferralClaimantSsn.Split(' ');
                    var keySolr = "";
                    var countKeySearch = listKeySearch.Length;
                    for (var i = 0; i < countKeySearch; i++)
                    {
                        var key = listKeySearch[i];
                        keySolr += "claimantssn:*" + key + "*";
                        if (i < (countKeySearch - 1))
                        {
                            keySolr += " AND ";
                        }
                    }
                    keySolr = "(" + keySolr + ")";

                    listFilter.Add(new SolrQuery(keySolr));
                    
                }
            }
            if (searchCondition.ReferralClaimantDob != null)
            {
                var objFilterAdd = new SolrQueryByRange<DateTime>("claimantdob",
                        searchCondition.ReferralClaimantDob.Value, searchCondition.ReferralClaimantDob.Value.AddMinutes(1).AddMilliseconds(-1), true);
                listFilter.Add(objFilterAdd);
            }
            if (searchCondition.ReferralClaimantDoi != null)
            {
                var objFilterAdd = new SolrQueryByRange<DateTime>("claimnumberdoi",
                        searchCondition.ReferralClaimantDoi.Value, searchCondition.ReferralClaimantDoi.Value.AddMinutes(1).AddMilliseconds(-1), true);
                listFilter.Add(objFilterAdd);
            }
            if (!string.IsNullOrEmpty(searchCondition.ReferralClaimNumber))
            {
                if (searchCondition.ReferralClaimNumber.MatchSpecialCharacterInSolr())
                {
                    listFilter.Add(new SolrQuery("claimnumber: \"" + searchCondition.ReferralClaimNumber + "\""));
                }
                else
                {
                    var listKeySearch = searchCondition.ReferralClaimNumber.Split(' ');
                    var keySolr = "";
                    var countKeySearch = listKeySearch.Length;
                    for (var i = 0; i < countKeySearch; i++)
                    {
                        var key = listKeySearch[i];
                        keySolr += "claimnumber:*" + key + "*";
                        if (i < (countKeySearch - 1))
                        {
                            keySolr += " AND ";
                        }
                    }
                    keySolr = "(" + keySolr + ")";
                    listFilter.Add(new SolrQuery(keySolr));
                }
            }
            if (!string.IsNullOrEmpty(searchCondition.ReferralPayerName))
            {
                if (searchCondition.ReferralPayerName.MatchSpecialCharacterInSolr())
                {
                    listFilter.Add(new SolrQuery("payername: \"" + searchCondition.ReferralPayerName + "\""));
                }
                else
                {
                    var listKeySearch = searchCondition.ReferralPayerName.Split(' ');
                    var keySolr = "";
                    var countKeySearch = listKeySearch.Length;
                    for (var i = 0; i < countKeySearch; i++)
                    {
                        var key = listKeySearch[i];
                        keySolr += "payername:*" + key + "*";
                        if (i < (countKeySearch - 1))
                        {
                            keySolr += " AND ";
                        }
                    }
                    keySolr = "(" + keySolr + ")";
                    listFilter.Add(new SolrQuery(keySolr));
                }
            }
            if (searchCondition.ReferralId != null)
            {
                var objFilterAdd = new SolrQueryByRange<int>("referralid",
                        searchCondition.ReferralId.Value, searchCondition.ReferralId.Value, true);
                listFilter.Add(objFilterAdd);
            }
            if (!string.IsNullOrEmpty(searchCondition.ControlNumber))
            {
                if (searchCondition.ControlNumber.MatchSpecialCharacterInSolr())
                {
                    listFilter.Add(new SolrQuery("controlnumber: \"" + searchCondition.ControlNumber + "\""));
                }
                else
                {
                    var listKeySearch = searchCondition.ControlNumber.Split(' ');
                    var keySolr = "";
                    var countKeySearch = listKeySearch.Length;
                    for (var i = 0; i < countKeySearch; i++)
                    {
                        var key = listKeySearch[i];
                        keySolr += "controlnumber:*" + key + "*";
                        if (i < (countKeySearch - 1))
                        {
                            keySolr += " AND ";
                        }
                    }
                    keySolr = "(" + keySolr + ")";
                    listFilter.Add(new SolrQuery(keySolr));
                }
            }
            if (!string.IsNullOrEmpty(searchCondition.ProductType))
            {
                if (searchCondition.ProductType.MatchSpecialCharacterInSolr())
                {
                    listFilter.Add(new SolrQuery("producttype: \"" + searchCondition.ProductType + "\""));
                }
                else
                {
                    var listKeySearch = searchCondition.ProductType.Split(' ');
                    var keySolr = "";
                    var countKeySearch = listKeySearch.Length;
                    for (var i = 0; i < countKeySearch; i++)
                    {
                        var key = listKeySearch[i];
                        keySolr += "producttype:*" + key + "*";
                        if (i < (countKeySearch - 1))
                        {
                            keySolr += " AND ";
                        }
                    }
                    keySolr = "(" + keySolr + ")";
                    listFilter.Add(new SolrQuery(keySolr));
                }
            }
            if (!string.IsNullOrEmpty(searchCondition.AdjusterName))
            {
                if (searchCondition.AdjusterName.MatchSpecialCharacterInSolr())
                {
                    listFilter.Add(new SolrQuery("adjustername: \"" + searchCondition.AdjusterName + "\""));
                }
                else
                {
                    var listKeySearch = searchCondition.AdjusterName.Split(' ');
                    var keySolr = "";
                    var countKeySearch = listKeySearch.Length;
                    for (var i = 0; i < countKeySearch; i++)
                    {
                        var key = listKeySearch[i];
                        keySolr += "adjustername:*" + key + "*";
                        if (i < (countKeySearch - 1))
                        {
                            keySolr += " AND ";
                        }
                    }
                    keySolr = "(" + keySolr + ")";
                    listFilter.Add(new SolrQuery(keySolr));
                }
            }
            if (!string.IsNullOrEmpty(searchCondition.CreatedBy))
            {
                if (searchCondition.CreatedBy.MatchSpecialCharacterInSolr())
                {
                    listFilter.Add(new SolrQuery("createdbyname: \"" + searchCondition.CreatedBy + "\""));
                }
                else
                {
                    var listKeySearch = searchCondition.CreatedBy.Split(' ');
                    var keySolr = "";
                    var countKeySearch = listKeySearch.Length;
                    for (var i = 0; i < countKeySearch; i++)
                    {
                        var key = listKeySearch[i];
                        keySolr += "createdbyname:*" + key + "*";
                        if (i < (countKeySearch - 1))
                        {
                            keySolr += " AND ";
                        }
                    }
                    keySolr = "(" + keySolr + ")";
                    listFilter.Add(new SolrQuery(keySolr));
                }
            }
            var countItemQuery = _referralSolrService.Query(SolrQuery.All, new QueryOptions
            {
                Rows = 0,
                FilterQueries = listFilter
            });
            queryInfo.TotalRecords = countItemQuery.NumFound;
            var objOrderBy = new List<SortOrder> { new SortOrder("referralid", Order.ASC) };
            var data = _referralSolrService.Query(SolrQuery.All, new QueryOptions
            {
                Start = queryInfo.Skip,
                Rows = queryInfo.Take,
                FilterQueries = listFilter,
                OrderBy = objOrderBy
            });
            var dataGrid = data.Select(x => new ReferralSearchByClaimantGridVo
            {
                PatientName = x.ClaimantName,
                Jurisdiction = x.Jurisdiction,
                ClaimNumber = x.ClaimNumber,
                PayerName = x.PayerName,
                AdjusterName = x.AdjusterName,
                Doi = x.Doi == null ? "" : x.Doi.Value.ToString("MM/dd/yyyy"),
                Status = x.Status,
                ReferralId = x.ReferralId,
                ClaimNumberId=0
            }).ToList();
            countTotal = queryInfo.TotalRecords;
            return dataGrid;
        }

        private List<ReferralSearchByClaimantGridVo> GetDataForClaimNumberByClaimantSearch(IQueryInfo queryInfo, ReferralSearchViewModel searchCondition, ref int countTotal)
        {
            // Get list referral with search condition
            var listFilter = new List<ISolrQuery>();
            if (!string.IsNullOrEmpty(searchCondition.ClaimantName))
            {
                if (searchCondition.ClaimantName.MatchSpecialCharacterInSolr())
                {
                    listFilter.Add(new SolrQuery("claimantname: \"" + searchCondition.ClaimantName + "\""));
                }
                else
                {
                    var listKeySearch = searchCondition.ClaimantName.Split(' ');
                    var keySolr = "";
                    var countKeySearch = listKeySearch.Length;
                    for (var i = 0; i < countKeySearch; i++)
                    {
                        var key = listKeySearch[i];
                        keySolr += "claimantname:*" + key + "*";
                        if (i < (countKeySearch - 1))
                        {
                            keySolr += " AND ";
                        }
                    }
                    keySolr = "(" + keySolr + ")";
                    listFilter.Add(new SolrQuery(keySolr));
                }
            }
            if (!string.IsNullOrEmpty(searchCondition.ClaimantSsn))
            {
                if (searchCondition.ClaimantSsn.MatchSpecialCharacterInSolr())
                {
                    listFilter.Add(new SolrQuery("claimantssn: \"" + searchCondition.ClaimantSsn + "\""));
                }
                else
                {
                    var listKeySearch = searchCondition.ClaimantSsn.Split(' ');
                    var keySolr = "";
                    var countKeySearch = listKeySearch.Length;
                    for (var i = 0; i < countKeySearch; i++)
                    {
                        var key = listKeySearch[i];
                        keySolr += "claimantssn:*" + key + "*";
                        if (i < (countKeySearch - 1))
                        {
                            keySolr += " AND ";
                        }
                    }
                    keySolr = "(" + keySolr + ")";
                    listFilter.Add(new SolrQuery(keySolr));
                }
            }
            if (searchCondition.ClaimantDob != null)
            {
                var objFilterAdd = new SolrQueryByRange<DateTime>("claimantdob",
                        searchCondition.ClaimantDob.Value, searchCondition.ClaimantDob.Value.AddMinutes(1).AddMilliseconds(-1), true);
                listFilter.Add(objFilterAdd);
            }
            if (searchCondition.ClaimantDoi != null)
            {
                var objFilterAdd = new SolrQueryByRange<DateTime>("doi",
                        searchCondition.ClaimantDoi.Value, searchCondition.ClaimantDoi.Value.AddMinutes(1).AddMilliseconds(-1), true);
                listFilter.Add(objFilterAdd);
            }
            if (!string.IsNullOrEmpty(searchCondition.ClaimNumber))
            {
                if (searchCondition.ClaimNumber.MatchSpecialCharacterInSolr())
                {
                    listFilter.Add(new SolrQuery("name: \"" + searchCondition.ClaimNumber + "\""));
                }
                else
                {
                    var listKeySearch = searchCondition.ClaimNumber.Split(' ');
                    var keySolr = "";
                    var countKeySearch = listKeySearch.Length;
                    for (var i = 0; i < countKeySearch; i++)
                    {
                        var key = listKeySearch[i];
                        keySolr += "name:*" + key + "*";
                        if (i < (countKeySearch - 1))
                        {
                            keySolr += " AND ";
                        }
                    }
                    keySolr = "(" + keySolr + ")";
                    listFilter.Add(new SolrQuery(keySolr));
                }
            }
            if (!string.IsNullOrEmpty(searchCondition.PayerName))
            {
                if (searchCondition.PayerName.MatchSpecialCharacterInSolr())
                {
                    listFilter.Add(new SolrQuery("payername: \"" + searchCondition.PayerName + "\""));
                }
                else
                {
                    var listKeySearch = searchCondition.PayerName.Split(' ');
                    var keySolr = "";
                    var countKeySearch = listKeySearch.Length;
                    for (var i = 0; i < countKeySearch; i++)
                    {
                        var key = listKeySearch[i];
                        keySolr += "payername:*" + key + "*";
                        if (i < (countKeySearch - 1))
                        {
                            keySolr += " AND ";
                        }
                    }
                    keySolr = "(" + keySolr + ")";
                    listFilter.Add(new SolrQuery(keySolr));
                }
            }
            var countItemQuery = _claimNumberSolrService.Query(SolrQuery.All, new QueryOptions
            {
                Rows = 0,
                FilterQueries = listFilter
            });
            queryInfo.TotalRecords = countItemQuery.NumFound;
            var data = _claimNumberSolrService.Query(SolrQuery.All, new QueryOptions
            {
                Start = queryInfo.Skip,
                Rows = queryInfo.Take,
                FilterQueries = listFilter
            });
            var dataGrid = data.Select(x => new ReferralSearchByClaimantGridVo
            {
                PatientName = x.ClaimantName,
                Jurisdiction=x.Jurisdiction,
                ClaimNumber=x.Name,
                PayerName=x.PayerName,
                AdjusterName=x.AdjusterName,
                Doi = x.Doi == null ? "" : x.Doi.Value.ToString("MM/dd/yyyy"),
                Status=x.Status,
                ClaimNumberId=x.ClaimNumberId,
                ReferralId=0
            }).ToList();
            countTotal = queryInfo.TotalRecords;
            return dataGrid;
        }

        private dynamic GetDataForReferralByReferralSearch(QueryInfo queryInfo, ReferralSearchViewModel searchCondition)
        {
            // Get list referral with search condition
            var listFilter = new List<ISolrQuery>();
            var listStrQuery = new List<string>();
            if (searchCondition.IsCompletedReferral.GetValueOrDefault())
            {
                listStrQuery.Add("(status:Completed)");
            }
            if (searchCondition.IsOpenReferral.GetValueOrDefault())
            {
                listStrQuery.Add("(status:Open)");
            }
            if (searchCondition.IsCancelReferral.GetValueOrDefault())
            {
                listStrQuery.Add("(status:Canceled)");
            }
            if (listStrQuery.Count != 0)
            {
                var strQuery = String.Join(" OR ", listStrQuery.ToArray());
                listFilter.Add(new SolrQuery(strQuery));
            }
            if (searchCondition.StartCreatedReferral != null)
            {
                var endSearch = DateTime.MaxValue;
                if (searchCondition.EndCreatedReferral != null)
                {
                    endSearch = searchCondition.EndCreatedReferral.Value.AddDays(1).AddMilliseconds(-1);
                }
                var objFilterAdd = new SolrQueryByRange<DateTime>("createddate",
                        searchCondition.StartCreatedReferral.Value, endSearch, true);
                listFilter.Add(objFilterAdd);
            }
            else
            {
                var endSearch = DateTime.MaxValue;
                if (searchCondition.EndCreatedReferral != null)
                {
                    endSearch = searchCondition.EndCreatedReferral.Value.AddDays(1).AddMilliseconds(-1);
                }
                var objFilterAdd = new SolrQueryByRange<DateTime>("createddate",
                         DateTime.MinValue, endSearch, true);
                listFilter.Add(objFilterAdd);
            }
            var countItemQuery = _referralSolrService.Query(SolrQuery.All, new QueryOptions
            {
                Rows = 0,
                FilterQueries = listFilter
            });
            queryInfo.TotalRecords = countItemQuery.NumFound;
            var objOrderBy = new List<SortOrder> { new SortOrder("referralid", Order.ASC) };
            var data = _referralSolrService.Query(SolrQuery.All, new QueryOptions
            {
                Start = queryInfo.Skip,
                Rows = queryInfo.Take,
                FilterQueries = listFilter,
                OrderBy = objOrderBy
            });
            var dataGrid = data.Select(x => new ReferralSearchByBusinessReportGridVo
            {
                ReferralId = x.ReferralId,
                ProductType = x.ProductType,
                Status = x.Status,
                PayerName = x.PayerName,
                DueDate = x.DueDate == null ? "" : x.DueDate.Value.ToString("MM/dd/yyyy"),
                Completed = x.Status == "Completed"?"Yes":"No",
                ClaimantName = x.ClaimantName,
                ClaimNumber = x.ClaimNumber,
                Doi = x.Doi == null ? "" : x.Doi.Value.ToString("MM/dd/yyyy"),
                Nov = x.NextOfficeVisitDate == null ? "" : x.NextOfficeVisitDate.Value.ToString("MM/dd/yyyy"),
                CreatedDate = x.CreatedDate == null ? "" : x.CreatedDate.Value.ToString("MM/dd/yyyy")
            }).ToList();
            return new { Data = dataGrid, TotalRowCount = queryInfo.TotalRecords };
        }
        private dynamic GetDataForReferralByPharmcogeneticSearch(QueryInfo queryInfo)
        {
            // Get list referral with search condition
            var listFilter = new List<ISolrQuery> { new SolrQuery("producttype:*pharmacogenetic*") };
            var countItemQuery = _referralSolrService.Query(SolrQuery.All, new QueryOptions
            {
                Rows = 0,
                FilterQueries = listFilter
            });
            queryInfo.TotalRecords = countItemQuery.NumFound;
            var objOrderBy = new List<SortOrder> { new SortOrder("referralid", Order.ASC) };
            var data = _referralSolrService.Query(SolrQuery.All, new QueryOptions
            {
                Start = queryInfo.Skip,
                Rows = queryInfo.Take,
                FilterQueries = listFilter,
                OrderBy = objOrderBy
            });
            var dataGrid = data.Select(x => new ReferralSearchByBusinessReportGridVo
            {
                ReferralId = x.ReferralId,
                ProductType = x.ProductType,
                Status = x.Status,
                PayerName = x.PayerName,
                DueDate = x.DueDate == null ? "" : x.DueDate.Value.ToString("MM/dd/yyyy"),
                Completed = x.Status == "Completed" ? "Yes" : "No",
                ClaimantName = x.ClaimantName,
                ClaimNumber = x.ClaimNumber,
                Doi = x.Doi == null ? "" : x.Doi.Value.ToString("MM/dd/yyyy"),
                Nov = x.NextOfficeVisitDate == null ? "" : x.NextOfficeVisitDate.Value.ToString("MM/dd/yyyy"),
                CreatedDate = x.CreatedDate == null ? "" : x.CreatedDate.Value.ToString("MM/dd/yyyy")
            }).ToList();
            return new { Data = dataGrid, TotalRowCount = queryInfo.TotalRecords };
        }
        private dynamic GetDataForReferralByNovSearch(QueryInfo queryInfo, ReferralSearchViewModel searchCondition)
        {
            // Get list referral with search condition
            var listFilter = new List<ISolrQuery>();
            
            if (searchCondition.StartNovDateReferral != null)
            {
                var endSearch = DateTime.MaxValue;
                if (searchCondition.EndNovDateReferral != null)
                {
                    endSearch = searchCondition.EndNovDateReferral.Value.AddDays(1).AddMilliseconds(-1);
                }
                var objFilterAdd = new SolrQueryByRange<DateTime>("createddate",
                        searchCondition.StartNovDateReferral.Value, endSearch, true);
                listFilter.Add(objFilterAdd);
            }
            else
            {
                var endSearch = DateTime.MinValue;
                if (searchCondition.EndNovDateReferral != null)
                {
                    endSearch = searchCondition.EndNovDateReferral.Value.AddDays(1).AddMilliseconds(-1);
                }
                var objFilterAdd = new SolrQueryByRange<DateTime>("createddate",
                         DateTime.MinValue, endSearch, true);
                listFilter.Add(objFilterAdd);
            }
            var objFilterNovAdd = new SolrQueryByRange<DateTime>("nextofficevisitdate",
                         DateTime.MinValue, DateTime.MaxValue, true);
            listFilter.Add(objFilterNovAdd);
            var countItemQuery = _referralSolrService.Query(SolrQuery.All, new QueryOptions
            {
                Rows = 0,
                FilterQueries = listFilter
            });
            queryInfo.TotalRecords = countItemQuery.NumFound;
            var objOrderBy = new List<SortOrder> { new SortOrder("referralid", Order.ASC) };
            var data = _referralSolrService.Query(SolrQuery.All, new QueryOptions
            {
                Start = queryInfo.Skip,
                Rows = queryInfo.Take,
                FilterQueries = listFilter,
                 OrderBy = objOrderBy
            });
            var dataGrid = data.Select(x => new ReferralSearchByBusinessReportGridVo
            {
                ReferralId = x.ReferralId,
                ProductType = x.ProductType,
                Status = x.Status,
                PayerName = x.PayerName,
                DueDate = x.DueDate == null ? "" : x.DueDate.Value.ToString("MM/dd/yyyy"),
                Completed = x.Status == "Completed" ? "Yes" : "No",
                ClaimantName = x.ClaimantName,
                ClaimNumber = x.ClaimNumber,
                Doi = x.Doi == null ? "" : x.Doi.Value.ToString("MM/dd/yyyy"),
                Nov = x.NextOfficeVisitDate == null ? "" : x.NextOfficeVisitDate.Value.ToString("MM/dd/yyyy"),
                CreatedDate = x.CreatedDate == null ? "" : x.CreatedDate.Value.ToString("MM/dd/yyyy")
            }).ToList();
            return new { Data = dataGrid, TotalRowCount = queryInfo.TotalRecords };
        }
        private dynamic GetDataForReferralByPhycicianSearch(QueryInfo queryInfo, ReferralSearchViewModel searchCondition)
        {
            // Get list referral with search condition
            var listFilter = new List<ISolrQuery>();

            if (searchCondition.StartDatePhysicianReferral != null)
            {
                var endSearch = DateTime.MaxValue;
                if (searchCondition.EndDatePhysicianReferral != null)
                {
                    endSearch = searchCondition.EndDatePhysicianReferral.Value.AddDays(1).AddMilliseconds(-1);
                }
                var objFilterAdd = new SolrQueryByRange<DateTime>("createddate",
                        searchCondition.StartDatePhysicianReferral.Value, endSearch, true);
                listFilter.Add(objFilterAdd);
            }
            else
            {
                var endSearch = DateTime.MinValue;
                if (searchCondition.EndDatePhysicianReferral != null)
                {
                    endSearch = searchCondition.EndDatePhysicianReferral.Value.AddDays(1).AddMilliseconds(-1);
                }
                var objFilterAdd = new SolrQueryByRange<DateTime>("createddate",
                         DateTime.MinValue, endSearch, true);
                listFilter.Add(objFilterAdd);
            }
            var objFilterPhysicianAdd = new SolrQueryByRange<int>("npinumberid", 1, int.MaxValue, true);
            listFilter.Add(objFilterPhysicianAdd);
            var countItemQuery = _referralSolrService.Query(SolrQuery.All, new QueryOptions
            {
                Rows = 0,
                FilterQueries = listFilter
            });
            queryInfo.TotalRecords = countItemQuery.NumFound;
            var objOrderBy = new List<SortOrder> { new SortOrder ( "referralid", Order.ASC ) };
            var data = _referralSolrService.Query(SolrQuery.All, new QueryOptions
            {
                Start = queryInfo.Skip,
                Rows = queryInfo.Take,
                FilterQueries = listFilter,
                OrderBy = objOrderBy
            });
            var dataGrid = data.Select(x => new ReferralPhysicianSearchGridVo
            {
                Id=x.ReferralId,
                ReferralId = x.ReferralId,
                CreatedDate = x.CreatedDate == null ? "" : x.CreatedDate.Value.ToString("MM/dd/yyyy"),
                FirstName = x.TreatingPhysicianFirstName,
                LastName = x.TreatingPhysicianLastName,
                MiddleName = x.TreatingPhysicianMiddleName,
                Address = x.TreatingPhysicianAddress,
                City = x.TreatingPhysicianCity,
                State = x.TreatingPhysicianState,
                PostalCode = x.TreatingPhysicianPostCode,
                Phone = x.TreatingPhysicianPhone,
                Fax = x.TreatingPhysicianFax,
                Email = x.TreatingPhysicianEmail,
                NpiNumber = x.NpiNumber,
            }).ToList();
            return new { Data = dataGrid, TotalRowCount = queryInfo.TotalRecords };
        }
        private dynamic GetDataForReferralByResultSearch(QueryInfo queryInfo, ReferralSearchViewModel searchCondition)
        {
            // Get list referral with search condition
            var listFilter = new List<ISolrQuery>();

            if (searchCondition.StartDatePhysicianReferral != null)
            {
                var endSearch = DateTime.MaxValue;
                if (searchCondition.EndDatePhysicianReferral != null)
                {
                    endSearch = searchCondition.EndDatePhysicianReferral.Value.AddDays(1).AddMilliseconds(-1);
                }
                var objFilterAdd = new SolrQueryByRange<DateTime>("createddate",
                        searchCondition.StartDatePhysicianReferral.Value, endSearch, true);
                listFilter.Add(objFilterAdd);
            }
            else
            {
                var endSearch = DateTime.MinValue;
                if (searchCondition.EndDatePhysicianReferral != null)
                {
                    endSearch = searchCondition.EndDatePhysicianReferral.Value.AddDays(1).AddMilliseconds(-1);
                }
                var objFilterAdd = new SolrQueryByRange<DateTime>("createddate",
                         DateTime.MinValue, endSearch, true);
                listFilter.Add(objFilterAdd);
            }
            var objOrderBy = new List<SortOrder> { new SortOrder("referralid", Order.ASC) };
            var listReferal = _referralSolrService.Query(SolrQuery.All, new QueryOptions
            {
                FilterQueries = listFilter,
                OrderBy = objOrderBy
            });
            // Get list control number with search condition
            listFilter = new List<ISolrQuery>();
            
            foreach (var condition in searchCondition.SelectedTestResultSearchCondition)
            {
                var objFilterAdd = new SolrQuery("consistencycmt:*" + condition + "*");
                listFilter.Add(objFilterAdd);
            }
            var referralResultSearchAll = new SolrMultipleCriteriaQuery(listFilter, "OR");
            var listReferalResult = _referralResultSolrService.Query(referralResultSearchAll);

            var dataGrid = (from referal in listReferal
                join result in listReferalResult on referal.ControlNumber.Trim() equals result.ControlNumber.Trim()
                select new ReferralTestResultSearchGridVo
                {
                    Id = referal.ReferralId,
                    ReferralId = referal.ReferralId,
                    ProductType = referal.ProductType,
                    Status = referal.Status,
                    PayerName = referal.PayerName,
                    DueDate = referal.DueDate == null ? "" : referal.DueDate.Value.ToString("MM/dd/yyyy"),
                    Completed = referal.Status == "Completed" ? "Yes" : "No",
                    ClaimantName = referal.ClaimantName,
                    ClaimNumber = referal.ClaimNumber,
                    Doi = referal.Doi == null ? "" : referal.Doi.Value.ToString("MM/dd/yyyy"),
                    Nov =
                        referal.NextOfficeVisitDate == null
                            ? ""
                            : referal.NextOfficeVisitDate.Value.ToString("MM/dd/yyyy"),
                    CreatedDate = referal.CreatedDate == null ? "" : referal.CreatedDate.Value.ToString("MM/dd/yyyy"),
                    ConsistencyCmt = result.Consistencycmt,
                    ResultStatus = result.ResultStatus
                }).ToList();
             queryInfo.TotalRecords = dataGrid.Count;
            return new { Data = dataGrid.Skip(queryInfo.Skip).Take(queryInfo.Take), TotalRowCount = queryInfo.TotalRecords };
        }
    }
}