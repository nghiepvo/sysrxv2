﻿using Framework.BusinessRule;
using Framework.DomainModel.Entities;
using Repositories.Interfaces;
using ServiceLayer.Interfaces;

namespace ServiceLayer
{
    public class AssayCodeService : MasterFileService<AssayCode>, IAssayCodeService
    {
        private readonly IAssayCodeRepository _assayCodeRepository;
        public AssayCodeService(IAssayCodeRepository assayCodeRepository, IBusinessRuleSet<AssayCode> businessRuleSet = null)
            : base(assayCodeRepository, assayCodeRepository, businessRuleSet)
        {
            _assayCodeRepository = assayCodeRepository;
        }
    }
}