﻿using System.Collections.Generic;
using Framework.BusinessRule;
using Framework.DomainModel.Entities;
using Framework.DomainModel.ValueObject;
using Repositories.Interfaces;
using ServiceLayer.Interfaces;

namespace ServiceLayer
{
    public class ReasonReferralService : MasterFileService<ReasonReferral>, IReasonReferralService
    {
        private readonly IReasonReferralRepository _reasonReferralRepository;

        public ReasonReferralService(IReasonReferralRepository reasonReferralRepository, IBusinessRuleSet<ReasonReferral> businessRuleSet = null)
            : base(reasonReferralRepository, reasonReferralRepository, businessRuleSet)
        {
            _reasonReferralRepository = reasonReferralRepository;
        }

        public IList<ReasonReferralWithReferralVo> GetSelectedReasonReferralbyReferral(int referralId)
        {
            return _reasonReferralRepository.GetSelectedReasonReferralbyReferral(referralId);
        }

        public IList<ReasonReferralWithReferralVo> GetUnselectedReasonReferralByReferral(int referralId)
        {
            return _reasonReferralRepository.GetUnselectedReasonReferralByReferral(referralId);
        }

        public IList<ReasonReferralParrentVo> GetReasonReferral(int? parrentId)
        {
            return _reasonReferralRepository.GetReasonReferral(parrentId);
        }

        public List<LookupItemVo> GetListReasonReferral()
        {
            return _reasonReferralRepository.GetListReasonReferral();
        }

        public bool UpdateActiveReasonReferral(int id)
        {
            return _reasonReferralRepository.UpdateActiveReasonReferral(id);
        }
    }
}