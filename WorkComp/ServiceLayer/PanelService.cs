﻿using Framework.BusinessRule;
using Framework.DomainModel.Entities;
using Repositories.Interfaces;
using ServiceLayer.Interfaces;

namespace ServiceLayer
{
    public class PanelService : MasterFileService<Panel>, IPanelService
    {
        private readonly IPanelRepository _panelRepository;
        public PanelService(IPanelRepository panelRepository, IBusinessRuleSet<Panel> businessRuleSet = null)
            : base(panelRepository, panelRepository, businessRuleSet)
        {
            _panelRepository = panelRepository;
        }

    }
}