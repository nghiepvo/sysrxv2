﻿using System.Collections.Generic;
using Framework.BusinessRule;
using Framework.DomainModel.Entities;
using Repositories.Interfaces;
using ServiceLayer.Common;
using ServiceLayer.Interfaces;

namespace ServiceLayer
{
    public class UserRoleService : MasterFileService<UserRole>, IUserRoleService
    {
        private readonly IUserRoleRepository _userRoleRepository;
        public UserRoleService(IUserRoleRepository userRoleRepository, IBusinessRuleSet<UserRole> businessRuleSet = null)
            : base(userRoleRepository, userRoleRepository, businessRuleSet)
        {
            _userRoleRepository = userRoleRepository;
        }

        public dynamic GetRoleFunction(int idRole)
        {
            return _userRoleRepository.GetRoleFunction(idRole);
        }

        public List<DocumentType> GetAllDocumentType()
        {
            return _userRoleRepository.GetAllDocumentType();
        }

        public override UserRole Add(UserRole entity)
        {
            var objResult=base.Add(entity);
            // Delete cache for menu 
            MenuExtractData.Instance.RefershListData();
            return objResult;
        }

        public override UserRole Update(UserRole entity)
        {
            var objResult= base.Update(entity);
            // Delete cache for menu 
            MenuExtractData.Instance.RefershListData();
            return objResult;
        }
    }
}