﻿using Framework.BusinessRule;
using Framework.DomainModel.Entities;
using Framework.DomainModel.Interfaces;
using Framework.DomainModel.ValueObject;
using Repositories.Interfaces;
using ServiceLayer.Interfaces;

namespace ServiceLayer
{
    public class EmployerService : MasterFileService<Employer>, IEmployerService
    {
        private readonly IEmployerRepository _employerRepository;

        public EmployerService(IEmployerRepository employerRepository, IBusinessRuleSet<Employer> businessRuleSet = null)
            : base(employerRepository, employerRepository, businessRuleSet)
        {
            _employerRepository = employerRepository;
        }

        public InfoWhenChangeEmployerInReferral GetInfoWhenChangeEmployerInReferral(int idEmployer)
        {
            return _employerRepository.GetInfoWhenChangeEmployerInReferral(idEmployer);
        }

        
    }
}