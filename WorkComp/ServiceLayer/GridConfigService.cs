﻿using System;
using Framework.DomainModel.Entities;
using Repositories.Interfaces;
using ServiceLayer.Interfaces;

namespace ServiceLayer
{
    public class GridConfigService : MasterFileService<GridConfig>, IGridConfigService
    {
        private readonly IGridConfigRepository _gridConfigRepository;
        public GridConfigService(IGridConfigRepository gridConfigRepository)
            : base(gridConfigRepository, gridConfigRepository)
        {
            _gridConfigRepository = gridConfigRepository;
        }

        public TResult GetGridConfig<TResult>(Func<GridConfig, TResult> selector,
                                                int? userId,
                                                int documentTypeId,
                                                string gridInternalName)
        {
            return _gridConfigRepository.GetGridConfig(selector,
                                                                    userId,
                                                                    documentTypeId,
                                                                    gridInternalName);
        }
    }
}
