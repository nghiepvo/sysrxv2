﻿using Framework.BusinessRule;
using Framework.DomainModel.Entities;
using Repositories.Interfaces;
using ServiceLayer.Interfaces;

namespace ServiceLayer
{
    public class ConfigurationService : MasterFileService<Configuration>, IConfigurationService
    {
        private readonly IConfigurationRepository _configurationRepository;
        public ConfigurationService(IConfigurationRepository configurationRepository, IBusinessRuleSet<Configuration> businessRuleSet = null)
            : base(configurationRepository, configurationRepository, businessRuleSet)
        {
            _configurationRepository = configurationRepository;
        }

    }
}