﻿using System.Transactions;
using Framework.BusinessRule;
using Framework.DomainModel.Entities;
using Repositories.Interfaces;
using ServiceLayer.Interfaces;
namespace ServiceLayer
{
    public class ReferralEmailTemplateService : MasterFileService<ReferralEmailTemplate>, IReferralEmailTemplateService
    {
        private readonly IReferralEmailTemplateRepository _referralEmailTemplateRepository;
        public ReferralEmailTemplateService(IReferralEmailTemplateRepository referralEmailTemplateRepository,
                                    IBusinessRuleSet<ReferralEmailTemplate> businessRuleSet = null)
            : base(referralEmailTemplateRepository, referralEmailTemplateRepository, businessRuleSet)
        {
            _referralEmailTemplateRepository = referralEmailTemplateRepository;
            
        }

        public ReferralEmailTemplate GetIncludeAttachment(int referralId, int emailTemplateId)
        {
            return _referralEmailTemplateRepository.GetIncludeAttachment(referralId, emailTemplateId);
        }
    }
}