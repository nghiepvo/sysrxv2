﻿using Framework.BusinessRule;
using Framework.DomainModel.Entities;
using Repositories.Interfaces;
using ServiceLayer.Interfaces;

namespace ServiceLayer
{
    public class BranchService : MasterFileService<Branch>, IBranchService
    {
        private readonly IBranchRepository _branchRepository;
        public BranchService(IBranchRepository branchRepository, IBusinessRuleSet<Branch> businessRuleSet = null)
            : base(branchRepository, branchRepository, businessRuleSet)
        {
            _branchRepository = branchRepository;
        }
    }
}
