﻿using System;
using Framework.BusinessRule;
using Framework.DomainModel.Entities;
using Repositories.Interfaces;
using ServiceLayer.Interfaces;

namespace ServiceLayer
{
    public class AlertService : MasterFileService<Alert>, IAlertService
    {
        private readonly IAlertRepository _alertRepository;

        public AlertService(IAlertRepository alertRepository, IBusinessRuleSet<Alert> businessRuleSet = null)
            : base(alertRepository, alertRepository, businessRuleSet)
        {
            _alertRepository = alertRepository;
        }


        public dynamic GetAlertPartial(int idCurrentUser, bool isAdminRole, int startIndex, int countItem)
        {
            return _alertRepository.GetAlertPartial(idCurrentUser, isAdminRole, startIndex, countItem);
        }

        public dynamic GetDataAlertForGrid(int idCurrentUser, bool isAdminRole, string searchId, DateTime? startDate, DateTime? endDate, int startIndex, int countItem)
        {
            return _alertRepository.GetDataAlertForGrid(idCurrentUser, isAdminRole, searchId, startDate, endDate, startIndex, countItem);
        }
    }
}