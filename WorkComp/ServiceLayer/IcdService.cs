﻿using System.Collections.Generic;
using Framework.BusinessRule;
using Framework.DomainModel.Entities;
using Framework.DomainModel.ValueObject;
using Repositories.Interfaces;
using ServiceLayer.Interfaces;

namespace ServiceLayer
{
    public class IcdService : MasterFileService<Icd>, IIcdService
    {
        private readonly IIcdRepository _icdRepository;
        public IcdService( IIcdRepository icdRepository, IBusinessRuleSet<Icd> businessRuleSet = null)
            : base(icdRepository, icdRepository, businessRuleSet)
        {
            _icdRepository = icdRepository;
        }

        public List<LookupItemVo> GetListIcdType()
        {
            return _icdRepository.GetListIcdType();
        }

        public List<Icd> GetIcdsByReferral(int referralId)
        {
            return _icdRepository.GetIcdsByReferral(referralId);
        }

    }
}