﻿using Framework.BusinessRule;
using Framework.DomainModel.Entities;
using Repositories.Interfaces;
using ServiceLayer.Interfaces;

namespace ServiceLayer
{
    public class SampleTestingTypeService : MasterFileService<SampleTestingType>, ISampleTestingTypeService
    {
        private readonly ISampleTestingTypeRepository _sampleTestingTypeRepository;

        public SampleTestingTypeService(ISampleTestingTypeRepository sampleTestingTypeRepository, IBusinessRuleSet<SampleTestingType> businessRuleSet = null)
            : base(sampleTestingTypeRepository, sampleTestingTypeRepository, businessRuleSet)
        {
            _sampleTestingTypeRepository = sampleTestingTypeRepository;
        }

    }
}