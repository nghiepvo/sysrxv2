﻿using System.Collections.Generic;
using Framework.BusinessRule;
using Framework.DomainModel.Entities;
using Framework.DomainModel.ValueObject;
using Repositories.Interfaces;
using ServiceLayer.Interfaces;

namespace ServiceLayer
{
    public class HeadingService : MasterFileService<Heading>, IHeadingService
    {
        private readonly IHeadingRepository _headingRepository;
        public HeadingService(IHeadingRepository headingRepository, IBusinessRuleSet<Heading> businessRuleSet = null)
            : base(headingRepository, headingRepository, businessRuleSet)
        {
            _headingRepository = headingRepository;
        }

        public List<LookupItemVo> GetListHeading()
        {
            return _headingRepository.GetListHeading();
        }
    }
}