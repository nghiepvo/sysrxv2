﻿using System.Collections.Generic;
using Framework.BusinessRule;
using Framework.DomainModel.Entities;
using Framework.DomainModel.Interfaces;
using Framework.DomainModel.ValueObject;
using Framework.Exceptions;
using Repositories.Interfaces;
using ServiceLayer.Interfaces;

namespace ServiceLayer
{
    public class TaskTemplateService : MasterFileService<TaskTemplate>, ITaskTemplateService
    {
        private readonly ITaskTemplateRepository _taskTemplateRepository;
        public TaskTemplateService(ITaskTemplateRepository taskTemplateRepository, IBusinessRuleSet<TaskTemplate> businessRuleSet = null)
            : base(taskTemplateRepository, taskTemplateRepository, businessRuleSet)
        {
            _taskTemplateRepository = taskTemplateRepository;
        }

        public override void DeleteById(int id)
        {
            // Check if isSystem
            var entity = GetById(id);
            if (entity != null && !entity.IsSystem.GetValueOrDefault())
            {
                base.DeleteById(id);
            }
            else
            {
                var exception = new UnAuthorizedAccessException("UnAuthorizedAccessText");
                throw exception;
            }
        }

        public List<DualListBoxItemVo> GetAllTaskTemplateForDualListBox()
        {
            return _taskTemplateRepository.GetAllTaskTemplateForDualListBox();
        }

        public List<DualListBoxItemVo> GetTaskTemplateByTaskGroup(int taskGroupId)
        {
            return _taskTemplateRepository.GetTaskTemplateByTaskGroup(taskGroupId);
        }
    }
}