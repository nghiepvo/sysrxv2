﻿using System.Collections.Generic;
using Framework.DomainModel.Entities.Common;
using Framework.DomainModel.ReportValueObject;
using Repositories.Interfaces;
using ServiceLayer.Interfaces;

namespace ServiceLayer
{
    public class SysRxReportService : ISysRxReportService
    {
        private readonly ISysRxReportRepository _sysRxReportRepository;
        public SysRxReportService(ISysRxReportRepository sysRxReportRepository)
        {
            _sysRxReportRepository = sysRxReportRepository;
        }

        public IList<UtilizationByInjuredWorkerVo> GetDataUtilizationByInjuredWorkerReport(SysRxReportQueryInfo queryInfo, ref int totalRow)
        {
            return _sysRxReportRepository.GetDataUtilizationByInjuredWorkerReport(queryInfo, ref totalRow);
        }

        public IList<TestResultsDetailVo> GetDataTestResultdetailReport(SysRxReportQueryInfo queryInfo, ref int totalRow)
        {
            return _sysRxReportRepository.GetDataTestResultdetailReport(queryInfo, ref totalRow);
        }

        public IList<CancellationDetailVo> GetCancellationDetailtReport(SysRxReportQueryInfo queryInfo, ref int totalRow)
        {
            return _sysRxReportRepository.GetCancellationDetailtReport(queryInfo, ref totalRow);
        }

        public IList<TestResultsSummaryVo> GetTestResultsSummaryReport(SysRxReportQueryInfo queryInfo, ref int totalRow)
        {
            return _sysRxReportRepository.GetTestResultsSummaryReport(queryInfo, ref totalRow);
        }

        public IList<MGMTStatusSummaryReportVo> GetMGMTStatusSummaryReport(SysRxReportQueryInfo queryInfo, ref int totalRow)
        {
            return _sysRxReportRepository.GetMGMTStatusSummaryReport(queryInfo, ref totalRow);
        }

        public IList<MGMTUserProductivityVo> GetMGMTUserProductivityReport(SysRxReportQueryInfo queryInfo, ref int totalRow)
        {
            return _sysRxReportRepository.GetMGMTUserProductivityReport(queryInfo, ref totalRow);
        }
    }
}