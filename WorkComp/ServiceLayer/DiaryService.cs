﻿using Framework.BusinessRule;
using Framework.DomainModel.Entities;
using Repositories.Interfaces;
using ServiceLayer.Interfaces;

namespace ServiceLayer
{
    public class DiaryService : MasterFileService<Diary>, IDiaryService
    {
        private readonly IDiaryRepository _diaryRepository;
        public DiaryService(IDiaryRepository diaryRepository, IBusinessRuleSet<Diary> businessRuleSet = null)
            : base(diaryRepository, diaryRepository, businessRuleSet)
        {
            _diaryRepository = diaryRepository;
        }

        public System.Collections.Generic.IList<Diary> GetDiaryWhenChangeReferral(int? referralId)
        {
            return _diaryRepository.GetDiaryWhenChangeReferral(referralId);
        }
    }
}