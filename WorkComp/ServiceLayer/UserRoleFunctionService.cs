﻿using Framework.BusinessRule;
using Framework.DomainModel.Entities;
using Repositories.Interfaces;
using ServiceLayer.Interfaces;

namespace ServiceLayer
{
    public class UserRoleFunctionService : MasterFileService<UserRoleFunction>, IUserRoleFunctionService
    {
        private readonly IUserRoleFunctionRepository _userRoleFunctionRepository;
        public UserRoleFunctionService(IUserRoleFunctionRepository userRoleFunctionRepository, IBusinessRuleSet<UserRoleFunction> businessRuleSet = null)
            : base(userRoleFunctionRepository, userRoleFunctionRepository, businessRuleSet)
        {
            _userRoleFunctionRepository = userRoleFunctionRepository;
        }

    }
}