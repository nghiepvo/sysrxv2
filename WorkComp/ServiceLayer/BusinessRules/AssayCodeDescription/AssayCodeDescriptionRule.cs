﻿using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using Framework.BusinessRule;
using Framework.DomainModel;
using Framework.DomainModel.Entities.Common;
using Framework.DomainModel.Interfaces;
using Framework.Service.Translation;
using Repositories.Interfaces;

namespace ServiceLayer.BusinessRules.AssayCodeDescription
{
     public class AssayCodeDescriptionRule<TEntity> : IBusinessRule<TEntity> where TEntity : Entity
    {
         private readonly IAssayCodeDescriptionRepository _assayCodeDescriptionRepository;

        public AssayCodeDescriptionRule(IAssayCodeDescriptionRepository assayCodeDescriptionRepository)
        {
            _assayCodeDescriptionRepository = assayCodeDescriptionRepository;
        }

        public BusinessRuleResult Execute(IEntity instance)
        {
            var failed = false;
            var assayCodeDescription = instance as Framework.DomainModel.Entities.AssayCodeDescription;
            var validationResult = new List<ValidationResult>();

            if (assayCodeDescription == null) return new BusinessRuleResult();

            if (!string.IsNullOrEmpty(assayCodeDescription.Description) && _assayCodeDescriptionRepository.CheckExist(o => o.Description.Trim().ToLower() == assayCodeDescription.Description.Trim().ToLower() && o.Id != assayCodeDescription.Id))
            {
                var mess = string.Format(SystemMessageLookup.GetMessage("ExistsTextResourceKey"), "Description");
                validationResult.Add(new ValidationResult(mess));
                failed = true;
            }
            var result = new BusinessRuleResult(failed, "", instance.GetType().Name, instance.Id, PropertyNames, Name) { ValidationResults = validationResult };
            return result;
        }

        public string Name
        {
            get { return "AssayCodeDescriptionRule"; }
        }

         public string[] PropertyNames { get; set; }
    }
}
