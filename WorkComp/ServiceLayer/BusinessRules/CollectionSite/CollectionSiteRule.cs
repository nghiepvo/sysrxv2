﻿using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using Framework.BusinessRule;
using Framework.DomainModel;
using Framework.DomainModel.Entities.Common;
using Framework.DomainModel.Interfaces;
using Framework.Service.Translation;
using Repositories.Interfaces;

namespace ServiceLayer.BusinessRules.CollectionSite
{
    public class CollectionSiteRule<TEntity> : IBusinessRule<TEntity> where TEntity : Entity
    {
        private readonly ICollectionSiteRepository _collectionSiteRepository;

        public CollectionSiteRule(ICollectionSiteRepository collectionSiteRepository)
        {
            _collectionSiteRepository = collectionSiteRepository;
        }

        public BusinessRuleResult Execute(IEntity instance)
        {
            var failed = false;
            var collectionSite = instance as Framework.DomainModel.Entities.CollectionSite;
            var validationResult = new List<ValidationResult>();

            if (collectionSite == null) return new BusinessRuleResult();

            if (!string.IsNullOrEmpty(collectionSite.Phone) && _collectionSiteRepository.CheckExist(o => o.Phone.Trim().ToLower() == collectionSite.Phone.Trim().ToLower() && o.Id != collectionSite.Id))
            {
                var mess = string.Format(SystemMessageLookup.GetMessage("ExistsTextResourceKey"), "Phone");
                validationResult.Add(new ValidationResult(mess));
                failed = true;
            }

            if (collectionSite.StateId == 0)
            {
                var mess = string.Format(SystemMessageLookup.GetMessage("RequiredTextResourceKey"), "State");
                validationResult.Add(new ValidationResult(mess));
                failed = true;
            }
            if (collectionSite.CityId == 0)
            {
                var mess = string.Format(SystemMessageLookup.GetMessage("RequiredTextResourceKey"), "City");
                validationResult.Add(new ValidationResult(mess));
                failed = true;
            }
            if (collectionSite.ZipId == 0)
            {
                var mess = string.Format(SystemMessageLookup.GetMessage("RequiredTextResourceKey"), "Zip");
                validationResult.Add(new ValidationResult(mess));
                failed = true;
            }
            var result = new BusinessRuleResult(failed, "", instance.GetType().Name, instance.Id, PropertyNames, Name) { ValidationResults = validationResult };
            return result;
        }

        public string Name
        {
            get { return "CollectionSiteRule"; }
        }

        public string[] PropertyNames { get; set; }
    }
}
