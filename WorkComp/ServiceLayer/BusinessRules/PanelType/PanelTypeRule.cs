﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Framework.BusinessRule;
using Framework.DomainModel;
using Framework.DomainModel.Entities.Common;
using Framework.DomainModel.Interfaces;
using Framework.Service.Translation;

namespace ServiceLayer.BusinessRules.PanelType
{
    public class PanelTypeRule<TEntity> : IBusinessRule<TEntity> where TEntity : Entity
    {
        public BusinessRuleResult Execute(IEntity instance)
        {
            bool failed = false;
            var panelType = instance as Framework.DomainModel.Entities.PanelType;
            var validationResult = new List<ValidationResult>();
            if (panelType != null)
            {
                if (panelType.PayerId.GetValueOrDefault() == 0&&!panelType.IsBasic.GetValueOrDefault())
                {
                    var mess = string.Format(SystemMessageLookup.GetMessage("RequiredTextResourceKey"), "Payer");
                    validationResult.Add(new ValidationResult(mess));
                    failed = true;
                }
                var result = new BusinessRuleResult(failed, "", instance.GetType().Name, instance.Id, PropertyNames, Name) { ValidationResults = validationResult };
                return result;
            }

            return new BusinessRuleResult();
        }

        public string Name
        {
            get { return "PanelTypeRule"; }
        }

        public string[] PropertyNames { get; set; }
    }
}
