﻿using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using Framework.BusinessRule;
using Framework.DomainModel;
using Framework.DomainModel.Entities.Common;
using Framework.DomainModel.Interfaces;
using Framework.Service.Translation;
using Repositories.Interfaces;

namespace ServiceLayer.BusinessRules.NpiNumber
{
    public class NpiNumberRule<TEntity> : IBusinessRule<TEntity> where TEntity : Entity
    {
        private readonly INpiNumberRepository _npiNumberRepository;

        public NpiNumberRule(INpiNumberRepository npiNumberRepository)
        {
            _npiNumberRepository = npiNumberRepository;
        }

        public BusinessRuleResult Execute(IEntity instance)
        {
            var failed = false;
            var npiNumber = instance as Framework.DomainModel.Entities.NpiNumber;
            var validationResult = new List<ValidationResult>();

            if (npiNumber == null) return new BusinessRuleResult();

            if (!string.IsNullOrEmpty(npiNumber.Npi) && _npiNumberRepository.CheckExist(o => o.Npi.ToLower().Equals(npiNumber.Npi.ToLower()) && o.Id != npiNumber.Id))
            {
                var mess = string.Format(SystemMessageLookup.GetMessage("ExistsTextResourceKey"), "NPI");
                validationResult.Add(new ValidationResult(mess));
                failed = true;
            }
            if (!string.IsNullOrEmpty(npiNumber.Phone) && _npiNumberRepository.CheckExist(o => o.Phone.ToLower().Equals(npiNumber.Phone.ToLower()) && o.Id != npiNumber.Id))
            {
                var mess = string.Format(SystemMessageLookup.GetMessage("ExistsTextResourceKey"), "Phone");
                validationResult.Add(new ValidationResult(mess));
                failed = true;
            }

            if (!string.IsNullOrEmpty(npiNumber.Email) && _npiNumberRepository.CheckExist(o => o.Email.ToLower().Equals(npiNumber.Email.ToLower()) && o.Id != npiNumber.Id))
            {
                var mess = string.Format(SystemMessageLookup.GetMessage("ExistsTextResourceKey"), "Email");
                validationResult.Add(new ValidationResult(mess));
                failed = true;
            }

            if (npiNumber.StateId == 0)
            {
                var mess = string.Format(SystemMessageLookup.GetMessage("RequiredTextResourceKey"), "State");
                validationResult.Add(new ValidationResult(mess));
                failed = true;
            }

            if (npiNumber.CityId == 0)
            {
                var mess = string.Format(SystemMessageLookup.GetMessage("RequiredTextResourceKey"), "City");
                validationResult.Add(new ValidationResult(mess));
                failed = true;
            }

            if (npiNumber.ZipId == 0)
            {
                var mess = string.Format(SystemMessageLookup.GetMessage("RequiredTextResourceKey"), "Zip");
                validationResult.Add(new ValidationResult(mess));
                failed = true;
            }

            if (!string.IsNullOrEmpty(npiNumber.OrganizationName) || (!string.IsNullOrEmpty(npiNumber.FirstName) && !string.IsNullOrEmpty(npiNumber.LastName)))
            {
                var mess = string.Format(SystemMessageLookup.GetMessage("RequiredTextResourceKeyMultiField"), "Organization or First Name, Last Name");
                validationResult.Add(new ValidationResult(mess));
                failed = true;
            }
            

            var result = new BusinessRuleResult(failed, "", instance.GetType().Name, instance.Id, PropertyNames, Name) { ValidationResults = validationResult };
            return result;
        }

        public string Name
        {
            get { return "NpiNumberRule"; }
        }

        public string[] PropertyNames { get; set; }
    }
}
