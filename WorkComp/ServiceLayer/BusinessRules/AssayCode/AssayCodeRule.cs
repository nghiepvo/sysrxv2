﻿using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using Framework.BusinessRule;
using Framework.DomainModel;
using Framework.DomainModel.Entities.Common;
using Framework.DomainModel.Interfaces;
using Framework.Service.Translation;
using Repositories.Interfaces;

namespace ServiceLayer.BusinessRules.AssayCode
{
    public class AssayCodeRule<TEntity> : IBusinessRule<TEntity> where TEntity : Entity
    {
        private readonly IAssayCodeRepository _assayCodeRepository;

        public AssayCodeRule(IAssayCodeRepository assayCodeRepository)
        {
            _assayCodeRepository = assayCodeRepository;
        }

        public BusinessRuleResult Execute(IEntity instance)
        {
            var failed = false;
            var assayCode = instance as Framework.DomainModel.Entities.AssayCode;
            var validationResult = new List<ValidationResult>();

            if (assayCode == null) return new BusinessRuleResult();

            if (!string.IsNullOrEmpty(assayCode.Code) && _assayCodeRepository.CheckExist(o => o.Code.Trim().ToLower() == assayCode.Code.Trim().ToLower() && o.Id != assayCode.Id))
            {
                var mess = string.Format(SystemMessageLookup.GetMessage("ExistsTextResourceKey"), "Code");
                validationResult.Add(new ValidationResult(mess));
                failed = true;
            }

            if (assayCode.SampleTestingTypeId == 0)
            {
                var mess = string.Format(SystemMessageLookup.GetMessage("RequiredTextResourceKey"), "Testing Type");
                validationResult.Add(new ValidationResult(mess));
                failed = true;
            }

            if (assayCode.AssayCodeDescriptionId == 0)
            {
                var mess = string.Format(SystemMessageLookup.GetMessage("RequiredTextResourceKey"), "Description");
                validationResult.Add(new ValidationResult(mess));
                failed = true;
            }

            var result = new BusinessRuleResult(failed, "", instance.GetType().Name, instance.Id, PropertyNames, Name) { ValidationResults = validationResult };
            return result;
        }

        public string Name
        {
            get { return "AssayCodeRule"; }
        }

        public string[] PropertyNames { get; set; }
    }
}
