﻿using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using Framework.BusinessRule;
using Framework.DomainModel;
using Framework.DomainModel.Entities.Common;
using Framework.DomainModel.Interfaces;
using Framework.Service.Translation;
using Repositories.Interfaces;

namespace ServiceLayer.BusinessRules.Payer
{
    public class PayerRule<TEntity> : IBusinessRule<TEntity> where TEntity : Entity
    {
        private readonly IPayerRepository _payerRepository;

        public PayerRule(IPayerRepository payerRepository)
        {
            _payerRepository = payerRepository;
        }
        public BusinessRuleResult Execute(IEntity instance)
        {
            bool failed = false;
            var payer = instance as Framework.DomainModel.Entities.Payer;
            var validationResult = new List<ValidationResult>();
            if (payer != null)
            {
                if (!string.IsNullOrEmpty(payer.Name) && _payerRepository.CheckExist(o => o.Name.Trim().ToLower() == payer.Name.Trim().ToLower() && o.Id != payer.Id))
                {
                    var mess = string.Format(SystemMessageLookup.GetMessage("ExistsTextResourceKey"), "Name");
                    validationResult.Add(new ValidationResult(mess));
                    failed = true;
                }
                var result = new BusinessRuleResult(failed, "", instance.GetType().Name, instance.Id, PropertyNames, Name) { ValidationResults = validationResult };
                return result;
            }

            return new BusinessRuleResult();
        }

        public string Name
        {
            get { return "PayerRule"; }
        }

        public string[] PropertyNames { get; set; }
    }
}
