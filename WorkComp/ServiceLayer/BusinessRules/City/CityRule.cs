﻿using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using Framework.BusinessRule;
using Framework.DomainModel;
using Framework.DomainModel.Entities.Common;
using Framework.DomainModel.Interfaces;
using Framework.Service.Translation;
using Repositories.Interfaces;

namespace ServiceLayer.BusinessRules.City
{
    public class CityRule<TEntity> : IBusinessRule<TEntity> where TEntity : Entity
    {
        private readonly ICityRepository _cityRepository;

        public CityRule(ICityRepository cityRepository)
        {
            _cityRepository = cityRepository;
        }
        public BusinessRuleResult Execute(IEntity instance)
        {
            bool failed = false;
            var city = instance as Framework.DomainModel.Entities.City;
            var validationResult = new List<ValidationResult>();
            if (city != null)
            {
                if (city.StateId==0)
                {
                    var mess = string.Format(SystemMessageLookup.GetMessage("RequiredTextResourceKey"), "State");
                    validationResult.Add(new ValidationResult(mess));
                    failed = true;
                }
                if (!string.IsNullOrEmpty(city.Name) && _cityRepository.CheckExist(o => o.Name.Trim().ToLower() == city.Name.Trim().ToLower() && o.StateId == city.StateId && o.Id != city.Id))
                {
                    var mess = string.Format(SystemMessageLookup.GetMessage("ItemExistsWithParentItem"), "city", city.Name, "state");
                    validationResult.Add(new ValidationResult(mess));
                    failed = true;
                }
                var result = new BusinessRuleResult(failed, "", instance.GetType().Name, instance.Id, PropertyNames, Name) { ValidationResults = validationResult };
                return result;
            }

            return new BusinessRuleResult();
        }

        public string Name
        {
            get { return "CityRule"; }
        }

        public string[] PropertyNames { get; set; }
    }
}
