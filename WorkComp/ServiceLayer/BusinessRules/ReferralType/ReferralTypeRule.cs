﻿using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using Framework.BusinessRule;
using Framework.DomainModel;
using Framework.DomainModel.Entities.Common;
using Framework.DomainModel.Interfaces;
using Framework.Service.Translation;
using Repositories.Interfaces;

namespace ServiceLayer.BusinessRules.ReferralType
{
    public class ReferralTypeRule<TEntity> : IBusinessRule<TEntity> where TEntity : Entity
    {
        private readonly IReferralTypeRepository _referralTypeRepository;

        public ReferralTypeRule(IReferralTypeRepository referralTypeRepository)
        {
            _referralTypeRepository = referralTypeRepository;
        }

        public BusinessRuleResult Execute(IEntity instance)
        {
            var failed = false;
            var referralType = instance as Framework.DomainModel.Entities.ReferralType;
            var validationResult = new List<ValidationResult>();

            if (referralType == null) return new BusinessRuleResult();

            if (!string.IsNullOrEmpty(referralType.Name) && _referralTypeRepository.CheckExist(o => o.Name.ToLower().Equals(referralType.Name.ToLower()) && o.Id != referralType.Id))
            {
                var mess = string.Format(SystemMessageLookup.GetMessage("ExistsTextResourceKey"), "Name");
                validationResult.Add(new ValidationResult(mess));
                failed = true;
            }
            if (failed)
            {
                var result = new BusinessRuleResult(true, "", instance.GetType().Name, instance.Id, PropertyNames, Name) { ValidationResults = validationResult };
                return result;    
            }
            return new BusinessRuleResult();
        }

        public string Name
        {
            get { return "ReferralTypeRule"; }
        }

        public string[] PropertyNames { get; set; }
    }
}
