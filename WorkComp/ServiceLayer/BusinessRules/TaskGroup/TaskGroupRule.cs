﻿using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using Framework.BusinessRule;
using Framework.DomainModel;
using Framework.DomainModel.Entities.Common;
using Framework.DomainModel.Interfaces;
using Framework.Service.Translation;
using Repositories.Interfaces;

namespace ServiceLayer.BusinessRules.TaskGroup
{
    public class TaskGroupRule<TEntity> : IBusinessRule<TEntity> where TEntity : Entity
    {
        private readonly ITaskGroupRepository _taskGroupRepository;

        public TaskGroupRule(ITaskGroupRepository taskGroupRepository)
        {
            _taskGroupRepository = taskGroupRepository;
        }
        public BusinessRuleResult Execute(IEntity instance)
        {
            bool failed = false;
            var taskGroup = instance as Framework.DomainModel.Entities.TaskGroup;
            var validationResult = new List<ValidationResult>();
            if (taskGroup != null)
            {
                if (!string.IsNullOrEmpty(taskGroup.Name) && _taskGroupRepository.CheckExist(o => o.Name.Trim().ToLower() == taskGroup.Name.Trim().ToLower() && o.Id != taskGroup.Id))
                {
                    var mess = string.Format(SystemMessageLookup.GetMessage("ExistsTextResourceKey"), "Name");
                    validationResult.Add(new ValidationResult(mess));
                    failed = true;
                }
                var result = new BusinessRuleResult(failed, "", instance.GetType().Name, instance.Id, PropertyNames, Name) { ValidationResults = validationResult };
                return result;
            }

            return new BusinessRuleResult();
        }

        public string Name
        {
            get { return "TaskGroupRule"; }
        }

        public string[] PropertyNames { get; set; }
    }
}
