﻿using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using Framework.BusinessRule;
using Framework.DomainModel;
using Framework.DomainModel.Entities.Common;
using Framework.DomainModel.Interfaces;
using Framework.Service.Translation;
using Repositories.Interfaces;

namespace ServiceLayer.BusinessRules.Attorney
{
    public class AttorneyRule<TEntity> : IBusinessRule<TEntity> where TEntity : Entity
    {
        private readonly IAttorneyRepository _attorneyRepository;

        public AttorneyRule(IAttorneyRepository attorneyRepository)
        {
            _attorneyRepository = attorneyRepository;
        }

        public BusinessRuleResult Execute(IEntity instance)
        {
            var failed = false;
            var attorney = instance as Framework.DomainModel.Entities.Attorney;
            var validationResult = new List<ValidationResult>();

            if (attorney == null) return new BusinessRuleResult();

            if (!string.IsNullOrEmpty(attorney.Phone) && _attorneyRepository.CheckExist(o => o.Phone.Trim().ToLower() == attorney.Phone.Trim().ToLower() && o.Id != attorney.Id))
            {
                var mess = string.Format(SystemMessageLookup.GetMessage("ExistsTextResourceKey"), "Phone");
                validationResult.Add(new ValidationResult(mess));
                failed = true;
            }

            if (!string.IsNullOrEmpty(attorney.Email) && _attorneyRepository.CheckExist(o => o.Email.Trim().ToLower() == attorney.Email.Trim().ToLower() && o.Id != attorney.Id))
            {
                var mess = string.Format(SystemMessageLookup.GetMessage("ExistsTextResourceKey"), "Email");
                validationResult.Add(new ValidationResult(mess));
                failed = true;
            }

            

            var result = new BusinessRuleResult(failed, "", instance.GetType().Name, instance.Id, PropertyNames, Name) { ValidationResults = validationResult };
            return result;
        }

        public string Name
        {
            get { return "AttorneyRule"; }
        }

        public string[] PropertyNames { get; set; }
    }
}
