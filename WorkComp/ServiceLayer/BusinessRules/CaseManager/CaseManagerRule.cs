﻿using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using Framework.BusinessRule;
using Framework.DomainModel;
using Framework.DomainModel.Entities.Common;
using Framework.DomainModel.Interfaces;
using Framework.Service.Translation;
using Repositories.Interfaces;

namespace ServiceLayer.BusinessRules.CaseManager
{
    public class CaseManagerRule<TEntity> : IBusinessRule<TEntity> where TEntity : Entity
    {
        private readonly ICaseManagerRepository _caseManagerRepository;

        public CaseManagerRule(ICaseManagerRepository caseManagerRepository)
        {
            _caseManagerRepository = caseManagerRepository;
        }
        public BusinessRuleResult Execute(IEntity instance)
        {
            var failed = false;
            var caseManager = instance as Framework.DomainModel.Entities.CaseManager;
            var validationResult = new List<ValidationResult>();

            if (caseManager == null) return new BusinessRuleResult();

            if (!string.IsNullOrEmpty(caseManager.Name) && _caseManagerRepository.CheckExist(o => o.Name.Trim().ToLower() == caseManager.Name.Trim().ToLower() && o.Id != caseManager.Id))
            {
                var mess = string.Format(SystemMessageLookup.GetMessage("ExistsTextResourceKey"), "Name");
                validationResult.Add(new ValidationResult(mess));
                failed = true;
            }

            if (!string.IsNullOrEmpty(caseManager.Phone) && _caseManagerRepository.CheckExist(o => o.Phone.Trim().ToLower() == caseManager.Phone.Trim().ToLower() && o.Id != caseManager.Id))
            {
                var mess = string.Format(SystemMessageLookup.GetMessage("ExistsTextResourceKey"), "Phone");
                validationResult.Add(new ValidationResult(mess));
                failed = true;
            }

            if (!string.IsNullOrEmpty(caseManager.Email) && _caseManagerRepository.CheckExist(o => o.Email.Trim().ToLower() == caseManager.Email.Trim().ToLower() && o.Id != caseManager.Id))
            {
                var mess = string.Format(SystemMessageLookup.GetMessage("ExistsTextResourceKey"), "Email");
                validationResult.Add(new ValidationResult(mess));
                failed = true;
            }

            if (caseManager.StateId == 0)
            {
                var mess = string.Format(SystemMessageLookup.GetMessage("RequiredTextResourceKey"), "State");
                validationResult.Add(new ValidationResult(mess));
                failed = true;
            }
            if (caseManager.CityId == 0)
            {
                var mess = string.Format(SystemMessageLookup.GetMessage("RequiredTextResourceKey"), "City");
                validationResult.Add(new ValidationResult(mess));
                failed = true;
            }
            if (caseManager.ZipId == 0)
            {
                var mess = string.Format(SystemMessageLookup.GetMessage("RequiredTextResourceKey"), "Zip");
                validationResult.Add(new ValidationResult(mess));
                failed = true;
            }

            var result = new BusinessRuleResult(failed, "", instance.GetType().Name, instance.Id, PropertyNames, Name) { ValidationResults = validationResult };
            return result;
        }

        public string Name
        {
            get { return "CaseManagerRule"; }
        }

        public string[] PropertyNames { get; set; }
    }
}
