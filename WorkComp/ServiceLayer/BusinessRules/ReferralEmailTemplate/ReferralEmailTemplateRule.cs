﻿using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using Framework.BusinessRule;
using Framework.DomainModel;
using Framework.DomainModel.Entities.Common;
using Framework.DomainModel.Interfaces;
using Framework.Service.Translation;
using Repositories.Interfaces;

namespace ServiceLayer.BusinessRules.ReferralEmailTemplate
{
    public class ReferralEmailTemplateRule<TEntity> : IBusinessRule<TEntity> where TEntity : Entity
    {
        private readonly IReferralEmailTemplateRepository _referralEmailTemplateRepository;

        public ReferralEmailTemplateRule(IReferralEmailTemplateRepository referralEmailTemplateRepository)
        {
            _referralEmailTemplateRepository = referralEmailTemplateRepository;
        }

        public BusinessRuleResult Execute(IEntity instance)
        {
            var failed = false;
            var referralEmailTemplate = instance as Framework.DomainModel.Entities.ReferralEmailTemplate;
            var validationResult = new List<ValidationResult>();

            if (referralEmailTemplate == null) return new BusinessRuleResult();

            if (referralEmailTemplate.DateToSendMail == null && referralEmailTemplate.DateToSendFax == null)
            {
                var mess = string.Format(SystemMessageLookup.GetMessage("RequiredTextResourceKey"), "Date to send email or fax");
                validationResult.Add(new ValidationResult(mess));
                failed = true;
            }

            var result = new BusinessRuleResult(failed, "", instance.GetType().Name, instance.Id, PropertyNames, Name) { ValidationResults = validationResult };
            return result;
        }

        public string Name
        {
            get { return "ReferralEmailTemplateRule"; }
        }

        public string[] PropertyNames { get; set; }
    }
}
