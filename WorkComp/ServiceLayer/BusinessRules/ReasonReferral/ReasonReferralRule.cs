﻿using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using Framework.BusinessRule;
using Framework.DomainModel;
using Framework.DomainModel.Entities.Common;
using Framework.DomainModel.Interfaces;
using Framework.Service.Translation;
using Repositories.Interfaces;

namespace ServiceLayer.BusinessRules.ReasonReferral
{
     public class ReasonReferralRule<TEntity> : IBusinessRule<TEntity> where TEntity : Entity
    {
         private readonly IReasonReferralRepository _reasonReferralRepository;

        public ReasonReferralRule(IReasonReferralRepository reasonReferralRepository)
        {
            _reasonReferralRepository = reasonReferralRepository;
        }

        public BusinessRuleResult Execute(IEntity instance)
        {
            var failed = false;
            var reasonReferral = instance as Framework.DomainModel.Entities.ReasonReferral;
            var validationResult = new List<ValidationResult>();

            if (reasonReferral == null) return new BusinessRuleResult();

            if (!string.IsNullOrEmpty(reasonReferral.Name) && _reasonReferralRepository.CheckExist(o => o.Name.Trim().ToLower() == reasonReferral.Name.Trim().ToLower() && o.Id != reasonReferral.Id && o.ParentId == reasonReferral.ParentId))
            {
                var mess = string.Format(SystemMessageLookup.GetMessage("ExistsTextResourceKey"), "Name");
                validationResult.Add(new ValidationResult(mess));
                failed = true;
            }

            
            int level;
            
            var subList = _reasonReferralRepository.Get(o => o.ParentId == reasonReferral.Id).ToList();
            //Check sub item data
            if (subList.Count > 0)
            {
                level = 2;
                foreach (var item in subList)
                {
                    var copyItem1 = item;
                    var subList1 = _reasonReferralRepository.Get(o => o.ParentId == copyItem1.Id).ToList();
                    if (subList1.Count > 0)
                    {
                        level = 3;
                        break;
                    }
                }

                    
            }
            else
            {
                level = 1;
            }

            //check parent data
            var objData = _reasonReferralRepository.Get(o => o.Id == reasonReferral.ParentId).SingleOrDefault();
            if (objData != null)
            {
                var copy1 = objData;
                objData = _reasonReferralRepository.Get(o => o.Id == copy1.ParentId).SingleOrDefault();
                if (objData == null)
                {
                    level = level + 1;
                }
                else
                {
                    var copy2 = objData;
                    objData = _reasonReferralRepository.Get(o => o.Id == copy2.ParentId).SingleOrDefault();
                    level = level + (objData != null ? 3 : 2);
                }
            }
            if (level > 3)
            {
                var mess = string.Format(SystemMessageLookup.GetMessage("MaximumCreateReasonReferral"));
                validationResult.Add(new ValidationResult(mess));
                failed = true;
            }
            var result = new BusinessRuleResult(failed, "", instance.GetType().Name, instance.Id, PropertyNames, Name) { ValidationResults = validationResult };
            return result;
        }


        public string Name
        {
            get { return "ReasonReferralRule"; }
        }

         public string[] PropertyNames { get; set; }
    }
}
