﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using Framework.BusinessRule;
using Framework.DomainModel;
using Framework.DomainModel.Entities.Common;
using Framework.DomainModel.Interfaces;
using Framework.Service.Translation;
using Repositories.Interfaces;

namespace ServiceLayer.BusinessRules.Claimant
{
    public class ClaimantRule<TEntity> : IBusinessRule<TEntity> where TEntity : Entity
    {
        private readonly IClaimantRepository _claimantRepository;

        public ClaimantRule(IClaimantRepository claimantRepository)
        {
            _claimantRepository = claimantRepository;
        }


        public BusinessRuleResult Execute(IEntity instance)
        {
            var failed = false;
            var claimant = instance as Framework.DomainModel.Entities.Claimant;
            var validationResult = new List<ValidationResult>();

            if (claimant == null) return new BusinessRuleResult();

            if (!string.IsNullOrEmpty(claimant.HomePhone) && _claimantRepository.CheckExist(o => o.HomePhone.Trim().ToLower() == claimant.HomePhone.Trim().ToLower() && o.Id != claimant.Id))
            {
                var mess = string.Format(SystemMessageLookup.GetMessage("ExistsTextResourceKey"), "Home Phone");
                validationResult.Add(new ValidationResult(mess));
                failed = true;
            }

            if (!string.IsNullOrEmpty(claimant.CellPhone) && _claimantRepository.CheckExist(o => o.CellPhone.Trim().ToLower() == claimant.CellPhone.Trim().ToLower() && o.Id != claimant.Id))
            {
                var mess = string.Format(SystemMessageLookup.GetMessage("ExistsTextResourceKey"), "Cell Phone");
                validationResult.Add(new ValidationResult(mess)); 
                failed = true;
            }

            if (!string.IsNullOrEmpty(claimant.WorkPhone) && _claimantRepository.CheckExist(o => o.WorkPhone.Trim().ToLower() == claimant.WorkPhone.Trim().ToLower() && o.Id != claimant.Id))
            {
                var mess = string.Format(SystemMessageLookup.GetMessage("ExistsTextResourceKey"), "Work Phone");
                validationResult.Add(new ValidationResult(mess));
                failed = true;
            }

            if (claimant.StateId == 0)
            {
                var mess = string.Format(SystemMessageLookup.GetMessage("RequiredTextResourceKey"), "State");
                validationResult.Add(new ValidationResult(mess));
                failed = true;
            }
            if (claimant.CityId == 0)
            {
                var mess = string.Format(SystemMessageLookup.GetMessage("RequiredTextResourceKey"), "City");
                validationResult.Add(new ValidationResult(mess));
                failed = true;
            }
            if (claimant.ZipId == 0)
            {
                var mess = string.Format(SystemMessageLookup.GetMessage("RequiredTextResourceKey"), "Zip");
                validationResult.Add(new ValidationResult(mess));
                failed = true;
            }

            if (claimant.ExpirationDate == DateTime.MinValue)
            {
                var mess = string.Format(SystemMessageLookup.GetMessage("DateTimeValid"), "Expiration Date");
                validationResult.Add(new ValidationResult(mess));
                failed = true;
            }

            if (claimant.Birthday == DateTime.MinValue)
            {
                var mess = string.Format(SystemMessageLookup.GetMessage("DateTimeValid"), "Day of Birth");
                validationResult.Add(new ValidationResult(mess));
                failed = true;
            }

            var result = new BusinessRuleResult(failed, "", instance.GetType().Name, instance.Id, PropertyNames, Name) { ValidationResults = validationResult };
            return result;
        }

        public string Name
        {
            get { return "ClaimantRule"; }
        }

        public string[] PropertyNames { get; set; }
    }
}
