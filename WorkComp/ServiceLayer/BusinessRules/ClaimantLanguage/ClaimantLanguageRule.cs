﻿using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using Framework.BusinessRule;
using Framework.DomainModel;
using Framework.DomainModel.Entities.Common;
using Framework.DomainModel.Interfaces;
using Framework.Service.Translation;
using Repositories.Interfaces;

namespace ServiceLayer.BusinessRules.ClaimantLanguage
{
    public class ClaimantLanguageRule<TEntity> : IBusinessRule<TEntity> where TEntity : Entity
    {
        private readonly IClaimantLanguageRepository _claimantLanguageRepository;

        public ClaimantLanguageRule(IClaimantLanguageRepository claimantLanguageRepository)
        {
            _claimantLanguageRepository = claimantLanguageRepository;
        }

        public BusinessRuleResult Execute(IEntity instance)
        {
            var failed = false;
            var claimantLanguage = instance as Framework.DomainModel.Entities.ClaimantLanguage;
            var validationResult = new List<ValidationResult>();

            if (claimantLanguage == null) return new BusinessRuleResult();

            if (!string.IsNullOrEmpty(claimantLanguage.Name) && _claimantLanguageRepository.CheckExist(o => o.Name.Trim().ToLower() == claimantLanguage.Name.Trim().ToLower() && o.Id != claimantLanguage.Id))
            {
                var mess = string.Format(SystemMessageLookup.GetMessage("ExistsTextResourceKey"), "Name");
                validationResult.Add(new ValidationResult(mess));
                failed = true;
            }

            var result = new BusinessRuleResult(failed, "", instance.GetType().Name, instance.Id, PropertyNames, Name) { ValidationResults = validationResult };
            return result;
        }

        public string Name
        {
            get { return "ClaimantLanguageRule"; }
        }

        public string[] PropertyNames { get; set; }
    }
}
