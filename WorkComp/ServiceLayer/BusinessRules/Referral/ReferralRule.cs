﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using Framework.BusinessRule;
using Framework.DomainModel;
using Framework.DomainModel.Entities.Common;
using Framework.DomainModel.Interfaces;
using Framework.Service.Translation;
using Repositories.Interfaces;

namespace ServiceLayer.BusinessRules.Referral
{
    public class ReferralRule<TEntity> : IBusinessRule<TEntity> where TEntity : Entity
    {
        private readonly IReferralRepository _referralRepository;
        private readonly IClaimNumberRepository _claimNumberRepository;
        private readonly IClaimantRepository _claimantRepository;
        private readonly IAdjusterRepository _adjusterRepository;

        public ReferralRule(IReferralRepository referralRepository, IClaimNumberRepository claimNumberRepository, IClaimantRepository claimantRepository, IAdjusterRepository adjusterRepository)
        {
            _referralRepository = referralRepository;
            _claimNumberRepository = claimNumberRepository;
            _claimantRepository = claimantRepository;
            _adjusterRepository = adjusterRepository;
        }

        public BusinessRuleResult Execute(IEntity instance)
        {
            var failed = false;
            var referral = instance as Framework.DomainModel.Entities.Referral;
            var validationResult = new List<ValidationResult>();

            if (referral == null) return new BusinessRuleResult();
            if (!string.IsNullOrEmpty(referral.ControlNumber) && _referralRepository.CheckExist(o => o.ControlNumber.Trim().ToLower() == referral.ControlNumber.Trim().ToLower() && o.Id != referral.Id))
            {
                var mess = string.Format(SystemMessageLookup.GetMessage("ExistsTextResourceKey"), "Control Number");
                validationResult.Add(new ValidationResult(mess));
                failed = true;
            }
            if (referral.ReferralSourceId == 0)
            {
                var mess = string.Format(SystemMessageLookup.GetMessage("RequiredTextResourceKey"), "Referral Source");
                validationResult.Add(new ValidationResult(mess));
                failed = true;
            }
            if (referral.ClaimantNumberId == 0)
            {
                var mess = string.Format(SystemMessageLookup.GetMessage("RequiredTextResourceKey"), "Claim Number");
                validationResult.Add(new ValidationResult(mess));
                failed = true;
            }

            if (referral.ClaimantNumberId != 0)
            {
                var objClaimNumber = _claimNumberRepository.GetById(referral.ClaimantNumberId);
                if (objClaimNumber != null && (objClaimNumber.Status == 2 || objClaimNumber.Status == 4))
                {
                    var mess = string.Format(SystemMessageLookup.GetMessage("IsClosed"), "Claim#");
                    validationResult.Add(new ValidationResult(mess));
                    failed = true;
                }
                if (objClaimNumber!= null)
                {
                    var objClaimant = _claimantRepository.GetById(objClaimNumber.ClaimantId.GetValueOrDefault());
                    if (objClaimant!= null && objClaimant.ExpirationDate != null)
                    {
                        var mess = string.Format(SystemMessageLookup.GetMessage("ClaimantExpiration"), "Claimant");
                        validationResult.Add(new ValidationResult(mess));
                        failed = true;
                    }
                }

                //if (objClaimNumber != null)
                //{
                //    var objAdjuster = _adjusterRepository.GetById(objClaimNumber.AdjusterId.GetValueOrDefault());
                //    if (objAdjuster != null && objAdjuster.AllowCreateReferral != true)
                //    {
                //        var mess = string.Format(SystemMessageLookup.GetMessage("NotAllowCreateReferral"), "Adjuster");
                //        validationResult.Add(new ValidationResult(mess));
                //        failed = true;
                //    }
                //}
            }

            if (referral.ProductTypeId == 0)
            {
                var mess = string.Format(SystemMessageLookup.GetMessage("RequiredTextResourceKey"), "Product Type");
                validationResult.Add(new ValidationResult(mess));
                failed = true;
            }
            if (referral.AssignToId == 0)
            {
                var mess = string.Format(SystemMessageLookup.GetMessage("RequiredTextResourceKey"), "Assign To");
                validationResult.Add(new ValidationResult(mess));
                failed = true;
            }
            if (referral.IsCollectionSite.GetValueOrDefault())
            {
                var flagReferralCollectionSites = false;
                if (referral.ReferralCollectionSites.Count == 0)
                {
                    flagReferralCollectionSites = true;
                }
                else
                {
                    var objReferralCollectionSite = referral.ReferralCollectionSites.FirstOrDefault();
                    if (objReferralCollectionSite!= null && objReferralCollectionSite.CollectionSiteId.GetValueOrDefault() == 0)
                    {
                        flagReferralCollectionSites = true;
                    }
                }
                if (flagReferralCollectionSites)
                {
                    var mess = string.Format(SystemMessageLookup.GetMessage("RequiredTextResourceKey"), "Collection Site");
                    validationResult.Add(new ValidationResult(mess));
                    failed = true;
                }
            }
            else
            {
                var flagReferralNpiNumbers = false;
                if (referral.ReferralNpiNumbers.Count == 0)
                {
                    flagReferralNpiNumbers = true;
                }
                else
                {
                    var objReferralNpiNumber = referral.ReferralNpiNumbers.FirstOrDefault();
                    if (objReferralNpiNumber != null && objReferralNpiNumber.NpiNumberId.GetValueOrDefault() == 0)
                    {
                        flagReferralNpiNumbers = true;
                    }
                }
                if (flagReferralNpiNumbers)
                {
                    var mess = string.Format(SystemMessageLookup.GetMessage("RequiredTextResourceKey"), "Treating Physician");
                    validationResult.Add(new ValidationResult(mess));
                    failed = true;
                }
            }

            if (referral.ReferralTasks.Count == 0)
            {
                var mess = string.Format(SystemMessageLookup.GetMessage("RequiredTextResourceKey"), "Task List").Replace("field ", "");
                validationResult.Add(new ValidationResult(mess));
                failed = true;
            }

            if (referral.Id==0&&referral.DueDate.Date < DateTime.Now.Date)
            {
                var mess = string.Format(SystemMessageLookup.GetMessage("CannotGreaterThanText"), "Current Date", "Referral Follow Up Date");
                validationResult.Add(new ValidationResult(mess));
                failed = true;
            }
            if (referral.ReceivedDate > referral.DueDate)
            {
                var mess = string.Format(SystemMessageLookup.GetMessage("CannotGreaterThanText"), "Referral Recieved Date", "Referral Follow Up Date");
                validationResult.Add(new ValidationResult(mess));
                failed = true;
            }
            // Check if referral is completed => all task of referral must be completed
            if (referral.StatusId == 3)
            {
                if (referral.ReferralTasks.Any(o => o.StatusId == 1 || o.StatusId == 4 || o.StatusId == 5))
                {
                    var mess = string.Format(SystemMessageLookup.GetMessage("ReferralCompleteHaveTaskNotComplete"));
                    validationResult.Add(new ValidationResult(mess));
                    failed = true;
                }
            }

            if (referral.PanelTypeId.GetValueOrDefault() == 0)
            {
                var mess = string.Format(SystemMessageLookup.GetMessage("RequiredTextResourceKey"), "Panel Type");
                validationResult.Add(new ValidationResult(mess));
                failed = true;
            }

            
            

            var result = new BusinessRuleResult(failed, "", instance.GetType().Name, instance.Id, PropertyNames, Name) { ValidationResults = validationResult };
            return result;
        }

        public string Name
        {
            get { return "ReferralRule"; }
        }

        public string[] PropertyNames { get; set; }
    }
}
