﻿using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using Framework.BusinessRule;
using Framework.DomainModel;
using Framework.DomainModel.Entities.Common;
using Framework.DomainModel.Interfaces;
using Framework.Service.Translation;
using Repositories.Interfaces;

namespace ServiceLayer.BusinessRules.State
{
    public class StateRule<TEntity> : IBusinessRule<TEntity> where TEntity : Entity
    {
        private readonly IStateRepository _stateRepository;

        public StateRule(IStateRepository stateRepository)
        {
            _stateRepository = stateRepository;
        }
        public BusinessRuleResult Execute(IEntity instance)
        {
            bool failed = false;
            var state = instance as Framework.DomainModel.Entities.State;
            var validationResult = new List<ValidationResult>();
            if (state != null)
            {
                if (!string.IsNullOrEmpty(state.Name) && _stateRepository.CheckExist(o => o.Name.Trim().ToLower() == state.Name.Trim().ToLower() && o.Id != state.Id))
                {
                    var mess = string.Format(SystemMessageLookup.GetMessage("ExistsTextResourceKey"), "Name");
                    validationResult.Add(new ValidationResult(mess));
                    failed = true;
                }
                if (!string.IsNullOrEmpty(state.AbbreviationName) && _stateRepository.CheckExist(o => o.AbbreviationName.Trim().ToLower() == state.AbbreviationName.Trim().ToLower() && o.Id != state.Id))
                {
                    var mess = string.Format(SystemMessageLookup.GetMessage("ExistsTextResourceKey"), "Abbreviation Name");
                    validationResult.Add(new ValidationResult(mess));
                    failed = true;
                }
                var result = new BusinessRuleResult(failed, "", instance.GetType().Name, instance.Id, PropertyNames, Name) { ValidationResults = validationResult };
                return result;
            }

            return new BusinessRuleResult();
        }

        public string Name
        {
            get { return "StateRule"; }
        }

        public string[] PropertyNames { get; set; }
    }
}
