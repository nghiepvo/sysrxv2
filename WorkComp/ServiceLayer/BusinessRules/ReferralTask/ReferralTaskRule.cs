﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Framework.BusinessRule;
using Framework.DomainModel;
using Framework.DomainModel.Entities.Common;
using Framework.DomainModel.Interfaces;
using Framework.Service.Translation;
using Repositories.Interfaces;

namespace ServiceLayer.BusinessRules.ReferralTask
{
    public class ReferralTaskRule<TEntity> : IBusinessRule<TEntity> where TEntity : Entity
    {
        private readonly IReferralTaskRepository _referralTaskRepository;

        public ReferralTaskRule(IReferralTaskRepository referralTaskRepository)
        {
            _referralTaskRepository = referralTaskRepository;
        }
        public BusinessRuleResult Execute(IEntity instance)
        {
            bool failed = false;
            var task = instance as Framework.DomainModel.Entities.ReferralTask;
            var validationResult = new List<ValidationResult>();

            if (task == null) return new BusinessRuleResult();

            if (task.ReferralId == 0)
            {
                var mess = string.Format(SystemMessageLookup.GetMessage("RequiredTextResourceKey"), "Referral");
                validationResult.Add(new ValidationResult(mess));
                failed = true;
            }

            if (task.AssignToId.GetValueOrDefault() == 0)
            {
                var mess = string.Format(SystemMessageLookup.GetMessage("RequiredTextResourceKey"), "Assign To");
                validationResult.Add(new ValidationResult(mess));
                failed = true;
            }

            if (task.StatusId == 0)
            {
                var mess = string.Format(SystemMessageLookup.GetMessage("RequiredTextResourceKey"), "Status");
                validationResult.Add(new ValidationResult(mess));
                failed = true;
            }

            if (string.IsNullOrEmpty(task.Title))
            {
                var mess = string.Format(SystemMessageLookup.GetMessage("RequiredTextResourceKey"), "Title");
                validationResult.Add(new ValidationResult(mess));
                failed = true;
            }

            if (task.StartDate == DateTime.MinValue)
            {
                var mess = string.Format(SystemMessageLookup.GetMessage("DateTimeValid"), "Start Date");
                validationResult.Add(new ValidationResult(mess));
                failed = true;
            }

            if (task.DueDate == DateTime.MinValue)
            {
                var mess = string.Format(SystemMessageLookup.GetMessage("DateTimeValid"), "End Date");
                validationResult.Add(new ValidationResult(mess));
                failed = true;
            }

            if (task.StartDate > task.DueDate)
            {
                var mess = string.Format(SystemMessageLookup.GetMessage("CannotGreaterThanText"), "Task Start Date", "Task Follow Up Date");
                validationResult.Add(new ValidationResult(mess));
                failed = true;
            }

            var result = new BusinessRuleResult(failed, "", instance.GetType().Name, instance.Id, PropertyNames, Name) { ValidationResults = validationResult };
            return result;
        }

        public string Name
        {
            get { return "ReferralTaskRule"; }
        }

        public string[] PropertyNames { get; set; }
    }
}
