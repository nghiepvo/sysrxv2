﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Transactions;
using Framework.BusinessRule;
using Framework.DomainModel.Entities;
using Framework.DomainModel.Entities.Common;
using Framework.DomainModel.Interfaces;
using Framework.DomainModel.ValueObject;
using Framework.Exceptions;
using Framework.Service.Translation;
using Repositories.Interfaces;
using ServiceLayer.Interfaces;
using ServiceLayer.Interfaces.Authentication;
using Solr.DomainModel;
using Solr.ServiceLayer.Interfaces;
using SolrNet;

namespace ServiceLayer
{
    public class ReferralTaskService : MasterFileService<ReferralTask>, IReferralTaskService
    {
        private readonly IReferralTaskRepository _referralTaskRepository;
        private readonly IDiaryRepository _diaryRepository;
        private readonly IReferralRepository _referralRepository;
        private readonly ISolrReferralTaskService _referralTaskSolrService;
        private readonly IAlertRepository _alertRepository;
        private readonly IUserRepository _userRepository;
        private readonly IAuthenticationService _authenticationService;
        public ReferralTaskService(IReferralTaskRepository referralTaskRepository, IDiaryRepository diaryRepository, 
                                    ISolrReferralTaskService referralTaskSolrService, IReferralRepository referralRepository,
                                    IAlertRepository alertRepository, IUserRepository userRepository, IAuthenticationService authenticationService,
                                    IBusinessRuleSet<ReferralTask> businessRuleSet = null)
            : base(referralTaskRepository, referralTaskRepository, businessRuleSet)
        {
            _referralTaskRepository = referralTaskRepository;
            _referralTaskSolrService = referralTaskSolrService;
            _referralRepository = referralRepository;
            _alertRepository = alertRepository;
            _userRepository = userRepository;
            _authenticationService = authenticationService;
            _diaryRepository = diaryRepository;
        }

        public dynamic GetDataParentForGrid(IQueryInfo queryInfo)
        {
            var objData = _referralTaskSolrService.GetDataForGridMasterfile(queryInfo);
            var result = objData ?? _referralTaskRepository.GetDataParentForGrid(queryInfo);
            return result;
        }

        public void AddMultiple(List<ReferralTask> listEntity, int referralId)
        {
            using (var objTransaction = new TransactionScope())
            {
                var referralItem = _referralRepository.FirstOrDefault(o => o.Id == referralId);
                if (referralItem == null)
                {
                    return;
                }
                foreach (var model in listEntity)
                {
                    if (model.AssignToId.GetValueOrDefault() == 0)
                    {
                        model.AssignToId = referralItem.AssignToId;
                    }

                    var objAddItem = base.Add(model);

                    if (objAddItem.Id > 0)
                    {
                        var objDiary = new Diary
                        {
                            Heading = SystemMessageLookup.GetMessage("DiaryHeadingAdd"),
                            Reason = SystemMessageLookup.GetMessage("DiaryReasonAddTask"),
                            Comment = string.Format(SystemMessageLookup.GetMessage("DiaryCommentTask"), objAddItem.Title, objAddItem.Description),
                            ReferralId = model.ReferralId,
                        };
                        _diaryRepository.Add(objDiary);
                        _diaryRepository.Commit();

                        // Create alert for rush
                        if (objAddItem.Rush.GetValueOrDefault() && objAddItem.StatusId == 1)
                        {
                            var objAlertRush = new Alert
                            {
                                StatusId = objAddItem.StatusId,
                                Type = 2,
                                Title = SystemMessageLookup.GetMessage("DiaryRush"),
                                Message = SystemMessageLookup.GetMessage("DiaryRush"),
                                LinkId = objAddItem.Id,
                                AssignToId = objAddItem.AssignToId.GetValueOrDefault(),
                                IsRush = true
                            };
                            _alertRepository.Add(objAlertRush);
                            _alertRepository.Commit();
                        }
                        //Write to solr
                        var objSolrItem = _referralTaskRepository.MapReferralTaskToSolrReferral(objAddItem.Id);
                        if (objSolrItem != null)
                        {
                            _referralTaskSolrService.Add(objSolrItem);
                            _referralTaskSolrService.Commit();
                        }
                    }
                }
                objTransaction.Complete();
            }
        }

        public void CompletedTask(int id)
        {
            using (var objTransaction = new TransactionScope())
            {
                _referralTaskRepository.CompletedTask(id);
                _referralTaskRepository.Commit();
                //Write to solr
                var objSolrItem = _referralTaskSolrService.GetById(id);
                if (objSolrItem != null)
                {
                    objSolrItem.Status = "Completed";
                    objSolrItem.CompletedDate = DateTime.Now;
                    objSolrItem.CancelDate = null;
                    _referralTaskSolrService.Add(objSolrItem);
                    _referralTaskSolrService.Commit();
                }

                var objAlert = _alertRepository.Get(o => o.LinkId == id && o.Type == 2).FirstOrDefault();
                if (objAlert!= null)
                {
                    _alertRepository.Delete(objAlert);
                    _alertRepository.Commit();
                }

                objTransaction.Complete();
            }

        }

        public override ReferralTask Add(ReferralTask model)
        {
            using (var objTransaction = new TransactionScope())
            {
                var objAddItem = base.Add(model);

                if (objAddItem.Id > 0)
                {
                    var objDiary = new Diary
                    {
                        Heading = SystemMessageLookup.GetMessage("DiaryHeadingAdd"),
                        Reason = SystemMessageLookup.GetMessage("DiaryReasonAddTask"),
                        Comment = string.Format(SystemMessageLookup.GetMessage("DiaryCommentTask"), model.Title, model.Description),
                        ReferralId = model.ReferralId,
                    };
                    _diaryRepository.Add(objDiary);
                    _diaryRepository.Commit();
                    //Write to solr
                    var objSolrItem = _referralTaskRepository.MapReferralTaskToSolrReferral(objAddItem.Id);
                    if (objSolrItem != null)
                    {
                        _referralTaskSolrService.Add(objSolrItem);
                        _referralTaskSolrService.Commit();
                    }
                }
                objTransaction.Complete();
                return objAddItem;
            }
        }

        public override ReferralTask Update(ReferralTask model)
        {
            using (var objTransaction = new TransactionScope())
            {
                base.Update(model);
                //Write to solr
                var objSolrItem = _referralTaskRepository.MapReferralTaskToSolrReferral(model.Id);
                if (objSolrItem != null)
                {
                    _referralTaskSolrService.Update(objSolrItem);
                    _referralTaskSolrService.Commit();
                }
                if (model.Rush == true)
                {
                    var objAlert = _alertRepository.Get(o => o.LinkId == model.Id && o.Type == 2).FirstOrDefault();
                    if (objAlert != null)
                    {
                        if (model.StatusId != 1)
                        {
                            _alertRepository.Delete(objAlert);
                            _alertRepository.Commit();
                        }
                    }
                    else
                    {
                        if (model.StatusId == 1)
                        {
                            _alertRepository.Add(new Alert
                            {
                                AssignToId = model.AssignToId.GetValueOrDefault(),
                                Type = 2,
                                Title = SystemMessageLookup.GetMessage("DiaryRush"),
                                Message = SystemMessageLookup.GetMessage("DiaryRush"),
                                LinkId = model.Id,
                                IsRush = true,
                                StatusId = model.StatusId
                            });
                            _alertRepository.Commit();
                        }
                    }
                }
                
                objTransaction.Complete();
                return model;
            }
        }
        public override List<LookupItemVo> GetLookup(LookupQuery query, Func<ReferralTask, LookupItemVo> selector)
        {
            var objData = _referralTaskSolrService.GetLookup(query);
            var result = objData ?? base.GetLookup(query, selector);
            return result;
        }


        public int GetPreviousReferralTaskId(int id, int referralId)
        {
            return _referralTaskRepository.GetPreviousReferralTaskId(id,referralId);
        }

        public int GetNextReferralTaskId(int id, int referralId)
        {
            return _referralTaskRepository.GetNextReferralTaskId(id, referralId);
        }

        public void AssignToForReferralTask(List<int> listIdSelected, bool isSelectAll, int? assignToId, string typeWithUser)
        {
            var validationResult = new List<ValidationResult>();
            var failed = false;
            if (assignToId.GetValueOrDefault() == 0)
            {
                var mess = string.Format(SystemMessageLookup.GetMessage("RequiredTextResourceKey"), "Assign To");
                validationResult.Add(new ValidationResult(mess));
                failed = true;
            }
            if (failed)
            {
                var result = new BusinessRuleResult(true, "", "ReferralTaskAssignTo", 0, null, "ReferralTaskAssignToRule") { ValidationResults = validationResult };
                throw new BusinessRuleException("BussinessGenericErrorMessageKey", new[] { result });
            }
            var enumTypeWithUser = string.IsNullOrEmpty(typeWithUser)
                ? TypeWithUserQueryEnum.All
                : (TypeWithUserQueryEnum)Enum.Parse(typeof(TypeWithUserQueryEnum), typeWithUser);
            var objAssignTo = _userRepository.GetById(assignToId.GetValueOrDefault());
            if (objAssignTo == null)
            {
                return;
            }
            using (var objTransaction = new TransactionScope())
            {
                _referralTaskRepository.AssignToForReferralTask(listIdSelected, isSelectAll, assignToId, enumTypeWithUser);
                _referralTaskRepository.Commit();
                //Uddate to solr
                var listReferralTask = new List<SolrReferralTask>();
                if (isSelectAll)
                {
                    var currentUser = _authenticationService.GetCurrentUser();
                    if (currentUser == null)
                    {
                        return;
                    }
                    switch (enumTypeWithUser)
                    {
                        case TypeWithUserQueryEnum.All:
                            listReferralTask = _referralTaskSolrService.ListAll().ToList();
                            break;
                        case TypeWithUserQueryEnum.Current:
                            listReferralTask =
                                _referralTaskSolrService.GetByField(new SolrQueryByField("assigntoid", currentUser.User.Id.ToString())).ToList();
                            break;
                        case TypeWithUserQueryEnum.Other:
                            listReferralTask =
                                _referralTaskSolrService.GetByField(new SolrQueryByField("-assigntoid", currentUser.User.Id.ToString())).ToList();
                            break;
                    }

                }
                else if (listIdSelected.Count > 0)
                {
                    foreach (var id in listIdSelected)
                    {
                        var objAdd = _referralTaskSolrService.GetById(id);
                        if (objAdd != null)
                        {
                            listReferralTask.Add(objAdd);
                        }
                    }
                }
                if (listReferralTask.Count > 0)
                {
                    foreach (var solrReferralTask in listReferralTask)
                    {
                        solrReferralTask.AssignToId = assignToId;
                        solrReferralTask.AssignToName = objAssignTo.FirstName + " " + (objAssignTo.MiddleName ?? "") +
                                                    objAssignTo.LastName;
                        _referralTaskSolrService.Update(solrReferralTask);
                    }
                    _referralTaskSolrService.Commit();
                }
                objTransaction.Complete();
            }

        }

        public dynamic GetDataPrintToReferralTask(List<int> listIdSelected, ReferralQueryInfo queryInfo)
        {
            var objData = _referralTaskSolrService.GetDataPrint(listIdSelected, queryInfo);
            var result = objData ?? _referralTaskRepository.GetDataPrint(listIdSelected, queryInfo);
            return result;
        }

        public bool UpdateComplateTask(ReferralTask referralTask)
        {
            return _referralTaskRepository.UpdateComplateTask(referralTask);
        }
    }
}