﻿using System;
using System.Collections.Generic;
using System.Linq;
using Framework.DomainModel.Entities;
using Framework.DomainModel.Entities.Common;
using Framework.DomainModel.Entities.Security;
using Framework.DomainModel.Interfaces;
using Framework.Web;
using Repositories.Interfaces;
using ServiceLayer.Common;

namespace ServiceLayer.Authorization
{
    public class OperationAuthorization : IOperationAuthorization
    {
        private IWorkCompHttpContext _workcompContext;
        private IUserRepository _userRepository;
        private IUserRoleFunctionRepository _userRoleFunctionRepostory;

        public OperationAuthorization(IWorkCompHttpContext workcompContext,
                                      IUserRoleFunctionRepository userRoleFunctionRepostory,
                                      IUserRepository userRepository)
        {
            _workcompContext = workcompContext;
            _userRoleFunctionRepostory = userRoleFunctionRepostory;
            _userRepository = userRepository;
        }
        public bool VerifyAccess(DocumentTypeKey documentType, OperationAction action,
                                       out List<UserRoleFunction> permissionOfThisView)
        {
            var principal = _workcompContext.User as IWorkcompPrincipal;
            var hasPermission = false;
            permissionOfThisView = null;

            if (principal != null && principal.User != null)
            {
                var userRoleId = principal.User.UserRoleId;

                var userGroupRights = MenuExtractData.Instance.LoadUserSecurityRoleFunction(userRoleId,
                    (int) documentType);
                if (userGroupRights == null)
                {
                    userGroupRights = _userRoleFunctionRepostory.LoadUserSecurityRoleFunction(userRoleId, (int)documentType);
                }

                if (userGroupRights != null)
                {
                    hasPermission = userGroupRights.Any(r => r.SecurityOperationId == (int)action);
                    permissionOfThisView = userGroupRights.ToList();
                }

            }

            return hasPermission;
        }
    }
}