﻿using Framework.BusinessRule;
using Framework.DomainModel.Entities;
using Repositories.Interfaces;
using ServiceLayer.Interfaces;

namespace ServiceLayer
{
    public class ReferralTypeService : MasterFileService<ReferralType>, IReferralTypeService
    {
        private readonly IReferralTypeRepository _referralTypeRepository;

        public ReferralTypeService(IReferralTypeRepository referralTypeRepository, IBusinessRuleSet<ReferralType> businessRuleSet = null)
            : base(referralTypeRepository, referralTypeRepository, businessRuleSet)
        {
            _referralTypeRepository = referralTypeRepository;
        }

    }
}