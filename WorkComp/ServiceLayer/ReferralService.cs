﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Text;
using System.Transactions;
using Framework.BusinessRule;
using Framework.DomainModel.Entities;
using Framework.DomainModel.Entities.Common;
using Framework.DomainModel.Interfaces;
using Framework.DomainModel.ValueObject;
using Framework.Exceptions;
using Framework.Service.Translation;
using Framework.Utility;
using Repositories.Interfaces;
using ServiceLayer.Interfaces;
using ServiceLayer.Interfaces.Authentication;
using Solr.DomainModel;
using Solr.ServiceLayer.Interfaces;
using SolrNet;

namespace ServiceLayer
{
    public class ReferralService : MasterFileService<Referral>, IReferralService
    {
        private readonly IReferralRepository _referralRepository;
        private readonly ISolrReferralService _referralSolrService;
        private readonly ISolrReferralTaskService _referralTaskSolrService;
        private readonly ITaskGroupRepository _taskGroupRepository;
        private readonly IReferralTaskRepository _referralTaskRepository;
        private readonly IProductTypeRepository _productTypeRepository;
        private readonly IPanelTypeRepository _panelTypeRepository;
        private readonly IReferralSourceRepository _referralSourceRepository;
        private readonly IAlertRepository _alertRepository;
        private readonly IUserRepository _userRepository;
        private readonly IAuthenticationService _authenticationService;
        private readonly IDiaryRepository _diaryRepository;
        public ReferralService(IReferralRepository referralRepository, IReferralTaskRepository referralTaskRepository,
                                IReferralSourceRepository referralSourceRepository, IProductTypeRepository productTypeRepository,
                                IPanelTypeRepository panelTypeRepository, IAlertRepository alertRepository,
                                ISolrReferralService referralSolrService, ISolrReferralTaskService referralTaskSolrService,
                                ITaskGroupRepository taskGroupRepository, IAuthenticationService authenticationService, IUserRepository userRepository, IDiaryRepository diaryRepository, IBusinessRuleSet<Referral> businessRuleSet = null)
            : base(referralRepository, referralRepository, businessRuleSet)
        {
            _referralRepository = referralRepository;
            _referralSolrService = referralSolrService;
            _taskGroupRepository = taskGroupRepository;
            _authenticationService = authenticationService;
            _userRepository = userRepository;
            _diaryRepository = diaryRepository;
            _referralTaskSolrService = referralTaskSolrService;
            _referralTaskRepository = referralTaskRepository;
            _referralSourceRepository = referralSourceRepository;
            _productTypeRepository = productTypeRepository;
            _panelTypeRepository = panelTypeRepository;
            _alertRepository = alertRepository;
        }

        public List<ReferralMedicationHistory> GetMedicationHistoriesByReferral(int referralId)
        {
            return _referralRepository.GetMedicationHistoriesByReferral(referralId);
        }

        public ReferralResultDetailVo GetReferralResultInfo(int referralId)
        {
            return _referralRepository.GetReferralResultInfo(referralId);
        }

        public dynamic GetDataParentForGrid(IQueryInfo queryInfo)
        {
            var objData = _referralSolrService.GetDataForGridMasterfile(queryInfo);
            var result = objData ?? _referralRepository.GetDataParentForGrid(queryInfo);
            return result;
            //return _referralRepository.GetDataParentForGrid(queryInfo);
        }



        public dynamic GetDataChildrenForGrid(IQueryInfo queryInfo)
        {
            return _referralRepository.GetDataChildrenForGrid(queryInfo);
        }

        public ReferralDetailVo GetReferralDeltailById(int id)
        {
            return _referralRepository.GetReferralDeltailById(id);
        }

        public List<ReasonReferralInReferralDetailVo> GetReasonReferralInfos(int id)
        {
            return _referralRepository.GetReasonReferralInfos(id);
        }

        public void RemoveAttachmentFile(int referralId, string rowGuid, string fileName)
        {
            using (var objTransaction = new TransactionScope())
            {
                _diaryRepository.Add(new Diary
                {
                    Heading = "Deleted",
                    Reason = "Delete Attachment File",
                    Comment =
                        string.Format("Delete attachment file: '{0}' on {1}", fileName,DateTime.Now.ToString("MM/dd/yyyy hh:mm tt")),
                    ReferralId = referralId
                });
                _diaryRepository.Commit();
                _referralRepository.RemoveAttachmentFile(referralId, rowGuid, fileName);
                _referralRepository.Commit();
                objTransaction.Complete();
            }
        }

        public void AddReferralAttachment(ReferralAttachment objAdd)
        {
            using (var objTransaction = new TransactionScope())
            {
                _diaryRepository.Add(new Diary
                {
                    Heading = "Added",
                    Reason = "New Attachment File",
                    Comment =
                        string.Format("Add new attachment file: '{0}', size: {1}.", objAdd.AttachedFileName,
                            objAdd.AttachedFileSize),
                    ReferralId = objAdd.ReferralId
                });
                _diaryRepository.Commit();
                _referralRepository.AddReferralAttachment(objAdd);
                _referralRepository.Commit();
                objTransaction.Complete();
            }
            
        }

        public dynamic GetListTaskByReferralId(int id)
        {
            return _referralRepository.GetListTaskByReferralId(id);
        }

        private static bool CheckIsAuthorization(ReferralSource objItem)
        {
            if (!objItem.IsAuthorization)
            {
                return true;
            }
            var dateFrom = objItem.AuthorizationFrom == null
                ? DateTime.MinValue
                : objItem.AuthorizationFrom.GetValueOrDefault();
            var dateTo = objItem.AuthorizationTo == null
                ? DateTime.MaxValue
                : objItem.AuthorizationTo.GetValueOrDefault().AddDays(1).AddSeconds(-1);
            return DateTime.Now >= dateFrom && DateTime.Now <= dateTo;
        }

        //private bool CheckIsDeleteTaskInTaskGroup(List<ReferralTask> objListReferralTask,int taskGroupIdSelected)
        //{
        //    if (taskGroupIdSelected == 0)
        //    {
        //        return false;
        //    }
        //    var objListTaskTemplateId = _taskGroupRepository.GetListTaskTemplate(taskGroupIdSelected).Select(o=>o.Id);
        //    //var objListIdTaskReferral=objListReferralTask.Select(o=>o)
        //}

        public override Referral Add(Referral objAddItem)
        {
            using (var objTransaction = new TransactionScope())
            {
                var objReferralSource = _referralSourceRepository.GetById(objAddItem.ReferralSourceId);
                var objProductType = _productTypeRepository.GetById(objAddItem.ProductTypeId);

                // Create diary for insert
                var objCommentAddReferral = new StringBuilder();

                var referralSourceAndTitle = "";
                if (objReferralSource != null && objProductType != null)
                {
                    referralSourceAndTitle = objReferralSource.FirstName + " " + objReferralSource.MiddleName +
                                             " " + objReferralSource.LastName + "," + objProductType.Name;
                }
                var serviceRequest = "";
                if (objAddItem.PanelTypeId.GetValueOrDefault() != 0)
                {
                    var objPanelType = _panelTypeRepository.GetById(objAddItem.PanelTypeId.GetValueOrDefault());
                    if (objPanelType != null)
                    {
                        serviceRequest = objPanelType.Name;
                    }
                }

                var referralMethod = XmlDataHelpper.Instance.GetValue(XmlDataTypeEnum.ReferralMethod.ToString(),
                    objAddItem.ReferralMethodId.ToString());
                var rush = objAddItem.Rush.GetValueOrDefault() ? SystemMessageLookup.GetMessage("DiaryRush") : "";
                objCommentAddReferral.AppendFormat(SystemMessageLookup.GetMessage("CommentFormatForAddDiaryWhenAddReferral"),
                    DateTime.Now, referralSourceAndTitle, referralMethod, serviceRequest,
                    objAddItem.SpecialInstruction, rush);
                var objDiary = new Diary
                {
                    Heading = SystemMessageLookup.GetMessage("DiaryHeadingAdd"),
                    Reason = SystemMessageLookup.GetMessage("DiaryReasonAddReferral"),
                    Comment = objCommentAddReferral.ToString(),
                };
                objAddItem.Diaries.Add(objDiary);
                // Create diary for referral source

                if (objReferralSource != null)
                {
                    var objDiaryReferralSource = new Diary();
                    var checkIsAuthorization = CheckIsAuthorization(objReferralSource);
                    if (checkIsAuthorization)
                    {
                        objDiaryReferralSource.Heading = SystemMessageLookup.GetMessage("DiaryHeadingValidated");
                        objDiaryReferralSource.Reason = SystemMessageLookup.GetMessage("DiaryReasonValidate");
                        objDiaryReferralSource.Comment = SystemMessageLookup.GetMessage("DiaryReasonComment");
                    }
                    else
                    {
                        objDiaryReferralSource.Heading = SystemMessageLookup.GetMessage("DiaryHeadingExpired");
                        objDiaryReferralSource.Reason = SystemMessageLookup.GetMessage("DiaryReasonExpired");
                        var urlToEditReferralSource = @"/ReferralSource/Update/" + objReferralSource.Id;
                        var referralSourceName = objReferralSource.FirstName + " " + objReferralSource.MiddleName + " " +
                                                 objReferralSource.LastName;
                        var commentMsg = string.Format(SystemMessageLookup.GetMessage("CommentMessage"),
                            urlToEditReferralSource, referralSourceName);
                        objDiaryReferralSource.Comment = commentMsg;
                    }
                    objAddItem.Diaries.Add(objDiaryReferralSource);
                }
                // Create task when next office visit not null
                if (!objAddItem.IsCollectionSite.GetValueOrDefault() && objAddItem.ReferralNpiNumbers.Count != 0)
                {
                    var objNpiNumber = objAddItem.ReferralNpiNumbers.FirstOrDefault();
                    if (objNpiNumber != null && objNpiNumber.NextMdVisitDate != null)
                    {
                        // Create task
                        var objTask = new ReferralTask
                        {
                            Title = SystemMessageLookup.GetMessage("TaskTitleAutoGenerate1"),
                            Description =string.Format(SystemMessageLookup.GetMessage("TaskDescriptionAutoGenerate1"), objAddItem.Id),
                            AssignToId = objAddItem.AssignToId,
                            StartDate = DateTime.Now,
                            DueDate = DateTime.Now,
                            ReferralId = objAddItem.Id,
                            StatusId = 1,
                            IsCollectionServiceRequestLetter = true
                        };
                        objAddItem.ReferralTasks.Add(objTask);
                    }
                }

                //Create Diary For Task
                foreach (var task in objAddItem.ReferralTasks)
                {
                    objAddItem.Diaries.Add(new Diary
                    {
                        Heading = SystemMessageLookup.GetMessage("DiaryHeadingAdd"),
                        Reason = SystemMessageLookup.GetMessage("DiaryReasonAddTask"),
                        Comment = string.Format(SystemMessageLookup.GetMessage("DiaryCommentTask"), task.Title, task.Description),
                    });
                }

                foreach (var attachment in objAddItem.ReferralAttachments)
                {
                    objAddItem.Diaries.Add(new Diary
                    {
                        Heading = SystemMessageLookup.GetMessage("DiaryHeadingAdd"),
                        Reason = SystemMessageLookup.GetMessage("DiaryReasonAddAttachment"),
                        Comment =
                            string.Format(SystemMessageLookup.GetMessage("DiaryCommentAddAttachment"), attachment.AttachedFileName,
                                attachment.AttachedFileSize),
                    });
                }

                objAddItem = base.Add(objAddItem);
                // Create alert for rush
                if (objAddItem.Rush.GetValueOrDefault() && objAddItem.StatusId == 1)
                {
                    var objAlertRush = new Alert
                    {
                        StatusId = objAddItem.StatusId,
                        Type = 1,
                        Title = SystemMessageLookup.GetMessage("DiaryRush"),
                        Message = SystemMessageLookup.GetMessage("DiaryRush"),
                        LinkId = objAddItem.Id,
                        AssignToId = objAddItem.AssignToId,
                        IsRush = true
                    };
                    _alertRepository.Add(objAlertRush);
                    _alertRepository.Commit();
                }

                if (objAddItem.Id > 0)
                {
                    //Write to solr
                    var objSolrItem = _referralRepository.MapReferralToSolrReferral(objAddItem.Id);
                    if (objSolrItem != null)
                    {
                        if (objAddItem.AdjusterIsReferral == true)
                        {
                            objSolrItem.AdjusterName = string.Empty;
                            objSolrItem.AdjusterId = 0;
                        }
                        _referralSolrService.Add(objSolrItem);
                        _referralSolrService.Commit();
                    }
                    // Add list task to solr
                    if (objAddItem.ReferralTasks.Count > 0)
                    {
                        foreach (var referralTask in objAddItem.ReferralTasks)
                        {
                            var objSolrTaskItem = _referralTaskRepository.MapReferralTaskToSolrReferral(referralTask.Id);
                            if (objSolrTaskItem != null)
                            {
                                _referralTaskSolrService.Add(objSolrTaskItem);
                            }
                        }
                        _referralTaskSolrService.Commit();
                    }
                }
                objTransaction.Complete();
                return objAddItem;
            }
        }

        public override Referral Update(Referral model)
        {
            using (var objTransaction = new TransactionScope())
            {
                var hasCreateCollectionLeterTask = false;
                // Create task when next office visit not null
                if (!model.IsCollectionSite.GetValueOrDefault() && model.ReferralNpiNumbers.Count != 0)
                {
                    var objNpiNumber = model.ReferralNpiNumbers.FirstOrDefault();
                    if (objNpiNumber != null && objNpiNumber.NextMdVisitDate != null)
                    {
                        // Check in task referral have exists task 'Collection Service Request Letter - Broadspire‏'
                        var objExistsTask = _referralTaskRepository.CheckExitTaskCollectionServiceRequestLetter(model.Id);
                        if (!objExistsTask)
                        {
                            // Create task
                            var objTask = new ReferralTask
                            {
                                Title = SystemMessageLookup.GetMessage("TaskTitleAutoGenerate1"),
                                Description = string.Format(SystemMessageLookup.GetMessage("TaskDescriptionAutoGenerate1"), model.Id),
                                AssignToId = model.AssignToId,
                                StartDate = DateTime.Now,
                                DueDate = DateTime.Now,
                                ReferralId = model.Id,
                                StatusId = 1,
                                IsCollectionServiceRequestLetter = true
                            };
                            hasCreateCollectionLeterTask = true;
                            model.ReferralTasks.Add(objTask);
                            model.Diaries.Add(new Diary
                            {
                                Heading = SystemMessageLookup.GetMessage("DiaryHeadingAdd"),
                                Reason = SystemMessageLookup.GetMessage("DiaryReasonAddTask"),
                                Comment = string.Format(SystemMessageLookup.GetMessage("DiaryCommentTask"), objTask.Title, objTask.Description),
                            });
                        }
                    }
                }
                //When canceled referral then canceled all task of this referral
                if (model.StatusId == 2)
                {
                    foreach (var item in model.ReferralTasks)
                    {
                        item.StatusId = 2;
                    }
                }
                // Delete all alert of task and referral
                if (model.StatusId == 2)
                {
                    _referralRepository.DeleteAllItemForReferral(model.Id);
                }
                // Update file result to empty => support for export to pdf

                //Write diary Attachment
                var listOldAttachment = model.ReferralAttachments.Where(o => o.IsDeleted).ToList();
                var listNewAttachment = model.ReferralAttachments.Where(o => !o.IsDeleted && o.ReferralId == 0).ToList();
                foreach (var attachment in listNewAttachment)
                {
                    _diaryRepository.Add(new Diary
                    {
                        Heading = SystemMessageLookup.GetMessage("DiaryHeadingAdd"),
                        Reason = SystemMessageLookup.GetMessage("DiaryReasonAddAttachment"),
                        Comment =
                            string.Format(SystemMessageLookup.GetMessage("DiaryCommentAddAttachment"), attachment.AttachedFileName,
                                attachment.AttachedFileSize),
                    });
                    _diaryRepository.Commit();
                }
                foreach (var attachment in listOldAttachment)
                {
                    _diaryRepository.Add(new Diary
                    {
                        Heading = SystemMessageLookup.GetMessage("DiaryHeadingDeleted"),
                        Reason = SystemMessageLookup.GetMessage("DiaryReasonDeleteAttachment"),
                        Comment =
                            string.Format(SystemMessageLookup.GetMessage("DiaryCommentDeleteAttachment"), attachment.AttachedFileName,
                                attachment.AttachedFileSize),
                        ReferralId = model.Id
                    });
                    _diaryRepository.Commit();
                }
                
                model.FileResult = string.Empty;
                base.Update(model);

                //Write to solr
                var objSolrItem = _referralRepository.MapReferralToSolrReferral(model.Id);
                if (objSolrItem != null)
                {
                    if (model.AdjusterIsReferral == true)
                    {
                        objSolrItem.AdjusterName = string.Empty;
                        objSolrItem.AdjusterId = 0;
                    }
                    _referralSolrService.Update(objSolrItem);
                    _referralSolrService.Commit();
                }
                if (hasCreateCollectionLeterTask)
                {
                    // Get task from model
                    var referralTask = model.ReferralTasks.FirstOrDefault(o => o.IsCollectionServiceRequestLetter == true);
                    if (referralTask != null && referralTask.Id != 0)
                    {
                        // Add task to referral task solr
                        var objSolrTaskItem = _referralTaskRepository.MapReferralTaskToSolrReferral(referralTask.Id);
                        if (objSolrTaskItem != null)
                        {
                            _referralTaskSolrService.Add(objSolrTaskItem);
                        }
                        _referralTaskSolrService.Commit();
                    }

                }
                // Update task to cancel
                if (model.StatusId == 2)
                {
                    foreach (var referralTask in model.ReferralTasks)
                    {
                        if (referralTask != null && referralTask.Id != 0)
                        {
                            // Add task to referral task solr
                            var objSolrTaskItem = _referralTaskSolrService.GetById(referralTask.Id);
                            if (objSolrTaskItem != null)
                            {
                                objSolrTaskItem.Status = XmlDataHelpper.Instance.GetValue(XmlDataTypeEnum.Status.ToString(), 2.ToString());
                                objSolrTaskItem.CancelDate = DateTime.Now;
                                objSolrTaskItem.CompletedDate = null;
                                _referralTaskSolrService.Add(objSolrTaskItem);
                            }

                        }
                    }
                    _referralTaskSolrService.Commit();
                }
                if (model.Rush == true)
                {
                    if (model.StatusId != 1)
                    {
                        var objAlert = _alertRepository.Get(o => o.LinkId == model.Id && o.Type == 1).FirstOrDefault();
                        if (objAlert != null)
                        {
                            _alertRepository.Delete(objAlert);
                            _alertRepository.Commit();
                        }
                    }
                    else
                    {
                        _alertRepository.Add(new Alert { AssignToId = model.AssignToId, Type = 1, Title = SystemMessageLookup.GetMessage("DiaryRush"), Message = SystemMessageLookup.GetMessage("DiaryRush"), LinkId = model.Id, IsRush = true, StatusId = model.StatusId });
                        _alertRepository.Commit();
                    }
                    
                }
                objTransaction.Complete();
                return model;
            }
        }
        public override List<LookupItemVo> GetLookup(LookupQuery query, Func<Referral, LookupItemVo> selector)
        {
            var objData = _referralSolrService.GetLookup(query);
            var result = objData ?? base.GetLookup(query, selector);
            return result;
        }

        public int GetPreviousReferralId(int id)
        {
            return _referralRepository.GetPreviousReferralId(id);
        }

        public int GetNextReferralId(int id)
        {
            return _referralRepository.GetNextReferralId(id);
        }

        public void AssignToForReferral(List<int> listIdSelected, bool isSelectAll, int? assignToId, string typeWithUser)
        {
            var validationResult = new List<ValidationResult>();
            var failed = false;
            if (assignToId.GetValueOrDefault() == 0)
            {
                var mess = string.Format(SystemMessageLookup.GetMessage("RequiredTextResourceKey"), "Assign To");
                validationResult.Add(new ValidationResult(mess));
                failed = true;
            }
            if (failed)
            {
                var result = new BusinessRuleResult(true, "", "ReferralAssignTo", 0, null, "ReferralAssignToRule") { ValidationResults = validationResult };
                throw new BusinessRuleException("BussinessGenericErrorMessageKey", new[] { result });
            }
            var enumTypeWithUser = string.IsNullOrEmpty(typeWithUser)
                ? TypeWithUserQueryEnum.All
                : (TypeWithUserQueryEnum)Enum.Parse(typeof(TypeWithUserQueryEnum), typeWithUser);
            var objAssignTo = _userRepository.GetById(assignToId.GetValueOrDefault());
            if (objAssignTo == null)
            {
                throw new Exception(string.Format(SystemMessageLookup.GetMessage("CannotAssignTo")));
            }
            using (var objTransaction = new TransactionScope())
            {
                var listReferralOld = _referralRepository.GetListReferralWithIncludeAssignTo(listIdSelected);
                _referralRepository.AssignToForReferral(listIdSelected, isSelectAll, assignToId, enumTypeWithUser);
                var assignToNew = objAssignTo.FirstName + " " +
                                      (string.IsNullOrEmpty(objAssignTo.MiddleName)
                                          ? ""
                                          : objAssignTo.MiddleName + " ") + objAssignTo.LastName;
                foreach (var referral in listReferralOld)
                {
                    var assignToOld = referral.AssignTo.FirstName + " " +
                                      (string.IsNullOrEmpty(referral.AssignTo.MiddleName)
                                          ? ""
                                          : referral.AssignTo.MiddleName + " ") + referral.AssignTo.LastName;
                    var objDiary = new Diary
                    {
                        Heading = "Update",
                        Reason = "Update referral",
                        Comment =
                            string.Format("Assign To change from [{0}], to [{1}]", assignToOld, assignToNew),
                        ReferralId = referral.Id,
                    };
                    _diaryRepository.Add(objDiary);
                    _diaryRepository.Commit();
                }
                //Uddate to solr
                var listReferral = new List<SolrReferral>();
                if (isSelectAll)
                {
                    var currentUser = _authenticationService.GetCurrentUser();
                    if (currentUser == null)
                    {
                        return;
                        
                    }
                    switch (enumTypeWithUser)
                    {
                        case TypeWithUserQueryEnum.All:
                            listReferral = _referralSolrService.ListAll().ToList();
                            break;
                        case TypeWithUserQueryEnum.Current:
                            listReferral =
                                _referralSolrService.GetByField(new SolrQueryByField("assigntoid", currentUser.User.Id.ToString())).ToList();
                            break;
                        case TypeWithUserQueryEnum.Other:
                            listReferral =
                                _referralSolrService.GetByField(new SolrQueryByField("-assigntoid", currentUser.User.Id.ToString())).ToList();
                            break;
                    }
                    
                }
                else if (listIdSelected.Count > 0)
                {
                    foreach (var id in listIdSelected)
                    {
                        var objAdd = _referralSolrService.GetById(id);
                        if (objAdd != null)
                        {
                            listReferral.Add(objAdd);
                        }
                    }
                }
                if (listReferral.Count > 0)
                {
                    foreach (var solrReferral in listReferral)
                    {
                        var fulName = objAssignTo.FirstName +
                                      (string.IsNullOrEmpty(objAssignTo.MiddleName) ? "" : " " + objAssignTo.MiddleName) +
                                      " " +
                                      objAssignTo.LastName;
                        solrReferral.AssignToId = assignToId;
                        solrReferral.AssignToName = fulName;
                        _referralSolrService.Update(solrReferral);
                        //Update Solr ReferralTask
                        var listReferralTask = _referralTaskSolrService.GetByField(new SolrQueryByField("referralid", solrReferral.Id)).ToList();
                        if (listReferralTask.Count > 0)
                        {
                            foreach (var referralTask in listReferralTask)
                            {
                                if (referralTask.Status.Equals(XmlDataHelpper.Instance.GetValue(XmlDataTypeEnum.Status.ToString(), 1.ToString()))
                                    || referralTask.Status.Equals(XmlDataHelpper.Instance.GetValue(XmlDataTypeEnum.Status.ToString(), 4.ToString()))
                                    || referralTask.Status.Equals(XmlDataHelpper.Instance.GetValue(XmlDataTypeEnum.Status.ToString(), 5.ToString())))
                                {
                                    if (!referralTask.Title.ToLower().Equals("request supply shipment"))
                                    {
                                        referralTask.AssignToId = assignToId;
                                        referralTask.AssignToName = fulName;
                                        _referralTaskSolrService.Update(referralTask);
                                    }
                                }
                                
                            }
                            _referralTaskSolrService.Commit();
                        }
                    }
                    _referralSolrService.Commit();
                }

               

                objTransaction.Complete();
            }

        }

        public dynamic GetDataPrintToReferral(List<int> listIdSelected, IQueryInfo queryInfo)
        {
            var objData = _referralSolrService.GetDataPrint(listIdSelected, queryInfo);
            var result = objData ?? _referralRepository.GetDataPrint(listIdSelected, queryInfo);
            return result;
        }

        public SendMailWithReferralIdVo GetSendMailInfo(int referralId)
        {
            return _referralRepository.GetSendMailInfo(referralId);
        }
    }
}