﻿using Framework.DomainModel.Entities;
using Repositories.Interfaces;
using ServiceLayer.Interfaces;
using Framework.BusinessRule;

namespace ServiceLayer
{
    public class ClaimantLanguageService : MasterFileService<ClaimantLanguage>, IClaimantLanguageService
    {
        private readonly IClaimantLanguageRepository _claimantLanguageRepository;
        public ClaimantLanguageService(IClaimantLanguageRepository claimantLanguageRepository, IBusinessRuleSet<ClaimantLanguage> businessRuleSet = null)
            : base(claimantLanguageRepository, claimantLanguageRepository, businessRuleSet)
        {
            _claimantLanguageRepository = claimantLanguageRepository;
        }
    }
}
