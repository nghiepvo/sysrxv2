﻿using System.Collections.Generic;
using System.Linq;
using System.Web.Mvc;
using Framework.DomainModel.Entities;
using Framework.DomainModel.Entities.Common;
using Framework.DomainModel.Entities.Security;
using Framework.DomainModel.ValueObject;
using ServiceLayer.Interfaces;

namespace ServiceLayer.Common
{
    public class MenuExtractData
    {
        public static MenuExtractData Instance
        {
            get { return Nested._instance; }
        }

        private class Nested
        {
            static Nested()
            {
            }

            internal static readonly MenuExtractData _instance = new MenuExtractData();
        }

        private readonly IDocumentTypeService _documentTypeService;
        private readonly IUserRoleFunctionService _userRoleFunctionService;

        private IList<DocumentType> _listDocumentType=new List<DocumentType>();

        private IList<UserRoleFunction> _listUserRoleFunction=new List<UserRoleFunction>();

        private MenuExtractData()
        {
            _documentTypeService = DependencyResolver.Current.GetService<IDocumentTypeService>();
            _userRoleFunctionService = DependencyResolver.Current.GetService<IUserRoleFunctionService>(); 
        }
        public List<UserRoleFunction> LoadUserSecurityRoleFunction(int userRoleId, int documentTypeId)
        {
            if (_listDocumentType.Count == 0 || _listUserRoleFunction.Count == 0)
            {
                _listUserRoleFunction = _userRoleFunctionService.ListAll();
                _listDocumentType = _documentTypeService.ListAll();
            }
            var result = (from urf in _listUserRoleFunction
                          join document in _listDocumentType
                              on urf.DocumentTypeId equals document.Id into temp
                          from docType in temp
                          where docType.Id == documentTypeId
                          && urf.UserRoleId == userRoleId
                          select urf);
            return result.ToList();
        }

        private bool CheckUserRoleForDocumentType(int idRole, DocumentTypeKey documentType,OperationAction action)
        {
            if (_listDocumentType.Count == 0 || _listUserRoleFunction.Count == 0)
            {
                _listUserRoleFunction = _userRoleFunctionService.ListAll();
                _listDocumentType = _documentTypeService.ListAll();
            }
            return  (from urf in _listUserRoleFunction
             join document in _listDocumentType
                 on urf.DocumentTypeId equals document.Id into temp
             from docType in temp
             where docType.Id == (int)documentType && urf.UserRoleId == idRole &&urf.SecurityOperationId==(int)action
             select urf).Any();
        }

        public void RefershListData()
        {
            _listDocumentType=new List<DocumentType>();
            _listUserRoleFunction=new List<UserRoleFunction>();
        }
        public MenuViewModel GetMenuViewModel(int idRole)
        {
            var objResult = new MenuViewModel
            {
                CanCreateReferral = CheckUserRoleForDocumentType(idRole,DocumentTypeKey.Referral,OperationAction.Add),
                CanViewAdjuster = CheckUserRoleForDocumentType(idRole, DocumentTypeKey.Adjuster, OperationAction.View),
                CanViewAssayCode = CheckUserRoleForDocumentType(idRole, DocumentTypeKey.AssayCode, OperationAction.View),
                CanViewAssayCodeDescription = CheckUserRoleForDocumentType(idRole, DocumentTypeKey.AssayCodeDescription, OperationAction.View),
                CanViewAttorney = CheckUserRoleForDocumentType(idRole, DocumentTypeKey.Attorney, OperationAction.View),
                CanViewBranch = CheckUserRoleForDocumentType(idRole, DocumentTypeKey.Branch, OperationAction.View),
                CanViewCaseManager = CheckUserRoleForDocumentType(idRole, DocumentTypeKey.CaseManager, OperationAction.View),
                CanViewCity = CheckUserRoleForDocumentType(idRole, DocumentTypeKey.City, OperationAction.View),
                CanViewClaimNumber = CheckUserRoleForDocumentType(idRole, DocumentTypeKey.ClaimNumber, OperationAction.View),
                CanViewClaimant = CheckUserRoleForDocumentType(idRole, DocumentTypeKey.Claimant, OperationAction.View),
                CanViewClaimantLanguage = CheckUserRoleForDocumentType(idRole, DocumentTypeKey.ClaimantLanguage, OperationAction.View),
                CanViewCollectionSite = CheckUserRoleForDocumentType(idRole, DocumentTypeKey.CollectionSite, OperationAction.View),
                CanViewConfiguration = CheckUserRoleForDocumentType(idRole, DocumentTypeKey.Configuration, OperationAction.View),
                CanViewDrug = CheckUserRoleForDocumentType(idRole, DocumentTypeKey.Drug, OperationAction.View),
                CanViewEmailTemplate = CheckUserRoleForDocumentType(idRole, DocumentTypeKey.EmailTemplate, OperationAction.View),
                CanViewEmployer = CheckUserRoleForDocumentType(idRole, DocumentTypeKey.Employer, OperationAction.View),
                CanViewIcd = CheckUserRoleForDocumentType(idRole, DocumentTypeKey.Icd, OperationAction.View),
                CanViewNpiNumber = CheckUserRoleForDocumentType(idRole, DocumentTypeKey.NpiNumber, OperationAction.View),
                CanViewPanelType = CheckUserRoleForDocumentType(idRole, DocumentTypeKey.PanelType, OperationAction.View),
                CanViewPayer = CheckUserRoleForDocumentType(idRole, DocumentTypeKey.Payer, OperationAction.View),
                CanViewProductType = CheckUserRoleForDocumentType(idRole, DocumentTypeKey.ProductType, OperationAction.View),
                CanViewReasonReferral = CheckUserRoleForDocumentType(idRole, DocumentTypeKey.ReasonReferral, OperationAction.View),
                CanViewReferral = CheckUserRoleForDocumentType(idRole, DocumentTypeKey.Referral, OperationAction.View),
                CanViewReferralReport = CheckUserRoleForDocumentType(idRole, DocumentTypeKey.ReferralReport, OperationAction.View),
                CanViewReferralSource = CheckUserRoleForDocumentType(idRole, DocumentTypeKey.ReferralSource, OperationAction.View),
                CanViewReferralType = CheckUserRoleForDocumentType(idRole, DocumentTypeKey.ReferralType, OperationAction.View),
                CanViewState = CheckUserRoleForDocumentType(idRole, DocumentTypeKey.State, OperationAction.View),
                CanViewSysRxReport = CheckUserRoleForDocumentType(idRole, DocumentTypeKey.SysRxReport, OperationAction.View),
                CanViewTask = CheckUserRoleForDocumentType(idRole, DocumentTypeKey.ReferralTask, OperationAction.View),
                CanViewTaskGroup = CheckUserRoleForDocumentType(idRole, DocumentTypeKey.TaskGroup, OperationAction.View),
                CanViewTaskTemplate = CheckUserRoleForDocumentType(idRole, DocumentTypeKey.TaskTemplate, OperationAction.View),
                CanViewUser = CheckUserRoleForDocumentType(idRole, DocumentTypeKey.User, OperationAction.View),
                CanViewUserRole = CheckUserRoleForDocumentType(idRole, DocumentTypeKey.UserRole, OperationAction.View),
                CanViewZip = CheckUserRoleForDocumentType(idRole, DocumentTypeKey.Zip, OperationAction.View),
                CanViewRequisitionPrint = CheckUserRoleForDocumentType(idRole, DocumentTypeKey.RequisitionPrint, OperationAction.View),
            };
            return objResult;

        }
    }
}
