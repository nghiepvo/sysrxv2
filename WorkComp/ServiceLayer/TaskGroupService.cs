﻿using System.Collections.Generic;
using Framework.BusinessRule;
using Framework.DomainModel.Entities;
using Repositories.Interfaces;
using ServiceLayer.Interfaces;

namespace ServiceLayer
{
    public class TaskGroupService : MasterFileService<TaskGroup>, ITaskGroupService
    {
        private readonly ITaskGroupRepository _taskGroupRepository;
        public TaskGroupService(ITaskGroupRepository taskGroupRepository, IBusinessRuleSet<TaskGroup> businessRuleSet = null)
            : base(taskGroupRepository, taskGroupRepository, businessRuleSet)
        {
            _taskGroupRepository = taskGroupRepository;
        }

        public List<TaskTemplate> GetListTaskTemplate(int taskGroupId, int? referralId)
        {
            return _taskGroupRepository.GetListTaskTemplate(taskGroupId, referralId);
        }
    }
}