﻿using System;
using System.Linq.Expressions;
using Framework.DomainModel;

namespace Framework.Repositories
{
    public interface IRepository
    {
        Entity GetById(int id);
        void Add(Entity entity);
        void Remove(Entity entity);
        void Attach(Entity entity);
    }
    public interface IRepository<TEntity> : IRepository where TEntity : Entity
    {
        new TEntity GetById(int id);
        TEntity GetByIdWithIncludeAll(int id);
        TEntity GetByIdWithIncludeAllWithCollection(int id);
        void Add(TEntity entity);
        void Remove(TEntity entity);
        void Attach(TEntity entity);
        void Update(TEntity entity);
        void Delete(TEntity entity);
        void Commit();
    }
}
