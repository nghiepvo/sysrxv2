﻿using System.Web;
using System.Net.Mime;

namespace Framework.Utility
{
    public class DownloadFileProvider
    {
        public static void DownloadFile(HttpContextBase context, string fileName, byte[] fileContent)
        {
            context.Response.ClearHeaders();
            context.Response.ClearContent();
            context.Response.Clear();

            // Set mime type.
            context.Response.ContentType = "application/octet-stream";

            var cd = new ContentDisposition
            {
                // for example foo.bak
                FileName = fileName,

                // always prompt the user for downloading, set to true if you want 
                // the browser to try to show the file inline
                Inline = true,
            };


            // Set the Content-Disposition.
            context.Response.AddHeader("Content-Disposition", cd.ToString());

            // Transfer the file.
            context.Response.OutputStream.Write(fileContent, 0, fileContent.Length);
            context.Response.Flush();
        }
    }
}
