﻿using System;
using System.Globalization;

namespace Framework.Utility
{
    public static class CaculatorHelper
    {
        public static int CaculateDuration(DateTime startDate, DateTime dueDate, DateTime? cancelDate, DateTime? completedDate)
        {
            if (cancelDate != null)
            {
                if (cancelDate < dueDate)
                {
                    return (int)((DateTime)cancelDate - startDate).TotalHours;
                }
                return (int)((DateTime)cancelDate - dueDate).TotalHours;
            }
            if (completedDate != null)
            {
                if (completedDate < dueDate)
                {
                    return (int)((DateTime)completedDate - startDate).TotalHours;
                }
                return (int)((DateTime)completedDate - dueDate).TotalHours;
            }

            if (DateTime.Now < dueDate)
            {
                return (int)(DateTime.Now - startDate).TotalHours;
            }
            return (int)(DateTime.Now - dueDate).TotalHours;
        }

        public static double CaculateDurationPercentLeft(DateTime dueDateDateTime, DateTime startDateDateTime, double durationPercentRigth, DateTime? cancelDate, DateTime? completedDate)
        {
            if (cancelDate!= null)
            {
                if (cancelDate < dueDateDateTime)
                {
                    var percent = (((DateTime)cancelDate - startDateDateTime).TotalHours /
                                        (dueDateDateTime - startDateDateTime).TotalHours) * 100;
                    return percent < 21.00 ? 21.00 : percent;
                }
                return (100 - (durationPercentRigth)) > 82.00 ? 82.00 : 100 - (durationPercentRigth);
            }
            if (completedDate != null)
            {
                if (completedDate < dueDateDateTime)
                {
                    var percent = (((DateTime)completedDate - startDateDateTime).TotalHours /
                                        (dueDateDateTime - startDateDateTime).TotalHours) * 100;
                    return percent < 21.00 ? 21.00 : percent;
                }
                return (100 - (durationPercentRigth)) > 82.00 ? 82.00 : 100 - (durationPercentRigth);
            }

            if (DateTime.Now < dueDateDateTime)
            {
                var percent = ((DateTime.Now - startDateDateTime).TotalHours /
                                    (dueDateDateTime - startDateDateTime).TotalHours) * 100;
                return percent < 21.00 ? 21.00 : percent;
            }
            return (100 - (durationPercentRigth)) > 82.00 ? 82.00 : 100 - (durationPercentRigth);
        }

        public static double CaculateDurationPercentRight(DateTime dueDateDateTime, DateTime startDateDateTime, DateTime? cancelDate, DateTime? completedDate)
        {
            double percent;

            if (cancelDate != null)
            {
                percent = (((DateTime)cancelDate - dueDateDateTime).TotalHours / ((DateTime)cancelDate - startDateDateTime).TotalHours) * 100;
                return percent < 17.00 ? 17.00 : percent;
            }

            if (completedDate != null)
            {
                percent = (((DateTime)completedDate - dueDateDateTime).TotalHours / ((DateTime)completedDate - startDateDateTime).TotalHours) * 100;
                return percent < 17.00 ? 17.00 : percent;
            }
            
            percent = ((DateTime.Now - dueDateDateTime).TotalHours / (DateTime.Now - startDateDateTime).TotalHours) * 100;
            return percent < 17.00 ? 17.00 : percent;
            
        }

        public static bool CaculateCategoryDuration(DateTime dueDateDateTime, DateTime? cancelDate, DateTime? completedDate)
        {
            if (cancelDate != null)
            {
                return cancelDate < dueDateDateTime;
            }

            if (completedDate != null)
            {
                return completedDate < dueDateDateTime;
            }
            return DateTime.Now < dueDateDateTime;
        }

        public static string CaculateFormatDuration(DateTime startDate, DateTime dueDate, DateTime? cancelDate, DateTime? completedDate)
        {
            var duration = 0;
            if (cancelDate != null)
            {
                duration = (int) ((DateTime) cancelDate - startDate).TotalHours;
            }
            else if (completedDate != null)
            {
                duration = (int)((DateTime)completedDate - startDate).TotalHours;
            }
            else
            {
                duration = (int)(DateTime.Now - startDate).TotalHours;
            }
            if (duration < 0)
            {
                return "0d0h";
            }
            return (duration / 24).ToString(CultureInfo.InvariantCulture) + "d" + (duration % 24).ToString(CultureInfo.InvariantCulture) + "h";
        }

        public static string CaculateFormatFullAddress(string address1, string state, string city, string zip,
            string address2 = null)
        {
            var result = "";
            //Address1
            result += string.IsNullOrEmpty(address1) ? "" : address1 + ", ";
            //Address2
            result += string.IsNullOrEmpty(address2) ? "" :  address2 + ", ";
            //City
            result += string.IsNullOrEmpty(city) ? "" : city + ", ";
            //State
            result += string.IsNullOrEmpty(state) ? "" : state + " ";
            //Zip
            result += string.IsNullOrEmpty(zip) ? "" : zip;
            return result;
        }
    }
}
