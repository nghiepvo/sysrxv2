﻿using System.Text.RegularExpressions;

namespace Solr.Utility
{
    public static class SpecialCharacterHelper
    {
        private const string Parent = "\\+|-|&&|\\|\\||!|\\(|\\)|{|}|\\[|\\]|\\^|\"|~|\\*|\\?|\\:|\\\\";
        public static bool MatchSpecialCharacterInSolr(this string value)
        {
            return Regex.IsMatch(value, Parent);
        }
        public static string ReplaceSpecialCharacterInSolr(this string value)
        {
            //"1+2-3&&4||5!6(7)8{9}1[2]3^4\"5~6*7?8:9\\1"
            //\\+|-|&&|\\|\\||!|\\(|\\)|{|}|\\[|\\]|\\^|\"|~|\\*|\\?|\\:|\\\\
            //result: "1 2 3 4 5 6 7 8 9 1 2 3 4 5 6 7 8 9 1"
            return Regex.Replace(value, Parent, " ");
        }
    }
}
