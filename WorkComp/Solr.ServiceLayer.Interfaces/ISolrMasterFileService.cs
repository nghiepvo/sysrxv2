﻿using System;
using System.Collections.Generic;
using System.Linq.Expressions;
using Framework.DomainModel.Entities.Common;
using Framework.DomainModel.Interfaces;
using Framework.DomainModel.ValueObject;
using Solr.DomainModel;
using SolrNet;

namespace Solr.ServiceLayer.Interfaces
{
    public interface ISolrMasterFileService<TEntity> where TEntity : SolrEntity
    {
        TEntity GetById(int id);
        TEntity Add(TEntity model);
        TEntity Update(TEntity model);
        void Delete(TEntity model);
        void Commit();
        /// <summary>
        ///     Delete an entity by its id.
        /// </summary>
        /// <param name="id">Id to delete</param>
        /// <returns>Affected record count.</returns>
        void DeleteById(int id);

        /// <summary>
        ///     List all entities in database
        /// </summary>
        /// <returns>List of entities</returns>
        IList<TEntity> ListAll();

        /// <summary>
        ///     The count number of entity.
        /// </summary>
        /// <param name="where">searching condition</param>
        /// <returns>number of record </returns>
        int Count(Expression<Func<TEntity, bool>> @where = null);

        /// <summary>
        ///     The check entity exist.
        /// </summary>
        /// <param name="where">Condition to check</param>
        /// <returns>true if exist, false if not</returns>
        bool CheckExist(Expression<Func<TEntity, bool>> @where = null);


        /// <summary>
        ///     Get entity by SolrQueryByField
        /// </summary>
        /// <param name="predicate">condition to get</param>
        /// <returns>List of  entities that qualify condition</returns>
        IList<TEntity> GetByField(SolrQueryByField predicate);

        /// <summary>
        ///     Insert or Update entity
        /// </summary>
        /// <param name="entity">Entity</param>
        void InsertOrUpdate(TEntity entity);

        /// <summary>
        ///     Delete a list of entities
        /// </summary>
        /// <param name="ids">List of entities</param>
        /// <returns>Affected record count.</returns>
        List<int> DeleteById(IEnumerable<int> ids);

        /// <summary>
        ///     Delete a list of entity
        /// </summary>
        /// <param name="entities">List of entities to delete</param>
        void DeleteAll(IEnumerable<TEntity> entities);

        /// <summary>
        ///     Delete entity base on condition
        /// </summary>
        /// <param name="predicate">Condition to delete</param>
        void DeleteAllByField(SolrQueryByField predicate);

        dynamic GetDataForGridMasterfile(IQueryInfo queryInfo);

        List<LookupItemVo> GetLookup(LookupQuery query);
    }
}