﻿using Solr.DomainModel;

namespace Solr.ServiceLayer.Interfaces
{
    public interface ISolrReferralResultService : ISolrMasterFileService<SolrReferralResult>
    {
    }
}