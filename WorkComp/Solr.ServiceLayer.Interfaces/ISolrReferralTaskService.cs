﻿using System.Collections.Generic;
using Framework.DomainModel.Entities.Common;
using Framework.DomainModel.Interfaces;
using Solr.DomainModel;

namespace Solr.ServiceLayer.Interfaces
{
    public interface ISolrReferralTaskService : ISolrMasterFileService<SolrReferralTask>
    {
        dynamic GetDataPrint(List<int> listIdSelected, IQueryInfo queryInfo);
    }
}