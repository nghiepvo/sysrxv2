﻿using System.Collections.Generic;
using Framework.DomainModel.Entities.Common;
using Framework.DomainModel.ValueObject;
using Solr.DomainModel;

namespace Solr.ServiceLayer.Interfaces
{
    public interface ISolrNpiNumberService : ISolrMasterFileService<SolrNpiNumber>
    {
        List<LookupItemVo> GetLookupTreatingPhysician(LookupQuery query);
    }
}