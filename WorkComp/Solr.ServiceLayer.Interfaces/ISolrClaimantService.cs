﻿using Solr.DomainModel;

namespace Solr.ServiceLayer.Interfaces
{
    public interface ISolrClaimantService : ISolrMasterFileService<SolrClaimant>
    {
    }
}