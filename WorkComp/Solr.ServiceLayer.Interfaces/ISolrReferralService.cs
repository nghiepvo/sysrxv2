﻿using System.Collections.Generic;
using Framework.DomainModel.Interfaces;
using Solr.DomainModel;

namespace Solr.ServiceLayer.Interfaces
{
    public interface ISolrReferralService : ISolrMasterFileService<SolrReferral>
    {
        dynamic GetDataPrint(List<int> selectedIds, IQueryInfo queryInfo);

    }
}