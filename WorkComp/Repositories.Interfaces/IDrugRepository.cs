﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Framework.DomainModel.Entities;
using Framework.Repositories;

namespace Repositories.Interfaces
{
    public interface IDrugRepository : IRepository<Drug>, IQueryableRepository<Drug>
    {
        string GetDrugs(int idReferral);
    }
}
