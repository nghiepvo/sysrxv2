﻿using Framework.DomainModel.Entities;
using Framework.Repositories;
namespace Repositories.Interfaces
{
    public interface IClaimantLanguageRepository : IRepository<ClaimantLanguage>, IQueryableRepository<ClaimantLanguage>
    {
    }
}
