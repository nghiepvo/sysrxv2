﻿using System.Collections;
using System.Collections.Generic;
using Framework.DomainModel.Entities;
using Framework.Repositories;

namespace Repositories.Interfaces
{
    public interface ICommentRepository : IRepository<Comment>, IQueryableRepository<Comment>
    {
        IList<Comment> GetCommentWhenChangeReferral(int? referralTaskId);
    }
}