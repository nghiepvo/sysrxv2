﻿using Framework.DomainModel.Entities;
using Framework.DomainModel.Entities.Common;
using Framework.DomainModel.ValueObject;
using Framework.Repositories;

namespace Repositories.Interfaces
{
    public interface IClaimNumberRepository : IRepository<ClaimNumber>, IQueryableRepository<ClaimNumber>
    {
        InfoWhenChangeClaimNumberInReferral GetInfoWhenChangeClaimNumberInReferral(int idClaimNumber);
    }
}
