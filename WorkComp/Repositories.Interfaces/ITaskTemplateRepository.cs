﻿using System.Collections.Generic;
using Framework.DomainModel.Entities;
using Framework.DomainModel.ValueObject;
using Framework.Repositories;

namespace Repositories.Interfaces
{
    public interface ITaskTemplateRepository : IRepository<TaskTemplate>, IQueryableRepository<TaskTemplate>
    {
        /// <summary>
        ///     Get all item for dual select
        /// </summary>
        /// <returns></returns>
        List<DualListBoxItemVo> GetAllTaskTemplateForDualListBox();
        /// <summary>
        /// Get selected task template by task group
        /// </summary>
        /// <param name="taskGroupId"></param>
        /// <returns></returns>
        List<DualListBoxItemVo> GetTaskTemplateByTaskGroup(int taskGroupId);
    }
}