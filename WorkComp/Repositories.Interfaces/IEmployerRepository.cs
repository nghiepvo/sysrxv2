﻿using Framework.DomainModel.Entities;
using Framework.DomainModel.Interfaces;
using Framework.DomainModel.ValueObject;
using Framework.Repositories;

namespace Repositories.Interfaces
{
    public interface IEmployerRepository : IRepository<Employer>, IQueryableRepository<Employer>
    {
        
        InfoWhenChangeEmployerInReferral GetInfoWhenChangeEmployerInReferral(int idEmployer);
    }
}
