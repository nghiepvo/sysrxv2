﻿using Framework.DomainModel.Entities;
using Framework.Repositories;

namespace Repositories.Interfaces
{
    public interface IReferralEmailTemplateRepository : IRepository<ReferralEmailTemplate>, IQueryableRepository<ReferralEmailTemplate>
    {
        ReferralEmailTemplate GetIncludeAttachment(int referralId, int emailTemplateId);
    }
}
