﻿using Framework.DomainModel.Entities;
using Framework.DomainModel.ValueObject;
using Framework.Repositories;

namespace Repositories.Interfaces
{
    public interface ICaseManagerRepository : IRepository<CaseManager>, IQueryableRepository<CaseManager>
    {
        InfoWhenChangeCaseManagerInReferral GetInfoWhenChangeCaseManagerInReferral(int idCaseManager);
    }
}
