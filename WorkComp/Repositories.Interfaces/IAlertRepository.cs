﻿using System;
using Framework.DomainModel.Entities;
using Framework.Repositories;

namespace Repositories.Interfaces
{
    public interface IAlertRepository : IRepository<Alert>, IQueryableRepository<Alert>
    {
        dynamic GetAlertPartial(int idCurrentUser, bool isAdminRole, int startIndex, int countItem);
        dynamic GetDataAlertForGrid(int idCurrentUser, bool isAdminRole, string searchId, DateTime? startDate, DateTime? endDate, int startIndex, int countItem);
        
    }
}
