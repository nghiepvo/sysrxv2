﻿using System.Collections.Generic;
using Framework.DomainModel.Entities;
using Framework.DomainModel.Entities.Common;
using Framework.DomainModel.Interfaces;
using Framework.Repositories;
using Solr.DomainModel;

namespace Repositories.Interfaces
{
    public interface IReferralTaskRepository : IRepository<ReferralTask>, IQueryableRepository<ReferralTask>
    {
        dynamic GetDataParentForGrid(IQueryInfo queryInfo);
        SolrReferralTask MapReferralTaskToSolrReferral(int id);
        bool CheckExitTaskCollectionServiceRequestLetter(int referralId);

        int GetPreviousReferralTaskId(int id, int referralId);
        int GetNextReferralTaskId(int id, int referralId);
        void CompletedTask(int id);
        void AssignToForReferralTask(List<int> listIdSelected, bool isSelectAll, int? assignToId, TypeWithUserQueryEnum enumTypeWithUser);
        dynamic GetDataPrint(List<int> listIdSelected, ReferralQueryInfo queryInfo);
        bool UpdateComplateTask(ReferralTask referralTask);
    }
}