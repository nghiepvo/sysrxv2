﻿using Framework.DomainModel.Entities;
using Framework.Repositories;

namespace Repositories.Interfaces
{
    public interface IProductTypeRepository : IRepository<ProductType>, IQueryableRepository<ProductType>
    {
    }
}