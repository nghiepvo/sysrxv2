﻿using System.Collections.Generic;
using Framework.DomainModel.Entities;
using Framework.DomainModel.Entities.Common;
using Framework.DomainModel.Interfaces;
using Framework.DomainModel.ValueObject;
using Framework.Repositories;
using Solr.DomainModel;

namespace Repositories.Interfaces
{
    public interface IReferralRepository : IRepository<Referral>, IQueryableRepository<Referral>
    {
        List<ReferralMedicationHistory> GetMedicationHistoriesByReferral(int referralId);
        dynamic GetDataParentForGrid(IQueryInfo queryInfo);
        dynamic GetDataChildrenForGrid(IQueryInfo queryInfo);
        ReferralDetailVo GetReferralDeltailById(int id);
        ReferralResultDetailVo GetReferralResultInfo(int referralId);
        List<ReasonReferralInReferralDetailVo> GetReasonReferralInfos(int id);
        dynamic GetListTaskByReferralId(int id);
        SolrReferral MapReferralToSolrReferral(int idReferral);
        void RemoveAttachmentFile(int referralId, string rowGuid, string fileName);
        void AddReferralAttachment(ReferralAttachment objAdd);

        int GetPreviousReferralId(int id);
        int GetNextReferralId(int id);
        void DeleteAllItemForReferral(int referralId);
        void AssignToForReferral(List<int> listIdSelected, bool isSelectAll, int? assignToId,TypeWithUserQueryEnum typeWithUser);
        dynamic GetDataPrint(List<int> selectedIds, IQueryInfo queryInfo);
        SendMailWithReferralIdVo GetSendMailInfo(int referralId);
        List<Referral> GetListReferralWithIncludeAssignTo(List<int> listIdSelected);
    }
}