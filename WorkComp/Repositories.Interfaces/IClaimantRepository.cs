﻿using Framework.DomainModel.Entities;
using Framework.DomainModel.ValueObject;
using Framework.Repositories;

namespace Repositories.Interfaces
{
    public interface IClaimantRepository : IRepository<Claimant>, IQueryableRepository<Claimant>
    {
        InfoWhenChangeClaimantInReferral GetInfoWhenChangeClaimantInReferral(int idClaimant);
    }
}
