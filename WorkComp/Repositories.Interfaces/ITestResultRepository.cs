﻿using Framework.DomainModel.Entities;
using Framework.DomainModel.ValueObject;
using Framework.Repositories;

namespace Repositories.Interfaces
{
    public interface ITestResultRepository : IRepository<TestResult>, IQueryableRepository<TestResult>
    {
    }
}
