﻿using Framework.DomainModel.Entities;
using Framework.DomainModel.ValueObject;
using Framework.Repositories;

namespace Repositories.Interfaces
{
    public interface IAdjusterRepository : IRepository<Adjuster>, IQueryableRepository<Adjuster>
    {
        InfoWhenChangeAdjusterInReferral GetInfoWhenChangeAdjusterInReferral(int idAdjuster);
    }
}
