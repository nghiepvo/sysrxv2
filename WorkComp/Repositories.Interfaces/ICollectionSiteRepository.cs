﻿using Framework.DomainModel.Entities;
using Framework.DomainModel.ValueObject;
using Framework.Repositories;

namespace Repositories.Interfaces
{
    public interface ICollectionSiteRepository : IRepository<CollectionSite>, IQueryableRepository<CollectionSite>
    {
        InfoWhenChangeCollectionSiteInReferral GetInfoWhenChangeCollectionSiteInReferral(int idCollectionSite);
    }
}
