﻿using Framework.DomainModel.Entities;
using Framework.Repositories;

namespace Repositories.Interfaces
{
    public interface IStateRepository : IRepository<State>, IQueryableRepository<State>
    {
    }
}