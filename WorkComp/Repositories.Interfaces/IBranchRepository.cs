﻿using Framework.DomainModel.Entities;
using Framework.Repositories;

namespace Repositories.Interfaces
{
    public interface IBranchRepository : IRepository<Branch>, IQueryableRepository<Branch>
    {
    }
}
