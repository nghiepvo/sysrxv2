﻿using System.Collections.Generic;
using Framework.DomainModel.Entities;
using Framework.Repositories;

namespace Repositories.Interfaces
{
    public interface ITaskGroupRepository : IRepository<TaskGroup>, IQueryableRepository<TaskGroup>
    {
        List<TaskTemplate> GetListTaskTemplate(int taskGroupId, int? referralId);
    }
}