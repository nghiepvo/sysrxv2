﻿using System.Collections.Generic;
using Framework.DomainModel.Entities;
using Framework.DomainModel.ValueObject;
using Framework.Repositories;

namespace Repositories.Interfaces
{
    public interface IReasonReferralRepository : IRepository<ReasonReferral>, IQueryableRepository<ReasonReferral>
    {
        IList<ReasonReferralParrentVo> GetReasonReferral(int? parrentId);
        IList<ReasonReferralWithReferralVo> GetSelectedReasonReferralbyReferral(int referralId);
        IList<ReasonReferralWithReferralVo> GetUnselectedReasonReferralByReferral(int referralId);
        List<LookupItemVo> GetListReasonReferral();
        bool UpdateActiveReasonReferral(int id);
    }
}