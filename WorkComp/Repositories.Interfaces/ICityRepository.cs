﻿using Framework.DomainModel.Entities;
using Framework.Repositories;

namespace Repositories.Interfaces
{
    public interface ICityRepository : IRepository<City>, IQueryableRepository<City>
    {
    }
}