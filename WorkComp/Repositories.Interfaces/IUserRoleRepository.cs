﻿using System.Collections.Generic;
using Framework.DomainModel.Entities;
using Framework.Repositories;

namespace Repositories.Interfaces
{
    public interface IUserRoleRepository : IRepository<UserRole>, IQueryableRepository<UserRole>
    {
        dynamic GetRoleFunction(int idRole);
        List<DocumentType> GetAllDocumentType();
    }
}