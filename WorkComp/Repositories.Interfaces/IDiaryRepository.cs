﻿using System.Collections;
using System.Collections.Generic;
using Framework.DomainModel.Entities;
using Framework.Repositories;

namespace Repositories.Interfaces
{
    public interface IDiaryRepository : IRepository<Diary>, IQueryableRepository<Diary>
    {
        IList<Diary> GetDiaryWhenChangeReferral(int? referralId);
    }
}