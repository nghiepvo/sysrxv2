﻿using System;
using System.Collections.Generic;
using Framework.DomainModel.Entities;
using Framework.DomainModel.Entities.Common;
using Framework.DomainModel.ValueObject;
using Framework.Repositories;

namespace Repositories.Interfaces
{
    public interface INpiNumberRepository : IRepository<NpiNumber>, IQueryableRepository<NpiNumber>
    {
        List<LookupItemVo> GetLookupTreatingPhysician(LookupQuery query, Func<NpiNumber, LookupItemVo> selector);
        InfoWhenChangeTreatingPhysicianInReferral GetInfoWhenChangeTreatingPhysicianInReferral(int idTreatingPhysician);
    }
}
