﻿
using Framework.DomainModel.Entities;
using Framework.DomainModel.ValueObject;
using Framework.Repositories;

namespace Repositories.Interfaces
{
    public interface IEmailTemplateRepository:IRepository<EmailTemplate>,IQueryableRepository<EmailTemplate>
    {
        ReferralEmailTemplate GetOrderEmailTemplate(int referralId, int emailId);
        DataRequestForAuthorizationEmailTemplate GetDataRequestForAuthorization(int referralId);
        AllEmailForReferral GetListEmailInReferral(int referralId);
        DataRequestForCollectionEmailTemplate GetDataRequestForCollection(int referralId);
        CollectionServiceRequestEmail GetDataRequestForCollectionServiceRequest(int referralId);
        dynamic GetDataForGridReferralDetail();
        string ContentHtmlRequestForAuthorization(int referralId, EmailTemplateType emailTemplateType);
        DataMdNotificationLetterTemplate GetDataMdNotificationLetterTemplate(int referralId);
        DataCustomCommunicationTemplate GetDataCustomCommunicationTemplate(int referralId);
    }
}
