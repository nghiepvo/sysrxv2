﻿using System.Linq;
using Framework.DomainModel.Entities;
using Framework.DomainModel.Interfaces;
using Framework.DomainModel.ValueObject;
using System.Linq.Dynamic;
using Repositories.Interfaces;

namespace Repositories
{
    public class EntityFrameworkCollectionSiteRepository : EntityFrameworkRepositoryBase<CollectionSite>, ICollectionSiteRepository
    {
        public EntityFrameworkCollectionSiteRepository()
        {
            SearchColumns.Add("Name");
            SearchColumns.Add("Phone");
            SearchColumns.Add("Address");
            SearchColumns.Add("State");
            SearchColumns.Add("City");
            SearchColumns.Add("Zip");
            DisplayColumnForCombobox = "Name";
        }

        public override IQueryable<ReadOnlyGridVo> BuildQueryToGetDataForGrid(IQueryInfo queryInfo)
        {
            var queryResult =(from entity in GetAll()
                              join s in WorkCompDb.Set<State>() on entity.StateId equals s.Id into states
                              from state in states.DefaultIfEmpty()
                              join c in WorkCompDb.Set<City>() on entity.CityId equals c.Id into cities
                              from city in cities.DefaultIfEmpty()
                              join z in WorkCompDb.Set<Zip>() on entity.ZipId equals z.Id into zips
                              from zip in zips.DefaultIfEmpty()
                              select new { entity, state, city, zip }).OrderBy(queryInfo.SortString)
                .Select(o => new CollectionSiteGridVo
            {
                Id = o.entity.Id,
                Name = o.entity.Name,
                Phone = o.entity.Phone,
                Address = o.entity.Address,
                State = o.state == null ?string.Empty : o.state.Name,
                City = o.city == null ? string.Empty : o.city.Name,
                Zip = o.zip == null ? string.Empty : o.zip.Name,

            });
            return queryResult;
        }

        protected override void BuildSortExpression(IQueryInfo queryInfo)
        {
            base.BuildSortExpression(queryInfo);
            queryInfo.Sort.ForEach(x =>
            {
                if (x.Field.Contains("entity.State"))
                {
                    x.Field = "(state != null) ? state.Name : string.Empty";
                }

                if (x.Field.Contains("entity.City"))
                {
                    x.Field = "(city != null) ? city.Name : string.Empty";
                }
                if (x.Field.Contains("entity.Zip"))
                {
                    x.Field = "(zip != null) ? zip.Name : string.Empty";

                }
            });

            if (queryInfo.Sort == null || queryInfo.Sort.Count <= 0) return;
            foreach (var sort in queryInfo.Sort.Where(sort => sort.Field == "entity.Claimant"))
            {
                sort.Field = "claimant.FirstName";
            }

            foreach (var sort in queryInfo.Sort.Where(sort => sort.Field == "entity.Adjuster"))
            {
                sort.Field = "adjuster.FirstName";
            }

           
        }

        public InfoWhenChangeCollectionSiteInReferral GetInfoWhenChangeCollectionSiteInReferral(int idCollectionSite)
        {
            var query = from entity in GetAll()
                        where entity.Id == idCollectionSite
                        select new InfoWhenChangeCollectionSiteInReferral
                        {
                            CollectionSiteId = idCollectionSite,
                            Fax = entity.Fax,
                            Phone = entity.Phone,
                            CollectionSiteAddress = entity.Address
                        };
            return query.FirstOrDefault();
        }
    }
}