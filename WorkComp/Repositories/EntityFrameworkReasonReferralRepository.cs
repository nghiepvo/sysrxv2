﻿using System.Collections.Generic;
using System.Linq;
using System.Web;
using Framework.DomainModel.Entities;
using Framework.DomainModel.ValueObject;
using Repositories.Interfaces;

namespace Repositories
{
    public class EntityFrameworkReasonReferralRepository : EntityFrameworkRepositoryBase<ReasonReferral>, IReasonReferralRepository
    {
        public EntityFrameworkReasonReferralRepository()
        {
            DisplayColumnForCombobox = "Name";
        }
        public IList<ReasonReferralWithReferralVo> GetSelectedReasonReferralbyReferral(int referralId)
        {
            var reasonReferralSelected = GetReasonReferralSelectedInReferralIds(referralId);

            var queryResult =
                from entity in WorkCompDb.ReasonReferrals
                where
                    reasonReferralSelected.Contains(entity.Id)
                orderby entity.Name
                select
                    new ReasonReferralWithReferralVo
                    {
                        Id = entity.Id,
                        Name = entity.Name,
                        ParentId = entity.ParentId
                    };
            var result = queryResult.Distinct();
            return result.ToList();
        }


        private IList<int> GetReasonReferralSelectedInReferralIds(int referralId)
        {
            return
                WorkCompDb.ReferralReasonReferrals.Where(x => x.ReferralId == referralId)
                    .Select(x => x.ReasonReferralId.Value)
                    .ToList();
        }

        public IList<ReasonReferralWithReferralVo> GetUnselectedReasonReferralByReferral(int referralId)
        {
            var reasonReferralSelected = GetReasonReferralSelectedInReferralIds(referralId);
            var objListReasonReferral = WorkCompDb.ReasonReferrals.OrderBy(o=>o.Name).ToList();
            var listParents = objListReasonReferral.Where(o=>o.ParentId==null).Select(o=>new ReasonReferralWithReferralVo
            {
                Id = o.Id,
                Name = o.Name,
            }).ToList();
            var objResult = new List<ReasonReferralWithReferralVo>();
            foreach (var parentItem in listParents)
            {
                GetChildItemForTreeView(objListReasonReferral, parentItem, reasonReferralSelected);
                objResult.Add(parentItem);
            }
            // Remove selected item for referral
            return objResult;
        }

        private static void GetChildItemForTreeView(List<ReasonReferral> objListReasonReferral, ReasonReferralWithReferralVo parentItem,IList<int> listSelectedId )
        {
            var listChild = objListReasonReferral.Where(o => o.ParentId == parentItem.Id && !listSelectedId.Contains(o.Id)).Select(o => new ReasonReferralWithReferralVo
            {
                Id = o.Id,
                Name = o.Name,
            }).ToList();
            if (listChild.Any())
            {
                parentItem.Childrent = listChild;
                foreach (var childItem in listChild)
                {
                    GetChildItemForTreeView(objListReasonReferral, childItem, listSelectedId);
                }
            }
            else
            {
                parentItem.Childrent = new List<ReasonReferralWithReferralVo>();
            }
        }

        //private IList<ReasonReferralWithReferralVo> GetChildByReasonReferralAndReferral(int reasonReferralId, int referralId)
        //{
        //    var queryResult = from reasonReferral in this.WorkCompDb.ReasonReferrals
        //                      join referralReasonReferral in WorkCompDb.ReferralReasonReferrals on reasonReferral.Id equals referralReasonReferral.ReasonReferralId
        //                      where referralReasonReferral.ReferralId == referralId && referralReasonReferral.ReasonReferralId == reasonReferralId
        //                      orderby reasonReferral.Name
        //                      select
        //                        new ReasonReferralWithReferralVo
        //                        {
        //                            Id = referralReasonReferral.ReasonReferralId.Value,
        //                            Name = reasonReferral.Name
        //                        };
        //    return queryResult.Distinct().ToList();
        //}

        public IList<ReasonReferralParrentVo> GetReasonReferral(int? parrentId)
        {
            var datas= GetAll().Distinct().ToList();
            return datas.Where(o => o.ParentId == parrentId).Select(item => new ReasonReferralParrentVo()
            {
                Id = item.Id,
                Name = HttpUtility.HtmlDecode(item.Name),
                HasChild = (datas.Any(o => o.ParentId == item.Id)),
                ReportsTo = parrentId,
                IsActive = item.IsActive
            }).ToList();
            
        }

        public bool UpdateActiveReasonReferral(int id)
        {
            var data = WorkCompDb.Set<ReasonReferral>().FirstOrDefault(o => o.Id == id);
            if (data!= null)
            {
                data.IsActive = !data.IsActive;
                
                if (data.ParentId == null)
                {
                    var listChirent = WorkCompDb.Set<ReasonReferral>().Where(o => o.ParentId == data.Id).ToList();
                    if (listChirent.Count > 0)
                    {
                        foreach (var reasonReferral in listChirent)
                        {
                            reasonReferral.IsActive = !reasonReferral.IsActive;
                        }
                    }
                }
                Commit();
                return true;
            }
            return false;
        }

        public List<LookupItemVo> GetListReasonReferral()
        {
            return WorkCompDb.ReasonReferrals.Select(o => new LookupItemVo
            {
                KeyId = o.Id,
                DisplayName = o.Name
            }).ToList();
        } 
    }
}