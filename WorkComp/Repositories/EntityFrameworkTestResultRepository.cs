﻿using Framework.DomainModel.Entities;
using Repositories.Interfaces;

namespace Repositories
{
    public class EntityFrameworkTestResultRepository : EntityFrameworkRepositoryBase<TestResult>, ITestResultRepository
    {
    }
}