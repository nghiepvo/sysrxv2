﻿using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.Data;
using System.Data.SqlClient;
using System.Linq;
using System.Linq.Dynamic;
using System.Security.Cryptography;
using System.Text;
using System.Data.Entity;
using Framework.DomainModel.Entities;
using Framework.DomainModel.Entities.Common;
using Framework.DomainModel.Interfaces;
using Framework.DomainModel.ValueObject;
using Framework.Utility;
using Repositories.Interfaces;
using Solr.DomainModel;

namespace Repositories
{
    public class EntityFrameworkReferralRepository : EntityFrameworkRepositoryBase<Referral>, IReferralRepository
    {
        public EntityFrameworkReferralRepository()
            : base()
        {
            #region Nghiep: Add Filter Column
            FilterColumns.Add("Id", new Collection<string>());
            FilterColumns.Add("ControlNumber", new Collection<string>());
            FilterColumns.Add("ProductType", new Collection<string>());
            FilterColumns.Add("DueDate", new Collection<string>() { "DueDateDateTime" });
            FilterColumns.Add("TreatingPhysicianName", new Collection<string>() { "CollectionSiteName", "NpiNumberName" });
            FilterColumns.Add("TreatingPhysicianPhone", new Collection<string>() { "CollectionSitePhone", "NpiNumberPhone" });
            FilterColumns.Add("AssignTo", new Collection<string>() );
            FilterColumns.Add("Payer", new Collection<string>());
            FilterColumns.Add("Jurisdiction", new Collection<string>());
            FilterColumns.Add("Claimant", new Collection<string>());
            FilterColumns.Add("ClaimNumber", new Collection<string>());
            FilterColumns.Add("PatientState", new Collection<string>());
            FilterColumns.Add("Doi", new Collection<string>() { "DoiDateTime" });
            FilterColumns.Add("CreatedDate", new Collection<string>() { "CreatedDateDateTime" });
            FilterColumns.Add("CreatedBy", new Collection<string>());
            #endregion

            DisplayColumnForCombobox = "ProductType.Name";
        }

        public List<ReferralMedicationHistory> GetMedicationHistoriesByReferral(int referralId)
        {
            return WorkCompDb.ReferralMedicationHistories.Where(o => o.ReferralId == referralId).Include(t => t.Drug).Include(t => t.ProvidedBy).ToList();
        }
        #region Get Send Mail Referral Detail

        public SendMailWithReferralIdVo GetSendMailInfo(int referralId)
        {
            var query = (from entity in GetAll().Where(o=>o.Id == referralId)
                join cn in WorkCompDb.Set<ClaimNumber>() on entity.ClaimantNumberId equals cn.Id into intoClaimNumber
                from claimNumber in intoClaimNumber.DefaultIfEmpty()
                select new {entity, claimNumber}
                ).Select(o => new SendMailWithReferralIdVo()
                {
                    ReferralId = o.entity.Id,
                    ClaimNumber = o.claimNumber.Name,
                    Claimant = o.claimNumber.Claimant!= null? o.claimNumber.Claimant.FirstName + " " + (string.IsNullOrEmpty(o.claimNumber.Claimant.MiddleName)? "": o.claimNumber.Claimant.MiddleName + " ") + o.claimNumber.Claimant.LastName: "",
                    FileName = o.entity.FileResult
                });
            return query.FirstOrDefault();
        }
        #endregion

        #region Detail Referral

        public ReferralDetailVo GetReferralDeltailById(int id)
        {
            #region query
            var queryResult = (from entity in GetAll()
                join p in WorkCompDb.Set<ProductType>() on entity.ProductTypeId equals p.Id into intoProductType
                from productType in intoProductType.DefaultIfEmpty()
                join rs in WorkCompDb.Set<ReferralSource>() on entity.ReferralSourceId equals rs.Id into intoReferralSource
                from referralSource in intoReferralSource.DefaultIfEmpty()
                join cm in WorkCompDb.Set<CaseManager>() on entity.ReferralSourceId equals cm.Id into intoCaseManager
                from caseManager in intoCaseManager.DefaultIfEmpty()
                join a in WorkCompDb.Set<Attorney>() on entity.AttorneyId equals a.Id into intoAttorney
                from attorney in intoAttorney.DefaultIfEmpty()
                join col in WorkCompDb.Set<ReferralCollectionSite>() on entity.Id equals col.ReferralId into
                    intoReferralCollectionSite
                from referralCollectionSite in intoReferralCollectionSite.DefaultIfEmpty()
                join npi in WorkCompDb.Set<ReferralNpiNumber>() on entity.Id equals npi.ReferralId into
                    intoReferralNpiNumber
                from referralNpiNumber in intoReferralNpiNumber.DefaultIfEmpty()
                join ass in WorkCompDb.Set<User>() on entity.AssignToId equals ass.Id into intoAssignTo
                from assignTo in intoAssignTo.DefaultIfEmpty()
                join cn in WorkCompDb.Set<ClaimNumber>() on entity.ClaimantNumberId equals cn.Id into intoClaimNumber
                from claimNumber in intoClaimNumber.DefaultIfEmpty()
                join createdBy in WorkCompDb.Set<User>() on entity.CreatedById equals createdBy.Id
                where entity.Id == id
                select
                    new
                    {
                        entity,
                        productType,
                        referralCollectionSite,
                        referralNpiNumber,
                        assignTo,
                        claimNumber,
                        createdBy,
                        referralSource,
                        caseManager,
                        attorney
                    })
                .Select(o => new ReferralDetailVo()
                {
                    #region ReferralInfo
                    ReferralInfoId = o.entity.Id,
                    ReferralInfoControlNumber = o.entity.ControlNumber,
                    ReferralInfoProductType = o.productType!= null? o.productType.Name: "",
                    ReferralInfoDateEntered = o.entity.EnteredDate,
                    ReferralInfoDateReceived = o.entity.ReceivedDate,
                    ReferralInfoDueDate = o.entity.DueDate,
                    ReferralInfoStatusId = o.entity.StatusId,
                    ReferralInfoAssignToId = o.entity.AssignToId,
                    ReferralInfoAssignToFirstName = o.assignTo!= null? o.assignTo.FirstName: "",
                    ReferralInfoAssignToMiddleName = o.assignTo!= null? o.assignTo.MiddleName: "",
                    ReferralInfoAssignToLastName = o.assignTo!= null? o.assignTo.LastName: "",
                    ReferralInfoReferralMethodId = o.entity.ReferralMethodId,
                    ReferralInfoSpecialInstruction = o.entity.SpecialInstruction,
                    ReferralInfoCreatedByFirstName = o.createdBy!= null? o.createdBy.FirstName: "",
                    ReferralInfoCreatedByMiddleName = o.createdBy!= null? o.createdBy.MiddleName: "",
                    ReferralInfoCreatedByLastName = o.createdBy!= null? o.createdBy.LastName: "",
                    ReferralInfoCreatedDate = o.entity.CreatedOn,
                    ReferralInfoPanelTypeId = o.entity.PanelTypeId,
                    #endregion

                    #region ReferralSourceInfo
                    ReferralSourceInfoFirstName = o.referralSource!= null? o.referralSource.FirstName: "",
                    ReferralSourceInfoMiddleName = o.referralSource!= null? o.referralSource.MiddleName: "",
                    ReferralSourceInfoLastName = o.referralSource!= null? o.referralSource.LastName:"",
                    ReferralSourceInfoType = o.referralSource!= null && o.referralSource.ReferralType!= null? o.referralSource.ReferralType.Name: "",
                    ReferralSourceInfoCompany = o.referralSource != null ? o.referralSource.Company : "",
                    ReferralSourceInfoPhone = o.referralSource != null ? o.referralSource.Phone : "",
                    ReferralSourceInfoFax = o.referralSource != null ? o.referralSource.Fax : "",
                    ReferralSourceInfoEmail = o.referralSource != null ? o.referralSource.Email : "",
                    ReferralSourceInfoState  = o.referralSource != null && o.referralSource.State!= null? o.referralSource.State.Name : "",
                    ReferralSourceInfoCity = o.referralSource != null && o.referralSource.City != null ? o.referralSource.City.Name : "",
                    ReferralSourceInfoZip = o.referralSource != null && o.referralSource.Zip != null ? o.referralSource.Zip.Name : "",
                    ReferralSourceInfoAddress  = o.referralSource != null ? o.referralSource.Company : "",
                    ReferralSourceInfoAuthorizationFrom  = o.referralSource != null ? o.referralSource.AuthorizationFrom : null,
                    ReferralSourceInfoAuthorizationTo = o.referralSource != null ? o.referralSource.AuthorizationTo : null,
                    ReferralSourceInfoIsAuthorization = o.referralSource != null && o.referralSource.IsAuthorization,
                    #endregion

                    #region PayorInfo
                    PayorInfoPayer = o.claimNumber!= null && o.claimNumber.Payer!= null? o.claimNumber.Payer.Name: "",
                    PayorInfoBranch = o.claimNumber != null && o.claimNumber.Branch != null ? o.claimNumber.Branch.Name : "",
                    PayorInfoSpecialInstruction = o.claimNumber != null && o.claimNumber.Payer != null ? o.claimNumber.Payer.SpecialInstructions : "",
                    #endregion

                    #region AdjusterInfo
                    AdjusterIsReferral = o.entity.AdjusterIsReferral,
                    AdjusterInfoFirstName = o.claimNumber!= null && o.claimNumber.Adjuster!= null? o.claimNumber.Adjuster.FirstName: "",
                    AdjusterInfoMiddleName = o.claimNumber!= null && o.claimNumber.Adjuster!= null? o.claimNumber.Adjuster.MiddleName: "",
                    AdjusterInfoLastName = o.claimNumber!= null && o.claimNumber.Adjuster!= null? o.claimNumber.Adjuster.LastName: "",
                    AdjusterInfoEmail = o.claimNumber!= null && o.claimNumber.Adjuster!= null? o.claimNumber.Adjuster.Email: "",
                    AdjusterInfoPhone = o.claimNumber!= null && o.claimNumber.Adjuster!= null? o.claimNumber.Adjuster.Phone: "",
                    AdjusterInfoExternalId = o.claimNumber!= null && o.claimNumber.Adjuster!= null? o.claimNumber.Adjuster.ExternalId: "",
                    AdjusterInfoState = o.claimNumber != null && o.claimNumber.Adjuster != null && o.claimNumber.Adjuster.State != null ? o.claimNumber.Adjuster.State.Name : "",
                    AdjusterInfoCity = o.claimNumber != null && o.claimNumber.Adjuster != null && o.claimNumber.Adjuster.City != null ? o.claimNumber.Adjuster.City.Name : "",
                    AdjusterInfoZip = o.claimNumber != null && o.claimNumber.Adjuster != null && o.claimNumber.Adjuster.Zip != null ? o.claimNumber.Adjuster.Zip.Name : "",
                    AdjusterInfoAddress  = o.claimNumber!= null && o.claimNumber.Adjuster!= null? o.claimNumber.Adjuster.Address: "",
                    #endregion

                    #region ClaimantInfo
                    ClaimantInfoId = o.claimNumber != null && o.claimNumber.Claimant != null ? o.claimNumber.Claimant.Id : 0,
                    ClaimantInfoFirstName = o.claimNumber!= null &&  o.claimNumber.Claimant != null? o.claimNumber.Claimant.FirstName: "",
                    ClaimantInfoMiddleName = o.claimNumber!= null &&  o.claimNumber.Claimant != null? o.claimNumber.Claimant.MiddleName: "",
                    ClaimantInfoLastName = o.claimNumber!= null &&  o.claimNumber.Claimant != null? o.claimNumber.Claimant.LastName: "",
                    ClaimantInfoPayerPatientId = o.claimNumber!= null &&  o.claimNumber.Claimant != null? o.claimNumber.Claimant.PayerPatientId: "",
                    ClaimantInfoHomePhone = o.claimNumber!= null &&  o.claimNumber.Claimant != null? o.claimNumber.Claimant.HomePhone: "",
                    ClaimantInfoCellPhone = o.claimNumber!= null &&  o.claimNumber.Claimant != null? o.claimNumber.Claimant.CellPhone: "",
                    ClaimantInfoGender = o.claimNumber!= null &&  o.claimNumber.Claimant != null? o.claimNumber.Claimant.Gender: "",
                    ClaimantInfoDateOfBirth = o.claimNumber != null && o.claimNumber.Claimant != null ? o.claimNumber.Claimant.Birthday : null,
                    ClaimantInfoSsnId = o.claimNumber!= null &&  o.claimNumber.Claimant != null? o.claimNumber.Claimant.Ssn: "",
                    ClaimantInfoClaimantLanguage = o.claimNumber!= null &&  o.claimNumber.Claimant != null? o.claimNumber.Claimant.Ssn: "",
                    ClaimantInfoEmail = o.claimNumber != null && o.claimNumber.Claimant != null ? o.claimNumber.Claimant.Email : "",
                    ClaimantInfoAddress1 = o.claimNumber != null && o.claimNumber.Claimant != null ? o.claimNumber.Claimant.Address1 : "",
                    ClaimantInfoAddress2 = o.claimNumber != null && o.claimNumber.Claimant != null ? o.claimNumber.Claimant.Address2 : "",
                    ClaimantInfoState = o.claimNumber != null && o.claimNumber.Claimant != null && o.claimNumber.Claimant.State != null ? o.claimNumber.Claimant.State.Name : "",
                    ClaimantInfoCity = o.claimNumber != null && o.claimNumber.Claimant != null && o.claimNumber.Claimant.City != null ? o.claimNumber.Claimant.City.Name : "",
                    ClaimantInfoZip = o.claimNumber != null && o.claimNumber.Claimant != null && o.claimNumber.Claimant.Zip != null ? o.claimNumber.Claimant.Zip.Name : "",
                   
                    #endregion

                    #region ClaimNumberInfo
                    ClaimNumberInfoClaimNumber = o.claimNumber != null? o.claimNumber.Name: "",
                    ClaimNumberInfoDoi = o.claimNumber != null? o.claimNumber.Doi: DateTime.MinValue,
                    ClaimNumberInfoClaimJurisdiction = o.claimNumber != null && o.claimNumber.State != null ? o.claimNumber.State.AbbreviationName : "",
                    ClaimNumberInfoSpecialInstruction = o.claimNumber != null ? o.claimNumber.SpecialInstructions : "",
                    ClaimNumberInfoStatusId = o.claimNumber != null ? o.claimNumber.Status : 0,
                    ClaimNumberInfoOpenDate = o.claimNumber != null ? o.claimNumber.OpenDate : null,
                    ClaimNumberInfoCloseDate = o.claimNumber != null ? o.claimNumber.CloseDate : null,
                    ClaimNumberInfoReopenDate = o.claimNumber != null ? o.claimNumber.ReOpenDate : null,
                    ClaimNumberInfoRecloseDate = o.claimNumber != null ? o.claimNumber.ReClosedDate : null,
                    #endregion

                    #region CaseManagerInfo
                    CaseManagerInfoName = o.caseManager!=null? o.caseManager.Name: "",
                    CaseManagerInfoEmail = o.caseManager!=null? o.caseManager.Email: "",
                    CaseManagerInfoPhone = o.caseManager!=null? o.caseManager.Phone: "",
                    CaseManagerInfoFax = o.caseManager!=null? o.caseManager.Fax: "",
                    CaseManagerInfoAgency = o.caseManager!=null? o.caseManager.Agency: "",
                    CaseManagerInfoSpecialInstruction = o.caseManager!=null? o.caseManager.SpecialInstructions: "",
                    CaseManagerInfoState = o.caseManager != null && o.caseManager.State != null ? o.caseManager.State.Name : "",
                    CaseManagerInfoCity = o.caseManager != null && o.caseManager.City != null ? o.caseManager.City.Name : "",
                    CaseManagerInfoZip = o.caseManager != null && o.caseManager.Zip != null ? o.caseManager.Zip.Name : "",
                    CaseManagerInfoAddress = o.caseManager != null ? o.caseManager.Address : "",
                    #endregion

                    #region  AttorneyInfo
                    AttorneyInfoFirstName = o.attorney!= null? o.attorney.FirstName: "",
                    AttorneyInfoMiddleName = o.attorney!= null? o.attorney.MiddleName: "",
                    AttorneyInfoLastName = o.attorney!= null? o.attorney.LastName: "",
                    AttorneyInfoEmail = o.attorney!= null? o.attorney.Email: "",
                    AttorneyInfoPhone = o.attorney!= null? o.attorney.Phone: "",
                    AttorneyInfoState = o.attorney!= null && o.attorney.State!= null? o.attorney.State.Name: "",
                    AttorneyInfoCity = o.attorney != null && o.attorney.City != null ? o.attorney.City.Name : "",
                    AttorneyInfoZip = o.attorney != null && o.attorney.Zip != null ? o.attorney.Zip.Name : "",
                    AttorneyInfoAddress = o.attorney != null ? o.attorney.Address : "",
                    #endregion

                    #region EmployerInfo
                    EmployerInfoName = o.claimNumber != null && o.claimNumber.Employer != null? o.claimNumber.Employer.Name: "",
                    EmployerInfoPhone = o.claimNumber != null && o.claimNumber.Employer != null? o.claimNumber.Employer.Phone: "",
                    EmployerInfoState = o.claimNumber != null && o.claimNumber.Employer != null && o.claimNumber.Employer.State != null ? o.claimNumber.Employer.State.Name : "",
                    EmployerInfoCity = o.claimNumber != null && o.claimNumber.Employer != null && o.claimNumber.Employer.City != null ? o.claimNumber.Employer.City.Name : "",
                    EmployerInfoZip = o.claimNumber != null && o.claimNumber.Employer != null && o.claimNumber.Employer.Zip != null ? o.claimNumber.Employer.Zip.Name : "",
                    EmployerInfoAddress = o.claimNumber != null && o.claimNumber.Employer != null ? o.claimNumber.Employer.Address : "",
                    #endregion

                    #region CollectionMethodInfo
                    #region Referral
                    IsCollectionSite = o.entity.IsCollectionSite,
                    ReferralRush = o.entity.Rush,
                    #endregion 

                    #region ReferralColectionSite
                    ReferralColectionSite = o.referralCollectionSite!= null && o.referralCollectionSite.CollectionSite != null? o.referralCollectionSite.CollectionSite.Name: "",
                    ReferralColectionSiteSpecialHandling = o.referralCollectionSite!= null? o.referralCollectionSite.SpecialInstructions: "",
                    ReferralColectionSitePhone = o.referralCollectionSite!= null? o.referralCollectionSite.Phone: "",
                    ReferralColectionSiteFax = o.referralCollectionSite!= null? o.referralCollectionSite.Fax: "",
                    ReferralColectionSiteAddress = o.referralCollectionSite!= null? o.referralCollectionSite.Address: "",
                    ReferralColectionSiteCollectionDate =  o.referralCollectionSite!= null? o.referralCollectionSite.CollectionDate: null,
                    #endregion

                    #region ReferralNpiNumber
                    ReferralNpiNumberTreatingPhysicianFirstName = o.referralNpiNumber != null && o.referralNpiNumber.NpiNumber!= null? o.referralNpiNumber.NpiNumber.FirstName:"",
                    ReferralNpiNumberTreatingPhysicianMiddleName = o.referralNpiNumber != null && o.referralNpiNumber.NpiNumber!= null? o.referralNpiNumber.NpiNumber.MiddleName:"",
                    ReferralNpiNumberTreatingPhysicianLastName = o.referralNpiNumber != null && o.referralNpiNumber.NpiNumber!= null? o.referralNpiNumber.NpiNumber.LastName:"",
                    ReferralNpiNumberNextMdVisit = o.referralNpiNumber != null? o.referralNpiNumber.NextMdVisitDate: null,
                    ReferralNpiNumberNpiNumber = o.referralNpiNumber != null && o.referralNpiNumber.NpiNumber!= null? o.referralNpiNumber.NpiNumber.Npi:"",
                    ReferralNpiNumberSpecialHandling = o.referralNpiNumber != null? o.referralNpiNumber.SpecialHandling: null,
                    ReferralNpiNumberPhone = o.referralNpiNumber != null? o.referralNpiNumber.Phone: null,
                    ReferralNpiNumberFax = o.referralNpiNumber != null? o.referralNpiNumber.Fax: null,
                    ReferralNpiNumberEmail = o.referralNpiNumber != null ? o.referralNpiNumber.Email : null,
                    ReferralNpiNumberAddress = o.referralNpiNumber != null ? o.referralNpiNumber.Address : null,
                    ReferralNpiNumberTreatingPhysicianOrganizationName = o.referralNpiNumber != null && o.referralNpiNumber.NpiNumber != null ? o.referralNpiNumber.NpiNumber.OrganizationName : "",
                    #endregion
                    #endregion

                    TotalAttachment = o.entity.ReferralAttachments.Count
                });
            #endregion
            return queryResult.FirstOrDefault();
        }

        public List<ReasonReferralInReferralDetailVo> GetReasonReferralInfos(int id)
        {
            #region query
            var queryResult = (from entity in WorkCompDb.Set<ReferralReasonReferral>()
                               join reason in WorkCompDb.Set<ReasonReferral>() on entity.ReasonReferralId equals reason.Id into intoReasonReferral
                               from reasonReferral in intoReasonReferral.DefaultIfEmpty()
                               where entity.ReferralId == id
                               select new { entity, reasonReferral }).Select(o => new ReasonReferralInReferralDetailVo()
                {
                    Id = o.reasonReferral.Id,
                    ParentId =  o.reasonReferral.ParentId,
                    Name = o.reasonReferral.Name,
                    NameParent = o.reasonReferral.Parent!= null? o.reasonReferral.Parent.Name: "",
                });
            #endregion

            var data = queryResult.ToList();
            return data;
        }

        public dynamic GetListTaskByReferralId(int id)
        {
            #region query
            var queryResult = (from entity in WorkCompDb.Set<ReferralTask>()
                               join u in WorkCompDb.Set<User>() on entity.AssignToId equals u.Id into intoUser
                               from assignTo in intoUser.DefaultIfEmpty()
                               join cb in WorkCompDb.Set<User>() on entity.CreatedById equals cb.Id into intoCreatedBy
                               from createdBy in intoCreatedBy.DefaultIfEmpty()
                               select new { entity, assignTo, createdBy }).Where(o => o.entity.ReferralId == id).OrderByDescending(o=>o.entity.Id)
                .Select(o => new ReferralTaskGridVo()
                {
                    Id = o.entity.Id,
                    Title = o.entity.Title,
                    StatusId = o.entity.StatusId,
                    StartDateDateTime = o.entity.StartDate,
                    DueDateDateTime = o.entity.DueDate,
                    AssignTo =
                        o.assignTo != null
                            ? o.assignTo.FirstName + " " +
                              (!string.IsNullOrEmpty(o.assignTo.MiddleName) ? o.assignTo.MiddleName + " " : "") +
                              o.assignTo.LastName
                            : "",
                    CreatedDateDateTime = o.entity.CreatedOn,
                    CreatedBy =
                        o.createdBy != null
                            ? o.createdBy.FirstName + " " +
                              (!string.IsNullOrEmpty(o.createdBy.MiddleName) ? o.createdBy.MiddleName + " " : "") +
                              o.createdBy.LastName
                            : "",
                    CompletedDate = o.entity.CompletedDate,
                    CancelDate = o.entity.CancelDate,
                });
            #endregion query

            var data = queryResult.ToList();
            var fomartStatus = string.Format("({0}/{1})", data.Count(o => o.StatusId == 3 || o.StatusId == 2), data.Count());

            return new { Data = data, FormatStatus = fomartStatus };

        }
        #endregion

        #region Referral Result Detal

        public ReferralResultDetailVo GetReferralResultInfo(int referralId)
        {
            var query = (from entity in GetAll().Where(o => o.Id == referralId)
                join t in WorkCompDb.Set<TestResult>() on entity.TestResultId equals t.Id into intoTestResult
                from testResult in intoTestResult.DefaultIfEmpty()
                join cn in WorkCompDb.Set<ClaimNumber>() on entity.ClaimantNumberId equals cn.Id into intoClaimNumber
                from claimNumber in intoClaimNumber.DefaultIfEmpty()
                select new {entity, testResult, claimNumber}).Select(o => new ReferralResultDetailVo()
                {
                    ReferralId = o.entity.Id,
                    ClaimNumber = o.claimNumber != null ? o.claimNumber.Name : "",
                    Claimant =
                        o.claimNumber != null && o.claimNumber.Claimant != null
                            ? o.claimNumber.Claimant.FirstName + " " +
                              (string.IsNullOrEmpty(o.claimNumber.Claimant.MiddleName)
                                  ? ""
                                  : o.claimNumber.Claimant.MiddleName + " ") + o.claimNumber.Claimant.LastName
                            : "",
                    FileResult = o.entity.FileResult,
                    IsResult = !string.IsNullOrEmpty(o.entity.FileResult),
                    ContentHtml = o.testResult.HtmlResult
                });

            var configuration =
                WorkCompDb.Set<Configuration>().FirstOrDefault(o => o.Id == (int) ConfigurationIdByEnum.HeaderReport);
            var data = query.FirstOrDefault();
            if (data != null && configuration != null)
            {
                data.HeaderReult = configuration.Value;
            }
            else
            {
                data = new ReferralResultDetailVo();
            }
            return data;
        }
        #endregion

        #region Parent Referral

        public override IQueryable<ReadOnlyGridVo> BuildQueryToGetDataForGrid(IQueryInfo queryInfo)
        {
            var conditionTypeWithUser = "";
            switch (queryInfo.TypeWithUser)
            {
                    case TypeWithUserQueryEnum.Current:
                    conditionTypeWithUser = "AssignToId == " + queryInfo.CreatedBy;
                    break;
                case TypeWithUserQueryEnum.Other:
                    conditionTypeWithUser = "AssignToId != " + queryInfo.CreatedBy;
                    break;
                default:
                    conditionTypeWithUser = "1 == 1";
                    break;
            }
            var referralQueryInfo = queryInfo as ReferralQueryInfo;
            if (referralQueryInfo == null)
            {
                return null;
            }
            var isGetAllDataForDueDate = false || referralQueryInfo.DueDateFrom == null && referralQueryInfo.DueDateTo == null;

            #region query
            var queryResult = (from entity in GetAll().Where(conditionTypeWithUser)
                join p in WorkCompDb.Set<ProductType>() on entity.ProductTypeId equals p.Id into intoProductType
                from productType in intoProductType.DefaultIfEmpty()
                join col in WorkCompDb.Set<ReferralCollectionSite>() on entity.Id equals col.ReferralId into
                    intoReferralCollectionSite
                from referralCollectionSite in intoReferralCollectionSite.DefaultIfEmpty()
                join npi in WorkCompDb.Set<ReferralNpiNumber>() on entity.Id equals npi.ReferralId into
                    intoReferralNpiNumber
                from referralNpiNumber in intoReferralNpiNumber.DefaultIfEmpty()
                join ass in WorkCompDb.Set<User>() on entity.AssignToId equals ass.Id into intoAssignTo
                from assignTo in intoAssignTo.DefaultIfEmpty()
                join cn in WorkCompDb.Set<ClaimNumber>() on entity.ClaimantNumberId equals cn.Id into intoClaimNumber
                from claimNumber in intoClaimNumber.DefaultIfEmpty()
                join createdBy in WorkCompDb.Set<User>() on entity.CreatedById equals createdBy.Id
                               where isGetAllDataForDueDate ||(entity.DueDate >= referralQueryInfo.DueDateFrom.Value && entity.DueDate <= referralQueryInfo.DueDateTo.Value)
                select
                    new
                    {
                        entity,
                        productType,
                        referralCollectionSite,
                        referralNpiNumber,
                        assignTo,
                        claimNumber,
                        createdBy
                    }).OrderBy(queryInfo.SortString)
                .Select(o => new ReferralParentGridVo()
                {
                    Id = o.entity.Id,
                    StatusId = o.entity.StatusId,
                    TotalNote = o.entity.ReferralNotes.Count,
                    TotalTask = o.entity.ReferralTasks.Count(),
                    TotalTaskCompleted = o.entity.ReferralTasks.Count(t => t.StatusId == 3),
                    ControlNumber = o.entity.ControlNumber,
                    ProductType = o.productType != null ? o.productType.Name : "",
                    StartDateDateTime = o.entity.ReceivedDate,
                    DueDateDateTime = o.entity.DueDate,
                    CancelDate = o.entity.CancelDate,
                    CompletedDate = o.entity.CompletedDate,
                    IsCollectionSite = o.entity.IsCollectionSite != null && (bool) o.entity.IsCollectionSite,
                    CollectionSiteName =
                        o.referralCollectionSite != null && o.referralCollectionSite.CollectionSite != null
                            ? o.referralCollectionSite.CollectionSite.Name
                            : "",
                    CollectionSitePhone =
                        o.referralCollectionSite != null && o.referralCollectionSite.CollectionSite != null
                            ? o.referralCollectionSite.CollectionSite.Phone
                            : "",
                    NpiNumberName = o.referralNpiNumber != null && o.referralNpiNumber.NpiNumber != null
                        ? o.referralNpiNumber.NpiNumber.FirstName + " " +
                          (!string.IsNullOrEmpty(o.referralNpiNumber.NpiNumber.MiddleName)
                              ? o.referralNpiNumber.NpiNumber.MiddleName + " "
                              : "") +
                          o.referralNpiNumber.NpiNumber.LastName
                        : "",

                    NpiNumberPhone =
                        o.referralNpiNumber != null && o.referralNpiNumber.NpiNumber != null
                            ? o.referralNpiNumber.NpiNumber.Phone
                            : "",
                    AssignTo = o.assignTo != null
                        ? o.assignTo.FirstName + " " +
                          (!string.IsNullOrEmpty(o.assignTo.MiddleName) ? o.assignTo.MiddleName + " " : "") +
                          o.assignTo.LastName
                        : "",
                    Payer = o.claimNumber != null && o.claimNumber.Payer != null ? o.claimNumber.Payer.Name : "",
                    Jurisdiction = o.claimNumber != null && o.claimNumber.State != null ? o.claimNumber.State.Name : "",
                    ClaimNumber = o.claimNumber != null ? o.claimNumber.Name : "",
                    DoiDateTime = o.claimNumber != null ? o.claimNumber.Doi : DateTime.MinValue,
                    Rush = o.entity.Rush != null && o.entity.Rush.Value,
                    Claimant = o.claimNumber != null && o.claimNumber.Claimant != null
                        ? o.claimNumber.Claimant.FirstName + " " +
                          (!string.IsNullOrEmpty(o.claimNumber.Claimant.MiddleName)
                              ? o.claimNumber.Claimant.MiddleName + " "
                              : "") +
                          o.claimNumber.Claimant.LastName
                        : "",
                    PatientState =
                        o.claimNumber != null && o.claimNumber.Claimant != null && o.claimNumber.Claimant.State != null
                            ? o.claimNumber.Claimant.State.Name
                            : "",
                    CreatedDateDateTime = o.entity.CreatedOn,
                    CreatedBy =
                        o.createdBy != null
                            ? o.createdBy.FirstName + " " +
                              (!string.IsNullOrEmpty(o.createdBy.MiddleName) ? o.createdBy.MiddleName + " " : "") +
                              o.createdBy.LastName
                            : "",
                });
            #endregion

            return queryResult;
        }

        public dynamic GetDataParentForGrid(IQueryInfo queryInfo)
        {
            
             BuildSortExpression(queryInfo);
            // Caculate for filter list
            var conditions = FilterForGetData(queryInfo);
            var finalResult =
                BuildQueryToGetDataForGrid(queryInfo)
                    .AsQueryable()
                    .Where(conditions);
            queryInfo.TotalRecords = finalResult.Count();
            
            var data = finalResult.Skip(queryInfo.Skip)
                    .Take(queryInfo.Take)
                    .ToList();
            return new { Data = data, TotalRowCount = queryInfo.TotalRecords };
        }

        #region Filter Query
        private string FilterForGetData(IQueryInfo queryInfo)
        {
            if (queryInfo.Filters == null || queryInfo.Filters.Count < 1)
                return " 1 = 1 ";
            var content = new StringBuilder();
            content.Append("(");
            var conditionList = new List<string>();
            foreach (var filter in queryInfo.Filters)
            {
                var condition = "";

                if (FilterColumns.ContainsKey(filter.Field))
                {
                    if (filter.FieldFilters.Count > 0)
                    {
                        for (var i = 0; i < filter.FieldFilters.Count; i++)
                        {
                            var fieldFilter = filter.FieldFilters[i];
                            if (!string.IsNullOrEmpty(fieldFilter.Value))
                            {
                                
                                if (FilterColumns[filter.Field].Count > 0)
                                {
                                    var lengthCollectionFilter = FilterColumns[filter.Field].Count;
                                    for (var j = 0; j  < lengthCollectionFilter; j++)
                                    {
                                        var item = FilterColumns[filter.Field][j];
                                        condition = string.Concat(condition, MapFilterString(fieldFilter.Operation, filter.FieldType, item, fieldFilter.Value));
                                        if (j + 1 < lengthCollectionFilter)
                                        {
                                            condition = string.Concat(condition, MapLogical(Logic.Or));
                                        }
                                    }
                                }
                                else
                                {
                                    condition = string.Concat(condition, MapFilterString(fieldFilter.Operation, filter.FieldType, filter.Field, fieldFilter.Value));
                                }
                            }
                            if (i < (filter.FieldFilters.Count - 1))
                            {
                                condition = string.Concat(condition, MapLogical(filter.Logical));
                            }

                            if (i == (filter.FieldFilters.Count - 1) && i >= 1)
                            {
                                condition = string.Concat("(", condition, ")");
                            }
                        }


                    }
                }
                conditionList.Add(condition);
            }

            if (conditionList.Count < 1) return " 1 = 1 ";
            content.Append(String.Join(MapLogical(Logic.And), conditionList.ToArray()));
            content.Append(")");
            return content.ToString();
        }

        private string MapLogical(Logic op)
        {
            switch (op)
            {
                case Logic.And:
                    return " && ";
                case Logic.Or:
                    return " || ";
                default:
                    return string.Empty;
            }
        }

        private string MapFilterString(Operator op, TypeDataKendo fieldType, string fieldName, string value)
        {
            if (value.IsFormatPhone())
            {
                value = value.RemoveFormatPhone();
            }
            DateTime temp;
            switch (op)
            {
                //1
                case Operator.Equals:
                    switch (fieldType)
                    {
                            
                        case TypeDataKendo.String:
                            return string.Format(" String.Compare({0}.ToLower(), \"{1}\") == 0 ", fieldName, value.ToLower());
                        case TypeDataKendo.Number:
                            return string.Format(" {0} == {1} ", fieldName, value);
                        case TypeDataKendo.DateTime:
                            value = value.Replace("Z", "");
                            if (DateTime.TryParse(value, out temp))
                            {
                                return string.Format(" {0}.ToString() == \"{1}\"", fieldName, GetFormatDateTime(temp));
                            }
                            return string.Empty;
                        case TypeDataKendo.Date:
                            value = value.Replace("Z", "");
                            if (DateTime.TryParse(value, out temp))
                            {
                                return string.Format(" {0}.ToString() == \"{1}\"", fieldName, temp.ToString("yyyy-MM-dd"));
                            }
                            return string.Empty;
                        default:
                            return string.Empty;
                    }
                //2
                case Operator.NotEquals:
                    switch (fieldType)
                    {
                        case TypeDataKendo.String:
                            return string.Format(" !({0}.ToLower().Equals(\"{1}\")) ", fieldName, value.ToLower());
                        case TypeDataKendo.Number:
                            return string.Format(" {0} != {1} ", fieldName, value);
                        case TypeDataKendo.DateTime:
                            value = value.Replace("Z", "");
                            if (DateTime.TryParse(value, out temp))
                            {
                                return string.Format(" String.Compare({0}, \"{1}\") != 0 ", fieldName, GetFormatDateTime(temp));
                            }
                            return string.Empty;
                        case TypeDataKendo.Date:
                            value = value.Replace("Z", "");
                            if (DateTime.TryParse(value, out temp))
                            {
                                return string.Format(" String.Compare({0}.ToString(), \"{1}\") != 0 ", fieldName, temp.ToString("yyyy-MM-dd"));
                            }
                            return string.Empty;
                        default:
                            return string.Empty;
                    }
                //3
                case Operator.GreaterThan:
                    switch (fieldType)
                    {
                        case TypeDataKendo.Number:
                            return string.Format(" {0} > {1} ", fieldName, value);
                        case TypeDataKendo.DateTime:
                            value = value.Replace("Z", "");
                            if (DateTime.TryParse(value, out temp))
                            {
                                return string.Format(" String.Compare({0}.ToString(), \"{1}\") > 0 ", fieldName, GetFormatDateTime(temp));
                            }
                            return string.Empty;
                        case TypeDataKendo.Date:
                            value = value.Replace("Z", "");
                            if (DateTime.TryParse(value, out temp))
                            {
                                return string.Format(" String.Compare({0}.ToString(), \"{1}\") > 0 ", fieldName, temp.ToString("yyyy-MM-dd"));
                            }
                            return string.Empty;
                        default:
                            return string.Empty;
                    }
                    //4
                case Operator.GreaterThanOrEqual:
                    switch (fieldType)
                    {
                        case TypeDataKendo.Number:
                            return string.Format(" {0} >= {1} ", fieldName, value);
                        case TypeDataKendo.DateTime:
                            value = value.Replace("Z", "");
                            if (DateTime.TryParse(value, out temp))
                            {
                                return string.Format(" String.Compare({0}.ToString(), \"{1}\") >= 0 ", fieldName, GetFormatDateTime(temp));
                            }
                            return string.Empty;
                        case TypeDataKendo.Date:
                            value = value.Replace("Z", "");
                            if (DateTime.TryParse(value, out temp))
                            {
                                return string.Format(" String.Compare({0}.ToString(), \"{1}\") >= 0 ", fieldName, temp.ToString("yyyy-MM-dd"));
                            }
                            return string.Empty;
                        default:
                            return string.Empty;
                    }
                //5
                case Operator.LessThan:
                    switch (fieldType)
                    {
                        case TypeDataKendo.Number:
                            return string.Format(" {0} < {1} ", fieldName, value);
                        case TypeDataKendo.DateTime:
                            value = value.Replace("Z", "");
                            if (DateTime.TryParse(value, out temp))
                            {
                                return string.Format(" String.Compare({0}.ToString(), \"{1}\") < 0 ", fieldName, GetFormatDateTime(temp));
                            }
                            return string.Empty;
                        case TypeDataKendo.Date:
                            value = value.Replace("Z", "");
                            if (DateTime.TryParse(value, out temp))
                            {
                                return string.Format(" String.Compare({0}.ToString(), \"{1}\") < 0 ", fieldName, temp.ToString("yyyy-MM-dd"));
                            }
                            return string.Empty;
                        default:
                            return string.Empty;
                    }
                //6
                case Operator.LessThanOrEqual:
                    switch (fieldType)
                    {
                        case TypeDataKendo.Number:
                            return string.Format(" {0} <= {1} ", fieldName, value);
                        case TypeDataKendo.DateTime:
                            value = value.Replace("Z", "");
                            if (DateTime.TryParse(value, out temp))
                            {
                                return string.Format(" String.Compare({0}.ToString(), \"{1}\") <= 0 ", fieldName, GetFormatDateTime(temp));
                            }
                            return string.Empty;
                        case TypeDataKendo.Date:
                            value = value.Replace("Z", "");
                            if (DateTime.TryParse(value, out temp))
                            {
                                return string.Format(" String.Compare({0}.ToString(), \"{1}\") <= 0 ", fieldName, temp.ToString("yyyy-MM-dd"));
                            }
                            return string.Empty;
                        default:
                            return string.Empty;
                            
                    }
                //7
                case Operator.Contains:
                    return fieldType == TypeDataKendo.String ? string.Format(" {0}.ToLower().Contains(\"{1}\") ", fieldName, value) : string.Empty;
                //8
                case Operator.NotContains:
                    return fieldType == TypeDataKendo.String ? string.Format(" !({0}.ToLower().Contains(\"{1}\")) ", fieldName, value) : string.Empty;
                //9
                case Operator.StartsWith:
                    return fieldType == TypeDataKendo.String ? string.Format(" {0}.ToLower().StartsWith(\"{1}\") ", fieldName, value) : string.Empty;
                //10
                case Operator.EndsWith:
                    return fieldType == TypeDataKendo.String ? string.Format(" {0}.ToLower().EndsWith(\"{1}\") ", fieldName, value) : string.Empty;
            }
            return null;

        }

        private string GetFormatDateTime(DateTime dateTime)
        {
            //MMM d yyyy h:mmtt
            //var resultFormatDateTime = "MMM d yyyy h:mmtt";
            var format = "";
            format += dateTime.Month > 9 ? "MM" : "MMM";
            format += dateTime.Day > 9 ? " d" : "  d";
            format += " yyyy";
            format += ((dateTime.Hour > 9 && dateTime.Hour < 13) || (dateTime.Hour > 21 && dateTime.Hour <= 23))? " h":"  h";
            format += ":mmtt";

            return dateTime.ToString(format);
        }
        #endregion

        #region Build Query Sql
        #endregion

        #endregion

        #region Children Referral

        #region Referral Task 
        private dynamic GetDataChildrenReferralTask(IQueryInfo queryInfo)
        {
            var finalResult = BuildQueryToGetDataChildrenReferralTaskByParentId(queryInfo).AsQueryable();
            queryInfo.TotalRecords = finalResult.Count();
            var data = finalResult.Skip(queryInfo.Skip)
                    .Take(queryInfo.Take)
                    .ToList();
            return new {data, TotalRowCount = queryInfo.TotalRecords };
        }

        private IEnumerable<ReadOnlyGridVo> BuildQueryToGetDataChildrenReferralTaskByParentId(IQueryInfo queryInfo)
        {
            var queryResult = (from entity in WorkCompDb.Set<ReferralTask>()
                join u in WorkCompDb.Set<User>() on entity.AssignToId equals u.Id into intoUser
                from assignTo in intoUser.DefaultIfEmpty()
                join cb in WorkCompDb.Set<User>() on entity.CreatedById equals cb.Id into intoCreatedBy
                from createdBy in intoCreatedBy.DefaultIfEmpty()
                select new {entity, assignTo, createdBy}).Where(o => o.entity.ReferralId == (queryInfo.ParentId ?? 0))
                .OrderBy(queryInfo.SortString)
                .Select(o => new ReferralTaskGridVo()
                {
                    Id = o.entity.Id,
                    Title = o.entity.Title,
                    StatusId = o.entity.StatusId,
                    StartDateDateTime = o.entity.StartDate,
                    DueDateDateTime = o.entity.DueDate,
                    CancelDate = o.entity.CancelDate,
                    CompletedDate = o.entity.CompletedDate,
                    AssignTo =
                        o.assignTo != null
                            ? o.assignTo.FirstName + " " +
                              (!string.IsNullOrEmpty(o.assignTo.MiddleName) ? o.assignTo.MiddleName + " " : "") +
                              o.assignTo.LastName
                            : "",
                    CreatedDateDateTime = o.entity.CreatedOn,
                    CreatedBy =
                        o.createdBy != null
                            ? o.createdBy.FirstName + " " +
                              (!string.IsNullOrEmpty(o.createdBy.MiddleName) ? o.createdBy.MiddleName + " " : "") +
                              o.createdBy.LastName
                            : "",
                });

            return queryResult;
        }
        #endregion Referral Task

        #region Referral Node
        private dynamic GetDataChildrenReferralNote(IQueryInfo queryInfo)
        {
            var finalResult = BuildQueryToGetDataChildrenReferralNoteByParentId(queryInfo).AsQueryable();
            queryInfo.TotalRecords = finalResult.Count();
            var data = finalResult.Skip(queryInfo.Skip)
                    .Take(queryInfo.Take)
                    .ToList();
            return new {data, TotalRowCount = queryInfo.TotalRecords };
        }

        private IEnumerable<ReadOnlyGridVo> BuildQueryToGetDataChildrenReferralNoteByParentId(IQueryInfo queryInfo)
        {
            var queryResult = (from entity in WorkCompDb.Set<ReferralNote>()
                               join u in WorkCompDb.Set<User>() on entity.AssignToId equals u.Id into intoUser
                               from assignTo in intoUser.DefaultIfEmpty()
                               join cb in WorkCompDb.Set<User>() on entity.CreatedById equals cb.Id into intoCreatedBy
                               from createdBy in intoCreatedBy.DefaultIfEmpty()
                               select new { entity, assignTo, createdBy }).Where(o => o.entity.ReferralId == (queryInfo.ParentId ?? 0)).OrderBy(queryInfo.SortString)
                .Select(o => new ReferralNoteGridVo()
                {
                    Id = o.entity.Id,
                    NoteTypeId = o.entity.NoteType,
                    Comment = o.entity.Comment,
                    AssignToFirstName = o.assignTo != null ? o.assignTo.FirstName : "",
                    AssignToMiddleName = o.assignTo != null && !string.IsNullOrEmpty(o.assignTo.MiddleName) ? o.assignTo.MiddleName : "",
                    AssignToLastName = o.assignTo != null ? o.assignTo.FirstName : "",
                    CreatedDateDateTime = o.entity.CreatedOn,
                    CreatedByFirstName = o.createdBy != null ? o.createdBy.FirstName : "",
                    CreatedByMiddleName = o.createdBy != null && !string.IsNullOrEmpty(o.createdBy.MiddleName) ? o.createdBy.MiddleName : "",
                    CreatedByLastName = o.createdBy != null ? o.createdBy.LastName : ""
                });

            return queryResult;
        }
        #endregion
        #endregion Children Referral

        #region BuildSortExpression commond
        protected override void BuildDefaultSortExpression(IQueryInfo queryInfo)
        {
            if (queryInfo.Sort == null || queryInfo.Sort.Count == 0)
            {
                queryInfo.Sort = new List<Sort> { new Sort { Field = "DueDate", Dir = "asc" } };
            }
        }

        protected override void BuildSortExpression(IQueryInfo queryInfo)
        {
            base.BuildSortExpression(queryInfo);
            queryInfo.Sort.ForEach(x =>
            {
                if (x.Field.Contains("entity.ProductType"))
                {
                    x.Field = "(productType != null) ? productType.Name : string.Empty";
                }
                if (x.Field.Contains("entity.Payer"))
                {
                    x.Field = "(claimNumber != null && claimNumber.Payer != null) ? claimNumber.Payer.Name : string.Empty";
                }
                if (x.Field.Contains("entity.Jurisdiction"))
                {
                    x.Field = "(claimNumber != null && claimNumber.State != null) ? claimNumber.State.Name : string.Empty";
                }
                if (x.Field.Contains("entity.ClaimNumber"))
                {
                    x.Field = "(claimNumber != null) ? claimNumber.Name : string.Empty";
                }
                if (x.Field.Contains("entity.Doi"))
                {
                    x.Field = "(claimNumber != null) ? claimNumber.Doi : null";
                }
                if (x.Field.Contains("entity.PatientState"))
                {
                    x.Field = "claimNumber != null && claimNumber.Claimant != null && claimNumber.Claimant.State != null ? claimNumber.Claimant.State.Name : string.Empty";
                }
                if (x.Field.Contains("entity.CreatedDate"))
                {
                    x.Field = "entity.CreatedOn";
                }

            });

            bool flagIsCollection = false, flagIsCollectionPhone = false, flagAssignTo = false, flagClaimant = false, flagCreatedBy = false;

            var dir = "";
            if (queryInfo.Sort == null || queryInfo.Sort.Count <= 0) return;



            foreach (var sort in queryInfo.Sort.Where(sort => sort.Field == "entity.TreatingPhysicianName"))
            {
                sort.Field = "(referralCollectionSite != null && referralCollectionSite.CollectionSite != null)?  referralCollectionSite.CollectionSite.Name : string.Empty";
                dir = sort.Dir;
                flagIsCollection = true;
            }
            if (flagIsCollection)
            {
                queryInfo.Sort.Add(new Sort { Field = "(referralNpiNumber != null && referralNpiNumber.NpiNumber != null)? referralNpiNumber.NpiNumber.FirstName : string.Empty", Dir = dir });
                queryInfo.Sort.Add(new Sort { Field = "(referralNpiNumber != null && referralNpiNumber.NpiNumber != null)? referralNpiNumber.NpiNumber.MiddleName : string.Empty", Dir = dir });
                queryInfo.Sort.Add(new Sort { Field = "(referralNpiNumber != null && referralNpiNumber.NpiNumber != null)? referralNpiNumber.NpiNumber.LastName : string.Empty", Dir = dir });
            }

            foreach (var sort in queryInfo.Sort.Where(sort => sort.Field == "entity.TreatingPhysicianPhone"))
            {
                sort.Field = "(referralCollectionSite != null && referralCollectionSite.CollectionSite != null)?  referralCollectionSite.CollectionSite.Phone : string.Empty";
                dir = sort.Dir;
                flagIsCollectionPhone = true;
            }
            if (flagIsCollectionPhone)
            {
                queryInfo.Sort.Add(new Sort { Field = "(referralNpiNumber != null && referralNpiNumber.NpiNumber != null)? referralNpiNumber.NpiNumber.Phone : string.Empty", Dir = dir });
            }

            foreach (var sort in queryInfo.Sort.Where(sort => sort.Field == "entity.AssignTo"))
            {
                sort.Field = "assignTo != null ? assignTo.FirstName : string.Empty";
                dir = sort.Dir;
                flagAssignTo = true;
            }

            if (flagAssignTo)
            {
                queryInfo.Sort.Add(new Sort { Field = "assignTo != null ? assignTo.MiddleName : string.Empty", Dir = dir });
                queryInfo.Sort.Add(new Sort { Field = "assignTo != null ? assignTo.LastName : string.Empty", Dir = dir });
            }

            foreach (var sort in queryInfo.Sort.Where(sort => sort.Field == "entity.Claimant"))
            {
                sort.Field = "(claimNumber != null && claimNumber.Claimant != null) ? claimNumber.Claimant.FirstName : string.Empty";
                dir = sort.Dir;
                flagClaimant = true;
            }

            if (flagClaimant)
            {
                queryInfo.Sort.Add(new Sort { Field = "(claimNumber != null && claimNumber.Claimant != null) ? claimNumber.Claimant.MiddleName : string.Empty", Dir = dir });
                queryInfo.Sort.Add(new Sort { Field = "(claimNumber != null && claimNumber.Claimant != null) ? claimNumber.Claimant.LastName : string.Empty", Dir = dir });
            }

            foreach (var sort in queryInfo.Sort.Where(sort => sort.Field == "entity.CreatedBy"))
            {
                sort.Field = "createdBy != null ? createdBy.FirstName : string.Empty";
                dir = sort.Dir;
                flagCreatedBy = true;
            }

            if (!flagCreatedBy) return;
            queryInfo.Sort.Add(new Sort { Field = "createdBy != null ? createdBy.MiddleName : string.Empty", Dir = dir });
            queryInfo.Sort.Add(new Sort { Field = "createdBy != null ? createdBy.LastName : string.Empty", Dir = dir });
        }
        #endregion BuildSortExpression commond

        #region Previous Next ReferralTask
        public int GetPreviousReferralId(int id)
        {
            var data = GetAll().Where(o => o.Id < id).OrderByDescending(o => o.Id).FirstOrDefault();
            return data != null ? data.Id : id;
        }

        public int GetNextReferralId(int id)
        {
            var data = GetAll().Where(o => o.Id > id).OrderBy(o => o.Id).FirstOrDefault();
            return data != null ? data.Id : id;
        }
        #endregion

        public dynamic GetDataChildrenForGrid(IQueryInfo queryInfo)
        {
            BuildSortExpression(queryInfo);

            var finalResultTask = BuildQueryToGetDataChildrenReferralTaskByParentId(queryInfo).AsQueryable();
            //queryInfo.TotalRecords = finalResultTask.Count();
            //var dataTask = finalResultTask.Skip(queryInfo.Skip)
            //        .Take(queryInfo.Take)
            //        .ToList();

            var dataTask = finalResultTask.ToList();

            var finalResultNote = BuildQueryToGetDataChildrenReferralNoteByParentId(queryInfo).AsQueryable();
            //queryInfo.TotalRecords = finalResultNote.Count();
            //var dataNote = finalResultNote.Skip(queryInfo.Skip)
            //        .Take(queryInfo.Take)
            //        .ToList();
            var dataNote = finalResultNote.ToList();
            return new { Data = new { TaskTab = dataTask, NoteTab = dataNote } };
        }

        public SolrReferral MapReferralToSolrReferral(int idReferral)
        {
            var objListReferralQuery = (from entity in WorkCompDb.Referrals
                                        join rnpi in WorkCompDb.ReferralNpiNumbers.Where(rnpi => rnpi.ReferralId == idReferral) on entity.Id equals rnpi.ReferralId into referralNpiNumbers
                                        from referralNpiNumber in referralNpiNumbers.DefaultIfEmpty()
                                        join prt in WorkCompDb.ProductTypes on entity.ProductTypeId equals prt.Id into productTypes
                                        from productType in productTypes.DefaultIfEmpty()
                                        join asg in WorkCompDb.Users on entity.AssignToId equals asg.Id into assignTos
                                        from assignTo in assignTos.DefaultIfEmpty()
                                        join clm in WorkCompDb.ClaimNumbers on entity.ClaimantNumberId equals clm.Id into claimantNumbers
                                        from claimantNumber in claimantNumbers.DefaultIfEmpty()
                                        join cre in WorkCompDb.Users on entity.CreatedById equals cre.Id into createdBys
                                        from createdBy in createdBys.DefaultIfEmpty()
                                        where entity.Id == idReferral
                                        select new { entity, referralNpiNumber, productType, assignTo, claimantNumber, createdBy });
            var objListReferralTemp = objListReferralQuery.Select(o => new
            {
                o.entity.Id,
                ReferralId = o.entity.Id,
                ControlNumber = o.entity.ControlNumber,
                ProductType = o.productType == null ? "" : o.productType.Name,
                TreatingPhysicianName = (o.referralNpiNumber == null)
                                ? ""
                                : o.referralNpiNumber.NpiNumber == null
                                    ? ""
                                    : (o.referralNpiNumber.NpiNumber.FirstName ?? "") + " "
                                      + (o.referralNpiNumber.NpiNumber.MiddleName ?? "") + " "
                                      + (o.referralNpiNumber.NpiNumber.LastName ?? ""),
                TreatingPhysicianFirstName = (o.referralNpiNumber == null)
                    ? ""
                    : o.referralNpiNumber.NpiNumber == null
                        ? ""
                        : (o.referralNpiNumber.NpiNumber.FirstName ?? ""),
                TreatingPhysicianMiddleName = (o.referralNpiNumber == null)
                    ? ""
                    : o.referralNpiNumber.NpiNumber == null
                        ? ""
                        : (o.referralNpiNumber.NpiNumber.MiddleName ?? ""),
                TreatingPhysicianLastName = (o.referralNpiNumber == null)
                    ? ""
                    : o.referralNpiNumber.NpiNumber == null
                        ? ""
                        : (o.referralNpiNumber.NpiNumber.LastName ?? ""),
                TreatingPhysicianAddress = (o.referralNpiNumber == null)
                    ? ""
                    : (o.referralNpiNumber.Address ?? ""),
                TreatingPhysicianCity = (o.referralNpiNumber == null)
                    ? ""
                    : o.referralNpiNumber.NpiNumber == null
                        ? ""
                        : (o.referralNpiNumber.NpiNumber.City == null
                            ? ""
                            : o.referralNpiNumber.NpiNumber.City.Name),
                TreatingPhysicianState = (o.referralNpiNumber == null)
                    ? ""
                    : o.referralNpiNumber.NpiNumber == null
                        ? ""
                        : (o.referralNpiNumber.NpiNumber.State == null
                            ? ""
                            : o.referralNpiNumber.NpiNumber.State.Name),
                TreatingPhysicianPostalCode = (o.referralNpiNumber == null)
                    ? ""
                    : o.referralNpiNumber.NpiNumber == null
                        ? ""
                        : (o.referralNpiNumber.NpiNumber.Zip == null
                            ? ""
                            : o.referralNpiNumber.NpiNumber.Zip.Name),
                TreatingPhysicianPhone = (o.referralNpiNumber == null)
                    ? ""
                    : (o.referralNpiNumber.Phone ?? ""),
                TreatingPhysicianFax = (o.referralNpiNumber == null)
                    ? ""
                    : (o.referralNpiNumber.Fax ?? ""),
                TreatingPhysicianEmail = (o.referralNpiNumber == null)
                    ? ""
                    : (o.referralNpiNumber.Email ?? ""),
                NpiNumber = (o.referralNpiNumber == null)
                    ? ""
                    : o.referralNpiNumber.NpiNumber == null
                        ? ""
                        : (o.referralNpiNumber.NpiNumber.Npi ?? ""),
                AssignToName = o.assignTo == null ? "" : o.assignTo.FirstName+" "+(o.assignTo.MiddleName??"")+o.assignTo.LastName,
                o.entity.StatusId,
                PayerName = o.claimantNumber == null ? "" : o.claimantNumber.Payer == null ? ""
                                     : o.claimantNumber.Payer.Name,
                Jurisdiction = o.claimantNumber == null ? "" : o.claimantNumber.State == null ? ""
                                     : o.claimantNumber.State.Name,
                Rush = o.entity.Rush,
                DueDate = o.entity.DueDate,
                ClaimantName = o.claimantNumber == null ? "" : o.claimantNumber.Claimant == null ? ""
                                    : (o.claimantNumber.Claimant.FirstName ?? "") + " "
                                        + (o.claimantNumber.Claimant.MiddleName ?? "") + " "
                                        + (o.claimantNumber.Claimant.LastName ?? ""),
                ClaimNumber = o.claimantNumber == null ? "" : o.claimantNumber.Name,
                PatientState = o.claimantNumber == null ? "" : o.claimantNumber.Claimant == null ? "" : o.claimantNumber.Claimant.State == null ? "" : o.claimantNumber.Claimant.State.Name,
                Doi = o.claimantNumber == null ? (DateTime?)null : o.claimantNumber.Doi,
                CreatedByName = o.createdBy == null ? "" : o.createdBy.FirstName + " " + (string.IsNullOrEmpty(o.createdBy.MiddleName) ? "" : o.createdBy.MiddleName + " ") + o.createdBy.LastName,
                CreatedDate = o.entity.CreatedOn,
                AdjusterName = o.claimantNumber == null ? "" : o.claimantNumber.Adjuster == null ? ""
                                    : (o.claimantNumber.Adjuster.FirstName ?? "") + " "
                                        + (o.claimantNumber.Adjuster.MiddleName ?? "") + " "
                                        + (o.claimantNumber.Adjuster.LastName ?? ""),
                NextOfficeVisitDate = (o.referralNpiNumber == null) ? (DateTime?)null : o.referralNpiNumber.NextMdVisitDate,
                ClaimantSsn = o.claimantNumber == null ? "" : o.claimantNumber.Claimant == null ? ""
                                    : (o.claimantNumber.Claimant.Ssn ?? ""),
                ClaimantDob = o.claimantNumber == null ? (DateTime?)null : o.claimantNumber.Claimant == null ? (DateTime?)null
                                    : o.claimantNumber.Claimant.Birthday,
                ClaimNumberDoi = o.claimantNumber == null ? (DateTime?)null : o.claimantNumber.Doi,
                TotalNote = o.entity.ReferralNotes.Count(),
                IsCollectionSite = o.entity.IsCollectionSite,
                o.entity.ReceivedDate,
                o.entity.AssignToId,
                ProductTypeId = o.productType.Id,
                NpiNumberId = (o.referralNpiNumber == null)
                    ? 0
                    : o.referralNpiNumber.NpiNumberId,
                PayerId = o.claimantNumber == null
                    ? 0
                    : o.claimantNumber.PayerId,
                ClaimantId = o.claimantNumber == null
                    ? 0
                    : o.claimantNumber.ClaimantId,
                ClaimNumberId = o.entity.ClaimantNumberId,
                AdjusterId = o.claimantNumber == null
                    ? 0
                    : o.claimantNumber.AdjusterId,
                o.entity.CancelDate,
                o.entity.CompletedDate
            });
            var referral = objListReferralTemp.FirstOrDefault();
            if (referral != null)
            {
                return new SolrReferral
                {
                    Id = referral.Id.ToString(),
                    ReferralId = referral.ReferralId,
                    ControlNumber = referral.ControlNumber,
                    ProductType = referral.ProductType,
                    TreatingPhysicianName = referral.TreatingPhysicianName,
                    TreatingPhysicianPhone = referral.TreatingPhysicianPhone,
                    AssignToName = referral.AssignToName,
                    Status = XmlDataHelpper.Instance.GetValue(XmlDataTypeEnum.Status.ToString(), referral.StatusId.ToString()),
                    PayerName = referral.PayerName,
                    Jurisdiction = referral.Jurisdiction,
                    Rush = referral.Rush.GetValueOrDefault(),
                    DueDate = referral.DueDate,
                    ClaimantName = referral.ClaimantName,
                    ClaimNumber = referral.ClaimNumber,
                    PatientState = referral.PatientState,
                    Doi = referral.Doi,
                    CreatedByName = referral.CreatedByName,
                    CreatedDate = referral.CreatedDate,
                    AdjusterName = referral.AdjusterName,
                    NextOfficeVisitDate = referral.NextOfficeVisitDate,
                    ClaimantSsn = referral.ClaimantSsn,
                    ClaimantDob = referral.ClaimantDob,
                    ClaimNumberDoi = referral.ClaimNumberDoi,
                    TotalNote = referral.TotalNote,
                    IsCollectionSite = referral.IsCollectionSite.GetValueOrDefault(),
                    AssignToId = referral.AssignToId,
                    RecievedDate = referral.ReceivedDate,
                    AdjusterId = referral.AdjusterId.GetValueOrDefault(),
                    ClaimNumberId = referral.ClaimNumberId,
                    ClaimantId = referral.ClaimantId.GetValueOrDefault(),
                    NpiNumberId = referral.NpiNumberId.GetValueOrDefault(),
                    PayerId = referral.PayerId.GetValueOrDefault(),
                    ProductTypeId = referral.ProductTypeId,
                    CompletedDate=referral.CompletedDate,
                    CancelDate = referral.CancelDate,
                    NpiNumber = referral.NpiNumber,
                    TreatingPhysicianAddress = referral.TreatingPhysicianAddress,
                    TreatingPhysicianCity = referral.TreatingPhysicianCity,
                    TreatingPhysicianEmail = referral.TreatingPhysicianEmail,
                    TreatingPhysicianFax = referral.TreatingPhysicianFax,
                    TreatingPhysicianFirstName = referral.TreatingPhysicianFirstName,
                    TreatingPhysicianLastName = referral.TreatingPhysicianLastName,
                    TreatingPhysicianMiddleName = referral.TreatingPhysicianMiddleName,
                    TreatingPhysicianPostCode = referral.TreatingPhysicianPostalCode,
                    TreatingPhysicianState = referral.TreatingPhysicianState
                };
            }
            return null;
        }

        public void RemoveAttachmentFile(int referralId, string rowGuid, string fileName)
        {
            var item =
                WorkCompDb.ReferralAttachments.FirstOrDefault(
                    o => o.ReferralId == referralId && o.AttachedFileName == fileName && o.RowGUID == new Guid(rowGuid));
            if (item != null)
            {
                WorkCompDb.ReferralAttachments.Remove(item);
            }
        }

        public void AddReferralAttachment(ReferralAttachment objAdd)
        {
            WorkCompDb.ReferralAttachments.Add(objAdd);
        }
        public void DeleteAllItemForReferral(int referralId)
        {
            var objListTaskId = WorkCompDb.ReferralTasks.Where(o => o.ReferralId == referralId).Select(o => new { o.Id }).ToList();
            var listIdTask = objListTaskId.Select(item => item.Id).ToList();
            var listDeleteAlert =
                WorkCompDb.Alerts.Where(
                        o => (o.LinkId == referralId && o.Type == 1) || (o.Type == 2 && listIdTask.Contains(o.LinkId)))
                    .ToList();
            WorkCompDb.Alerts.RemoveRange(listDeleteAlert);
        }

        public void AssignToForReferral(List<int> listIdSelected, bool isSelectAll, int? assignToId, TypeWithUserQueryEnum typeWithUser)
        {
            if (assignToId.GetValueOrDefault() == 0)
            {
                return;
            }
            if (isSelectAll)
            {
                var conditionTypeWithUser = "";
                var currentUser = WorkCompDb.GetCurrentUser().Id;
                switch (typeWithUser)
                {
                    case TypeWithUserQueryEnum.Current:
                        conditionTypeWithUser = "AssignToId == " + currentUser;
                        break;
                    case TypeWithUserQueryEnum.Other:
                        conditionTypeWithUser = "AssignToId != " + currentUser;
                        break;
                    default:
                        conditionTypeWithUser = "1 == 1";
                        break;
                }
                var listReferral = WorkCompDb.Set<Referral>().Where(conditionTypeWithUser).ToList();
                foreach (var referral in listReferral)
                {
                    referral.AssignToId = assignToId.GetValueOrDefault();
                    var listReferralTaskByReferralId = WorkCompDb.Set<ReferralTask>().Where(o => o.ReferralId == referral.Id).ToList();
                    foreach (var referralTask in listReferralTaskByReferralId.Where(referralTask => referralTask.StatusId == 1 || referralTask.StatusId == 4 || referralTask.StatusId == 5).Where(referralTask => !referralTask.Title.ToLower().Equals("request supply shipment")))
                    {
                        referralTask.AssignToId = assignToId.GetValueOrDefault();
                    }
                }
                
            }
            else if(listIdSelected.Count>0)
            {
                var listReferral = WorkCompDb.Set<Referral>().Where(o => listIdSelected.Contains(o.Id)).ToList();
                foreach (var referral in listReferral)
                {
                    referral.AssignToId = assignToId.GetValueOrDefault();
                    var listReferralTaskByReferralId = WorkCompDb.Set<ReferralTask>().Where(o => o.ReferralId == referral.Id).ToList();
                    foreach (var referralTask in listReferralTaskByReferralId.Where(referralTask => referralTask.StatusId == 1 || referralTask.StatusId == 4 || referralTask.StatusId == 5).Where(referralTask => !referralTask.Title.ToLower().Equals("request supply shipment")))
                    {
                        referralTask.AssignToId = assignToId.GetValueOrDefault();
                    }
                }
            }
            Commit();
        }

        public List<Referral> GetListReferralWithIncludeAssignTo(List<int> listIdSelected)
        {
            return WorkCompDb.Set<Referral>().Include("AssignTo").Where(o => listIdSelected.Contains(o.Id)).AsNoTracking().ToList();
        }

        public dynamic GetDataPrint(List<int> selectedIds, IQueryInfo queryInfo)
        {
            BuildSortExpression(queryInfo);
            // Caculate for filter list
            var finalResult =
                BuildQueryToGetDataForGrid(queryInfo)
                    .AsQueryable();
            finalResult = finalResult.Where(o => selectedIds.Contains(o.Id));
            queryInfo.TotalRecords = finalResult.Count();

            var data = finalResult.Skip(queryInfo.Skip)
                    .Take(queryInfo.Take)
                    .ToList();
            return new { Data = data, TotalRowCount = queryInfo.TotalRecords };
        }
    }
}