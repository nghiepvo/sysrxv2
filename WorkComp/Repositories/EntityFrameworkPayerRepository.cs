﻿using System.Linq;
using System.Linq.Dynamic;
using Framework.DomainModel.Entities;
using Repositories.Interfaces;
using Framework.DomainModel.ValueObject;
using Framework.DomainModel.Interfaces;

namespace Repositories
{
    public class EntityFrameworkPayerRepository : EntityFrameworkRepositoryBase<Payer>, IPayerRepository
    {
        public EntityFrameworkPayerRepository()
            : base()
        {
            SearchColumns.Add("Name");
            SearchColumns.Add("ControlNumberPrefix");
            SearchColumns.Add("SpecialInstructions");
            SearchColumns.Add("Address");
            SearchColumns.Add("State");
            SearchColumns.Add("City");
            SearchColumns.Add("Zip");

            DisplayColumnForCombobox = "Name";
        }
        public override IQueryable<ReadOnlyGridVo> BuildQueryToGetDataForGrid(IQueryInfo queryInfo)
        {
            var finalResult = (from entity in GetAll()
                               join city in WorkCompDb.Set<City>()
                                   on entity.CityId equals city.Id into jcity
                               from subcity in jcity.DefaultIfEmpty()
                               join state in WorkCompDb.Set<State>()
                                  on entity.StateId equals state.Id into jstate
                               from substate in jstate.DefaultIfEmpty()
                               join zip in WorkCompDb.Set<Zip>()
                                  on entity.ZipId equals zip.Id into jzip
                               from subzip in jzip.DefaultIfEmpty()
                               select new { entity, subcity, substate, subzip }).OrderBy(queryInfo.SortString).Select(x => new PayerGridVo
                               {
                                   Name = x.entity.Name,
                                   ControlNumberPrefix = x.entity.ControlNumberPrefix,
                                   SpecialInstructions = x.entity.SpecialInstructions,
                                   Address = x.entity.Address,
                                   Id = x.entity.Id,
                                   State = x.substate == null ? string.Empty : x.substate.Name,
                                   City = x.subcity == null ? string.Empty : x.subcity.Name,
                                   Zip = x.subzip == null ? string.Empty : x.subzip.Name
                               });
            return finalResult;
        }

        /// <summary>
        /// Defual id = 1
        /// </summary>
        /// <returns></returns>
        public LookupItemVo GetDefaulDataSource(int id)
        {
            var query =
                GetAll()
                    .Where(o => o.Id == id)
                    .Select(o => new LookupItemVo {DisplayName = o.Name, KeyId = o.Id});
            return query.FirstOrDefault();
        }

        protected override void BuildSortExpression(IQueryInfo queryInfo)
        {
            base.BuildSortExpression(queryInfo);
            queryInfo.Sort.ForEach(x =>
            {
                if (x.Field.Contains("State"))
                {
                    x.Field = "(entity.State != null) ? entity.State.Name : string.Empty";
                }
                else if (x.Field.Contains("City"))
                {
                    x.Field = "(entity.City != null) ? entity.City.Name : string.Empty";
                }
                else if (x.Field.Contains("Zip"))
                {
                    x.Field = "(entity.Zip != null) ? entity.Zip.Name : string.Empty";
                }
            });
        }
    }
}
