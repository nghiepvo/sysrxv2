﻿using System.Collections.Generic;
using System.Linq;
using System.Linq.Dynamic;
using Framework.DomainModel.Entities;
using Framework.DomainModel.Entities.Common;
using Framework.DomainModel.Interfaces;
using Framework.DomainModel.ValueObject;
using Repositories.Interfaces;

namespace Repositories
{
    public class EntityFrameworkCommentRepository : EntityFrameworkRepositoryBase<Comment>, ICommentRepository
    {

        public override IQueryable<ReadOnlyGridVo> BuildQueryToGetDataForGrid(IQueryInfo queryInfo)
        {
            var queryResult = (from entity in GetAll().Where(o=>o.ReferralTaskId == queryInfo.ParentId)
                join c in WorkCompDb.Set<User>() on entity.CreatedById equals c.Id into intoCreatedBy
                from createBy in intoCreatedBy.DefaultIfEmpty()
                               select new { entity, createBy })

                .OrderBy(queryInfo.SortString).Select(s => new CommentGridVo
            {
                Id = s.entity.Id,
                Comment = s.entity.CommentContent,
                CreatedByFistName = s.createBy != null ? s.createBy.FirstName: string.Empty,
                CreatedByMiddleName = s.createBy != null ? s.createBy.MiddleName : string.Empty,
                CreatedByLastName = s.createBy != null ? s.createBy.LastName : string.Empty,
                CreatedDateDateTime = s.entity.CreatedOn
            });
            return queryResult;
        }

        protected override void BuildDefaultSortExpression(IQueryInfo queryInfo)
        {
            if (queryInfo.Sort == null || queryInfo.Sort.Count == 0)
            {
                queryInfo.Sort = new List<Sort> { new Sort { Field = "Id", Dir = "asc" } };
            }
        }

        public IList<Comment> GetCommentWhenChangeReferral(int? referralTaskId)
        {
            return WorkCompDb.Comments.Where(p => p.ReferralTaskId == referralTaskId).ToList();
        }
    }
}