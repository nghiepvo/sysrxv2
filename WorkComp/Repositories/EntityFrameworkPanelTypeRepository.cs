﻿using System;
using System.Collections.Generic;
using System.Data.Entity;
using System.Linq;
using System.Linq.Dynamic;
using System.Text;
using Framework.DomainModel.Entities;
using Framework.DomainModel.Entities.Common;
using Framework.DomainModel.Interfaces;
using Framework.DomainModel.ValueObject;
using Repositories.Interfaces;

namespace Repositories
{
    public class EntityFrameworkPanelTypeRepository : EntityFrameworkRepositoryBase<PanelType>, IPanelTypeRepository
    {
        public EntityFrameworkPanelTypeRepository()
            
        {
            //Includes.Add("States");
            SearchColumns.Add("Name");
            DisplayColumnForCombobox = "Name";
        }
        public override IQueryable<ReadOnlyGridVo> BuildQueryToGetDataForGrid(IQueryInfo queryInfo)
        {
            var finalResult = (from entity in GetAll()
                               join p in WorkCompDb.Set<Payer>() on entity.PayerId equals p.Id into payers
                               from payer in payers.DefaultIfEmpty()
                               select new { entity, payer }).OrderBy(queryInfo.SortString).Select(x => new PanelTypeGridVo
                {
                    PayerName = x.payer == null ? "" : x.payer.Name,
                    Name = x.entity.Name,
                    IsBasic = x.entity.IsBasic != null && x.entity.IsBasic.Value,
                    Id = x.entity.Id,
                    Code = x.entity.Code,
                });
            return finalResult;
        }

        protected override void BuildSortExpression(IQueryInfo queryInfo)
        {
            base.BuildSortExpression(queryInfo);
            queryInfo.Sort.ForEach(x =>
            {
                if (x.Field.Contains("Payer"))
                {
                    x.Field = "(entity.Payer != null) ? entity.Payer.Name : string.Empty";
                }
            });
        }

        public List<LookupItemVo> GetListPanelTypeStandard()
        {
            return WorkCompDb.PanelTypes.Where(o=>o.IsBasic==true).OrderByDescending(o=>o.Id).Select(o => new LookupItemVo
            {
                KeyId = o.Id,
                DisplayName = o.Name
            }).ToList();
        }

        public List<PanelCode> GetListPanelCode(int panelTypeId)
        {
            return WorkCompDb.PanelCodes.Include(o => o.Panel).Where(o => o.PanelTypeId == panelTypeId).OrderBy(o => o.Panel.Name).ToList();
        }

        public InfoWhenChangePanelTypeInReferral GetInfoWhenChangePanelTypeInReferral(int idPanelType)
        {
            var query = from entity in GetAll()
                        where entity.Id == idPanelType
                        select new InfoWhenChangePanelTypeInReferral
                        {
                           PanelTypeCode = entity.Code,
                           PanelTypeId = entity.Id
                        };
            return query.FirstOrDefault();
        }

        public List<LookupItemVo> GetLookupForReferral(LookupQuery query, Func<PanelType, LookupItemVo> selector)
        {
            var where = new StringBuilder();
            @where.Append("(");
            var innerWhere = new List<string>();
            var queryDisplayName = String.Format("{0}.Contains(\"{1}\") ", DisplayColumnForCombobox, query.Query);
            innerWhere.Add(queryDisplayName);
            @where.Append(String.Join(" OR ", innerWhere.ToArray()));
            @where.Append(")");
            // In referral page => we need filter the panel type with payer. If payer is null => show list panel type with payer null( not show all like another lookup)
            if (query.HierachyItems != null)
            {
                if ((query.HierachyItems.Count == 0) || (query.HierachyItems.Count == 1 && query.HierachyItems[0].Value == "0"))
                {
                    @where.Append(string.Format(" AND ( PayerId = null)"));
                }
                foreach (var parentItem in query.HierachyItems.Where(parentItem => parentItem.Value != string.Empty
                    && parentItem.Value != "-1"
                    && parentItem.Value != "0"
                    && !parentItem.IgnoredFilter))
                {
                    var filterValue = parentItem.Value.Replace(",", string.Format(" OR {0} = ", parentItem.Name));
                    @where.Append(string.Format(" AND ( {0} = {1})", parentItem.Name, filterValue));
                }
            }
            var lookupWhere = @where.ToString();

            var lookupList = GetAll().AsNoTracking().Where(lookupWhere);
            var currentRecord = GetAll().AsNoTracking().Where(x => x.Id == query.Id);
            if (!query.IncludeCurrentRecord && currentRecord.SingleOrDefault() != null) // Return single record to reduce the size of return data when first time binding.
            {
                return currentRecord.Select(selector).ToList();
            }

            if (!string.IsNullOrEmpty(query.Query) || !query.IncludeCurrentRecord)
            {
                currentRecord = Enumerable.Empty<PanelType>().AsQueryable();
            }

            var lookupAnonymous = lookupList
                        .Union(currentRecord)
                        .OrderBy(DisplayColumnForCombobox)
                        .Skip(0)
                        .Take(query.Take)
                        .Select(selector);
            return lookupAnonymous.ToList();
        }
    }
}