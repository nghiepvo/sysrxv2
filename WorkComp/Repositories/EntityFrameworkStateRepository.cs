﻿using System.Collections.Generic;
using System.Linq;
using Framework.DomainModel.Entities;
using Framework.DomainModel.Entities.Common;
using Framework.DomainModel.Interfaces;
using Framework.DomainModel.ValueObject;
using System.Linq.Dynamic;
using Repositories.Interfaces;

namespace Repositories
{
    public class EntityFrameworkStateRepository : EntityFrameworkRepositoryBase<State>, IStateRepository
    {
        public EntityFrameworkStateRepository()
           
        {
            SearchColumns.Add("Name");
            SearchColumns.Add("AbbreviationName");
            DisplayColumnForCombobox = "Name";
        }

        public override IQueryable<ReadOnlyGridVo> BuildQueryToGetDataForGrid(IQueryInfo queryInfo)
        {
            var queryResult = GetAll().OrderBy(queryInfo.SortString).Select(s => new StateGridVo
            {
                Id = s.Id,
                Name = s.Name,
                AbbreviationName = s.AbbreviationName,
            });
            return queryResult;
        }

        protected override void BuildSortExpression(IQueryInfo queryInfo)
        {
            if (queryInfo.Sort == null || (queryInfo.Sort != null && queryInfo.Sort.Count == 0))
            {
                queryInfo.Sort = new List<Sort> { new Sort { Field = "Name", Dir = "asc" } };
            }
        }
    }
}