﻿using System.Diagnostics;
using System.Linq;
using Framework.DomainModel.Entities;
using Framework.DomainModel.Interfaces;
using Framework.DomainModel.ValueObject;
using System.Linq.Dynamic;
using Repositories.Interfaces;
using Repositories.Utility;

namespace Repositories
{
    public class EntityFrameworkAssayCodeRepository : EntityFrameworkRepositoryBase<AssayCode>, IAssayCodeRepository
    {
        public EntityFrameworkAssayCodeRepository()
        {
            SearchColumns.Add("Code");
            SearchColumns.Add("Description");
            SearchColumns.Add("SampleTestingType");
        }

        public override IQueryable<ReadOnlyGridVo> BuildQueryToGetDataForGrid(IQueryInfo queryInfo)
        {
            var queryResult = (from entity in GetAll()
                join s in WorkCompDb.Set<SampleTestingType>() on entity.SampleTestingTypeId equals s.Id into
                    sampleTestingTypes
                from sampleTestingType in sampleTestingTypes.DefaultIfEmpty()
                join a in WorkCompDb.Set<AssayCodeDescription>() on entity.AssayCodeDescriptionId equals a.Id into
                    assayCodeDescriptions
                from assayCodeDescription in assayCodeDescriptions.DefaultIfEmpty()
                select new {entity, assayCodeDescription, sampleTestingType})
                .OrderBy(queryInfo.SortString)
                .Select(s => new AssayCodeGridVo
                {
                    Id = s.entity.Id,
                    Description = s.assayCodeDescription != null ? s.assayCodeDescription.Description: string.Empty,
                    SampleTestingType = s.sampleTestingType != null ? s.sampleTestingType.Name : string.Empty,
                    Code = s.entity.Code
                });
            return queryResult;
        }

        protected override void BuildSortExpression(IQueryInfo queryInfo)
        {
            base.BuildSortExpression(queryInfo);
            queryInfo.Sort.ForEach(x =>
            {
                if (x.Field.Contains("Description"))
                {
                    x.Field = "(entity.assayCodeDescription != null) ? entity.assayCodeDescription.Description : string.Empty";
                }
                else if (x.Field.Contains("SampleTestingType"))
                {
                    x.Field = "(entity.sampleTestingType != null) ? entity.sampleTestingType.Name : string.Empty";
                }
            });
        }
    }
}