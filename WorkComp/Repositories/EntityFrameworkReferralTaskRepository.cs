﻿using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.Linq;
using System.Linq.Dynamic;
using System.Text;
using Framework.DomainModel.Entities;
using Framework.DomainModel.Entities.Common;
using Framework.DomainModel.Interfaces;
using Framework.DomainModel.ValueObject;
using Framework.Utility;
using Repositories.Interfaces;
using Solr.DomainModel;

namespace Repositories
{
    public class EntityFrameworkReferralTaskRepository : EntityFrameworkRepositoryBase<ReferralTask>, IReferralTaskRepository
    {
        public EntityFrameworkReferralTaskRepository()
            : base()
        {
            #region Nghiep: Add Filter Column
            FilterColumns.Add("Id", new Collection<string>());
            FilterColumns.Add("Title", new Collection<string>());
            FilterColumns.Add("DueDate", new Collection<string>() { "DueDateDateTime" });
            FilterColumns.Add("AssignTo", new Collection<string>());
            FilterColumns.Add("Payer", new Collection<string>());
            FilterColumns.Add("Jurisdiction", new Collection<string>());
            FilterColumns.Add("Claimant", new Collection<string>());
            FilterColumns.Add("PatientState", new Collection<string>());
            FilterColumns.Add("CreatedDate", new Collection<string>() { "CreatedDateDateTime" });
            #endregion
        }

        #region Nghiep work

        #region Parent Referral Task

        public override IQueryable<ReadOnlyGridVo> BuildQueryToGetDataForGrid(IQueryInfo queryInfo)
        {
            var conditionTypeWithUser = "";
            switch (queryInfo.TypeWithUser)
            {
                case TypeWithUserQueryEnum.Current:
                    conditionTypeWithUser = "AssignToId == " + queryInfo.CreatedBy;
                    break;
                case TypeWithUserQueryEnum.Other:
                    conditionTypeWithUser = "AssignToId != " + queryInfo.CreatedBy;
                    break;
                default:
                    conditionTypeWithUser = "1 == 1";
                    break;
            }

            var referralQueryInfo = queryInfo as ReferralQueryInfo;
            if (referralQueryInfo == null)
            {
                return null;
            }
            var isGetAllDataForDueDate = false || referralQueryInfo.DueDateFrom == null && referralQueryInfo.DueDateTo == null;

            var queryResult = (from entity in GetAll().Where(conditionTypeWithUser)
                               join refe in WorkCompDb.Set<Referral>() on entity.ReferralId equals refe.Id into intoReferral
                               from referral in intoReferral.DefaultIfEmpty()
                               join ass in WorkCompDb.Set<User>() on entity.AssignToId equals ass.Id into intoAssign
                               from assignTo in intoAssign.DefaultIfEmpty()
                               where isGetAllDataForDueDate || (entity.DueDate >= referralQueryInfo.DueDateFrom.Value && entity.DueDate <= referralQueryInfo.DueDateTo.Value)
                               select new { entity, referral, assignTo }).OrderBy(queryInfo.SortString)

                .Select(o => new ReferralTaskParentGridVo()
                {
                    Id = o.entity.Id,
                    StatusId = o.entity.StatusId,
                    ReferralId = o.entity.ReferralId,
                    Title = o.entity.Title,
                    StartDateDateTime = o.entity.StartDate,
                    DueDateDateTime = o.entity.DueDate,
                    CancelDate = o.entity.CancelDate,
                    CompletedDate = o.entity.CompletedDate,
                    AssignTo = o.assignTo != null
                        ? o.assignTo.FirstName + " " +
                          (!string.IsNullOrEmpty(o.assignTo.MiddleName) ? o.assignTo.MiddleName + " " : "") +
                          o.assignTo.LastName
                        : "",

                    Jurisdiction =
                        o.referral != null && o.referral.ClaimNumber != null && o.referral.ClaimNumber.State != null
                            ? o.referral.ClaimNumber.State.Name
                            : "",
                    Claimant =
                        o.referral != null && o.referral.ClaimNumber != null && o.referral.ClaimNumber.Claimant != null
                            ? o.referral.ClaimNumber.Claimant.FirstName + " " +
                              (!string.IsNullOrEmpty(o.referral.ClaimNumber.Claimant.MiddleName)
                                  ? o.referral.ClaimNumber.Claimant.MiddleName + " "
                                  : "") + o.referral.ClaimNumber.Claimant.LastName
                            : "",

                    PatientState =
                        o.referral != null && o.referral.ClaimNumber != null && o.referral.ClaimNumber.Claimant != null &&
                        o.referral.ClaimNumber.Claimant.State != null
                            ? o.referral.ClaimNumber.Claimant.State.Name
                            : "",
                    CreatedDateDateTime = o.entity.CreatedOn
                });

            return queryResult;
        }

        public dynamic GetDataParentForGrid(IQueryInfo queryInfo)
        {

            BuildSortExpression(queryInfo);
            // Caculate for filter list
            var conditions = FilterForGetData(queryInfo);

            var finalResult =
                BuildQueryToGetDataForGrid(queryInfo)
                    .AsQueryable()
                    .Where(conditions);
            queryInfo.TotalRecords = finalResult.Count();

            var data = finalResult.Skip(queryInfo.Skip)
                    .Take(queryInfo.Take)
                    .ToList();
            return new { Data = data, TotalRowCount = queryInfo.TotalRecords };
        }

        public SolrReferralTask MapReferralTaskToSolrReferral(int id)
        {
            var objListReferralTaskQuery = (from entity in WorkCompDb.ReferralTasks.Where(o => o.Id == id)
                                            join refe in WorkCompDb.Set<Referral>() on entity.ReferralId equals refe.Id into intoReferral
                                            from referral in intoReferral.DefaultIfEmpty()
                                            join asg in WorkCompDb.Users on entity.AssignToId equals asg.Id into assignTos
                                            from assignTo in assignTos.DefaultIfEmpty()
                                            select new { entity, referral, assignTo });
            var objListReferralTaskTemp = objListReferralTaskQuery
                .Select(o => new
                {
                    Id = o.entity.Id,
                    o.entity.StatusId,
                    ReferralId = o.entity.ReferralId,
                    Title = o.entity.Title,
                    StartDateDateTime = o.entity.StartDate,
                    DueDateDateTime = o.entity.DueDate,
                    AssignToFirstName = o.assignTo != null ? o.assignTo.FirstName : "",
                    AssignToMiddleName = o.assignTo != null ? o.assignTo.MiddleName : "",
                    AssignToLastName = o.assignTo != null ? o.assignTo.LastName : "",
                    Jurisdiction =
                        o.referral != null && o.referral.ClaimNumber != null && o.referral.ClaimNumber.State != null
                            ? o.referral.ClaimNumber.State.Name
                            : "",
                    ClaimantFirstName =
                        o.referral != null && o.referral.ClaimNumber != null && o.referral.ClaimNumber.Claimant != null
                            ? o.referral.ClaimNumber.Claimant.FirstName
                            : "",
                    ClaimantMiddleName =
                        o.referral != null && o.referral.ClaimNumber != null && o.referral.ClaimNumber.Claimant != null
                            ? o.referral.ClaimNumber.Claimant.MiddleName
                            : "",
                    ClaimantLastName =
                        o.referral != null && o.referral.ClaimNumber != null && o.referral.ClaimNumber.Claimant != null
                            ? o.referral.ClaimNumber.Claimant.LastName
                            : "",
                    PatientState =
                        o.referral != null && o.referral.ClaimNumber != null && o.referral.ClaimNumber.Claimant != null &&
                        o.referral.ClaimNumber.Claimant.State != null
                            ? o.referral.ClaimNumber.Claimant.State.Name
                            : "",
                    CreatedDateDateTime = o.entity.CreatedOn,
                    o.entity.AssignToId,
                    ClaimNumberId = o.referral != null
                        ? o.referral.ClaimantNumberId
                        : (int?) null,
                    ClaimantId = o.referral != null && o.referral.ClaimNumber != null
                        ? o.referral.ClaimNumber.ClaimantId
                        : (int?) null,
                    o.entity.CancelDate,
                    o.entity.CompletedDate
                });
            var referralTask = objListReferralTaskTemp.FirstOrDefault();
            if (referralTask != null)
            {
                return new SolrReferralTask
                {
                    Id = referralTask.Id.ToString(),
                    ReferralId = referralTask.ReferralId,
                    ReferralTaskId = referralTask.Id,
                    Title = referralTask.Title,
                    AssignToName = referralTask.AssignToFirstName + " " + referralTask.AssignToMiddleName + " " + referralTask.AssignToLastName,
                    Status = XmlDataHelpper.Instance.GetValue(XmlDataTypeEnum.Status.ToString(), referralTask.StatusId.ToString()),
                    Jurisdiction = referralTask.Jurisdiction,
                    DueDate = referralTask.DueDateDateTime,
                    ClaimantName = referralTask.ClaimantFirstName + " " + referralTask.ClaimantMiddleName + " " + referralTask.ClaimantLastName,
                    PatientState = referralTask.PatientState,
                    CreatedDate = referralTask.CreatedDateDateTime,
                    StartDate = referralTask.StartDateDateTime,
                    AssignToId = referralTask.AssignToId,
                    ClaimNumberId = referralTask.ClaimNumberId,
                    ClaimantId = referralTask.ClaimantId,
                    CompletedDate = referralTask.CompletedDate,
                    CancelDate = referralTask.CancelDate
                };
            }
            return null;
        }

        public bool CheckExitTaskCollectionServiceRequestLetter(int referralId)
        {
            return GetAll().Any(o => o.ReferralId == referralId && o.IsCollectionServiceRequestLetter == true);
        }

        #region Filter Query
        private string FilterForGetData(IQueryInfo queryInfo)
        {
            if (queryInfo.Filters == null || queryInfo.Filters.Count < 1)
                return " 1 = 1 ";
            var content = new StringBuilder();
            content.Append("(");
            var conditionList = new List<string>();
            foreach (var filter in queryInfo.Filters)
            {
                var condition = "";

                if (FilterColumns.ContainsKey(filter.Field))
                {
                    if (filter.FieldFilters.Count > 0)
                    {
                        for (var i = 0; i < filter.FieldFilters.Count; i++)
                        {
                            var fieldFilter = filter.FieldFilters[i];
                            if (!string.IsNullOrEmpty(fieldFilter.Value))
                            {

                                if (FilterColumns[filter.Field].Count > 0)
                                {
                                    var lengthCollectionFilter = FilterColumns[filter.Field].Count;
                                    for (var j = 0; j < lengthCollectionFilter; j++)
                                    {
                                        var item = FilterColumns[filter.Field][j];
                                        condition = string.Concat(condition, MapFilterString(fieldFilter.Operation, filter.FieldType, item, fieldFilter.Value));
                                        if (j + 1 < lengthCollectionFilter)
                                        {
                                            condition = string.Concat(condition, MapLogical(Logic.Or));
                                        }
                                    }
                                }
                                else
                                {
                                    condition = string.Concat(condition, MapFilterString(fieldFilter.Operation, filter.FieldType, filter.Field, fieldFilter.Value));
                                }
                            }
                            if (i < (filter.FieldFilters.Count - 1))
                            {
                                condition = string.Concat(condition, MapLogical(filter.Logical));
                            }

                            if (i == (filter.FieldFilters.Count - 1) && i >= 1)
                            {
                                condition = string.Concat("(", condition, ")");
                            }
                        }


                    }
                }
                conditionList.Add(condition);
            }

            if (conditionList.Count < 1) return " 1 = 1 ";
            content.Append(String.Join(MapLogical(Logic.And), conditionList.ToArray()));
            content.Append(")");
            return content.ToString();
        }

        private string MapLogical(Logic op)
        {
            switch (op)
            {
                case Logic.And:
                    return " && ";
                case Logic.Or:
                    return " || ";
                default:
                    return string.Empty;
            }
        }

        private string MapFilterString(Operator op, TypeDataKendo fieldType, string fieldName, string value)
        {
            if (value.IsFormatPhone())
            {
                value = value.RemoveFormatPhone();
            }
            DateTime temp;
            switch (op)
            {
                //1
                case Operator.Equals:
                    switch (fieldType)
                    {

                        case TypeDataKendo.String:
                            return string.Format(" String.Compare({0}.ToLower(), \"{1}\") == 0 ", fieldName, value.ToLower());
                        case TypeDataKendo.Number:
                            return string.Format(" {0} == {1} ", fieldName, value);
                        case TypeDataKendo.DateTime:
                            value = value.Replace("Z", "");
                            if (DateTime.TryParse(value, out temp))
                            {
                                return string.Format(" {0}.ToString() == \"{1}\"", fieldName, GetFormatDateTime(temp));
                            }
                            return string.Empty;
                        case TypeDataKendo.Date:
                            value = value.Replace("Z", "");
                            if (DateTime.TryParse(value, out temp))
                            {
                                return string.Format(" {0}.ToString() == \"{1}\"", fieldName, temp.ToString("yyyy-MM-dd"));
                            }
                            return string.Empty;
                        default:
                            return string.Empty;
                    }
                //2
                case Operator.NotEquals:
                    switch (fieldType)
                    {
                        case TypeDataKendo.String:
                            return string.Format(" !({0}.ToLower().Equals(\"{1}\")) ", fieldName, value.ToLower());
                        case TypeDataKendo.Number:
                            return string.Format(" {0} != {1} ", fieldName, value);
                        case TypeDataKendo.DateTime:
                            value = value.Replace("Z", "");
                            if (DateTime.TryParse(value, out temp))
                            {
                                return string.Format(" String.Compare({0}, \"{1}\") != 0 ", fieldName, GetFormatDateTime(temp));
                            }
                            return string.Empty;
                        case TypeDataKendo.Date:
                            value = value.Replace("Z", "");
                            if (DateTime.TryParse(value, out temp))
                            {
                                return string.Format(" String.Compare({0}.ToString(), \"{1}\") != 0 ", fieldName, temp.ToString("yyyy-MM-dd"));
                            }
                            return string.Empty;
                        default:
                            return string.Empty;
                    }
                //3
                case Operator.GreaterThan:
                    switch (fieldType)
                    {
                        case TypeDataKendo.Number:
                            return string.Format(" {0} > {1} ", fieldName, value);
                        case TypeDataKendo.DateTime:
                            value = value.Replace("Z", "");
                            if (DateTime.TryParse(value, out temp))
                            {
                                return string.Format(" String.Compare({0}.ToString(), \"{1}\") > 0 ", fieldName, GetFormatDateTime(temp));
                            }
                            return string.Empty;
                        case TypeDataKendo.Date:
                            value = value.Replace("Z", "");
                            if (DateTime.TryParse(value, out temp))
                            {
                                return string.Format(" String.Compare({0}.ToString(), \"{1}\") > 0 ", fieldName, temp.ToString("yyyy-MM-dd"));
                            }
                            return string.Empty;
                        default:
                            return string.Empty;
                    }
                //4
                case Operator.GreaterThanOrEqual:
                    switch (fieldType)
                    {
                        case TypeDataKendo.Number:
                            return string.Format(" {0} >= {1} ", fieldName, value);
                        case TypeDataKendo.DateTime:
                            value = value.Replace("Z", "");
                            if (DateTime.TryParse(value, out temp))
                            {
                                return string.Format(" String.Compare({0}.ToString(), \"{1}\") >= 0 ", fieldName, GetFormatDateTime(temp));
                            }
                            return string.Empty;
                        case TypeDataKendo.Date:
                            value = value.Replace("Z", "");
                            if (DateTime.TryParse(value, out temp))
                            {
                                return string.Format(" String.Compare({0}.ToString(), \"{1}\") >= 0 ", fieldName, temp.ToString("yyyy-MM-dd"));
                            }
                            return string.Empty;
                        default:
                            return string.Empty;
                    }
                //5
                case Operator.LessThan:
                    switch (fieldType)
                    {
                        case TypeDataKendo.Number:
                            return string.Format(" {0} < {1} ", fieldName, value);
                        case TypeDataKendo.DateTime:
                            value = value.Replace("Z", "");
                            if (DateTime.TryParse(value, out temp))
                            {
                                return string.Format(" String.Compare({0}.ToString(), \"{1}\") < 0 ", fieldName, GetFormatDateTime(temp));
                            }
                            return string.Empty;
                        case TypeDataKendo.Date:
                            value = value.Replace("Z", "");
                            if (DateTime.TryParse(value, out temp))
                            {
                                return string.Format(" String.Compare({0}.ToString(), \"{1}\") < 0 ", fieldName, temp.ToString("yyyy-MM-dd"));
                            }
                            return string.Empty;
                        default:
                            return string.Empty;
                    }
                //6
                case Operator.LessThanOrEqual:
                    switch (fieldType)
                    {
                        case TypeDataKendo.Number:
                            return string.Format(" {0} <= {1} ", fieldName, value);
                        case TypeDataKendo.DateTime:
                            value = value.Replace("Z", "");
                            if (DateTime.TryParse(value, out temp))
                            {
                                return string.Format(" String.Compare({0}.ToString(), \"{1}\") <= 0 ", fieldName, GetFormatDateTime(temp));
                            }
                            return string.Empty;
                        case TypeDataKendo.Date:
                            value = value.Replace("Z", "");
                            if (DateTime.TryParse(value, out temp))
                            {
                                return string.Format(" String.Compare({0}.ToString(), \"{1}\") <= 0 ", fieldName, temp.ToString("yyyy-MM-dd"));
                            }
                            return string.Empty;
                        default:
                            return string.Empty;

                    }
                //7
                case Operator.Contains:
                    return fieldType == TypeDataKendo.String ? string.Format(" {0}.ToLower().Contains(\"{1}\") ", fieldName, value) : string.Empty;
                //8
                case Operator.NotContains:
                    return fieldType == TypeDataKendo.String ? string.Format(" !({0}.ToLower().Contains(\"{1}\")) ", fieldName, value) : string.Empty;
                //9
                case Operator.StartsWith:
                    return fieldType == TypeDataKendo.String ? string.Format(" {0}.ToLower().StartsWith(\"{1}\") ", fieldName, value) : string.Empty;
                //10
                case Operator.EndsWith:
                    return fieldType == TypeDataKendo.String ? string.Format(" {0}.ToLower().EndsWith(\"{1}\") ", fieldName, value) : string.Empty;
            }
            return null;

        }

        private string GetFormatDateTime(DateTime dateTime)
        {
            if (dateTime.Month > 9)
            {
                if ((dateTime.Hour > 9 && dateTime.Hour < 13) || (dateTime.Hour > 21 && dateTime.Hour < 24))
                {
                    return dateTime.ToString("MMM d yyyy  h:mmtt");
                }
                return dateTime.ToString("MMM d yyyy h:mmtt");
            }

            if ((dateTime.Hour > 9 && dateTime.Hour < 13) || (dateTime.Hour > 21 && dateTime.Hour < 24))
            {
                return dateTime.ToString("MMM  d yyyy  h:mmtt");
            }
            return dateTime.ToString("MMM  d yyyy h:mmtt");
        }
        #endregion

        #region Build Query Sql
        #endregion

        #endregion

        #region BuildSortExpression commond
        protected override void BuildSortExpression(IQueryInfo queryInfo)
        {
            base.BuildSortExpression(queryInfo);
            queryInfo.Sort.ForEach(x =>
            {

                if (x.Field.Contains("entity.Jurisdiction"))
                {
                    x.Field = "(claimNumber != null && claimNumber.State != null) ? claimNumber.State.Name : string.Empty";
                }
                if (x.Field.Contains("entity.ClaimNumber"))
                {
                    x.Field = "(claimNumber != null) ? claimNumber.Name : string.Empty";
                }
                if (x.Field.Contains("entity.PatientState"))
                {
                    x.Field = "claimNumber != null && claimNumber.Claimant != null && claimNumber.Claimant.State != null ? claimNumber.Claimant.State.Name : string.Empty";
                }
                if (x.Field.Contains("entity.CreatedDate"))
                {
                    x.Field = "entity.CreatedOn";
                }

            });

            bool flagAssignTo = false, flagClaimant = false;

            var dir = "";
            if (queryInfo.Sort == null || queryInfo.Sort.Count <= 0) return;

            foreach (var sort in queryInfo.Sort.Where(sort => sort.Field == "entity.AssignTo"))
            {
                sort.Field = "assignTo != null ? assignTo.FirstName : string.Empty";
                dir = sort.Dir;
                flagAssignTo = true;
            }

            if (flagAssignTo)
            {
                queryInfo.Sort.Add(new Sort { Field = "assignTo != null ? assignTo.MiddleName : string.Empty", Dir = dir });
                queryInfo.Sort.Add(new Sort { Field = "assignTo != null ? assignTo.LastName : string.Empty", Dir = dir });
            }

            foreach (var sort in queryInfo.Sort.Where(sort => sort.Field == "entity.Claimant"))
            {
                sort.Field = "(claimNumber != null && claimNumber.Claimant != null) ? claimNumber.Claimant.FirstName : string.Empty";
                dir = sort.Dir;
                flagClaimant = true;
            }

            if (!flagClaimant) return;
            queryInfo.Sort.Add(new Sort { Field = "(claimNumber != null && claimNumber.Claimant != null) ? claimNumber.Claimant.MiddleName : string.Empty", Dir = dir });
            queryInfo.Sort.Add(new Sort { Field = "(claimNumber != null && claimNumber.Claimant != null) ? claimNumber.Claimant.LastName : string.Empty", Dir = dir });
        }
        #endregion BuildSortExpression commond

        #region Previous Next ReferralTask
        public int GetPreviousReferralTaskId(int id, int referralId)
        {
            var data = GetAll().Where(o => o.Id < id && o.ReferralId == referralId).OrderByDescending(o => o.Id).FirstOrDefault();
            return data != null ? data.Id : id;
        }

        public int GetNextReferralTaskId(int id, int referralId)
        {
            var data = GetAll().Where(o => o.Id > id && o.ReferralId == referralId).OrderBy(o => o.Id).FirstOrDefault();
            return data != null ? data.Id : id;
        }

        public void CompletedTask(int id)
        {
            var item = GetById(id);
            item.StatusId = 3;
            item.CompletedDate = DateTime.Now;
            item.CancelDate = null;
        }

        public void AssignToForReferralTask(List<int> listIdSelected, bool isSelectAll, int? assignToId,
            TypeWithUserQueryEnum typeWithUser)
        {
            if (assignToId.GetValueOrDefault() == 0)
            {
                return;
            }
            if (isSelectAll)
            {
                var conditionTypeWithUser = "";
                var currentUser = WorkCompDb.GetCurrentUser().Id;
                switch (typeWithUser)
                {
                    case TypeWithUserQueryEnum.Current:
                        conditionTypeWithUser = "AssignToId == " + currentUser;
                        break;
                    case TypeWithUserQueryEnum.Other:
                        conditionTypeWithUser = "AssignToId != " + currentUser;
                        break;
                    default:
                        conditionTypeWithUser = "1 == 1";
                        break;
                }
                var listReferral = GetAll().Where(conditionTypeWithUser).ToList();
                foreach (var referral in listReferral)
                {
                    referral.AssignToId = assignToId.GetValueOrDefault();
                }
            }
            else if (listIdSelected.Count > 0)
            {
                var listReferral = GetAll().Where(o => listIdSelected.Contains(o.Id)).ToList();
                foreach (var referral in listReferral)
                {
                    referral.AssignToId = assignToId.GetValueOrDefault();
                }
            }
        }

       

        #endregion

        #region Update Complate Task

        public bool UpdateComplateTask(ReferralTask referralTask)
        {
            referralTask.StatusId = 3;
            referralTask.CancelDate = null;
            Update(referralTask);
            Commit();
            return true;
        }
        #endregion
        #endregion Nghiep work



        public dynamic GetDataPrint(List<int> listIdSelected, ReferralQueryInfo queryInfo)
        {
            BuildSortExpression(queryInfo);

            var finalResult =
                BuildQueryToGetDataForGrid(queryInfo)
                    .AsQueryable();
            finalResult = finalResult.Where(o => listIdSelected.Contains(o.Id));
            queryInfo.TotalRecords = finalResult.Count();

            var data = finalResult.Skip(queryInfo.Skip)
                    .Take(queryInfo.Take)
                    .ToList();
            return new { Data = data, TotalRowCount = queryInfo.TotalRecords };
        }
    }
}