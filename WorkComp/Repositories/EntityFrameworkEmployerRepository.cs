﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using Framework.DomainModel.Entities;
using Framework.DomainModel.Entities.Common;
using Framework.DomainModel.Interfaces;
using Framework.DomainModel.ValueObject;
using System.Linq.Dynamic;
using Repositories.Interfaces;

namespace Repositories
{
    public class EntityFrameworkEmployerRepository : EntityFrameworkRepositoryBase<Employer>, IEmployerRepository
    {
        public EntityFrameworkEmployerRepository()
            
        {
            SearchColumns.Add("Name");
            SearchColumns.Add("FederalTaxId");
            SearchColumns.Add("Phone");
            SearchColumns.Add("Address1");
            SearchColumns.Add("Address2");
            SearchColumns.Add("State");
            SearchColumns.Add("City");
            SearchColumns.Add("Zip");

            DisplayColumnForCombobox = "Name";
        }

        public override IQueryable<ReadOnlyGridVo> BuildQueryToGetDataForGrid(IQueryInfo queryInfo)
        {
            var queryResult = (from entity in GetAll()
                               join s in WorkCompDb.Set<State>() on entity.StateId equals s.Id into states
                               from state in states.DefaultIfEmpty()
                               join c in WorkCompDb.Set<City>() on entity.CityId equals c.Id into cities
                               from city in cities.DefaultIfEmpty()
                               join z in WorkCompDb.Set<Zip>() on entity.ZipId equals z.Id into zips
                               from zip in zips.DefaultIfEmpty()
                               select new { entity, state, city, zip }).OrderBy(queryInfo.SortString)
                .Select(o => new EmployerGridVo
                {
                    Id = o.entity.Id,
                    Name = o.entity.Name,
                    Phone = o.entity.Phone,
                    FederalTaxId = o.entity.FederalTaxId,
                    Address1 = o.entity.Address,
                    Address2 = o.entity.Address1,
                    State = o.state.Name,
                    City = o.city.Name,
                    Zip = o.zip.Name
                });
            return queryResult;
        }

        protected override void BuildSortExpression(IQueryInfo queryInfo)
        {
            base.BuildSortExpression(queryInfo);

            queryInfo.Sort.ForEach(x =>
            {
                if (x.Field.Contains("entity.State"))
                {
                    x.Field = "(state != null) ? state.Name : string.Empty";
                }
                if (x.Field.Contains("entity.City"))
                {
                    x.Field = "(city != null) ? city.Name : string.Empty";
                }
                if (x.Field.Contains("entity.Zip"))
                {
                    x.Field = "(zip != null) ? zip.Name : string.Empty";

                }
                if (x.Field.Contains("entity.Address1"))
                {
                    x.Field = "entity.Address";
                }

                if (x.Field.Contains("entity.Address2"))
                {
                    x.Field = "entity.Address1";
                }

            });
        }

        public InfoWhenChangeEmployerInReferral GetInfoWhenChangeEmployerInReferral(int idEmployer)
        {
            var query = from entity in GetAll()
                        join s in WorkCompDb.States on entity.StateId equals s.Id into states
                        from state in states.DefaultIfEmpty()
                        join c in WorkCompDb.Cities on entity.CityId equals c.Id into cities
                        from city in cities.DefaultIfEmpty()
                        join z in WorkCompDb.Zips on entity.ZipId equals z.Id into zips
                        from zip in zips.DefaultIfEmpty()
                        where entity.Id == idEmployer
                        select new InfoWhenChangeEmployerInReferral
                        {
                            EmployerId = entity.Id,
                            EmployerAddress = entity.Address,
                            EmployerCity = city == null ? "" : city.Name,
                            EmployerState = state == null ? "" : state.Name,
                            EmployerZip = zip == null ? "" : zip.Name,
                            Phone = entity.Phone
                        };
            return query.FirstOrDefault();
        }
    }
}