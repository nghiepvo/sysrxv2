﻿using System.Linq.Dynamic;
using Framework.DomainModel.Entities;
using Repositories.Interfaces;
using System.Linq;

namespace Repositories
{
    public class EntityFrameworkReferralNoteRepository : EntityFrameworkRepositoryBase<ReferralNote>, IReferralNoteRepository
    {
        public EntityFrameworkReferralNoteRepository()
            : base()
        {
        }

        public System.Collections.Generic.IList<ReferralNote> GetReferralNoteWhenChangeReferral(int? referralId)
        {
            return WorkCompDb.ReferralNotes.Where(p => p.ReferralId == referralId).ToList();
        }
    }
}