﻿using System.Collections.Generic;
using System.Linq;
using System.Linq.Dynamic;
using Framework.DomainModel.Entities;
using Framework.DomainModel.Entities.Common;
using Framework.DomainModel.Interfaces;
using Framework.DomainModel.ValueObject;
using Repositories.Interfaces;

namespace Repositories
{
    public class EntityFrameworkHeadingRepository : EntityFrameworkRepositoryBase<Heading>, IHeadingRepository
    {
        public EntityFrameworkHeadingRepository()
        {
            DisplayColumnForCombobox = "Name";
        }

        public override IQueryable<ReadOnlyGridVo> BuildQueryToGetDataForGrid(IQueryInfo queryInfo)
        {
            var queryResult = GetAll().OrderBy(queryInfo.SortString).Select(s => new HeadingGridVo
            {
                Id = s.Id,
                Name = s.Name,
            });
            return queryResult;
        }

        protected override void BuildSortExpression(IQueryInfo queryInfo)
        {
            if (queryInfo.Sort == null || (queryInfo.Sort != null && queryInfo.Sort.Count == 0))
            {
                queryInfo.Sort = new List<Sort> { new Sort { Field = "Name", Dir = "asc" } };
            }
        }

        public List<LookupItemVo> GetListHeading()
        {
            return WorkCompDb.Headings.Select(o => new LookupItemVo
            {
                KeyId = o.Id,
                DisplayName = o.Name
            }).ToList();
        } 
    }
}