﻿using System.Linq;
using Framework.DomainModel.Entities;
using Framework.DomainModel.Interfaces;
using Framework.DomainModel.ValueObject;
using System.Linq.Dynamic;
using Repositories.Interfaces;
using Repositories.Utility;

namespace Repositories
{
    public class EntityFrameworkAssayCodeDescriptionRepository : EntityFrameworkRepositoryBase<AssayCodeDescription>, IAssayCodeDescriptionRepository
    {
        public EntityFrameworkAssayCodeDescriptionRepository()
            : base()
        {
            SearchColumns.Add("Description");
            DisplayColumnForCombobox = "Description";
        }

        public override IQueryable<ReadOnlyGridVo> BuildQueryToGetDataForGrid(IQueryInfo queryInfo)
        {
            
            var queryResult = (from entity in GetAll() select new {entity}).OrderBy(queryInfo.SortString).Select(s => new AssayCodeDescriptionGridVo()
            {
                Id = s.entity.Id,
                Description = s.entity.Description
            });
            return queryResult;
        }
    }
}