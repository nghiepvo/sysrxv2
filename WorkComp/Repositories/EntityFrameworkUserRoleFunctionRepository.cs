﻿using System.Collections.Generic;
using System.Data.Entity;
using System.Linq;
using Framework.DomainModel.Entities;
using Repositories.Interfaces;

namespace Repositories
{
    public class EntityFrameworkUserRoleFunctionRepository : EntityFrameworkRepositoryBase<UserRoleFunction>, IUserRoleFunctionRepository
    {
        public EntityFrameworkUserRoleFunctionRepository()
            : base()
        {
            
        }

        public List<UserRoleFunction> LoadUserSecurityRoleFunction(int userRoleId, int documentTypeId)
        {
            var result = (from urf in WorkCompDb.Set<UserRoleFunction>().AsQueryable().AsNoTracking()
                          join document in WorkCompDb.Set<DocumentType>().AsQueryable().AsNoTracking()
                              on urf.DocumentTypeId equals document.Id into temp
                          from docType in temp
                          where docType.Id == documentTypeId
                          where urf.UserRoleId == userRoleId
                          select urf);
            return result.ToList();
        }

        
    }
}