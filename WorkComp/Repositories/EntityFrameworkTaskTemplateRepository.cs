﻿using System.Collections.Generic;
using System.Data.Entity;
using System.Linq;
using Framework.DomainModel.Entities;
using Framework.DomainModel.Entities.Common;
using Framework.DomainModel.Interfaces;
using Framework.DomainModel.ValueObject;
using Repositories.Interfaces;

using System.Linq.Dynamic;
namespace Repositories
{
    public class EntityFrameworkTaskTemplateRepository : EntityFrameworkRepositoryBase<TaskTemplate>, ITaskTemplateRepository
    {
        public EntityFrameworkTaskTemplateRepository()
            : base()
        {
            SearchColumns.Add("Title");
            SearchColumns.Add("Description");
            DisplayColumnForCombobox = "Title";
        }
        public override IQueryable<ReadOnlyGridVo> BuildQueryToGetDataForGrid(IQueryInfo queryInfo)
        {
            var finalResult = (from entity in GetAll()
                               select new { entity }).OrderBy(queryInfo.SortString).Select(x => new TaskTemplateGridVo()
                               {
                                   Title = x.entity.Title,
                                   Id = x.entity.Id,
                                   Description = x.entity.Description,
                                   NumDueDate = x.entity.NumDueDate,
                                   NumDueHour = x.entity.NumDueHour,
                                   StatusId = x.entity.StatusId,
                                   TaskTypeId = x.entity.TaskTypeId,
                                   IsReadOnly=x.entity.IsSystem
                               });
            return finalResult;
        }

        protected override void BuildSortExpression(IQueryInfo queryInfo)
        {
            base.BuildSortExpression(queryInfo);
            queryInfo.Sort.ForEach(x =>
            {
                if (x.Field.Contains("TaskStatus"))
                {
                    x.Field = "(entity.TaskStatus != null) ? entity.TaskStatus.Name : string.Empty";
                }
                else if (x.Field.Contains("TaskType"))
                {
                    x.Field = "(entity.TaskType != null) ? entity.TaskType.Name : string.Empty";
                }
            });
        }

        protected override void BuildDefaultSortExpression(IQueryInfo queryInfo)
        {
            if (queryInfo.Sort == null || queryInfo.Sort.Count == 0)
            {
                queryInfo.Sort = new List<Sort> { new Sort { Field = "IsSystem", Dir = "desc" }, new Sort { Field = "Id", Dir = "desc" } };
            }
        }

        public override void Delete(TaskTemplate entity)
        {
            // Check entity is not system default
            if (entity != null && !entity.IsSystem.GetValueOrDefault())
            {
                base.Delete(entity);
            }
        }

        public override int DeleteById(int id)
        {
            // Get item
            var entity = GetById(id);
            if (entity != null && !entity.IsSystem.GetValueOrDefault())
            {
                return base.DeleteById(id);
            }
            return 0;
        }

        public List<DualListBoxItemVo> GetAllTaskTemplateForDualListBox()
        {
            return
                GetAll()
                    .AsNoTracking()
                    .OrderByDescending(o => o.IsSystem)
                    .ThenByDescending(o => o.Id)
                    .Select(o => new DualListBoxItemVo
                    {
                        Id = o.Id,
                        Name = o.Title
                    }).ToList();
        }

        public List<DualListBoxItemVo> GetTaskTemplateByTaskGroup(int taskGroupId)
        {
            return (from o in GetAll().AsNoTracking()
                join tg in WorkCompDb.Set<TaskGroupTaskTemplate>() on o.Id equals tg.TaskTemplateId into
                    taskGroupTaskTemplates
                from taskGroupTaskTemplate in taskGroupTaskTemplates
                where taskGroupTaskTemplate.TaskGroupId == taskGroupId
                select new DualListBoxItemVo
                {
                    Id = o.Id,
                    Name = o.Title
                }).ToList();

        }
    }
}