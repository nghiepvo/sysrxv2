﻿using System.Linq;
using System.Linq.Dynamic;
using Framework.DomainModel.Entities;
using Framework.DomainModel.Entities.Common;
using Repositories.Interfaces;
using Framework.DomainModel.Interfaces;
using Framework.DomainModel.ValueObject;

namespace Repositories
{
    public class EntityFrameworkBranchRepository : EntityFrameworkRepositoryBase<Branch>, IBranchRepository
    {
        public EntityFrameworkBranchRepository()
        {
            SearchColumns.Add("Name");
            SearchColumns.Add("ManagerName");
            SearchColumns.Add("Phone");
            SearchColumns.Add("Fax");
            SearchColumns.Add("Address1");
            SearchColumns.Add("Address2");
            SearchColumns.Add("Payer");
            SearchColumns.Add("City");
            SearchColumns.Add("State");
            SearchColumns.Add("Zip");
            SearchColumns.Add("String.Concat(String.Concat(Address1, \", \"), String.Concat(State, \", \"), String.Concat(City, \", \"), Zip)");
            SearchColumns.Add("String.Concat(String.Concat(Address1, \" - \", Address2, \", \"), String.Concat(State, \", \"), String.Concat(City, \", \"), Zip)");
            DisplayColumnForCombobox = "Name";
        }

        public override IQueryable<ReadOnlyGridVo> BuildQueryToGetDataForGrid(IQueryInfo queryInfo)
        {
            var queryResult = (from entity in GetAll()
                               join s in WorkCompDb.Set<State>() on entity.StateId equals s.Id into states
                               from state in states.DefaultIfEmpty()
                               join c in WorkCompDb.Set<City>() on entity.CityId equals c.Id into cities
                               from city in cities.DefaultIfEmpty()
                               join z in WorkCompDb.Set<Zip>() on entity.ZipId equals z.Id into zips
                               from zip in zips.DefaultIfEmpty()
                join payer in WorkCompDb.Set<Payer>() on entity.PayerId equals payer.Id
                               select new { entity, payer, state, city, zip })
                .OrderBy(queryInfo.SortString)
                .Select(o => new BranchGridVo
                {
                    Id = o.entity.Id,
                    Name = o.entity.Name,
                    ManagerName = o.entity.ManagerName,
                    Payer = o.payer.Name,
                    Fax = o.entity.Fax,
                    Phone = o.entity.Phone,
                    Address1 = o.entity.Address1,
                    Address2 = o.entity.Address2,
                    State = o.state.Name,
                    City = o.city.Name,
                    Zip = o.zip.Name
                });
            return queryResult;
        }

        protected override void BuildSortExpression(IQueryInfo queryInfo)
        {
            base.BuildSortExpression(queryInfo);
            queryInfo.Sort.ForEach(x =>
            {
                if (x.Field.Contains("Payer"))
                {
                    x.Field = "(entity.Payer != null) ? entity.Payer.Name : string.Empty";
                }
            });


            bool flagAddress = false;

            var dir = "";
            if (queryInfo.Sort == null || queryInfo.Sort.Count <= 0) return;

            foreach (var sort in queryInfo.Sort.Where(sort => sort.Field == "entity.FullAddress"))
            {
                sort.Field = "entity.Address1";
                dir = sort.Dir;
                flagAddress = true;
            }

            if (!flagAddress) return;
            queryInfo.Sort.Add(new Sort { Field = "entity.Address2", Dir = dir });
            queryInfo.Sort.Add(new Sort { Field = "(state != null) ? state.Name : string.Empty", Dir = dir });
            queryInfo.Sort.Add(new Sort { Field = "(city != null) ? city.Name : string.Empty", Dir = dir });
            queryInfo.Sort.Add(new Sort { Field = "(zip != null) ? zip.Name : string.Empty", Dir = dir });
        }
    }
}
