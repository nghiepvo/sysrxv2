﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Data.Common;
using System.Data.Entity.Infrastructure;
using System.Data.Linq;
using System.Data.SqlClient;
using System.Linq;
using Framework.DomainModel.Entities;
using Framework.DomainModel.ValueObject;
using Framework.QueryEngine;
using Framework.Utility;
using Repositories.Interfaces;

namespace Repositories
{
    public class EntityFrameworkAlertRepository : EntityFrameworkRepositoryBase<Alert>, IAlertRepository
    {

        private DataTable UdsGetListAlert(int idCurrentUser, bool isAdminRole, string searchId, DateTime? startDate, DateTime? endDate, int startIndex, int countItem, ref int totalRecords)
        {
            var cmd = WorkCompDb.Database.Connection.CreateCommand();
            cmd.CommandText = "[dbo].[udsGetListAlert]";
            cmd.CommandType = CommandType.StoredProcedure;
            cmd.Parameters.Add(new SqlParameter("@AssignToId", idCurrentUser));
            cmd.Parameters.Add(new SqlParameter("@IsAdmin", isAdminRole));
            cmd.Parameters.Add(new SqlParameter("@SearchLinkId", string.IsNullOrEmpty(searchId)? null: searchId));
            cmd.Parameters.Add(new SqlParameter("@StartDate", startDate));
            cmd.Parameters.Add(new SqlParameter("@EndDate", endDate));
            cmd.Parameters.Add(new SqlParameter("@StartIndex", startIndex));
            cmd.Parameters.Add(new SqlParameter("@CountItem", countItem));
            cmd.Parameters.Add(new SqlParameter("@RowCount", SqlDbType.Int)
            {
                Direction = ParameterDirection.ReturnValue
            });
            try
            {

                WorkCompDb.Database.Connection.Open();
                // Run the sproc  
                var reader = cmd.ExecuteReader();
                var table = new DataTable();
                table.Load(reader);
                var totalCount = 0;
                int.TryParse(cmd.Parameters["@RowCount"].Value.ToString(), out totalCount);
                totalRecords = totalCount;
                return table;
            }
            finally
            {
                WorkCompDb.Database.Connection.Close();
            }
        }

        public dynamic GetAlertPartial(int idCurrentUser, bool isAdminRole, int startIndex, int countItem)
        {
            var totalRecords = 0;
            var table = UdsGetListAlert(idCurrentUser, isAdminRole, null,null, null, startIndex, countItem, ref totalRecords);
            var result = table.AsEnumerable().Select(row => new AlertPanelGridVo
            {
                Id = int.Parse(row["RowNumber"].ToString()),
                Type = int.Parse(row["Type"].ToString()),
                LinkId = int.Parse(row["LinkId"].ToString()),
                Message = row["Message"].ToString(),
                IsRush = row["IsRush"].ToString() == "1",
            }).ToList();
            return new { Data = result, TotalRowCount = totalRecords };
           
        }

        public dynamic GetDataAlertForGrid(int idCurrentUser, bool isAdminRole, string searchId,DateTime? startDate, DateTime? endDate, int startIndex, int countItem)
        {
            var totalRecords = 0;
            var table = UdsGetListAlert(idCurrentUser, isAdminRole, searchId, startDate,endDate,startIndex, countItem, ref totalRecords);
            var result = table.AsEnumerable().Select(row => new AlertGridVo
            {
                Id = int.Parse(row["RowNumber"].ToString()),
                Type = string.IsNullOrEmpty(row["Type"].ToString()) ? 0 : int.Parse(row["Type"].ToString()),
                LinkId = string.IsNullOrEmpty(row["LinkId"].ToString()) ? 0 : int.Parse(row["LinkId"].ToString()),
                Title = row["Title"].ToString(),
                Message = row["Message"].ToString(),
                IsRush = row["IsRush"].ToString() == "1",
                CreatedOn = string.IsNullOrEmpty(row["CreatedOn"].ToString()) ? null : (DateTime?)DateTime.Parse(row["CreatedOn"].ToString())
            }).ToList();
            return new { Data = result, TotalRowCount = totalRecords };

        }
    }
}