﻿using System.Linq;
using Framework.DomainModel.Entities;
using Framework.DomainModel.Interfaces;
using Framework.DomainModel.ValueObject;
using System.Linq.Dynamic;
using Repositories.Interfaces;

namespace Repositories
{
    public class EntityFrameworkDrugRepository : EntityFrameworkRepositoryBase<Drug>, IDrugRepository
    {
        public EntityFrameworkDrugRepository()
        {
            SearchColumns.Add("Name");
            SearchColumns.Add("Class");
            SearchColumns.Add("Description");
            DisplayColumnForCombobox = "Name";
        }

        public override IQueryable<ReadOnlyGridVo> BuildQueryToGetDataForGrid(IQueryInfo queryInfo)
        {
            var queryResult =
                (from entity in GetAll() select new {entity}).OrderBy(queryInfo.SortString).Select(s => new DrugGridVo
                {
                    Id = s.entity.Id,
                    Name = s.entity.Name,
                    Class = s.entity.Class,
                    Description = s.entity.Description
                });
            return queryResult;
        }

        public string GetDrugs(int idReferral)
        {
            var queryMedication = (from o in WorkCompDb.ReferralMedicationHistories
                                   join d in WorkCompDb.Drugs on o.DrugId equals d.Id into drugs
                                   from drug in drugs.DefaultIfEmpty()
                                   where o.ReferralId == idReferral
                                   select new
                                   {
                                       DrugName = drug == null ? "" : drug.Name,
                                   }).ToList();
            string drugList = "";
            if (queryMedication.Count > 0)
            {
                foreach (var d in queryMedication.Distinct())
                {
                    if (d != null)
                    {
                        drugList += d.DrugName + ", ";
                    }
                }
            }
            if (drugList.Length > 0)
            {
                drugList = drugList.TrimEnd(',', ' ');
            }
            return drugList;
        }
    }
}