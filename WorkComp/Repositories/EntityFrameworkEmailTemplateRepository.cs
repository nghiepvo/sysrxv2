﻿using System;
using System.Data.Entity;
using System.Linq;
using System.Linq.Dynamic;
using Framework.DomainModel.Entities;
using Framework.DomainModel.Interfaces;
using Framework.DomainModel.ValueObject;
using Repositories.Interfaces;
using Framework.DomainModel.EmailTemplateValueObject;

namespace Repositories
{
    public class EntityFrameworkEmailTemplateRepository : EntityFrameworkRepositoryBase<EmailTemplate>, IEmailTemplateRepository
    {
        private const string ContentHeader = @"@using Framework.DomainModel.EmailTemplateValueObject
                            @model {0}
                            @{
                                ViewBag.Title = "";
                                Layout = null;
                            }";
       

        public EntityFrameworkEmailTemplateRepository()
        {
            SearchColumns.Add("Title");
            SearchColumns.Add("Subject");
        }

        public override IQueryable<ReadOnlyGridVo> BuildQueryToGetDataForGrid(IQueryInfo queryInfo)
        {
            var resultQuery = (from entity in GetAll() select new { entity })
                .OrderBy(queryInfo.SortString)
                .Select(o => new EmailTemplateGridVo()
                {
                    Id = o.entity.Id,
                    Title = o.entity.Title,
                    Subject = o.entity.Subject,
                    IsService = o.entity.IsService,
                    IsActive = o.entity.IsActived
                });
            return resultQuery;
        }

        public dynamic GetDataForGridReferralDetail()
        {
            var resultQuery = (from entity in GetAll().Where(o => o.IsService == false && o.IsActived == true) select new { entity })
               .OrderBy(o=>o.entity.Id)
               .Select(o => new EmailTemplateGridVo()
               {
                   Id = o.entity.Id,
                   Title = o.entity.Title,
                   Subject = o.entity.Subject,
                   IsActive = o.entity.IsActived
               });
            var data = resultQuery.ToList();
            return new { Data = data, TotalRowCount = data.Count };
        }

        public ReferralEmailTemplate GetOrderEmailTemplate(int orderId, int emailId)
        {
            return
                WorkCompDb.ReferralEmailTemplates.FirstOrDefault(
                    o => o.ReferralId == orderId && o.EmailTemplateId == emailId);
        }

        public DataRequestForAuthorizationEmailTemplate GetDataRequestForAuthorization(int referralId)
        {
            var tempQuery = (from referral in WorkCompDb.Referrals.Where(o => o.Id == referralId)
                             join cn in WorkCompDb.ClaimNumbers on referral.ClaimantNumberId equals cn.Id into claimnumbers
                             from claimnumber in claimnumbers.DefaultIfEmpty()
                             join aj in WorkCompDb.Adjusters on claimnumber.AdjusterId equals aj.Id into adjusters
                             from adjust in adjusters.DefaultIfEmpty()
                             join p in WorkCompDb.Payers on claimnumber.PayerId equals p.Id into payers
                             from payer in payers.DefaultIfEmpty()
                             join c in WorkCompDb.Claimants on claimnumber.ClaimantId equals c.Id into claimants
                             from claimant in claimants.DefaultIfEmpty()
                             join pn in WorkCompDb.PanelTypes on referral.PanelTypeId equals pn.Id into panelTypes
                             from panelType in panelTypes.DefaultIfEmpty()
                             join asg in WorkCompDb.Users on referral.AssignToId equals asg.Id into assignToes
                             from assignTo in assignToes.DefaultIfEmpty()
                             join rnpi in WorkCompDb.ReferralNpiNumbers.Where(rnpi => rnpi.ReferralId == referralId) on referral.Id
                                 equals rnpi.ReferralId into referralNpiNumbers
                             from referralNpiNumber in referralNpiNumbers.DefaultIfEmpty()
                             join rcls in WorkCompDb.ReferralCollectionSites.Where(rcls => rcls.ReferralId == referralId) on
                                 referral.Id equals rcls.ReferralId into referralCollectionSites
                             from referralCollectionSite in referralCollectionSites.DefaultIfEmpty()
                             select new DataRequestForAuthorizationEmailTemplate
                             {
                                 AdjusterEmail = adjust == null ? "" : adjust.Email,
                                 AdjusterName = adjust == null ? "" : adjust.FirstName + " " + adjust.LastName,
                                 PayerName = payer == null ? "" : payer.Name,
                                 PayerAddress = payer == null ? "" : payer.Address,
                                 PayerCity = payer == null ? "" : payer.City == null ? "" : payer.City.Name,
                                 PayerState = payer == null ? "" : payer.State == null ? "" : payer.State.Name,
                                 PayerZip = payer == null ? "" : payer.Zip == null ? "" : payer.Zip.Name,
                                 Claimant = claimant == null ? "" : claimant.FirstName + " " + claimant.LastName,
                                 PatientPhone = claimant == null ? "" : claimant.BestContactNumber,
                                 PatientAddr = claimant == null ? "" : claimant.Address1,
                                 PatientCity = claimant == null ? "" : claimant.City == null ? "" : claimant.City.Name,
                                 PatientState = claimant == null ? "" : claimant.State == null ? "" : claimant.State.Name,
                                 PatientZip = claimant == null ? "" : claimant.Zip == null ? "" : claimant.Zip.Name,
                                 ClaimNumber = claimnumber == null ? "" : claimnumber.Name,
                                 Doi = claimnumber == null ? (DateTime?)null : claimnumber.Doi,
                                 ClaimJurisdiction =
                                     claimnumber == null ? null : claimnumber.State == null ? null : claimnumber.State.Name,
                                 Dob = claimant == null ? (DateTime?)null : claimant.Birthday,
                                 PanelType = panelType == null ? "" : panelType.Name,
                                 AssignTo =
                                     assignTo == null ? "" : assignTo.FirstName + " " + assignTo.LastName + " " + assignTo.MiddleName,
                                 AssignEmail = assignTo == null ? "" : assignTo.Email,
                                 OrderTitle = referral.ProductType == null ? "" : referral.ProductType.Name,
                                 NextMdVisit = (referralNpiNumber == null) ? (DateTime?)null : referralNpiNumber.NextMdVisitDate,
                                 CollectionSiteOrPhysicianName = referral.IsCollectionSite == true
                                     ? (referralCollectionSite == null
                                         ? ""
                                         : (referralCollectionSite.CollectionSite == null
                                             ? ""
                                             : referralCollectionSite.CollectionSite.Name))
                                     : (referralNpiNumber == null
                                         ? ""
                                         : (referralNpiNumber.NpiNumber == null
                                             ? ""
                                             : referralNpiNumber.NpiNumber.FirstName + " " + referralNpiNumber.NpiNumber.MiddleName +
                                               " " + referralNpiNumber.NpiNumber.LastName))
                             });
            return tempQuery.FirstOrDefault();
        }

        public AllEmailForReferral GetListEmailInReferral(int referralId)
        {
            return (from referral in WorkCompDb.Referrals.Where(o => o.Id == referralId)
                    join rs in WorkCompDb.ReferralSources on referral.ReferralSourceId equals rs.Id into referralSources
                    from referralSource in referralSources.DefaultIfEmpty()
                    join cn in WorkCompDb.ClaimNumbers on referral.ClaimantNumberId equals cn.Id into claimnumbers
                    from claimnumber in claimnumbers.DefaultIfEmpty()
                    join aj in WorkCompDb.Adjusters on claimnumber.AdjusterId equals aj.Id into adjusters
                    from adjuster in adjusters.DefaultIfEmpty()
                    join c in WorkCompDb.Claimants on claimnumber.ClaimantId equals c.Id into claimants
                    from claimant in claimants.DefaultIfEmpty()
                    join cm in WorkCompDb.CaseManagers on referral.CaseManagerId equals cm.Id into caseManagers
                    from caseManager in caseManagers.DefaultIfEmpty()
                    join at in WorkCompDb.Attorneys on referral.AttorneyId equals at.Id into attorneys
                    from attorney in attorneys.DefaultIfEmpty()
                    join rnpi in WorkCompDb.ReferralNpiNumbers.Where(rnpi => rnpi.ReferralId == referralId) on referral.Id
                        equals rnpi.ReferralId into referralNpiNumbers
                    from referralNpiNumber in referralNpiNumbers.DefaultIfEmpty()
                    join rcls in WorkCompDb.ReferralCollectionSites.Where(rcls => rcls.ReferralId == referralId) on
                        referral.Id equals rcls.ReferralId into referralCollectionSites
                    from referralCollectionSite in referralCollectionSites.DefaultIfEmpty()
                    select new AllEmailForReferral
                    {
                        EmailReferral = referralSource == null ? "" : referralSource.Email,
                        NameReferral =
                            referralSource == null
                                ? ""
                                : referralSource.FirstName + " " + (referralSource.MiddleName ?? "") + " " +
                                  referralSource.LastName,
                        EmailAdjuster = adjuster == null ? "" : adjuster.Email,
                        NameAdjuster =
                            adjuster == null
                                ? ""
                                : adjuster.FirstName + " " + (adjuster.MiddleName ?? "") + adjuster.LastName,
                        EmailClaimant = claimant == null ? "" : claimant.Email,
                        NameClaimant =
                            claimant == null
                                ? ""
                                : claimant.FirstName + " " + (claimant.MiddleName ?? "") + " " + claimant.LastName,
                        EmailCaseManager = caseManager == null ? "" : caseManager.Email,
                        NameCaseManager = caseManager == null ? "" : caseManager.Name,
                        EmailAttorney = attorney == null ? "" : attorney.Email,
                        NameAttorney =
                            attorney == null
                                ? ""
                                : attorney.FirstName + " " + (attorney.MiddleName ?? "") + " " + attorney.LastName,
                        IsCollectionSite = referral.IsCollectionSite,
                        CollectionSiteOrPhysicianName = referral.IsCollectionSite == true
                            ? (referralCollectionSite == null
                                ? ""
                                : (referralCollectionSite.CollectionSite == null
                                    ? ""
                                    : referralCollectionSite.CollectionSite.Name))
                            : (referralNpiNumber == null
                                ? ""
                                : (referralNpiNumber.NpiNumber == null
                                    ? ""
                                    : referralNpiNumber.NpiNumber.FirstName + " " + referralNpiNumber.NpiNumber.MiddleName +
                                      " " + referralNpiNumber.NpiNumber.LastName)),
                        CollectionSiteOrPhysicianEmail = referral.IsCollectionSite == true
                            ? (referralCollectionSite == null
                                ? ""
                                : (referralCollectionSite.CollectionSite == null
                                    ? ""
                                    : referralCollectionSite.CollectionSite.Email))
                            : (referralNpiNumber == null
                                ? ""
                                : (referralNpiNumber.NpiNumber == null
                                    ? ""
                                    : referralNpiNumber.NpiNumber.Email))
                    }).FirstOrDefault();
        }

        public DataRequestForCollectionEmailTemplate GetDataRequestForCollection(int referralId)
        {
            var query = (from referral in WorkCompDb.Referrals.Where(o => o.Id == referralId)
                join cn in WorkCompDb.ClaimNumbers on referral.ClaimantNumberId equals cn.Id into claimnumbers
                from claimnumber in claimnumbers.DefaultIfEmpty()
                join c in WorkCompDb.Claimants on claimnumber.ClaimantId equals c.Id into claimants
                from claimant in claimants.DefaultIfEmpty()
                join p in WorkCompDb.Payers on claimnumber.PayerId equals p.Id into payers
                from payer in payers.DefaultIfEmpty()
                join pn in WorkCompDb.PanelTypes on referral.PanelTypeId equals pn.Id into panelTypes
                from panelType in panelTypes.DefaultIfEmpty()
                join asg in WorkCompDb.Users on referral.AssignToId equals asg.Id into assignToes
                from assignTo in assignToes.DefaultIfEmpty()
                join rnpi in WorkCompDb.ReferralNpiNumbers.Where(rnpi => rnpi.ReferralId == referralId) on referral.Id
                    equals rnpi.ReferralId into referralNpiNumbers
                from referralNpiNumber in referralNpiNumbers.DefaultIfEmpty()
                join rcls in WorkCompDb.ReferralCollectionSites.Where(rcls => rcls.ReferralId == referralId) on
                    referral.Id equals rcls.ReferralId into referralCollectionSites
                from referralCollectionSite in referralCollectionSites.DefaultIfEmpty()
                select new DataRequestForCollectionEmailTemplate
                {
                    Claimant = claimant == null ? "" : claimant.FirstName + " " + claimant.LastName,
                    PayerName = payer == null ? "" : payer.Name,
                    PatientPhone = claimant == null ? "" : claimant.BestContactNumber,
                    PatientEmail = claimant == null ? "" : claimant.Email,
                    PatientAddr = claimant == null ? "" : claimant.Address1,
                    PatientCity = claimant == null ? "" : claimant.City == null ? "" : claimant.City.Name,
                    PatientState = claimant == null ? "" : claimant.State == null ? "" : claimant.State.Name,
                    PatientZip = claimant == null ? "" : claimant.Zip == null ? "" : claimant.Zip.Name,
                    ClaimNumber = claimnumber == null ? "" : claimnumber.Name,
                    Doi = claimnumber == null ? (DateTime?) null : claimnumber.Doi,
                    ClaimJurisdiction =
                        claimnumber == null ? null : claimnumber.State == null ? null : claimnumber.State.Name,
                    Dob = claimant == null ? (DateTime?) null : claimant.Birthday,
                    PanelType = panelType == null ? "" : panelType.Name,
                    AssignTo =
                        assignTo == null ? "" : assignTo.FirstName + " " + assignTo.LastName + " " + assignTo.MiddleName,
                    AssignEmail = assignTo == null ? "" : assignTo.Email,
                    NextMdVisit = (referralNpiNumber == null) ? (DateTime?) null : referralNpiNumber.NextMdVisitDate,
                    CollectionSiteOrPhysicianPhone = referral.IsCollectionSite == true
                        ? (referralCollectionSite == null
                            ? ""
                            : (referralCollectionSite.CollectionSite == null
                                ? ""
                                : referralCollectionSite.CollectionSite.Phone))
                        : (referralNpiNumber == null
                            ? ""
                            : (referralNpiNumber.NpiNumber == null
                                ? ""
                                : referralNpiNumber.NpiNumber.Phone)),
                    CollectionSiteOrPhysicianFax = referral.IsCollectionSite == true
                        ? (referralCollectionSite == null
                            ? ""
                            : (referralCollectionSite.CollectionSite == null
                                ? ""
                                : referralCollectionSite.CollectionSite.Fax))
                        : (referralNpiNumber == null
                            ? ""
                            : (referralNpiNumber.NpiNumber == null
                                ? ""
                                : referralNpiNumber.NpiNumber.Fax)),
                    CollectionSiteOrPhysicianName = referral.IsCollectionSite == true
                        ? (referralCollectionSite == null
                            ? ""
                            : (referralCollectionSite.CollectionSite == null
                                ? ""
                                : referralCollectionSite.CollectionSite.Name))
                        : (referralNpiNumber == null
                            ? ""
                            : (referralNpiNumber.NpiNumber == null
                                ? ""
                                : referralNpiNumber.NpiNumber.FirstName + " " + referralNpiNumber.NpiNumber.MiddleName +
                                  " " + referralNpiNumber.NpiNumber.LastName)),
                    CollectionSiteOrPhysicianAddress = referral.IsCollectionSite == true
                        ? (referralCollectionSite == null
                            ? ""
                            : (referralCollectionSite.CollectionSite == null
                                ? ""
                                : referralCollectionSite.CollectionSite.Address))
                        : (referralNpiNumber == null
                            ? ""
                            : (referralNpiNumber.NpiNumber == null
                                ? ""
                                : referralNpiNumber.NpiNumber.Address)),
                    CollectionSiteOrPhysicianCity = referral.IsCollectionSite == true
                        ? (referralCollectionSite == null
                            ? ""
                            : (referralCollectionSite.CollectionSite == null
                                ? ""
                                : (referralCollectionSite.CollectionSite.City == null
                                    ? ""
                                    : referralCollectionSite.CollectionSite.City.Name)))
                        : (referralNpiNumber == null
                            ? ""
                            : (referralNpiNumber.NpiNumber == null
                                ? ""
                                : (referralNpiNumber.NpiNumber.City == null
                                    ? ""
                                    : referralNpiNumber.NpiNumber.City.Name))),
                    CollectionSiteOrPhysicianState = referral.IsCollectionSite == true
                        ? (referralCollectionSite == null
                            ? ""
                            : (referralCollectionSite.CollectionSite == null
                                ? ""
                                : (referralCollectionSite.CollectionSite.State == null
                                    ? ""
                                    : referralCollectionSite.CollectionSite.State.Name)))
                        : (referralNpiNumber == null
                            ? ""
                            : (referralNpiNumber.NpiNumber == null
                                ? ""
                                : (referralNpiNumber.NpiNumber.State == null
                                    ? ""
                                    : referralNpiNumber.NpiNumber.State.Name))),
                    CollectionSiteOrPhysicianZip = referral.IsCollectionSite == true
                        ? (referralCollectionSite == null
                            ? ""
                            : (referralCollectionSite.CollectionSite == null
                                ? ""
                                : (referralCollectionSite.CollectionSite.Zip == null
                                    ? ""
                                    : referralCollectionSite.CollectionSite.Zip.Name)))
                        : (referralNpiNumber == null
                            ? ""
                            : (referralNpiNumber.NpiNumber == null
                                ? ""
                                : (referralNpiNumber.NpiNumber.Zip == null
                                    ? ""
                                    : referralNpiNumber.NpiNumber.Zip.Name))),
                    HasCollectionSite = referral.IsCollectionSite,
                    PhysicianName = referral.IsCollectionSite == true
                        ? ""
                        : (referralNpiNumber == null
                            ? ""
                            : (referralNpiNumber.NpiNumber == null
                                ? ""
                                : (referralNpiNumber.NpiNumber.FirstName + " " +
                                   (referralNpiNumber.NpiNumber.MiddleName ?? "") + " " +
                                   referralNpiNumber.NpiNumber.LastName))),
                    PhysicianAddress = referral.IsCollectionSite == true
                        ? ""
                        : (referralNpiNumber == null
                            ? ""
                            : (referralNpiNumber.NpiNumber == null
                                ? ""
                                : referralNpiNumber.NpiNumber.Address)),
                    PhysicianPhone = referral.IsCollectionSite == true
                        ? ""
                        : (referralNpiNumber == null
                            ? ""
                            : (referralNpiNumber.NpiNumber == null
                                ? ""
                                : referralNpiNumber.NpiNumber.Phone)),
                    PhysicianFax = referral.IsCollectionSite == true
                        ? ""
                        : (referralNpiNumber == null
                            ? ""
                            : (referralNpiNumber.NpiNumber == null
                                ? ""
                                : referralNpiNumber.NpiNumber.Fax)),
                    ProviderCredentialText = referral.IsCollectionSite == true
                        ? ""
                        : (referralNpiNumber == null
                            ? ""
                            : (referralNpiNumber.NpiNumber == null
                                ? ""
                                : referralNpiNumber.NpiNumber.ProviderCredentialText)),
                    PhysicianZip = referral.IsCollectionSite == true
                        ? ""
                        : (referralNpiNumber == null
                            ? ""
                            : (referralNpiNumber.NpiNumber == null
                                ? ""
                                : (referralNpiNumber.NpiNumber.Zip == null
                                    ? ""
                                    : referralNpiNumber.NpiNumber.Zip.Name))),
                    PhysicianState = referral.IsCollectionSite == true
                        ? ""
                        : (referralNpiNumber == null
                            ? ""
                            : (referralNpiNumber.NpiNumber == null
                                ? ""
                                : (referralNpiNumber.NpiNumber.State == null
                                    ? ""
                                    : referralNpiNumber.NpiNumber.State.Name))),
                    PhysicianCity = referral.IsCollectionSite == true
                        ? ""
                        : (referralNpiNumber == null
                            ? ""
                            : (referralNpiNumber.NpiNumber == null
                                ? ""
                                : (referralNpiNumber.NpiNumber.City == null
                                    ? ""
                                    : referralNpiNumber.NpiNumber.City.Name))),
                }).FirstOrDefault();
            if (query != null)
            {
                var sampleTestingTypeQuery =
                    WorkCompDb.ReferralSampleTestingTypes.Where(o => o.ReferralId == referralId).Include(o => o.SampleTestingType).ToList();
                var strPanelTypeTest = "";
                foreach (var item in sampleTestingTypeQuery)
                {
                    strPanelTypeTest += (item.SampleTestingType == null ? "" : (item.SampleTestingType.Name ?? "")) + ", ";
                }
                query.PanelTypeSimpleTest = strPanelTypeTest + query.PanelType;
            }
            return query;

        }

        public DataMdNotificationLetterTemplate GetDataMdNotificationLetterTemplate(int referralId)
        {
            var query = (from referral in WorkCompDb.Referrals.Where(o => o.Id == referralId)
                         join cn in WorkCompDb.ClaimNumbers on referral.ClaimantNumberId equals cn.Id into claimnumbers
                         from claimnumber in claimnumbers.DefaultIfEmpty()
                         join c in WorkCompDb.Claimants on claimnumber.ClaimantId equals c.Id into claimants
                         from claimant in claimants.DefaultIfEmpty()
                         join p in WorkCompDb.Payers on claimnumber.PayerId equals p.Id into payers
                         from payer in payers.DefaultIfEmpty()
                         join pn in WorkCompDb.PanelTypes on referral.PanelTypeId equals pn.Id into panelTypes
                         from panelType in panelTypes.DefaultIfEmpty()
                         join asg in WorkCompDb.Users on referral.AssignToId equals asg.Id into assignToes
                         from assignTo in assignToes.DefaultIfEmpty()
                         join rnpi in WorkCompDb.ReferralNpiNumbers.Where(rnpi => rnpi.ReferralId == referralId) on referral.Id
                             equals rnpi.ReferralId into referralNpiNumbers
                         from referralNpiNumber in referralNpiNumbers.DefaultIfEmpty()
                         join rcls in WorkCompDb.ReferralCollectionSites.Where(rcls => rcls.ReferralId == referralId) on
                             referral.Id equals rcls.ReferralId into referralCollectionSites
                         from referralCollectionSite in referralCollectionSites.DefaultIfEmpty()
                         select new DataMdNotificationLetterTemplate
                         {
                             Claimant = claimant == null ? "" : claimant.FirstName + " " + claimant.LastName,
                             PayerName = payer == null ? "" : payer.Name,
                             PatientPhone = claimant == null ? "" : claimant.BestContactNumber,
                             PatientEmail = claimant == null ? "" : claimant.Email,
                             PatientAddr = claimant == null ? "" : claimant.Address1,
                             PatientCity = claimant == null ? "" : claimant.City == null ? "" : claimant.City.Name,
                             PatientState = claimant == null ? "" : claimant.State == null ? "" : claimant.State.Name,
                             PatientZip = claimant == null ? "" : claimant.Zip == null ? "" : claimant.Zip.Name,
                             ClaimNumber = claimnumber == null ? "" : claimnumber.Name,
                             Doi = claimnumber == null ? (DateTime?)null : claimnumber.Doi,
                             ClaimJurisdiction =
                                 claimnumber == null ? null : claimnumber.State == null ? null : claimnumber.State.Name,
                             Dob = claimant == null ? (DateTime?)null : claimant.Birthday,
                             PanelType = panelType == null ? "" : panelType.Name,
                             AssignTo =
                                 assignTo == null ? "" : assignTo.FirstName + " " + assignTo.LastName + " " + assignTo.MiddleName,
                             
                             PhysicianName = referral.IsCollectionSite == true
                                 ? ""
                                 : (referralNpiNumber == null
                                     ? ""
                                     : (referralNpiNumber.NpiNumber == null
                                         ? ""
                                         : (referralNpiNumber.NpiNumber.FirstName + " " +
                                            (referralNpiNumber.NpiNumber.MiddleName ?? "") + " " +
                                            referralNpiNumber.NpiNumber.LastName))),
                             PhysicianAddress = referral.IsCollectionSite == true
                                 ? ""
                                 : (referralNpiNumber == null
                                     ? ""
                                     : (referralNpiNumber.NpiNumber == null
                                         ? ""
                                         : referralNpiNumber.NpiNumber.Address)),
                            
                             PhysicianFax = referral.IsCollectionSite == true
                                 ? ""
                                 : (referralNpiNumber == null
                                     ? ""
                                     : (referralNpiNumber.NpiNumber == null
                                         ? ""
                                         : referralNpiNumber.NpiNumber.Fax)),
                             PhysicianZip = referral.IsCollectionSite == true
                                 ? ""
                                 : (referralNpiNumber == null
                                     ? ""
                                     : (referralNpiNumber.NpiNumber == null
                                         ? ""
                                         : (referralNpiNumber.NpiNumber.Zip == null
                                             ? ""
                                             : referralNpiNumber.NpiNumber.Zip.Name))),
                             PhysicianState = referral.IsCollectionSite == true
                                 ? ""
                                 : (referralNpiNumber == null
                                     ? ""
                                     : (referralNpiNumber.NpiNumber == null
                                         ? ""
                                         : (referralNpiNumber.NpiNumber.State == null
                                             ? ""
                                             : referralNpiNumber.NpiNumber.State.Name))),
                             PhysicianCity = referral.IsCollectionSite == true
                                 ? ""
                                 : (referralNpiNumber == null
                                     ? ""
                                     : (referralNpiNumber.NpiNumber == null
                                         ? ""
                                         : (referralNpiNumber.NpiNumber.City == null
                                             ? ""
                                             : referralNpiNumber.NpiNumber.City.Name))),
                         }).FirstOrDefault();
            if (query != null)
            {
                var sampleTestingTypeQuery =
                    WorkCompDb.ReferralSampleTestingTypes.Where(o => o.ReferralId == referralId).Include(o => o.SampleTestingType).ToList();
                var strPanelTypeTest = sampleTestingTypeQuery.Aggregate("", (current, item) => current + ((item.SampleTestingType == null ? "" : (item.SampleTestingType.Name ?? "")) + ", "));
                query.SampleTestingType = strPanelTypeTest;

                var listReasonReferral =
                    WorkCompDb.Set<ReferralReasonReferral>()
                        .Where(o => o.ReferralId == referralId)
                        .Include(o => o.ReasonReferral)
                        .ToList();
                if (listReasonReferral.Count > 0)
                {
                    foreach (var reasonReferral in listReasonReferral)
                    {
                        if (reasonReferral.ReasonReferral!= null)
                        {
                            query.ReasonReferrals.Add(reasonReferral.ReasonReferral.Name);
                        }
                        
                    }
                }
            }
            return query;
        }

        public DataCustomCommunicationTemplate GetDataCustomCommunicationTemplate(int referralId)
        {
            var query = (from referral in WorkCompDb.Referrals.Where(o => o.Id == referralId)
                         join cn in WorkCompDb.ClaimNumbers on referral.ClaimantNumberId equals cn.Id into claimnumbers
                         from claimnumber in claimnumbers.DefaultIfEmpty()
                         join c in WorkCompDb.Claimants on claimnumber.ClaimantId equals c.Id into claimants
                         from claimant in claimants.DefaultIfEmpty()
                         join p in WorkCompDb.Payers on claimnumber.PayerId equals p.Id into payers
                         from payer in payers.DefaultIfEmpty()
                         join pn in WorkCompDb.PanelTypes on referral.PanelTypeId equals pn.Id into panelTypes
                         from panelType in panelTypes.DefaultIfEmpty()
                         join asg in WorkCompDb.Users on referral.AssignToId equals asg.Id into assignToes
                         from assignTo in assignToes.DefaultIfEmpty()
                         join rnpi in WorkCompDb.ReferralNpiNumbers.Where(rnpi => rnpi.ReferralId == referralId) on referral.Id
                             equals rnpi.ReferralId into referralNpiNumbers
                         from referralNpiNumber in referralNpiNumbers.DefaultIfEmpty()
                         join rcls in WorkCompDb.ReferralCollectionSites.Where(rcls => rcls.ReferralId == referralId) on
                             referral.Id equals rcls.ReferralId into referralCollectionSites
                         from referralCollectionSite in referralCollectionSites.DefaultIfEmpty()
                         select new DataCustomCommunicationTemplate
                         {
                             Claimant = claimant == null ? "" : claimant.FirstName + " " + claimant.LastName,
                             PatientPhone = claimant == null ? "" : claimant.BestContactNumber,
                             PatientEmail = claimant == null ? "" : claimant.Email,
                             PatientAddr = claimant == null ? "" : claimant.Address1,
                             PatientCity = claimant == null ? "" : claimant.City == null ? "" : claimant.City.Name,
                             PatientState = claimant == null ? "" : claimant.State == null ? "" : claimant.State.Name,
                             PatientZip = claimant == null ? "" : claimant.Zip == null ? "" : claimant.Zip.Name,
                             ClaimNumber = claimnumber == null ? "" : claimnumber.Name,
                             Doi = claimnumber == null ? (DateTime?)null : claimnumber.Doi,
                             ClaimJurisdiction =
                                 claimnumber == null ? null : claimnumber.State == null ? null : claimnumber.State.Name,
                             Dob = claimant == null ? (DateTime?)null : claimant.Birthday,
                             PanelType = panelType == null ? "" : panelType.Name,
                             AssignTo =
                                 assignTo == null ? "" : assignTo.FirstName + " " + assignTo.LastName + " " + assignTo.MiddleName,

                             PhysicianName = referral.IsCollectionSite == true
                                 ? ""
                                 : (referralNpiNumber == null
                                     ? ""
                                     : (referralNpiNumber.NpiNumber == null
                                         ? ""
                                         : (referralNpiNumber.NpiNumber.FirstName + " " +
                                            (referralNpiNumber.NpiNumber.MiddleName ?? "") + " " +
                                            referralNpiNumber.NpiNumber.LastName))),
                             PhysicianAddress = referral.IsCollectionSite == true
                                 ? ""
                                 : (referralNpiNumber == null
                                     ? ""
                                     : (referralNpiNumber.NpiNumber == null
                                         ? ""
                                         : referralNpiNumber.NpiNumber.Address)),

                             PhysicianPhone = referral.IsCollectionSite == true
                                 ? ""
                                 : (referralNpiNumber == null
                                     ? ""
                                     : (referralNpiNumber.NpiNumber == null
                                         ? ""
                                         : referralNpiNumber.NpiNumber.Phone)),
                         }).FirstOrDefault();

            return query;
        }

        public CollectionServiceRequestEmail GetDataRequestForCollectionServiceRequest(int referralId)
        {
            var objResult = (from referral in WorkCompDb.Referrals.Where(o=>o.Id==referralId)
                             join cn in WorkCompDb.ClaimNumbers on referral.ClaimantNumberId equals cn.Id into claimnumbers
                             from claimnumber in claimnumbers.DefaultIfEmpty()
                             join p in WorkCompDb.Payers on claimnumber.PayerId equals p.Id into payers
                             from payer in payers.DefaultIfEmpty()
                             join c in WorkCompDb.Claimants on claimnumber.ClaimantId equals c.Id into claimants
                             from claimant in claimants.DefaultIfEmpty()
                             join asg in WorkCompDb.Users on referral.AssignToId equals asg.Id into assignToes
                             from assignTo in assignToes.DefaultIfEmpty()
                             join rnpi in WorkCompDb.ReferralNpiNumbers.Where(rnpi => rnpi.ReferralId == referralId) on referral.Id
                       equals rnpi.ReferralId into referralNpiNumbers
                             from referralNpiNumber in referralNpiNumbers.DefaultIfEmpty()
                             join rcls in WorkCompDb.ReferralCollectionSites.Where(rcls => rcls.ReferralId == referralId) on
                                 referral.Id equals rcls.ReferralId into referralCollectionSites
                             from referralCollectionSite in referralCollectionSites.DefaultIfEmpty()
                             select new CollectionServiceRequestEmail
                             {
                                 ClaimantName = claimant == null ? "" : claimant.FirstName + " " + claimant.LastName,
                                 CarrierName = payer == null ? "" : payer.Name,
                                 CarrierAddr = payer == null ? "" : payer.Address,
                                 ClaimNo = claimnumber == null ? "" : claimnumber.Name,
                                 DateOfInjury = claimnumber == null ? (DateTime?)null : claimnumber.Doi,
                                 ReferralTitle = referral.ProductType == null ? "" : referral.ProductType.Name,
                                 AssignTo = assignTo == null ? "" : assignTo.FirstName + " " + assignTo.LastName + " " + assignTo.MiddleName,
                                 AssignEmail = assignTo == null ? "" : assignTo.Email,
                                 ControlNo = referral.ControlNumber,
                                 CollectionSiteOrPhysicianPhone = referral.IsCollectionSite == true
                                     ? (referralCollectionSite == null
                                         ? ""
                                         : (referralCollectionSite.CollectionSite == null
                                             ? ""
                                             : referralCollectionSite.CollectionSite.Phone))
                                     : (referralNpiNumber == null
                                         ? ""
                                         : (referralNpiNumber.NpiNumber == null
                                             ? ""
                                             : referralNpiNumber.NpiNumber.Phone)),
                                 CollectionSiteOrPhysicianFax = referral.IsCollectionSite == true
                                     ? (referralCollectionSite == null
                                         ? ""
                                         : (referralCollectionSite.CollectionSite == null
                                             ? ""
                                             : referralCollectionSite.CollectionSite.Fax))
                                     : (referralNpiNumber == null
                                         ? ""
                                         : (referralNpiNumber.NpiNumber == null
                                             ? ""
                                             : referralNpiNumber.NpiNumber.Fax)),
                                 CollectionSiteOrPhysicianName = referral.IsCollectionSite == true
                                     ? (referralCollectionSite == null
                                         ? ""
                                         : (referralCollectionSite.CollectionSite == null
                                             ? ""
                                             : referralCollectionSite.CollectionSite.Name))
                                     : (referralNpiNumber == null
                                         ? ""
                                         : (referralNpiNumber.NpiNumber == null
                                             ? ""
                                             : referralNpiNumber.NpiNumber.FirstName + " " + referralNpiNumber.NpiNumber.MiddleName +
                                               " " + referralNpiNumber.NpiNumber.LastName)),
                                 CollectionSiteOrPhysicianAddress = referral.IsCollectionSite == true
                                     ? (referralCollectionSite == null
                                         ? ""
                                         : (referralCollectionSite.CollectionSite == null
                                             ? ""
                                             : referralCollectionSite.CollectionSite.Address))
                                     : (referralNpiNumber == null
                                         ? ""
                                         : (referralNpiNumber.NpiNumber == null
                                             ? ""
                                             : referralNpiNumber.NpiNumber.Address)),
                                 CollectionSiteOrPhysicianCity = referral.IsCollectionSite == true
                                     ? (referralCollectionSite == null
                                         ? ""
                                         : (referralCollectionSite.CollectionSite == null
                                             ? ""
                                             : (referralCollectionSite.CollectionSite.City == null
                                                 ? ""
                                                 : referralCollectionSite.CollectionSite.City.Name)))
                                     : (referralNpiNumber == null
                                         ? ""
                                         : (referralNpiNumber.NpiNumber == null
                                             ? ""
                                             : (referralNpiNumber.NpiNumber.City == null
                                                 ? ""
                                                 : referralNpiNumber.NpiNumber.City.Name))),
                                 CollectionSiteOrPhysicianState = referral.IsCollectionSite == true
                                     ? (referralCollectionSite == null
                                         ? ""
                                         : (referralCollectionSite.CollectionSite == null
                                             ? ""
                                             : (referralCollectionSite.CollectionSite.State == null
                                                 ? ""
                                                 : referralCollectionSite.CollectionSite.State.Name)))
                                     : (referralNpiNumber == null
                                         ? ""
                                         : (referralNpiNumber.NpiNumber == null
                                             ? ""
                                             : (referralNpiNumber.NpiNumber.State == null
                                                 ? ""
                                                 : referralNpiNumber.NpiNumber.State.Name))),
                                 CollectionSiteOrPhysicianZip = referral.IsCollectionSite == true
                                     ? (referralCollectionSite == null
                                         ? ""
                                         : (referralCollectionSite.CollectionSite == null
                                             ? ""
                                             : (referralCollectionSite.CollectionSite.Zip == null
                                                 ? ""
                                                 : referralCollectionSite.CollectionSite.Zip.Name)))
                                     : (referralNpiNumber == null
                                         ? ""
                                         : (referralNpiNumber.NpiNumber == null
                                             ? ""
                                             : (referralNpiNumber.NpiNumber.Zip == null
                                                 ? ""
                                                 : referralNpiNumber.NpiNumber.Zip.Name))),
                                 HasCollectionSite = referral.IsCollectionSite,
                             }).FirstOrDefault();

            if (objResult != null)
            {
                objResult.ReferralNo = referralId.ToString();

                var sampleTestingTypeQuery =
                    WorkCompDb.ReferralSampleTestingTypes.Where(o => o.ReferralId == referralId).Include(o => o.SampleTestingType).ToList();
                var strPanelTypeTest = "";
                foreach (var item in sampleTestingTypeQuery)
                {
                    strPanelTypeTest += (item.SampleTestingType == null ? "" : (item.SampleTestingType.Name ?? "")) + ", ";
                }
                objResult.RequestedProduct = strPanelTypeTest != "" ? strPanelTypeTest.Substring(0, strPanelTypeTest.Length - 1) : "";
                // Get data for MedicationHist
                var queryMedication = (from o in WorkCompDb.ReferralMedicationHistories
                                       join d in WorkCompDb.Drugs on o.DrugId equals d.Id into drugs
                                       from drug in drugs.DefaultIfEmpty()
                                       select new MedicationHistoryEmail
                                       {
                                           Class = drug == null ? "" : drug.Class,
                                           DaySupply = o.DaysSupply,
                                           Dosage = o.Dosage,
                                           DrugName = drug == null ? "" : drug.Name,
                                           MedicationHist = o.ProvidedBy == null ? "" : o.ProvidedBy.Name,
                                           StartDay = o.FillDate
                                       }).ToList();

                objResult.MedicationHistoryEmails = queryMedication;
            }
            else
            {
                return null;
            }
            return objResult;
        }

        #region EmailTemplateValueObject

        public string ContentHtmlRequestForAuthorization(int referralId, EmailTemplateType emailTemplateType)
        {

            var obj = GetEmailTemplateVoBase(referralId);
            
            return null;
        }

        private EmailTemplateVoBase GetEmailTemplateVoBase(int referralId)
        {
            var query = (from entity in WorkCompDb.Set<Referral>().Where(o => o.Id == referralId)
                         join pt in WorkCompDb.Set<PanelType>() on entity.PanelTypeId equals pt.Id into intoPanelType
                         from panelType in intoPanelType.DefaultIfEmpty()
                         join cn in WorkCompDb.Set<ClaimNumber>() on entity.ClaimantNumberId equals cn.Id into intoClaimNumber
                         from claimNumber in intoClaimNumber.DefaultIfEmpty()
                         join p in WorkCompDb.Set<Payer>() on claimNumber.PayerId equals p.Id into intoPayer
                         from payer in intoPayer.DefaultIfEmpty()
                         join c in WorkCompDb.Set<Claimant>() on claimNumber.ClaimantId equals c.Id into intoClaimant
                         from claimant in intoClaimant.DefaultIfEmpty()
                         join a in WorkCompDb.Set<Adjuster>() on claimNumber.AdjusterId equals a.Id into intoAdjuster
                         from adjuster in intoAdjuster.DefaultIfEmpty()
                         join u in WorkCompDb.Set<User>() on entity.AssignToId equals u.Id into intoUser
                         from user in intoUser.DefaultIfEmpty()

                         select new { entity, claimNumber, claimant, adjuster, user, payer,  panelType}).Select(o => new EmailTemplateVoBase()
                         {
                             ReferralId = o.entity.Id,
                             AdjusterName = o.adjuster != null ? o.adjuster.FirstName + (string.IsNullOrEmpty(o.adjuster.MiddleName) ? "" : " " + o.adjuster.MiddleName) + " " + o.adjuster.LastName : "",
                             AssignTo = o.user != null ? o.user.FirstName + (string.IsNullOrEmpty(o.user.MiddleName) ? "" : " " + o.user.MiddleName) + " " + o.user.LastName : "",
                             PayerName = o.payer != null ? o.payer.Name : "",
                             PayerAddress = o.payer != null ? o.payer.Address : "",
                             PayerStateCityZip = o.payer != null ? ((o.payer.State != null ? o.payer.State.Name + ", " : "") + (o.payer.City != null ? o.payer.City.Name + ", " : "") + (o.payer.Zip != null ? o.payer.Zip.Name + ", " : "")) : "",
                             ClaimantName = o.claimant != null ? o.claimant.FirstName + (string.IsNullOrEmpty(o.claimant.MiddleName) ? "" : " " + o.claimant.MiddleName) + " " + o.claimant.LastName : "",
                             ClaimantAddress = o.claimant != null ? o.claimant.Address1 + (string.IsNullOrEmpty(o.claimant.Address2) ? "" : ", " + o.claimant.Address2) : "",
                             ClaimantState = o.claimant != null ? o.claimant.State != null ? o.claimant.State.Name : "" : "",
                             ClaimantCity = o.claimant != null ? o.claimant != null ? o.claimant.City.Name : "" : "",
                             ClaimantZip = o.claimant != null ? o.claimant != null ? o.claimant.Zip.Name : "" : "",
                             ClaimNumber = o.claimNumber != null ? o.claimNumber.Name : "",
                             ClaimNumberDoiDateTime = o.claimNumber != null ? (DateTime?)o.claimNumber.Doi : null,
                             ClaimantPhone = o.claimant != null ? o.claimant.BestContactNumber : "",
                             ClaimantDobDateTime = o.claimant != null ? (DateTime?)o.claimant.Birthday : null,
                             Jurisdiction = o.claimNumber != null ? o.claimNumber.State != null ? o.claimNumber.State.AbbreviationName : "" : "",
                             PanelType = o.panelType!= null? o.panelType.Name: "",
                         });
            var querySampleTestingType = (from entity in
                WorkCompDb.Set<ReferralSampleTestingType>().Where(o => o.ReferralId == referralId)
                join st in WorkCompDb.Set<SampleTestingType>() on entity.SampleTestingTypeId equals st.Id into
                    intoSampleTestingType
                from sampleTestingType in intoSampleTestingType.DefaultIfEmpty()
                select new {entity, sampleTestingType})
                .Select(o => new {o.entity.SampleTestingTypeId, SampleTestingTypeName = o.sampleTestingType.Name});
            var listSampleTestingType = querySampleTestingType.ToList();
            var sampleTestingTypeStr = "";
            if (listSampleTestingType.Count > 0)
            {
                sampleTestingTypeStr = listSampleTestingType.Aggregate(sampleTestingTypeStr, (current, item) => current + (item.SampleTestingTypeName + ", "));
                sampleTestingTypeStr = sampleTestingTypeStr.TrimEnd(new[] {',', ' '});
            }
            var data = query.FirstOrDefault();
            if (data != null)
            {
                data.SampleTestingType = sampleTestingTypeStr;
            }
            return data;
        }
        #endregion
    }
}
