﻿using System.Data.Entity;
using System.Linq;
using Framework.DomainModel.Entities;
using Framework.DomainModel.Interfaces;
using Framework.DomainModel.ValueObject;
using Repositories.Interfaces;
using System.Linq.Dynamic;

namespace Repositories
{
    public class EntityFrameworkZipRepository : EntityFrameworkRepositoryBase<Zip>, IZipRepository
    {
        public EntityFrameworkZipRepository()
            : base()
        {
            //Includes.Add("States");
            SearchColumns.Add("Name");
            SearchColumns.Add("State");
            SearchColumns.Add("City");
            DisplayColumnForCombobox = "Name";
        }

        public override IQueryable<ReadOnlyGridVo> BuildQueryToGetDataForGrid(IQueryInfo queryInfo)
        {
            var finalResult = (from entity in GetAll().AsNoTracking()
                               join city in WorkCompDb.Set<City>()
                                   on entity.CityId equals city.Id
                               join state in WorkCompDb.Set<State>()
                                  on city.StateId equals state.Id
                               select new { entity,city, state }).OrderBy(queryInfo.SortString).Select(x => new ZipGridVo()
                               {
                                   Name = x.entity.Name,
                                   Id = x.entity.Id,
                                   State = x.state.Name,
                                   City=x.city.Name
                               });
            return finalResult;
        }

        protected override void BuildSortExpression(IQueryInfo queryInfo)
        {
            base.BuildSortExpression(queryInfo);
            queryInfo.Sort.ForEach(x =>
            {
                if (x.Field.Contains("State"))
                {
                    x.Field = "(entity.City.State != null) ? entity.City.State.Name : string.Empty";
                }
                else if (x.Field.Contains("City"))
                {
                    x.Field = "(entity.City != null) ? entity.City.Name : string.Empty";
                }
            });
        }
    }
}