﻿using System;
using System.Collections.Generic;
using System.Linq;
using Framework.DomainModel.Entities;
using Repositories.Interfaces;

namespace Repositories
{
    public class EntityFrameworkReferralAttachmentRepository : EntityFrameworkRepositoryBase<ReferralAttachment>, IReferralAttachmentRepository
    {
        public IList<TResult> GetAttachments<TResult>(int referralId, Func<ReferralAttachment, TResult> selector)
        {
            var attachments = (from attachment in WorkCompDb.ReferralAttachments
                               where
                                   attachment.ReferralId == referralId
                               select attachment).Select(selector);
            return attachments.ToList();
        }

        public byte[] GetAttachmentByGuid(Guid rowGuid)
        {
            var fileContent = (from attachment in WorkCompDb.ReferralAttachments
                               where attachment.RowGUID == rowGuid
                               select attachment.AttachedFileContent).SingleOrDefault();

            return fileContent;
        }
    }
}