﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Linq.Dynamic;
using System.Text;
using Framework.DomainModel.Entities;
using Framework.DomainModel.Entities.Common;
using Framework.DomainModel.Interfaces;
using Framework.DomainModel.ValueObject;
using Repositories.Interfaces;

namespace Repositories
{
    public class EntityFrameworkConfigurationRepository : EntityFrameworkRepositoryBase<Configuration>, IConfigurationRepository
    {
        public EntityFrameworkConfigurationRepository()
        {
            SearchColumns.Add("Name");
        }

        public override IQueryable<ReadOnlyGridVo> BuildQueryToGetDataForGrid(IQueryInfo queryInfo)
        {
            var queryResult = (from entity in GetAll() select new {entity}).OrderBy(queryInfo.SortString).Select(s => new ConfigurationGridVo()
            {
                Id = s.entity.Id,
                TypeId = s.entity.TypeId,
                Name = s.entity.Name,
                Configurable = s.entity.Configurable,
                IsHtml = s.entity.IsHtml,
                Value = s.entity.Value,
            });
            return queryResult;
        }
    }
}