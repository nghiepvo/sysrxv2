﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using Framework.DomainModel.Entities;
using Framework.DomainModel.Entities.Common;
using Framework.DomainModel.Interfaces;
using Framework.DomainModel.ValueObject;
using System.Linq.Dynamic;
using Repositories.Interfaces;

namespace Repositories
{
    public class EntityFrameworkAttorneyRepository : EntityFrameworkRepositoryBase<Attorney>, IAttorneyRepository
    {
        public EntityFrameworkAttorneyRepository()
        {
            SearchColumns.Add("FirstName");
            SearchColumns.Add("MiddleName");
            SearchColumns.Add("LastName");
            SearchColumns.Add("Lawfirm");
            SearchColumns.Add("Phone");
            SearchColumns.Add("Email");
            SearchColumns.Add("Address");
            SearchColumns.Add("State");
            SearchColumns.Add("City");
            SearchColumns.Add("Zip");
            SearchColumns.Add("String.Concat(String.Concat(FirstName,\" \"),String.Concat(MiddleName,\" \"),LastName)");
            DisplayColumnForCombobox = "FirstName";
        }

        public override IQueryable<ReadOnlyGridVo> BuildQueryToGetDataForGrid(IQueryInfo queryInfo)
        {

            var queryResult = (from entity in GetAll()
                               join s in WorkCompDb.Set<State>() on entity.StateId equals s.Id into states
                               from state in states.DefaultIfEmpty()
                               join c in WorkCompDb.Set<City>() on entity.CityId equals c.Id into cities
                               from city in cities.DefaultIfEmpty()
                               join z in WorkCompDb.Set<Zip>() on entity.ZipId equals z.Id into zips
                               from zip in zips.DefaultIfEmpty()
                               select new { entity, state, city, zip }).OrderBy(queryInfo.SortString)
                .Select(o => new AttorneyGridVo
                {
                    Id = o.entity.Id,
                    LastName = o.entity.LastName,
                    FirstName = o.entity.FirstName,
                    MiddleName = o.entity.MiddleName,
                    Lawfirm = o.entity.Lawfirm,
                    Phone = o.entity.Phone,
                    Email = o.entity.Email,
                    Address = o.entity.Address,
                    State = o.state!= null? o.state.Name: string.Empty,
                    City = o.city!= null? o.city.Name: string.Empty,
                    Zip = o.zip != null ? o.zip.Name:string.Empty
                });
            return queryResult;
        }

        protected override void BuildSortExpression(IQueryInfo queryInfo)
        {
            base.BuildSortExpression(queryInfo);

            queryInfo.Sort.ForEach(x =>
            {
                if (x.Field.Contains("entity.State"))
                {
                    x.Field = "(state != null) ? state.Name : string.Empty";
                }
                if (x.Field.Contains("entity.City"))
                {
                    x.Field = "(city != null) ? city.Name : string.Empty";
                }
                if (x.Field.Contains("entity.Zip"))
                {
                    x.Field = "(zip != null) ? zip.Name : string.Empty";

                }
            });
            var flag = false;
            var dir = "";
            if (queryInfo.Sort == null || queryInfo.Sort.Count <= 0) return;

          

            foreach (var sort in queryInfo.Sort.Where(sort => sort.Field == "entity.Name"))
            {
                sort.Field = "entity.FirstName";
                dir = sort.Dir;
                flag = true;
            }

            if (!flag) return;
            queryInfo.Sort.Add(new Sort { Field = "entity.MiddleName", Dir = dir });
            queryInfo.Sort.Add(new Sort { Field = "entity.LastName", Dir = dir });
        }

        protected override string BuildLookupCondition(LookupQuery query)
        {
            var where = new StringBuilder();

            @where.Append("(");
            var innerWhere = new List<string>();
            var queryDisplayName = String.Format("FirstName.Contains(\"{0}\") OR LastName.Contains(\"{0}\") OR MiddleName.Contains(\"{0}\")", query.Query);
            innerWhere.Add(queryDisplayName);
            @where.Append(String.Join(" OR ", innerWhere.ToArray()));
            @where.Append(")");

            if (query.HierachyItems != null)
            {
                foreach (var parentItem in query.HierachyItems.Where(parentItem => parentItem.Value != string.Empty
                    && parentItem.Value != "-1"
                    && parentItem.Value != "0"
                    && !parentItem.IgnoredFilter))
                {
                    var filterValue = parentItem.Value.Replace(",", string.Format(" OR {0} = ", parentItem.Name));
                    @where.Append(string.Format(" AND ( {0} = {1})", parentItem.Name, filterValue));
                }
            }
            return @where.ToString();
        }

        public InfoWhenChangeAttorneyInReferral GetInfoWhenChangeAttorneyInReferral(int idAttorney)
        {
            var query = from entity in GetAll()
                        join s in WorkCompDb.States on entity.StateId equals s.Id into states
                        from state in states.DefaultIfEmpty()
                        join c in WorkCompDb.Cities on entity.CityId equals c.Id into cities
                        from city in cities.DefaultIfEmpty()
                        join z in WorkCompDb.Zips on entity.ZipId equals z.Id into zips
                        from zip in zips.DefaultIfEmpty()
                        where entity.Id == idAttorney
                        select new InfoWhenChangeAttorneyInReferral
                        {
                            AttorneyId = entity.Id,
                            AttorneyAddress = entity.Address,
                            AttorneyCity = city == null ? "" : city.Name,
                            AttorneyState = state == null ? "" : state.Name,
                            AttorneyZip = zip == null ? "" : zip.Name,
                            AttorneyEmail = entity.Email,
                            Phone = entity.Phone
                        };
            return query.FirstOrDefault();
        }
    }
}