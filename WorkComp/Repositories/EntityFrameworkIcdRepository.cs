﻿using System.Collections.Generic;
using System.Data.Entity;
using System.Linq;
using System.Linq.Dynamic;
using Framework.DomainModel.Entities;
using Framework.DomainModel.Interfaces;
using Framework.DomainModel.ValueObject;
using Repositories.Interfaces;

namespace Repositories
{
    public class EntityFrameworkIcdRepository : EntityFrameworkRepositoryBase<Icd>, IIcdRepository
    {
        public EntityFrameworkIcdRepository()
            : base()
        {
            //Includes.Add("IcdTypes");
            SearchColumns.Add("Code");
            SearchColumns.Add("Description");
            DisplayColumnForCombobox = "Code";
        }

        public override IQueryable<ReadOnlyGridVo> BuildQueryToGetDataForGrid(IQueryInfo queryInfo)
        {
            var finalResult = (from entity in GetAll().AsNoTracking()
                join t in WorkCompDb.Set<IcdType>().AsNoTracking() on entity.IcdTypeId equals t.Id into types
                 from type in types.DefaultIfEmpty()
                select new {entity, type}).OrderBy(queryInfo.SortString).Select(x => new IcdGridVo
                {
                    Code = x.entity.Code,
                    Id = x.entity.Id,
                    Description = x.entity.Description,
                    Type = x.type == null ? "" : x.type.Name
                });
            return finalResult;
        }

        public List<LookupItemVo> GetListIcdType()
        {
            return WorkCompDb.IcdTypes.Select(o => new LookupItemVo
            {
                KeyId = o.Id,
                DisplayName = o.Name
            }).ToList();
        }

        public List<Icd> GetIcdsByReferral(int referralId)
        {
            return (from entity in GetAll()
                join referralIcd in WorkCompDb.ReferralIcds on entity.Id equals referralIcd.IcdId
                where referralIcd.ReferralId == referralId
                select entity).ToList();
        }

        protected override void BuildSortExpression(IQueryInfo queryInfo)
        {
            base.BuildSortExpression(queryInfo);
            queryInfo.Sort.ForEach(x =>
            {
                if (x.Field.Contains("Type"))
                {
                    x.Field = "(entity.IcdType != null) ? entity.IcdType.Name : string.Empty";
                }
            });
        }
    }
}