﻿using System;
using System.Collections.Generic;
using System.Data.Entity;
using System.Data.Entity.Core.Objects;
using System.Data.Entity.Infrastructure;
using System.Data.Entity.Validation;
using System.Linq;
using System.Text;
using System.Threading;
using System.Web.Mvc;
using Framework.DomainModel;
using Framework.DomainModel.Entities;
using Framework.DomainModel.Entities.Mapping;
using Framework.DomainModel.Interfaces;
using Framework.Exceptions;
using Framework.Web;

namespace Repositories
{
    public class WorkCompDbContext : DbContext
    {
        
        public DbSet<User> Users { get; set; }
        public DbSet<SecurityOperation> SecurityOperations { get; set; }
        public DbSet<UserRole> UserRoles { get; set; }
        public DbSet<UserRoleFunction> UserRoleFunctions { get; set; }
        public DbSet<City> Cities { get; set; }
        public DbSet<Icd> Icds { get; set; }
        public DbSet<IcdType> IcdTypes { get; set; }
        public DbSet<DocumentType> DocumentTypes { get; set; }
        public DbSet<State> States { get; set; }
        public DbSet<Drug> Drugs { get; set; }
        public DbSet<Employer> Employers { get; set; }
        public DbSet<NpiNumber> NpiNumbers { get; set; }
        public DbSet<Attorney> Attorneys { get; set; }
        public DbSet<CollectionSite> CollectionSites { get; set; }
        public DbSet<ReferralType> ReferralTypes { get; set; }
        public DbSet<ReferralSource> ReferralSources { get; set; }
        public DbSet<SampleTestingType> SampleTestingTypes { get; set; }
        public DbSet<AssayCodeDescription> AssayCodeDescriptions { get; set; }
        public DbSet<AssayCode> AssayCodes { get; set; }
        public DbSet<Adjuster> Adjusters { get; set; }
        public DbSet<ClaimNumber> ClaimNumbers { get; set; }
        public DbSet<Zip> Zips { get; set; }
        public DbSet<ClaimantLanguage> ClaimantLanguages { get; set; }
        public DbSet<Payer> Payers { get; set; }
        public DbSet<TaskTemplate> TaskTemplates { get; set; }
        public DbSet<TaskGroup> TaskGroups { get; set; }
        public DbSet<TaskGroupTaskTemplate> TaskGroupTaskTemplates { get; set; }
        public DbSet<Branch> Branchs { get; set; }
        public DbSet<Claimant> Claimants { get; set; }
        public DbSet<PanelCode> PanelCodes { get; set; }
        public DbSet<PanelType> PanelTypes { get; set; }
        public DbSet<Panel> Panels { get; set; }
        public DbSet<Referral> Referrals { get; set; }
        public DbSet<CaseManager> CaseManagers { get; set; }
        public DbSet<ReasonReferral> ReasonReferrals { get; set; }
        public DbSet<ReferralReasonReferral> ReferralReasonReferrals { get; set; }
        public DbSet<ProductType> ProductTypes { get; set; }

        public DbSet<EmailTemplate> EmailTemplates { get; set; }
        public DbSet<ReferralIcd> ReferralIcds { get; set; }
        public DbSet<ReferralSampleTestingType> ReferralSampleTestingTypes { get; set; }
        public DbSet<ReferralCollectionSite> ReferralCollectionSites { get; set; }
        public DbSet<ReferralNpiNumber> ReferralNpiNumbers { get; set; }
        public DbSet<ReferralMedicationHistory> ReferralMedicationHistories { get; set; }
        public DbSet<ReferralTask> ReferralTasks { get; set; }
        public DbSet<ReferralAttachment> ReferralAttachments { get; set; }
        public DbSet<Configuration> Configurations { get; set; }
        public DbSet<ReferralNote> ReferralNotes { get; set; }
        public DbSet<Diary> Diaries { get; set; }
        public DbSet<Heading> Headings { get; set; }
        public DbSet<Comment> Comments { get; set; }
        public DbSet<Alert> Alerts { get; set; }
        public DbSet<ReferralEmailTemplate> ReferralEmailTemplates { get; set; }
        public DbSet<ReferralCancel> ReferralCancels { get; set; }
        public DbSet<TestResult> TestResults { get; set; }
        public DbSet<ReferralEmailTemplateAttachment> ReferralEmailTemplateAttachments { get; set; }


        public WorkCompDbContext()
            : base("Name=WorkCompDB")
        {
            Database.SetInitializer<WorkCompDbContext>(null);
        }
        private static string NameOrConnectionString { get; set; }
        public ObjectContext ObjectContext { get; private set; }
        public WorkCompDbContext(string nameOrConnectionString)
            : base(nameOrConnectionString)
        {
            Database.SetInitializer<WorkCompDbContext>(null);
            var objectContext = (this as IObjectContextAdapter).ObjectContext;

            // TODO: Need configuration . Sets the command timeout for all the commands
            objectContext.CommandTimeout = 360;
            ObjectContext = objectContext;
            NameOrConnectionString = nameOrConnectionString;
        }

        protected override void OnModelCreating(DbModelBuilder modelBuilder)
        {
            modelBuilder.Configurations.Add(new UserMap());
            modelBuilder.Configurations.Add(new SecurityOperationMap());
            modelBuilder.Configurations.Add(new UserRoleMap());
            modelBuilder.Configurations.Add(new UserRoleFunctionMap());
            modelBuilder.Configurations.Add(new CityMap());
            modelBuilder.Configurations.Add(new IcdTypeMap());
            modelBuilder.Configurations.Add(new IcdMap());
            modelBuilder.Configurations.Add(new DocumentTypeMap());
            modelBuilder.Configurations.Add(new GridConfigMap());
            modelBuilder.Configurations.Add(new StateMap());
            modelBuilder.Configurations.Add(new DrugMap());
            modelBuilder.Configurations.Add(new EmployerMap());
            modelBuilder.Configurations.Add(new NpiNumberMap());
            modelBuilder.Configurations.Add(new AttorneyMap());
            modelBuilder.Configurations.Add(new CollectionSiteMap());
            modelBuilder.Configurations.Add(new ReferralTypeMap());
            modelBuilder.Configurations.Add(new ReferralSourceMap());
            modelBuilder.Configurations.Add(new SampleTestingTypeMap());
            modelBuilder.Configurations.Add(new AssayCodeDescriptionMap());
            modelBuilder.Configurations.Add(new AssayCodeMap());
            modelBuilder.Configurations.Add(new AdjusterMap());
            modelBuilder.Configurations.Add(new ClaimNumberMap());
            modelBuilder.Configurations.Add(new ZipMap());
            modelBuilder.Configurations.Add(new ClaimantLanguageMap());
            modelBuilder.Configurations.Add(new PayerMap());
            modelBuilder.Configurations.Add(new TaskGroupMap());
            modelBuilder.Configurations.Add(new TaskTemplateMap());
            modelBuilder.Configurations.Add(new TaskGroupTaskTemplateMap());
            modelBuilder.Configurations.Add(new BranchMap());
            modelBuilder.Configurations.Add(new ClaimantMap());
            modelBuilder.Configurations.Add(new PanelCodeMap());
            modelBuilder.Configurations.Add(new PanelTypeMap());
            modelBuilder.Configurations.Add(new PanelMap());
            modelBuilder.Configurations.Add(new ReferralMap());
            modelBuilder.Configurations.Add(new CaseManagerMap());
            modelBuilder.Configurations.Add(new ReasonReferralMap());
            modelBuilder.Configurations.Add(new ReferralReasonReferralMap());
            modelBuilder.Configurations.Add(new EmailTemplateMap());
            modelBuilder.Configurations.Add(new ProductTypeMap());
            modelBuilder.Configurations.Add(new ReferralIcdMap());
            modelBuilder.Configurations.Add(new ReferralCollectionSiteMap());
            modelBuilder.Configurations.Add(new ReferralNpiNumberMap());
            modelBuilder.Configurations.Add(new ReferralMedicationHistoryMap());
            modelBuilder.Configurations.Add(new ReferralTaskMap());
            modelBuilder.Configurations.Add(new ReferralAttachmentMap());
            modelBuilder.Configurations.Add(new ConfigurationMap());
            modelBuilder.Configurations.Add(new ReferralNoteMap());
            modelBuilder.Configurations.Add(new DiaryMap());
            modelBuilder.Configurations.Add(new HeadingMap());
            modelBuilder.Configurations.Add(new CommentMap());
            modelBuilder.Configurations.Add(new AlertMap());
            modelBuilder.Configurations.Add(new ReferralEmailTemplateMap());
            modelBuilder.Configurations.Add(new ReferralSampleTestingTypeMap());
            modelBuilder.Configurations.Add(new ReferralCancelMap());
            modelBuilder.Configurations.Add(new TestResultMap());
            modelBuilder.Configurations.Add(new ReferralEmailTemplateAttachmentMap());
        }
        public User GetCurrentUser()
        {
            User currentUser = null;
            var context = DependencyResolver.Current.GetService<IWorkCompHttpContext>(); 
            IWorkcompPrincipal principal;
            if (context != null)
            {
                principal = context.User as IWorkcompPrincipal;
            }
            else
            {
                principal = Thread.CurrentPrincipal as IWorkcompPrincipal;
            }

            if (principal != null && principal.User != null)
            {
                currentUser = Users.SingleOrDefault(x => x.Id == principal.User.Id);
            }

            return currentUser;
        }

        public override int SaveChanges()
        {
            var currentUser = GetCurrentUser();

            var deletedItemList = new List<DbEntityEntry<Entity>>();

            foreach (var entry in ChangeTracker.Entries<Entity>())
            {
                if (entry.Entity.IsDeleted)
                {
                    entry.State = EntityState.Deleted;
                }

                switch (entry.State)
                {
                    case EntityState.Added:
                        entry.Entity.SetCreatedOn(DateTime.Now);
                        
                        entry.Entity.SetCreatedBy(currentUser);
                        entry.Entity.SetLastUser(currentUser);
                        entry.Entity.SetLastModified(DateTime.Now);
                        break;
                    case EntityState.Modified:
                        entry.Entity.SetLastModified(DateTime.Now);
                        entry.Entity.SetLastUser(currentUser);
                        //entry.OriginalValues["LastModified"] = entry.Entity.LastModified;
                        break;
                    default:
                        //if (entry.State == EntityState.Deleted || entry.Entity.IsDeleted)
                        //{
                        //    // For the audit trigger of deletion case we have to update before deletion so that
                        //    // we can audit who delete the record.
                        //    deletedItemList.Add(entry);
                        //    entry.State = EntityState.Modified;

                        //    foreach (var fieldName in entry.CurrentValues.PropertyNames)
                        //    {
                        //        if (fieldName != "LastModified")
                        //            entry.CurrentValues[fieldName] = entry.OriginalValues[fieldName]; // want to keep original state of deletion object.
                        //    }
                        //    entry.Entity.SetLastUser(currentUser);
                        //    entry.OriginalValues["LastModified"] = entry.Entity.LastModified;
                        //}
                        break;
                }
            }

            int result;
            try
            {

                result = base.SaveChanges();

                //if (deletedItemList.Count > 0)
                //{
                //    foreach (var deleteItem in deletedItemList)
                //    {
                //        deleteItem.State = EntityState.Deleted;
                //        deleteItem.OriginalValues["LastModified"] = deleteItem.Entity.LastModified;
                //    }
                //    base.SaveChanges();
                //}

            }
            catch (DbEntityValidationException ex)
            {
                var sb = new StringBuilder();

                foreach (var failure in ex.EntityValidationErrors)
                {
                    sb.AppendFormat("{0} failed validation\n", failure.Entry.Entity.GetType());
                    foreach (var error in failure.ValidationErrors)
                    {
                        sb.AppendFormat("- {0} : {1}", error.PropertyName, error.ErrorMessage);
                        sb.AppendLine();
                    }
                }

                throw new UserVisibleException("GeneralExceptionMessageText", ex);
            }
            //catch (DbUpdateException ex)
            //{
            //    var updateException = ex.InnerException;
            //    if (updateException == null) throw new UserVisibleException("GeneralExceptionMessageText", ex);

            //    var sqlExeption = updateException.InnerException;
            //    if (sqlExeption is SqlException)
            //    {
            //        throw new UserVisibleException("DeleteChildText", ex);
            //    }

            //    throw new UserVisibleException("GeneralExceptionMessageText", ex);
            //}
            catch (Exception ex)
            {
                throw new UserVisibleException("GeneralExceptionMessageText", ex);
            }


            return result;
        }
    }
}
