﻿using System;
using System.Collections.Generic;
using System.Data.Entity;
using System.Linq;
using System.Text;
using Framework.DomainModel.Entities;
using Framework.DomainModel.Entities.Common;
using Framework.DomainModel.Interfaces;
using Framework.DomainModel.ValueObject;
using System.Linq.Dynamic;
using Repositories.Interfaces;

namespace Repositories
{
    public class EntityFrameworkNpiNumberRepository : EntityFrameworkRepositoryBase<NpiNumber>, INpiNumberRepository
    {
        public EntityFrameworkNpiNumberRepository()
        {
            SearchColumns.Add("FirstName");
            SearchColumns.Add("LastName");
            SearchColumns.Add("MiddleName");
            SearchColumns.Add("OrganizationName");
            SearchColumns.Add("Npi");
            SearchColumns.Add("Phone");
            SearchColumns.Add("Fax");
            SearchColumns.Add("LicenseNumber");
            SearchColumns.Add("ProviderCredentialText");
            SearchColumns.Add("Email");
            SearchColumns.Add("Address");
            SearchColumns.Add("State");
            SearchColumns.Add("City");
            SearchColumns.Add("Zip");
            SearchColumns.Add("String.Concat(String.Concat(FirstName,\" \"),String.Concat(MiddleName,\" \"),LastName)");
            SearchColumns.Add("String.Concat(String.Concat(Address, \", \"), String.Concat(State, \", \"), String.Concat(City, \", \"), Zip)");
            DisplayColumnForCombobox = "Npi";
        }

        public override IQueryable<ReadOnlyGridVo> BuildQueryToGetDataForGrid(IQueryInfo queryInfo)
        {
            var queryResult =(from entity in GetAll()
                              join s in WorkCompDb.Set<State>() on entity.StateId equals s.Id into states
                              from state in states.DefaultIfEmpty()
                              join c in WorkCompDb.Set<City>() on entity.CityId equals c.Id into cities
                              from city in cities.DefaultIfEmpty()
                              join z in WorkCompDb.Set<Zip>() on entity.ZipId equals z.Id into zips
                              from zip in zips.DefaultIfEmpty()
                              select new { entity, state, city, zip }).OrderBy(queryInfo.SortString)
                .Select(s => new NpiNumberGridVo
            {
                Id = s.entity.Id,
                FirstName = s.entity.FirstName,
                MiddleName = s.entity.MiddleName,
                LastName = s.entity.LastName,
                OrganizationName = s.entity.OrganizationName,
                Npi = s.entity.Npi,
                LicenseNumber = s.entity.LicenseNumber,
                ProviderCredentialText = s.entity.ProviderCredentialText,
                Phone = s.entity.Phone,
                Fax = s.entity.Fax,
                Email = s.entity.Email,
                Address = s.entity.Address,
                State = s.state != null? s.state.Name: string.Empty,
                City = s.city != null? s.city.Name: string.Empty,
                Zip = s.zip!= null? s.zip.Name: string.Empty
            });
            return queryResult;
        }

        protected override void BuildSortExpression(IQueryInfo queryInfo)
        {
            base.BuildSortExpression(queryInfo);

            bool flagAddress = false, flagName = false;

            var dir = "";
            if (queryInfo.Sort == null || queryInfo.Sort.Count <= 0) return;

            foreach (var sort in queryInfo.Sort.Where(sort => sort.Field == "entity.FullAddress"))
            {
                sort.Field = "entity.Address";
                dir = sort.Dir;
                flagAddress = true;
            }
            if (flagAddress)
            {
                queryInfo.Sort.Add(new Sort { Field = "(entity.State != null) ? entity.State.Name : string.Empty", Dir = dir });
                queryInfo.Sort.Add(new Sort { Field = "(entity.City != null) ? entity.City.Name : string.Empty", Dir = dir });
                queryInfo.Sort.Add(new Sort { Field = "(entity.Zip != null) ? entity.Zip.Name : string.Empty", Dir = dir });
            }

            foreach (var sort in queryInfo.Sort.Where(sort => sort.Field == "entity.Name"))
            {
                sort.Field = "entity.FirstName";
                dir = sort.Dir;
                flagName = true;
            }

            if (flagName)
            {
                queryInfo.Sort.Add(new Sort { Field = "entity.MiddleName", Dir = dir });
                queryInfo.Sort.Add(new Sort { Field = "entity.LastName", Dir = dir });
            }
        }

        public List<LookupItemVo> GetLookupTreatingPhysician(LookupQuery query, Func<NpiNumber, LookupItemVo> selector)
        {
            var lookupWhere = BuildLookupConditionForTreatingPhysician(query);

            var lookupList = GetAll().AsNoTracking().Where(lookupWhere);
            var currentRecord = GetAll().AsNoTracking().Where(x => x.Id == query.Id);
            if (!query.IncludeCurrentRecord && currentRecord.SingleOrDefault() != null) // Return single record to reduce the size of return data when first time binding.
            {
                return currentRecord.Select(selector).ToList();
            }

            if (!string.IsNullOrEmpty(query.Query) || !query.IncludeCurrentRecord)
            {
                currentRecord = Enumerable.Empty<NpiNumber>().AsQueryable();
            }

            var lookupAnonymous = lookupList
                        .Union(currentRecord)
                        .OrderBy("FirstName")
                        .Skip(0)
                        .Take(query.Take)
                        .Select(selector);
            return lookupAnonymous.ToList();
        }

        protected string BuildLookupConditionForTreatingPhysician(LookupQuery query)
        {
            var where = new StringBuilder();

            @where.Append("(");
            var innerWhere = new List<string>();
            var queryDisplayName = String.Format("FirstName.Contains(\"{0}\") OR LastName.Contains(\"{0}\") OR MiddleName.Contains(\"{0}\")", query.Query);
            innerWhere.Add(queryDisplayName);
            @where.Append(String.Join(" OR ", innerWhere.ToArray()));
            @where.Append(")");

            if (query.HierachyItems != null)
            {
                foreach (var parentItem in query.HierachyItems.Where(parentItem => parentItem.Value != string.Empty
                    && parentItem.Value != "-1"
                    && parentItem.Value != "0"
                    && !parentItem.IgnoredFilter))
                {
                    var filterValue = parentItem.Value.Replace(",", string.Format(" OR {0} = ", parentItem.Name));
                    @where.Append(string.Format(" AND ( {0} = {1})", parentItem.Name, filterValue));
                }
            }
            return @where.ToString();
        }

        public InfoWhenChangeTreatingPhysicianInReferral GetInfoWhenChangeTreatingPhysicianInReferral(int idTreatingPhysician)
        {
            var query = from entity in GetAll()
                        where entity.Id == idTreatingPhysician
                        select new InfoWhenChangeTreatingPhysicianInReferral
                        {
                            TreatingPhysicianId = idTreatingPhysician,
                            Fax = entity.Fax,
                            Phone = entity.Phone,
                            TreatingPhysicianAddress = entity.Address,
                            TreatingPhysicianEmail = entity.Email,
                            NpiNumberText = entity.Npi,
                            TreatingPhysicianName = entity.FirstName+" "+entity.MiddleName+" "+entity.LastName
                        };
            return query.FirstOrDefault();
        }
    }
}