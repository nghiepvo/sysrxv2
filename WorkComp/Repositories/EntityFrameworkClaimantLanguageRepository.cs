﻿using System.Collections.Generic;
using System.Linq;
using Repositories.Interfaces;
using Framework.DomainModel.ValueObject;
using Framework.DomainModel.Interfaces;
using System.Linq.Dynamic;
using Framework.DomainModel.Entities.Common;
using Framework.DomainModel.Entities;

namespace Repositories
{
    public class EntityFrameworkClaimantLanguageRepository : EntityFrameworkRepositoryBase<ClaimantLanguage>, IClaimantLanguageRepository
    {
        public EntityFrameworkClaimantLanguageRepository()
        {
            SearchColumns.Add("Name");
            DisplayColumnForCombobox = "Name";
        }

        public override IQueryable<ReadOnlyGridVo> BuildQueryToGetDataForGrid(IQueryInfo queryInfo)
        {
            var queryResult = GetAll().OrderBy(queryInfo.SortString).Select(s => new ClaimantLanguageGridVo
            {
                Id = s.Id,
                Name = s.Name
            });
            return queryResult;
        }

        protected override void BuildSortExpression(IQueryInfo queryInfo)
        {
            if (queryInfo.Sort == null || (queryInfo.Sort != null && queryInfo.Sort.Count == 0))
            {
                queryInfo.Sort = new List<Sort> { new Sort { Field = "Name", Dir = "asc" } };
            }
        }
    }
}
