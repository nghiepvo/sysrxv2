﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using Framework.DomainModel.Entities;
using Framework.DomainModel.Entities.Common;
using Framework.DomainModel.Interfaces;
using Framework.DomainModel.ValueObject;
using System.Linq.Dynamic;
using Repositories.Interfaces;

namespace Repositories
{
    public class EntityFrameworkAdjusterRepository : EntityFrameworkRepositoryBase<Adjuster>, IAdjusterRepository
    {
        public EntityFrameworkAdjusterRepository()
        {
            SearchColumns.Add("FirstName");
            SearchColumns.Add("MiddleName");
            SearchColumns.Add("LastName");
            SearchColumns.Add("Phone");
            SearchColumns.Add("Email");
            SearchColumns.Add("Payer");
            SearchColumns.Add("Branch");
            SearchColumns.Add("Extension");
            SearchColumns.Add("Address");
            SearchColumns.Add("State");
            SearchColumns.Add("City");
            SearchColumns.Add("Zip");
            SearchColumns.Add("String.Concat(String.Concat(FirstName,\" \"),String.Concat(MiddleName,\" \"),LastName)");
            SearchColumns.Add("String.Concat(String.Concat(Address,\",\",\" \"),String.Concat(State,\",\",\" \"),String.Concat(City,\",\",\" \"),Zip)");
            DisplayColumnForCombobox = "FirstName";
        }

        public override IQueryable<ReadOnlyGridVo> BuildQueryToGetDataForGrid(IQueryInfo queryInfo)
        {

            var queryResult = (from entity in GetAll()
                               join s in WorkCompDb.Set<State>() on entity.StateId equals s.Id into states
                               from state in states.DefaultIfEmpty()
                               join c in WorkCompDb.Set<City>() on entity.CityId equals c.Id into cities
                               from city in cities.DefaultIfEmpty()
                               join z in WorkCompDb.Set<Zip>() on entity.ZipId equals z.Id into zips
                               from zip in zips.DefaultIfEmpty()
                              
                               join b in WorkCompDb.Set<Branch>() on entity.BranchId equals b.Id into branchs
                               from branch in branchs.DefaultIfEmpty()
                               select new { entity, state, zip, city,  branch })
                               .OrderBy(queryInfo.SortString)
                                .Select(o => new AdjusterGridVo
                                {
                                    Id = o.entity.Id,
                                    Branch = o.branch == null ? "" : o.branch.Name,
                                    FirstName = o.entity.FirstName,
                                    MiddleName = o.entity.MiddleName,
                                    LastName = o.entity.LastName,
                                    Email = o.entity.Email,
                                    Extension = o.entity.Extension,
                                    Phone = o.entity.Phone,
                                    Address = o.entity.Address,
                                    State = o.state == null ? "" : o.state.Name,
                                    City = o.city == null ? "" : o.city.Name,
                                    Zip = o.zip == null ? "" : o.zip.Name,
                                    //TODO: Edit Allow Create Referral
                                    //AllowCreateReferral = o.entity.AllowCreateReferral== true,
                                    OptedOutSendMail = o.entity.OptedOutSendMail == true,
                                })
                                ;
            return queryResult;
        }

        protected override void BuildSortExpression(IQueryInfo queryInfo)
        {
            base.BuildSortExpression(queryInfo);
            bool flagAddress = false, flagName = false;

            var dir = "";
            if (queryInfo.Sort == null || queryInfo.Sort.Count <= 0) return;

            foreach (var sort in queryInfo.Sort.Where(sort => sort.Field == "entity.FullAddress"))
            {
                sort.Field = "entity.Address";
                dir = sort.Dir;
                flagAddress = true;
            }
            if (flagAddress)
            {
                queryInfo.Sort.Add(new Sort { Field = "(entity.State != null) ? entity.State.Name : string.Empty", Dir = dir });
                queryInfo.Sort.Add(new Sort { Field = "(entity.City != null) ? entity.City.Name : string.Empty", Dir = dir });
                queryInfo.Sort.Add(new Sort { Field = "(entity.Zip != null) ? entity.Zip.Name : string.Empty", Dir = dir });
            }

            foreach (var sort in queryInfo.Sort.Where(sort => sort.Field == "entity.Name"))
            {
                sort.Field = "entity.FirstName";
                dir = sort.Dir;
                flagName = true;
            }

            if (flagName)
            {
                queryInfo.Sort.Add(new Sort { Field = "entity.MiddleName", Dir = dir });
                queryInfo.Sort.Add(new Sort { Field = "entity.LastName", Dir = dir });    
            }

            queryInfo.Sort.ForEach(x =>
            {
                if (x.Field.Contains("Payer"))
                {
                    x.Field = "(entity.Payer != null) ? entity.Payer.Name : string.Empty";
                }
                else if (x.Field.Contains("Branch"))
                {
                    x.Field = "(entity.Branch != null) ? entity.Branch.Name : string.Empty";
                }
            });
        }

        protected override string BuildLookupCondition(LookupQuery query)
        {
            var where = new StringBuilder();

            @where.Append("(");
            var innerWhere = new List<string>();
            var queryDisplayName = String.Format("FirstName.Contains(\"{0}\") OR LastName.Contains(\"{0}\") OR MiddleName.Contains(\"{0}\")", query.Query);
            innerWhere.Add(queryDisplayName);
            @where.Append(String.Join(" OR ", innerWhere.ToArray()));
            @where.Append(")");

            if (query.HierachyItems != null)
            {
                foreach (var parentItem in query.HierachyItems.Where(parentItem => parentItem.Value != string.Empty
                    && parentItem.Value != "-1"
                    && parentItem.Value != "0"
                    && !parentItem.IgnoredFilter))
                {
                    var filterValue = parentItem.Value.Replace(",", string.Format(" OR {0} = ", parentItem.Name));
                    @where.Append(string.Format(" AND ( {0} = {1})", parentItem.Name, filterValue));
                }
            }
            return @where.ToString();
        }

        public InfoWhenChangeAdjusterInReferral GetInfoWhenChangeAdjusterInReferral(int idAdjuster)
        {
            var query = from entity in GetAll()
                        join s in WorkCompDb.States on entity.StateId equals s.Id into states
                        from state in states.DefaultIfEmpty()
                        join c in WorkCompDb.Cities on entity.CityId equals c.Id into cities
                        from city in cities.DefaultIfEmpty()
                        join z in WorkCompDb.Zips on entity.ZipId equals z.Id into zips
                        from zip in zips.DefaultIfEmpty()
                        where entity.Id == idAdjuster
                        select new InfoWhenChangeAdjusterInReferral()
                        {
                            AdjusterId = entity.Id,
                            AdjusterAddress = entity.Address,
                            AdjusterCity = city == null ? "" : city.Name,
                            AdjusterState = state == null ? "" : state.Name,
                            AdjusterZip = zip == null ? "" : zip.Name,
                            AdjusterExternalId = entity.ExternalId,
                            AdjusterEmail = entity.Email,
                            AdjusterExtension = entity.Extension,
                            Phone = entity.Phone,
                            AllowCreateReferral = entity.AllowCreateReferral
                        };
            return query.FirstOrDefault();
        }
    }
}