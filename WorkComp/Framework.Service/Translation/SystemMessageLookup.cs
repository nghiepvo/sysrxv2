﻿namespace Framework.Service.Translation
{
    public class SystemMessageLookup
    {
        public static string GetMessage(string resourceKey)
        {
            switch (resourceKey)
            {
                case "RestorePasswordPageTitle":
                    return "Restore password";
                case "InvalidUserAndPasswordText":
                    return "Username and/or password not recognized";
                case "PasswordOldInvalid":
                    return "Invalid Password";
                case "PasswordNotMatch":
                    return "Confirm password does not match";
                case "ChangePasswordError":
                    return "Change password attempt failed ";
                case "FieldInvalidText":
                    return "{0} invalid input";
                case "GeneralExceptionMessageText":
                    return
                        "An unexpected exception has occurred in the code, please contact your system administrator and pass on the information contained within this message.";
                case "RequiredTextResourceKey":
                    return "The {0} field is required";
                case "RequiredTextResourceKeyMultiField":
                    return "The {0} fields are required";
                case "ExistsTextResourceKey":
                    return "The {0} field have already existed in the system.";
                case "BussinessGenericErrorMessageKey":
                    return "Please fill out all the required fields:";
                case "SignInPageTitle":
                    return "Sign in";
                case "DirtyDialogMessageText":
                    return
                        "To prevent information loss, please save the changes before closing.";
                case "NoRecord":
                    return "No Record.";
                case "RestorePasswordSuccessText":
                    return "Password reset completed. Please check your email for confirmation. ";
                case "ConfirmationRequiredText":
                    return "Confirmation Required";
                case "ConfirmDeleteText":
                    return "Are you sure you want to delete this record?";
                case "ConfirmCompleteTaskText":
                    return "Are you sure you want to complete this task?";
                case "NoText":
                    return "No";
                case "YesText":
                    return "Yes";
                case "CreateText":
                    return "Create";
                case "UpdateText":
                    return "Update";
                case "CannotCopyText":
                    return "Please select an item to be copy";
                case "UpdateSuccessText":
                    return "Successfully updated!";
                case "CreateSuccessText":
                    return "Successfully created!";
                case "DeleteSuccessText":
                    return "Successfully deleted!";
                case "MaxLengthRequied":
                    return "The {0} must be {1} characters long.";
                case "EmailValid":
                    return "Invalid email address.";
                case "PhoneValid":
                    return "Invalid phone number.";
                case "FaxValid":
                    return "Invalid fax number.";
                case "DateTimeValid":
                    return "Invalid date and/or time.";
                case "SubjectToSendEmailForCreateUser":
                    return "Login information for SysRX.";
                case "UnAuthorizedAccessText":
                    return "You are not authorized to access the {0}.";
                case "ItemExistsWithParentItem":
                    return "The {0} {1} existed within the {2} system ";
                case "DuplicateItemText":
                    return "There is a duplicate error in the {0} field.";
                case "ActiveText":
                    return "Are you sure you want to activate the record? ";
                case "InactiveText":
                    return "Are you sure you want to deactivate the record?";
                case "DeleteChildText"://Xem lai
                    return "Please delete the {0} first before deleting this text.";
                case "MaximumCreateReasonReferral":
                    return "The user have a level 3 permission";
                case "CannotGreaterThanText":
                    return "{0} cannot greater than {1}.";
                case "UserMustChooseSearchCondition":
                    return "Please choose a search condition.";
                case "CommentFormatForAddDiaryWhenAddReferral":
                    return @"Date & Time Stamp:{0}<br />
                            Referral Source and Title:{1}<br />
                            Referral Method:{2}<br />
                            Service requested:{3}<br />
                            Special Instructions:{4}<br />
                            {5}<br />";
                case "UserMustChooseCancelReason":
                    return "Please choose a cancellation reason.";
                case "ReferralCompleteHaveTaskNotComplete":
                    return "For the referral to have a completed status, all of the tasks in the referral must be completed.";
                case "SendEmailFailed":
                    return "The sent email has fail.";
                case "SendEmailSuccess":
                    return "Email successfully sent.";
                case "SendFaxFailed":
                    return "The sent fax has fail.";
                case "SendFaxSuccess":
                    return "Fax successfully sent.";
                case "IncludeContentHtmlAttachment":
                    return "Include attachment: <i style='color:#ff0000; font-size: 20px;' class='fa fa-file-pdf-o'></i> {0}";
                case "IncludedMessageHtml":
                    return "The working link will be included in the message: ";
                case "SecureEmail":
                    return "[PHI]";
                case "InformationConcerning":
                    return "The information relating to {0}";
                case "IsClosed":
                    return "The {0} is closed.";
                case "CannotAssignTo":
                    return "Currently the system cannot use the assign functionality.";
                case "ClaimantExpiration":
                    return "The {0} is expired.";
                case "NotAllowCreateReferral":
                    return "{0} is not allowed to create referral.";
                case "CommentMessage":
                    return "Required pre-authorization renewal: <a href='javascript:void(0);' onclick='ShowPopup(600, 800, \"Update Item\", \"{0}\",null);'>{1}</a>";
                case "DiaryHeadingAdd":
                    return "Added";
                case "DiaryReasonAddReferral":
                    return "New referral";
                case "DiaryHeadingValidated":
                    return "Validated";
                case "DiaryReasonValidate":
                    return "Validate pre-authorization";
                case "DiaryReasonComment":
                    return "Pre-authorization is validated.";
                case "DiaryHeadingExpired":
                    return "Expired";
                case "DiaryReasonExpired":
                    return "Pre-authorization has been expired";
                case "TaskTitleAutoGenerate1":
                    return "Collection Service Request Letter - Broadspire‏";
                case "TaskDescriptionAutoGenerate1":
                    return "View:<a href='javascript:void(0)' onclick='ShowPopupToSendMailForCollectionServiceRequestLetter({0})' class='orange'><span>Collection Service Request Letter - Broadspire‏</span></a>";
                case "DiaryReasonAddTask":
                    return "New task";
                case "DiaryCommentTask":
                    return "Title: {0}<br />" + "Description:{1}";
                case "DiaryReasonAddAttachment":
                    return "New file attachment ";
                case "DiaryCommentAddAttachment":
                    return "Add new file attachment: '{0}', size: {1}";
                case "DiaryRush":
                    return "Rush";
                case "DiaryHeadingDeleted":
                    return "Deleted";
                case "DiaryReasonDeleteAttachment":
                    return "Delete attached file";
                case "DiaryCommentDeleteAttachment":
                    return "Delete attached file: '{0}', size: {1}";
            }
            return "";
        }

    }
}
