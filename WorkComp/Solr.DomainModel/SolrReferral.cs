﻿using System;
using SolrNet.Attributes;

namespace Solr.DomainModel
{
    public class SolrReferral : SolrEntity
    {
        [SolrField("referralid")]
        public int ReferralId { get; set; }
        [SolrField("controlnumber")]
        public string ControlNumber { get; set; }
        [SolrField("producttype")]
        public string ProductType { get; set; }
        [SolrField("producttypeid")]
        public int ProductTypeId { get; set; }
        [SolrField("treatingphysicianname")]
        public string TreatingPhysicianName { get; set; }
        [SolrField("treatingphysicianphone")]
        public string TreatingPhysicianPhone { get; set; }
        [SolrField("npinumberid")]
        public int NpiNumberId { get; set; }
        [SolrField("assigntoname")]
        public string AssignToName { get; set; }
        [SolrField("status")]
        public string Status { get; set; }
        [SolrField("payername")]
        public string PayerName { get; set; }
        [SolrField("payerid")]
        public int PayerId { get; set; }
        [SolrField("jurisdiction")]
        public string Jurisdiction { get; set; }
        [SolrField("rush")]
        public bool Rush { get; set; }
        [SolrField("duedate")]
        public DateTime? DueDate { get; set; }
        [SolrField("claimantname")]
        public string ClaimantName { get; set; }
        [SolrField("claimantid")]
        public int ClaimantId { get; set; }
        [SolrField("claimnumber")]
        public string ClaimNumber { get; set; }
        [SolrField("claimnumberid")]
        public int ClaimNumberId { get; set; }
        [SolrField("patientstate")]
        public string PatientState { get; set; }
        [SolrField("doi")]
        public DateTime? Doi { get; set; }
        [SolrField("createdbyname")]
        public string CreatedByName { get; set; }
        [SolrField("createddate")]
        public DateTime? CreatedDate { get; set; }
        [SolrField("adjustername")]
        public string AdjusterName { get; set; }
        [SolrField("adjusterid")]
        public int AdjusterId { get; set; }
        [SolrField("nextofficevisitdate")]
        public DateTime? NextOfficeVisitDate { get; set; }
        [SolrField("claimantssn")]
        public string ClaimantSsn { get; set; }
        [SolrField("claimantdob")]
        public DateTime? ClaimantDob { get; set; }
        [SolrField("claimnumberdoi")]
        public DateTime? ClaimNumberDoi { get; set; }
        [SolrField("totalnote")]
        public int TotalNote { get; set; }
        [SolrField("iscollectionsite")]
        public bool IsCollectionSite { get; set; }
        [SolrField("recieveddate")]
        public DateTime? RecievedDate { get; set; }
        [SolrField("assigntoid")]
        public int? AssignToId { get; set; }
        [SolrField("canceldate")]
        public DateTime? CancelDate { get; set; }
        [SolrField("completeddate")]
        public DateTime? CompletedDate { get; set; }
        [SolrField("treatingphysicianfirstname")]
        public string TreatingPhysicianFirstName { get; set; }
        [SolrField("treatingphysicianlastname")]
        public string TreatingPhysicianLastName { get; set; }
        [SolrField("treatingphysicianmiddlename")]
        public string TreatingPhysicianMiddleName { get; set; }
        [SolrField("treatingphysicianaddress")]
        public string TreatingPhysicianAddress { get; set; }
        [SolrField("treatingphysiciancity")]
        public string TreatingPhysicianCity { get; set; }
        [SolrField("treatingphysicianstate")]
        public string TreatingPhysicianState { get; set; }
        [SolrField("treatingphysicianpostcode")]
        public string TreatingPhysicianPostCode { get; set; }
        [SolrField("treatingphysicianfax")]
        public string TreatingPhysicianFax { get; set; }
        [SolrField("treatingphysicianemail")]
        public string TreatingPhysicianEmail { get; set; }
        [SolrField("npinumber")]
        public string NpiNumber { get; set; }
    }
}