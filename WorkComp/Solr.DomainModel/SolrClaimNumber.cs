﻿using System;
using SolrNet.Attributes;

namespace Solr.DomainModel
{
    public class SolrClaimNumber : SolrEntity
    {
        [SolrField("claimnumberid")]
        public int ClaimNumberId { get; set; }
        [SolrField("claimantid")]
        public int ClaimantId { get; set; }
        [SolrField("claimantname")]
        public string ClaimantName { get; set; }
        [SolrField("adjusterid")]
        public int AdjusterId { get; set; }
        [SolrField("adjustername")]
        public string AdjusterName { get; set; }
        [SolrField("branchid")]
        public int BranchId { get; set; }
        [SolrField("branchname")]
        public string BranchName { get; set; }
        [SolrField("payerid")]
        public int PayerId { get; set; }
        [SolrField("payername")]
        public string PayerName { get; set; }
        [SolrField("employerid")]
        public int EmployerId { get; set; }
        [SolrField("doi")]
        public DateTime? Doi { get; set; }
        [SolrField("name")]
        public string Name { get; set; }
        [SolrField("stateid")]
        public int StateId { get; set; }
        [SolrField("jurisdiction")]
        public string Jurisdiction { get; set; }
        [SolrField("specialinstructions")]
        public string SpecialInstructions { get; set; }
        [SolrField("closedate")]
        public DateTime? CloseDate { get; set; }
        [SolrField("opendate")]
        public DateTime? OpenDate { get; set; }
        [SolrField("reportdate")]
        public DateTime? ReportDate { get; set; }
        [SolrField("status")]
        public string Status { get; set; }
        [SolrField("claimantssn")]
        public string ClaimantSsn { get; set; }
        [SolrField("claimantdob")]
        public DateTime? ClaimantDob { get; set; }
    }
}