﻿using SolrNet.Attributes;

namespace Solr.DomainModel
{
    public class SolrEntity
    {
        [SolrUniqueKey("id")]
        public string Id { get; set; }
    }
}