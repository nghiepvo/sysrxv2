﻿using System;
using SolrNet.Attributes;

namespace Solr.DomainModel
{
    public class SolrClaimant : SolrEntity
    {
        [SolrField("idclaimant")]
        public int IdClaimant { get; set; }
        [SolrField("fullname")]
        public string FullName { get; set; }
        [SolrField("gender")]
        public string Gender { get; set; }
        [SolrField("dob")]
        public DateTime? Dob { get; set; }
        [SolrField("email")]
        public string Email { get; set; }
        [SolrField("bestcontactnumber")]
        public string BestContactNumber { get; set; }
        [SolrField("ssn")]
        public string Ssn { get; set; }
        [SolrField("language")]
        public string Language { get; set; }
        [SolrField("address")]
        public string Address { get; set; }
    }
}