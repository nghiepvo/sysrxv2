﻿using SolrNet.Attributes;

namespace Solr.DomainModel
{
    public class SolrNpiNumber : SolrEntity
    {
        [SolrField("npinumberid")]
        public int? NpiNumberId { get; set; }
        [SolrField("npi")]
        public string Npi { get; set; }
        [SolrField("organization")]
        public string Organization { get; set; }
        [SolrField("fullname")]
        public string FullName { get; set; }
        [SolrField("providercredential")]
        public string ProviderCredential { get; set; }
        [SolrField("licensenumber")]
        public string LicenseNumber { get; set; }
        [SolrField("phone")]
        public string Phone { get; set; }
        [SolrField("fax")]
        public string Fax { get; set; }
        [SolrField("email")]
        public string Email { get; set; }
        [SolrField("address")]
        public string Address { get; set; }
    }
}