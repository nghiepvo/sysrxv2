using System;

namespace Framework.Exceptions
{
    [Serializable]
    public class BusinessLogicException :WorkCompException
    {
        public BusinessLogicException(string message, Exception rootCause)
            : base(message, rootCause)
        {
        }
    }
}