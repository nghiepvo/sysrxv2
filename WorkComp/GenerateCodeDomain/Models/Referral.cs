using System;
using System.Collections.Generic;

namespace GenerateCodeDomain.Models
{
    public partial class Referral
    {
        public Referral()
        {
            this.Notes = new List<Note>();
            this.ReferralAttachments = new List<ReferralAttachment>();
            this.ReferralCollectionSites = new List<ReferralCollectionSite>();
            this.ReferralIcds = new List<ReferralIcd>();
            this.ReferralMedicationHistories = new List<ReferralMedicationHistory>();
            this.ReferralNpiNumbers = new List<ReferralNpiNumber>();
            this.ReferralReasonReferrals = new List<ReferralReasonReferral>();
            this.ReferralTasks = new List<ReferralTask>();
        }

        public int Id { get; set; }
        public string ControlNumber { get; set; }
        public int CreatedById { get; set; }
        public int LastUserId { get; set; }
        public System.DateTime LastTime { get; set; }
        public System.DateTime CreatedOn { get; set; }
        public byte[] LastModified { get; set; }
        public int ReferralSourceId { get; set; }
        public int PayerId { get; set; }
        public Nullable<int> BranchId { get; set; }
        public Nullable<int> AdjusterId { get; set; }
        public int ClaimantId { get; set; }
        public int ClaimantNumberId { get; set; }
        public Nullable<bool> AdjusterIsReferral { get; set; }
        public int ProductTypeId { get; set; }
        public System.DateTime EnteredDate { get; set; }
        public System.DateTime ReceivedDate { get; set; }
        public System.DateTime DueDate { get; set; }
        public int AssignToId { get; set; }
        public int StatusId { get; set; }
        public Nullable<int> ReferralMethodId { get; set; }
        public string SpecialInstruction { get; set; }
        public Nullable<int> AttorneyId { get; set; }
        public Nullable<int> EmployerId { get; set; }
        public Nullable<int> CaseManagerId { get; set; }
        public Nullable<bool> IsCollectionSite { get; set; }
        public Nullable<bool> NoMedicationHistory { get; set; }
        public Nullable<int> PanelTypeId { get; set; }
        public virtual Adjuster Adjuster { get; set; }
        public virtual Attorney Attorney { get; set; }
        public virtual Branch Branch { get; set; }
        public virtual CaseManager CaseManager { get; set; }
        public virtual Claimant Claimant { get; set; }
        public virtual ClaimNumber ClaimNumber { get; set; }
        public virtual Employer Employer { get; set; }
        public virtual ICollection<Note> Notes { get; set; }
        public virtual PanelType PanelType { get; set; }
        public virtual Payer Payer { get; set; }
        public virtual ProductType ProductType { get; set; }
        public virtual ReferralSource ReferralSource { get; set; }
        public virtual User User { get; set; }
        public virtual ICollection<ReferralAttachment> ReferralAttachments { get; set; }
        public virtual ICollection<ReferralCollectionSite> ReferralCollectionSites { get; set; }
        public virtual ICollection<ReferralIcd> ReferralIcds { get; set; }
        public virtual ICollection<ReferralMedicationHistory> ReferralMedicationHistories { get; set; }
        public virtual ICollection<ReferralNpiNumber> ReferralNpiNumbers { get; set; }
        public virtual ICollection<ReferralReasonReferral> ReferralReasonReferrals { get; set; }
        public virtual ICollection<ReferralTask> ReferralTasks { get; set; }
    }
}
