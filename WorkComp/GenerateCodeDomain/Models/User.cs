using System;
using System.Collections.Generic;

namespace GenerateCodeDomain.Models
{
    public partial class User
    {
        public User()
        {
            this.GridConfigs = new List<GridConfig>();
            this.Notes = new List<Note>();
            this.Referrals = new List<Referral>();
            this.ReferralTasks = new List<ReferralTask>();
            this.TaskTemplates = new List<TaskTemplate>();
        }

        public int Id { get; set; }
        public int CreatedById { get; set; }
        public int LastUserId { get; set; }
        public Nullable<System.DateTime> LastTime { get; set; }
        public System.DateTime CreatedOn { get; set; }
        public byte[] LastModified { get; set; }
        public string FirstName { get; set; }
        public string LastName { get; set; }
        public string MiddleName { get; set; }
        public string UserName { get; set; }
        public string Password { get; set; }
        public string Email { get; set; }
        public string Phone { get; set; }
        public string Avatar { get; set; }
        public string Address { get; set; }
        public int UserRoleId { get; set; }
        public int StateId { get; set; }
        public int CityId { get; set; }
        public int ZipId { get; set; }
        public Nullable<bool> IsActive { get; set; }
        public virtual City City { get; set; }
        public virtual ICollection<GridConfig> GridConfigs { get; set; }
        public virtual ICollection<Note> Notes { get; set; }
        public virtual ICollection<Referral> Referrals { get; set; }
        public virtual ICollection<ReferralTask> ReferralTasks { get; set; }
        public virtual State State { get; set; }
        public virtual ICollection<TaskTemplate> TaskTemplates { get; set; }
        public virtual UserRole UserRole { get; set; }
        public virtual Zip Zip { get; set; }
    }
}
