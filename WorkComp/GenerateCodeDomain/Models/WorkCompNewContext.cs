using System.Data.Entity;
using System.Data.Entity.Infrastructure;
using GenerateCodeDomain.Models.Mapping;

namespace GenerateCodeDomain.Models
{
    public partial class WorkCompNewContext : DbContext
    {
        static WorkCompNewContext()
        {
            Database.SetInitializer<WorkCompNewContext>(null);
        }

        public WorkCompNewContext()
            : base("Name=WorkCompNewContext")
        {
        }

        public DbSet<Adjuster> Adjusters { get; set; }
        public DbSet<AssayCode> AssayCodes { get; set; }
        public DbSet<AssayCodeDescription> AssayCodeDescriptions { get; set; }
        public DbSet<Attorney> Attorneys { get; set; }
        public DbSet<Branch> Branches { get; set; }
        public DbSet<CaseManager> CaseManagers { get; set; }
        public DbSet<City> Cities { get; set; }
        public DbSet<Claimant> Claimants { get; set; }
        public DbSet<ClaimantLanguage> ClaimantLanguages { get; set; }
        public DbSet<ClaimNumber> ClaimNumbers { get; set; }
        public DbSet<CollectionSite> CollectionSites { get; set; }
        public DbSet<Configuration> Configurations { get; set; }
        public DbSet<DocumentType> DocumentTypes { get; set; }
        public DbSet<Drug> Drugs { get; set; }
        public DbSet<EmailTemplate> EmailTemplates { get; set; }
        public DbSet<Employer> Employers { get; set; }
        public DbSet<GridConfig> GridConfigs { get; set; }
        public DbSet<Icd> Icds { get; set; }
        public DbSet<IcdType> IcdTypes { get; set; }
        public DbSet<Note> Notes { get; set; }
        public DbSet<NpiNumber> NpiNumbers { get; set; }
        public DbSet<Panel> Panels { get; set; }
        public DbSet<PanelCode> PanelCodes { get; set; }
        public DbSet<PanelType> PanelTypes { get; set; }
        public DbSet<Payer> Payers { get; set; }
        public DbSet<ProductType> ProductTypes { get; set; }
        public DbSet<ReasonReferral> ReasonReferrals { get; set; }
        public DbSet<Referral> Referrals { get; set; }
        public DbSet<ReferralAttachment> ReferralAttachments { get; set; }
        public DbSet<ReferralCollectionSite> ReferralCollectionSites { get; set; }
        public DbSet<ReferralIcd> ReferralIcds { get; set; }
        public DbSet<ReferralMedicationHistory> ReferralMedicationHistories { get; set; }
        public DbSet<ReferralNpiNumber> ReferralNpiNumbers { get; set; }
        public DbSet<ReferralReasonReferral> ReferralReasonReferrals { get; set; }
        public DbSet<ReferralSource> ReferralSources { get; set; }
        public DbSet<ReferralTask> ReferralTasks { get; set; }
        public DbSet<ReferralType> ReferralTypes { get; set; }
        public DbSet<SampleTestingType> SampleTestingTypes { get; set; }
        public DbSet<SecurityOperation> SecurityOperations { get; set; }
        public DbSet<State> States { get; set; }
        public DbSet<sysdiagram> sysdiagrams { get; set; }
        public DbSet<TaskGroup> TaskGroups { get; set; }
        public DbSet<TaskGroupTaskTemplate> TaskGroupTaskTemplates { get; set; }
        public DbSet<TaskTemplate> TaskTemplates { get; set; }
        public DbSet<User> Users { get; set; }
        public DbSet<UserRole> UserRoles { get; set; }
        public DbSet<UserRoleFunction> UserRoleFunctions { get; set; }
        public DbSet<Zip> Zips { get; set; }

        protected override void OnModelCreating(DbModelBuilder modelBuilder)
        {
            modelBuilder.Configurations.Add(new AdjusterMap());
            modelBuilder.Configurations.Add(new AssayCodeMap());
            modelBuilder.Configurations.Add(new AssayCodeDescriptionMap());
            modelBuilder.Configurations.Add(new AttorneyMap());
            modelBuilder.Configurations.Add(new BranchMap());
            modelBuilder.Configurations.Add(new CaseManagerMap());
            modelBuilder.Configurations.Add(new CityMap());
            modelBuilder.Configurations.Add(new ClaimantMap());
            modelBuilder.Configurations.Add(new ClaimantLanguageMap());
            modelBuilder.Configurations.Add(new ClaimNumberMap());
            modelBuilder.Configurations.Add(new CollectionSiteMap());
            modelBuilder.Configurations.Add(new ConfigurationMap());
            modelBuilder.Configurations.Add(new DocumentTypeMap());
            modelBuilder.Configurations.Add(new DrugMap());
            modelBuilder.Configurations.Add(new EmailTemplateMap());
            modelBuilder.Configurations.Add(new EmployerMap());
            modelBuilder.Configurations.Add(new GridConfigMap());
            modelBuilder.Configurations.Add(new IcdMap());
            modelBuilder.Configurations.Add(new IcdTypeMap());
            modelBuilder.Configurations.Add(new NoteMap());
            modelBuilder.Configurations.Add(new NpiNumberMap());
            modelBuilder.Configurations.Add(new PanelMap());
            modelBuilder.Configurations.Add(new PanelCodeMap());
            modelBuilder.Configurations.Add(new PanelTypeMap());
            modelBuilder.Configurations.Add(new PayerMap());
            modelBuilder.Configurations.Add(new ProductTypeMap());
            modelBuilder.Configurations.Add(new ReasonReferralMap());
            modelBuilder.Configurations.Add(new ReferralMap());
            modelBuilder.Configurations.Add(new ReferralAttachmentMap());
            modelBuilder.Configurations.Add(new ReferralCollectionSiteMap());
            modelBuilder.Configurations.Add(new ReferralIcdMap());
            modelBuilder.Configurations.Add(new ReferralMedicationHistoryMap());
            modelBuilder.Configurations.Add(new ReferralNpiNumberMap());
            modelBuilder.Configurations.Add(new ReferralReasonReferralMap());
            modelBuilder.Configurations.Add(new ReferralSourceMap());
            modelBuilder.Configurations.Add(new ReferralTaskMap());
            modelBuilder.Configurations.Add(new ReferralTypeMap());
            modelBuilder.Configurations.Add(new SampleTestingTypeMap());
            modelBuilder.Configurations.Add(new SecurityOperationMap());
            modelBuilder.Configurations.Add(new StateMap());
            modelBuilder.Configurations.Add(new sysdiagramMap());
            modelBuilder.Configurations.Add(new TaskGroupMap());
            modelBuilder.Configurations.Add(new TaskGroupTaskTemplateMap());
            modelBuilder.Configurations.Add(new TaskTemplateMap());
            modelBuilder.Configurations.Add(new UserMap());
            modelBuilder.Configurations.Add(new UserRoleMap());
            modelBuilder.Configurations.Add(new UserRoleFunctionMap());
            modelBuilder.Configurations.Add(new ZipMap());
        }
    }
}
