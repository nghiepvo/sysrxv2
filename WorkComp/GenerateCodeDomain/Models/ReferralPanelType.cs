using System;
using System.Collections.Generic;

namespace GenerateCodeDomain.Models
{
    public partial class ReferralPanelType
    {
        public int Id { get; set; }
        public int CreatedById { get; set; }
        public int LastUserId { get; set; }
        public System.DateTime LastTime { get; set; }
        public System.DateTime CreatedOn { get; set; }
        public byte[] LastModified { get; set; }
        public Nullable<int> ReferralId { get; set; }
        public Nullable<int> PanelTypeId { get; set; }
        public virtual PanelType PanelType { get; set; }
        public virtual Referral Referral { get; set; }
    }
}
