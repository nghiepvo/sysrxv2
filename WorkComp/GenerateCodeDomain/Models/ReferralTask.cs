using System;
using System.Collections.Generic;

namespace GenerateCodeDomain.Models
{
    public partial class ReferralTask
    {
        public int Id { get; set; }
        public int CreatedById { get; set; }
        public int LastUserId { get; set; }
        public System.DateTime LastTime { get; set; }
        public System.DateTime CreatedOn { get; set; }
        public byte[] LastModified { get; set; }
        public string Title { get; set; }
        public string Description { get; set; }
        public Nullable<int> AssignToId { get; set; }
        public Nullable<System.DateTime> StartDate { get; set; }
        public Nullable<System.DateTime> DueDate { get; set; }
        public Nullable<int> TaskTypeId { get; set; }
        public int ReferralId { get; set; }
        public virtual Referral Referral { get; set; }
        public virtual User User { get; set; }
    }
}
