using System;
using System.Collections.Generic;

namespace GenerateCodeDomain.Models
{
    public partial class DocumentType
    {
        public DocumentType()
        {
            this.UserRoleFunctions = new List<UserRoleFunction>();
        }

        public int Id { get; set; }
        public string Name { get; set; }
        public int CreatedById { get; set; }
        public int LastUserId { get; set; }
        public System.DateTime LastTime { get; set; }
        public System.DateTime CreatedOn { get; set; }
        public byte[] LastModified { get; set; }
        public string Title { get; set; }
        public Nullable<int> Order { get; set; }
        public virtual ICollection<UserRoleFunction> UserRoleFunctions { get; set; }
    }
}
