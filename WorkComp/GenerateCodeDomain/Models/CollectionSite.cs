using System;
using System.Collections.Generic;

namespace GenerateCodeDomain.Models
{
    public partial class CollectionSite
    {
        public CollectionSite()
        {
            this.ReferralCollectionSites = new List<ReferralCollectionSite>();
        }

        public int Id { get; set; }
        public string Name { get; set; }
        public string Phone { get; set; }
        public string Fax { get; set; }
        public string Email { get; set; }
        public string CollectionHour { get; set; }
        public string LunchHour { get; set; }
        public string LocationIdentified { get; set; }
        public string CostInformation { get; set; }
        public string ContactName { get; set; }
        public string ContactEmail { get; set; }
        public string Address { get; set; }
        public int StateId { get; set; }
        public int CityId { get; set; }
        public int ZipId { get; set; }
        public Nullable<double> Lat { get; set; }
        public Nullable<double> Lng { get; set; }
        public Nullable<bool> Contracted { get; set; }
        public int CreatedById { get; set; }
        public int LastUserId { get; set; }
        public System.DateTime LastTime { get; set; }
        public System.DateTime CreatedOn { get; set; }
        public byte[] LastModified { get; set; }
        public virtual City City { get; set; }
        public virtual State State { get; set; }
        public virtual Zip Zip { get; set; }
        public virtual ICollection<ReferralCollectionSite> ReferralCollectionSites { get; set; }
    }
}
