using System;
using System.Collections.Generic;

namespace GenerateCodeDomain.Models
{
    public partial class CaseManager
    {
        public CaseManager()
        {
            this.Referrals = new List<Referral>();
        }

        public int Id { get; set; }
        public int ClaimNumberId { get; set; }
        public string Name { get; set; }
        public string Agency { get; set; }
        public string Phone { get; set; }
        public string Email { get; set; }
        public string Fax { get; set; }
        public string SpecialInstructions { get; set; }
        public string Address { get; set; }
        public int CityId { get; set; }
        public int StateId { get; set; }
        public int ZipId { get; set; }
        public int CreatedById { get; set; }
        public int LastUserId { get; set; }
        public System.DateTime LastTime { get; set; }
        public System.DateTime CreatedOn { get; set; }
        public byte[] LastModified { get; set; }
        public virtual City City { get; set; }
        public virtual ClaimNumber ClaimNumber { get; set; }
        public virtual State State { get; set; }
        public virtual Zip Zip { get; set; }
        public virtual ICollection<Referral> Referrals { get; set; }
    }
}
