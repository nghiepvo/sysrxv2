using System;
using System.Collections.Generic;

namespace GenerateCodeDomain.Models
{
    public partial class ReferralAttachment
    {
        public int Id { get; set; }
        public int ReferralId { get; set; }
        public int CreatedById { get; set; }
        public int LastUserId { get; set; }
        public System.DateTime LastTime { get; set; }
        public System.DateTime CreatedOn { get; set; }
        public byte[] LastModified { get; set; }
        public System.Guid RowGUID { get; set; }
        public int AttachedFileSize { get; set; }
        public string AttachedFileName { get; set; }
        public byte[] AttachedFileContent { get; set; }
        public virtual Referral Referral { get; set; }
    }
}
