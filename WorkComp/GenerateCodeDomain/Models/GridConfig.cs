using System;
using System.Collections.Generic;

namespace GenerateCodeDomain.Models
{
    public partial class GridConfig
    {
        public int Id { get; set; }
        public int DocumentTypeId { get; set; }
        public int UserId { get; set; }
        public string XmlText { get; set; }
        public string GridInternalName { get; set; }
        public int CreatedById { get; set; }
        public int LastUserId { get; set; }
        public System.DateTime LastTime { get; set; }
        public System.DateTime CreatedOn { get; set; }
        public byte[] LastModified { get; set; }
        public virtual User User { get; set; }
    }
}
