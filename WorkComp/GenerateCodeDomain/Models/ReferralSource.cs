using System;
using System.Collections.Generic;

namespace GenerateCodeDomain.Models
{
    public partial class ReferralSource
    {
        public ReferralSource()
        {
            this.Referrals = new List<Referral>();
        }

        public int Id { get; set; }
        public string FirstName { get; set; }
        public string LastName { get; set; }
        public string MiddleName { get; set; }
        public string Company { get; set; }
        public string Phone { get; set; }
        public string Extension { get; set; }
        public string Fax { get; set; }
        public string Email { get; set; }
        public string Address { get; set; }
        public int StateId { get; set; }
        public int CityId { get; set; }
        public int ZipId { get; set; }
        public int ReferralTypeId { get; set; }
        public Nullable<bool> IsAuthorization { get; set; }
        public Nullable<System.DateTime> AuthorizationFrom { get; set; }
        public Nullable<System.DateTime> AuthorizationTo { get; set; }
        public Nullable<int> NoAuthorizationType { get; set; }
        public Nullable<System.DateTime> OneTimeDate { get; set; }
        public int CreatedById { get; set; }
        public int LastUserId { get; set; }
        public System.DateTime LastTime { get; set; }
        public System.DateTime CreatedOn { get; set; }
        public byte[] LastModified { get; set; }
        public virtual City City { get; set; }
        public virtual ICollection<Referral> Referrals { get; set; }
        public virtual ReferralType ReferralType { get; set; }
        public virtual State State { get; set; }
        public virtual Zip Zip { get; set; }
    }
}
