using System;
using System.Collections.Generic;

namespace GenerateCodeDomain.Models
{
    public partial class Note
    {
        public Nullable<int> Id { get; set; }
        public Nullable<System.DateTime> DiaryDate { get; set; }
        public string Comment { get; set; }
        public int ReferralId { get; set; }
        public Nullable<int> NoteType { get; set; }
        public int AssignToId { get; set; }
        public int CreatedById { get; set; }
        public int LastUserId { get; set; }
        public System.DateTime LastTime { get; set; }
        public System.DateTime CreatedOn { get; set; }
        public byte[] LastModified { get; set; }
        public virtual Referral Referral { get; set; }
        public virtual User User { get; set; }
    }
}
