using System;
using System.Collections.Generic;

namespace GenerateCodeDomain.Models
{
    public partial class ClaimNumber
    {
        public ClaimNumber()
        {
            this.CaseManagers = new List<CaseManager>();
            this.Referrals = new List<Referral>();
        }

        public int Id { get; set; }
        public int ClaimantId { get; set; }
        public int AdjusterId { get; set; }
        public Nullable<int> BranchId { get; set; }
        public int PayerId { get; set; }
        public int EmployerId { get; set; }
        public System.DateTime DOI { get; set; }
        public string Name { get; set; }
        public int StateId { get; set; }
        public string SpecialInstructions { get; set; }
        public Nullable<System.DateTime> CloseDate { get; set; }
        public Nullable<System.DateTime> OpenDate { get; set; }
        public Nullable<System.DateTime> ReClosedDate { get; set; }
        public Nullable<System.DateTime> ReOpenDate { get; set; }
        public Nullable<System.DateTime> ReportDate { get; set; }
        public Nullable<short> Status { get; set; }
        public bool IsUserUpdate { get; set; }
        public int CreatedById { get; set; }
        public int LastUserId { get; set; }
        public System.DateTime LastTime { get; set; }
        public System.DateTime CreatedOn { get; set; }
        public byte[] LastModified { get; set; }
        public virtual Adjuster Adjuster { get; set; }
        public virtual Branch Branch { get; set; }
        public virtual ICollection<CaseManager> CaseManagers { get; set; }
        public virtual Claimant Claimant { get; set; }
        public virtual Employer Employer { get; set; }
        public virtual ICollection<Referral> Referrals { get; set; }
    }
}
