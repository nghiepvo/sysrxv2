using System.Data.Entity;
using System.Data.Entity.Infrastructure;
using GenerateCodeDomain.Models.Mapping;

namespace GenerateCodeDomain.Models
{
    public partial class WorkCompContext : DbContext
    {
        static WorkCompContext()
        {
            Database.SetInitializer<WorkCompContext>(null);
        }

        public WorkCompContext()
            : base("Name=WorkCompContext")
        {
        }

        public DbSet<City> Cities { get; set; }
        public DbSet<Function> Functions { get; set; }
        public DbSet<Icd> Icds { get; set; }
        public DbSet<IcdType> IcdTypes { get; set; }
        public DbSet<SecurityOperation> SecurityOperations { get; set; }
        public DbSet<sysdiagram> sysdiagrams { get; set; }
        public DbSet<User> Users { get; set; }
        public DbSet<UserRole> UserRoles { get; set; }
        public DbSet<UserRoleFunction> UserRoleFunctions { get; set; }

        protected override void OnModelCreating(DbModelBuilder modelBuilder)
        {
            modelBuilder.Configurations.Add(new CityMap());
            modelBuilder.Configurations.Add(new FunctionMap());
            modelBuilder.Configurations.Add(new IcdMap());
            modelBuilder.Configurations.Add(new IcdTypeMap());
            modelBuilder.Configurations.Add(new SecurityOperationMap());
            modelBuilder.Configurations.Add(new sysdiagramMap());
            modelBuilder.Configurations.Add(new UserMap());
            modelBuilder.Configurations.Add(new UserRoleMap());
            modelBuilder.Configurations.Add(new UserRoleFunctionMap());
        }
    }
}
