using System;
using System.Collections.Generic;

namespace GenerateCodeDomain.Models
{
    public partial class Drug
    {
        public Drug()
        {
            this.ReferralMedicationHistories = new List<ReferralMedicationHistory>();
        }

        public int Id { get; set; }
        public string Name { get; set; }
        public string Class { get; set; }
        public string Description { get; set; }
        public int CreatedById { get; set; }
        public int LastUserId { get; set; }
        public System.DateTime LastTime { get; set; }
        public System.DateTime CreatedOn { get; set; }
        public byte[] LastModified { get; set; }
        public virtual ICollection<ReferralMedicationHistory> ReferralMedicationHistories { get; set; }
    }
}
