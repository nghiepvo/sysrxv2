using System;
using System.Collections.Generic;

namespace GenerateCodeDomain.Models
{
    public partial class EmailTemplate
    {
        public int Id { get; set; }
        public string Title { get; set; }
        public string Subject { get; set; }
        public string Content { get; set; }
        public Nullable<int> Type { get; set; }
        public bool IsActived { get; set; }
        public int CreatedById { get; set; }
        public int LastUserId { get; set; }
        public System.DateTime LastTime { get; set; }
        public System.DateTime CreatedOn { get; set; }
        public byte[] LastModified { get; set; }
    }
}
