using System.ComponentModel.DataAnnotations.Schema;
using System.Data.Entity.ModelConfiguration;

namespace GenerateCodeDomain.Models.Mapping
{
    public class ReferralReasonReferralMap : EntityTypeConfiguration<ReferralReasonReferral>
    {
        public ReferralReasonReferralMap()
        {
            // Primary Key
            this.HasKey(t => t.Id);

            // Properties
            this.Property(t => t.LastModified)
                .IsRequired()
                .IsFixedLength()
                .HasMaxLength(8)
                .IsRowVersion();

            // Table & Column Mappings
            this.ToTable("ReferralReasonReferral");
            this.Property(t => t.Id).HasColumnName("Id");
            this.Property(t => t.ReferralId).HasColumnName("ReferralId");
            this.Property(t => t.ReasonReferralId).HasColumnName("ReasonReferralId");
            this.Property(t => t.CreatedById).HasColumnName("CreatedById");
            this.Property(t => t.LastUserId).HasColumnName("LastUserId");
            this.Property(t => t.LastTime).HasColumnName("LastTime");
            this.Property(t => t.CreatedOn).HasColumnName("CreatedOn");
            this.Property(t => t.LastModified).HasColumnName("LastModified");

            // Relationships
            this.HasOptional(t => t.ReasonReferral)
                .WithMany(t => t.ReferralReasonReferrals)
                .HasForeignKey(d => d.ReasonReferralId);
            this.HasOptional(t => t.Referral)
                .WithMany(t => t.ReferralReasonReferrals)
                .HasForeignKey(d => d.ReferralId);

        }
    }
}
