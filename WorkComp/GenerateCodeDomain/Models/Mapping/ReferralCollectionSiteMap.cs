using System.ComponentModel.DataAnnotations.Schema;
using System.Data.Entity.ModelConfiguration;

namespace GenerateCodeDomain.Models.Mapping
{
    public class ReferralCollectionSiteMap : EntityTypeConfiguration<ReferralCollectionSite>
    {
        public ReferralCollectionSiteMap()
        {
            // Primary Key
            this.HasKey(t => t.Id);

            // Properties
            this.Property(t => t.LastModified)
                .IsRequired()
                .IsFixedLength()
                .HasMaxLength(8)
                .IsRowVersion();

            this.Property(t => t.Phone)
                .HasMaxLength(50);

            this.Property(t => t.Fax)
                .HasMaxLength(50);

            this.Property(t => t.Address)
                .HasMaxLength(200);

            // Table & Column Mappings
            this.ToTable("ReferralCollectionSite");
            this.Property(t => t.Id).HasColumnName("Id");
            this.Property(t => t.CreatedById).HasColumnName("CreatedById");
            this.Property(t => t.LastUserId).HasColumnName("LastUserId");
            this.Property(t => t.LastTime).HasColumnName("LastTime");
            this.Property(t => t.CreatedOn).HasColumnName("CreatedOn");
            this.Property(t => t.LastModified).HasColumnName("LastModified");
            this.Property(t => t.CollectionSiteId).HasColumnName("CollectionSiteId");
            this.Property(t => t.ReferralId).HasColumnName("ReferralId");
            this.Property(t => t.SpecialInstructions).HasColumnName("SpecialInstructions");
            this.Property(t => t.CollectionDate).HasColumnName("CollectionDate");
            this.Property(t => t.Phone).HasColumnName("Phone");
            this.Property(t => t.Fax).HasColumnName("Fax");
            this.Property(t => t.Address).HasColumnName("Address");
            this.Property(t => t.IsRush).HasColumnName("IsRush");

            // Relationships
            this.HasOptional(t => t.CollectionSite)
                .WithMany(t => t.ReferralCollectionSites)
                .HasForeignKey(d => d.CollectionSiteId);
            this.HasRequired(t => t.Referral)
                .WithMany(t => t.ReferralCollectionSites)
                .HasForeignKey(d => d.ReferralId);

        }
    }
}
