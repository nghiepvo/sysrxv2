using System.ComponentModel.DataAnnotations.Schema;
using System.Data.Entity.ModelConfiguration;

namespace GenerateCodeDomain.Models.Mapping
{
    public class ReferralNpiNumberMap : EntityTypeConfiguration<ReferralNpiNumber>
    {
        public ReferralNpiNumberMap()
        {
            // Primary Key
            this.HasKey(t => t.Id);

            // Properties
            this.Property(t => t.LastModified)
                .IsRequired()
                .IsFixedLength()
                .HasMaxLength(8)
                .IsRowVersion();

            this.Property(t => t.Phone)
                .HasMaxLength(50);

            this.Property(t => t.Fax)
                .HasMaxLength(50);

            this.Property(t => t.Address)
                .HasMaxLength(200);

            this.Property(t => t.Email)
                .HasMaxLength(100);

            // Table & Column Mappings
            this.ToTable("ReferralNpiNumber");
            this.Property(t => t.Id).HasColumnName("Id");
            this.Property(t => t.CreatedById).HasColumnName("CreatedById");
            this.Property(t => t.LastUserId).HasColumnName("LastUserId");
            this.Property(t => t.LastTime).HasColumnName("LastTime");
            this.Property(t => t.CreatedOn).HasColumnName("CreatedOn");
            this.Property(t => t.LastModified).HasColumnName("LastModified");
            this.Property(t => t.NpiNumberId).HasColumnName("NpiNumberId");
            this.Property(t => t.ReferralId).HasColumnName("ReferralId");
            this.Property(t => t.NextMdVisitDate).HasColumnName("NextMdVisitDate");
            this.Property(t => t.SpecialHandling).HasColumnName("SpecialHandling");
            this.Property(t => t.Phone).HasColumnName("Phone");
            this.Property(t => t.Fax).HasColumnName("Fax");
            this.Property(t => t.Address).HasColumnName("Address");
            this.Property(t => t.Email).HasColumnName("Email");
            this.Property(t => t.IsRush).HasColumnName("IsRush");

            // Relationships
            this.HasOptional(t => t.NpiNumber)
                .WithMany(t => t.ReferralNpiNumbers)
                .HasForeignKey(d => d.NpiNumberId);
            this.HasRequired(t => t.Referral)
                .WithMany(t => t.ReferralNpiNumbers)
                .HasForeignKey(d => d.ReferralId);

        }
    }
}
