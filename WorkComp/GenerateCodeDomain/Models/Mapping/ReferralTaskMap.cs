using System.ComponentModel.DataAnnotations.Schema;
using System.Data.Entity.ModelConfiguration;

namespace GenerateCodeDomain.Models.Mapping
{
    public class ReferralTaskMap : EntityTypeConfiguration<ReferralTask>
    {
        public ReferralTaskMap()
        {
            // Primary Key
            this.HasKey(t => t.Id);

            // Properties
            this.Property(t => t.LastModified)
                .IsRequired()
                .IsFixedLength()
                .HasMaxLength(8)
                .IsRowVersion();

            this.Property(t => t.Title)
                .IsRequired()
                .HasMaxLength(200);

            this.Property(t => t.Description)
                .HasMaxLength(1000);

            // Table & Column Mappings
            this.ToTable("ReferralTask");
            this.Property(t => t.Id).HasColumnName("Id");
            this.Property(t => t.CreatedById).HasColumnName("CreatedById");
            this.Property(t => t.LastUserId).HasColumnName("LastUserId");
            this.Property(t => t.LastTime).HasColumnName("LastTime");
            this.Property(t => t.CreatedOn).HasColumnName("CreatedOn");
            this.Property(t => t.LastModified).HasColumnName("LastModified");
            this.Property(t => t.Title).HasColumnName("Title");
            this.Property(t => t.Description).HasColumnName("Description");
            this.Property(t => t.AssignToId).HasColumnName("AssignToId");
            this.Property(t => t.StartDate).HasColumnName("StartDate");
            this.Property(t => t.DueDate).HasColumnName("DueDate");
            this.Property(t => t.TaskTypeId).HasColumnName("TaskTypeId");
            this.Property(t => t.ReferralId).HasColumnName("ReferralId");

            // Relationships
            this.HasRequired(t => t.Referral)
                .WithMany(t => t.ReferralTasks)
                .HasForeignKey(d => d.ReferralId);
            this.HasOptional(t => t.User)
                .WithMany(t => t.ReferralTasks)
                .HasForeignKey(d => d.AssignToId);

        }
    }
}
