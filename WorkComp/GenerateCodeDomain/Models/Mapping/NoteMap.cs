using System.ComponentModel.DataAnnotations.Schema;
using System.Data.Entity.ModelConfiguration;

namespace GenerateCodeDomain.Models.Mapping
{
    public class NoteMap : EntityTypeConfiguration<Note>
    {
        public NoteMap()
        {
            // Primary Key
            this.HasKey(t => new { t.Comment, t.ReferralId, t.AssignToId, t.CreatedById, t.LastUserId, t.LastTime, t.CreatedOn, t.LastModified });

            // Properties
            this.Property(t => t.Comment)
                .IsRequired();

            this.Property(t => t.ReferralId)
                .HasDatabaseGeneratedOption(DatabaseGeneratedOption.None);

            this.Property(t => t.AssignToId)
                .HasDatabaseGeneratedOption(DatabaseGeneratedOption.None);

            this.Property(t => t.CreatedById)
                .HasDatabaseGeneratedOption(DatabaseGeneratedOption.None);

            this.Property(t => t.LastUserId)
                .HasDatabaseGeneratedOption(DatabaseGeneratedOption.None);

            this.Property(t => t.LastModified)
                .IsRequired()
                .IsFixedLength()
                .HasMaxLength(8)
                .IsRowVersion();

            // Table & Column Mappings
            this.ToTable("Note");
            this.Property(t => t.Id).HasColumnName("Id");
            this.Property(t => t.DiaryDate).HasColumnName("DiaryDate");
            this.Property(t => t.Comment).HasColumnName("Comment");
            this.Property(t => t.ReferralId).HasColumnName("ReferralId");
            this.Property(t => t.NoteType).HasColumnName("NoteType");
            this.Property(t => t.AssignToId).HasColumnName("AssignToId");
            this.Property(t => t.CreatedById).HasColumnName("CreatedById");
            this.Property(t => t.LastUserId).HasColumnName("LastUserId");
            this.Property(t => t.LastTime).HasColumnName("LastTime");
            this.Property(t => t.CreatedOn).HasColumnName("CreatedOn");
            this.Property(t => t.LastModified).HasColumnName("LastModified");

            // Relationships
            this.HasRequired(t => t.Referral)
                .WithMany(t => t.Notes)
                .HasForeignKey(d => d.ReferralId);
            this.HasRequired(t => t.User)
                .WithMany(t => t.Notes)
                .HasForeignKey(d => d.AssignToId);

        }
    }
}
