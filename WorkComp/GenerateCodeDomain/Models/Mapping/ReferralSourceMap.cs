using System.ComponentModel.DataAnnotations.Schema;
using System.Data.Entity.ModelConfiguration;

namespace GenerateCodeDomain.Models.Mapping
{
    public class ReferralSourceMap : EntityTypeConfiguration<ReferralSource>
    {
        public ReferralSourceMap()
        {
            // Primary Key
            this.HasKey(t => t.Id);

            // Properties
            this.Property(t => t.FirstName)
                .IsRequired()
                .HasMaxLength(50);

            this.Property(t => t.LastName)
                .IsRequired()
                .HasMaxLength(50);

            this.Property(t => t.MiddleName)
                .HasMaxLength(50);

            this.Property(t => t.Company)
                .HasMaxLength(500);

            this.Property(t => t.Phone)
                .IsRequired()
                .HasMaxLength(50);

            this.Property(t => t.Extension)
                .HasMaxLength(50);

            this.Property(t => t.Fax)
                .HasMaxLength(50);

            this.Property(t => t.Email)
                .HasMaxLength(50);

            this.Property(t => t.Address)
                .IsRequired()
                .HasMaxLength(100);

            this.Property(t => t.LastModified)
                .IsRequired()
                .IsFixedLength()
                .HasMaxLength(8)
                .IsRowVersion();

            // Table & Column Mappings
            this.ToTable("ReferralSource");
            this.Property(t => t.Id).HasColumnName("Id");
            this.Property(t => t.FirstName).HasColumnName("FirstName");
            this.Property(t => t.LastName).HasColumnName("LastName");
            this.Property(t => t.MiddleName).HasColumnName("MiddleName");
            this.Property(t => t.Company).HasColumnName("Company");
            this.Property(t => t.Phone).HasColumnName("Phone");
            this.Property(t => t.Extension).HasColumnName("Extension");
            this.Property(t => t.Fax).HasColumnName("Fax");
            this.Property(t => t.Email).HasColumnName("Email");
            this.Property(t => t.Address).HasColumnName("Address");
            this.Property(t => t.StateId).HasColumnName("StateId");
            this.Property(t => t.CityId).HasColumnName("CityId");
            this.Property(t => t.ZipId).HasColumnName("ZipId");
            this.Property(t => t.ReferralTypeId).HasColumnName("ReferralTypeId");
            this.Property(t => t.IsAuthorization).HasColumnName("IsAuthorization");
            this.Property(t => t.AuthorizationFrom).HasColumnName("AuthorizationFrom");
            this.Property(t => t.AuthorizationTo).HasColumnName("AuthorizationTo");
            this.Property(t => t.NoAuthorizationType).HasColumnName("NoAuthorizationType");
            this.Property(t => t.OneTimeDate).HasColumnName("OneTimeDate");
            this.Property(t => t.CreatedById).HasColumnName("CreatedById");
            this.Property(t => t.LastUserId).HasColumnName("LastUserId");
            this.Property(t => t.LastTime).HasColumnName("LastTime");
            this.Property(t => t.CreatedOn).HasColumnName("CreatedOn");
            this.Property(t => t.LastModified).HasColumnName("LastModified");

            // Relationships
            this.HasRequired(t => t.City)
                .WithMany(t => t.ReferralSources)
                .HasForeignKey(d => d.CityId);
            this.HasRequired(t => t.ReferralType)
                .WithMany(t => t.ReferralSources)
                .HasForeignKey(d => d.ReferralTypeId);
            this.HasRequired(t => t.State)
                .WithMany(t => t.ReferralSources)
                .HasForeignKey(d => d.StateId);
            this.HasRequired(t => t.Zip)
                .WithMany(t => t.ReferralSources)
                .HasForeignKey(d => d.ZipId);

        }
    }
}
