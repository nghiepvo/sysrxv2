using System.ComponentModel.DataAnnotations.Schema;
using System.Data.Entity.ModelConfiguration;

namespace GenerateCodeDomain.Models.Mapping
{
    public class AdjusterMap : EntityTypeConfiguration<Adjuster>
    {
        public AdjusterMap()
        {
            // Primary Key
            this.HasKey(t => t.Id);

            // Properties
            this.Property(t => t.FirstName)
                .IsRequired()
                .HasMaxLength(50);

            this.Property(t => t.LastName)
                .IsRequired()
                .HasMaxLength(50);

            this.Property(t => t.MiddleName)
                .HasMaxLength(50);

            this.Property(t => t.Phone)
                .IsRequired()
                .HasMaxLength(50);

            this.Property(t => t.Extension)
                .HasMaxLength(50);

            this.Property(t => t.Email)
                .IsRequired()
                .HasMaxLength(50);

            this.Property(t => t.Address)
                .IsRequired()
                .HasMaxLength(100);

            this.Property(t => t.LastModified)
                .IsRequired()
                .IsFixedLength()
                .HasMaxLength(8)
                .IsRowVersion();

            this.Property(t => t.ExternalId)
                .HasMaxLength(50);

            // Table & Column Mappings
            this.ToTable("Adjuster");
            this.Property(t => t.Id).HasColumnName("Id");
            this.Property(t => t.BranchId).HasColumnName("BranchId");
            this.Property(t => t.PayerId).HasColumnName("PayerId");
            this.Property(t => t.FirstName).HasColumnName("FirstName");
            this.Property(t => t.LastName).HasColumnName("LastName");
            this.Property(t => t.MiddleName).HasColumnName("MiddleName");
            this.Property(t => t.Phone).HasColumnName("Phone");
            this.Property(t => t.Extension).HasColumnName("Extension");
            this.Property(t => t.Email).HasColumnName("Email");
            this.Property(t => t.AssignedDate).HasColumnName("AssignedDate");
            this.Property(t => t.Address).HasColumnName("Address");
            this.Property(t => t.StateId).HasColumnName("StateId");
            this.Property(t => t.CityId).HasColumnName("CityId");
            this.Property(t => t.ZipId).HasColumnName("ZipId");
            this.Property(t => t.IsUserUpdate).HasColumnName("IsUserUpdate");
            this.Property(t => t.CreatedById).HasColumnName("CreatedById");
            this.Property(t => t.LastUserId).HasColumnName("LastUserId");
            this.Property(t => t.LastTime).HasColumnName("LastTime");
            this.Property(t => t.CreatedOn).HasColumnName("CreatedOn");
            this.Property(t => t.LastModified).HasColumnName("LastModified");
            this.Property(t => t.ExternalId).HasColumnName("ExternalId");

            // Relationships
            this.HasOptional(t => t.Branch)
                .WithMany(t => t.Adjusters)
                .HasForeignKey(d => d.BranchId);
            this.HasRequired(t => t.City)
                .WithMany(t => t.Adjusters)
                .HasForeignKey(d => d.CityId);
            this.HasRequired(t => t.Payer)
                .WithMany(t => t.Adjusters)
                .HasForeignKey(d => d.PayerId);
            this.HasRequired(t => t.State)
                .WithMany(t => t.Adjusters)
                .HasForeignKey(d => d.StateId);
            this.HasRequired(t => t.Zip)
                .WithMany(t => t.Adjusters)
                .HasForeignKey(d => d.ZipId);

        }
    }
}
