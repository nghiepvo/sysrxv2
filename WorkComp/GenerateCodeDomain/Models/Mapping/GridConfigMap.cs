using System.ComponentModel.DataAnnotations.Schema;
using System.Data.Entity.ModelConfiguration;

namespace GenerateCodeDomain.Models.Mapping
{
    public class GridConfigMap : EntityTypeConfiguration<GridConfig>
    {
        public GridConfigMap()
        {
            // Primary Key
            this.HasKey(t => t.Id);

            // Properties
            this.Property(t => t.XmlText)
                .IsRequired();

            this.Property(t => t.GridInternalName)
                .IsRequired()
                .HasMaxLength(255);

            this.Property(t => t.LastModified)
                .IsRequired()
                .IsFixedLength()
                .HasMaxLength(8)
                .IsRowVersion();

            // Table & Column Mappings
            this.ToTable("GridConfig");
            this.Property(t => t.Id).HasColumnName("Id");
            this.Property(t => t.DocumentTypeId).HasColumnName("DocumentTypeId");
            this.Property(t => t.UserId).HasColumnName("UserId");
            this.Property(t => t.XmlText).HasColumnName("XmlText");
            this.Property(t => t.GridInternalName).HasColumnName("GridInternalName");
            this.Property(t => t.CreatedById).HasColumnName("CreatedById");
            this.Property(t => t.LastUserId).HasColumnName("LastUserId");
            this.Property(t => t.LastTime).HasColumnName("LastTime");
            this.Property(t => t.CreatedOn).HasColumnName("CreatedOn");
            this.Property(t => t.LastModified).HasColumnName("LastModified");

            // Relationships
            this.HasRequired(t => t.User)
                .WithMany(t => t.GridConfigs)
                .HasForeignKey(d => d.UserId);

        }
    }
}
