using System.ComponentModel.DataAnnotations.Schema;
using System.Data.Entity.ModelConfiguration;

namespace GenerateCodeDomain.Models.Mapping
{
    public class AssayCodeMap : EntityTypeConfiguration<AssayCode>
    {
        public AssayCodeMap()
        {
            // Primary Key
            this.HasKey(t => t.Id);

            // Properties
            this.Property(t => t.Code)
                .IsRequired()
                .HasMaxLength(50);

            this.Property(t => t.LastModified)
                .IsRequired()
                .IsFixedLength()
                .HasMaxLength(8)
                .IsRowVersion();

            // Table & Column Mappings
            this.ToTable("AssayCode");
            this.Property(t => t.Id).HasColumnName("Id");
            this.Property(t => t.AssayCodeDescriptionId).HasColumnName("AssayCodeDescriptionId");
            this.Property(t => t.SampleTestingTypeId).HasColumnName("SampleTestingTypeId");
            this.Property(t => t.Code).HasColumnName("Code");
            this.Property(t => t.CreatedById).HasColumnName("CreatedById");
            this.Property(t => t.LastUserId).HasColumnName("LastUserId");
            this.Property(t => t.LastTime).HasColumnName("LastTime");
            this.Property(t => t.CreatedOn).HasColumnName("CreatedOn");
            this.Property(t => t.LastModified).HasColumnName("LastModified");

            // Relationships
            this.HasRequired(t => t.AssayCodeDescription)
                .WithMany(t => t.AssayCodes)
                .HasForeignKey(d => d.AssayCodeDescriptionId);
            this.HasRequired(t => t.SampleTestingType)
                .WithMany(t => t.AssayCodes)
                .HasForeignKey(d => d.SampleTestingTypeId);

        }
    }
}
