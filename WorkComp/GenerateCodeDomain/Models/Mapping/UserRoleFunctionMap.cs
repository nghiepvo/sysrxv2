using System.ComponentModel.DataAnnotations.Schema;
using System.Data.Entity.ModelConfiguration;

namespace GenerateCodeDomain.Models.Mapping
{
    public class UserRoleFunctionMap : EntityTypeConfiguration<UserRoleFunction>
    {
        public UserRoleFunctionMap()
        {
            // Primary Key
            this.HasKey(t => t.Id);

            // Properties
            this.Property(t => t.LastModified)
                .IsRequired()
                .IsFixedLength()
                .HasMaxLength(8)
                .IsRowVersion();

            // Table & Column Mappings
            this.ToTable("UserRoleFunction");
            this.Property(t => t.Id).HasColumnName("Id");
            this.Property(t => t.UserRoleId).HasColumnName("UserRoleId");
            this.Property(t => t.SecurityOperationId).HasColumnName("SecurityOperationId");
            this.Property(t => t.CreatedById).HasColumnName("CreatedById");
            this.Property(t => t.LastUserId).HasColumnName("LastUserId");
            this.Property(t => t.LastTime).HasColumnName("LastTime");
            this.Property(t => t.CreatedOn).HasColumnName("CreatedOn");
            this.Property(t => t.LastModified).HasColumnName("LastModified");
            this.Property(t => t.DocumentTypeId).HasColumnName("DocumentTypeId");

            // Relationships
            this.HasOptional(t => t.DocumentType)
                .WithMany(t => t.UserRoleFunctions)
                .HasForeignKey(d => d.DocumentTypeId);
            this.HasOptional(t => t.SecurityOperation)
                .WithMany(t => t.UserRoleFunctions)
                .HasForeignKey(d => d.SecurityOperationId);
            this.HasOptional(t => t.UserRole)
                .WithMany(t => t.UserRoleFunctions)
                .HasForeignKey(d => d.UserRoleId);

        }
    }
}
