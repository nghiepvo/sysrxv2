using System.ComponentModel.DataAnnotations.Schema;
using System.Data.Entity.ModelConfiguration;

namespace GenerateCodeDomain.Models.Mapping
{
    public class TaskGroupTaskTemplateMap : EntityTypeConfiguration<TaskGroupTaskTemplate>
    {
        public TaskGroupTaskTemplateMap()
        {
            // Primary Key
            this.HasKey(t => t.Id);

            // Properties
            this.Property(t => t.LastModified)
                .IsRequired()
                .IsFixedLength()
                .HasMaxLength(8)
                .IsRowVersion();

            // Table & Column Mappings
            this.ToTable("TaskGroupTaskTemplate");
            this.Property(t => t.Id).HasColumnName("Id");
            this.Property(t => t.TaskTemplateId).HasColumnName("TaskTemplateId");
            this.Property(t => t.TaskGroupId).HasColumnName("TaskGroupId");
            this.Property(t => t.CreatedById).HasColumnName("CreatedById");
            this.Property(t => t.LastUserId).HasColumnName("LastUserId");
            this.Property(t => t.LastTime).HasColumnName("LastTime");
            this.Property(t => t.CreatedOn).HasColumnName("CreatedOn");
            this.Property(t => t.LastModified).HasColumnName("LastModified");

            // Relationships
            this.HasOptional(t => t.TaskGroup)
                .WithMany(t => t.TaskGroupTaskTemplates)
                .HasForeignKey(d => d.TaskGroupId);
            this.HasOptional(t => t.TaskTemplate)
                .WithMany(t => t.TaskGroupTaskTemplates)
                .HasForeignKey(d => d.TaskTemplateId);

        }
    }
}
