using System.ComponentModel.DataAnnotations.Schema;
using System.Data.Entity.ModelConfiguration;

namespace GenerateCodeDomain.Models.Mapping
{
    public class ClaimNumberMap : EntityTypeConfiguration<ClaimNumber>
    {
        public ClaimNumberMap()
        {
            // Primary Key
            this.HasKey(t => t.Id);

            // Properties
            this.Property(t => t.Name)
                .HasMaxLength(50);

            this.Property(t => t.LastModified)
                .IsRequired()
                .IsFixedLength()
                .HasMaxLength(8)
                .IsRowVersion();

            // Table & Column Mappings
            this.ToTable("ClaimNumber");
            this.Property(t => t.Id).HasColumnName("Id");
            this.Property(t => t.ClaimantId).HasColumnName("ClaimantId");
            this.Property(t => t.AdjusterId).HasColumnName("AdjusterId");
            this.Property(t => t.BranchId).HasColumnName("BranchId");
            this.Property(t => t.PayerId).HasColumnName("PayerId");
            this.Property(t => t.EmployerId).HasColumnName("EmployerId");
            this.Property(t => t.DOI).HasColumnName("DOI");
            this.Property(t => t.Name).HasColumnName("Name");
            this.Property(t => t.StateId).HasColumnName("StateId");
            this.Property(t => t.SpecialInstructions).HasColumnName("SpecialInstructions");
            this.Property(t => t.CloseDate).HasColumnName("CloseDate");
            this.Property(t => t.OpenDate).HasColumnName("OpenDate");
            this.Property(t => t.ReClosedDate).HasColumnName("ReClosedDate");
            this.Property(t => t.ReOpenDate).HasColumnName("ReOpenDate");
            this.Property(t => t.ReportDate).HasColumnName("ReportDate");
            this.Property(t => t.Status).HasColumnName("Status");
            this.Property(t => t.IsUserUpdate).HasColumnName("IsUserUpdate");
            this.Property(t => t.CreatedById).HasColumnName("CreatedById");
            this.Property(t => t.LastUserId).HasColumnName("LastUserId");
            this.Property(t => t.LastTime).HasColumnName("LastTime");
            this.Property(t => t.CreatedOn).HasColumnName("CreatedOn");
            this.Property(t => t.LastModified).HasColumnName("LastModified");

            // Relationships
            this.HasRequired(t => t.Adjuster)
                .WithMany(t => t.ClaimNumbers)
                .HasForeignKey(d => d.AdjusterId);
            this.HasOptional(t => t.Branch)
                .WithMany(t => t.ClaimNumbers)
                .HasForeignKey(d => d.BranchId);
            this.HasRequired(t => t.Claimant)
                .WithMany(t => t.ClaimNumbers)
                .HasForeignKey(d => d.ClaimantId);
            this.HasRequired(t => t.Employer)
                .WithMany(t => t.ClaimNumbers)
                .HasForeignKey(d => d.EmployerId);

        }
    }
}
