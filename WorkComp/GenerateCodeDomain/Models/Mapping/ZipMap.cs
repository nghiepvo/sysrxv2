using System.ComponentModel.DataAnnotations.Schema;
using System.Data.Entity.ModelConfiguration;

namespace GenerateCodeDomain.Models.Mapping
{
    public class ZipMap : EntityTypeConfiguration<Zip>
    {
        public ZipMap()
        {
            // Primary Key
            this.HasKey(t => t.Id);

            // Properties
            this.Property(t => t.Name)
                .HasMaxLength(10);

            this.Property(t => t.Extention)
                .HasMaxLength(10);

            this.Property(t => t.LastModified)
                .IsRequired()
                .IsFixedLength()
                .HasMaxLength(8)
                .IsRowVersion();

            // Table & Column Mappings
            this.ToTable("Zip");
            this.Property(t => t.Id).HasColumnName("Id");
            this.Property(t => t.Name).HasColumnName("Name");
            this.Property(t => t.Extention).HasColumnName("Extention");
            this.Property(t => t.CityId).HasColumnName("CityId");
            this.Property(t => t.CreatedById).HasColumnName("CreatedById");
            this.Property(t => t.LastUserId).HasColumnName("LastUserId");
            this.Property(t => t.LastTime).HasColumnName("LastTime");
            this.Property(t => t.CreatedOn).HasColumnName("CreatedOn");
            this.Property(t => t.LastModified).HasColumnName("LastModified");

            // Relationships
            this.HasOptional(t => t.City)
                .WithMany(t => t.Zips)
                .HasForeignKey(d => d.CityId);

        }
    }
}
