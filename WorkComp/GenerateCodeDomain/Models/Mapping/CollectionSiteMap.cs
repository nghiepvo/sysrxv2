using System.ComponentModel.DataAnnotations.Schema;
using System.Data.Entity.ModelConfiguration;

namespace GenerateCodeDomain.Models.Mapping
{
    public class CollectionSiteMap : EntityTypeConfiguration<CollectionSite>
    {
        public CollectionSiteMap()
        {
            // Primary Key
            this.HasKey(t => t.Id);

            // Properties
            this.Property(t => t.Name)
                .IsRequired()
                .HasMaxLength(500);

            this.Property(t => t.Phone)
                .HasMaxLength(50);

            this.Property(t => t.Fax)
                .HasMaxLength(50);

            this.Property(t => t.Email)
                .HasMaxLength(100);

            this.Property(t => t.CollectionHour)
                .HasMaxLength(500);

            this.Property(t => t.LunchHour)
                .HasMaxLength(500);

            this.Property(t => t.LocationIdentified)
                .HasMaxLength(500);

            this.Property(t => t.ContactName)
                .HasMaxLength(500);

            this.Property(t => t.ContactEmail)
                .HasMaxLength(50);

            this.Property(t => t.Address)
                .IsRequired()
                .HasMaxLength(100);

            this.Property(t => t.LastModified)
                .IsRequired()
                .IsFixedLength()
                .HasMaxLength(8)
                .IsRowVersion();

            // Table & Column Mappings
            this.ToTable("CollectionSite");
            this.Property(t => t.Id).HasColumnName("Id");
            this.Property(t => t.Name).HasColumnName("Name");
            this.Property(t => t.Phone).HasColumnName("Phone");
            this.Property(t => t.Fax).HasColumnName("Fax");
            this.Property(t => t.Email).HasColumnName("Email");
            this.Property(t => t.CollectionHour).HasColumnName("CollectionHour");
            this.Property(t => t.LunchHour).HasColumnName("LunchHour");
            this.Property(t => t.LocationIdentified).HasColumnName("LocationIdentified");
            this.Property(t => t.CostInformation).HasColumnName("CostInformation");
            this.Property(t => t.ContactName).HasColumnName("ContactName");
            this.Property(t => t.ContactEmail).HasColumnName("ContactEmail");
            this.Property(t => t.Address).HasColumnName("Address");
            this.Property(t => t.StateId).HasColumnName("StateId");
            this.Property(t => t.CityId).HasColumnName("CityId");
            this.Property(t => t.ZipId).HasColumnName("ZipId");
            this.Property(t => t.Lat).HasColumnName("Lat");
            this.Property(t => t.Lng).HasColumnName("Lng");
            this.Property(t => t.Contracted).HasColumnName("Contracted");
            this.Property(t => t.CreatedById).HasColumnName("CreatedById");
            this.Property(t => t.LastUserId).HasColumnName("LastUserId");
            this.Property(t => t.LastTime).HasColumnName("LastTime");
            this.Property(t => t.CreatedOn).HasColumnName("CreatedOn");
            this.Property(t => t.LastModified).HasColumnName("LastModified");

            // Relationships
            this.HasRequired(t => t.City)
                .WithMany(t => t.CollectionSites)
                .HasForeignKey(d => d.CityId);
            this.HasRequired(t => t.State)
                .WithMany(t => t.CollectionSites)
                .HasForeignKey(d => d.StateId);
            this.HasRequired(t => t.Zip)
                .WithMany(t => t.CollectionSites)
                .HasForeignKey(d => d.ZipId);

        }
    }
}
