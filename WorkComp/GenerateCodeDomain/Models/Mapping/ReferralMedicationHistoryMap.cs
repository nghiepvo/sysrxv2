using System.ComponentModel.DataAnnotations.Schema;
using System.Data.Entity.ModelConfiguration;

namespace GenerateCodeDomain.Models.Mapping
{
    public class ReferralMedicationHistoryMap : EntityTypeConfiguration<ReferralMedicationHistory>
    {
        public ReferralMedicationHistoryMap()
        {
            // Primary Key
            this.HasKey(t => t.Id);

            // Properties
            this.Property(t => t.LastModified)
                .IsRequired()
                .IsFixedLength()
                .HasMaxLength(8)
                .IsRowVersion();

            this.Property(t => t.Dosage)
                .HasMaxLength(50);

            // Table & Column Mappings
            this.ToTable("ReferralMedicationHistory");
            this.Property(t => t.Id).HasColumnName("Id");
            this.Property(t => t.CreatedById).HasColumnName("CreatedById");
            this.Property(t => t.LastUserId).HasColumnName("LastUserId");
            this.Property(t => t.LastTime).HasColumnName("LastTime");
            this.Property(t => t.CreatedOn).HasColumnName("CreatedOn");
            this.Property(t => t.LastModified).HasColumnName("LastModified");
            this.Property(t => t.DrugId).HasColumnName("DrugId");
            this.Property(t => t.ReferralId).HasColumnName("ReferralId");
            this.Property(t => t.DaysSupply).HasColumnName("DaysSupply");
            this.Property(t => t.Dosage).HasColumnName("Dosage");
            this.Property(t => t.DosageUnit).HasColumnName("DosageUnit");
            this.Property(t => t.ProvidedBy).HasColumnName("ProvidedBy");
            this.Property(t => t.FillDate).HasColumnName("FillDate");

            // Relationships
            this.HasRequired(t => t.Drug)
                .WithMany(t => t.ReferralMedicationHistories)
                .HasForeignKey(d => d.DrugId);
            this.HasRequired(t => t.Referral)
                .WithMany(t => t.ReferralMedicationHistories)
                .HasForeignKey(d => d.ReferralId);

        }
    }
}
