using System.ComponentModel.DataAnnotations.Schema;
using System.Data.Entity.ModelConfiguration;

namespace GenerateCodeDomain.Models.Mapping
{
    public class ReferralIcdMap : EntityTypeConfiguration<ReferralIcd>
    {
        public ReferralIcdMap()
        {
            // Primary Key
            this.HasKey(t => t.Id);

            // Properties
            this.Property(t => t.LastModified)
                .IsRequired()
                .IsFixedLength()
                .HasMaxLength(8)
                .IsRowVersion();

            // Table & Column Mappings
            this.ToTable("ReferralIcd");
            this.Property(t => t.Id).HasColumnName("Id");
            this.Property(t => t.CreatedById).HasColumnName("CreatedById");
            this.Property(t => t.LastUserId).HasColumnName("LastUserId");
            this.Property(t => t.LastTime).HasColumnName("LastTime");
            this.Property(t => t.CreatedOn).HasColumnName("CreatedOn");
            this.Property(t => t.LastModified).HasColumnName("LastModified");
            this.Property(t => t.ReferralId).HasColumnName("ReferralId");
            this.Property(t => t.IcdId).HasColumnName("IcdId");

            // Relationships
            this.HasOptional(t => t.Icd)
                .WithMany(t => t.ReferralIcds)
                .HasForeignKey(d => d.IcdId);
            this.HasOptional(t => t.Referral)
                .WithMany(t => t.ReferralIcds)
                .HasForeignKey(d => d.ReferralId);

        }
    }
}
