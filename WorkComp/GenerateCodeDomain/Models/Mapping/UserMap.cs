using System.ComponentModel.DataAnnotations.Schema;
using System.Data.Entity.ModelConfiguration;

namespace GenerateCodeDomain.Models.Mapping
{
    public class UserMap : EntityTypeConfiguration<User>
    {
        public UserMap()
        {
            // Primary Key
            this.HasKey(t => t.Id);

            // Properties
            this.Property(t => t.LastModified)
                .IsRequired()
                .IsFixedLength()
                .HasMaxLength(8)
                .IsRowVersion();

            this.Property(t => t.FirstName)
                .HasMaxLength(50);

            this.Property(t => t.LastName)
                .HasMaxLength(50);

            this.Property(t => t.MiddleName)
                .HasMaxLength(50);

            this.Property(t => t.UserName)
                .IsRequired()
                .HasMaxLength(100);

            this.Property(t => t.Password)
                .IsRequired()
                .HasMaxLength(100);

            this.Property(t => t.Email)
                .IsRequired()
                .HasMaxLength(200);

            this.Property(t => t.Phone)
                .HasMaxLength(50);

            this.Property(t => t.Avatar)
                .HasMaxLength(50);

            this.Property(t => t.Address)
                .HasMaxLength(1000);

            // Table & Column Mappings
            this.ToTable("User");
            this.Property(t => t.Id).HasColumnName("Id");
            this.Property(t => t.CreatedById).HasColumnName("CreatedById");
            this.Property(t => t.LastUserId).HasColumnName("LastUserId");
            this.Property(t => t.LastTime).HasColumnName("LastTime");
            this.Property(t => t.CreatedOn).HasColumnName("CreatedOn");
            this.Property(t => t.LastModified).HasColumnName("LastModified");
            this.Property(t => t.FirstName).HasColumnName("FirstName");
            this.Property(t => t.LastName).HasColumnName("LastName");
            this.Property(t => t.MiddleName).HasColumnName("MiddleName");
            this.Property(t => t.UserName).HasColumnName("UserName");
            this.Property(t => t.Password).HasColumnName("Password");
            this.Property(t => t.Email).HasColumnName("Email");
            this.Property(t => t.Phone).HasColumnName("Phone");
            this.Property(t => t.Avatar).HasColumnName("Avatar");
            this.Property(t => t.Address).HasColumnName("Address");
            this.Property(t => t.UserRoleId).HasColumnName("UserRoleId");
            this.Property(t => t.StateId).HasColumnName("StateId");
            this.Property(t => t.CityId).HasColumnName("CityId");
            this.Property(t => t.ZipId).HasColumnName("ZipId");
            this.Property(t => t.IsActive).HasColumnName("IsActive");

            // Relationships
            this.HasRequired(t => t.City)
                .WithMany(t => t.Users)
                .HasForeignKey(d => d.CityId);
            this.HasRequired(t => t.State)
                .WithMany(t => t.Users)
                .HasForeignKey(d => d.StateId);
            this.HasRequired(t => t.UserRole)
                .WithMany(t => t.Users)
                .HasForeignKey(d => d.UserRoleId);
            this.HasRequired(t => t.Zip)
                .WithMany(t => t.Users)
                .HasForeignKey(d => d.ZipId);

        }
    }
}
