using System.ComponentModel.DataAnnotations.Schema;
using System.Data.Entity.ModelConfiguration;

namespace GenerateCodeDomain.Models.Mapping
{
    public class ReferralMap : EntityTypeConfiguration<Referral>
    {
        public ReferralMap()
        {
            // Primary Key
            this.HasKey(t => t.Id);

            // Properties
            this.Property(t => t.ControlNumber)
                .IsRequired()
                .HasMaxLength(50);

            this.Property(t => t.LastModified)
                .IsRequired()
                .IsFixedLength()
                .HasMaxLength(8)
                .IsRowVersion();

            // Table & Column Mappings
            this.ToTable("Referral");
            this.Property(t => t.Id).HasColumnName("Id");
            this.Property(t => t.ControlNumber).HasColumnName("ControlNumber");
            this.Property(t => t.CreatedById).HasColumnName("CreatedById");
            this.Property(t => t.LastUserId).HasColumnName("LastUserId");
            this.Property(t => t.LastTime).HasColumnName("LastTime");
            this.Property(t => t.CreatedOn).HasColumnName("CreatedOn");
            this.Property(t => t.LastModified).HasColumnName("LastModified");
            this.Property(t => t.ReferralSourceId).HasColumnName("ReferralSourceId");
            this.Property(t => t.PayerId).HasColumnName("PayerId");
            this.Property(t => t.BranchId).HasColumnName("BranchId");
            this.Property(t => t.AdjusterId).HasColumnName("AdjusterId");
            this.Property(t => t.ClaimantId).HasColumnName("ClaimantId");
            this.Property(t => t.ClaimantNumberId).HasColumnName("ClaimantNumberId");
            this.Property(t => t.AdjusterIsReferral).HasColumnName("AdjusterIsReferral");
            this.Property(t => t.ProductTypeId).HasColumnName("ProductTypeId");
            this.Property(t => t.EnteredDate).HasColumnName("EnteredDate");
            this.Property(t => t.ReceivedDate).HasColumnName("ReceivedDate");
            this.Property(t => t.DueDate).HasColumnName("DueDate");
            this.Property(t => t.AssignToId).HasColumnName("AssignToId");
            this.Property(t => t.StatusId).HasColumnName("StatusId");
            this.Property(t => t.ReferralMethodId).HasColumnName("ReferralMethodId");
            this.Property(t => t.SpecialInstruction).HasColumnName("SpecialInstruction");
            this.Property(t => t.AttorneyId).HasColumnName("AttorneyId");
            this.Property(t => t.EmployerId).HasColumnName("EmployerId");
            this.Property(t => t.CaseManagerId).HasColumnName("CaseManagerId");
            this.Property(t => t.IsCollectionSite).HasColumnName("IsCollectionSite");
            this.Property(t => t.NoMedicationHistory).HasColumnName("NoMedicationHistory");
            this.Property(t => t.PanelTypeId).HasColumnName("PanelTypeId");

            // Relationships
            this.HasOptional(t => t.Adjuster)
                .WithMany(t => t.Referrals)
                .HasForeignKey(d => d.AdjusterId);
            this.HasOptional(t => t.Attorney)
                .WithMany(t => t.Referrals)
                .HasForeignKey(d => d.AttorneyId);
            this.HasOptional(t => t.Branch)
                .WithMany(t => t.Referrals)
                .HasForeignKey(d => d.BranchId);
            this.HasOptional(t => t.CaseManager)
                .WithMany(t => t.Referrals)
                .HasForeignKey(d => d.CaseManagerId);
            this.HasRequired(t => t.Claimant)
                .WithMany(t => t.Referrals)
                .HasForeignKey(d => d.ClaimantId);
            this.HasRequired(t => t.ClaimNumber)
                .WithMany(t => t.Referrals)
                .HasForeignKey(d => d.ClaimantNumberId);
            this.HasOptional(t => t.Employer)
                .WithMany(t => t.Referrals)
                .HasForeignKey(d => d.EmployerId);
            this.HasOptional(t => t.PanelType)
                .WithMany(t => t.Referrals)
                .HasForeignKey(d => d.PanelTypeId);
            this.HasRequired(t => t.Payer)
                .WithMany(t => t.Referrals)
                .HasForeignKey(d => d.PayerId);
            this.HasRequired(t => t.ProductType)
                .WithMany(t => t.Referrals)
                .HasForeignKey(d => d.ProductTypeId);
            this.HasRequired(t => t.ReferralSource)
                .WithMany(t => t.Referrals)
                .HasForeignKey(d => d.ReferralSourceId);
            this.HasRequired(t => t.User)
                .WithMany(t => t.Referrals)
                .HasForeignKey(d => d.AssignToId);

        }
    }
}
