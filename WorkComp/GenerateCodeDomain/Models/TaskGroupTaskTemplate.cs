using System;
using System.Collections.Generic;

namespace GenerateCodeDomain.Models
{
    public partial class TaskGroupTaskTemplate
    {
        public int Id { get; set; }
        public Nullable<int> TaskTemplateId { get; set; }
        public Nullable<int> TaskGroupId { get; set; }
        public int CreatedById { get; set; }
        public int LastUserId { get; set; }
        public System.DateTime LastTime { get; set; }
        public System.DateTime CreatedOn { get; set; }
        public byte[] LastModified { get; set; }
        public virtual TaskGroup TaskGroup { get; set; }
        public virtual TaskTemplate TaskTemplate { get; set; }
    }
}
