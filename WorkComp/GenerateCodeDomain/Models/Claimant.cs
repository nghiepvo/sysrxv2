using System;
using System.Collections.Generic;

namespace GenerateCodeDomain.Models
{
    public partial class Claimant
    {
        public Claimant()
        {
            this.ClaimNumbers = new List<ClaimNumber>();
            this.Referrals = new List<Referral>();
        }

        public int Id { get; set; }
        public string PayerPatientId { get; set; }
        public string FirstName { get; set; }
        public string LastName { get; set; }
        public string MiddleName { get; set; }
        public string Suffix { get; set; }
        public System.DateTime Birthday { get; set; }
        public string Gender { get; set; }
        public string Email { get; set; }
        public string PatientAbbr { get; set; }
        public string Ssn { get; set; }
        public string WorkPhone { get; set; }
        public string WorkPhoneExtension { get; set; }
        public string HomePhone { get; set; }
        public string HomePhoneExtension { get; set; }
        public string CellPhone { get; set; }
        public string ContactPhone { get; set; }
        public string BestContactNumber { get; set; }
        public Nullable<int> ClaimantLanguageId { get; set; }
        public Nullable<System.DateTime> ExpirationDate { get; set; }
        public string Address1 { get; set; }
        public string Address2 { get; set; }
        public int CityId { get; set; }
        public int StateId { get; set; }
        public int ZipId { get; set; }
        public Nullable<double> Lat { get; set; }
        public Nullable<double> Lng { get; set; }
        public bool IsUserUpdate { get; set; }
        public int CreatedById { get; set; }
        public int LastUserId { get; set; }
        public System.DateTime LastTime { get; set; }
        public System.DateTime CreatedOn { get; set; }
        public byte[] LastModified { get; set; }
        public virtual City City { get; set; }
        public virtual ClaimantLanguage ClaimantLanguage { get; set; }
        public virtual State State { get; set; }
        public virtual Zip Zip { get; set; }
        public virtual ICollection<ClaimNumber> ClaimNumbers { get; set; }
        public virtual ICollection<Referral> Referrals { get; set; }
    }
}
