using System;
using System.Collections.Generic;

namespace GenerateCodeDomain.Models
{
    public partial class Payer
    {
        public Payer()
        {
            this.Adjusters = new List<Adjuster>();
            this.Branches = new List<Branch>();
            this.PanelTypes = new List<PanelType>();
            this.ProductTypes = new List<ProductType>();
            this.Referrals = new List<Referral>();
        }

        public int Id { get; set; }
        public string Name { get; set; }
        public string ControlNumberPrefix { get; set; }
        public string SpecialInstructions { get; set; }
        public string Address { get; set; }
        public Nullable<int> StateId { get; set; }
        public Nullable<int> CityId { get; set; }
        public Nullable<int> ZipId { get; set; }
        public int CreatedById { get; set; }
        public int LastUserId { get; set; }
        public System.DateTime LastTime { get; set; }
        public System.DateTime CreatedOn { get; set; }
        public byte[] LastModified { get; set; }
        public virtual ICollection<Adjuster> Adjusters { get; set; }
        public virtual ICollection<Branch> Branches { get; set; }
        public virtual City City { get; set; }
        public virtual ICollection<PanelType> PanelTypes { get; set; }
        public virtual State State { get; set; }
        public virtual Zip Zip { get; set; }
        public virtual ICollection<ProductType> ProductTypes { get; set; }
        public virtual ICollection<Referral> Referrals { get; set; }
    }
}
