using System;
using System.Collections.Generic;

namespace GenerateCodeDomain.Models
{
    public partial class ReferralNpiNumber
    {
        public int Id { get; set; }
        public int CreatedById { get; set; }
        public int LastUserId { get; set; }
        public System.DateTime LastTime { get; set; }
        public System.DateTime CreatedOn { get; set; }
        public byte[] LastModified { get; set; }
        public Nullable<int> NpiNumberId { get; set; }
        public int ReferralId { get; set; }
        public Nullable<System.DateTime> NextMdVisitDate { get; set; }
        public string SpecialHandling { get; set; }
        public string Phone { get; set; }
        public string Fax { get; set; }
        public string Address { get; set; }
        public string Email { get; set; }
        public Nullable<bool> IsRush { get; set; }
        public virtual NpiNumber NpiNumber { get; set; }
        public virtual Referral Referral { get; set; }
    }
}
