using System;
using System.Collections.Generic;

namespace GenerateCodeDomain.Models
{
    public partial class Branch
    {
        public Branch()
        {
            this.Adjusters = new List<Adjuster>();
            this.ClaimNumbers = new List<ClaimNumber>();
            this.Referrals = new List<Referral>();
        }

        public int Id { get; set; }
        public string Name { get; set; }
        public string ManagerName { get; set; }
        public string Fax { get; set; }
        public string Phone { get; set; }
        public string Address1 { get; set; }
        public string Address2 { get; set; }
        public int PayerId { get; set; }
        public int StateId { get; set; }
        public int CityId { get; set; }
        public int ZipId { get; set; }
        public bool IsUserUpdate { get; set; }
        public int CreatedById { get; set; }
        public int LastUserId { get; set; }
        public System.DateTime LastTime { get; set; }
        public System.DateTime CreatedOn { get; set; }
        public byte[] LastModified { get; set; }
        public virtual ICollection<Adjuster> Adjusters { get; set; }
        public virtual City City { get; set; }
        public virtual Payer Payer { get; set; }
        public virtual State State { get; set; }
        public virtual Zip Zip { get; set; }
        public virtual ICollection<ClaimNumber> ClaimNumbers { get; set; }
        public virtual ICollection<Referral> Referrals { get; set; }
    }
}
