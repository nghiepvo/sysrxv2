using System;
using System.Collections.Generic;

namespace GenerateCodeDomain.Models
{
    public partial class ReasonReferral
    {
        public ReasonReferral()
        {
            this.ReasonReferral1 = new List<ReasonReferral>();
            this.ReferralReasonReferrals = new List<ReferralReasonReferral>();
        }

        public int Id { get; set; }
        public string Name { get; set; }
        public Nullable<int> ParentId { get; set; }
        public int CreatedById { get; set; }
        public int LastUserId { get; set; }
        public System.DateTime LastTime { get; set; }
        public System.DateTime CreatedOn { get; set; }
        public byte[] LastModified { get; set; }
        public virtual ICollection<ReasonReferral> ReasonReferral1 { get; set; }
        public virtual ReasonReferral ReasonReferral2 { get; set; }
        public virtual ICollection<ReferralReasonReferral> ReferralReasonReferrals { get; set; }
    }
}
