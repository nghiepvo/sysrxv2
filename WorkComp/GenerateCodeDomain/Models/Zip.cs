using System;
using System.Collections.Generic;

namespace GenerateCodeDomain.Models
{
    public partial class Zip
    {
        public Zip()
        {
            this.Adjusters = new List<Adjuster>();
            this.Attorneys = new List<Attorney>();
            this.Branches = new List<Branch>();
            this.CaseManagers = new List<CaseManager>();
            this.Claimants = new List<Claimant>();
            this.CollectionSites = new List<CollectionSite>();
            this.Employers = new List<Employer>();
            this.NpiNumbers = new List<NpiNumber>();
            this.Payers = new List<Payer>();
            this.ReferralSources = new List<ReferralSource>();
            this.Users = new List<User>();
        }

        public int Id { get; set; }
        public string Name { get; set; }
        public string Extention { get; set; }
        public Nullable<int> CityId { get; set; }
        public int CreatedById { get; set; }
        public int LastUserId { get; set; }
        public System.DateTime LastTime { get; set; }
        public System.DateTime CreatedOn { get; set; }
        public byte[] LastModified { get; set; }
        public virtual ICollection<Adjuster> Adjusters { get; set; }
        public virtual ICollection<Attorney> Attorneys { get; set; }
        public virtual ICollection<Branch> Branches { get; set; }
        public virtual ICollection<CaseManager> CaseManagers { get; set; }
        public virtual City City { get; set; }
        public virtual ICollection<Claimant> Claimants { get; set; }
        public virtual ICollection<CollectionSite> CollectionSites { get; set; }
        public virtual ICollection<Employer> Employers { get; set; }
        public virtual ICollection<NpiNumber> NpiNumbers { get; set; }
        public virtual ICollection<Payer> Payers { get; set; }
        public virtual ICollection<ReferralSource> ReferralSources { get; set; }
        public virtual ICollection<User> Users { get; set; }
    }
}
