using System;
using System.Collections.Generic;

namespace GenerateCodeDomain.Models
{
    public partial class TaskGroup
    {
        public TaskGroup()
        {
            this.TaskGroupTaskTemplates = new List<TaskGroupTaskTemplate>();
        }

        public int Id { get; set; }
        public string Name { get; set; }
        public Nullable<bool> IsDefault { get; set; }
        public int CreatedById { get; set; }
        public int LastUserId { get; set; }
        public System.DateTime LastTime { get; set; }
        public System.DateTime CreatedOn { get; set; }
        public byte[] LastModified { get; set; }
        public virtual ICollection<TaskGroupTaskTemplate> TaskGroupTaskTemplates { get; set; }
    }
}
