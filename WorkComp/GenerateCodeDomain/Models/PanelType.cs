using System;
using System.Collections.Generic;

namespace GenerateCodeDomain.Models
{
    public partial class PanelType
    {
        public PanelType()
        {
            this.PanelCodes = new List<PanelCode>();
            this.Referrals = new List<Referral>();
        }

        public int Id { get; set; }
        public string Name { get; set; }
        public string Code { get; set; }
        public Nullable<int> PayerId { get; set; }
        public Nullable<bool> IsBasic { get; set; }
        public int CreatedById { get; set; }
        public int LastUserId { get; set; }
        public System.DateTime LastTime { get; set; }
        public System.DateTime CreatedOn { get; set; }
        public byte[] LastModified { get; set; }
        public virtual ICollection<PanelCode> PanelCodes { get; set; }
        public virtual Payer Payer { get; set; }
        public virtual ICollection<Referral> Referrals { get; set; }
    }
}
