using System;
using System.Collections.Generic;

namespace GenerateCodeDomain.Models
{
    public partial class NpiNumber
    {
        public NpiNumber()
        {
            this.ReferralNpiNumbers = new List<ReferralNpiNumber>();
        }

        public int Id { get; set; }
        public string OrganizationName { get; set; }
        public string FirstName { get; set; }
        public string LastName { get; set; }
        public string MiddleName { get; set; }
        public string Npi { get; set; }
        public string Phone { get; set; }
        public string Fax { get; set; }
        public string LicenseNumber { get; set; }
        public string ProviderCredentialText { get; set; }
        public string Email { get; set; }
        public string Address { get; set; }
        public int StateId { get; set; }
        public int CityId { get; set; }
        public int ZipId { get; set; }
        public Nullable<bool> IsUserUpdate { get; set; }
        public int CreatedById { get; set; }
        public int LastUserId { get; set; }
        public System.DateTime LastTime { get; set; }
        public System.DateTime CreatedOn { get; set; }
        public byte[] LastModified { get; set; }
        public virtual City City { get; set; }
        public virtual State State { get; set; }
        public virtual Zip Zip { get; set; }
        public virtual ICollection<ReferralNpiNumber> ReferralNpiNumbers { get; set; }
    }
}
