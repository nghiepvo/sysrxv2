﻿using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.ComponentModel.DataAnnotations.Schema;
using Framework.DataAnnotations;
using Framework.Utility;

namespace Framework.DomainModel.Entities
{
    public class User: Entity
    {
        public User()
        {
            GridConfigs=new Collection<GridConfig>();
            TaskTemplates=new Collection<TaskTemplate>();
            Referrals=new Collection<Referral>();
            ReferralTasks = new Collection<ReferralTask>();
            ReferralNotes = new Collection<ReferralNote>();
            Alerts = new Collection<Alert>();
        }
        [LocalizeRequired]
        [LocalizeMaxLength(50)]
        public string FirstName { get; set; }
        [LocalizeRequired]
        [LocalizeMaxLength(50)]
        public string LastName { get; set; }
         [LocalizeMaxLength(50)]
        public string MiddleName { get; set; }
         [LocalizeRequired]
         [LocalizeMaxLength(100)]
        public string UserName { get; set; }
        public string Password { get; set; }
        [LocalizeRequired]
        [LocalizeEmailAddress]
        public string Email { get; set; }

        private string _phone;

        [LocalizePhone]
        public string Phone {
            get
            {
                return _phone;
            }
            set
            {
                _phone = value.RemoveFormatPhone();
            } 
        }
        public string Avatar { get; set; }
        [LocalizeRequired]
        [LocalizeMaxLength(1000)]
        public string Address { get; set; }
        public int UserRoleId { get; set; }
        public bool? IsActive { get; set; }
        public bool? IsEnable { get; set; }
        public DateTime? LockUser { get; set; }
        public int? StateId { set; get; }
        public int? CityId { set; get; }
        public int? ZipId { set; get; }
        [NotMapped]
        public bool IsWorkcompUser { get; set; }
        public virtual UserRole UserRole { get; set; }
        public virtual ICollection<GridConfig> GridConfigs { get; set; }
        public virtual ICollection<TaskTemplate> TaskTemplates { get; set; }
        public virtual State State { get; set; }
        public virtual City City { get; set; }
        public virtual Zip Zip { get; set; }
        public virtual ICollection<Referral> Referrals { get; set; }
        public virtual ICollection<ReferralTask> ReferralTasks { get; set; }
        public virtual ICollection<ReferralNote> ReferralNotes { get; set; }
        public virtual ICollection<Alert> Alerts { get; set; }
    }
}
