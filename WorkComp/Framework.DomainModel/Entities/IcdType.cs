using System.Collections.Generic;
using System.Collections.ObjectModel;

namespace Framework.DomainModel.Entities
{
    public class IcdType:Entity
    {
        public IcdType()
        {
            Icds = new Collection<Icd>();
        }
        public string Name { get; set; }
        public string Description { get; set; }
        public virtual ICollection<Icd> Icds { get; set; }
    }
}
