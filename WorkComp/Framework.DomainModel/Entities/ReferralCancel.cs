using Framework.DataAnnotations;

namespace Framework.DomainModel.Entities
{
    public class ReferralCancel : Entity
    {
        [LocalizeRequired]
        public string Comment { get; set; }
        [LocalizeRequired]
        [LocalizeMaxLength(200)]
        public string Reason { get; set; }
        public int ReferralId { get; set; }
        public virtual Referral Referral { get; set; }
    }
}