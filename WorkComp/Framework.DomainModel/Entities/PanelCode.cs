using Framework.DataAnnotations;

namespace Framework.DomainModel.Entities
{
    public class PanelCode : Entity
    {
        [LocalizeMaxLength(50)]
        public string CptCode { get; set; }
        [LocalizeRequired]
        [LocalizeMaxLength(50)]
        public string Code { get; set; }
        public int SampleTestingTypeId { get; set; }
        public int PanelTypeId { get; set; }
        [LocalizeRequired]
        public int PanelId { get; set; }
        public virtual PanelType PanelType { get; set; }
        public virtual Panel Panel { get; set; }
        public virtual SampleTestingType SampleTestingType { get; set; }
    }

    public enum SampleTestingTypeEnum
    {
        Urine=1,
        OralFluid=2,
        Blood=3,
        Hair=4
    }
}