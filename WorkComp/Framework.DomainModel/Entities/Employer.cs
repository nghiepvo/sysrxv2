﻿using System.Collections.Generic;
using System.Collections.ObjectModel;
using Framework.DataAnnotations;

namespace Framework.DomainModel.Entities
{
    public class Employer: Entity
    {
        public Employer()
        {
            ClaimNumbers = new Collection<ClaimNumber>();
        }
        public string ExternalId { get; set; }
        [LocalizeRequired]
        public string Name { get; set; }
        [LocalizePhone]
        public string Phone { get; set; }
        public string FederalTaxId { get; set; }
        public string GeoAddress { get; set; }
        public string Address { get; set; }
        public string Address1 { get; set; }
        
        public int? Rank { get; set; }
        public bool? IsUserUpdate { get; set; }
        public bool? OptedOutSendMail { get; set; }

        public int? StateId { get; set; }
        public virtual State State { get; set; }
        public int? CityId { get; set; }
        public virtual City City { get; set; }
        public int? ZipId { get; set; }
        public virtual Zip Zip { get; set; }

        public virtual ICollection<ClaimNumber> ClaimNumbers { get; set; }
    }
}
