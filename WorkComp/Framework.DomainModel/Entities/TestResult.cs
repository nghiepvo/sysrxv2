﻿using System.Collections.Generic;

namespace Framework.DomainModel.Entities
{
    public class TestResult: Entity
    {
        public string ControlNumber { get; set; }
        public string HtmlResult { get; set; }
        public string IsSendMail { get; set; }

        public virtual ICollection<Referral> Referrals { get; set; }
    }
}
