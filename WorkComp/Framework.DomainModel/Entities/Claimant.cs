﻿using System.Collections.Generic;
using System.Collections.ObjectModel;
using Framework.DataAnnotations;
using System;

namespace Framework.DomainModel.Entities
{
    public class Claimant : Entity
    {
        public Claimant()
        {
            ClaimNumbers = new Collection<ClaimNumber>();
        }

        public string PayerPatientId { get; set; }
        [LocalizeRequired]
        public string FirstName { get; set; }
        [LocalizeRequired]
        public string LastName { get; set; }
        public string MiddleName { get; set; }
        public string Suffix { get; set; }
        public string Ssn { get; set; }
        public string PatientAbbr { get; set; }
        [LocalizeRequired]
        public string Address1 { get; set; }
        public string Address2 { get; set; }
        
        public DateTime? ExpirationDate { get; set; }
        [LocalizeRequired]
        public string Gender { get; set; }
        [LocalizeRequired]
        public DateTime? Birthday { get; set; }

        [LocalizeEmailAddress]
        public string Email { get; set; }

        public int? ClaimantLanguageId { get; set; }
        public virtual ClaimantLanguage ClaimantLanguage { get; set; }
        [LocalizePhone]
        public string WorkPhone { get; set; }
        public string WorkPhoneExtension { get; set; }
        [LocalizePhone]
        public string HomePhone { get; set; }
        public string HomePhoneExtension { get; set; }
        [LocalizePhone]
        public string CellPhone { get; set; }
        
        public string BestContactNumber { get; set; }

        
        public int? StateId { get; set; }
        public int? CityId { get; set; }
        public int? ZipId { get; set; }
        public virtual State State { get; set; }
        public virtual City City { get; set; }
        public virtual Zip Zip { get; set; }

        public bool IsUserUpdate { get; set; }

        public virtual ICollection<ClaimNumber> ClaimNumbers { get; set; }
    }
}
