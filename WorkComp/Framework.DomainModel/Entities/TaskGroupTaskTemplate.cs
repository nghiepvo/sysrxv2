﻿namespace Framework.DomainModel.Entities
{
    public class TaskGroupTaskTemplate : Entity
    {
        public int? TaskTemplateId { get; set; }
        public int? TaskGroupId { get; set; }
        public virtual TaskGroup TaskGroup { get; set; }
        public virtual TaskTemplate TaskTemplate { get; set; }
    }
}