using System.Collections.Generic;
using System.Collections.ObjectModel;
using Framework.DataAnnotations;

namespace Framework.DomainModel.Entities
{
    public class Drug : Entity
    {
        public Drug()
        {
            ReferralMedicationHistories = new Collection<ReferralMedicationHistory>();
        }
        [LocalizeRequired]
        [LocalizeMaxLength(100)]
        public string Name { get; set; }
        [LocalizeMaxLength(100)]
        public string Class { get; set; }
        [LocalizeMaxLength(1000)]
        public string Description { get; set; }
        public virtual ICollection<ReferralMedicationHistory> ReferralMedicationHistories { get; set; }
    }
}