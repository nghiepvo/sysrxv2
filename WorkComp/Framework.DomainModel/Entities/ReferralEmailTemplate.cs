using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using Framework.DataAnnotations;

namespace Framework.DomainModel.Entities
{
    public class ReferralEmailTemplate : Entity
    {
        public ReferralEmailTemplate()
        {
            ReferralEmailTemplateAttachments = new Collection<ReferralEmailTemplateAttachment>();
        }

        public int ReferralId { get; set; }
        public int EmailTemplateId { get; set; }
        [LocalizeRequired]
        public string Content { get; set; }
        [LocalizeRequired]
        [LocalizeMaxLength(1000)]
        public string Subject { get; set; }
        public string EmailAddress { get; set; }
        public DateTime? DateToSendMail { get; set; }
        [LocalizeFax]
        public string FaxNumber { get; set; }
        public DateTime? DateToSendFax { get; set; }
        public virtual Referral Referral { get; set; }
        public virtual EmailTemplate EmailTemplate { get; set; }
        public virtual ICollection<ReferralEmailTemplateAttachment> ReferralEmailTemplateAttachments { get; set; }
    }
}