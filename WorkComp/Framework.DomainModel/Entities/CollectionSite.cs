﻿using System.Collections.Generic;
using System.Collections.ObjectModel;
using Framework.DataAnnotations;

namespace Framework.DomainModel.Entities
{
    public class CollectionSite : Entity
    {
        public CollectionSite()
        {
            ReferralCollectionSites = new Collection<ReferralCollectionSite>();
        }
        [LocalizeRequired]
        [LocalizeMaxLength(500)]
        public string Name { get; set; }
        [LocalizePhone]
        public string Phone { get; set; }
        [LocalizePhone]
        public string Fax { get; set; }
        [LocalizeEmailAddress]
        public string Email { get; set; }

        public string CollectionHour { get; set; }
        public string LunchHour { get; set; }
        public string LocationIdentified { get; set; }
        public string CostInformation { get; set; }
        public string ContactName { get; set; }
        public string ContactEmail { get; set; }

        [LocalizeRequired]
        public string Address { get; set; }

        public int? StateId { get; set; }
        public virtual State State { get; set; }
        public int? CityId { get; set; }
        public virtual City City { get; set; }
        public int? ZipId { get; set; }
        public virtual Zip Zip { get; set; }

        public double? Lat { get; set; }
        public double? Lng { get; set; }
        public bool Contracted { get; set; }
        public virtual ICollection<ReferralCollectionSite> ReferralCollectionSites { get; set; }
    }
}
