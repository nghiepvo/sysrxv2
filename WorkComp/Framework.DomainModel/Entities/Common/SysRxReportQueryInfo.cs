﻿using System;
using Framework.Utility;

namespace Framework.DomainModel.Entities.Common
{
    public enum SysRxReportType
    {
        UtilizationByInjuredWorkerReport =1,
        TestResultsDetailReport = 2,
        TestResultsSummaryReport = 3,
        CancellationDetailReport = 4,
// ReSharper disable once InconsistentNaming
        MGMTStatusSummaryReport = 5,
// ReSharper disable once InconsistentNaming
        MGMTUserProductivityReport = 6,
        UtilizationByPhysicianReport = 7,
        UtilizationByEmployerReport = 8,
        UtilizationByJurisdictionReport = 9,
        CancellationPhysicianReport = 10,
        OpenReferralDetailReport = 11,
    }

    public enum SysRxExportType
    {
        View = 1,
        Print = 2,
        Pdf = 3,
        Excel = 4,
    }

    public class SysRxReportQueryInfo
    {
        public SysRxReportType? ReportType { get; set; }
        public SysRxExportType? ExportType { get; set; }
        public int? PayerId { get; set; }
        public string PayerName { get; set; }
        public DateTime? DateFrom { get; set; }
        public DateTime? DateTo { get; set; }
        public int? CurrentPage { get; set; }
        public int? PageSize { get; set; }
        public int TotalRow { get; set; }

        public string Title
        {
            get
            {
                if (ReportType != null)
                {
                    var changeType = Convert.ChangeType((SysRxReportType)ReportType, ((SysRxReportType)ReportType).GetTypeCode());
                    if (changeType !=null)
                        return XmlDataHelpper.Instance.GetValue(XmlDataTypeEnum.SysRxReportType.ToString(), changeType.ToString());
                }
                return string.Empty;
            }
        }
    }
}
