﻿namespace Framework.DomainModel.Entities.Common
{
    public enum XmlDataTypeEnum
    {
        Gender,
        ClaimNumberStatus,
        Status,
        TaskType,
        ReferralMethod,
        CollectionMethod,
        DosageUnit,
        NoteType,
        TypeAlert,
        CancelReason,
        ConfigurationType,
        SysRxReportType,
        ReferralSourceAuthorization,
    }
}
