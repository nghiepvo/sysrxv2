﻿using System;
using System.Collections.Generic;
using System.Text;
using System.Xml.Linq;
using Framework.DomainModel.Interfaces;
using Newtonsoft.Json;

namespace Framework.DomainModel.Entities.Common
{
    public enum ReferralFilterType
    {
        All=0,
        Today=1,
        ThisWeek=2,
        LastWeek=3,
        ThisMonth=4,
        LastMonth=5,
        Custom=6
    }

    public class ReferralFilterItem
    {
        public ReferralFilterType FilterType { get; set; }
        public DateTime? DueDateFrom { get; set; }
        public DateTime? DueDateTo { get; set; }
    }
    public class ReferralQueryInfo : QueryInfo
    {
        public DateTime? DueDateFrom { get; set; }
        public DateTime? DueDateTo { get; set; }
        public ReferralFilterType FilterType { get; set; }
    }
    public class QueryInfo : IQueryInfo
    {
        public QueryInfo()
        {
            // defaults
            Take = 50;
            //ActiveRecords = true;
            //InactiveRecords = true;
            Sort = new List<Sort>();
        }

        #region Nghiep
        public int? ParentId { get; set; }
        public string FiltersStringEncode { get; set; }

        public string FiltersStringDecode
        {
            get
            {
                if (string.IsNullOrEmpty(FiltersStringEncode))
                {
                    return string.Empty;
                }
                byte[] data = Convert.FromBase64String(FiltersStringEncode);
                return Encoding.UTF8.GetString(data);
            }
        }
        public List<Filter> Filters
        {
            get
            {
                var data = JsonConvert.DeserializeObject<List<Filter>>(FiltersStringDecode);
                return data;
            }
        }
        public string TypeWithUserString { get; set; }

        public TypeWithUserQueryEnum TypeWithUser
        {
            get
            {
                return string.IsNullOrEmpty(TypeWithUserString)
                    ? TypeWithUserQueryEnum.All
                    : (TypeWithUserQueryEnum) Enum.Parse(typeof (TypeWithUserQueryEnum), TypeWithUserString);
            }
        }

        
        #endregion

        public int Take { get; set; }
        public int Skip { get; set; }
        public int QueryId { get; set; }
        public List<Sort> Sort { get; set; }

        public string SearchString { get; set; }
        public string AdditionalSearchString { get; set; }

        public string SearchTerms { get; set; }
        public DateTime? CreatedBefore { get; set; }
        public DateTime? CreatedAfter { get; set; }
        public int CreatedBy { get; set; }
        public DateTime? ModifiedBefore { get; set; }
        public DateTime? ModifiedAfter { get; set; }
        public int ModifiedBy { get; set; }
        //public bool ActiveRecords { get; set; }
        //public bool InactiveRecords { get; set; }
        public int TotalRecords { get; set; }

        public string SortString
        {
            get
            {
                // order the results
                if (Sort != null && Sort.Count > 0)
                {
                    var sorts = new List<string>();
                    Sort.ForEach(x => sorts.Add(string.Format("{0} {1}", x.Field, x.Dir)));
                    return string.Join(",", sorts.ToArray());
                }
                return string.Empty;
            }
        }

        public virtual void ParseParameters(string xmlParams)
        {
            if (xmlParams == null)
                return;
            var xml = XDocument.Parse(xmlParams);
            foreach (var param in xml.Descendants("AdvancedQueryParameters"))
            {
                var searchTerm = param.Element("SearchTerms");
                if (searchTerm != null)
                    SearchTerms = searchTerm.Value;
            }
        }
    }
}