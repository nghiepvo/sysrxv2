﻿namespace Framework.DomainModel.Entities.Common
{
    public enum TypeWithUserQueryEnum
    {
        All = 1,
        Other = 2,
        Current = 3
    }
}
