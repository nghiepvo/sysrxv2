﻿using System.Collections.Generic;
using System.Collections.ObjectModel;
using Framework.DataAnnotations;

namespace Framework.DomainModel.Entities
{
    public class ReferralType: Entity
    {
        public ReferralType()
        {
            ReferralSources = new Collection<ReferralSource>();
            ReferralMedicationHistories=new Collection<ReferralMedicationHistory>();
        }

        [LocalizeRequired]
        public string Name { get; set; }

        public ICollection<ReferralSource> ReferralSources { get; set; }
        public ICollection<ReferralMedicationHistory> ReferralMedicationHistories { get; set; }
    }
}
