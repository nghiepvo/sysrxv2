using System;
namespace Framework.DomainModel.Entities
{
    public class ReferralEmailTemplateAttachment : Entity
    {
        public int ReferralEmailTemplateId { get; set; }
        public Guid RowGuid { get; set; }
        public int AttachedFileSize { get; set; }
        public string AttachedFileName { get; set; }
        public byte[] AttachedFileContent { get; set; }
        public virtual ReferralEmailTemplate ReferralEmailTemplate { get; set; }
    }
}