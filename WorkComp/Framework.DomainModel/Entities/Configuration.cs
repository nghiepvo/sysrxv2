﻿namespace Framework.DomainModel.Entities
{
    public enum ConfigurationIdByEnum
    {
        //From 1 to 8 in INCONSISTENT RESULTS
        MedicationReportedNotDetected = 1,
        MedicationNotReportedDetected = 2,
        IllicitResults = 3,
        IllicitDrugs = 4,
        NoMedication = 5,
        ValidityTesting = 6,
        EthylGlucuronide = 7,
        EthylSulfate = 8,
        //9 in CONSISTENT RESULTS
        MedicationReportedDetected = 9,
        //below commond
        InconsistentResults = 10, 
        TestBy = 11,
        CustomerService = 12, 
        ContactReport = 13,
        HeaderReport = 14,
        FooterReport = 15,
        //end commond

        //Export Servive 16 - 20 for send Mail Export Pdf
        ExportServiceIsAutoSendMail = 16,
        ExportServiceListEmailCc = 17,
        ExportServiceHasAttachment = 18,
        ExportServiceHasEmailFrom = 19,
        ExportServiceHasDisplayName = 20,
        ExportServiceHasSercure = 21,
        ExportServiceDefaultSendTo = 22,
        //end Servive
    }
    public class Configuration : Entity
    {
        public int TypeId { get; set; }
        public string Name { get; set; }
        public string Value { get; set; }
        public bool? Configurable { get; set; }
        public bool? IsHtml { get; set; }
    }
}