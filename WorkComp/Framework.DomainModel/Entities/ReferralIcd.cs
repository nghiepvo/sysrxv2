namespace Framework.DomainModel.Entities
{
    public class ReferralIcd : Entity
    {
        public int? ReferralId { get; set; }
        public int? IcdId { get; set; }
        public virtual Icd Icd { get; set; }
        public virtual Referral Referral { get; set; }
    }
}