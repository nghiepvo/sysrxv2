using Framework.DataAnnotations;
namespace Framework.DomainModel.Entities
{
    public class AssayCode : Entity
    {

        [LocalizeRequired]
        [LocalizeMaxLength(10)]
        public string Code { get; set; }

        public int AssayCodeDescriptionId { get; set; }
        public virtual AssayCodeDescription AssayCodeDescription { get; set; }
        public int SampleTestingTypeId { get; set; }
        public virtual SampleTestingType SampleTestingType { get; set; }
    }
}