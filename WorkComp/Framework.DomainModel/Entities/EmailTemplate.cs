﻿using System.Collections.Generic;
using System.Collections.ObjectModel;
using Framework.DataAnnotations;

namespace Framework.DomainModel.Entities
{
    public enum EmailTemplateType
    {
        RequestForAuthorization = 1,
        RequestForAuthorizationSecondRequest = 2,
        CancellationDueToLackOfInformation = 3,
        IwAppointmentReminder = 4,
        SchedulingConfirmed = 5,
        IwAppointmentNotification = 6,
        CollectionServiceRequestBroadSpire = 7,
        NotificateTestingCandidate = 8,
        CollectionServiceRequestMedicationMonitoringPme = 9,
        MdNotificationLetter = 10,
        CustomCommunication = 11,
        PhysicianShipmentConfirmation = 12,
        //Service Export
        ExportServiceAutoSendMail = 13,
    }

    public class EmailTemplate : Entity
    {
        public EmailTemplate()
        {
            ReferralEmailTemplates = new Collection<ReferralEmailTemplate>();
        }
        [LocalizeRequired, LocalizeMaxLength(1000)]
        public string Title { get; set; }
        [LocalizeRequired]
        public string Subject { get; set; }
        public string Content { get; set; }
        public EmailTemplateType Type { get; set; }
        public bool IsActived { get; set; }
        public bool IsService { get; set; }
        public virtual ICollection<ReferralEmailTemplate> ReferralEmailTemplates { get; set; }
    }
}
