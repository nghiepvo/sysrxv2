namespace Framework.DomainModel.Entities
{
    public class ReferralAttachment:Entity
    {
        public int ReferralId { get; set; }
        public System.Guid RowGUID { get; set; }
        public int AttachedFileSize { get; set; }
        public string AttachedFileName { get; set; }
        public byte[] AttachedFileContent { get; set; }
        public virtual Referral Referral { get; set; }
    }
}