using System.Collections.Generic;

namespace Framework.DomainModel.Entities
{
    public class Panel : Entity
    {
        public Panel()
        {
            PanelCodes = new List<PanelCode>();
        }
        public string Name { get; set; }
        public virtual ICollection<PanelCode> PanelCodes { get; set; }
    }
}