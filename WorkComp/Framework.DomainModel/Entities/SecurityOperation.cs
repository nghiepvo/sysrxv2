using System.Collections.Generic;
using System.Collections.ObjectModel;

namespace Framework.DomainModel.Entities
{
    public class SecurityOperation : Entity
    {
        public SecurityOperation()
        {
            UserRoleFunctions = new Collection<UserRoleFunction>();
        }

        public string Name { get; set; }
        public virtual ICollection<UserRoleFunction> UserRoleFunctions { get; set; }
    }
}
