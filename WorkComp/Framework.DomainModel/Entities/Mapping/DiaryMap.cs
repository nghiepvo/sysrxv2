namespace Framework.DomainModel.Entities.Mapping
{
    public class DiaryMap : WorkCompEntityTypeConfiguration<Diary>
    {
        public DiaryMap()
        {
            // Properties
            Property(t => t.Heading)
                .HasMaxLength(1000);
            Property(t => t.Reason)
               .HasMaxLength(1000);
            // Table & Column Mappings
            ToTable("Diary");
            Property(t => t.Id).HasColumnName("Id");
            Property(t => t.Heading).HasColumnName("Heading");
            Property(t => t.Reason).HasColumnName("Reason");
            Property(t => t.Comment).HasColumnName("Comment");
            Property(t => t.ReferralId).HasColumnName("ReferralId");

            // Relationships
            HasRequired(t => t.Referral)
                .WithMany(t => t.Diaries)
                .HasForeignKey(d => d.ReferralId);
        }
    }
}