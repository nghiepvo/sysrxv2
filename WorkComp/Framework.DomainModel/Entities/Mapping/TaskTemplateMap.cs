namespace Framework.DomainModel.Entities.Mapping
{
    public class TaskTemplateMap : WorkCompEntityTypeConfiguration<TaskTemplate>
    {
        public TaskTemplateMap()
        {
            // Properties
            Property(t => t.Title)
                .HasMaxLength(200);

            // Table & Column Mappings
            ToTable("TaskTemplate");
            Property(t => t.Title).HasColumnName("Title");
            Property(t => t.Description).HasColumnName("Description");
            Property(t => t.NumDueDate).HasColumnName("NumDueDate");
            Property(t => t.NumDueHour).HasColumnName("NumDueHour");
            Property(t => t.StatusId).HasColumnName("StatusId");
            Property(t => t.AssignToId).HasColumnName("AssignToId");
            Property(t => t.TaskTypeId).HasColumnName("TaskTypeId");
            Property(t => t.IsSystem).HasColumnName("IsSystem");

            // Relationships
            HasOptional(t => t.AssignTo)
               .WithMany(t => t.TaskTemplates)
               .HasForeignKey(d => d.AssignToId);
        }
    }
}