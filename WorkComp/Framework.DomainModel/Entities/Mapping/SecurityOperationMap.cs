using System.ComponentModel.DataAnnotations.Schema;

namespace Framework.DomainModel.Entities.Mapping
{
    public class SecurityOperationMap : WorkCompEntityTypeConfiguration<SecurityOperation>
    {
        public SecurityOperationMap()
        {
            // Properties
            Property(t => t.Id)
                .HasDatabaseGeneratedOption(DatabaseGeneratedOption.None);

            Property(t => t.Name)
                .IsRequired()
                .HasMaxLength(50);

            // Table & Column Mappings
            ToTable("SecurityOperation");
            Property(t => t.Name).HasColumnName("Name");
        }
    }
}