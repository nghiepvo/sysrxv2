namespace Framework.DomainModel.Entities.Mapping
{
    public class StateMap : WorkCompEntityTypeConfiguration<State>
    {
        public StateMap()
        {
            // Properties
            Property(t => t.Name)
                .HasMaxLength(100);

            Property(t => t.AbbreviationName)
                .HasMaxLength(5);

            // Table & Column Mappings
            ToTable("State");
            Property(t => t.Name).HasColumnName("Name");
            Property(t => t.AbbreviationName).HasColumnName("AbbreviationName");
        }
    }
}