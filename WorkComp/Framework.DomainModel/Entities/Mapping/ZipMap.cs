namespace Framework.DomainModel.Entities.Mapping
{
    public class ZipMap : WorkCompEntityTypeConfiguration<Zip>
    {
        public ZipMap()
        {
            // Properties
            Property(t => t.Name)
                .HasMaxLength(10).IsRequired();
            Property(t => t.CityId).IsRequired();

           
            // Table & Column Mappings
            ToTable("Zip");
            Property(t => t.Name).HasColumnName("Name");
            Property(t => t.CityId).HasColumnName("CityId");

            // Relationships
            HasRequired(t => t.City)
                .WithMany(t => t.Zips)
                .HasForeignKey(d => d.CityId);
        }
    }
}