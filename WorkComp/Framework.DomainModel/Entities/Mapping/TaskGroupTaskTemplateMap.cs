namespace Framework.DomainModel.Entities.Mapping
{
    public class TaskGroupTaskTemplateMap : WorkCompEntityTypeConfiguration<TaskGroupTaskTemplate>
    {
        public TaskGroupTaskTemplateMap()
        {
            // Table & Column Mappings
            ToTable("TaskGroupTaskTemplate");
            Property(t => t.TaskTemplateId).HasColumnName("TaskTemplateId");
            Property(t => t.TaskGroupId).HasColumnName("TaskGroupId");
            // Relationships
            HasOptional(t => t.TaskGroup)
                .WithMany(t => t.TaskGroupTaskTemplates)
                .HasForeignKey(d => d.TaskGroupId);
            HasRequired(t => t.TaskTemplate)
                .WithMany(t => t.TaskGroupTaskTemplates)
                .HasForeignKey(d => d.TaskTemplateId);
        }
    }
}