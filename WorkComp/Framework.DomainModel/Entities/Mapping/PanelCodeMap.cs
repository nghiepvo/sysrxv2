namespace Framework.DomainModel.Entities.Mapping
{
    public class PanelCodeMap : WorkCompEntityTypeConfiguration<PanelCode>
    {
        public PanelCodeMap()
        {
            // Properties
            Property(t => t.CptCode)
                .HasMaxLength(50);

            Property(t => t.Code)
                .IsRequired()
                .HasMaxLength(50);
            // Table & Column Mappings
            ToTable("PanelCode");
            Property(t => t.PanelId).HasColumnName("PanelId");
            Property(t => t.CptCode).HasColumnName("CptCode");
            Property(t => t.Code).HasColumnName("Code");
            Property(t => t.SampleTestingTypeId).HasColumnName("SampleTestingTypeId");
            Property(t => t.PanelTypeId).HasColumnName("PanelTypeId");

            // Relationships
            HasRequired(t => t.PanelType)
                .WithMany(t => t.PanelCodes)
                .HasForeignKey(d => d.PanelTypeId);
            HasRequired(t => t.SampleTestingType)
                .WithMany(t => t.PanelCodes)
                .HasForeignKey(d => d.SampleTestingTypeId);
            HasRequired(t => t.Panel)
                .WithMany(t => t.PanelCodes)
                .HasForeignKey(d => d.PanelId);
        }
    }
}