namespace Framework.DomainModel.Entities.Mapping
{
    public class ClaimNumberMap : WorkCompEntityTypeConfiguration<ClaimNumber>
    {
        public ClaimNumberMap()
        {
            // Properties
            Property(t => t.Doi).IsRequired();
            Property(t => t.Name)
                .HasMaxLength(50);
            Property(t => t.SpecialInstructions)
                .IsMaxLength();
            Property(t => t.IsUserUpdate)
                .IsRequired();
            
          

            // Table & Column Mappings
            ToTable("ClaimNumber");

            Property(t => t.BranchId).HasColumnName("BranchId");
            Property(t => t.PayerId).HasColumnName("PayerId");
            Property(t => t.ClaimantId).HasColumnName("ClaimantId");
            Property(t => t.AdjusterId).HasColumnName("AdjusterId");
            Property(t => t.EmployerId).HasColumnName("EmployerId");
            Property(t => t.Doi).HasColumnName("Doi");
            Property(t => t.Name).HasColumnName("Name");
            Property(t => t.StateId).HasColumnName("StateId");
            Property(t => t.SpecialInstructions).HasColumnName("SpecialInstructions");
            Property(t => t.CloseDate).HasColumnName("CloseDate");
            Property(t => t.OpenDate).HasColumnName("OpenDate");
            Property(t => t.ReClosedDate).HasColumnName("ReClosedDate");
            Property(t => t.ReOpenDate).HasColumnName("ReOpenDate");
            Property(t => t.ReportDate).HasColumnName("ReportDate"); 

            Property(t => t.Status).HasColumnName("Status");
            Property(t => t.IsUserUpdate).HasColumnName("IsUserUpdate");

            // Relationship

            HasOptional(o => o.Branch).WithMany(o => o.ClaimNumbers).HasForeignKey(o => o.BranchId);
            HasOptional(o => o.Payer).WithMany(o => o.ClaimNumbers).HasForeignKey(o => o.PayerId);
            HasOptional(o => o.State).WithMany(o => o.ClaimNumbers).HasForeignKey(o => o.StateId);
            HasOptional(o => o.Adjuster).WithMany(o => o.ClaimNumbers).HasForeignKey(o => o.AdjusterId);
            HasOptional(o => o.Claimant).WithMany(o => o.ClaimNumbers).HasForeignKey(o => o.ClaimantId);
            HasOptional(o => o.Employer).WithMany(o => o.ClaimNumbers).HasForeignKey(o => o.EmployerId);
        }
    }
}