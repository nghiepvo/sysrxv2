namespace Framework.DomainModel.Entities.Mapping
{
    public class ReferralMap : WorkCompEntityTypeConfiguration<Referral>
    {
        public ReferralMap()
        {
            // Properties
            Property(t => t.ControlNumber)
                .HasMaxLength(50);
            Property(t => t.FileResult)
                .HasMaxLength(50);
            Property(t => t.InsertFrom)
                .HasMaxLength(1000);
            // Table & Column Mappings
            ToTable("Referral");
            Property(t => t.ControlNumber).HasColumnName("ControlNumber");
            Property(t => t.ReferralSourceId).HasColumnName("ReferralSourceId");
            Property(t => t.ClaimantNumberId).HasColumnName("ClaimantNumberId");
            Property(t => t.AdjusterIsReferral).HasColumnName("AdjusterIsReferral");
            Property(t => t.ProductTypeId).HasColumnName("ProductTypeId");
            Property(t => t.EnteredDate).HasColumnName("EnteredDate");
            Property(t => t.ReceivedDate).HasColumnName("ReceivedDate");
            Property(t => t.DueDate).HasColumnName("DueDate");
            Property(t => t.AssignToId).HasColumnName("AssignToId");
            Property(t => t.StatusId).HasColumnName("StatusId");
            Property(t => t.ReferralMethodId).HasColumnName("ReferralMethodId");
            Property(t => t.SpecialInstruction).HasColumnName("SpecialInstruction");
            Property(t => t.AttorneyId).HasColumnName("AttorneyId");
            Property(t => t.CaseManagerId).HasColumnName("CaseManagerId");
            Property(t => t.IsCollectionSite).HasColumnName("IsCollectionSite");
            Property(t => t.NoMedicationHistory).HasColumnName("NoMedicationHistory");
            Property(t => t.PanelTypeId).HasColumnName("PanelTypeId");
            Property(t => t.Rush).HasColumnName("Rush");
            Property(t => t.FileResult).HasColumnName("FileResult");
            Property(t => t.IsSendMailTreatingPhysician).HasColumnName("IsSendMailTreatingPhysician");
            Property(t => t.InsertFrom).HasColumnName("InsertFrom");
            Property(t => t.CreatedDateNov).HasColumnName("CreatedDateNov");
            Property(t => t.CancelDate).HasColumnName("CancelDate");
            Property(t => t.CompletedDate).HasColumnName("CompletedDate");
            Property(t => t.TestResultId).HasColumnName("TestResultId");
            // Relationships
            HasOptional(t => t.Attorney)
                .WithMany(t => t.Referrals)
                .HasForeignKey(d => d.AttorneyId);
            HasOptional(t => t.CaseManager)
                .WithMany(t => t.Referrals)
                .HasForeignKey(d => d.CaseManagerId);
            HasRequired(t => t.ClaimNumber)
                .WithMany(t => t.Referrals)
                .HasForeignKey(d => d.ClaimantNumberId);
            HasRequired(t => t.ProductType)
                .WithMany(t => t.Referrals)
                .HasForeignKey(d => d.ProductTypeId);
            HasRequired(t => t.ReferralSource)
                .WithMany(t => t.Referrals)
                .HasForeignKey(d => d.ReferralSourceId);
            HasRequired(t => t.AssignTo)
                .WithMany(t => t.Referrals)
                .HasForeignKey(d => d.AssignToId);
            HasOptional(t => t.PanelType)
                .WithMany(t => t.Referrals)
                .HasForeignKey(d => d.PanelTypeId);
            HasOptional(t => t.TestResult)
                .WithMany(t => t.Referrals)
                .HasForeignKey(t => t.TestResultId);
        }
    }
}