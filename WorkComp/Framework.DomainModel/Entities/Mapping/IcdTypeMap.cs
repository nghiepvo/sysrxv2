namespace Framework.DomainModel.Entities.Mapping
{
    public class IcdTypeMap : WorkCompEntityTypeConfiguration<IcdType>
    {
        public IcdTypeMap()
        {
            // Properties
            Property(t => t.Name)
                .HasMaxLength(500);

            // Table & Column Mappings
            ToTable("IcdType");
            Property(t => t.Name).HasColumnName("Name");
            Property(t => t.Description).HasColumnName("Description");
        }
    }
}