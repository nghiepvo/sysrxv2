namespace Framework.DomainModel.Entities.Mapping
{
    public class ReferralSourceMap : WorkCompEntityTypeConfiguration<ReferralSource>
    {
        public ReferralSourceMap()
        {
            // Properties
            Property(t => t.FirstName)
                .HasMaxLength(50).IsRequired();
            Property(t => t.LastName)
                .HasMaxLength(50).IsRequired();
            Property(t => t.MiddleName)
                .HasMaxLength(50);
            Property(t => t.Company)
                .HasMaxLength(500);
            Property(t => t.Phone)
                .HasMaxLength(50);
            Property(t => t.Extension)
                .HasMaxLength(50);
            Property(t => t.Fax)
                .HasMaxLength(50);
            Property(t => t.Email)
                .HasMaxLength(50);
            Property(t => t.Address).IsRequired()
                .HasMaxLength(500);
            

            // Table & Column Mappings
            ToTable("ReferralSource");
            Property(t => t.FirstName).HasColumnName("FirstName");
            Property(t => t.LastName).HasColumnName("LastName");
            Property(t => t.MiddleName).HasColumnName("MiddleName");
            Property(t => t.Company).HasColumnName("Company");
            Property(t => t.Phone).HasColumnName("Phone");
            Property(t => t.Extension).HasColumnName("Extension");
            Property(t => t.Fax).HasColumnName("Fax");
            Property(t => t.Email).HasColumnName("Email");
            Property(t => t.Address).HasColumnName("Address");
            Property(t => t.StateId).HasColumnName("StateId");
            Property(t => t.CityId).HasColumnName("CityId");
            Property(t => t.ZipId).HasColumnName("ZipId");
            Property(t => t.ReferralTypeId).HasColumnName("ReferralTypeId");
            Property(t => t.IsAuthorization).HasColumnName("IsAuthorization");
            Property(t => t.AuthorizationFrom).HasColumnName("AuthorizationFrom");
            Property(t => t.AuthorizationTo).HasColumnName("AuthorizationTo");
            Property(t => t.NoAuthorizationType).HasColumnName("NoAuthorizationType");
            Property(t => t.OneTimeDate).HasColumnName("OneTimeDate");

            //Relationship

            HasOptional(o => o.State).WithMany(o => o.ReferralSources).HasForeignKey(o => o.StateId);
            HasOptional(o => o.City).WithMany(o => o.ReferralSources).HasForeignKey(o => o.CityId);
            HasOptional(o => o.Zip).WithMany(o => o.ReferralSources).HasForeignKey(o => o.ZipId);
            HasOptional(o => o.ReferralType).WithMany(o => o.ReferralSources).HasForeignKey(o => o.ReferralTypeId);
        }
    }
}