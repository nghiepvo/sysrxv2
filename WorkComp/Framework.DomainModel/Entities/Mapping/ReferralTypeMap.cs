namespace Framework.DomainModel.Entities.Mapping
{
    public class ReferralTypeMap : WorkCompEntityTypeConfiguration<ReferralType>
    {
        public ReferralTypeMap()
        {
            // Properties
            Property(t => t.Name)
                .HasMaxLength(50).IsRequired();

            // Table & Column Mappings
            ToTable("ReferralType");
            Property(t => t.Name).HasColumnName("Name");
        }
    }
}