namespace Framework.DomainModel.Entities.Mapping
{
    public class ReferralIcdMap : WorkCompEntityTypeConfiguration<ReferralIcd>
    {
        public ReferralIcdMap()
        {
            // Table & Column Mappings
            ToTable("ReferralIcd");
            Property(t => t.ReferralId).HasColumnName("ReferralId");
            Property(t => t.IcdId).HasColumnName("IcdId");

            // Relationships
            HasOptional(t => t.Icd)
                .WithMany(t => t.ReferralIcds)
                .HasForeignKey(d => d.IcdId);
            HasOptional(t => t.Referral)
                .WithMany(t => t.ReferralIcds)
                .HasForeignKey(d => d.ReferralId);
        }
    }
}