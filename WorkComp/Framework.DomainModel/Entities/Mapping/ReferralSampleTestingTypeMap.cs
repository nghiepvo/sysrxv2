namespace Framework.DomainModel.Entities.Mapping
{
    public class ReferralSampleTestingTypeMap : WorkCompEntityTypeConfiguration<ReferralSampleTestingType>
    {
        public ReferralSampleTestingTypeMap()
        {
            // Table & Column Mappings
            ToTable("ReferralSampleTestingType");
            Property(t => t.ReferralId).HasColumnName("ReferralId");
            Property(t => t.SampleTestingTypeId).HasColumnName("SampleTestingTypeId");

            // Relationships
            HasOptional(t => t.SampleTestingType)
                .WithMany(t => t.ReferralSampleTestingTypes)
                .HasForeignKey(d => d.SampleTestingTypeId);
            HasOptional(t => t.Referral)
                .WithMany(t => t.ReferralSampleTestingTypes)
                .HasForeignKey(d => d.ReferralId);
        }
    }
}