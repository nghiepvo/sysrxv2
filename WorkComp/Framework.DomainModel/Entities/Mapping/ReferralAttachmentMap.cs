namespace Framework.DomainModel.Entities.Mapping
{
    public class ReferralAttachmentMap : WorkCompEntityTypeConfiguration<ReferralAttachment>
    {
        public ReferralAttachmentMap()
        {
            Property(t => t.AttachedFileName)
                .IsRequired()
                .HasMaxLength(255);

            // Table & Column Mappings
            ToTable("ReferralAttachment");
            Property(t => t.ReferralId).HasColumnName("ReferralId");
            Property(t => t.RowGUID).HasColumnName("RowGUID");
            Property(t => t.AttachedFileSize).HasColumnName("AttachedFileSize");
            Property(t => t.AttachedFileName).HasColumnName("AttachedFileName");
            Property(t => t.AttachedFileContent).HasColumnName("AttachedFileContent");

            // Relationships
            HasRequired(t => t.Referral)
                .WithMany(t => t.ReferralAttachments)
                .HasForeignKey(d => d.ReferralId);
        }
    }
}