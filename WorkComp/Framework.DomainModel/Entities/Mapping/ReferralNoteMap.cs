namespace Framework.DomainModel.Entities.Mapping
{
    public class ReferralNoteMap : WorkCompEntityTypeConfiguration<ReferralNote>
    {
        public ReferralNoteMap()
        {
            // Properties
            Property(t => t.Comment)
                .IsMaxLength().IsRequired();
            Property(t => t.ReferralId).IsRequired();
            Property(t => t.AssignToId)
                .IsRequired();

            // Table & Column Mappings
            ToTable("ReferralNote");
            Property(t => t.Comment).HasColumnName("Comment");
            Property(t => t.ReferralId).HasColumnName("ReferralId");
            Property(t => t.NoteType).HasColumnName("NoteType");
            Property(t => t.AssignToId).HasColumnName("AssignToId");

            //Relationship
            this.HasRequired(t => t.Referral)
                .WithMany(t => t.ReferralNotes)
                .HasForeignKey(d => d.ReferralId);
            this.HasRequired(t => t.User)
                .WithMany(t => t.ReferralNotes)
                .HasForeignKey(d => d.AssignToId);
        }
    }
}