namespace Framework.DomainModel.Entities.Mapping
{
    public class UserRoleFunctionMap : WorkCompEntityTypeConfiguration<UserRoleFunction>
    {
        public UserRoleFunctionMap()
        {
            // Table & Column Mappings
            ToTable("UserRoleFunction");
            Property(t => t.UserRoleId).HasColumnName("UserRoleId");
            Property(t => t.DocumentTypeId).HasColumnName("DocumentTypeId");
            Property(t => t.SecurityOperationId).HasColumnName("SecurityOperationId");

            // Relationships
            HasOptional(t => t.DocumentType)
                .WithMany(t => t.UserRoleFunctions)
                .HasForeignKey(d => d.DocumentTypeId);
            HasOptional(t => t.SecurityOperation)
                .WithMany(t => t.UserRoleFunctions)
                .HasForeignKey(d => d.SecurityOperationId);
            HasOptional(t => t.UserRole)
                .WithMany(t => t.UserRoleFunctions)
                .HasForeignKey(d => d.UserRoleId);
        }
    }
}