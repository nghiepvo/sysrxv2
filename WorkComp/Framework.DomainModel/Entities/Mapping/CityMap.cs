namespace Framework.DomainModel.Entities.Mapping
{
    public class CityMap : WorkCompEntityTypeConfiguration<City>
    {
        public CityMap()
        {
            // Properties
            Property(t => t.Name)
                .HasMaxLength(100).IsRequired();
            Property(t => t.StateId).IsRequired();

            // Table & Column Mappings
            ToTable("City");
            Property(t => t.Name).HasColumnName("Name");
            Property(t => t.StateId).HasColumnName("StateId");

            // Relationships
            HasRequired(t => t.State)
                .WithMany(t => t.Cities)
                .HasForeignKey(d => d.StateId);
        }
    }
}