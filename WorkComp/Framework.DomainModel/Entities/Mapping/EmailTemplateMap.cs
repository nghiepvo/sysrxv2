namespace Framework.DomainModel.Entities.Mapping
{
    public class EmailTemplateMap : WorkCompEntityTypeConfiguration<EmailTemplate>
    {
        public EmailTemplateMap()
        {
            // Properties
            Property(t => t.Title)
                .HasMaxLength(1000).IsRequired();
            Property(t => t.Subject)
                .IsMaxLength().IsRequired();
            Property(t => t.Content).IsMaxLength();

            // Table & Column Mappings
            ToTable("EmailTemplate");
            Property(t => t.Title).HasColumnName("Title");
            Property(t => t.Subject).HasColumnName("Subject");
            Property(t => t.Content).HasColumnName("Content");
            Property(t => t.Type).HasColumnName("Type");
            Property(t => t.IsService).HasColumnName("IsService");
            Property(t => t.IsActived).HasColumnName("IsActived");
        }
    }
}