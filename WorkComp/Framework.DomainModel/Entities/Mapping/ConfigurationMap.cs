namespace Framework.DomainModel.Entities.Mapping
{
    public class ConfigurationMap : WorkCompEntityTypeConfiguration<Configuration>
    {
        public ConfigurationMap()
        {
            // Properties
            this.Property(t => t.Name)
                .HasMaxLength(100);

            this.Property(t => t.Value)
                .IsRequired();

            this.Property(t => t.LastModified)
                .IsRequired()
                .IsFixedLength()
                .HasMaxLength(8)
                .IsRowVersion();

            // Table & Column Mappings
            this.ToTable("Configuration");
            this.Property(t => t.TypeId).HasColumnName("TypeId");
            this.Property(t => t.Name).HasColumnName("Name");
            this.Property(t => t.Value).HasColumnName("Value");
            this.Property(t => t.Configurable).HasColumnName("Configurable");
            this.Property(t => t.IsHtml).HasColumnName("IsHtml");

        }
    }
}