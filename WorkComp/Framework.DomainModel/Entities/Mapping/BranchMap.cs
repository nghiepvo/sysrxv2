﻿namespace Framework.DomainModel.Entities.Mapping
{
    public class BranchMap : WorkCompEntityTypeConfiguration<Branch>
    {
        public BranchMap()
        {
            // Properties
            Property(t => t.Name)
                .HasMaxLength(50).IsRequired();

            Property(t => t.ManagerName)
                .HasMaxLength(50);

            Property(t => t.Fax)
                .HasMaxLength(50);

            Property(t => t.Phone)
                .HasMaxLength(50);

            Property(t => t.Address1)
                .HasMaxLength(200).IsRequired();

            Property(t => t.Address2)
                .HasMaxLength(100);
            Property(t => t.IsUserUpdate).IsRequired();
            // Table & Column Mappings
            ToTable("Branch");
            Property(t => t.Name).HasColumnName("Name");
            Property(t => t.ManagerName).HasColumnName("ManagerName");
            Property(t => t.Fax).HasColumnName("Fax");
            Property(t => t.Phone).HasColumnName("Phone");
            Property(t => t.Address1).HasColumnName("Address1");
            Property(t => t.Address2).HasColumnName("Address2");
            Property(t => t.PayerId).HasColumnName("PayerId");
            Property(t => t.StateId).HasColumnName("StateId");
            Property(t => t.CityId).HasColumnName("CityId");
            Property(t => t.ZipId).HasColumnName("ZipId");
            Property(t => t.IsUserUpdate).HasColumnName("IsUserUpdate");

            // Relationships
            HasRequired(t => t.Payer)
                .WithMany(t => t.Branchs)
                .HasForeignKey(d => d.PayerId);
            HasOptional(t => t.State)
                .WithMany(t => t.Branchs)
                .HasForeignKey(d => d.StateId);
            HasOptional(t => t.City)
                .WithMany(t => t.Branchs)
                .HasForeignKey(d => d.CityId);
            HasOptional(t => t.Zip)
                .WithMany(t => t.Branchs)
                .HasForeignKey(d => d.ZipId);
        }
    }
}
