namespace Framework.DomainModel.Entities.Mapping
{
    public class AssayCodeDescriptionMap : WorkCompEntityTypeConfiguration<AssayCodeDescription>
    {
        public AssayCodeDescriptionMap()
        {
            // Properties
            Property(t => t.Description)
                .HasMaxLength(200);

            // Table & Column Mappings
            ToTable("AssayCodeDescription");
            Property(t => t.Description).HasColumnName("Description");
        }
    }
}