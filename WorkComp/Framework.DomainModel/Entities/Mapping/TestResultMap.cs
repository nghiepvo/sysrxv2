﻿namespace Framework.DomainModel.Entities.Mapping
{
    public class TestResultMap : WorkCompEntityTypeConfiguration<TestResult>
    {
        public TestResultMap() {
          // Table & Column Mappings
            ToTable("TestResult");
            Property(t => t.ControlNumber).HasColumnName("ControlNumber");
            Property(t => t.HtmlResult).HasColumnName("HtmlResult");
        }
    }
}
