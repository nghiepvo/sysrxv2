namespace Framework.DomainModel.Entities.Mapping
{
    public class ReferralEmailTemplateAttachmentMap : WorkCompEntityTypeConfiguration<ReferralEmailTemplateAttachment>
    {
        public ReferralEmailTemplateAttachmentMap()
        {
            Property(t => t.AttachedFileName)
                .IsRequired()
                .HasMaxLength(255);

            // Table & Column Mappings
            ToTable("ReferralEmailTemplateAttachment");
            Property(t => t.ReferralEmailTemplateId).HasColumnName("ReferralEmailTemplateId");
            Property(t => t.RowGuid).HasColumnName("RowGuid");
            Property(t => t.AttachedFileSize).HasColumnName("AttachedFileSize");
            Property(t => t.AttachedFileName).HasColumnName("AttachedFileName");
            Property(t => t.AttachedFileContent).HasColumnName("AttachedFileContent");

            // Relationships
            HasRequired(t => t.ReferralEmailTemplate)
                .WithMany(t => t.ReferralEmailTemplateAttachments)
                .HasForeignKey(d => d.ReferralEmailTemplateId);
        }
    }
}