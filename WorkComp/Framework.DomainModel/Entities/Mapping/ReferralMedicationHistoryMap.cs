namespace Framework.DomainModel.Entities.Mapping
{
    public class ReferralMedicationHistoryMap : WorkCompEntityTypeConfiguration<ReferralMedicationHistory>
    {
        public ReferralMedicationHistoryMap()
        {
            Property(t => t.Dosage)
                .HasMaxLength(50);

            // Table & Column Mappings
            ToTable("ReferralMedicationHistory");
            Property(t => t.DrugId).HasColumnName("DrugId");
            Property(t => t.ReferralId).HasColumnName("ReferralId");
            Property(t => t.DaysSupply).HasColumnName("DaysSupply");
            Property(t => t.Dosage).HasColumnName("Dosage");
            Property(t => t.DosageUnit).HasColumnName("DosageUnit");
            Property(t => t.ProvidedById).HasColumnName("ProvidedBy");
            Property(t => t.FillDate).HasColumnName("FillDate");

            // Relationships
            HasRequired(t => t.Drug)
                .WithMany(t => t.ReferralMedicationHistories)
                .HasForeignKey(d => d.DrugId);
            HasRequired(t => t.Referral)
                .WithMany(t => t.ReferralMedicationHistories)
                .HasForeignKey(d => d.ReferralId);
            HasOptional(t => t.ProvidedBy)
                .WithMany(t => t.ReferralMedicationHistories)
                .HasForeignKey(d => d.ProvidedById);
        }
    }
}