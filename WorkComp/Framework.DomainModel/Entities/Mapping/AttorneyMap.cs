namespace Framework.DomainModel.Entities.Mapping
{
    public class AttorneyMap : WorkCompEntityTypeConfiguration<Attorney>
    {
        public AttorneyMap()
        {
            // Properties
            Property(t => t.FirstName)
                .HasMaxLength(50).IsRequired();
            Property(t => t.LastName)
                .HasMaxLength(50).IsRequired();
            Property(t => t.MiddleName)
                .HasMaxLength(50);
            Property(t => t.Lawfirm)
               .HasMaxLength(500);
            Property(t => t.Phone)
                .HasMaxLength(50);
            Property(t => t.Email)
                .HasMaxLength(50);
            Property(t => t.Address)
               .HasMaxLength(500).IsRequired();

            //FirstName	nvarchar(50)	Unchecked
            //LastName	nvarchar(50)	Unchecked
            //MiddleName	nvarchar(50)	Checked
            //Lawfirm	nvarchar(500)	Checked
            //Phone	nvarchar(50)	Unchecked
            //Email	nvarchar(50)	Checked
            //Address	nvarchar(100)	Checked
            //IdState	int	Checked
            //IdCity	int	Checked
            //IdZip	int	Checked

            // Table & Column Mappings
            ToTable("Attorney");
            Property(t => t.FirstName).HasColumnName("FirstName");
            Property(t => t.LastName).HasColumnName("LastName");
            Property(t => t.MiddleName).HasColumnName("MiddleName");
            Property(t => t.Lawfirm).HasColumnName("Lawfirm");
            Property(t => t.Phone).HasColumnName("Phone");
            Property(t => t.Email).HasColumnName("Email");
            Property(t => t.Address).HasColumnName("Address");
            Property(t => t.StateId).HasColumnName("StateId");
            Property(t => t.CityId).HasColumnName("CityId");
            Property(t => t.ZipId).HasColumnName("ZipId");

            // Relationship
            HasOptional(o => o.State).WithMany(o => o.Attorneys).HasForeignKey(o => o.StateId);
            HasOptional(o => o.City).WithMany(o => o.Attorneys).HasForeignKey(o => o.CityId);
            HasOptional(o => o.Zip).WithMany(o => o.Attorneys).HasForeignKey(o => o.ZipId);
        }
    }
}