namespace Framework.DomainModel.Entities.Mapping
{
    public class UserMap : WorkCompEntityTypeConfiguration<User>
    {
        public UserMap()
        {
            Property(t => t.FirstName)
                .HasMaxLength(50);

            Property(t => t.LastName)
                .HasMaxLength(50);

            Property(t => t.MiddleName)
                .HasMaxLength(50);

            Property(t => t.UserName)
                .IsRequired()
                .HasMaxLength(100);

            Property(t => t.Password)
                .IsRequired()
                .HasMaxLength(100);

            Property(t => t.Email)
                .HasMaxLength(200);

            Property(t => t.Phone)
                .HasMaxLength(50);

            Property(t => t.Avatar)
                .HasMaxLength(50);

            Property(t => t.Address)
                .HasMaxLength(1000); 

            // Table & Column Mappings
            ToTable("User");
            Property(t => t.FirstName).HasColumnName("FirstName");
            Property(t => t.LastName).HasColumnName("LastName");
            Property(t => t.MiddleName).HasColumnName("MiddleName");
            Property(t => t.UserName).HasColumnName("UserName");
            Property(t => t.Password).HasColumnName("Password");
            Property(t => t.Email).HasColumnName("Email");
            Property(t => t.Phone).HasColumnName("Phone");
            Property(t => t.Avatar).HasColumnName("Avatar");
            Property(t => t.Address).HasColumnName("Address");
            Property(t => t.UserRoleId).HasColumnName("UserRoleId");
            Property(t => t.StateId).HasColumnName("StateId");
            Property(t => t.CityId).HasColumnName("CityId");
            Property(t => t.ZipId).HasColumnName("ZipId");
            Property(t => t.IsActive).HasColumnName("IsActive");
            Property(t => t.IsEnable).HasColumnName("IsEnable");
            // Relationships
            HasRequired(t => t.UserRole)
                .WithMany(t => t.Users)
                .HasForeignKey(d => d.UserRoleId);

            HasOptional(o => o.State).WithMany(s => s.Users).HasForeignKey(s => s.StateId);
            HasOptional(o => o.City).WithMany(s => s.Users).HasForeignKey(s => s.CityId);
            HasOptional(o => o.Zip).WithMany(s => s.Users).HasForeignKey(s => s.ZipId);
        }
    }
}