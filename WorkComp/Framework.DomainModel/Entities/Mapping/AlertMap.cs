namespace Framework.DomainModel.Entities.Mapping
{
    public class AlertMap : WorkCompEntityTypeConfiguration<Alert>
    {
        public AlertMap()
        {
            // Properties
            

            // Table & Column Mappings
            ToTable("Alert");
            Property(t => t.Id).HasColumnName("Id");
            Property(t => t.StatusId).HasColumnName("StatusId");
            Property(t => t.Type).HasColumnName("Type");
            Property(t => t.Title).HasColumnName("Title");
            Property(t => t.Message).HasColumnName("Message");
            Property(t => t.LinkId).HasColumnName("LinkId");
            Property(t => t.AssignToId).HasColumnName("AssignToId");
            Property(t => t.IsRush).HasColumnName("IsRush");
            

            // Relationship
            HasRequired(o => o.AssignTo).WithMany(o => o.Alerts).HasForeignKey(o => o.AssignToId);

        }
    }
}