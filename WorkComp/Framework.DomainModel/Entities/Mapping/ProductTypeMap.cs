namespace Framework.DomainModel.Entities.Mapping
{
    public class ProductTypeMap : WorkCompEntityTypeConfiguration<ProductType>
    {
        public ProductTypeMap()
        {
            // Properties
            Property(t => t.Name)
                .IsRequired()
                .HasMaxLength(100);

            // Table & Column Mappings
            ToTable("ProductType");
            Property(t => t.Name).HasColumnName("Name");
            Property(t => t.PayerId).HasColumnName("PayerId");

            // Relationships
            HasOptional(t => t.Payer)
                .WithMany(t => t.ProductTypes)
                .HasForeignKey(d => d.PayerId);
        }
    }
}