namespace Framework.DomainModel.Entities.Mapping
{
    public class UserRoleMap : WorkCompEntityTypeConfiguration<UserRole>
    {
        public UserRoleMap()
        {
            Property(t => t.Name)
                .IsRequired()
                .HasMaxLength(50);

            // Table & Column Mappings
            ToTable("UserRole");
            Property(t => t.Name).HasColumnName("Name");
        }
    }
}