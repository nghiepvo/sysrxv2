namespace Framework.DomainModel.Entities.Mapping
{
    public class EmployerMap : WorkCompEntityTypeConfiguration<Employer>
    {
        public EmployerMap()
        {
            // Properties
            Property(t => t.ExternalId)
                .HasMaxLength(50);
            Property(t => t.Name)
                .HasMaxLength(50);
            Property(t => t.Phone)
                .HasMaxLength(50);
            Property(t => t.FederalTaxId)
                .HasMaxLength(50);
            Property(t => t.GeoAddress)
                .HasMaxLength(50);
            Property(t => t.Address)
                .HasMaxLength(100);
            Property(t => t.Address1)
                .HasMaxLength(100);

            

            // Table & Column Mappings
            ToTable("Employer");
            Property(t => t.ExternalId).HasColumnName("ExternalId");
            Property(t => t.Name).HasColumnName("Name");
            Property(t => t.Phone).HasColumnName("Phone");
            Property(t => t.FederalTaxId).HasColumnName("FederalTaxId");
            Property(t => t.GeoAddress).HasColumnName("GeoAddress");
            Property(t => t.Address).HasColumnName("Address");
            Property(t => t.Address1).HasColumnName("Address1");
            Property(t => t.Rank).HasColumnName("Rank");
            Property(t => t.StateId).HasColumnName("StateId");
            Property(t => t.CityId).HasColumnName("CityId");
            Property(t => t.ZipId).HasColumnName("ZipId");
            Property(t => t.IsUserUpdate).HasColumnName("IsUserUpdate");

            //Relationship
            HasOptional(o => o.State).WithMany(s => s.Employers).HasForeignKey(s => s.StateId);
            HasOptional(o => o.City).WithMany(s => s.Employers).HasForeignKey(s => s.CityId);
            HasOptional(o => o.Zip).WithMany(s => s.Employers).HasForeignKey(s => s.ZipId);
            
        }
    }
}