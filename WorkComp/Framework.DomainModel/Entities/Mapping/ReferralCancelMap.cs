namespace Framework.DomainModel.Entities.Mapping
{
    public class ReferralCancelMap : WorkCompEntityTypeConfiguration<ReferralCancel>
    {
        public ReferralCancelMap()
        {
            // Properties
            Property(t => t.Reason)
                .HasMaxLength(200).IsRequired();
            Property(t => t.ReferralId).IsRequired();
            Property(t => t.Comment).IsRequired();

            // Table & Column Mappings
            ToTable("ReferralCancel");
            Property(t => t.Reason).HasColumnName("Reason");
            Property(t => t.Comment).HasColumnName("Comment");
            Property(t => t.ReferralId).HasColumnName("ReferralId");

            // Relationships
            HasRequired(t => t.Referral)
                .WithMany(t => t.ReferralCancels)
                .HasForeignKey(d => d.ReferralId);
        }
    }
}