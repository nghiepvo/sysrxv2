namespace Framework.DomainModel.Entities.Mapping
{
    public class SampleTestingTypeMap : WorkCompEntityTypeConfiguration<SampleTestingType>
    {
        public SampleTestingTypeMap()
        {
            //Name	nvarchar(50)	Unchecked
            //Description	nvarchar(MAX)	Checked
            //Checked	bit	Checked
            // Properties
            Property(t => t.Name)
                .HasMaxLength(50);
            Property(t => t.Description).IsMaxLength();

            // Table & Column Mappings
            ToTable("SampleTestingType");
            Property(t => t.Name).HasColumnName("Name");
            Property(t => t.Description).HasColumnName("Description");
            Property(t => t.Checked).HasColumnName("Checked");
        }
    }
}