namespace Framework.DomainModel.Entities.Mapping
{
    public class CommentMap : WorkCompEntityTypeConfiguration<Comment>
    {
        public CommentMap()
        {
            // Properties
            this.Property(t => t.CommentContent)
                .IsMaxLength();
            
            // Table & Column Mappings
            this.ToTable("Comment");
            this.Property(t => t.Id).HasColumnName("Id");
            this.Property(t => t.ReferralTaskId).HasColumnName("ReferralTaskId");
            this.Property(t => t.CommentContent).HasColumnName("CommentContent");

            // Relationships
            this.HasOptional(t => t.ReferralTask)
                .WithMany(t => t.Comments)
                .HasForeignKey(d => d.ReferralTaskId);

        }
    }
}