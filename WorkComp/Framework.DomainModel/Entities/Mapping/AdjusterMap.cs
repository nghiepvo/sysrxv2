namespace Framework.DomainModel.Entities.Mapping
{
    public class AdjusterMap : WorkCompEntityTypeConfiguration<Adjuster>
    {
        public AdjusterMap()
        {
            // Properties

            Property(t => t.FirstName).IsRequired()
                .HasMaxLength(50);
            Property(t => t.LastName).IsRequired()
                .HasMaxLength(50);
            Property(t => t.MiddleName)
                .HasMaxLength(50);
            Property(t => t.Phone)
                .HasMaxLength(50);
            Property(t => t.Extension)
                .HasMaxLength(50);
            Property(t => t.Email)
               .HasMaxLength(50);
            Property(t => t.Address)
               .HasMaxLength(100).IsRequired();
            Property(t => t.IsUserUpdate)
                .IsRequired();
           
            

            //BranchId	int	Checked
            //PayerId	int	Unchecked
            //FirstName	nvarchar(50)	Unchecked
            //LastName	nvarchar(50)	Unchecked
            //MiddleName	nvarchar(50)	Checked
            //Phone	nvarchar(50)	Unchecked
            //Extension	nvarchar(50)	Checked
            //Email	nvarchar(50)	Unchecked
            //AssignedDate	datetime	Checked
            //Address	nvarchar(100)	Unchecked
            //StateId	int	Unchecked
            //CityId	int	Unchecked
            //ZipId	int	Unchecked
            //Rank	int	Checked
            //IsUserUpdate	bit	Unchecked

            // Table & Column Mappings
            ToTable("Adjuster");
            Property(t => t.BranchId).HasColumnName("BranchId");
            Property(t => t.FirstName).HasColumnName("FirstName");
            Property(t => t.LastName).HasColumnName("LastName");
            Property(t => t.MiddleName).HasColumnName("MiddleName");
            Property(t => t.Phone).HasColumnName("Phone");
            Property(t => t.Extension).HasColumnName("Extension");
            Property(t => t.Email).HasColumnName("Email");
            Property(t => t.AssignedDate).HasColumnName("AssignedDate");
            Property(t => t.Address).HasColumnName("Address");
            Property(t => t.ExternalId).HasColumnName("ExternalId");
            Property(t => t.StateId).HasColumnName("StateId");
            Property(t => t.CityId).HasColumnName("CityId");
            Property(t => t.ZipId).HasColumnName("ZipId");
            Property(t => t.IsUserUpdate).HasColumnName("IsUserUpdate");

            // Relationship
            HasOptional(o => o.Branch).WithMany(o => o.Adjusters).HasForeignKey(o => o.BranchId);

            HasOptional(o => o.State).WithMany(o => o.Adjusters).HasForeignKey(o => o.StateId);
            HasOptional(o => o.City).WithMany(o => o.Adjusters).HasForeignKey(o => o.CityId);
            HasOptional(o => o.Zip).WithMany(o => o.Adjusters).HasForeignKey(o => o.ZipId);
        }
    }
}