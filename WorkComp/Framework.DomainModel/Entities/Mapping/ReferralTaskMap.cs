namespace Framework.DomainModel.Entities.Mapping
{
    public class ReferralTaskMap : WorkCompEntityTypeConfiguration<ReferralTask>
    {
        public ReferralTaskMap()
        {
            Property(t => t.Title)
                .IsRequired()
                .HasMaxLength(200);


            // Table & Column Mappings
            ToTable("ReferralTask");
            Property(t => t.Title).HasColumnName("Title");
            Property(t => t.Description).HasColumnName("Description");
            Property(t => t.AssignToId).HasColumnName("AssignToId");
            Property(t => t.StartDate).HasColumnName("StartDate");
            Property(t => t.DueDate).HasColumnName("DueDate");
            Property(t => t.TaskTypeId).HasColumnName("TaskTypeId");
            Property(t => t.ReferralId).HasColumnName("ReferralId");
            Property(t => t.Rush).HasColumnName("Rush");
            Property(t => t.IsCollectionServiceRequestLetter).HasColumnName("IsCollectionServiceRequestLetter");
            Property(t => t.CancelDate).HasColumnName("CancelDate");
            Property(t => t.CompletedDate).HasColumnName("CompletedDate");
            // Relationships
            HasRequired(t => t.Referral)
                .WithMany(t => t.ReferralTasks)
                .HasForeignKey(d => d.ReferralId);
            HasOptional(t => t.AssignTo)
                .WithMany(t => t.ReferralTasks)
                .HasForeignKey(d => d.AssignToId);
        }
    }
}