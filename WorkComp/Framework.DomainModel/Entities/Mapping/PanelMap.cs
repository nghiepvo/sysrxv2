namespace Framework.DomainModel.Entities.Mapping
{
    public class PanelMap : WorkCompEntityTypeConfiguration<Panel>
    {
        public PanelMap()
        {

            // Properties
            Property(t => t.Name)
                .IsRequired()
                .HasMaxLength(100);

            // Table & Column Mappings
            ToTable("Panel");
            Property(t => t.Name).HasColumnName("Name");
        }
    }
}