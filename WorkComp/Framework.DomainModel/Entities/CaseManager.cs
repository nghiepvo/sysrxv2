﻿using System.Collections.Generic;
using System.Collections.ObjectModel;
using Framework.DataAnnotations;

namespace Framework.DomainModel.Entities
{
    public class CaseManager : Entity
    {
        public CaseManager()
        {
            Referrals = new Collection<Referral>();
        }

        public int ClaimNumberId { get; set; }
        public virtual ClaimNumber ClaimNumber { get; set; }

        [LocalizeRequired, LocalizeMaxLength(100)]
        public string Name { get; set; }

        [LocalizeMaxLength(100)]
        public string Agency { get; set; }

        [LocalizeRequired, LocalizePhone]
        public string Phone { get; set; }

        [LocalizeRequired, LocalizeEmailAddress]
        public string Email { get; set; }

        [LocalizePhone]
        public string Fax { get; set; }

        public string SpecialInstructions { get; set; }

        [LocalizeRequired]
        public string Address { get; set; }

        public int? StateId { get; set; }
        public virtual State State { get; set; }
        public int? CityId { get; set; }
        public virtual City City { get; set; }
        public int? ZipId { get; set; }
        public virtual Zip Zip { get; set; }
        public virtual ICollection<Referral> Referrals { get; set; }
    }
}
