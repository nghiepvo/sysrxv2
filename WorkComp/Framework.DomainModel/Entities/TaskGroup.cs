﻿using System.Collections.Generic;
using Framework.DataAnnotations;

namespace Framework.DomainModel.Entities
{
    public class TaskGroup:Entity
    {
        public TaskGroup()
        {
            TaskGroupTaskTemplates = new List<TaskGroupTaskTemplate>();
        }
        [LocalizeRequired]
        [LocalizeMaxLength(200)]
        public string Name { get; set; }
        public bool? IsDefault { get; set; }
        public virtual ICollection<TaskGroupTaskTemplate> TaskGroupTaskTemplates { get; set; }

    }
}