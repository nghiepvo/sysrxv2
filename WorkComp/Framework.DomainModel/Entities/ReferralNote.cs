using System;

namespace Framework.DomainModel.Entities
{
    public class ReferralNote : Entity
    {
        public string Comment { get; set; }
        public int ReferralId { get; set; }
        public int? NoteType { get; set; }
        public int AssignToId { get; set; }
        public virtual Referral Referral { get; set; }
        public virtual User User { get; set; }
    }
}