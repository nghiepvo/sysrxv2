using System.Collections.Generic;
using System.Collections.ObjectModel;
using Framework.DataAnnotations;

namespace Framework.DomainModel.Entities
{
    public class City : Entity
    {
        public City()
        {
            Zips = new Collection<Zip>();
            NpiNumbers = new Collection<NpiNumber>();
            Attorneys = new Collection<Attorney>();
            CollectionSites = new Collection<CollectionSite>();
            ReferralSources = new Collection<ReferralSource>();
            Payers = new List<Payer>();
            Branchs = new List<Branch>();
            Users = new Collection<User>();
            Claimants = new Collection<Claimant>();
            Adjusters = new Collection<Adjuster>();
            CaseManagers = new Collection<CaseManager>();
        }

        [LocalizeRequired]
        [LocalizeMaxLength(100)]
        public string Name { get; set; }
        public int StateId { get; set; }
        public virtual State State { get; set; }
        public virtual ICollection<Zip> Zips { get; set; }
        public virtual ICollection<Employer> Employers { get; set; }
        public virtual ICollection<NpiNumber> NpiNumbers { get; set; }
        public virtual ICollection<Attorney> Attorneys { get; set; }
        public virtual ICollection<Payer> Payers { get; set; }
        public virtual ICollection<CollectionSite> CollectionSites { get; set; }
        public virtual ICollection<ReferralSource> ReferralSources { get; set; }
        public virtual ICollection<Branch> Branchs { get; set; }
        public virtual ICollection<User> Users { get; set; }
        public virtual ICollection<Claimant> Claimants { get; set; }
        public virtual ICollection<Adjuster> Adjusters { get; set; }
        public virtual ICollection<CaseManager> CaseManagers { get; set; }
    }
}