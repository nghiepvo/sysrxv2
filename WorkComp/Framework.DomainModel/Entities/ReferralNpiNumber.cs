﻿using System;

namespace Framework.DomainModel.Entities
{
    public class ReferralNpiNumber:Entity
    {
        public int? NpiNumberId { get; set; }
        public int ReferralId { get; set; }
        public DateTime? NextMdVisitDate { get; set; }
        public string SpecialHandling { get; set; }
        public string Phone { get; set; }
        public string Fax { get; set; }
        public string Address { get; set; }
        public string Email { get; set; }
        public virtual NpiNumber NpiNumber { get; set; }
        public virtual Referral Referral { get; set; }
    }
}