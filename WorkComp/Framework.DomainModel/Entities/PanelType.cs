using System.Collections.Generic;
using System.Collections.ObjectModel;
using Framework.DataAnnotations;

namespace Framework.DomainModel.Entities
{
    public class PanelType:Entity
    {
        public PanelType()
        {
            PanelCodes = new Collection<PanelCode>();
            Referrals=new Collection<Referral>();
        }
        [LocalizeRequired]
        [LocalizeMaxLength(100)]
        public string Name { get; set; }
        [LocalizeMaxLength(50)]
        public string Code { get; set; }
        public int? PayerId { get; set; }
        public bool? IsBasic { get; set; }
        public virtual ICollection<PanelCode> PanelCodes { get; set; }
        public virtual ICollection<Referral> Referrals { get; set; }
        public virtual Payer Payer { get; set; }
    }
}