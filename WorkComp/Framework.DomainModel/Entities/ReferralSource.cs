﻿using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using Framework.DataAnnotations;

namespace Framework.DomainModel.Entities
{
    public class ReferralSource : Entity
    {
        public ReferralSource()
        {
            Referrals=new Collection<Referral>();
        }
        [LocalizeRequired]
        public string FirstName { get; set; }
        [LocalizeRequired]
        public string LastName { get; set; }
        public string MiddleName { get; set; }
        public string Company { get; set; }
        [LocalizeRequired, LocalizePhone]
        public string Phone { get; set; }
        public string Extension { get; set; }
        [LocalizePhone]
        public string Fax { get; set; }

        [LocalizeEmailAddress]
        public string Email { get; set; }
        [LocalizeRequired]
        public string Address { get; set; }
        
        public int? StateId { get; set; }
        public virtual State State { get; set; }
        public int? CityId { get; set; }
        public virtual City City { get; set; }
        public int? ZipId { get; set; }
        public virtual Zip Zip { get; set; }

        public int? ReferralTypeId { get; set; }
        public virtual ReferralType ReferralType { get; set; }

        public bool IsAuthorization { get; set; }
        public DateTime? AuthorizationFrom { get; set; }
        public DateTime? AuthorizationTo { get; set; }
        public int? NoAuthorizationType { get; set; }
        public DateTime? OneTimeDate { get; set; }
        public virtual ICollection<Referral> Referrals { get; set; }
    }
}
