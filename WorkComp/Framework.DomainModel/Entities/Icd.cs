using System.Collections.Generic;
using System.Collections.ObjectModel;
using Framework.DataAnnotations;

namespace Framework.DomainModel.Entities
{
    public class Icd : Entity
    {
        public Icd()
        {
            ReferralIcds = new Collection<ReferralIcd>();
        }
        public int? IcdTypeId { get; set; }
        [LocalizeRequired]
        [LocalizeMaxLength(50)]
        public string Code { get; set; }
        [LocalizeRequired]
        public string Description { get; set; }
        public virtual IcdType IcdType { get; set; }
        public virtual ICollection<ReferralIcd> ReferralIcds { get; set; }
    }
}