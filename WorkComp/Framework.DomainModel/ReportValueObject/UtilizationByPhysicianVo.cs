﻿using System;
using Framework.Utility;

namespace Framework.DomainModel.ReportValueObject
{
    public class UtilizationByPhysicianVo
    {
        public int? TreatingPhysicianId { get; set; }
        public string PhysiscianState { get; set; }
        public string TreatingPhysicianName { get; set; }
        public string TreatingPhysicianPhone { get; set; }
        public string TreatingPhysicianNpi { get; set; }
        public double? ConsistentCompletedTests { get; set; }
        public double? InConsistentCompletedTests { get; set; }
        public double? TotalCompletedTests
        {
            get
            {
                return ConsistentCompletedTests + InConsistentCompletedTests;
            }
        }

        public double? ConsistentResultsRatio
        {
            get
            {
                return TotalCompletedTests==0?0:Math.Round(DataTypeProtectExtension.ProtectDouble((ConsistentCompletedTests / TotalCompletedTests) * 100, 0), 1);
            }
        }
        public double? InConsistentResultsRatio
        {
            get
            {
                return TotalCompletedTests == 0 ? 0 : Math.Round(DataTypeProtectExtension.ProtectDouble((InConsistentCompletedTests / TotalCompletedTests) * 100, 0),1);
            }
        }

        public double? NumHightRisk { get; set; }
        public double? HightRisk
        {
            get
            {
                return TotalCompletedTests == 0 ? 0 : Math.Round(DataTypeProtectExtension.ProtectDouble((NumHightRisk / TotalCompletedTests) * 100, 0), 1);
            }
        }
        public double? NumMediumRisk { get; set; }
        public double? MediumRisk
        {
            get
            {
                return TotalCompletedTests == 0 ? 0 : Math.Round(DataTypeProtectExtension.ProtectDouble((NumMediumRisk / TotalCompletedTests) * 100, 0), 1);
            }
        }
        public double? NumLowRisk { get; set; }
        public double? LowRisk
        {
            get
            {
                return TotalCompletedTests == 0 ? 0 : Math.Round(DataTypeProtectExtension.ProtectDouble((NumLowRisk / TotalCompletedTests) * 100, 0), 1);
            }
        }

        public string PayerName { get; set; }
        public string PayerAddress { get; set; }
        public DateTime? DateFrom { get; set; }
        public DateTime? DateTo { get; set; }
        public int? TotalPage { get; set; }
        public int? TotalRow { get; set; }
    }
}
