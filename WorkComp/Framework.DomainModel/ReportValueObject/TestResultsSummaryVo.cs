﻿using System;

namespace Framework.DomainModel.ReportValueObject
{
    public class TestResultsSummaryVo
    {
        public string PayerName { get; set; }
        public string PayerAddress { get; set; }

        public int? ConsistentCompletedTests { get; set; }
        public int? InConsistentCompletedTests { get; set; }
        public double? TotalCompletedTests {
            get
            {
                return ConsistentCompletedTests + InConsistentCompletedTests;
            }
        }


        public double? ConsistentResultsRatio {
            get
            {
                return TotalCompletedTests == 0 ? 0 : Math.Round(Utility.DataTypeProtectExtension.ProtectDouble((ConsistentCompletedTests / TotalCompletedTests) * 100, 0), 1);
            }
        }
        public double? InConsistentResultsRatio
        {
            get
            {
                return TotalCompletedTests == 0 ? 0 : Math.Round(Utility.DataTypeProtectExtension.ProtectDouble((InConsistentCompletedTests / TotalCompletedTests) * 100, 0), 1);
            }
        }

        public int? NumReportedPrescriptionMedNotDetected { get; set; }
        public double? ReportedPrescriptionMedNotDetected
        {
            get
            {
                return InConsistentCompletedTests == 0 ? 0 : Math.Round(Utility.DataTypeProtectExtension.ProtectDouble((NumReportedPrescriptionMedNotDetected / InConsistentCompletedTests) * 100, 0), 1);
            }
        }

        public int? NumNonReportedPrescriptionMedsDetected { get; set; }
        public double? NonReportedPrescriptionMedsDetected
        {
            get
            {
                return InConsistentCompletedTests == 0 ? 0 : Math.Round(Utility.DataTypeProtectExtension.ProtectDouble((NumNonReportedPrescriptionMedsDetected / InConsistentCompletedTests) * 100, 0), 1);
            }
        }

        public int? NumWithAlcoholPresent { get; set; }
        public double? WithAlcoholPresent
        {
            get
            {
                return InConsistentCompletedTests == 0 ? 0 : Math.Round(Utility.DataTypeProtectExtension.ProtectDouble((NumWithAlcoholPresent / InConsistentCompletedTests) * 100, 0), 1);
            }
        }

        public int? NumWithIllicitDrugsPresent { get; set; }
        public double? WithIllicitDrugsPresent
        {
            get
            {
                return InConsistentCompletedTests == 0 ? 0 : Math.Round(Utility.DataTypeProtectExtension.ProtectDouble((NumWithIllicitDrugsPresent / InConsistentCompletedTests) * 100, 0), 1);
            }
        }

        public int? NumWithInvalidSample { get; set; }
        public double? WithInvalidSample
        {
            get
            {
                return InConsistentCompletedTests == 0 ? 0 : Math.Round(Utility.DataTypeProtectExtension.ProtectDouble((NumWithInvalidSample / InConsistentCompletedTests) * 100, 0), 1);
            }
        }


        public int? NumNoMedicationHistoryProvided { get; set; }
        public double? NoMedicationHistoryProvided
        {
            get
            {
                return InConsistentCompletedTests == 0 ? 0 : Math.Round(Utility.DataTypeProtectExtension.ProtectDouble((NumNoMedicationHistoryProvided / InConsistentCompletedTests) * 100, 0), 1);
            }
        }

        public int? NumConsistentTestsPerClaimantDoiSmall3Month { get; set; }
        public double? ConsistentTestsPerClaimantDoiSmall3Month
        {
            get
            {
                return (NumConsistentTestsPerClaimantDoiSmall3Month + NumInConsistentTestsPerClaimantDoiSmall3Month) == 0 ? 0 : Math.Round(Utility.DataTypeProtectExtension.ProtectDouble((NumConsistentTestsPerClaimantDoiSmall3Month / (NumConsistentTestsPerClaimantDoiSmall3Month + NumInConsistentTestsPerClaimantDoiSmall3Month)) * 100, 0), 1);
            }
        }
        public int? NumInConsistentTestsPerClaimantDoiSmall3Month { get; set; }
        public double? InConsistentTestsPerClaimantDoiSmall3Month
        {
            get
            {
                return (NumConsistentTestsPerClaimantDoiSmall3Month + NumInConsistentTestsPerClaimantDoiSmall3Month) == 0 ? 0 : Math.Round(Utility.DataTypeProtectExtension.ProtectDouble((NumInConsistentTestsPerClaimantDoiSmall3Month / (NumConsistentTestsPerClaimantDoiSmall3Month + NumInConsistentTestsPerClaimantDoiSmall3Month)) * 100, 0), 1);
            }
        }
        public int? NumConsistentTestsPerClaimantDoiLarge3MonthAndSmall6Month { get; set; }
        public double? ConsistentTestsPerClaimantDoiLarge3MonthAndSmall6Month
        {
            get
            {
                return (NumConsistentTestsPerClaimantDoiLarge3MonthAndSmall6Month + NumInConsistentTestsPerClaimantDoiLarge3MonthAndSmall6Month) == 0 ? 0 : Math.Round(Utility.DataTypeProtectExtension.ProtectDouble((NumConsistentTestsPerClaimantDoiLarge3MonthAndSmall6Month / (NumConsistentTestsPerClaimantDoiLarge3MonthAndSmall6Month + NumInConsistentTestsPerClaimantDoiLarge3MonthAndSmall6Month)) * 100, 0), 1);
            }
        }
        public int? NumInConsistentTestsPerClaimantDoiLarge3MonthAndSmall6Month { get; set; }
        public double? InConsistentTestsPerClaimantDoiLarge3MonthAndSmall6Month
        {
            get
            {
                return (NumConsistentTestsPerClaimantDoiLarge3MonthAndSmall6Month + NumInConsistentTestsPerClaimantDoiLarge3MonthAndSmall6Month) == 0 ? 0 : Math.Round(Utility.DataTypeProtectExtension.ProtectDouble((NumInConsistentTestsPerClaimantDoiLarge3MonthAndSmall6Month / (NumConsistentTestsPerClaimantDoiLarge3MonthAndSmall6Month + NumInConsistentTestsPerClaimantDoiLarge3MonthAndSmall6Month)) * 100, 0), 1);
            }
        }
        public int? NumConsistentTestsPerClaimantDoiLarge6MonthAndSmall12Month { get; set; }
        public double? ConsistentTestsPerClaimantDoiLarge6MonthAndSmall12Month
        {
            get
            {
                return (NumConsistentTestsPerClaimantDoiLarge6MonthAndSmall12Month + NumInConsistentTestsPerClaimantDoiLarge6MonthAndSmall12Month) == 0 ? 0 : Math.Round(Utility.DataTypeProtectExtension.ProtectDouble((NumConsistentTestsPerClaimantDoiLarge6MonthAndSmall12Month / (NumConsistentTestsPerClaimantDoiLarge6MonthAndSmall12Month + NumInConsistentTestsPerClaimantDoiLarge6MonthAndSmall12Month)) * 100, 0), 1);
            }
        }
        public int? NumInConsistentTestsPerClaimantDoiLarge6MonthAndSmall12Month { get; set; }
        public double? InConsistentTestsPerClaimantDoiLarge6MonthAndSmall12Month
        {
            get
            {
                return (NumConsistentTestsPerClaimantDoiLarge6MonthAndSmall12Month + NumInConsistentTestsPerClaimantDoiLarge6MonthAndSmall12Month) == 0 ? 0 : Math.Round(Utility.DataTypeProtectExtension.ProtectDouble((NumInConsistentTestsPerClaimantDoiLarge6MonthAndSmall12Month / (NumConsistentTestsPerClaimantDoiLarge6MonthAndSmall12Month + NumInConsistentTestsPerClaimantDoiLarge6MonthAndSmall12Month)) * 100, 0), 1);
            }
        }
        public int? NumConsistentTestsPerClaimantDoiLarge12Month { get; set; }
        public double? ConsistentTestsPerClaimantDoiLarge12Month
        {
            get
            {
                return (NumConsistentTestsPerClaimantDoiLarge12Month + NumInConsistentTestsPerClaimantDoiLarge12Month) == 0 ? 0 : Math.Round(Utility.DataTypeProtectExtension.ProtectDouble((NumConsistentTestsPerClaimantDoiLarge12Month / (NumConsistentTestsPerClaimantDoiLarge12Month + NumInConsistentTestsPerClaimantDoiLarge12Month)) * 100, 0), 1);
            }
        }
        public int? NumInConsistentTestsPerClaimantDoiLarge12Month { get; set; }
        public double? InConsistentTestsPerClaimantDoiLarge12Month
        {
            get
            {
                return (NumConsistentTestsPerClaimantDoiLarge12Month + NumInConsistentTestsPerClaimantDoiLarge12Month) == 0 ? 0 : Math.Round(Utility.DataTypeProtectExtension.ProtectDouble((NumInConsistentTestsPerClaimantDoiLarge12Month / (NumConsistentTestsPerClaimantDoiLarge12Month + NumInConsistentTestsPerClaimantDoiLarge12Month)) * 100, 0), 1);
            }
        }


        public int? NumConsistentUrine { get; set; }
        public double? ConsistentUrine
        {
            get
            {
                return (NumConsistentUrine + NumInConsistentUrine) == 0 ? 0 : Math.Round(Utility.DataTypeProtectExtension.ProtectDouble(((NumConsistentUrine) / (NumConsistentUrine + NumInConsistentUrine)) * 100, 0), 1);
            }
        }
        public int? NumInConsistentUrine { get; set; }
        public double? InConsistentUrine
        {
            get
            {
                return (NumConsistentUrine + NumInConsistentUrine) == 0 ? 0 : Math.Round(Utility.DataTypeProtectExtension.ProtectDouble(((NumInConsistentUrine) / (NumConsistentUrine + NumInConsistentUrine)) * 100, 0), 1);
            }
        }

        public int? NumConsistentOral { get; set; }
        public double? ConsistentOral
        {
            get
            {
                return (NumConsistentOral + NumInConsistentOral) == 0 ? 0 : Math.Round(Utility.DataTypeProtectExtension.ProtectDouble(((NumConsistentOral) / (NumConsistentOral + NumInConsistentOral)) * 100, 0), 1);
            }
        }
        public int? NumInConsistentOral { get; set; }
        public double? InConsistentOral
        {
            get
            {
                return (NumConsistentOral + NumInConsistentOral) == 0 ? 0 : Math.Round(Utility.DataTypeProtectExtension.ProtectDouble(((NumInConsistentOral) / (NumConsistentOral + NumInConsistentOral)) * 100, 0), 1);
            }
        }

        public int? NumConsistentBlood { get; set; }
        public double? ConsistentBlood
        {
            get
            {
                return (NumConsistentBlood + NumInConsistentBlood) == 0 ? 0 : Math.Round(Utility.DataTypeProtectExtension.ProtectDouble(((NumConsistentBlood) / (NumConsistentBlood + NumInConsistentBlood)) * 100, 0), 1);
            }
        }
        public int? NumInConsistentBlood { get; set; }
        public double? InConsistentBlood
        {
            get
            {
                return (NumConsistentBlood + NumInConsistentBlood) == 0 ? 0 : Math.Round(Utility.DataTypeProtectExtension.ProtectDouble(((NumInConsistentBlood) / (NumConsistentBlood + NumInConsistentBlood)) * 100, 0), 1);
            }
        }

        public int? NumConsistentHair { get; set; }
        public double? ConsistentHair
        {
            get
            {
                return (NumConsistentHair + NumInConsistentHair) == 0 ? 0 : Math.Round(Utility.DataTypeProtectExtension.ProtectDouble(((NumConsistentHair) / (NumConsistentHair + NumInConsistentHair)) * 100, 0), 1);
            }
        }
        public int? NumInConsistentHair { get; set; }
        public double? InConsistentHair
        {
            get
            {
                return (NumConsistentHair + NumInConsistentHair) == 0 ? 0 : Math.Round(Utility.DataTypeProtectExtension.ProtectDouble(((NumInConsistentHair) / (NumConsistentHair + NumInConsistentHair)) * 100, 0), 1);
            }
        }


        public DateTime? DateFrom { get; set; }
        public DateTime? DateTo { get; set; }
    }
}
