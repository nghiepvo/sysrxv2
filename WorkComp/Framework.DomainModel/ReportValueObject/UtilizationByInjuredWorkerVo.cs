﻿using System;
namespace Framework.DomainModel.ReportValueObject
{
    public class UtilizationByInjuredWorkerVo
    {
        public int? ReferralId { get; set; }
        public string ClaimNumber { get; set; }
        public string PayerName { get; set; }
        public string ClaimantFirstName { get; set; }
        public string ClaimantLastName { get; set; }
        public string AdjusterName { get; set; }
        public string ReferralSourceName { get; set; }
        public string EmployerName { get; set; }
        public string EmployerLocation { get; set; }
        public string Jurisdiction { get; set; }
        public DateTime? InjuryDate { get; set; }
        public string Icd910 { get; set; }
        public string TreatingPhysician { get; set; }
        public DateTime? LastCompletedTestDate { get; set; }
        public int Consistent { get; set; }
        public int InConsistent { get; set; }

        public int ReportedDetected { get; set; }
        public int ReportedNotDetected { get; set; }
        public int NotReportedDetected { get; set; }
        public int AlcoholDetected { get; set; }
        public int IllicitsDetected { get; set; }
        public int InvalidSample { get; set; }
        public int NoMedication { get; set; }
        public string TestingSchedule { get; set; }
    }
}
