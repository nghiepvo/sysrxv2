﻿using System;

namespace Framework.DomainModel.ReportValueObject
{
    public class CancellationPhysicianVo
    {
        public int? ClaimNumberId { get; set; }
        public string ClaimNumber { get; set; }
        public int? ReferralId { get; set; }
        public string PayerName { get; set; }
        public string Criteria { get; set; }
        public string TreatingPhysicianNpi { get; set; }
        public string TreatingPhysicianName { get; set; }
        public string TreatingPhysicianAddress { get; set; }
        public string TreatingPhysicianCity { get; set; }
        public string TreatingPhysicianState { get; set; }
        public string TreatingPhysicianZip { get; set; }
        public string TreatingPhysicianPhone { get; set; }
        public string CancelDate { get; set; }
        public string CancelReason { get; set; }
        public DateTime? DateFrom { get; set; }
        public DateTime? DateTo { get; set; }
        public int? TotalPage { get; set; }
        public int? TotalRow { get; set; }
    }
}
