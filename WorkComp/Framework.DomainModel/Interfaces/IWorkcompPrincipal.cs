﻿using System.Security.Principal;
using Framework.DomainModel.Entities;

namespace Framework.DomainModel.Interfaces
{
    public interface IWorkcompPrincipal : IPrincipal
    {
        string AuthToken { get; set; }
        new WorkcompIdentity Identity { get; set; }
        User User { get; set; }
    }
}
