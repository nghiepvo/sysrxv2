﻿namespace Framework.DomainModel.ValueObject
{
    public class AssayCodeDescriptionGridVo : ReadOnlyGridVo
    {
        public string Description { get; set; }
    }
}
