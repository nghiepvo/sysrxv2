﻿namespace Framework.DomainModel.ValueObject
{
    public class CityGridVo : ReadOnlyGridVo
    {
        public string Name { get; set; }
        public string State { get; set; }
    }
}
