﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Net;
using System.Text;
using System.Threading.Tasks;
using Framework.Utility;

namespace Framework.DomainModel.ValueObject
{
    public class SendMailWithReferralIdVo
    {
        public int ReferralId { get; set; }
        public string ClaimNumber { get; set; }
        public string Claimant { get; set; }
        private string _fileName;

        public string FileName
        {
            get
            {
                if (string.IsNullOrEmpty(_fileName)) return string.Empty;
                var pathWeb = Path.Combine(AppDomain.CurrentDomain.BaseDirectory, "FileUpload", "ResultPdf", _fileName);
                return File.Exists(pathWeb) ? _fileName : string.Empty;
            }
            set { _fileName = value; }
        }
    }
}
