﻿using System;
using System.Collections.Generic;
using System.Linq;
using Framework.DomainModel.Entities;

namespace Framework.DomainModel.ValueObject
{
    public class ReferralResultDetailVo
    {
        public int ReferralId { get; set; }
        public string ClaimNumber { get; set; }
        public string Claimant { get; set; }
        public string HeaderReult { get; set; }
        public string FileResult { get; set; }
        public bool IsResult { get; set; }
        public string ContentHtml { get; set; }
    }
}
