﻿namespace Framework.DomainModel.ValueObject
{
    public class EmailTemplateGridVo : ReadOnlyGridVo
    {
        public string Title { get; set; }
        public string Subject { get; set; }
        public bool IsService { get; set; }
        public bool IsActive { get; set; }
        public string Active
        {
            get
            {
                return IsActive ? "Inactive" : "Active";
            }
        }


    }
}
