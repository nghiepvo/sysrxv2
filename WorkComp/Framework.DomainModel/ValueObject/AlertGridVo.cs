﻿using System;
using System.Globalization;
using Framework.DomainModel.Entities.Common;
using Framework.Utility;

namespace Framework.DomainModel.ValueObject
{
    public class AlertGridVo : ReadOnlyGridVo
    {
        public int Type { get; set; }

        public string TypeStr
        {
            get
            {
                return XmlDataHelpper.Instance.GetValue(XmlDataTypeEnum.TypeAlert.ToString(), Type.ToString(CultureInfo.InvariantCulture));
            }
        }

        public int LinkId { get; set; }

        public string Object
        {
            get
            {

                return string.Format("{0}: {1}", TypeStr, LinkId);
            }
        }

        public string Title { get; set; }
        public string Message { get; set; }
        public DateTime? CreatedOn { get; set; }

        public string CreatedDate
        {
            get { return CreatedOn.GetValueOrDefault().ToString("g"); }
        }

        public bool IsRush { get; set; }

        public string Status
        {
            get { return "Open"; }
        }

        public string ModelName
        {
            get { return Type == 1 ? "Referral" : "ReferralTask"; }
        }

    }
}
