﻿namespace Framework.DomainModel.ValueObject
{
    public class LookupItemVo
    {
        public int KeyId { get; set; }
        public string DisplayName { get; set; }
    }
    public class LookupInGridDataSourceViewModel
    {
        public string Name { get; set; }
        public int KeyId { get; set; }
    }
}
