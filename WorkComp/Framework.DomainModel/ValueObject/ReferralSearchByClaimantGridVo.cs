﻿using System;

namespace Framework.DomainModel.ValueObject
{
    public class ReferralSearchByClaimantGridVo : ReadOnlyGridVo
    {
        public string PatientName { get; set; }
        public string Jurisdiction { get; set; }
        public string ClaimNumber { get; set; }
        public string PayerName { get; set; }
        public string AdjusterName { get; set; }
        public string Doi { get; set; }
        public string Status { get; set; }
        public int? ClaimNumberId { get; set; }
        public int? ReferralId { get; set; }
    }

    public class ReferralSearchByBusinessReportGridVo : ReadOnlyGridVo
    {
        public int? ReferralId { get; set; }
        public string ProductType { get; set; }
        public string Status { get; set; }
        public string PayerName { get; set; }
        public string DueDate { get; set; }
        public string Completed { get; set; }
        public string ClaimantName { get; set; }
        public string ClaimNumber { get; set; }
        public string Doi { get; set; }
        public string Nov { get; set; }
        public string CreatedDate { get; set; }
    }

    public class ReferralPhysicianSearchGridVo : ReadOnlyGridVo
    {
        public int? ReferralId { get; set; }
        public string CreatedDate { get; set; }
        public string FirstName { get; set; }
        public string MiddleName { get; set; }
        public string LastName { get; set; }
        public string Address { get; set; }
        public string City { get; set; }
        public string State { get; set; }
        public string PostalCode { get; set; }
        public string Phone { get; set; }
        public string Fax { get; set; }
        public string Email { get; set; }
        public string NpiNumber { get; set; }
        
    }

    public class ReferralTestResultSearchGridVo : ReferralSearchByBusinessReportGridVo
    {
        public string ConsistencyCmt { get; set; }
        public string ResultStatus { get; set; }
    }
}