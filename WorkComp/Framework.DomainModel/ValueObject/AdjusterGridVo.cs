﻿using Framework.Utility;

namespace Framework.DomainModel.ValueObject
{
    public class AdjusterGridVo : ReadOnlyGridVo
    {

        public string FirstName { get; set; }
        public string MiddleName { get; set; }
        public string LastName { get; set; }

        public string Name
        {
            get
            {
                return FirstName + " " + MiddleName + " " + LastName;
            }
        }
        public string Branch { get; set; }

        public string State { get; set; }
        public string City { get; set; }
        public string Zip { get; set; }
        public string Address { get; set; }

        public string FullAddress
        {
            get { return CaculatorHelper.CaculateFormatFullAddress(Address, State, City, Zip); }
        }

        public string Email { get; set; }
        private string _phone;

        public string Phone
        {
            get
            {
                return _phone.ApplyFormatPhone();
            }
            set
            {
                _phone = value;
            } 
        }

        public string Extension { get; set; }
        public bool AllowCreateReferral { get; set; }
        public bool OptedOutSendMail { get; set; }
    }
}
