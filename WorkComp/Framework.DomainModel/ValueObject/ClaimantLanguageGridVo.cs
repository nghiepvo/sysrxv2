﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Framework.DomainModel.ValueObject
{
    public class ClaimantLanguageGridVo : ReadOnlyGridVo
    {
        public string Name { get; set; }
    }
}
