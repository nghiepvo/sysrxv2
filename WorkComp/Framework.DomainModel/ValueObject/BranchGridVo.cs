﻿using Framework.Utility;

namespace Framework.DomainModel.ValueObject
{
    public class BranchGridVo : ReadOnlyGridVo
    {
        public string Name { get; set; }
        public string Payer { get; set; }
        public string ManagerName { get; set; }

        private string _phone;
        public string Phone
        {
            get
            {
                return _phone.ApplyFormatPhone();
            }
            set
            {
                _phone = value;
            }
        }
        
        public string Fax { get; set; }
        public string Address1 { get; set; }
        public string Address2 { get; set; }
        public string State { get; set; }
        public string City { get; set; }
        public string Zip { get; set; }

        public string FullAddress
        {
            get
            { return CaculatorHelper.CaculateFormatFullAddress(Address1, State, City, Zip, Address2); }
        }
    }
}
