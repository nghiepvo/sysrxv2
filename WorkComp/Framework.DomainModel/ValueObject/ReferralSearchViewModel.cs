﻿using System;
using System.Collections.Generic;

namespace Framework.DomainModel.ValueObject
{
    public class ReferralSearchViewModel
    {
        public string TypeSearch { get; set; }
        public string ClaimantName { get; set; }
        public string ClaimantSsn { get; set; }
        public DateTime? ClaimantDob { get; set; }
        public DateTime? ClaimantDoi { get; set; }
        public string ClaimNumber { get; set; }
        public string PayerName { get; set; }
        public int? ReferralId { get; set; }
        public string ControlNumber { get; set; }
        public string ProductType { get; set; }
        public string AdjusterName { get; set; }
        public string CreatedBy { get; set; }
        public bool? IsCompletedReferral { get; set; }
        public bool? IsOpenReferral { get; set; }
        public bool? IsCancelReferral { get; set; }
        public DateTime? StartCreatedReferral { get; set; }
        public DateTime? EndCreatedReferral { get; set; }
        public DateTime? StartNovDateReferral { get; set; }
        public DateTime? EndNovDateReferral { get; set; }
        public DateTime? StartTestResultReferral { get; set; }
        public DateTime? EndTestResultReferral { get; set; }

        public DateTime? StartDatePhysicianReferral { get; set; }
        public DateTime? EndDatePhysicianReferral { get; set; }

        public List<string> SelectedTestResultSearchCondition { get; set; } 

        public string ReferralClaimantName { get; set; }
        public string ReferralClaimantSsn { get; set; }
        public DateTime? ReferralClaimantDob { get; set; }
        public DateTime? ReferralClaimantDoi { get; set; }
        public string ReferralClaimNumber { get; set; }
        public string ReferralPayerName { get; set; }
    }

}