﻿namespace Framework.DomainModel.ValueObject
{
    public class ProductTypeGridVo : ReadOnlyGridVo
    {
        public string Name { get; set; }
        public string Payer { get; set; }
    }
}