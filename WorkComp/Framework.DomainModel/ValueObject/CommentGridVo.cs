﻿using System;

namespace Framework.DomainModel.ValueObject
{
    public class CommentGridVo : ReadOnlyGridVo
    {
        public string CreatedByFistName { get; set; }
        public string CreatedByMiddleName { get; set; }
        public string CreatedByLastName { get; set; }
        public string CreatedBy
        {
            get
            {
                return CreatedByFistName + " " + (string.IsNullOrEmpty(CreatedByMiddleName) ? "" : CreatedByMiddleName + " ") + CreatedByLastName;
            }
        }
        public string Comment { get; set; }

        public string CreateDate
        {
            get { return CreatedDateDateTime.ToString("g"); }
        }

        public DateTime CreatedDateDateTime { get; set; }
    }
}