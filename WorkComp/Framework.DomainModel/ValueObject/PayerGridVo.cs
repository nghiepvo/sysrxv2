﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Framework.DomainModel.ValueObject
{
    public class PayerGridVo : ReadOnlyGridVo
    {
        public string Name { get; set; }
        public string ControlNumberPrefix { get; set; }
        public string SpecialInstructions { get; set; }
        public string Address { get; set; }
        public string City { get; set; }
        public string State { get; set; }
        public string Zip { get; set; }
    }
}
