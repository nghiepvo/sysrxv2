﻿namespace Framework.DomainModel.ValueObject
{
    public class LookupItemDictionaryVo
    {
        public string KeyId { get; set; }
        public string DisplayName { get; set; }
    }
}
