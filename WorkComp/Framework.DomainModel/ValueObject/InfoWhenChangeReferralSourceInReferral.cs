﻿using System;
using System.Collections.Generic;
using Framework.DomainModel.Entities.Common;
using Framework.Utility;

namespace Framework.DomainModel.ValueObject
{
    public class InfoWhenChangeReferralSourceInReferral
    {
        public int? ReferralSourceId { get; set; }
        public string ReferralType { get; set; }
        public string ReferralCompany { get; set; }
        public string ReferralSourceName { get;set; }
        public string Phone { get; set; }

        public string ReferralPhone
        {
            get
            {
                return Phone.ApplyFormatPhone();
            }
        }

        public string ReferralExtention { get; set; }
        public string Fax { get; set; }
        public string ReferralFax
        {
            get
            {
                return Fax.ApplyFormatPhone();
            }
        }
        public string ReferralEmail { get; set; }
        public string ReferralAddress { get; set; }
        public string ReferralState { get; set; }
        public string ReferralCity { get; set; }
        public string ReferralZip { get; set; }
        public DateTime? ReferralClaimDateFrom { get; set; }

        public string ReferralClaimDateFromStr
        {
            get
            {
                return ReferralClaimDateFrom == null ? "" : ReferralClaimDateFrom.GetValueOrDefault().ToString("MM/dd/yyyy");
            }
        }
        public DateTime? ReferralClaimDateTo { get; set; }
        public string ReferralClaimDateToStr
        {
            get
            {
                return ReferralClaimDateTo == null ? "" : ReferralClaimDateTo.GetValueOrDefault().ToString("MM/dd/yyyy");
            }
        }
        public bool IsAuthorization { get; set; }

        public KeyValuePair<int, string> Authorization
        {
            get
            {
                if (!IsAuthorization)
                {
                    return new KeyValuePair<int, string>(1,  XmlDataHelpper.Instance.GetValue(XmlDataTypeEnum.ReferralSourceAuthorization.ToString(), "1"));
                }

                var dateFrom = ReferralClaimDateFrom == null
                    ? DateTime.MinValue
                    : ReferralClaimDateFrom.GetValueOrDefault();
                var dateTo = ReferralClaimDateTo == null
                    ? DateTime.MaxValue
                    : ReferralClaimDateTo.GetValueOrDefault().AddDays(1).AddSeconds(-1);

                if (DateTime.Now >= dateFrom && DateTime.Now <= dateTo)
                {
                    return new KeyValuePair<int, string>(2, XmlDataHelpper.Instance.GetValue(XmlDataTypeEnum.ReferralSourceAuthorization.ToString(), "2"));
                }
                return new KeyValuePair<int, string>(3, XmlDataHelpper.Instance.GetValue(XmlDataTypeEnum.ReferralSourceAuthorization.ToString(), "3"));
            }
        }
    }


    public class InfoWhenChangeClaimNumberInReferral
    {
        public int? ClaimNumberId { get; set; }
        public DateTime? Doi { get; set; }
        public string ClaimNumberDoi
        {
            get
            {
                return Doi == null ? "" : Doi.GetValueOrDefault().ToString("MM/dd/yyyy");
            }
        }

        public string ClaimNumberJurisdiction { get; set; }
        public string ClaimNumberInstruction { get; set; }
        public int PayerId { get; set; }
        public int? BranchId { get; set; }
        public int ClaimantId { get; set; }
        public int AdjusterId { get; set; }
        public int JurisdictionId { get; set; }
        public int EmployerId { get; set; }
        public int? StatusId { get; set; }

        public string ClaimNumberStatus
        {
            get
            {
                if (StatusId.GetValueOrDefault() == 0)
                {
                    return "";
                }
                return XmlDataHelpper.Instance.GetValue(XmlDataTypeEnum.ClaimNumberStatus.ToString(), StatusId.ToString());
            }
        }

    }

    public class InfoWhenChangePayerInReferral
    {
        public int? PayerId { get; set; }
        public string PayerInstruction { get; set; }
    }

    public class InfoWhenChangeAdjusterInReferral
    {
        public int? AdjusterId { get; set; }
        public string AdjusterExternalId { get; set; }
        public string AdjusterEmail { get; set; }
        public string Phone { get; set; }

        public string AdjusterPhone
        {
            get
            {
                return Phone.ApplyFormatPhone();
            }
        }

        public string AdjusterExtension { get; set; }
        public string AdjusterAddress { get; set; }
        public string AdjusterState { get; set; }
        public string AdjusterCity { get; set; }
        public string AdjusterZip { get; set; }
        public bool? AllowCreateReferral { get; set; }
    }

    public class InfoWhenChangeClaimantInReferral
    {
        public int? ClaimantId { get; set; }
        public string ClaimantPayerPatientId { get; set; }
        public string HomePhone { get; set; }

        public string ClaimantHomePhone
        {
            get
            {
                return HomePhone.ApplyFormatPhone();
            }
        }
        public string CellPhone { get; set; }

        public string ClaimantCellPhone
        {
            get
            {
                return CellPhone.ApplyFormatPhone();
            }
        }

        public string Gender { get; set; }

        public string ClaimantGender
        {
            get
            {
                return XmlDataHelpper.Instance.GetValue(XmlDataTypeEnum.Gender.ToString(), Gender);
            }
        }
        public DateTime? Dob { get; set; }

        public string ClaimantDob
        {
            get
            {
                return Dob == null ? "" : Dob.GetValueOrDefault().ToString("MM/dd/yyyy");
            }
        }

        public DateTime? ExpirationDate { get; set; }

        public string ClaimantSsn { get; set; }
        public string ClaimantLanguage { get; set; }
        public string ClaimantAddress1 { get; set; }
        public string ClaimantAddress2 { get; set; }
        public string ClaimantState { get; set; }
        public string ClaimantCity { get; set; }
        public string ClaimantZip { get; set; }
    }

    public class InfoWhenChangeCaseManagerInReferral
    {
        public int? CaseManagerId { get; set; }
        public string CaseManagerAgency { get; set; }
        public string Phone { get; set; }

        public string CaseManagerPhone
        {
            get
            {
                return Phone.ApplyFormatPhone();
            }
        }
        public string Fax { get; set; }

        public string CaseManagerFax
        {
            get
            {
                return Fax.ApplyFormatPhone();
            }
        }

        public string CaseManagerEmail { get; set; }
        public string CaseManagerAddress { get; set; }
        public string CaseManagerInstruction { get; set; }
        public string CaseManagerState { get; set; }
        public string CaseManagerCity { get; set; }
        public string CaseManagerZip { get; set; }
    }

    public class InfoWhenChangeAttorneyInReferral
    {
        public int? AttorneyId { get; set; }
        public string Phone { get; set; }

        public string AttorneyPhone
        {
            get
            {
                return Phone.ApplyFormatPhone();
            }
        }

        public string AttorneyEmail { get; set; }
        public string AttorneyAddress { get; set; }
        public string AttorneyState { get; set; }
        public string AttorneyCity { get; set; }
        public string AttorneyZip { get; set; }
    }

    public class InfoWhenChangeEmployerInReferral
    {
        public int? EmployerId { get; set; }
        public string Phone { get; set; }

        public string EmployerPhone
        {
            get
            {
                return Phone.ApplyFormatPhone();
            }
        }

        public string EmployerAddress { get; set; }
        public string EmployerState { get; set; }
        public string EmployerCity { get; set; }
        public string EmployerZip { get; set; }
    }

    public class InfoWhenChangeCollectionSiteInReferral
    {
        public int? CollectionSiteId { get; set; }
        public string Phone { get; set; }

        public string CollectionSitePhone
        {
            get
            {
                return Phone.ApplyFormatPhone();
            }
        }

        public string Fax { get; set; }

        public string CollectionSiteFax
        {
            get
            {
                return Fax.ApplyFormatPhone();
            }
        }

        public string CollectionSiteAddress { get; set; }
    }

    public class InfoWhenChangeTreatingPhysicianInReferral
    {
        public int? TreatingPhysicianId { get; set; }
        public string Phone { get; set; }

        public string TreatingPhysicianPhone
        {
            get
            {
                return Phone.ApplyFormatPhone();
            }
        }

        public string Fax { get; set; }

        public string TreatingPhysicianFax
        {
            get
            {
                return Fax.ApplyFormatPhone();
            }
        }

        public string TreatingPhysicianAddress { get; set; }

        public string TreatingPhysicianEmail { get; set; }

        public string NpiNumberText { get; set; }

        public string TreatingPhysicianName { get; set; }
        
    }

    public class InfoWhenChangePanelTypeInReferral
    {
        public int? PanelTypeId { get; set; }
        public string PanelTypeCode { get; set; }
    }
}
