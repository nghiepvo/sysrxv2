﻿using System;
using Framework.DomainModel.Entities.Common;
using Framework.Utility;

namespace Framework.DomainModel.ValueObject
{
    public class ReferralParentGridVo : ReadOnlyGridVo
    {
        public int StatusId { get; set; }

        private string _status;
        public string Status
        {
            get
            {
                if (string.IsNullOrEmpty(_status))
                {
                    _status = XmlDataHelpper.Instance.GetValue(XmlDataTypeEnum.Status.ToString(), StatusId.ToString());
                }
                return _status;
            }
            set
            {
                _status = value;
            }
        }
        
        public int TotalNote { get; set; }
        public int TotalTask { get; set; }
        public int TotalTaskCompleted { get; set; }
        public string ControlNumber { get; set; }
        public string ProductType { get; set; }

        public DateTime StartDateDateTime { get; set; }
        public string StartDate
        {
            get { return StartDateDateTime.ToString("g"); }
        }

        public string DueDate
        {
            get { return DueDateDateTime.ToString("g"); }
        }

        public DateTime DueDateDateTime { get; set; }

        public DateTime? CancelDate { get; set; }
        public DateTime? CompletedDate { get; set; }

        public int Duration
        {
            get
            {
                return CaculatorHelper.CaculateDuration(StartDateDateTime, DueDateDateTime, CancelDate, CompletedDate);
            }
        }

        public string DurationFormat
        {
            get { return CaculatorHelper.CaculateFormatDuration(StartDateDateTime, DueDateDateTime, CancelDate, CompletedDate); }
        }

        public double DurationPercentLeft
        {
            get
            {
                if (DateTime.Now < DueDateDateTime)
                {
                    var percent = ((DateTime.Now - StartDateDateTime).TotalHours/
                                      (DueDateDateTime - StartDateDateTime).TotalHours)*100;
                    return percent< 21.00? 21.00: percent;
                }
                return (100 - (DurationPercentRight)) > 82.00 ? 82.00 : 100 - (DurationPercentRight);
            }
        }

        public double DurationPercentRight
        {
            get
            {
                var percent = ((DateTime.Now - DueDateDateTime).TotalHours / (DateTime.Now - StartDateDateTime).TotalHours) * 100;
                return percent < 17.00 ? 17.00 : percent;
            }
        }


        public bool CategoryDuration
        {
            get { return DateTime.Now < DueDateDateTime; }
        }

        public bool IsCollectionSite { get; set; }

        public string CollectionSiteName { get; set; }
        public string CollectionSitePhone { get; set; }

        public string NpiNumberName { get; set; }
        public string NpiNumberPhone { get; set; }

        private string _treatingPhysicianName;
        public string TreatingPhysicianName
        {
            get
            {
                if (string.IsNullOrEmpty(_treatingPhysicianName))
                {
                    _treatingPhysicianName = IsCollectionSite
                        ? ""
                        : NpiNumberName;
                }
                return _treatingPhysicianName;
            }
            set
            {
                _treatingPhysicianName = value;
            }
        }
        
        private string _treatingPhysicianPhone;
        public string TreatingPhysicianPhone
        {
            get
            {
                if (string.IsNullOrEmpty(_treatingPhysicianPhone))
                {
                    _treatingPhysicianPhone = IsCollectionSite ? "" : NpiNumberPhone.ApplyFormatPhone();
                }
                return _treatingPhysicianPhone;
            }
            set
            {
                _treatingPhysicianPhone = value;
            }
        }

        public string AssignTo { get; set; }
        public string Payer { get; set; }
        public string Jurisdiction { get; set; }
        public bool Rush { get; set; }
        
        public string ClaimantFirstName { get; set; }
        public string ClaimantMiddleName { get; set; }
        public string ClaimantLastName { get; set; }
        public string Claimant { get; set; }

        public string ClaimNumber { get; set; }
        public string PatientState { get; set; }
        public DateTime DoiDateTime { get; set; }

        public string Doi
        {
            get { return DoiDateTime.CompareTo(DateTime.MinValue) > 0? DoiDateTime.ToShortDateString(): string.Empty; }
        }

        public string CreatedDate
        {
            get { return CreatedDateDateTime.ToString("g"); }
        }
        public DateTime CreatedDateDateTime { get; set; }
        public string CreatedBy { get; set; }

    }
}
