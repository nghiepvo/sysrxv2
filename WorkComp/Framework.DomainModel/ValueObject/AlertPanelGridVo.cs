﻿using Framework.DomainModel.Entities.Common;
using Framework.Utility;

namespace Framework.DomainModel.ValueObject
{
    public class AlertPanelGridVo : ReadOnlyGridVo
    {
        public int Type { get; set; }

        public string TypeStr
        {
            get
            {
                return XmlDataHelpper.Instance.GetValue(XmlDataTypeEnum.TypeAlert.ToString(), Type.ToString());
            }
        }

        public int LinkId { get; set; }

        public string Object
        {
            get
            {
                return string.Format("{0}: {1}", TypeStr, LinkId);
            }
        }
        public string Message { get; set; }

        public bool IsRush { get; set; }

        public string ModelName
        {
            get { return Type == 1 ? "Referral" : "ReferralTask"; }
        }
    }
}
