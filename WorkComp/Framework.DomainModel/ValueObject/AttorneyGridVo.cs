﻿using Framework.Utility;

namespace Framework.DomainModel.ValueObject
{
    public class AttorneyGridVo : ReadOnlyGridVo
    {
        public string FirstName { get; set; }
        public string LastName { get; set; }
        public string MiddleName { get; set; }

        public string Name
        {
            get
            {
                return FirstName + " " + (string.IsNullOrEmpty(MiddleName) ? "" : MiddleName + " ") + LastName;
            }
        }

        public string Lawfirm { get; set; }

        private string _phone;
        public string Phone
        {
            get
            {
                return _phone.ApplyFormatPhone();
            }
            set
            {
                _phone = value;
            }
        }
        public string Email { get; set; }
        public string Address { get; set; }
        public string State { get; set; }
        public string City { get; set; }
        public string Zip { get; set; }
    }
}
