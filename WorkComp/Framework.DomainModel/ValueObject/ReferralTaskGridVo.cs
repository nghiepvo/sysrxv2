﻿using System;
using System.Globalization;
using Framework.DomainModel.Entities.Common;
using Framework.Utility;

namespace Framework.DomainModel.ValueObject
{
    public class ReferralTaskGridVo : ReadOnlyGridVo
    {
        public int StatusId { get; set; }

        public string Status
        {
            get
            {
                return XmlDataHelpper.Instance.GetValue(XmlDataTypeEnum.Status.ToString(), StatusId.ToString());
            }
        }

        public string Title { get; set; }

        public DateTime StartDateDateTime { get; set; }
        public string StartDate
        {
            get
            {
                return StartDateDateTime.ToString("g");
            }
        }

        public DateTime DueDateDateTime { get; set; }
        public string DueDate
        {
            get { return DueDateDateTime.ToString("g"); }
        }

        public DateTime? CancelDate { get; set; }
        public DateTime? CompletedDate { get; set; }

        public int Duration
        {
            get
            {
                return CaculatorHelper.CaculateDuration(StartDateDateTime, DueDateDateTime,CancelDate, CompletedDate);
            }
        }

        public string DurationFormat
        {
            get
            {
                if (Duration < 0)
                {
                    return "0d0h";
                }
                return (Duration / 24).ToString(CultureInfo.InvariantCulture) + "d" + (Duration % 24).ToString(CultureInfo.InvariantCulture) + "h";
            }
        }

        public double DurationPercentLeft
        {

            get
            {
                return CaculatorHelper.CaculateDurationPercentLeft(
                    DueDateDateTime, 
                    StartDateDateTime,
                    DurationPercentRight,
                    CancelDate,
                    CompletedDate
                 );

            }
        }

        public double DurationPercentRight
        {
            get
            {
                return CaculatorHelper.CaculateDurationPercentRight(DueDateDateTime, StartDateDateTime, CancelDate,
                    CompletedDate);
            }
        }

        public bool CategoryDuration
        {
            get
            {
                return CaculatorHelper.CaculateCategoryDuration(DueDateDateTime, CancelDate,CompletedDate);
            }
        }

        public string AssignTo { get; set; }
        

        public string CreatedDate
        {
            get { return CreatedDateDateTime.ToString("g"); }
        }
        public DateTime CreatedDateDateTime { get; set; }

        

        public string CreatedBy { get; set; }
        

    }
}
