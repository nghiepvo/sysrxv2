﻿using System.Collections.Generic;

namespace Framework.DomainModel.ValueObject
{
    public class ReasonReferralParrentVo
    {
        public int Id { get; set; }
        public string Name { get; set; }
        public bool HasChild { get; set; }
        public int? ReportsTo { get; set; }
        public bool IsActive { get; set; }
    }
}