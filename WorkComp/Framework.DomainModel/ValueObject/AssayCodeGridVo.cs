﻿namespace Framework.DomainModel.ValueObject
{
    public class AssayCodeGridVo : ReadOnlyGridVo
    {
        public string SampleTestingType { get; set; }
        public string Description { get; set; }
        public string Code { get; set; }
    }
}
