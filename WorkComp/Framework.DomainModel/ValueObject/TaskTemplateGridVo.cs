﻿using Framework.DomainModel.Entities.Common;
using Framework.Utility;

namespace Framework.DomainModel.ValueObject
{
    public class TaskTemplateGridVo : ReadOnlyGridVo
    {
        public string Title { get; set; }
        public string Description { get; set; }
        public int? NumDueDate { get; set; }
        public double? NumDueHour { get; set; }
        public int StatusId { get; set; }

        public string Status
        {
            get
            {
                return XmlDataHelpper.Instance.GetValue(XmlDataTypeEnum.Status.ToString(), StatusId.ToString());
            }
        }

        public string AssignTo { get; set; }
        public int? TaskTypeId { get; set; }

        public string Type
        {
            get
            {
                return TaskTypeId.GetValueOrDefault() == 0 ? "" : XmlDataHelpper.Instance.GetValue(XmlDataTypeEnum.TaskType.ToString(), TaskTypeId.GetValueOrDefault().ToString());
            }
        }

        public bool? IsReadOnly { get; set; }
    }
}