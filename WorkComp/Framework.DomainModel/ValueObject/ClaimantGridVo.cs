﻿using System;
using Framework.DomainModel.Entities.Common;
using Framework.Utility;

namespace Framework.DomainModel.ValueObject
{
    public class ClaimantGridVo : ReadOnlyGridVo
    {
        
        public string FirstName { get; set; }
        public string LastName { get; set; }
        public string MiddleName { get; set; }

        private string _name;
        public string Name
        {
            get
            {
                // Use column Name in 2 case: solr and get from db
                if (string.IsNullOrEmpty(_name))
                {
                    _name = FirstName + " " + MiddleName + " " + LastName;    
                }
                
                return _name;
            }
            set
            {
                _name = value;
            }
        }

        public string Gender { get; set; }

        private string _genderName;
        public string GenderName
        {
            get
            {
                if (string.IsNullOrEmpty(_genderName))
                {
                    return XmlDataHelpper.Instance.GetValue(XmlDataTypeEnum.Gender.ToString(), Gender);    
                }
                return _genderName;
            }
            set
            {
                _genderName = value;
            }
        }

        public DateTime? Birthday { get; set; }

        private string _dob;
        public string Dob
        {
            get
            {
                if (string.IsNullOrEmpty(_dob))
                {
                    _dob = Birthday!= null? Birthday.GetValueOrDefault().ToShortDateString(): "";
                }
                return _dob;
            }
            set
            {
                _dob = value;
            }
        }

        public string Language { get; set; }
        public string Ssn { get; set; }
        private string _bestContactNumber;

        public string BestContactNumber
        {
            get
            {
                return _bestContactNumber.ApplyFormatPhone();
            }
            set
            {
                _bestContactNumber = value;
            }
        }
        
        public string Email { get; set; }

        public string Address1 { get; set; }
        public string Address2 { get; set; }
        public string State { get; set; }
        public string City { get; set; }
        public string Zip { get; set; }

        private string _fullAddress;
        public string FullAddress
        {
            get
            {
                if (string.IsNullOrEmpty(_fullAddress))
                {
                     return CaculatorHelper.CaculateFormatFullAddress(Address1, State, City, Zip, Address2); 
                }

                return _fullAddress;
            }
            set
            {
                _fullAddress = value;
            }
        }

    }
}
