﻿using System;
using System.Globalization;
using System.Runtime.InteropServices;
using Framework.DomainModel.Entities.Common;
using Framework.Utility;

namespace Framework.DomainModel.ValueObject
{
    public class ReferralNoteGridVo : ReadOnlyGridVo
    {
        public string Comment { get; set; }

        public int? NoteTypeId { get; set; }
        public string NoteType
        {
            get
            {
                return XmlDataHelpper.Instance.GetValue(XmlDataTypeEnum.NoteType.ToString(), NoteTypeId.ToString());
            }
        }

        public string CreatedDate
        {
            get { return CreatedDateDateTime.ToString("g"); }
        }
        public DateTime CreatedDateDateTime { get; set; }

        public string CreatedByFirstName { get; set; }
        public string CreatedByLastName { get; set; }
        public string CreatedByMiddleName { get; set; }

        public string CreatedBy
        {
            get
            {
                return CreatedByFirstName + " " + CreatedByMiddleName + " " + CreatedByLastName;
            }
        }

        public string AssignToFirstName { get; set; }
        public string AssignToMiddleName { get; set; }
        public string AssignToLastName { get; set; }

        public string AssignTo
        {
            get
            {
                return AssignToFirstName + " " + AssignToMiddleName + " " + AssignToLastName;
            }
        }

        
    }
}
