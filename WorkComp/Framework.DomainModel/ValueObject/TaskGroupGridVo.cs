﻿namespace Framework.DomainModel.ValueObject
{
    public class TaskGroupGridVo : ReadOnlyGridVo
    {
        public string Name { get; set; }
        public bool? IsDefault { get; set; }
    }
}