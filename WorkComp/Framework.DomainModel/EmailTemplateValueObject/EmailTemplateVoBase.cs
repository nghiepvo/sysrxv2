﻿using System;
using System.Configuration;
using Framework.Utility;

namespace Framework.DomainModel.EmailTemplateValueObject
{
    public class EmailTemplateVoBase
    {

        protected string FormatDate
        {
            get { return "MMM dd, yyyy"; }
        }

        protected string UrlWebApplication
        {
            get { return ConfigurationManager.AppSettings["Url"]; }
        }

        public string UrlLogo
        {
            get { return UrlWebApplication + "Content/images/logo.png"; }
        }

        public int ReferralId { get; set; }
        public string DateNow { get { return DateTime.Now.ToString(FormatDate); } }
        public string AdjusterName { get; set; }
        public string PayerName { get; set; }
        public string PayerAddress { get; set; }
        public string PayerStateCityZip { get; set; }
        public string ClaimantName { get; set; }
        private string _claimantPhone;

        public string ClaimantPhone
        {
            get { return _claimantPhone.ApplyFormatPhone(); }
            set { _claimantPhone = value; }
        }

        public string ClaimantAddress { get; set; }
        public string ClaimantState { get; set; }
        public string ClaimantCity { get; set; }
        public string ClaimantZip { get; set; }
        public string ClaimNumber { get; set; }
        public DateTime? ClaimNumberDoiDateTime { get; set; }

        public string ClaimNumberDoi
        {
            get
            {
                return ClaimNumberDoiDateTime == null
                    ? ""
                    : ClaimNumberDoiDateTime.GetValueOrDefault().ToString(FormatDate);
            }
        }

        public string Jurisdiction { get; set; }
        public DateTime? ClaimantDobDateTime { get; set; }

        public string ClaimantDob
        {
            get
            {
                return ClaimNumberDoiDateTime == null ? "" : ClaimantDobDateTime.GetValueOrDefault().ToString(FormatDate);
            }
        }

        
        public string SampleTestingType { get; set; }
        public string PanelType { get; set; }

        //sample testing type + panel type
        public string ServiceRequest
        {
            get { return (string.IsNullOrEmpty(SampleTestingType)? "": SampleTestingType) + ", " + PanelType; }
        }

        public string AssignTo { get; set; }
    }
}