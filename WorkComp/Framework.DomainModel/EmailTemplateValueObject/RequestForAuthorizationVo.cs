﻿using System;
using Framework.Utility;

namespace Framework.DomainModel.EmailTemplateValueObject
{
    public class RequestForAuthorizationVo :EmailTemplateVoBase
    {
        public string LinkYesHref { get { return UrlWebApplication + "Referral/Copy/" + ReferralId; } }
    }
}